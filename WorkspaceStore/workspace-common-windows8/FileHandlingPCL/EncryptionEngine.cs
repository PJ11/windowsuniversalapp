﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Paddings;
using Org.BouncyCastle.Crypto.Parameters;



namespace FileHandlingPCL
{
    public class EncryptionEngine
    {
        private readonly Encoding _encoding;
        private readonly IBlockCipher _blockCipher;
        private PaddedBufferedBlockCipher _cipher;
        private IBlockCipherPadding _padding;
        private static EncryptionEngine _encryptionEngineInstance;

        private EncryptionEngine()
        {
            _blockCipher = new AesEngine();
            _encoding = Encoding.UTF8;
            _padding = new Pkcs7Padding();
        }

        public static EncryptionEngine EncryptionEngineInstance
        {
            get
            {
                if (_encryptionEngineInstance == null)
                {
                    _encryptionEngineInstance = new EncryptionEngine();
                }
                return _encryptionEngineInstance;
            }
        }
        public void SetPadding(IBlockCipherPadding padding)
        {
            if (padding != null)
                _padding = padding;
        }

        public string Encrypt(string plain, string key)
        {
            byte[] result = BouncyCastleCrypto(true, _encoding.GetBytes(plain), key);
            return Convert.ToBase64String(result);
        }

        //Changed code for file 
        public byte[] EncryptByteArray(byte[] plain, string key)
        {

            byte[] result = BouncyCastleCrypto(true, plain, key);
            return result;

        }
        public string Decrypt(string cipher, string key)
        {
            byte[] result = BouncyCastleCrypto(false, Convert.FromBase64String(cipher), key);
            return _encoding.GetString(result, 0, result.Length);
        }


        public byte[] DecryptByteArray(byte[] cipher, string key)
        {
            byte[] result = BouncyCastleCrypto(false, cipher, key);
            return result;

        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="forEncrypt"></param>
        /// <param name="input"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        /// <exception cref="CryptoException"></exception>
        private byte[] BouncyCastleCrypto(bool forEncrypt, byte[] input, string key)
        {
            try
            {
                _cipher = _padding == null ? new PaddedBufferedBlockCipher(_blockCipher) : new PaddedBufferedBlockCipher(_blockCipher, _padding);
                byte[] keyByte = _encoding.GetBytes(key);
                _cipher.Init(forEncrypt, new KeyParameter(keyByte));
                return _cipher.DoFinal(input);
            }
            catch (Org.BouncyCastle.Crypto.CryptoException ex)
            {
                throw new CryptoException(ex.Message);
            }
            catch(System.ArgumentNullException)
            {
                // Must be 128/192/256 bits
            }
            return null;
        }
    }
}
