﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PCLStorage;
using System.Threading.Tasks;
using System.IO;

namespace FileHandlingPCL
{
    public class FileManager
    {
        private String _errorMessage = String.Empty;
        private IFolder _rootFolder = null;
        private static FileManager _instance;

        public FileManager()
        {
            _rootFolder = FileSystem.Current.LocalStorage;
        }

        //Going forward, we can avoid object creation of File Manager for evry file operation.
        static public FileManager GetInstance()
        {
            if (_instance == null)
            {
                _instance = new FileManager();
                return _instance;
            }
            else
                return _instance;
        }
        public async Task CreateRootFolders()
        {
            try
            {
                await _rootFolder.CreateFolderAsync("root", CreationCollisionOption.ReplaceExisting);
                await _rootFolder.CreateFolderAsync("root\\Downloads", CreationCollisionOption.ReplaceExisting);
                await _rootFolder.CreateFolderAsync("root\\Documents", CreationCollisionOption.ReplaceExisting);
                await _rootFolder.CreateFolderAsync("root\\Photos", CreationCollisionOption.ReplaceExisting);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> CheckIfFileOrFolderExist(string path, string fileName)
        {
            IFolder workingFolder = await GetFolderFromPath(path);

            ExistenceCheckResult result = await workingFolder.CheckExistsAsync(fileName);

            if (result == ExistenceCheckResult.FileExists || result == ExistenceCheckResult.FolderExists)
                return true;
            else
                return false;
        }
        public async Task<string> GetFullPath()
        {
            string path = "";
            IFolder folder = null;
            try
            {
                folder = await GetFolderFromPath("");
                path = folder.Path;
            }
            catch (Exception e)
            {
                string msg = e.Message;
            }

            return path;

        }

        private async Task<IFolder> GetFolderFromPath(string path)
        {
            IFolder folder = null;
            IFileSystem fileSystem = FileSystem.Current;

            if (path == String.Empty)
                folder = _rootFolder;
            else
                folder = await fileSystem.GetFolderFromPathAsync(path);

            return folder;
        }

        public async Task<IFile> GetFileFromPath(string path, string filename)
        {
            IFolder workingFolder = await GetFolderFromPath(path);
            IFile file = await workingFolder.GetFileAsync(filename);
            return file;
        }

        //for creating a new file in desired path
        public async Task<bool> CreateFile(String fileName, String path, byte[] dataBytes, String encryptionKey)
        {
            try
            {
                byte[] encryptedByteArray = EncryptionEngine.EncryptionEngineInstance.EncryptByteArray(dataBytes, encryptionKey);
                IFolder workingFolder = await GetFolderFromPath(path);

                if (workingFolder != null)
                {
                    IFile file = await workingFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
                    using (Stream stream = await file.OpenAsync(PCLStorage.FileAccess.ReadAndWrite))
                    {
                        stream.Write(encryptedByteArray, 0, encryptedByteArray.Length);
                        stream.Flush();
                    }
                    _errorMessage = String.Empty;
                    return true;
                }
                else
                {
                    _errorMessage = "folder not found";
                    return false;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return false;
            }
        }

        //this function is for testing and debugging. Not for end developer use
        public async Task<bool> CreateFile(String fileName, String path, byte[] dataBytes)
        {
            try
            {
                IFolder workingFolder = await GetFolderFromPath(path);

                if (workingFolder != null)
                {
                    IFile file = await workingFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
                    using (Stream stream = await file.OpenAsync(PCLStorage.FileAccess.ReadAndWrite))
                    {
                        if (dataBytes != null)
                        {
                            stream.Write(dataBytes, 0, dataBytes.Length);
                            stream.Flush();
                        }
                    }
                    _errorMessage = String.Empty;
                    return true;

                }
                else
                {
                    _errorMessage = "folder not found";
                    return false;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return false;
            }
        }

        public async Task<bool> TestWriteFile(String fileName, String path, byte[] dataBytes)
        {
            try
            {
                IFolder workingFolder = await GetFolderFromPath(path);
                if (workingFolder != null)
                {
                    IFile file = await workingFolder.GetFileAsync(fileName);
                    using (Stream stream = await file.OpenAsync(PCLStorage.FileAccess.ReadAndWrite))
                    {
                        stream.Write(dataBytes, 0, dataBytes.Length);
                        stream.Flush();
                    }
                    _errorMessage = String.Empty;
                    return true;
                }
                else
                {
                    _errorMessage = "folder not found";
                    return false;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return false;
            }
        }

        //for deleting file 
        public async Task<bool> DeleteFile(String fileName, String path)
        {
            try
            {
                IFile workingFile = await GetFileFromPath(path, fileName);
                if (workingFile != null)
                {
                    await workingFile.DeleteAsync();
                    _errorMessage = String.Empty;
                    return true;
                }
                else
                {
                    _errorMessage = "Path does not exists";
                    return false;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return false;
            }

        }

        //for creating folder in desired path
        public async Task<bool> CreateFolder(String folderName, String path)
        {
            try
            {
                IFolder workingFolder = await GetFolderFromPath(path);
                if (workingFolder != null)
                {
                    await workingFolder.CreateFolderAsync(folderName, CreationCollisionOption.ReplaceExisting);
                    _errorMessage = String.Empty;
                    return true;
                }
                else
                {
                    _errorMessage = "folder not found";
                    return false;

                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return false;
            }
        }
        //for deleting folder
        public async Task<Boolean> DeleteFolder(String folderName, String path)
        {
            try
            {
                IFolder workingFolder = await GetFolderFromPath(path);

                if (workingFolder != null)
                {
                    IFolder folder = await workingFolder.GetFolderAsync(folderName);
                    await folder.DeleteAsync();
                    _errorMessage = String.Empty;
                    return true;
                }
                else
                {
                    _errorMessage = "folder not found";
                    return true;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return false;
            }
        }

        //for Move folder
        public async Task<Boolean> MoveFolder(string folderName, string sourcePath, string destinationPath)
        {
            try
            {
                IFolder workingFolder = await GetFolderFromPath(sourcePath);
                if (workingFolder != null)
                {
                    IFolder tempFolder = await workingFolder.GetFolderAsync(folderName);
                    IFolder destinationFolder = await GetFolderFromPath(destinationPath);
                    IFolder tempFolder1 = await destinationFolder.CreateFolderAsync(tempFolder.Name, CreationCollisionOption.ReplaceExisting);
                    tempFolder1 = tempFolder;
                    return true;
                }
                else
                {
                    _errorMessage = "folder not found";
                    return true;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return false;
            }

        }

        //for Rename folder
        public async Task<Boolean> RenameFolder(string oldfolderName, string newfolderName, string Path)
        {
            try
            {
                String ParentDir = Path.Substring(0,Path.LastIndexOf("\\"));
                IFolder workingFolder = await GetFolderFromPath(ParentDir);
               
                
                if (workingFolder != null)
                {
                   //Step1 : Create new folder 
                    //Creates new folder in the same directory and not sub directory
                    //Need to check of duplicate name and handling. as of now it gives a unique name if duplicate.
                   IFolder NewFolder = await workingFolder.CreateFolderAsync(newfolderName, CreationCollisionOption.GenerateUniqueName);
                   
                   //Step2: Need to copy all Files and folder from the old folder.
                   IFolder tempFolder = await workingFolder.GetFolderAsync(oldfolderName);
                   NewFolder = tempFolder;
                  //Step 3 : Need to delete the old folder.  
                  await DeleteFolder(oldfolderName, ParentDir);
                   
                    return true;
                }
                else
                {
                    _errorMessage = "folder not found";
                    return true;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return false;
            }

        }


        //for moving a file to new path
        public async Task<Boolean> MoveFile(string fileName, string sourcePath, string destinationPath, string decryptionKey)
        {
            try
            {
                byte[] bytes = await ReadFile(fileName, sourcePath, decryptionKey);
                bool result = false;
                if (null != bytes)
                {
                    result = await CreateFile(fileName, destinationPath, bytes, decryptionKey);

                    if(result)
                    {
                        await DeleteFile(fileName, sourcePath);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return false;
            }
        }


        //for Copy a file to newpath
        public async Task<Boolean> CopyFile(string fileName, string sourcePath, string destinationPath, string decryptionKey)
        {
            try
            {
                byte[] bytes = await ReadFile(fileName, sourcePath, decryptionKey);
                bool result = false;
                if(null != bytes)
                {
                    result = await CreateFile(fileName, destinationPath, bytes, decryptionKey);
                }
                return result;
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return false;
            }
        }

        //for rename a file 
        public async Task<Boolean> RenameFile(String newFileName, String oldFileName, String path)
        {
            try
            {
                IFile workingFile = await GetFileFromPath(path, oldFileName);
                if (workingFile != null)
                {
                    await workingFile.RenameAsync(newFileName);
                    _errorMessage = String.Empty;
                    return true;
                }
                else
                {
                    _errorMessage = "folder not found";
                    return false;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return false;
            }
        }


        //Writing content in existing file
        public async Task<bool> WriteInFile(String fileName, String path, byte[] dataInByteArray, String encryptionKey)
        {
            try
            {
                byte[] encryptedByteArry = EncryptionEngine.EncryptionEngineInstance.EncryptByteArray(dataInByteArray, encryptionKey);
                IFolder workingFolder = await GetFolderFromPath(path);
                if (workingFolder != null)
                {
                    IFile file = await workingFolder.GetFileAsync(fileName);
                    using (Stream stream = await file.OpenAsync(PCLStorage.FileAccess.ReadAndWrite))
                    {
                        stream.Write(encryptedByteArry, 0, encryptedByteArry.Length);
                        stream.Flush();
                        _errorMessage = String.Empty;
                        return true;
                    }
                }
                else
                {
                    _errorMessage = "folder not found";
                    return false;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return false;
            }
        }

        //Get all folders in a folder
        public async Task<IList<IFolder>> GetAllFolders(String path)
        {
            try
            {
                IFolder workingFolder = await GetFolderFromPath(path);
                if (workingFolder != null)
                {
                    IList<IFolder> folderList = await workingFolder.GetFoldersAsync();
                    return folderList;
                }
                else
                {
                    _errorMessage = "Folder not Found";
                    return null;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return null;
            }
        }

        //get all files in a folder
        public async Task<IList<IFile>> GetAllFiles(String folderPath)
        {
            try
            {
                IFolder workingFolder = await GetFolderFromPath(folderPath);

                if (workingFolder != null)
                {
                    IList<IFile> fileList = await workingFolder.GetFilesAsync();
                    return fileList;
                }
                else
                {
                    _errorMessage = "Folder not Found";
                    return null;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return null;
            }
        }

        private byte[] StreamToByteArray(Stream input)
        {
            /*byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                await ms.FlushAsync();
                return ms.ToArray();
            }*/

            using (var streamReader = new MemoryStream())
            {
                input.CopyTo(streamReader);
                byte[] dataBytes = streamReader.ToArray();
                streamReader.Flush();
                return dataBytes;
            }
        }
        //this function is for testing and debugging. Not for end developer use
        public async Task<Byte[]> TestReadFile(String fileName, String path)
        {
            try
            {

                IFolder workingFolder = await GetFolderFromPath(path); ;
                if (workingFolder != null)
                {
                    IFile file = await workingFolder.GetFileAsync(fileName);
                    using (Stream dataStream = await file.OpenAsync(PCLStorage.FileAccess.Read))
                    {
                        byte[] dataBytes = StreamToByteArray(dataStream);
                        _errorMessage = String.Empty;
                        dataStream.Flush();
                        return dataBytes;
                    }
                }
                else
                {
                    _errorMessage = "Folder not Found";
                    return null;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return null;
            }
        }

        //read content of a file and returns a byte array of content
        public async Task<Byte[]> ReadFile(String fileName, String path, String encryptionKey)
        {
            try
            {
                byte[] decryptedBytes = null;
                IFolder workingFolder = await GetFolderFromPath(path); ;
                if (workingFolder != null)
                {
                    //Get all files from folder
                    var getAllFiles = await workingFolder.GetFilesAsync();
                    //write query to get current file name which you want to read
                    var getFile = getAllFiles.Where(c => c.Name.ToLower() == fileName.ToLower());
                    //if not found then return null else read using below code
                    if (getFile == null || getFile.Count() < 1)
                    {
                        _errorMessage = "File not Found";
                        return null;
                    }
                    IFile file = await workingFolder.GetFileAsync(fileName);
                    using (Stream dataStream = await file.OpenAsync(PCLStorage.FileAccess.Read))
                    {
                        byte[] dataBytes = StreamToByteArray(dataStream);
                        decryptedBytes = EncryptionEngine.EncryptionEngineInstance.DecryptByteArray(dataBytes, encryptionKey);

                        _errorMessage = String.Empty;
                        dataStream.Flush();
                        return decryptedBytes;
                    }
                }
                else
                {
                    _errorMessage = "Folder not Found";
                    return null;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return null;
            }
        }

        public async Task<Byte[]> ReadFileFullPath(String pathAndFileName, String encryptionKey)
        {
            try
            {
                // Tease apart the filename from the path name
                string[] diskItems = pathAndFileName.Split('\\');
                string fileName = "";
                string path = "";

                for (int i = 0; i < diskItems.Length - 1; i++)
                {
                    path += diskItems[i];
                    if (i < diskItems.Length - 2)
                        path += '\\';
                }
                fileName = diskItems[diskItems.Length - 1];

                byte[] decryptedBytes = null;

                IFolder workingFolder = await GetFolderFromPath(path); ;
                if (workingFolder != null)
                {
                    IFile file = await workingFolder.GetFileAsync(fileName);
                    using (Stream dataStream = await file.OpenAsync(PCLStorage.FileAccess.Read))
                    {
                        byte[] dataBytes = StreamToByteArray(dataStream);
                        decryptedBytes = EncryptionEngine.EncryptionEngineInstance.DecryptByteArray(dataBytes, encryptionKey);

                        _errorMessage = String.Empty;
                        dataStream.Flush();
                        return decryptedBytes;
                    }
                }
                else
                {
                    _errorMessage = "Folder not Found";
                    return null;
                }
            }
            catch (Exception ex)
            {
                _errorMessage = ex.Message;
                return null;
            }
        }
    }

}
