﻿using PCLStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandlingPCL
{
    public abstract class FileManagerBase
    {
        static FileManagerBase instance = null;
        public static FileManagerBase Instance
        {
            get
            {
                return instance;
            }
            set
            {
                instance = value;
            }
        }


        public abstract Task CreateFolder(String folderName, String path);
        public abstract Task DeleteFolder(String fileName, String path);
        public abstract Task CreateFile(String fileName, String path, byte[] dataBytes, String encryptionKey);
        public abstract Task DeleteFile(String fileName, String path);
        public abstract Task<IList<IFile>> GetAllFiles(String path);
        public abstract Task<IList<IFolder>> GetAllFolders(String path);
        public abstract Task<Byte[]> ReadFile(String fileName, String filePath, String encryptionKey);
        public abstract Task WriteInFile(String fileName, String filePath, Byte[] dataBytes, String encryptionKey);

    }
}


