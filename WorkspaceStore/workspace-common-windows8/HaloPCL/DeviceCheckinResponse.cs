﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{
    [DataContract]
    public sealed class DeviceCheckinResponse
    {
        [DataMember(Name = "deviceElements", EmitDefaultValue = true)]
        public List< HaloPCL.ConfigurationElementLean> deviceElements { get; set; }

        [DataMember(Name = "fullConfiguration", EmitDefaultValue = true)]
        public bool fullConfiguration { get; set; }

        [DataMember(Name = "shouldSendRemoteCommand", EmitDefaultValue = true)]
        public bool shouldSendRemoteCommand { get; set; }

        [DataMember(Name = "isJailBroken", EmitDefaultValue = true)]
        public bool isJailBroken { get; set; }

        [DataMember(Name = "compliantStatus", EmitDefaultValue = true)]
        public int compliantStatus { get; set; }

        [DataMember(Name = "lastUpdatedAt", EmitDefaultValue = true)]
        public long lastUpdatedAt { get; set; }

        [DataMember(Name = "personInfoLean", EmitDefaultValue = true)]
        public PersonInfoLean personInfoLean { get; set; }



        //[DataMember(Name = "deviceElements")]
        //public ConfigurationElementLean[] DeviceElements { get; set; }

        //[DataMember(Name = "fullConfiguration")]
        //public bool FullConfiguration { get; set; }

        //[DataMember(Name = "shouldSendRemoteCommand")]
        //public bool ShouldSendRemoteCommand { get; set; }

        //[DataMember(Name = "isJailBroken")]
        //public bool IsJailBroken { get; set; }

        //[DataMember(Name = "compliantStatus")]
        //public int CompliantStatus { get; set; }

        //[DataMember(Name = "passcodeCompliant")]
        //public bool PasscodeCompliant { get; set; }
        ///*public bool PasscodeCompliant { get; set; }*/

        //[DataMember(Name = "computeJailbreak")]
        //public bool ComputeJailbreak { get; set; }

        //[DataMember(Name = "personInfoLean")]
        //public PersonInfoLean PersonInfoLean { get; set; }

        //[DataMember(Name = "lastUpdatedAt")]
        //public DateTime LastUpdatedAt { get; set; } // DateTime that can be null coming back from server, change to Object then cast later

        ///*public List<ConfigurationElementLean> PasscodeProfileDescription { get; set; }*/

        //public static DeviceCheckinResponse fromJSON(Stream inputStream)
        //{
        //    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(DeviceCheckinResponse));
        //    DeviceCheckinResponse jsonResponse = null;
        //    try
        //    {
        //        //StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
        //        //string text = reader.ReadToEnd(); 
        //        object objResponse = jsonSerializer.ReadObject(inputStream);
        //        jsonResponse = (DeviceCheckinResponse)objResponse;
        //    }
        //    catch (Exception e)
        //    {
        //        String msg = e.Message;
        //        jsonResponse = new DeviceCheckinResponse();
        //        //string text = reader.ReadToEnd();
        //    }
        //    finally
        //    {
        //        inputStream.Close();
        //    }
        //    return jsonResponse;
        //}
    }
}
