﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{
    public class StratusType
    {

        protected String name;
        protected String description;
        protected int type;

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getDescription()
        {
            return description;
        }

        public void setDescription(String description)
        {
            this.description = description;
        }

        public int getType()
        {
            return type;
        }

        public void setType(int type)
        {
            this.type = type;
        }
    }

    [DataContract]
    public sealed class DeviceAction
    {
        [DataMember(Name = "id", EmitDefaultValue = true)]
        public int id { get; set; }

        [DataMember(Name = "registrationPriority", EmitDefaultValue = true)]
        public int registrationPriority { get; set; }

        [DataMember(Name = "priority", EmitDefaultValue = true)]
        public int priority { get; set; }

        [DataMember(Name = "name", EmitDefaultValue = true)]
        public String name { get; set; }

        [DataMember(Name = "isAutoSuccess", EmitDefaultValue = false)]
        public bool isAutoSuccess { get; set; }

        [DataMember(Name = "type", EmitDefaultValue = false)]
        public int type { get; set; }

    }

    //public sealed class DeviceAction : StratusType
    //{
    //    private HashSet<DeviceType> dTypes = new HashSet<DeviceType>();

    //    public int Id { get; set; }

    //    public int RegistrationPriority { get; set; }

    //    public int Priority { get; set; }

    //    public bool AutoSuccess { get; set; }

    //    public HashSet<DeviceType> DeviceTypes 
    //    {
    //        get
    //        {
    //            return dTypes;
    //        }

    //        set
    //        {
    //            dTypes = value;
    //        }
    //    }

    //    public void fromJSON(string optJSONObject) 
    //    {
    //        if (optJSONObject != null)
    //        {
    //            JSONObject jo = JSONObject.DeserializeObject(optJSONObject);
    //            this.Id = (int)jo["id"];
    //            this.RegistrationPriority = (int)jo["registrationPriority"];
    //            this.Priority = (int)jo["priority"];
    //            this.setName(jo["name"] as string);
    //            this.AutoSuccess = (bool)jo["isAutoSuccess"];
    //            this.setType((int)jo["type"]);
    //        }
    //    }
    //}
}
