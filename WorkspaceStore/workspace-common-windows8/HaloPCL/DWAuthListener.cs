﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{
    public interface DWAuthListener : DWListener
    {
        void onAuthenticateResult(DWStatusCode state, String reason);
    }
}
