﻿using HaloPCL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HaloPCL
{
    public class SchedulerManager
    {
        private bool InstanceFieldsInitialized = false;

        private void InitializeInstanceFields()
        {
            fThread = new SchedulerThread(this);
        }


        protected internal readonly long fId;
        protected internal SchedulerThread fThread;
        protected internal readonly long fMilisecond;
        protected internal readonly bool fOneTime;
        protected internal readonly long fDelegate;
        private long fParam;



        private static IDictionary<object, SchedulerManager> fThreadmap = new Dictionary<object, SchedulerManager>();

        public SchedulerManager(long id, long milisecond, bool oneTime, long @delegate, long param)
        {
            if (!InstanceFieldsInitialized)
            {
                InitializeInstanceFields();
                InstanceFieldsInitialized = true;
            }
            fId = id;
            fMilisecond = milisecond;
            fOneTime = oneTime;
            fDelegate = @delegate;
            fParam = param;
        }


        private static void runCallback(long id, long @delegate, long param)
        {


        }

        protected internal class SchedulerThread /*: System.Threading.Thread*/
        {
            private readonly SchedulerManager outerInstance;



            internal bool terminated = false;

            internal SchedulerThread(SchedulerManager outerInstance)
            {
                this.outerInstance = outerInstance;
                outerInstance.fThread = this;
            }


            public virtual void halt()
            {
                lock (this)
                {

                    terminated = true;
                    Monitor.Pulse(this);
                }
            }


            public void run()
            {

                lock (this)
                {
                    try
                    {
                        do
                        {
                            try
                            {
                                Monitor.Wait(this, TimeSpan.FromMilliseconds(outerInstance.fMilisecond));
                            }
                            catch (Exception e)
                            {
                                HaloLog.v("SchedulerThread interrupted.");
                                break;
                            }

                            if (terminated)
                            {
                                HaloLog.d("Scheduled thead-process exited.");
                                break;
                            }
                            else
                            {
                                runCallback(outerInstance.fId, outerInstance.fDelegate, outerInstance.fParam);
                            }

                        } while (!outerInstance.fOneTime);
                    }
                    catch (Exception)
                    {
                        HaloLog.e("Unable to send outcome to UI thread.");
                    }
                }
            }

        }

        public static int start(long id, long milisecond, bool oneTime, long @delegate, long param)
        {
            lock (fThreadmap)
            {
                //			try {
                //				stop(id);
                //				SchedulerManager proc = new SchedulerManager(id, milisecond,
                //						oneTime, delegate, param);
                //				proc.fThread.start();
                //				fThreadmap.put(id, proc);
                //
                //			} catch (Exception ex) {
                //				return (int)DWStatusCode.DWStatus_FAIL;
                //			}
                return (int)DWStatusCode.DWStatus_SUCCESS;
            }
        }

        public static int stopAll()
        {
            lock (fThreadmap)
            {
                //			try {
                //				for (SchedulerManager proc : fThreadmap.values()) {
                //					if (proc != null) {
                //						proc.fThread.halt();
                //					}
                //				}
                //				fThreadmap.clear();
                //
                //			} catch (Exception ex) {
                //				Log.e("Unable to stop all threads.", ex);
                //				return (int)DWStatusCode.DWStatus_FAIL;
                //			}

                return (int)DWStatusCode.DWStatus_SUCCESS;
            }
        }

        public static int stop(long id)
        {
            lock (fThreadmap)
            {
                //			try {
                //				SchedulerManager proc = fThreadmap.remove(id);
                //				if (proc != null) {
                //					proc.fThread.halt();
                //				}
                //
                //			} catch (Exception ex) {
                //				return DWStatusCode.DWStatus_FAIL.code;
                //			}

                return (int)DWStatusCode.DWStatus_SUCCESS;
            }
        }

    }
}
