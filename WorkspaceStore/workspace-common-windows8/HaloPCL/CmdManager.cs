﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{
    public sealed class CmdManager
    {
        static Dictionary<int, DWListener> mCmd = new Dictionary<int, DWListener>();
        private readonly static object syncLock = new object();

        public static void addCommand(int id, DWListener mCallback)
        {
            lock (syncLock)
            {
                mCmd.Add(id, mCallback);
            }
        }

        public static DWListener getCommand(int id)
        {
            lock (syncLock)
            {
                if (mCmd.ContainsKey(id))
                {
                    DWListener listener = mCmd[id];
                    mCmd.Remove(id);
                    return listener;
                }
                return null;

            }
        }
    }
}
