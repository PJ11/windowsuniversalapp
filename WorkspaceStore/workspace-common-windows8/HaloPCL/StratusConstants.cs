﻿using System;

namespace HaloPCL
{
    public class StratusConstants
    {
        public static long HourMs = 60 * 60 * 1000;
        public static long DayMs = 24 * HourMs;
        public static String DATEFMT = "yyyy-MM-dd-HH-mm-ss-SS";
        public static String DATE_ONLY_FORMAT = "MMMM dd, yyyy (EEEE)";
        static String DATEFORMAT = "yyyy-MM-dd HH:mm:ss z";
        static String DATEFORMAT_NO_TZ = "yyyy-MM-dd HH:mm:ss";
        static String DATEFORMAT_WITH_TZ = "EEE MMM dd  HH:mm:ss z yyyy";
        public static String ENCODE = "UTF-8";
        public static String SYSTEM_SUPER_TENANT = "supertenant";
        public static String Devicefile = "devicefile";
        public static long MinCheckinWaitMs = 30000;
        public static long MinDupCmdWaitMs = 30000;
        public static int PrefixLength = 4;
        public static int MaxPrefixTry = 128;
        public static String ALPHANUM_PREFIX = "abcdefghijklm0123456789nopqrstuvwxyz";
        public static int DefaultSearchLimit = 20;
        public static int AllowFailedLoginAttempt = 8;
        public static int MaxThreadNum = 7;
        public static int MinThreadNum = 4;
        public static int MinItemsPerThread = 20;
        public static long FailedLoginInterval = 1000 * 60 * 15; // 15 minutes
        public static int BatchSize = 100;
        public static long MaxUploadImageSize = 20971520; // Max 20 MB size
        public static long MaxUploadImageLogoSize = 102400; // Max 100KB size
        public static long MaxLogoFilenameLength = 128; // Max Logo file name length
        public static long MaxUploadEulaTxtSize = 51200; // Max 50KB size
        public static String ANDROID_ADMIN_APK_FILE_NM = "DeviceAdmin.apk";


        public static String STRATUS_DIR = "stratusapp.server.url";
        public static String STRATUS_DATA_DIR = "data";
        // user picture storage
        public static String IMAGE_DIR = "data/images";
        public static String IMAGE_URL = "/images";
        // datalocker storage
        public static String FILE_LOCKER_DIR = "data/fileLocker";
        public static String FILE_LOCKER_URL = "/fileLocker";
        // ThinClient Image Repository
        public static String TCIMAGEREPOSITORY_DIR = "data/tcimagerepository";
        public static String TCWALLPAPERREPOSITORY_DIR = "data/tcwallpaperrepository";
        public static String TC_LOGO_REPOSITORY_DIR = "data/tclogorepository";
        public static String TC_EULA_REPOSITORY_DIR = "data/tceularepository";

        // adConnector (on premises installer repository)
        public static String ADCONNECTOR_DIR = "data/adConnector";
        public static String CCMPROXY_VERSION_FILE_NAME = "version.txt";

        //PC-Agent storage
        public static String PC_AGENT_DIR = "data/pcAgent";
        public static String PC_AGENT_DOWNLOAD_URL = "/open/{tenantId}/windowsagent";
        public static String PC_AGENT_LATEST_DOWNLOAD_URL = "/open/latest/windowsagent";
        public static String PC_AGENT_MSI_FILE_NAME = "WindowsAgent";
        public static String PC_AGENT_MSI_EXTENSION = ".msi";
        //public static String PC_AGENT_DEVELOPER = "Wyse";
        //public static String PC_AGENT_APP_NAME = "PC Agent";

        //enterprise app storage
        public static String ENTERPRISE_APP_DIR = "data/enterpriseApp";
        public static String ENTERPRISE_APP_URL = "/enterpriseApp";
        public static String ENTERPRISE_APP_IOS_PREFIX = "itms-services://?action=download-manifest&url=";

        public static class ENTERPRISE_APP_FILE_TYPE
        {
            public static int FILE_INVALID = 0;
            public static int FILE_APK = 1;
            public static int FILE_IPA = 2;
            public static int FILE_PLIST = 3;
        }

        // constant for license expiry
        public static long Be4MidNight = 86399000; //<- 23h 59m 59s <= (((23 * 60) + 59) * 60 + 59) * 1000;

        public static class APP_MANAGEMENT_FLAGS
        {
            public static int RemovedAppWhenNotManaged = 0x00000001;
            public static int AllowDataBackup = 0x00000004;
        }

        public static class COMPLIANCE_TYPE
        {
            public static int CheckinCompliance = 1;
            public static int AppCompliance = 2;
            public static int ConfigCompliance = 3;
            public static int PasscodeCompliance = 4;
            public static int JailbrokenCompliance = 5;
            public static int DeviceEncryptionCompliance = 6;
        }

        public static class COMPLIANT_STATUS
        {
            public static int Compliant = 1;
            public static int Pending = 0;
            public static int NotCompliant = -1;
        }

        public static class PROFILE_INSTALL_TYPE
        {
            public static int Profile = 1;
            public static int Checkin = 2;
            public static int OnDemand = 3;
        }

        public static class APP_POLICY
        {
            public static int DoNothing = 0;
            public static int Block = 1;
            public static int Required = 2;
        }

        public static class APP_STATUS
        {
            public static String NeedsRedemption = "NeedsRedemption";
            public static String Redeeming = "Redeeming";
            public static String Prompting = "Prompting";
            public static String Installing = "Installing";
            public static String Managed = "Managed";
            public static String ManagedButUninstalled = "ManagedButUninstalled";
            public static String Unknown = "Unknown";
            public static String UserInstalledApp = "UserInstalledApp";
            public static String UserRejected = "UserRejected";
            public static String Failed = "Failed";
            public static String PromptingForLogin = "PromptingForLogin";
            public static String PromptingForUpdate = "PromptingForUpdate";
            public static String PromptingForUpdateLogin = "PromptingForUpdateLogin";
            public static String Queued = "Queued";
            public static String UpdateRejected = "UpdateRejected";
            public static String UserRejectedRemoval = "UserRejectedRemoval";
        }

        public static class APP_VPP_REDEMPTION_CODE_STATUS
        {
            public static String Redeemed = "Redeemed";
        }

        public static class APP_SUPPORTED_DEVICE
        {
            public static String Ipad = "iPad";
            public static String Iphone = "iPhone";
            public static String Android = "Android";
        }

        //used for enterprise apps
        public static class APP_SUPPORTED_DEVICE_TYPE
        {
            public static int Android = 1;
            public static int Iphone = 2;
            public static int Ipad = 3;
            public static int IphoneAndIpad = 4;
        }

        public static class DEFAULT_APP_POLICY
        {
            public static bool AllowDataBackup = true;
            public static bool RemoveWhenNotManaged = true;
            public static int Policy = APP_POLICY.DoNothing;
        }

        public static class APPSTORE_PARAMETERKEY
        {
            public static String Term = "term";
            public static String Country = "country";
            public static String Media = "media";
            public static String Entity = "entity";
            public static String Attribute = "attribute";
            public static String Callback = "callback";
            public static String Limit = "limit";
            public static String Lang = "lang";
            public static String Version = "version";
            public static String Explicit = "explicit";
        }

        public static class APPSTORE_MEDIA
        {
            public static String Movie = "movie";
            public static String Podcast = "podcast";
            public static String Music = "music";
            public static String MusicVideo = "musicVideo";
            public static String ShortFilm = "shortFilm";
            public static String TvShow = "tvShow";
            public static String Software = "software";
            public static String Ebook = "ebook";
            public static String All = "all";
        }
        /*
            public static class COUNTRY {
                private static int AD = 1; // Andorra
                private static int AE = 2; // United Arab Emirates
                private static int AF = 3; // Afghanistan
                private static int AG = 4; // Antigua and Barbuda
                private static int AI = 5; // Anguilla
                private static int AL = 6; // Albania
                private static int AM = 7; // Armenia
                private static int AO = 8; // Angola
                private static int AQ = 9; // Antarctica
                private static int AR = 10; // Argentina
                private static int AS = 11; // American Samoa
                private static int AT = 12; // Austria
                private static int AU = 13; // Australia
                private static int AW = 14; // Aruba
                private static int AX = 15; // Aland Islands
                private static int AZ = 16; // Azerbaijan
                private static int BA = 17; // Bosnia and Herzegovina
                private static int BB = 18; // Barbados
                private static int BD = 19; // Bangladesh
                private static int BE = 20; // Belgium 1974
                private static int BF = 22; // Burkina Faso
                private static int BG = 23; // Bulgaria
                private static int BH = 24; // Bahrain
                private static int BI = 25; // Burundi
                private static int BJ = 26; // Benin
                private static int BL = 27; // Saint BarthŽlemy
                private static int BM = 28; // Bermuda
                private static int BN = 29; // Brunei Darussalam
                private static int BO = 30; // Bolivia, Plurinational State
                private static int BQ = 31; // Bonaire, Sint Eustatius and Saba

                private static int BR = 32; // Brazil
                private static int BS = 33; // Bahamas
                private static int BT = 34; // Bhutan
                private static int BV = 35; // Bouvet Island
                private static int BW = 36; // Botswana
                private static int BY = 37; // Belarus
                private static int BZ = 38; // Belize
                private static int CA = 39; // Canada
                private static int CC = 40; // Cocos (Keeling) Islands
                private static int CD = 41;// Congo, the Democratic Republic of
                                                    // the
                private static int CF = 42; // Central African Republic
                private static int CG = 43; // Congo
                private static int CH = 44; // Switzerland
                private static int CI = 45; // C™te d'Ivoire
                private static int CK = 46; // Cook Islands
                private static int CL = 47; // Chile
                private static int CM = 48; // Cameroon
                private static int CN = 49; // China
                private static int CO = 50; // Colombia
                private static int CR = 51; // Costa Rica
                private static int CU = 52; // Cuba
                private static int CV = 53; // Cape Verde
                private static int CW = 54; // Curacao
                private static int CX = 55; // Christmas Island
                private static int CY = 56;// Cyprus
                private static int CZ = 57; // Czech Republic
                private static int DE = 58; // Germany
                private static int DJ = 59; // Djibouti
                private static int DK = 60; // Denmark
                private static int DM = 61; // Dominica
                private static int DO = 62; // Dominican Republic
                private static int DZ = 63; // Algeria
                private static int EC = 64; // Ecuador
                private static int EE = 65; // Estonia
                private static int EG = 66; // Egypt
                private static int EH = 67; // Western Sahara
                private static int ER = 68; // Eritrea
                private static int ES = 69; // Spain
                private static int ET = 70; // Ethiopia
                private static int FI = 71; // Finland
                private static int FJ = 72; // Fiji
                private static int FK = 73; // Falkland Islands (Malvinas
                private static int FM = 74; // Micronesia, Federated States of

                private static int FO = 75; // Faroe Islands
                private static int FR = 76; // France
                private static int GA = 77; // Gabon
                private static int GB = 78; // United Kingdom
                private static int GD = 79; // Grenada
                private static int GE = 80;// Georgia
                private static int GF = 81; // French Guiana
                private static int GG = 82; // Guernsey
                private static int GH = 83; // Ghana
                private static int GI = 84; // Gibraltar
                private static int GL = 85; // Greenland
                private static int GM = 86; // Gambia
                private static int GN = 87; // Guinea
                private static int GP = 88; // Guadeloupe
                private static int GQ = 89;// Equatorial Guinea
                private static int GR = 90;// Greece
                private static int GS = 91;// South Georgia and the South Sandwich
                                                    // Islands
                private static int GT = 92;// Guatemala
                private static int GU = 93;// Guam
                private static int GW = 94;// Guinea-Bissau
                private static int GY = 95;// Guyana
                private static int HK = 96;// Hong Kong
                private static int HM = 97;// Heard Island and McDonald Islands
                private static int HN = 98;// Honduras
                private static int HR = 99;// Croatia
                private static int HT = 100;// Haiti
                private static int HU = 101;// Hungary
                private static int ID = 102;// Indonesia
                private static int IE = 103;// Ireland
                private static int IL = 104;// Israel
                private static int IM = 105;// Isle of Man
                private static int IN = 106;// India
                private static int IO = 107;// British Indian Ocean Territory
                private static int IQ = 108;// Iraq
                private static int IR = 109;// Iran, Islamic Republic of
                private static int IS = 110;// Iceland
                private static int IT = 111;// Italy
                private static int JE = 112;// Jersey
                private static int JM = 113;// Jamaica
                private static int JO = 114;// Jordan
                private static int JP = 115;// Japan
                private static int KE = 116;// Kenya
                private static int KG = 117;// Kyrgyzstan
                private static int KH = 118;// Cambodia
                private static int KI = 119;// Kiribati
                private static int KM = 120;// Comoros
                private static int KN = 130;// Saint Kitts and Nevis
                private static int KP = 140;// Korea, Democratic People's Republic
                                                    // of
                private static int KR = 141;// Korea, Republic of
                private static int KW = 142;// Kuwait
                private static int KY = 143;// Cayman Islands
                private static int KZ = 144;// Kazakhstan
                private static int LA = 145;// Lao People's Democratic Republic
                private static int LB = 146;// Lebanon
                private static int LC = 147;// Saint Lucia
                private static int LI = 148;// Liechtenstein
                private static int LK = 149;// Sri Lanka
                private static int LR = 150;// Liberia
                private static int LS = 151;// Lesotho
                private static int LT = 152;// Lithuania
                private static int LU = 153;// Luxembourg
                private static int LV = 154;// Latvia
                private static int LY = 155;// Libya
                private static int MA = 156;// Morocco
                private static int MC = 157;// Monaco
                private static int MD = 158;// Moldova, Republic of
                private static int ME = 159;// Montenegro
                private static int MF = 160;// Saint Martin (French part)
                private static int MG = 161;// Madagascar
                private static int MH = 162;// Marshall Islands
                private static int MK = 163;// Macedonia, the former Yugoslav
                                                    // Republic of
                private static int ML = 164; // Mali
                private static int MM = 165;// Myanmar
                private static int MN = 166;// Mongolia
                private static int MO = 167;// Macao
                private static int MP = 168;// Northern Mariana Islands
                private static int MQ = 169;// Martinique
                private static int MR = 170;// Mauritania
                private static int MS = 171;// Montserrat
                private static int MT = 172;// Malta
                private static int MU = 173;// Mauritius
                private static int MV = 174;// Maldives
                private static int MW = 175;// Malawi
                private static int MX = 176;// Mexico
                private static int MY = 177;// Malaysia
                private static int MZ = 178;// Mozambique
                private static int NA = 179;// Namibia
                private static int NC = 180;// New Caledonia
                private static int NE = 181;// Niger
                private static int NF = 182;// Norfolk Island
                private static int NG = 183;// Nigeria
                private static int NI = 184;// Nicaragua
                private static int NL = 185;// Netherlands
                private static int NO = 186;// Norway
                private static int NP = 187;// Nepal
                private static int NR = 188;// Nauru
                private static int NU = 189;// Niue
                private static int NZ = 190;// New Zealand
                private static int OM = 191;// Oman
                private static int PA = 192;// Panama
                private static int PE = 193;// Peru
                private static int PF = 194;// French Polynesia
                private static int PG = 195;// Papua New Guinea
                private static int PH = 196;// Philippines
                private static int PK = 197;// Pakistan
                private static int PL = 198;// Poland
                private static int PM = 199;// Saint Pierre and Miquelon
                private static int PN = 200;// Pitcairn
                private static int PR = 201;// Puerto Rico
                private static int PS = 202;// Palestinian Territory, Occupied
                private static int PT = 203;// Portugal
                private static int PW = 204; // Palau
                private static int PY = 205;// Paraguay
                private static int QA = 206;// Qatar
                private static int RE = 207;// RŽunion
                private static int RO = 208;// Romania
                private static int RS = 209;// Serbia
                private static int RU = 210;// Russian Federation
                private static int RW = 211;// Rwanda
                private static int SA = 212;// Saudi Arabia
                private static int SB = 213;// Solomon Islands
                private static int SC = 214;// Seychelles
                private static int SD = 215;// Sudan
                private static int SE = 216;// Sweden
                private static int SG = 217;// Singapore
                private static int SH = 218;// Saint Helena, Ascension and Tristan
                                                    // da Cunha
                private static int SI = 219;// Slovenia
                private static int SJ = 220;// Svalbard and Jan Mayen
                private static int SK = 221;// Slovakia
                private static int SL = 222;// Sierra Leone
                private static int SM = 223; // San Marino
                private static int SN = 224;// Senegal
                private static int SO = 225;// Somalia
                private static int SR = 226;// Suriname
                private static int SS = 227;// South Sudan
                private static int ST = 228;// Sao Tome and Principe
                private static int SV = 229;// El Salvador
                private static int SX = 230;// Sint Maarten (Dutch part)
                private static int SY = 231;// Syrian Arab Republic
                private static int SZ = 232;// Swaziland
                private static int TC = 233;// Turks and Caicos Islands
                private static int TD = 234;// Chad
                private static int TF = 235;// French Southern Territories
                private static int TG = 236;// Togo
                private static int TH = 237;// Thailand
                private static int TJ = 238;// Tajikistan
                private static int TK = 239;// Tokelau
                private static int TL = 240;// Timor-Leste
                private static int TM = 241;// Turkmenistan
                private static int TN = 242;// Tunisia
                private static int TO = 243;// Tonga
                private static int TR = 244;// Turkey
                private static int TT = 245;// Trinidad and Tobago
                private static int TV = 246;// Tuvalu
                private static int TW = 247;// Taiwan
                private static int TZ = 248;// Tanzania, United Republic of
                private static int UA = 249;// Ukraine
                private static int UG = 250;// Uganda
                private static int UM = 251;// United States Minor Outlying
                                                    // Islands
                private static int US = 252;// United States
                private static int UY = 253;// Uruguay
                private static int UZ = 254;// Uzbekistan
                private static int VA = 255;// Holy See (Vatican City State)
                private static int VC = 256;// Saint Vincent and the Grenadines
                private static int VE = 257;// Venezuela, Bolivarian Republic of
                private static int VG = 258;// Virgin Islands, British
                private static int VI = 259;// Virgin Islands, U.S.
                private static int VN = 260;// Viet Nam
                private static int VU = 270;// Vanuatu
                private static int WF = 271;// Wallis and Futuna
                private static int WS = 272;// Samoa
                private static int YE = 273;// Yemen
                private static int YT = 274;// Mayotte
                private static int ZA = 275;// South Africa
                private static int ZM = 276;// Zambia
                private static int ZW = 277;// Zimbabwe
            }
        */
        public static class SOFTWARE_ENTITY
        {
            public static String Software = "software";
            public static String IPadSoftware = "iPadSoftware";
            public static String MacSoftware = "macSoftware";
        }

        public static class SOFTWARE_ATTRIBUTE
        {
            public static String SoftwareDeveloper = "softwareDeveloper";
        }

        public static class APP_DEVICE_TYPE
        {
            public static int Android = 1;
            public static int Ipad = 2;
            public static int Iphone = 3;
        }

        public static class APP_SOURCE
        {
            public static int AppleStore = 1;
            public static int Enterprise = 2;
            public static int Device = 3;
            public static int AndroidApp = 4;
        }

        public static class ENTERPRISE_APP_TYPE
        {
            public static int NonEnterpriseApp = 0;
            public static int LinkProvidedEnterpriseApp = 1;
            public static int UploadedEnterpriseApp = 2;
        }

        public static class ENTERPRISE_APP_ERRORS
        {
            public static String InvalidUpload = "Add Enterprise App Failed. Reason: Invalid app file. The file selected is not a valid iOS or Android app.&lt;br&gt;"
                    + "Select a valid app file of type .apk (Android) or .ipa (iOS), and retry.";
            public static String InvalidFile = "Add Enterprise App Failed. Reason: App validation failed.<br/>"
                    + "Unable to retrieve app information from link provided. Verify that the link is configured correctly and that there are no firewall or security rules preventing Dell Management Portal from accessing app file, and retry.";
        }

        public static class APP_SEARCH_TYPE
        {
            public static int ByDeveloper = 1;
            public static int ByTerm = 2;
        }

        public static class iOSPayloadType
        {
            public static String ProfileService = "Profile Service";
            public static String Configuration = "Configuration";
            public static String SCEP = "com.apple.security.scep";
            public static String MDM = "com.apple.mdm";
            public static String Cetficate = "com.apple.security.pkcs12";
            public static String WebClip = "com.apple.webClip.managed";
        }

        public static class InstallProfileVariable
        {
            public static String exchangeUser = "exchangeUser";
            public static String exchangeEmail = "exchangeEmail";
            public static String imapUser = "imapUser";
            public static String imapEmail = "imapEmail";
            public static String popUser = "popUser";
            public static String popEmail = "popEmail";
        }

        public static class DISPLAY_TYPE
        {
            public static int Unknown = 0;
            public static int ReadWrite = 1;
            public static int ReadOnly = 2;
            public static int Hidden = 3;
            public static int NoDisplay = 4;
        }

        // stratus exception error code
        public static class RETCODE
        {
            public static int success = 0;
            public static int Unknown = -999;
            public static int InternalError = -1;
            public static int InvalidInput = -2;
            public static int AuthenticationError = -3;
            public static int ConfigureDeviceError = -4;
            public static int ObjectNotFound = -5;
            public static int CrossTenantValidationError = -6;
            public static int StratusEventPublishException = -7;
            public static int ConfigurationValidationException = -8;
            public static int TenantDisabledException = -9;
            public static int InvalidCertificate = -10;
            public static int InvalidCertificatePassword = -11;

            public static int LicenseExpired = -12;
            public static int DeviceCountLimitViolation = -13;
            public static int UserCountLimitViolation = -14;
            public static int InvalidParameter = -15;

            public static int GeneralSecurityException = -16;
            public static int SecurityConfigurationException = -17;
            public static int SecurityAuthenticationException = -18;
            public static int SecurityAuthorizationException = -19;
            public static int SecurityUnknowAuthenticatorException = -20;
            public static int InvalidPersonForDisable = -21;
            public static int SecurityAuthenticationTimeoutException = -22;
            public static int SecurityHostUnreachableException = -23;
            public static int SecurityHostUnknownException = -24;
            public static int DeviceNotRegisteredException = -25;

            public static int MQTTEventPublishException = -26;

            public static int KeystoneCountLimitViolation = -27;

            public static int OnPremiseUrlUpdateException = -28;

            public static int IllegalMdmSettingException = -29;

            public static int MdmConnectionException = -30;

            public static int MdmNoEntityFoundException = -31;

            public static int StratusServiceObjCreationException = -35;
        }

        public static class MANAGED_APP_POLICY
        {
            public static int DoNothing = 0;
            public static int Block = 1;
            public static int Recommended = 2;
            public static int Mandatory = 3;
        }

        public static class CERTIFICATE_FILENAME
        {
            public static String CaRoot = "appleRootCa.cer";
            public static String CaInter = "caroot.pem.cer";
            public static String MdmVendor = "mdmVendor.cer";
            public static String MdmSignFileName = "mdmVendor.key";
            public static String SystemCert = "system.p12";
        }

        public static class IMAGE_TYPE
        {
            public static int JPG = 0;
            public static int GIF = 1;
            public static int PNG = 2;
            public static String[] Suffix = { ".jpg", ".gif", ".png" };
        }

        public static class DN
        {
            public static String CN = "Wyse Technology";
            public static String OU = "Stratus";
            public static String O = "Stratus-MDM";
            public static String L = "San Jose";
            public static String S = "California";
            public static String C = "USA";
            public static String EmailAddress = "wysestratus@wyse.com";
        }

        public static class EMAIL_TEMPLATE_TYPE
        {

            // public static int Unknown = 0;
            public static int ThinClientReg = 1;
            public static int IosCCMAgentReg = 2;
            public static int IosSelfServiceReg = 3;
            public static int AndroidReg = 4;
            public static int ResetPassword = 5;
            public static int DeviceAssigned = 6;
            public static int SendResetPasswordLink = 7;
            public static int CloudConnectReg = 8;
            public static int WindowsPCReg = 9;
            public static int KeystoneReg = 10;
            public static int DeviceReRegRequired = 11;


            public static int VPPProgramInvitation = 10;
        }

        public static class PROFILE_GROUP
        {
            // public static int Unknown = 0;
            public static int RestrictPolicy = 1;
            public static int Wifi = 2;
            public static int Email = 3;
            public static int Vpn = 4;
            public static int Other = 5;
            public static int Mdm = 6;
            public static int Ldap = 7;
            public static int PocketCloud = 8;
            public static int iOSAirPlay = 9;
            public static int iOSAirPrint = 10;
            public static int iOSFont = 11;
            public static int iOSAdvanced = 12;
            public static int AndroidAdvanced = 13;
            public static int iOSCredentials = 14;
        }

        public static class EVENT_CATEGORY
        {

            // public static int Unknown = 100;
            public static int Device = 1;
            public static int User = 2;
            public static int Group = 3;
            public static int Configuration = 4;
            public static int NonCompliant = 5;
            public static int Tenant = 6;
        }

        public static class ALERT_CATEGORY
        {

            // public static int Unknown = 100;
            public static int DeviceJailBroken = 1;
            public static int DeviceApp = 2;
            public static int DeviceCheckin = 3;
            public static int DevicePassword = 4;
            public static int ConfigurationException = 5;
            public static int DeviceNotEncrypted = 6;
        }

        // stratus event severity
        public static class EVENT_SEVERITY
        {

            // public static int Unknown = 100;
            public static int Debug = 1;
            public static int Info = 2;
            public static int Warning = 3;
            public static int Major = 4;
            public static int Critical = 5;
        }

        // Device FAMILY
        public static class DEVICE_FAMILY
        {
            public static int Unknown = 0;
            public static int ThinClient = 6;
            public static int Android = 7;
            public static int Ios = 8;
            public static int PC = 9;
            public static int Keystone = 12;
            public static int OnPremise = 13;
        }

        // Device TYPE
        public static class DEVICE_TYPE
        {
            public static int General = 1;
            public static int iPhone = 2;
            public static int iPAD = 3;
            public static int AndroidPhone = 4;
            public static int AndroidPad = 5;
            public static int ThinClient = 6;
            public static int Android = 7;
            public static int IOS = 8;
            public static int iPod = 9;
            public static int PC = 10;
            public static int Ophelia = 11;
            public static int Keystone = 12;
            public static int OnPremContainer = 13;
        }

        // device type sort group
        public static class DEVICE_TYPE_SORTGROUP
        {
            public static int Unknown = 0;
            public static int ThinClient = 1;
            public static int Ios = 2;
            public static int Android = 3;
            public static int PC = 4;
            public static int Keystone = 5;
        }


        // device platform types
        public static class DEVICE_PLATFORM_TYPE
        {

            public static int UnknownPlatform = 0;
            public static int DellPC = 1;
            public static int LEO = 2;
            public static int Midway = 3;
            public static int UTC = 4;

            public static int Franklin = 6;
            public static int XL = 7;
            public static int Armstrong = 8;
            public static int UnknownCiscoPlatform = 9;

            public static int Unknown = 13;
            public static int Utc = 14;

            public static int Airspeak = 16;
            public static int Neoware4000Series = 17;
            public static int IBM2200Series = 18;
            public static int DellAximX5 = 19;
            public static int HPIPAQSeries = 20;
            public static int SymbolPDT8100Series = 21;
            public static int Gemini = 22;

            public static int PalmTungstenT = 24;
            public static int PalmTungstenW = 25;
            public static int PalmIIIc = 26;
            public static int PalmIIIx = 27;
            public static int PalmIIIe = 28;
            public static int PalmIIIxe = 29;
            public static int PalmVx = 30;
            public static int PalmZire = 31;
            public static int RX0LE = 32;
            public static int RX0L = 33;
            public static int PalmM125 = 34;
            public static int PalmM130 = 35;
            public static int PalmM500 = 36;
            public static int PalmM505 = 37;
            public static int CX0LE = 38;
            public static int Kyocera7135 = 39;
            public static int CX0L = 40;
            public static int GenericPC = 41;
            public static int SymbolSPT1800 = 42;
            public static int Intermec700Series = 43;
            public static int E200 = 44;
            public static int SymbolPDT8000Series = 45;
            public static int P20 = 46;
            public static int TX0 = 47;

            public static int CLI = 48;
            public static int X150 = 49;
            public static int ZX0S = 50;
            public static int ZX0D = 51;
            public static int XX0M = 52;

            public static int IPV600 = 53;
            public static int BestBuy = 54;
            public static int Safeway = 55;
            public static int Sx0 = 56;
            public static int C3425 = 57;
            public static int XX0J = 58;
            public static int IPV550 = 59;
            public static int ZX0DE = 60;

            public static int DX0D = 61;
            public static int vx0LThinClient = 62;
            public static int GX0 = 63;
            public static int N10 = 64;
            public static int XX0 = 65;
            public static int XX0MobileThinClient = 66;
            public static int XX0eMobileThinClient = 67;
            public static int XX0LMobileThinClient = 68;
            public static int XX0LeMobileThinClient = 69;
            public static int ET3000 = 70;
            public static int MT1500 = 71;
            public static int T10D = 72;
            public static int VianceProMobile = 73;
            public static int ViancePro = 74;
            public static int RX0L_1 = 75;
            public static int RX0LE_1 = 76;
            public static int LenovoR400 = 77;
            public static int CX0LE_1 = 78;
            public static int VX0LXThinClient = 79;
            public static int CX0L_1 = 80;
            public static int GenericPC_1 = 81;
            public static int WyseLinux = 82;
            public static int XX0CMobileThinClient = 83;
            public static int LenovoR400ThinThinkpad = 84;
            public static int LenovoX200sThinThinkpad = 85;
            public static int P20_1 = 86;
            public static int TX0_1 = 87;
            public static int XX0b = 88;
            public static int LenovoL412ThinThinkpad = 89;
            public static int ZX0S_1 = 90;
            public static int ZX0D_1 = 91;
            public static int XX0M_1 = 92;
            public static int VXC2112 = 93;
            public static int VXC2212 = 94;
            public static int VXC2111 = 95;
            public static int VXC2211 = 96;
            public static int VXC6215 = 97;
            public static int D10DP = 71;   // Must be 71 cause it replaced MT1500
            public static int MapleLeaf = 501;
            public static int _30X2 = 502;
        }

        // device Os types
        public static class DEVICE_OS_TYPE
        {

            public static int UnknownOS = 0;
            public static int XP = 1;
            public static int NT = 2;
            public static int Win98 = 3;
            public static int CE = 4;
            public static int CEN = 5;
            public static int Win2K = 6;
            public static int LX = 7;
            public static int SO = 8;
            public static int BL = 9;
            public static int TPC = 10;
            public static int PPC2K = 11;
            public static int PPC2K2 = 12;
            public static int PPCSP = 13;
            public static int NLX = 14;
            public static int RLX6 = 15;
            public static int RLX7 = 16;
            public static int RLX8 = 17;
            public static int TLX = 18;
            public static int PM3 = 19;
            public static int PM4 = 20;
            public static int PM5 = 21;
            public static int DOS = 22;
            public static int LXS = 23;
            public static int SLX = 24;
            public static int LVE = 25;
            public static int ALL = 26;
            public static int WES = 27;
            public static int CE6 = 28;
            public static int WES7 = 29;
            public static int XEN = 30;
            public static int UBN = 31;
            public static int TDC = 32;
            public static int PCOIP = 36;
        }

        public static class PROFILE_REMOVE_OPTION
        {
            public static int ALWAYS = 0;
            public static int WITH_AUTHENTICATION = 1;
            public static int NEVER = 2;
        }

        public static class ACCEPT_COOKIES_OPTION
        {
            public static int NEVER = 0;
            public static int FROM_VISITED_SITES = 1;
            public static int ALWAYS = 2;
        }

        public static class MOVIES_ALLOWED_RATINGS
        {
            public static int MOVIE_NOT_ALLOWED = 0;
            public static int G = 1;
            public static int PG = 2;
            public static int PG_13 = 3;
            public static int R = 4;
            public static int NC_17 = 5;
            public static int ALLOW_ALL = 6;
        }

        public static class TV_ALLOWED_RATINGS
        {
            public static int TV_NOT_ALLOWED = 0;
            public static int TV_Y = 1;
            public static int TV_Y7 = 2;
            public static int TV_G = 3;
            public static int TV_PG = 4;
            public static int TV_PG_14 = 5;
            public static int TV_MA = 6;
            public static int ALLOW_ALL = 7;
        }

        public static class APP_ALLOWED_RATINGS
        {
            public static int APP_NOT_ALLOWED = 0;
            public static int FOUR_PLUS = 1;
            public static int NINE_PLUS = 2;
            public static int TWELVE_PLUS = 3;
            public static int SEVENTEEN_PLUS = 4;
            public static int ALLOW_ALL = 5;
        }

        public static class DEVICE_LOCK_GRACE_PERIOD
        {
            public static int NONE = 0;
            public static int IMMEDIATELY = 1;
            public static int ONE_MINUTE = 2;
            public static int FIVE_MINUTES = 3;
            public static int FIFTEEN_MINUTES = 4;
            public static int ONE_HOUR = 5;
            public static int FOUR_HOURS = 6;
        }

        // wi-fi security types
        public static class WIFI_SECURITY_TYPE
        {
            public static int NONE = 0;
            public static int WEP = 1;
            public static int WPA_WPA2 = 2;
            public static int ANY = 3;
            public static int WEP_ENTERPRISE = 4;
            public static int WPA_WPA2_ENTERPRISE = 5;
            public static int ANY_ENTERPRISE = 6;
        }

        public static class WINDOWS_SECURITY_TYPE
        {
            public static int NetworkFirewall = 1;
            public static int WindowsUpdate = 2;
            public static int VirusProtection = 3;
            public static int Spyware = 4;
            public static int InternetSecurity = 5;
            public static int UserAccountControl = 6;
            public static int NetworkAccessProtection = 7;
        }

        public static class WINDOWS_FIREWALL_NETWORK_LOCATION_TYPE
        {
            public static int DomainNetworks = 1;
            public static int HomeOrWorkNetworks = 2;
            public static int PublicNetworks = 3;
        }

        public static class TTLSInnerIdentity
        {
            public static int PAP = 0;
            public static int CHAP = 1;
            public static int MSCHAP = 2;
            public static int MSCHAPv2 = 3;
        }

        public static class WifiProxyMode
        {
            public static int NONE = 0;
            public static int MANUAL = 1;
            public static int AUTO = 2;
        }

        // Commands
        public static class COMMAND
        {
            public static String WipeData = "WipeData";
            public static String Lock = "Lock";
            /**
             * command(message) to send first to device, then the real command is
             * retrieved from server to device.
             */
            public static String Alert = "Alert";
            public static String ApplyRedemptionCode = "ApplyRedemptionCode";
            public static String CertificateList = "CertificateList";
            public static String ClearPasscode = "ClearPasscode";
            public static String DeviceInformation = "DeviceInformation";
            public static String InstallMissingApps = "InstallMissingApps";
            public static String InstallApplication = "InstallApplication";
            public static String InstallApplicationList = "InstallApplicationList";
            public static String InstallProfile = "InstallProfile";
            public static String InstallWebclip = "InstallWebclip";
            public static String ManagedApplicationList = "ManagedApplicationList";
            public static String ProfileList = "ProfileList";
            public static String ProvisioningProfileList = "ProvisioningProfileList";
            public static String RemoveMissingApps = "RemoveMissingApps";
            public static String RemoveApplication = "RemoveApplication";
            public static String RemoveProfile = "RemoveProfile";
            public static String Restrictions = "Restrictions";
            public static String SecurityInfo = "SecurityInfo";
            public static String UnEnrollDevice = "UnEnrollDevice";
            public static String Restart = "Restart";
            public static String GroupChanged = "GroupChanged";
            public static String ApplyFilePolicy = "ApplyFilePolicy";
            public static String Shutdown = "Shutdown";
            public static String SendMessage = "SendMessage";
            public static String GetProcesses = "GetProcesses";
            public static String GetServices = "GetServices";
            public static String KillProcess = "KillProcess";

            //Ophelia
            public static String OpheliaCheckUpdate = "OpheliaCheckUpdate";
            public static String OpheliaForceCheckUpdate = "OpheliaForceCheckUpdate";
            public static String StartService = "StartService";
            public static String StopService = "StopService";
            public static String RestartService = "RestartService";
            public static String GetScreenCapture = "GetScreenCapture";
            public static String GetPerformanceMetrics = "GetPerformanceMetrics";
            public static String SetDeviceLocation = "SetDeviceLocation";
            public static String KeystoneAdminLock = "WorkspaceAdminLock";
            public static String KeystoneUserLock = "WorkspaceUserLock";
            public static String KeystoneUnlock = "WorkspaceUnLock";
            public static String ResetPIN = "WorkspaceResetPIN";
            public static String WipeWorkspaceData = "WipeWorkspaceData";

            // for OnPremise Commands
            public static String OnPremiseCommand = "OnPremiseCommand";
        }

        // device actions
        public static class DEVICE_ACTION
        {

            // public static int Unknown = 0;
            public static int WipeData = 1;
            public static int Lock = 2;
            public static int UnEnrollDevice = 3;
            public static int SecurityInfo = 4;
            public static int Alert = 5;
            public static int ApplyRedemptionCode = 6;
            public static int CertificateList = 7;
            public static int ClearPasscode = 8;
            public static int DeviceInformation = 9;
            public static int InstallMissingApps = 10;
            public static int InstallApplication = 11;
            public static int InstallApplicationList = 12;
            public static int InstallProfile = 13;
            public static int InstallWebclip = 14;
            public static int ManagedApplicationList = 15;
            public static int ProfileList = 16;
            public static int ProvisioningProfileList = 17;
            public static int RemoveMissingApps = 18;
            public static int RemoveApplication = 19;
            public static int RemoveProfile = 20;
            public static int Restrictions = 21;
            public static int Restart = 22;
            public static int Query = 23;

            // new command for Cloud client - when user's group is changed and
            // cloud client device need to re-register automatically with new group
            // token in the command
            public static int GroupChanged = 25;
            public static int ApplyFilePolicy = 26;

            //Ophelia
            public static int OpheliaCheckUpdate = 27;
            public static int OpheliaForceCheckUpdate = 28;

            public static int GetScreenCapture = 35;

            public static int Shutdown = 29;
            public static int SendMessage = 30;
            public static int GetProcesses = 31;
            public static int GetServices = 32;
            public static int KillProcess = 33;

            public static int StartService = 34;
            public static int StopService = 36;
            public static int RestartService = 37;
            public static int GetPerformanceMetrics = 38;
            public static int SetDeviceLocation = 39;
            // Keystone related actions
            public static int KeystoneAdminLock = 40;
            public static int KeystoneUserLock = 41;
            public static int KeystoneUnlock = 42;
            public static int ResetPIN = 43;
            public static int WipeWorkspaceData = 44;

            // OnPremise commands
            public static int OnPremiseCommand = 45;
        }

        // agent packages
        public static class AGENTPACKAGE_TYPE
        {

            // public static int Unknown = 0;
            public static int DefaultAgent = 1;
            public static int WyseHQEngineering_Android = 2;
        }

        // user group type
        public static class USERGROUP_TYPE
        {

            public static int DefaultUserGroup = 1;
            public static int MasterUserGroup = 2;
            public static int SelfServiceUserGroup = 3;
        }

        // user group family
        public static class USERGROUP_FAMILY
        {

            public static int LocalUserGroup = 1;
            public static int ActiveDirectoryUserGroup = 2;
        }

        // value type
        public static class PARAMETER_NAME
        {
            public static String Name = "Name";
            public static String AppName = "AppName";
            public static String AppId = "AppId";
            public static String BundleId = "BundleId";
            public static String Policy = "Policy";
            public static String ProfileIdentifier = "ProfileIdentifier";
            public static String Restricted = "Restricted";
            public static String Mandatory = "Mandatory";
            public static String NotManaged = "NotManaged";
            public static String ExistingPolicy = "ExistingPolicy";
            public static String DevicePlatform = "DevicePlatform";
            public static String DeviceOsType = "DeviceOsType";
            public static String DeviceFileId = "DeviceFileId";
            public static String DeviceFileType = "DeviceFileType";
            public static String DeviceFileAction = "DeviceFileAction";
        }

        // value type
        public static class VALUE_TYPE
        {

            // public static int Unknown = 0;
            public static int String = 1;
            public static int Integer = 2;
            public static int Boolean = 3;
            /**
             * static selection list: need options list from
             * ConfigurationParamCatalog (name value pairs)
             */
            public static int Collection = 4;
            public static int LONG = 5;
            /**
             * selection list: need options list (name value pairs) from server
             * call.
             */
            public static int REFERENCE = 6;
            /**
             * Icon is a Base64 encoded string representation of ICON file binary.
             */
            public static int ICON = 7;
            public static int TEXT = 8;

            /*
             *  This is a special type to handle certificates on SDK (Keystone or any other App)
             *  only user while sending payload to SDK. orginal type changed to this type while sending
             *  cert payload to SDK so SDK does not need to handle special pasring for cert data instead
             *  based on this type SDK can generalize the parsing.
             */
            public static int CERTS = 10;
        }

        // configuration property type
        public static class CONFIGURATION_PROPERTY
        {

            // public static int Unknown = 0;
            public static int PasswordExpirationDaynum = 1;
            public static int LastDailyTenantCountDaynum = 2;
            public static int LastDailyUserGroupCountDaynum = 3;
            public static int MaxCheckinIntervalHournum = 4;
            public static int CheckDeviceCheckinComplianceHournum = 5;
            public static int LastCheckDeviceCheckinComplianceTime = 6;
            public static int RefreshVppInfoIntervalHournum = 7;
            public static int LastRefreshVppInfoTime = 8;
        }

        // device status
        public static class DEVICE_STATUS
        {

            // public static int Unknown = 0;
            // inactive status
            public static int Offline = -2;
            public static int Discovered = -1;
            // active status
            public static int Registered = 1;
            public static int GroupAuthenticated = 2;
            public static int UserAuthenticated = 3;
            public static int PreRegistered = 4;
            public static int GroupChanged = 5;
        }

        // device profile security type
        public static class DEVICE_PROFILE_SECURITY
        {
            // public static int Unknown = 0;
            public static int Always = 1;
            public static int WithAuthentication = 2;
            public static int Never = 3;
        }

        // device command status
        public static class DEVICE_COMMAND_STATUS
        {

            public static int Created = 0;
            public static int Success = 1;
            public static int Failed = 2;
            public static int InProgress = 3;
            public static int Canceled = 4;
            // Non iOS device command status - support for scheduling of notification sending
            // new state change: Created --> NotifSent (after MQTT notification sent) --> InProgress (after device gets the device command) --> [Failed | Success | Canceled] 
            //public static int NotificationSent = 5;
        }

        // configuration parameter calalog
        public static class CONFIGURATION_PARAM_CATALOG
        {

            // General
            public static int ProfileName = 1;
            public static int ProfileIdentifier = 2;
            // public static int ProfileOrganization = 3;
            public static int ProfileDescription = 4;
            public static int ProfileRemovalAllowed = 5;
            public static int ProfileRemovalPassword = 6;
            // public static int GenerateRandomPassword = 7;
            // public static int UseDefaultGroupPassword = 8;
            // public static int ShowPassword = 9;
            // public static int PasswordNeverExpires = 10;
            // public static int PasswordExpireOn = 11;
            // public static int GroupPassword = 12;

            // Passcode for android
            public static int RequiredPasscode = 1000;
            public static int AllowSimpleValue = 1001;
            public static int RequireAlphaNumeric = 1002;
            public static int MinPasscodeLength = 1003;
            public static int MaxGracePeriodForDeviceLock = 1004;
            public static int NumberOfFailedAttemptsBeforeWipe = 1005;
            public static int MaxPasscodeAge = 1006;
            public static int RequireAlpha = 1007;
            public static int RequireAlphaOrNumeric = 1008;

            // Restrictions
            public static int AllowCamera = 2001;
            public static int AllowUsb = 2002;
            public static int AllowBluetooth = 2003;
            public static int AllowMobileApp = 2004;
            public static int AllowTethering = 2005;
            public static int AllowYoutube = 2006;
            public static int AllowDataRoaming = 2007;
            public static int UseDataNetwork = 2008;
            public static int AllowAndroidMarket = 2009;
            public static int AllowBrowser = 2010;
            public static int AllowFacebook = 2011;
            public static int iOSAllowOTAPKIUpdates = 2012;
            public static int iOSForceLimitAdTracking = 2013;
            public static int iOSAllowLockScreenWiFiModification = 2014;
            public static int iOSAllowCloudKeychainSync = 2015;

            // Other
            public static int DeviceHeartbeatFrequency = 3001;
            public static int InactivityTimeBeforeDeemedOutOFCompliance = 3002;
            public static int WarningIfDisableDeviceManagement = 3003;
            public static int EnforcementActionAfterDisableDeviceManagement = 3004;

            // MDM Settings ---?
            public static int SecuritySettings = 4001;
            public static int RestrictionSettings = 4002;
            public static int ConfigurationProfiles = 4003;
            public static int Applications = 4004;
            public static int AddNRemoveConfigurationProfiles = 4005;
            public static int ChangeDevicePassword = 4006;
            public static int RemoteWipe = 4007;

            // Advanced for Android
            public static int AndroidAdvGeoLocation = 10300;
            public static int AndroidAdvGoeLocationNotifUser = 10301;
            public static int AndroidAdvGoeLocationNotifUserDay = 10302;
            public static int AndroidAdvRestrictJailbrokenDevices = 10303;

            // Passcode for iOS
            public static int iOSAllowSimpleValue = 5001;
            public static int iOSRequireAlphaNumeric = 5002;
            public static int iOSMinPasscodeLength = 5003;
            public static int iOSMinComplexCharacters = 5004;
            public static int iOSMaxPasscodeAge = 5005;
            public static int iOSAutoLock = 5006;
            public static int iOSPasscodeHistory = 5007;
            public static int iOSGracePeriodForDeviceLock = 5008;
            public static int iOSMaxFailedAttempts = 5009;

            // restrictions for iOS
            public static int iOSResAllowInstallApps = 5051;
            // restriction - camera group
            public static int iOSResAllowUseCamera = 5052;
            public static int iOSResAllowFaceTime = 5053;

            public static int iOSResAllowScreenCapture = 5054;
            public static int iOSResAutoSyncWhileRoaming = 5055;
            public static int iOSResAllowSiri = 5056;
            public static int iOSResAllowVoiceDialing = 5057;
            public static int iOSResAllowInAppPurchase = 5058;
            public static int iOSResForceiTunePasswordForPurchase = 5059;
            public static int iOSResAllowMultiPlayerGame = 5060;
            public static int iOSResAllowAddingGameCenterFriend = 5061;
            public static int iOSResAllowAssistantWhileLocked = 5062;
            public static int iOSResAllowPassbookWhileLocked = 5063;
            public static int iOSResAllowControlCenterWhileLocked = 5064;
            public static int iOSResAllowNotificationWhileLocked = 5065;
            public static int iOSResAllowViewWhileLocked = 5066;
            public static int iOSResAllowFingerprintUnLocked = 5067;

            // restriction - application
            public static int iOSResAllowUTube = 5070;
            public static int iOSResAllowiTuneStore = 5071;

            // restriction - safari
            public static int iOSResAllowSafari = 5072;
            public static int iOSResEnableAutoFill = 5073;
            public static int iOSResForceFraudWarning = 5074;
            public static int iOSResEnableJavaScript = 5075;
            public static int iOSResAllowPopups = 5076;
            public static int iOSResAcceptCookies = 5077;

            // restriction - iCloud
            public static int iOSResAllowBackup = 5080;
            public static int iOSResAllowDocumentSync = 5081;
            public static int iOSResAllowPhotoStream = 5082;
            public static int iOSResAllowSharedStream = 5088;

            // restriction - security and privacy
            public static int iOSResAllowDiagnoseDataToApple = 5083;
            public static int iOSResUntrustedTLSCertificates = 5084;
            public static int iOSResForceEncryptedBackups = 5085;

            // restriction - content ratings
            public static int iOSResAllowExplicitMusicPodCast = 5086;
            public static int iOSResRatingsRegion = 5087;

            // restriction - allow content ratings
            public static int iOSResMoviesAllowedRating = 5090;
            public static int iOSResTVShowsAllowedRating = 5091;
            public static int iOSResAppsAllowedRatings = 5092;

            //Data security
            public static int iOSResAllowOpenFromManagedToUnmanaged = 5093;
            public static int iOSResAllowOpenFromUnmanagedToManaged = 5094;

            // WiFi
            public static int SSID = 5101;
            public static int AutoConnect = 5102;
            public static int HiddenNetwork = 5103;

            //WiFi Hotspot 2.0
            public static int IsHotspot = 5104;
            public static int DomainName = 5105;
            public static int HESSID = 5106;
            public static int ServiceProviderRoamingEnabled = 5107;
            public static int RoamingConsortiumOIs = 5108;
            public static int NAIRealmNames = 5109;
            public static int MCCAndMNCs = 5110;
            public static int DisplayedOperator = 5111;

            // WiFI Proxy
            public static int WiFiProxyMode = 5201;
            public static int WiFiProxyUrl = 5202;
            public static int WiFiProxyServer = 5203;
            public static int WiFiProxyPort = 5204;
            public static int WiFiProxyAuthentication = 5205;
            public static int WiFiProxyPassword = 5206;

            // WiFI security
            public static int WiFiSecurityType = 5301;
            public static int WiFiPassword = 5302;
            public static int WiFiEAPType = 5303;
            public static int WiFiEAPFASTConfig = 5304;
            public static int WiFiAuthUserName = 5305;
            public static int WiFiAuthUsePerConnectionPassword = 5306;
            public static int WiFiAuthPassword = 5307;
            public static int WiFiAuthIdCertificate = 5308;
            public static int WiFiAuthOuterIdentity = 5309;
            public static int WiFiTrustTrusetedServerCertificateNames = 5311;
            public static int WiFiTrustAllowExceptions = 5312;
            public static int WiFiTTLSInnerId = 5313;
            public static int WiFiAcceptedEAPTLS = 5314;
            public static int WiFiAcceptedEAPLEAP = 5315;
            public static int WiFiAcceptedEAPFast = 5316;
            public static int WiFiAcceptedEAPTTLS = 5317;
            public static int WiFiAcceptedEAPPEAP = 5318;
            public static int WiFiAcceptedEAPSIM = 5319;
            public static int WiFiAcceptedEAPFastUsePAC = 5320;
            public static int WiFiAcceptedEAPFastProvisionPac = 5321;
            public static int WiFiAcceptedEAPFastProvisionPacAnonymously = 5322;
            public static int WiFiTrustTrustedCerificateEnabled = 5323;
            public static int WiFiTrustTrustedCerificateName = 5324;
            public static int WiFiTrustTLSCertificateIs = 5325;

            // iOS VPN
            public static int VPNConnectionName = 5401;
            public static int VPNConnectionType = 5402;
            public static int VPNServer = 5403;
            public static int VPNAccount = 5404;
            public static int VPNL2TPPPTPUserAuthentication = 5405;
            public static int VPNL2TPPPTPUserAuthenticationPassword = 5406;
            public static int VPNSharedSecret = 5407;
            public static int VPNL2TPSendAllTraffic = 5408;
            public static int VPNPPTPEncryptinonLevel = 5409;
            public static int VPNIPSECPassword = 5410;
            public static int VPNMachineAuthType = 5411;
            public static int MachineAuthGroupSCGroupName = 5412;
            public static int MachineAuthGroupSCSharedSecret = 5413;
            public static int MachineAuthGroupSCUseHybridAuthentication = 5414;
            public static int MachineAuthGroupSCPromptForPassword = 5415;
            public static int MachineAuthCertificateIdentityCertificate = 5416;
            public static int MachineAuthCertificatencludePin = 5417;
            public static int CiscoAnyConnectGroup = 5418;
            public static int JuniperSSLREALM = 5419;
            public static int JuniperSSLRole = 5420;
            public static int SonicWALLLoginGroupDomain = 5421;
            public static int VPNUserAuthType = 5422;
            public static int VPNUserAuthPassword = 5423;
            public static int VPNUserAuthCert = 5424;

            public static int VPNProxy = 5425;
            public static int VPNProxyServer = 5426;
            public static int VPNProxyPort = 5427;
            public static int VPNProxyAuthentication = 5428;
            public static int VPNProxyPassword = 5429;
            public static int VPNProxyUrl = 5430;

            // Email
            public static int EmailAccountDescription = 5501;
            public static int EmailAcctType = 5502;
            public static int EmailPathPrefix = 5503;
            public static int UserDisplayName = 5504;
            public static int EmailAddress = 5505;
            public static int EmailAllowMove = 5506;
            public static int EmailAllowMobileUserInfoFromDb = 5507;
            public static int EmailDisableMailRecentsSyncing = 5508;

            // Incomming
            public static int IncomingMailServer = 5601;
            public static int IncomingMailServerPort = 5602;
            public static int IncomingAuthType = 5603;
            public static int IncomingMailPassword = 5604;
            public static int UseSSLForIncomingEmail = 5605;
            public static int IncomingMailUserName = 5606;

            // Outgoing
            public static int OutgoingMailServer = 5701;
            public static int OutgoingMailServerPort = 5702;
            public static int OutgoingMailUserName = 5703;
            public static int OutgoingMailPassword = 5704;
            public static int OutgoingPasswordSameAsIncoming = 5705;
            public static int OutogingMailUseOnlyInMail = 5706;
            public static int OutgoingMailUseSSL = 5707;
            public static int OutgoingMailUseSMIME = 5708;
            public static int OutgoingAuthType = 5709;
            public static int OutgoingMailSigningCert = 5710;
            public static int OutgoingMailEncryptionCert = 5711;

            // Exchange activeSync
            public static int ExchangeAccountName = 5801;
            public static int ExchangeAccountHost = 5802;
            public static int ExchangeAllowMove = 5803;
            public static int ExchangeUseOnlyInMail = 5804;
            public static int ExchangeUseSSL = 5805;
            public static int ExchangeUseSMIME = 5806;
            public static int ExchangeDomain = 5807;
            public static int ExchangeSyncAllowMobileUserInfoFromDb = 5819;
            public static int ExchangeUser = 5808;
            public static int ExchangeEmailAddr = 5809;
            public static int ExchangePassword = 5810;
            public static int ExchnagePastDaysOfMailToSync = 5811;
            public static int ExchangeSMIMEEncryptionCertificateUuid = 5812;
            public static int ExchangeSMIMESigningCertificateUuid = 5813;
            public static int MakeCertCompatibleWithIOS4 = 5814;
            public static int ExchangeIdentityCertificate = 5815;
            // public static int ExchangeSyncCertificate = 5816;
            // public static int ExchangeSyncCertificateName = 5817;
            // public static int ExchangeSyncCertificatePassword = 5818;
            public static int ExchangeDisableMailRecentsSyncing = 5820;
            public static int HeaderMagic = 5821;

            // LDAP
            public static int LDAPAcctDescription = 5901;
            public static int LDAPAcctUserName = 5902;
            public static int LDAPAcctPassword = 5903;
            public static int LDAPAcctHost = 5904;
            public static int LDAPUseSSL = 5905;
            public static int LDAPSeachSettings = 5906;

            // CalDav and more
            public static int CalDavAccountName = 6001;
            public static int CalDavAccountHost = 6002;
            public static int CalDavAccountPort = 6003;
            public static int CalDavAccountUrl = 6004;
            public static int CalDavAcccountUserName = 6005;
            public static int CalDavAccountPassword = 6006;
            public static int CalDavUseSSL = 6007;

            // Subscribed Calendar
            public static int SubscribedCalDescription = 6101;
            public static int SubscribedCalUrl = 6102;
            public static int SubscribedCalUserName = 6103;
            public static int SubscribedCalPassword = 6104;
            public static int SubscribedCalUseSSL = 6105;
            // CardDav
            public static int CardDavAcctDescription = 6201;
            public static int CardDavAcctUserName = 6202;
            public static int CardDavAcctPassword = 6203;
            public static int CardDavAcctHost = 6204;
            public static int CardDavAcctPort = 6205;
            public static int CardDavUseSSL = 6206;
            public static int CardDavPrincipalUrl = 6207;
            // Web clips
            public static int WebClipLable = 6301;
            public static int WebClipUrl = 6302;
            public static int WebClipRemovable = 6303;
            public static int WebClipIcon = 6304;
            public static int WebClipPreposedIcon = 6305;
            public static int WebClipFullScreen = 6306;
            public static int WebClipIconFileName = 6307;
            // Credential
            public static int CredentialName = 6401;
            public static int CredentialData = 6402;
            public static int CredentialType = 6403;
            public static int CredentialPassword = 6404;
            public static int CredentialUuid = 6405;
            public static int CredentialFileName = 6406;
            public static int CredentialPrivateKey = 6407;
            public static int CredentialPublicKey = 6408;
            // SCEP
            public static int SCEPUrl = 6501;
            public static int SCEPName = 6502;
            public static int SCEPSubject = 6503;
            public static int SCEPSubjectAlternativeNameType = 6504;
            public static int SCEPSubjectAlternativeNameValue = 6505;
            public static int SCEPNTPrincipalName = 6506;
            public static int SCEPChallenge = 6507;
            public static int SCEPKeySize = 6508;
            public static int SCEPUseasDigitalSignature = 6509;
            public static int SCEPUseForKeyEncipherment = 6510;
            public static int SCEPFingerprint = 6511;
            public static int SCEPRetries = 6512;
            public static int SCEPRetryDelay = 6513;

            // MDM for iOS
            public static int MDM_SERVER_URL = 6601;
            public static int MDM_CHECK_IN_URL = 6602;
            public static int MDM_TOPIC = 6603;
            public static int MDM_CREDENTIAL = 6604;
            public static int MDM_SIGN_MESSAGE = 6605;
            public static int MDM_CHECK_OUT_WHEN_REMOVED = 6606;
            public static int MDM_GRANT_ACCESS_TO_REMOTE_ADMIN = 6607;

            // MDM / QUERY DEVICE
            public static int MDM_QUERY_DEVICE_FOR_GENERAL_SETTINGS = 6611;
            public static int MDM_QUERY_DEVICE_FOR_NETWORK_SETTINGS = 6612;
            public static int MDM_QUERY_DEVICE_FOR_SECURITY_SETTINGS = 6613;
            public static int MDM_QUERY_DEVICE_FOR_RESTRICTION_SETTINGS = 6614;
            public static int MDM_QUERY_DEVICE_FOR_CONFIGURATION_PROFILES = 6615;
            public static int MDM_QUERY_DEVICE_FOR_PROVISIONING_PROFILES = 6616;
            public static int MDM_QUERY_DEVICE_FOR_APPLICATIONS = 6617;

            // MDM / add remove
            public static int MDM_ADD_REMOVE_CONFIGURATION_PROFILES = 6621;
            public static int MDM_ADD_REMOVE_PROVISIONING_PROFILES = 6622;

            // MDM / security
            public static int MDM_SECURITY_CHANGE_DEVICE_PASSWORD = 6631;
            public static int MDM_SECURITY_REMOTE_WIPE = 6632;

            // MDM / APNS
            public static int MDM_USE_DEV_APNS_SERVER = 6641;

            // Advanced
            public static int AccessPointName = 6701;
            public static int AccessPointUserName = 6702;
            public static int AccessPointPassword = 6703;
            public static int AdvancedProxyServer = 6704;
            public static int AdvancedProxyServerPort = 6705;

            // RDP for Thin OS
            // Central Configuration for Thin OS

            // Thin OS RC connection broker settings
            public static int ThinOSRemoteSelectBroker = 6800;
            public static int ThinOSConfigureTSGateway = 9004;
            public static int ThinOSSecurityMode = 9005;
            public static int ThinOSProtocol = 9006;
            public static int ThinOSAutomaticallyConnectToSesions = 9000;
            public static int ThinOSUseRecommendedSettings = 9002;
            public static int ThinOSManuallyDefineDirectRDP = 9003;

            // Thin OS RC TS Gateway Settings
            public static int ThinOSTSGatwayHostAddr = 9020;
            public static int ThinOSTSUsername = 9021;
            public static int ThinOSTSPassword = 9022;
            public static int ThinOSTSDomain = 9023;
            public static int ThinOSAutoDetectNetwork = 9024;

            // Thin OS RC Session Behavior
            public static int ThinOSSessionWindowBehavior = 9030;
            public static int ThinOSSeamlessModeSupport = 9031;
            public static int ThinOSHideTaskbarInSeamlessMode = 9032;
            public static int ThinOSCitrixMultimediaRedirection = 9033;
            public static int ThinOSCitrixFontSmoothing = 9034;
            public static int ThinOSOffscreenSupport = 9035;
            public static int ThinOSImproveKB = 9036;
            public static int ThinOSImproveMouseover = 9037;
            public static int ThinOSOtherMultimediaRedirection = 9038;
            public static int ThinOSOtherFontSmoothing = 9039;
            public static int ThinOSRemoteFX = 9041;
            public static int ThinOSVideoOptimizedVOR = 9042;
            public static int ThinOSBitmapCodecRemoteFX = 9043;
            public static int ThinOSUDPTrafficChannel = 9044;
            public static int ThinOSMultiMonitor = 9045;
            public static int ThinOSAutoConnect = 9047;
            // public static int ThinOSShowRDPProtocolFeaturesForCitrix = 9048;
            public static int ThinOSShowRDPProtocolFeaturesForOther = 9049;

            public static int ThinOSFileServer = 6801;
            // public static int ThinOSPath = 6802;
            public static int ThinOSFtpUser = 6803;
            public static int ThinOSFtpPassword = 6804;
            public static int ThinOSRemoteBrokerServer = 6805;
            public static int ThinOSFirmwareUpdateLogic = 6806;
            public static int ThinOSFirmwareUpdateLocalPrompt = 6807;


            // Thin OS system preference -- No need for now
            // public static int ThinOSTerminalName = 6811;
            // public static int ThinOSTimeZone = 6812;
            // public static int ThinOSTimeFormat=6813;
            // public static int ThinOSTimeServer=6814;

            // Display settings
            public static int ThinOSMonitorResolution = 6821;
            public static int ThinOSMonitorRotation = 6822;
            public static int ThinOSDesktopColorDepth = 6823;
            public static int ThinOSWallpaperColor = 6824;
            public static int ThinOSEnableDualMonitor = 6825;
            public static int ThinOSDualDisplayMode = 6826;
            public static int ThinOSDualMainScreen = 6827;
            public static int ThinOSDualAlignment = 6828;
            public static int ThinOSEnableDesktopWallpaper = 7030;
            public static int ThinOSWallpaperFile = 7031;
            public static int ThinOSWallpaperLayout = 7032;
            public static int ThinOSEnableCompanyLogo = 7033;
            public static int ThinOSLogoFile = 7034;
            public static int ThinOSEnableEula = 7035;
            public static int ThinOSEulaFile = 7036;



            // Thin OS Security
            public static int ThinOSPrivilegeLevelObsolete = 6840;
            public static int ThinOSPrivilegeLevel = 6841;
            public static int ThinOSAdminMode = 6842;
            public static int ThinOSAdminName = 6843;
            public static int ThinOSAdminPassword = 6844;
            public static int ThinOSEnbaleGkeyReset = 6908;
            public static int ThinOSEnableTrace = 6909;
            public static int ThinOSEnableVNC = 6910;
            public static int ThinOSVncPassword = 6911;
            public static int ThinOSVncPromptUserOnStart = 6912;
            public static int ThinOSVncCounterSeconds = 6913;
            public static int ThinOSVncPromptUserOnEnd = 6914;
            public static int ThinOSVncLockResource = 6915;
            public static int ThinOSVncForce8Bit = 6916;
            public static int ThinOSVncAlloZlibCompression = 6917;  // <- Not used anymore but MUST NOT reuse because older TC version is using it
            public static int ThinOSCertificates = 6918;
            public static int ThinOSAutoInstallCert = 6942;

            public static int ThinOSAllowLockdown = 6919;
            public static int ThinOSRequireDomainLoginOptions = 6920;
            public static int ThinOSDisableGuest = 6921;

            public static int ThinOSReenterPassword = 6922;
            public static int ThinOSExpirationTime = 6923;

            public static int ThinOSRequiredSmartCard = 6924;
            public static int ThinOSRequireSmartCardValuesOptions = 6925;

            public static int ThinOSEncryptFlash = 6926;
            public static int ThinOSDisableWDM = 6927;
            public static int ThinOSDisableThinPrint = 6928;
            public static int ThinOSDisableVNCShadowing = 6929;
            public static int ThinOSFastDisconnect = 6930;


            // Thin OS Other
            public static int ThinOSOtherName = 6845;
            public static int ThinOSOtherPassword = 6846;
            public static int ThinOSOtherDomain = 6847;
            public static int ThinOSOtherTimeServers = 6848;
            public static int ThinOSOtherTimeZone = 6849;
            public static int ThinOSOtherTCXLicense = 6850; // <- Not used in R101 but MUST NOT reuse because older TC version is using it
            public static int ThinOSOtherRememberUsername = 6836;
            public static int ThinOSOtherTimeZoneOption = 6837;
            public static int ThinOSOtherTimeZoneManualSet = 6838;
            public static int ThinOSOtherTimeZoneEnableDST = 6839;
            public static int ThinOSOtherTimeZoneDSTRegion = 6340;
            public static int ThinOSOtherShowLastUserOnLoginScreen = 6855; //6855 is used by old TC agent!

            public static int ThinOSOtherDSTStartMonth = 7021;
            public static int ThinOSOtherDSTStartWeek = 7022;
            public static int ThinOSOtherDSTStartDay = 7023;
            public static int ThinOSOtherDSTEndMonth = 7024;
            public static int ThinOSOtherDSTEndWeek = 7025;
            public static int ThinOSOtherDSTEndDay = 7026;
            public static int ThinOSOtherDSTTzName = 7027;
            public static int ThinOSOtherDaylightName = 7028;


            // Thin OS VDI
            public static int ThinOSVDIBroker = 6851;
            public static int ThinOSAutoConnectList = 6852;
            public static int ThinOSVirtualCenter = 6853;
            public static int ThinOSForceVDISmartCardLogin = 6854;
            // Thin OS Citrix
            public static int ThinOSICABrowsing = 6861;
            public static int ThinOSPNLiteServer = 6862;
            public static int ThinOSDomainList = 6863;
            public static int ThinOSUSBRedirectionType = 6864;
            // Thin OS Startup
            public static int ThinOSAutoPower = 6871;
            // Thin OS shutdown
            public static int ThinOSAutoSignOff = 6881;
            public static int ThinOSShutdown = 6882;
            public static int ThinOSShutdownCount = 6883;
            public static int ThinOSReboot = 6884;
            public static int ThinOSForceFullShutdown = 6885;
            // Thin OS RDP
            public static int ThinOSRDPDefaultColorDepth = 6889;
            public static int ThinOSRDPAudioPlayback = 6890;
            public static int ThinOSRDPConnectionName = 6891;
            public static int ThinOSRDPHostName = 6892;
            public static int ThinOSRDPAutoStart = 6893;
            public static int ThinOSRDPReconnect = 6894;
            public static int ThinOSRDPUserName = 6895;
            public static int ThinOSRDPPassworde = 6896;
            public static int ThinOSRDPDomainName = 6897;
            public static int ThinOSRDPColorDepth = 6898;
            public static int ThinOSRDPFullScreen = 6899;

            public static int ThinOSRDPEnableNLA = 6829;
            public static int ThinOSRDPEnableRecoding = 6830;
            public static int ThinOSRDPForceSpan = 6831;
            public static int ThinOSRDPEnableDesktopWallPaper = 6832;
            public static int ThinOSRDPWindowDragging = 6833;
            public static int ThinOSRDPAnimation = 6834;
            public static int ThinOSRDPTheme = 6835;

            public static int ThinOSRDPUsbRedirection = 11001;
            public static int ThinOSRDPEnableSession = 11002;
            public static int ThinOSRDPEnableProgressive = 11003;
            public static int ThinOSRDPDisplayOnDesktop = 11004;
            public static int ThinOSRDPBranchRepeater = 11005;
            public static int ThinOSRDPIcaPing = 11006;
            public static int ThinOSRDPAudioQuality = 11007;
            public static int ThinOSRDPMapUSBDisk = 11008;
            public static int CitrixStoreFrontAuthentication = 11009;

            // Thin OS RC Peripheral Behavior
            public static int ThinOSPBPrinters = 9050;
            public static int ThinOSPBSerials = 9051;
            public static int ThinOSPBSmartCards = 9052;
            public static int ThinOSPBSound = 9053;
            public static int ThinOSPBDisk = 9054;
            public static int ThinOSPBEnableUSBRedirection = 9055;
            public static int ThinOSPBExcludeDiskDevices = 9056;
            public static int ThinOSPBExcludeAudioDevices = 9057;
            public static int ThinOSPBExcludePrinterDevices = 9058;
            public static int ThinOSPBExcludeVideoDevices = 9059;
            public static int ThinOSPBMouseQueTimer = 9060;

            // Thin OS RC Addisional Settings
            public static int ThinOSASAutomaticallyReconnectAtLogon = 9070;
            public static int ThinOSASAutomaticallyReconnectFromBtnMenu = 9071;
            public static int ThinOSASAccountSelfServiceServer = 9072;
            public static int ThinOSASAccessGatewayAuthMethod = 9073;
            public static int ThinOSASUseHTTPForBrowsing = 9074;
            public static int ThinOSASAlternateAddressViaFirewall = 9075;
            public static int ThinOSASSystemMenu = 9076;
            public static int ThinOSASDisableResetVM = 9077;
            public static int ThinOSASShow32BitIconsForFirstConn = 9078;
            public static int ThinOSASMaximumBitmapCache = 9079;
            public static int ThinOSASSessionWidth = 9080;

            // Thin OS Visual Experience
            public static int ThinOSClassicVsZero = 6900;
            public static int ThinOSActionAfterSessionExit = 6901;
            public static int ThinOSShutdownRestartCounter = 6902;
            public static int ThinOSEnableZeroToolbar = 6903;
            public static int ThinOSEnableZeroToolbarAfterSecond = 6904;
            public static int ThinOSDisableHotkey = 6905;
            public static int ThinOSAlwaysDisableToolbar = 6906;
            public static int ThinOSDisableHomeIcon = 6907;
            public static int ThinOSPreventToolbarFromClosing = 6940;
            public static int ThinOSAutoHideToolbar = 6941;

            // THIN OS Custom lines - Advanced settings
            //public static int ThinOSNoGlobalIniNew = 6999; // to replace 7020
            public static int ThinOSCustomLine1 = 7000;
            public static int ThinOSCustomLine2 = 7001;
            public static int ThinOSCustomLine3 = 7002;
            public static int ThinOSCustomLine4 = 7003;
            public static int ThinOSCustomLine5 = 7004;
            public static int ThinOSCustomLine6 = 7005;
            public static int ThinOSCustomLine7 = 7006;
            public static int ThinOSCustomLine8 = 7007;
            public static int ThinOSCustomLine9 = 7008;
            public static int ThinOSCustomLine10 = 7009;

            public static int ThinOSCustomLine11 = 7010;
            public static int ThinOSCustomLine12 = 7011;
            public static int ThinOSCustomLine13 = 7012;
            public static int ThinOSCustomLine14 = 7013;
            public static int ThinOSCustomLine15 = 7014;
            public static int ThinOSCustomLine16 = 7015;
            public static int ThinOSCustomLine17 = 7016;
            public static int ThinOSCustomLine18 = 7017;
            public static int ThinOSCustomLine19 = 7018;
            public static int ThinOSCustomLine20 = 7019;
            /**/
            public static int ThinOSNoGlobalIni = 7020;
            /**/

            // android wifi
            public static int AndroidWifiSSID = 8001;
            public static int AndroidWifiHiddenNetwork = 8002;
            public static int AndroidWifiSecurityType = 8003;
            public static int AndroidWifiPassword = 8004;
            public static int AndroidWifiAcceptedEapType = 8005;
            public static int AndroidWifiInnerIdentity = 8006;
            public static int AndroidWifiAuthUserName = 8007;
            public static int AndroidWifiAuthPassword = 8008;
            public static int AndroidWifiAuthOuterIdentity = 8009;
            public static int AndroidWifiAuthIdentityCert = 8010;
            public static int AndroidWifiAuthCACert = 8011;

            // Android VPN
            public static int AndroidVPNConnectionName = 8500;
            public static int AndroidVPNConnectionType = 8501;
            public static int AndroidVPNServer = 8502;
            public static int AndroidVPNUserName = 8503;
            public static int AndroidVPNPassword = 8504;
            public static int AndroidVPNRealm = 8505;
            public static int AndroidVPNDomain = 8506;
            public static int AndroidVPNUseCert = 8507;
            public static int AndroidVPNCertCommonName = 8508;

            // android Exchange ActiveSync -- Commented out by Ngoc cause not used
            // public static int AndroidExchAccountName = 9001;
            // public static int AndroidExchHost = 9002;
            // public static int AndroidExchDomain = 9003;
            // public static int AndroidExchAllowMobileUserInfoFromDb = 9004;
            // public static int AndroidExchUserName = 9005;
            // public static int AndroidExchEmailAddr = 9006;
            // public static int AndroidExchPassword = 9007;
            // public static int AndroidExchPastDaysOfMailToSync = 9008;
            // public static int AndroidExchPastDaysOfCalendarToSync = 9009;
            // public static int AndroidExchMaxBodyTruncation = 9010;
            // public static int AndroidExchClientCertificate = 9011;
            // public static int AndroidExchEmailAttachmentEnabled = 9102;
            // public static int AndroidExchEmailDisableCopyPaste = 9103;
            // public static int AndroidExchEmailAllowHTML = 9104;
            // public static int AndroidExchEmailManualSyncWhenRoaming = 9105;
            // public static int AndroidExchEmailDisableCopyToPhoneBook = 9106;
            // public static int AndroidExchEmailDisableDatabaseBackupSDCard = 9107;
            // public static int AndroidExchEmailDisableSettingBackupSDCard = 9108;
            // public static int AndroidExchEmailDisablePolicyReconfiguration = 9109;
            // public static int AndroidExchSecurityRequireDeviceEncryption = 9201;
            // public static int AndroidExchSecurityAllowSDCard = 9202;
            // public static int AndroidExchSecurityRequireSDCardEncryption = 9203;
            // public static int AndroidExchTouchDownLicKey = 9301;

            // Pocket cloud
            public static int PocketCloudConnectionName = 10001;
            public static int PocketCloudHostAddr = 10002;
            public static int PocketCloudUserName = 10003;
            public static int PocketCloudPassword = 10004;
            public static int PocketCloudDomain = 10005;
            public static int PocketCloudResolution = 10006;
            public static int PocketCloudCustomResolution = 10007;
            public static int PocketCloudSessionColorDepth = 10008;
            public static int PocketCloudKeyboardLayout = 10009;
            public static int PocketCloudPort = 10010;
            //public static int PocketCloudRDGateway = 10011;
            //public static int PocketCloudNLASwitch = 10012;
            //public static int PocketCloudConsoleMode = 10013;
            public static int PocketCloudAudio = 10014;
            public static int PocketCloudDisableFullWindowDrag = 10015;
            //public static int PocketCloudDisableScreenshots = 10016;
            public static int PocketCloudSaveOnAgent = 10017;
            public static int PocketCloudDesktopWidth = 10018;
            public static int PocketCloudDesktopHeight = 10019;
            public static int PocketCloudAlternateShell = 10020;
            public static int PocketCloudShellWorkingDirectory = 10021;
            public static int PocketCloudGatewayHostname = 10022;
            public static int PocketCloudRDPCredentials = 10023;
            public static int PocketCloudGatewayUsername = 10024;
            public static int PocketCloudGatewayPassword = 10025;
            public static int PocketCloudGatewayDomain = 10026;

            // Ios Pocket cloud
            public static int IosPocketCloudConnectionName = 10100;
            public static int IosPocketCloudGatewayDomain = 10101;
            public static int IosPocketCloudHostAddr = 10102;
            public static int IosPocketCloudUserName = 10103;
            public static int IosPocketCloudPassword = 10104;
            public static int IosPocketCloudDomain = 10105;
            public static int IosPocketCloudResolution = 10106;
            public static int IosPocketCloudCustomResolution = 10107;
            public static int IosPocketCloudSessionColorDepth = 10108;
            public static int IosPocketCloudKeyboardLayout = 10109;
            public static int IosPocketCloudPort = 10110;
            public static int IosPocketCloudGatewayUsername = 10111;
            public static int IosPocketCloudGatewayPassword = 10112;
            public static int IosPocketCloudGatewayHostname = 10113;
            public static int IosPocketCloudAudio = 10114;
            //public static int IosPocketCloudDisableFullWindowDrag = 10115;
            public static int IosPocketCloudDisableScreenshots = 10116;
            public static int IosPocketCloudSaveOnAgent = 10117;
            public static int IosPocketCloudDesktopWidth = 10118;
            public static int IosPocketCloudDesktopHeight = 10119;
            //public static int IosPocketCloudDisableWallPaper = 10120;
            public static int IosPocketCloudAlternateShell = 10121;
            public static int IosPocketCloudShellWorkingDirectory = 10122;
            public static int IosPocketCloudRDPCredentials = 10123;

            // Ios Advance
            public static int IosAdvGeoLocation = 10200;
            public static int IosAdvGoeLocationNotifUser = 10201;
            public static int IosAdvGoeLocationNotifUserDay = 10202;
            public static int IosAdvRestrictJailbrokenDevices = 10203;

            // TC Image & Certificate Repository
            public static int TCImageFileName = 12000;
            public static int TCImageFileDescription = 12001;
            public static int TCImageFileVersion = 12002;
            public static int TCImageFileUploadedOn = 12003;
            public static int TCImageApplicablePlatformType = 12004;
            public static int TCImageApplicablePlatformName = 12005;
            public static int TCCertificateFileName = 12006;
            public static int TCCertificateFileDescription = 12007;
            public static int TCCertificateFileUploadedOn = 12008;
            public static int TCCertificateFileVersion = 12009;
            public static int TCCertificateApplicablePlatformName = 12010;
            public static int TCCertificateContent = 12011;

            // TC Wallpaper Repository
            public static int TCWallpaperFileName = 12012;
            public static int TCWallpaperFileDescription = 12013;
            public static int TCWallpaperFileUploadedOn = 12014;
            public static int TCWallpaperFileChecksum = 12015;

            // TC Logo Repository
            public static int TCLogoFileName = 12016;
            public static int TCLogoFileDescription = 12017;
            public static int TCLogoFileUploadedOn = 12018;
            public static int TCLogoFileChecksum = 12019;

            // TC EULA Repository
            public static int TCEulaFileName = 12020;
            public static int TCEulaFileDescription = 12021;
            public static int TCEulaFileUploadedOn = 12022;
            public static int TCEulaFileChecksum = 12023;


            // TC Image Upgrade
            public static int TCPolicyPlatformTypeR10 = 12100;
            public static int TCPolicyPlatformTypeR50 = 12101;
            public static int TCPolicyPlatformTypeR90 = 12102;

            public static int TCPolicyPlatformTypeC10 = 12103;
            public static int TCPolicyPlatformTypeC50 = 12104;
            public static int TCPolicyPlatformTypeC90 = 12105;

            public static int TCPolicyPlatformTypeT10 = 12106;
            public static int TCPolicyPlatformTypeT50 = 12107;
            public static int TCPolicyPlatformTypeT90 = 12108;

            public static int TCPolicyPlatformTypeZ10 = 12109;
            public static int TCPolicyPlatformTypeZ50 = 12110;
            public static int TCPolicyPlatformTypeZ90 = 12111;

            public static int TCPolicyPlatformTypeD10 = 12112;
            public static int TCPolicyPlatformTypeD50 = 12113;
            public static int TCPolicyPlatformTypeD90 = 12114;

            public static int TCPolicyPlatformTypeXenith = 12115;
            public static int TCPolicyPlatformTypeXenith2 = 12116;
            public static int TCPolicyPlatformTypeXenithPro = 12117;
            public static int TCPolicyPlatformTypeXenithPro2 = 12118;

            public static int TCSkipLocalFirmwareCheck = 12119;
            public static int TCUpgradeOnNextReboot = 12120;

            public static int TCPolicyPlatformTypeD10DP = 12121;
            public static int TCPolicyPlatformTypeT10D = 12122;

            //Ophelia Config setting
            //public static int OpheliaBlockAdminRights = 13000;
            public static int OpheliaAllowExternalFlash = 13001;
            public static int OpheliaBluetoothPrivilegeMode = 13002;
            public static int OpheliaUnknwnAppInstallationMode = 13003;
            public static int OpheliaAllowNotifications = 13004;
            //public static int OpheliaAllowUnknownSources = 13005;
            //public static int OpheliaShowUnknownsourcesfield = 13006;
            public static int OpheliaLockDownHomeScreen = 13007;
            public static int OpheliaUserMode = 13008;
            public static int OphelialaunchPadApps = 13009;
            public static int OpheliaKioskApp = 13010;
            public static int OpheliaAutoInstallCert = 13011;
            public static int OpheliaCertificates = 13012;
            //public static int OpheliaBlockSideLoading = 13013;
            public static int OpheliaAdminMode = 13014;

            // OPM Custom Lines in 'Advanced' tab
            public static int OPMCustomLine1 = 14001;
            public static int OPMCustomLine2 = 14002;
            public static int OPMCustomLine3 = 14003;
            public static int OPMCustomLine4 = 14004;
            public static int OPMCustomLine5 = 14005;
            public static int OPMCustomLine6 = 14006;
            public static int OPMCustomLine7 = 14007;
            public static int OPMCustomLine8 = 14008;
            public static int OPMCustomLine9 = 14009;
            public static int OPMCustomLine10 = 14010;

            public static int iOSAirplayDeviceId = 12200;
            public static int iOSAirplayPassword = 12201;
            public static int iOSAirPrintIpAddress = 12210;
            public static int iOSAirPrintResourcePath = 12211;
            public static int iOSFontName = 12220;
            public static int iOSFontData = 12221;
            public static int iOSFontFileName = 12222;

            // OnPremCcmProxy
            public static int OnPremCcmProxyContainerHost = 15001;


            // OnPremSsoAuth 
            public static int OnPremSsoAuthEnabled = 15101;
            public static int OnPremSsoAuthExchangeServer = 15102;
            public static int OnPremSsoAuthDomain = 15103;

            // OnPremADSync
            public static int OnPremAdSyncEnabled = 15201;
            public static int OnPremAdSyncExchangeServer = 15202;
            public static int OnPremAdSyncUserName = 15203;
            public static int OnPremAdSyncPassword = 15204;

            // OnPremKace 
            public static int OnPremKaceEnabled = 15301;
            public static int OnPremKaceServerHost = 15302;
            public static int OnPremKacePassword = 15303;
            public static int OnPremKaceIsHttps = 15304;
            public static int OnPremKaceSyncInterval = 15305;
            public static int OnPremKaceBatchSize = 15306;


            // for Keystone workspace
            public static int AllowExternalDataShare = 17000;
            public static int CopyAndPaste = 17001;
            public static int AllowGenuineOS = 17002;
            public static int MaxNoWorkspace = 17003;
            public static int WorkspacePin = 17004;
            public static int WorkspaceLoginFailure = 17005;
            public static int WorkspaceCheckInExpiration = 17006;
            public static int CheckInExpirationAction = 17007;
            public static int CalenderEnabled = 17010;
            public static int ContactsEnabled = 17011;
            public static int EmailEnabled = 17012;
            public static int FileBrowserEnabled = 17013;
            public static int WebBrowserEnabled = 17014;
            public static int WorkspaceInactivityLogout = 17015;
            public static int FailedLoginsAction = 17016;

            // for Keystone workspace
            public static int KeystoneAllowExternalDataShare = 17000;
            public static int KeystoneCopyAndPaste = 17001;
            public static int KeystoneAllowGenuineOS = 17002;
            public static int KeystoneMaxNoWorkspace = 17003;
            public static int KeystoneWorkspacePin = 17004;
            public static int KeystoneWorkspaceLoginFailure = 17005;
            public static int KeystoneWorkspaceCheckInExpiration = 17006;
            public static int KeystoneCheckInExpirationAction = 17007;
            public static int KeystoneCalenderEnabled = 17010;
            public static int KeystoneContactsEnabled = 17011;
            public static int KeystoneEmailEnabled = 17012;
            public static int KeystoneFileBrowserEnabled = 17013;
            public static int KeystoneWebBrowserEnabled = 17014;
            public static int KeystoneWorkspaceInactivityLogout = 17015;
            public static int KeystoneFailedLoginsAction = 17016;
            public static int KeystoneEmailUserDisplayName = 17017;
            public static int KeystoneEmailSignature = 17018;
            public static int KeystoneEmailSyncFrequency = 17019;
            public static int KeystoneDefaultHomePage = 17020;
            public static int KeystoneBookmark = 17021;
            public static int KeystoneWebBrowserProxyRequired = 17022;
            public static int KeystoneWebBrowserProxyIP = 17023;
            public static int KeystoneWebBrowserProxyPort = 17024;
            public static int KeystoneExchangeActiveSyncHost = 17025;
            public static int KeystoneExchangeActiveSyncUseSSL = 17026;
            public static int KeystoneExchangeActiveSyncDomain = 17027;
            public static int KeystoneExchangeActiveSyncDynamicUserInfo = 17028;
            public static int KeystoneExchangeActiveSyncUser = 17029;
            public static int KeystoneExchangeActiveSyncEmailAddress = 17030;
            public static int KeystoneExchangeActiveSyncPassword = 17031;
            public static int KeystoneExchangeActiveSyncAgentIdentifier = 17032;
            public static int KeystoneBrowserIdentityCertificate = 17033;
            public static int KeystoneBrowserCertificates = 17034;
            public static int KeystoneActiveSyncIdentityCertificate = 17035;
            public static int KeystoneActiveSyncCertificates = 17036;

            // registration rules
            public static int registrationMaxTotal = 18000;
            public static int registrationMaxDeviceIos = 18001;
            public static int registrationMaxDeviceIPhone = 18002;
            public static int registrationMaxDeviceIPad = 18003;
            public static int registrationMaxDeviceIPod = 18004;
            public static int registrationMaxDeviceAndroid = 18005;
            public static int registrationMaxDeviceCloudConnect = 18006;
            public static int registrationMinOsIPhone = 18010;
            public static int registrationMinOsIPad = 18011;
            public static int registrationMinOsIPod = 18012;
            public static int registrationMinOsAndroid = 18013;
            public static int registrationMaxKeystone = 18015;

            public static int iOSDeviceAllowNonEncryption = 18300;
            public static int AndroidDeviceAllowNonEncryption = 18301;

        }

        // configuration parameter group
        public static class PARAMETER_GROUP
        {

            // public static int Unknown = 0;
            // public static int General = 1;
            public static int Passcode = 2;
            public static int Restrictions = 3;
            public static int Other = 4;
            public static int MDM = 5;
            public static int APPLICATION = 6;
            public static int REGISTRATION = 7;

            public static int AndroidWifi = 10;
            public static int AndroidWifiEnterprise = 11;
            public static int AndroidWifiProtocol = 12; // no longer used, all group by AndroidWifiEnterprise
            public static int AndroidWifiAuthentication = 13; // no longer used, all grouop by AndroidWifiEnterprise
            // public static int AndroidExchangeActiveSync = 20;
            public static int AndroidExchangeActiveSyncEmailPolicyGroup = 21;
            public static int AndroidExchangeActiveSyncSecurityPolicyGroup = 22;
            public static int AndroidExchangeActiveSyncTouchDownGroup = 23;
            public static int Android_Advanced = 24;
            public static int Android_Advanced_Geolocation = 25;
            public static int Android_Advanced_DeviceCompromisation = 26;

            public static int AndroidVPN = 30;

            // IOS groupings
            public static int IOSPasscode = 101;
            public static int IOSRestrictions = 102;
            public static int IOSResFunctionality = 103;
            public static int IOSResUseCamera = 104;
            public static int IOSResApplication = 105;
            public static int IOSResAllowSafari = 106;
            public static int IOSResiCloud = 107;
            public static int IOSResSecurity = 108;
            public static int IOSResContentRating = 109;
            public static int IOSResAllowedContentRating = 110;
            public static int IOSDataSecurity = 111;

            public static int WiFi = 121;
            public static int WiFiSecurity = 122;
            public static int WiFiSecurityEnterprise = 123;
            public static int WiFiSecurityPersonal = 124;
            public static int WiFiSecurityEnterpriseProtocol = 125;
            public static int WiFiSecurityEnterpriseProtocolInnerIden = 126;
            public static int WiFiSecurityEnterpriseAuthentication = 127;
            public static int WiFiSecurityEnterpriseTrust = 128;
            public static int WiFiProxy = 129;
            public static int WiFiProxyManual = 130;
            public static int WiFiProxyAutomatic = 131;
            public static int WiFiProtocolAcceptedEAPTypes = 132;
            public static int WiFiProtocolEAPFast = 133;
            public static int WiFiSecurityEnterpriseTrustTrustedCerts = 134;
            public static int WiFiSecurityEnterpriseTrustTrustedServerCertNames = 135;
            public static int WiFiHotspot = 136;

            public static int VPN = 140;
            public static int VPNConnectionType = 1400;
            public static int VPNProxy = 1401;

            public static int Email = 161;
            public static int IncomingEmail = 162;
            public static int OutgoingEmail = 163;
            public static int OutgoingMailPassword = 164;
            public static int ExchangeActiveSync = 167;
            public static int LDAP = 170;
            public static int CalDav = 180;
            public static int SubscribedCalendars = 190;
            public static int CardDav = 200;
            public static int WebClips = 210;
            public static int Credentials = 220;
            public static int SCEP = 230;
            public static int IOSMDM = 240;
            public static int IOSMDMQueryDevice = 241;
            public static int IOSMDMAddRemove = 242;
            public static int IOSMDMSecurity = 243;
            public static int IOSMDMApns = 244;
            public static int Advanced = 250;
            public static int Advanced_Geolocation = 251;
            public static int Advanced_DeviceCompromisation = 252;

            public static int ThinOSRemoteConnections = 330;
            public static int ThinOSRemoteBrokeredConnections = 331;
            public static int ThinOSVDI = 341;
            public static int ThinOSCritrix = 342;
            public static int ThinOSStartup = 343;
            public static int ThinOSShutdown = 344;
            public static int ThinOSRDP = 345;
            public static int ThinOSContralConfig = 350;
            public static int ThinOSGeneral = 360;
            public static int ThinOSSecurity = 365;
            public static int ThinOSDisplay = 370;
            public static int ThinOSVisualExperience = 380;
            public static int ThinOSCustom = 390;
            // public static int ThinOSSystemPreferences=260;
            // public static int ThisOSTime=261;
            public static int ThinOSTSGatewaySettings = 391;
            public static int ThinOSSessionBehaviorDefaults = 392;
            public static int ThinOSPeripheralBehavior = 393;
            public static int ThinOSAdditionalSettings = 394;
            public static int ThinOSHDXProtocolSettings = 395;


            // Pocket cloud
            public static int PocketCloudRDP = 401;
            public static int IosPocketCloud = 402;

            // TC Image Repository and upgrade
            public static int TCImageRepository = 411;
            public static int TCFirmware = 412;
            // TC Certificate Repository
            public static int TCCertificateRepository = 413;
            // Wallpaper files for TC
            public static int TCWallpaperFileRepository = 414;
            // Logo files for TC
            public static int TCLogoFileRepository = 415;
            // EULA files for TC
            public static int TCEulaFileRepository = 416;

            public static int IOSAirPlay = 601;
            public static int IOSAirPrint = 611;
            public static int IOSFont = 621;

            public static int MaxTotalRegRestriction = 641;
            public static int MaxTypeRegRestriction = 642;
            public static int MinOsRegRestriction = 643;
            public static int MaxKeystoneRegRestriction = 644;

            // Ophelia related Parameter Group
            public static int Ophelia = 501;

            public static int OpheliaSecurity = 502;
            public static int OpheliaHomeScreen = 503;
            public static int OpheliaAdvanced = 504;

            public static int KeystoneGeneral = 701;
            public static int KeystoneAccess = 702;
            public static int KeystoneApps = 703;
            public static int KeystoneRestriction = 704;
            public static int KeystoneExchangeActiveSync = 705;

            // OnPremise parameter group, each correspond to one component
            public static int OnPremCcmProxy = 801;
            public static int OnPremSsoAuth = 802;
            public static int OnPremADSync = 803;
            public static int OnPremKace = 804;
            //public static int OnPremDeviceConcentrator = 805;

            public static int Unsupported = 901;

            public static int IOSAdvanced = 800;
            public static int AndroidAdvanced = 801;

        }

        public static class JOB_STATUS
        {

            // public static int Unknown = 0;
            public static int Queued = 1;
            public static int Completed = 2;
            public static int Running = 3;
            public static int Cancelled = 4;
            public static int Error = 5;
        }

        public static class TASK_STATUS
        {
            // public static int Unknown = 0;
            public static int Success = 1;
            public static int Running = 2;
            public static int Cancelled = 3;
            public static int Failed = 4;
        }

        public static class DEVICE_CONFIGURATION_TASK_STATE
        {
            // public static int Unknown = 0;
            public static int SendingNotification = 1;
            public static int DeviceNotified = 2;
            public static int NewConfigurationSent = 3;
            public static int Completed = 4;
        }

        public static class DEVICE_OPERATION_TASK_STATE
        {
            // public static int Unknown = 0;
            public static int SendingNotification = 1;
            public static int DeviceNotified = 2;
            public static int Completed = 3;
        }

        public static class SERVICE
        {

            // public static int Unknown = 0;
            public static int DeviceManagement = 1;
            public static int Rbac = 2;
        }

        public static class ROLE
        {

            // public static int Unknown = 0;
            public static int SuperAdmin = 1;
            public static int Admin = 2;
            public static int AdminViewOnly = 3;
            public static int SelfServiceView = 6;
        }

        public static class RULE_TYPE
        {
            public static int MaxTotalRegRestriction = 1;
            public static int MaxTypeRegRestriction = 2;
            public static int MinOsRegRestriction = 3;
            public static int MaxKeystoneRegRestriction = 4;
            public static int RegistrationRestriction = 5;
            public static int IosPredictiveRegRestriction = 6;
        }

        public static class CELLULAR_TECHNOLOGY
        {
            public static int None = 0;
            public static int Gsm = 1;
            public static int Cdma = 2;
        }

        // MDM Permission stuff
        public static class MDM_PERMISSION_GROUP
        {
            public static int queryBitValue = 1857; // for bit
            // 1,16,32,64,256,512,1024
            public static int addRemoveConfigProfileBitValue = 3; // bit 2 with 1
            public static int clearPasscodeBitValue = 4;
            public static int remoteWipeBitValue = 8;
            public static int addRemoveProvisioningProfileBitValue = 192; // bit 64
            // with
            // 128
            public static int addRemoveAppsBitValue = 4096;
            public static int voiceDataRoamingBitValue = 2048;
        }

        public static class PERMISSION_GROUP
        {
            public static String SuperAdmin = "SuperAdmin";
            public static String Admin = "Admin";
            public static String AdminViewOnly = "AdminViewOnly";
            public static String SelfService = "SelfService"; // mobile-user
            public static String CustomPermissionGroup = "CustomPermissionGroup"; //admin viewer with device privileges
        }

        public static class NETWORK_EAPTYPE
        {
            public static String TLS = "EAP-TLS";
            public static String PEAP = "EAP-PEAP";
            public static String LEAP = "EAP-LEAP";
        }

        public static class PRIVILEGE
        {

            // Role as privileges
            public static int SuperAdmin = 1;
            public static int Admin = 2;
            public static int AdminViewOnly = 3;
            public static int SelfService = 4;

            // regular privileges
            public static int Any = 100;
            public static int DisableTenant = 101;
            public static int CreateTenant = 102;
            public static int CreateTenantConfig = 103;
            public static int UpdateTenantInfo = 104;
            public static int ViewTenantInfo = 105;
            public static int CreateGroup = 110;
            public static int UpdateGroup = 111;
            public static int ViewGroup = 112;
            public static int RemoveGroup = 113;
            public static int CreateUser = 121;
            public static int UpdateUserInfo = 122;
            public static int ViewUserInfo = 123;
            public static int ViewUserSettings = 124;
            public static int UpdateUserSettings = 125;
            public static int RemoveUserSettings = 126;
            public static int RemoveUser = 127;
            public static int AddUserNote = 128;
            public static int RemoveUserNote = 129;
            public static int BulkImportUser = 130;
            public static int UpdateUser = 132;
            public static int ViewUserApp = 133;
            public static int ViewEvent = 151;
            public static int ViewAlert = 152;
            public static int ViewRequest = 153;
            public static int ViewEventLog = 154;
            public static int AssignUserToGroup = 161;
            public static int AssignDeviceToGroup = 162;
            public static int ApproveDenyRequest = 171;
            public static int ImportActiveDirectory = 181;
            public static int ImportDiscoveredDevices = 182;
            public static int SendRequest = 191;
            public static int RemoteLock = 192;
            public static int RemoteWipe = 193;
            public static int QueryDeviceInfo = 194;
            public static int ClearPasscode = 195;
            public static int UnEnrollDevice = 196;
            public static int RestartDevice = 197;
            public static int SendMessage = 198;
            public static int Troubleshooting = 199;
            public static int ViewDashBoard = 201;
            public static int ChangeDeviceOwnership = 211;
            //
            public static int ChangeDeviceAssignment = 212;
            public static int BulkImportDevice = 213;
            public static int ViewDeviceList = 214;
            public static int ViewDeviceInfo = 215;
            public static int ViewDeviceEventsAndAlerts = 216;
            public static int ViewDeviceSettings = 217;
            public static int UpdateDeviceSettings = 218;
            public static int RemoveDeviceSettings = 219;
            public static int AddDeviceNote = 220;
            public static int RemoveDeviceNote = 221;
            public static int ViewDeviceApp = 222;
            public static int ViewGroupSettings = 231;
            public static int UpdateGroupSettings = 232;
            public static int UploadGroupCredentials = 241;
            public static int UploadUserCredentials = 242;
            public static int UploadDeviceCredentials = 243;
            public static int ViewSelfServiceAgreement = 251;
            public static int SetSelfServiceAgreementStatus = 252;
            public static int UpdateSelfServiceAgreement = 253;
            public static int ViewEmailTemplate = 261;
            public static int SendInvitation = 262;
            public static int SendPassword = 263;
            public static int GlobalSearch = 271;
            public static int ViewManagedApp = 281;
            public static int CreateManagedApp = 282;
            public static int RemoveManagedApp = 283;
            public static int SearchAppStore = 284;
            public static int CreateDeviceAppPolicy = 285;
            public static int RemoveDeviceAppPolicy = 286;
            public static int CreateUserAppPolicy = 287;
            public static int RemoveUserAppPolicy = 288;
            public static int CreateGroupAppPolicy = 289;
            public static int RemoveGroupAppPolicy = 290;
            public static int ViewAppPolicy = 291;
            public static int InstallDeviceApplication = 292;
            public static int RemoveDeviceApplication = 293;
            public static int UpdateAppPolicy = 294;
            public static int EnableTenant = 295;
            public static int ForcePasswordChange = 296;
            public static int NotifyDevice = 301;
            public static int ViewManagedDataFolder = 310;
            public static int CreateManagedDataFolder = 311;
            public static int RemoveManagedDataFolder = 312;
            public static int ViewManagedDataFile = 315;
            public static int CreateManagedDataFile = 316;
            public static int RemoveManagedDataFile = 317;
            public static int UpdateDataFolderPolicy = 318;
            public static int ViewDataFolderPolicy = 319;
            public static int ViewPlatformList = 320;
            public static int AddTCImage = 321;
            public static int DeleteTCImage = 322;
            public static int UpdateTCImage = 323;

            public static int Republish = 324;

            public static int SendVPPInvite = 325;
        }

        // stratus alert types
        public static class ALERT_TYPE
        {

            // public static int Unknown = 0;

            public static int DeviceJailBroken = 101;
            public static int DevicePasswordUncompliance = 102;
            public static int DeviceMissingRequiredApp = 103;
            public static int DeviceInstalledBlacklistedApp = 104;
            public static int DeviceNotCheckin = 105;
            public static int DeviceMissingRequiredProfiles = 106;

            public static int DeviceNonEncryptionAlert = 107;
            public static int ConfigurationExceptionRequest = 201;
        }

        // stratus event types
        public static class EVENT_TYPE
        {

            // public static int Unknown = 0;

            public static int DeviceLogin = 101;
            // not on DASHBOARD.
            // Single: $timestamp | $user logged in on $device-name
            // Multipel: $date-only | Summary: $1st-user and $rest users logged in.
            // Detail: $user logged into $device-name at $timestamp

            public static int DeviceLogout = 102;
            // not on DASHBOARD.
            // Single: $timestamp | $user logged out of $device-name
            // Multipel: $date-only | Summary: $1st-user and $rest users logged out.
            // Detail: $user logged out of $device-name at $timestamp

            // ***** MUST HAVE
            public static int DeviceRegister = 103;
            // Single: $timestamp | $user registered $device-name
            // Multipel: $date-only | Summary: $1st-user and $rest users registered
            // devices. Detail: $user registered $device-name at $timestamp

            public static int DeviceCheckin = 104;
            // Single: $timestamp | $device checked-in
            // Multipel: $date-only | Summary: $1st-device and $rest devices
            // checked-in. Detail: $device-name checked-in at $timestamp

            public static int DeviceNotCheckin = 105;
            // Single: $timestamp | $device has not checked-in for more than
            // $check-in-interval
            // Multipel: $date-only | Summary: $1st-device and $rest devices have
            // not checked-in for more than $check-in-interval. Detail: $device-name
            // has not checked-in for $check-in-interval
            // END ***** MUST HAVE

            public static int DeviceJailbroken = 106;

            // ***** MUST HAVE
            public static int DeviceInstalledBlacklistApps = 107;
            // Single: $timestamp | $device-name installed blacklist apps - $app1,
            // $app2, etc.
            // Multipel: $date-only | Summary: $1st-device and $rest devices
            // installed blacklist apps. Detail: $device-name - $app11, $app22, etc.
            // END ***** MUST HAVE

            // ***** MUST HAVE
            public static int DeviceConfigParamCatalogNonCompliant = 108;
            // Single: $timestamp | $device-name is out-of-compliance - $reason1,
            // $reason2, etc.
            // Multipel: $date-only | Summary: $1st-device and $rest devices are
            // out-of-compliance. Detail: $device-name - $reason1, $reason2, etc.
            // END ***** MUST HAVE

            public static int DeviceRemotelyLocked = 109;
            public static int DeviceConfigurationCreated = 110;
            public static int DeviceConfigurationUpdated = 111;
            public static int DeviceConfigurationFailed = 112;
            public static int DeviceConfigurationDeleted = 113;
            public static int DeviceReRegister = 114;
            public static int DeviceCheckout = 115;

            // newly added for thin client: device remote connection launched
            public static int DeviceRemoteConnection = 116;

            // newly added for thin client: device connection exit Event
            public static int DeviceRemoteConnectionExit = 117;

            // newly added for thin client: device generic Audit Event
            public static int DeviceGenericAudit = 118;

            // newly added for thin client: device generic Audit Event
            public static int DeviceAssignmentChanged = 119;

            // ***** MUST HAVE
            public static int DeviceCreate = 120;
            // Single: $timestamp | $initiator created $device
            // Multipel: $date-only | Summary: $1st-device and $rest devices were
            // created. Detail: $intiator created $device at $timestamp
            public static int DeviceDelete = 121;
            public static int DeviceMissingRequiredApps = 122;
            public static int DeviceAppPolicyUpdated = 123;
            public static int DeviceAppPolicyCreated = 124;
            public static int DeviceAppPolicyDeleted = 125;
            public static int DeviceEnroll = 126;

            public static int DevicePasscodeNonCompliant = 127;
            public static int DeviceMissingRequiredProfiles = 128;
            // Single: $timestamp | $initiator deleted $device
            // Multipel: $date-only | Summary: $1st-device and $rest device(s) were
            // deleted. Detail: $intiator deleted $device at $timestamp
            public static int UserCreate = 201;
            // Single: $timestamp | $initiator created $user
            // Multipel: $date-only | Summary: $1st-user and $rest user(s) were
            // created. Detail: $intiator created $user at $timestamp
            public static int UserDelete = 202;
            // Single: $timestamp | $initiator deleted $user
            // Multipel: $date-only | Summary: $1st-user and $rest user(s) were
            // deleted. Detail: $intiator deleted $user at $timestamp
            // END ***** MUST HAVE

            public static int UserConfigurationNonCompliant = 203;
            public static int UserConfigurationUpdated = 204;
            public static int UserConfigurationCreated = 205;
            public static int UserConfigurationDeleted = 207;
            public static int UserReRegister = 206;
            public static int UserAppPolicyUpdated = 207;
            public static int UserAppPolicyCreated = 208;
            public static int UserAppPolicyDeleted = 209;

            public static int UserDisabled = 210;
            public static int UserEnabled = 211;

            public static int UserGroupChanged = 212;
            public static int UserSharesITunesAccount = 213;
            public static int UserSharesITunesAccountResolved = 214;

            // ***** MUST HAVE
            public static int GroupCreate = 301;
            // Single: $timestamp | $intiator creaste $group
            // Multipel: $date-only | Summary: $1st-group and $rest group(s) were
            // created. Detail: $intiator deleted $group at $timestamp
            public static int GroupDelete = 302;
            // Single: $timestamp | $intiator deleted $group
            // Multipel: $date-only | Summary: $1st-group and $rest group(s) were
            // deleted. Detail: $intiator deleted $group at $timestamp
            // END ***** MUST HAVE

            public static int GroupConfigurationNonCompliant = 303;

            // ***** MUST HAVE
            public static int GroupConfigurationUpdated = 304;
            // Single: $timestamp | $initiator modified settings for
            // $user-group-name.
            // Multipel: $date-only | Summary: Group settings have been modified
            // $times. Detail: $initiator modified $param-group-name in $group-name
            // at $timestamp
            // END ***** MUST HAVE
            public static int GroupAppPolicyUpdated = 306;
            public static int GroupAppPolicyCreated = 307;
            public static int GroupAppPolicyDeleted = 308;
            public static int ConfigurationExceptionRequestCreated = 401;
            public static int ConfigurationExceptionRequestResolved = 402;
            public static int ConfigurationExceptionRequestReasonUpdated = 403;
            public static int ApplyGroupAppPolicyJobCreated = 404;

            public static int DeviceRemoteCommand = 501;
            public static int IntallApplication = 502;
            public static int RemoveApplication = 503;
            public static int InstallProfile = 504;
            public static int RemoveProfile = 505;
            public static int RemoteWipe = 506;
            public static int DeviceInformation = 507;
            public static int Lock = 508;
            public static int UnEnrollDevice = 509;
            public static int Restart = 510;
            public static int ClearPassowrd = 511;
            public static int ManagedApplicationList = 512;
            public static int GroupChanged = 513;
            public static int Shutdown = 514;
            public static int SendMessage = 515;
            public static int Troubleshooting = 516;
            public static int GetScreenCapture = 517;
            public static int GetPerformanceMetrics = 518;
            public static int SetDeviceLocation = 519;
            public static int ProfileList = 520;
            public static int InstallApplicationList = 521;


            public static int TenantCreated = 601;
            public static int TenantDeleted = 602;
            public static int ManagedAppCreated = 603;
            public static int ManagedAppDeleted = 604;

            public static int TenantDisabled = 605;
            public static int TenantEnabled = 606;

            // dataLocker related events
            public static int FolderCreated = 607;
            public static int FolderDeleted = 608;
            public static int FolderUpdated = 609;
            public static int FileCreated = 610;
            public static int FileUpdated = 611;
            public static int FileDeleted = 612;
            public static int TenantAppPolicyUpdated = 613;
            public static int FolderPolicyChanged = 614;
            public static int FileDownloadedByUserOnDevice = 615;
            public static int FileRemovedByUserOnDevice = 616;
            public static int DataLockerEnabled = 617;

            // VPP Order related events
            public static int VppOrderUploaded = 618;
            public static int VppOrderUpdated = 619;
            public static int VppOrderDeleted = 620;
            public static int VppRedemptionCodeReleased = 621;

            public static int TenantUpdated = 622;
            public static int DeviceFolderCreated = 623;
            public static int DeviceFolderDeleted = 624;
            public static int DeviceFolderUpdated = 625;
            public static int DeviceFileCreated = 626;
            public static int DeviceFileUpdated = 627;
            public static int DeviceFileDeleted = 628;

            // PC Agent FileRenamed
            public static int FileRenamed = 629;
            public static int FileActivated = 630;
            public static int FileDeactivated = 631;
            public static int PCAgentPublishMode = 632;
            public static int PublishedLatestPCAgent = 633;

            // PC Agent Deletion
            public static int PCAgentDeleted = 637;

            // Portal Notification events
            public static int PortalNotificationUpdate = 634;
            public static int PortalNotificationEnabled = 635;
            public static int PortalNotificationDisabled = 636;

            // Resolved Events
            public static int DeviceJailbrokenResolved = 701;
            public static int DeviceMissingRequiredAppsResolved = 702;
            public static int DeviceInstalledBlacklistAppsResolved = 703;
            public static int DeviceNotCheckinResolved = 704;
            public static int DeviceConfigParamCatalogNonCompliantResolved = 705;
            public static int DevicePasscodeNonCompliantResolved = 706;
            public static int DeviceMissingRequiredProfilesResolved = 707;

            // MDM APNS Certificate Uploaded
            public static int MdmApnsCertificateUploaded = 708;

            // Device Maximum Compliance check-in interval updated
            public static int DeviceComplianceCheckinIntervalUpdated = 709;

            // Active Directory Mode changed
            public static int ActiveDirectoryModeChanged = 800;

            public static int UserRoleAssigned = 810;
            public static int UserRoleRevoked = 811;
            public static int UserRoleEdited = 812;

            public static int BulkImportLicenseError = 820;
            public static int ManualADSyncLicenseError = 821;

            // SSO event
            public static int SSOEnabled = 830;
            public static int SSODisabled = 831;

            // TC image related events
            public static int TCImageUploaded = 840;
            public static int TCImageDeleted = 841;
            public static int TCImageDownloadedByDevice = 842;
            public static int TCCertificateUploaded = 843;
            public static int TCCertificateDeleted = 844;
            public static int TCImageUpdated = 845;
            public static int TCCertificateUpdated = 846;
            public static int TCWallpaperUploaded = 847;
            public static int TCWallpaperDeleted = 848;
            public static int TCWallpaperUpdated = 849;
            public static int TCLogoUploaded = 1850;
            public static int TCLogoDeleted = 1851;
            public static int TCLogoUpdated = 1852;
            public static int TCEulaUploaded = 1853;
            public static int TCEulaDeleted = 1854;
            public static int TCEulaUpdated = 1855;


            public static int DeviceCheckoutServerRemoteCommand = 851;
            // PC Agent checkout
            public static int PCCheckoutUserManualUnregister = 852;
            public static int PCCheckoutAgentUninstalled = 853;
            // iOS checkout
            public static int IOSCheckoutUserManualUnregister = 855;
            // iOS checkout

            public static int AndroidCheckoutUserManualUnregister = 860;

            // Unregister Remote command per device type
            public static int UnregisterThinClient = 865;
            public static int UnregisteriOS = 866;
            public static int UnregisterAndroid = 867;
            public static int UnregisterPC = 868;

            //Enterprise apps
            public static int EnterpriseAppCreated = 900;
            public static int EnterpriseAppEdited = 901;

            // Ophelia Device
            public static int OpheliaCheckUpdate = 1001;
            public static int OpheliaForceCheckUpdate = 1002;

            // Keystone related events
            public static int KeystoneRegistration = 1101;
            public static int KeystoneUnRegistration = 1102;
            public static int WorkspaceAdminLockEvent = 1103;
            public static int WorkspaceUnlockEvent = 1104;
            public static int WorkspaceUserLockEvent = 1105;
            public static int WorkspaceResetPINEvent = 1106;
            public static int WipeWorkspaceData = 1107;
            public static int KeystoneReRegistration = 1108;

            // Registration restriction events
            public static int MaxTotalRestrictionEnabled = 1201;
            public static int MaxTotalRestrictionDisabled = 1202;
            public static int MaxTotalRestrictionUpdated = 1203;

            public static int MaxTypeRestrictionEnabled = 1204;
            public static int MaxTypeRestrictionDisabled = 1205;
            public static int MaxTypeRestrictionUpdated = 1206;

            public static int MinOsRestrictionEnabled = 1207;
            public static int MinOsRestrictionDisabled = 1208;
            public static int MinOsRestrictionUpdated = 1209;

            public static int MaxKeystoneRestrictionEnabled = 1210;
            public static int MaxKeystoneRestrictionDisabled = 1211;
            public static int MaxKeystoneRestrictionUpdated = 1212;

            public static int MaxTotalRegistrationBlocked = 1220;
            public static int MaxTypeRegistrationBlocked = 1221;
            public static int MinOsRegistrationBlocked = 1222;
            public static int MaxKeystoneRegistrationBlocked = 1223;

            public static int DeviceEncryptionNotEnabled = 1240;
            public static int DeviceEncryptionNotEnabledResolved = 1241;

            public static int OnPremiseConfigChange = 1301;
        }

        // counter type
        public static class COUNTER_TYPE
        {

            // public static int Unknown = 0;
            // Total user group counters
            public static int TotalUserGroup = 1;
            // Total user counters
            public static int TotalUser = 1000;
            public static int TotalUserRoaming = 1001;
            public static int TotalUserRoamingByCountry = 1002;
            public static int TotalUserOutOfCompliance = 1003;
            public static int TotalUserByInstalledApp = 1004;
            public static int TotalUserWithNoEncryptionDevice = 1005;
            public static int TotalUserWithJailbrokenDevice = 1006;
            // Total device counters
            public static int TotalDevice = 2001;
            public static int TotalDeviceByDeviceType = 2002;
            public static int TotalDeviceByInstalledApp = 2003;
            public static int TotalDeviceWithWeakEncryption = 2004;
            public static int TotalDeviceJailbroken = 2005;
            public static int TotalDeviceOutOfCompliance = 2006;
            public static int TotalDeviceWithBlacklistedApps = 2007;
            public static int TotalDeviceByDeviceStatus = 2008;
            public static int TotalDeviceAssigned = 2009;
            public static int TotalDeviceRoaming = 2010;
            // Total event counters
            public static int TotalEvent = 3001;
            public static int TotalEventByType = 3002;
            public static int TotalTenantEvent = 3003;
            public static int TotalTenantEventByType = 3004;
            public static int TotalUserGroupEvent = 3005;
            public static int TotalUserGroupEventByType = 3006;
            public static int TotalUserEvent = 3007;
            public static int TotalUserEventByType = 3008;
            public static int TotalDeviceEvent = 3009;
            public static int TotalDeviceEventByType = 3010;
        }

        public static class DEVICE_FILTER
        {
            public static short NONE = 0;
            public static short NON_COMPLIANT = 1;
            public static short PENDING = 2;
            public static short NEW = 3;
            public static short COMPLIANT = 4;
        }

        public static class USER_FILTER
        {
            public static short NONE = 0;
            public static short NON_COMPLIANT = 1;
            public static short PENDING = 2;
            public static short NEW = 3;
            public static short COMPLIANT = 4;
        }

        public static class EVENT_SEARCH_FILTER
        {
            public static short ALERT_AND_EVENT = 0;
            public static short ALERT = 1;
            public static short ALL = 2; // including request's event
            public static short EVENTS = 3;
            public static short ALERT_HISTORY = 4;

        }

        public static class EVENT_TIME_FILTER
        {
            public static short NONE = 0;
            public static short TODAY = 1;
            public static short YESTERDAY = 2;
            public static short THIS_WEEK = 3;
            public static short THIS_MONTH = 4;
            public static short LAST_WEEK = 5;
            public static short CUSTOM_DATE = 6;
        }

        public static class EVENT_GROUP_TYPE
        {
            public static short ACCESS = 1;
            public static short REGISTRAION = 2;
            public static short CONFIGURATION = 3;
            public static short REMOTE_COMMANDS = 4;
            public static short MANAGEMENT = 5;
            public static short COMPLIANCE = 6;

        }

        public static class ALERT_GROUP_TYPE
        {
            public static short NOTCHECKEDIN = 1;
            public static short DEVICE_COMPLIANCE = 2;
            public static short APP_COMPLIANCE = 3;
            public static short OTHER = 4;

        }

        public static class EVENT_GROUPING
        {
            public static short YES = 0;
            public static short NO = 1;
        }

        public static class THINOS_DESKTOP_MODE
        {
            public static short CLASSIC = 0;
            public static short ZERO_LAUNCHPAD = 1;
        }

        public static class THINOS_ACTION_AFTER_SESSION_EXIT
        {
            public static short NONE = 0;
            public static short SIGN_OFF_AUTOMATICALLY = 1;
            public static short SHUTDOWN_AUTOMATICALLY = 2;
            public static short RESTART_AUTOMATICALLY = 3;
        }

        public static class OWNERSHIP_TYPE
        {
            public static int UNDEFINED = 0;
            public static int COMPANY_DEDICATED = 1;
            public static int COMPANY_SHARED = 2;
            public static int EMPLOYEE_OWNED = 3;
        }

        public static class CONFIG_ITEM_LEVEL
        {
            public static int GLOBAL = 1;
            public static int GROUP = 2;
            public static int USER = 3;
            public static int DEVICE = 4;
            public static int DEFAULT = 5;
        }

        public static class REMOTE_CONNECTION_TYPE
        {
            public static int CITRIX = 1;
            public static int MICROSOFT = 2; // RDP
            public static int VMWARE = 3;
        }

        public static class AndroidInnerIdentityType
        {
            public static String None = "None";
            public static String PAP = "PAP";
            public static String MSCHAP = "MSCHAP";
            public static String MSCHAPv2 = "MSCHAPv2";
            public static String GTC = "GTC";
        }

        public static class SERVICE_PROVIDER
        {
            public static int OtherSP = 0;
            public static int Rogers = 1;
            public static int Fido = 2;
            public static int VerizonWireless = 3;
            public static int Telus = 4;
            public static int Bell = 5;
            public static int Sprint = 6;
            public static int ATT = 7;
            public static int Vodafone = 8;
            public static int VodafoneSweden = 9;
            public static int VodafoneAlbania = 10;
            public static int VodafoneAustralia = 11;
            public static int VodafoneCzechRepublic = 12;
            public static int VodafoneIreland = 13;
            public static int VodafoneEgypt = 14;
            public static int VodafoneGreece = 15;
            public static int VodafoneSpain = 16;
            public static int VodafoneGermany = 17;
            public static int VodafoneHungary = 18;
            public static int VodafoneItaly = 19;
            public static int VodafoneJapan = 20;
            public static int VodafoneRomania = 21;
            public static int VodafoneMalta = 22;
            public static int VodafoneNorway = 23;
            public static int VodafoneNewZealand = 24;
            public static int VodafonePortugal = 25;
            public static int VodafoneNetherlands = 26;
            public static int OrangeAustria = 27;
            public static int OrangeDenmark = 28;
            public static int OrangeDominicanRepublic = 29;
            public static int OrangeBelgium = 30;
            public static int OrangeFrance = 31;
            public static int OrangeIsrael = 32;
            public static int OrangePoland = 33;
            public static int OrangeRomania = 34;
            public static int OrangeSlovakia = 35;
            public static int OrangeES = 36;
            public static int OrangeSwitzerland = 37;
            public static int OrangeUK = 38;
            public static int OrangeHongKong = 39;
            public static int RelianceIndia = 40;
            public static int TMobile = 41;
            public static int Telstra = 42;
            public static int TelefonicaArgentina = 43;
            public static int MovistarChile = 44;
            public static int TelefonicaCzechRepublic = 45;
            public static int TelefonicaElSalvador = 46;
            public static int TelefonicaIreland = 47;
            public static int TelefonicaNicaragua = 48;
            public static int TelefonicaPanama = 49;
            public static int TelefonicaSpain = 50;
            public static int TelefonicaUK = 51;
            public static int TelefonicaNetherlands = 52;
            public static int TelecomItaliaBrazil = 53;
            public static int TelecomItaliaItaly = 54;
            public static int BellHSPA = 55;
            public static int TelusHSPA = 56;
            public static int VivoBrazil = 57;
            public static int ClaroBrazil = 58;
            public static int PlusGSMPoland = 59;
            public static int EraPoland = 60;
            public static int EntelChile = 61;
            public static int ChinaMobile = 62;
            public static int ChinaUnicom = 63;
            public static int BhartiAirtelIndia = 64;
            public static int BSNLIndia = 65;
            public static int TelcelMexico = 66;
            public static int MTSRussia = 67;
            public static int BeelineRussia = 68;
            public static int SingTelSingapore = 69;
            public static int StarhubSingapore = 70;
            public static int EtisalatUAE = 71;
            public static int TMobileGermany = 72;
            public static int TurkcellTurkey = 73;
            public static int Videotron = 74;
            public static int BellTelus = 75;
        }

        public static class IMSI
        {
            public static int Rogers302720 = 302720;
            public static int Fido302370 = 302370;
            public static int VerizonWireless310008 = 310008;
            public static int VerizonWireless310012 = 310012;
            public static int Telus302086 = 302086;
            public static int Bell302640 = 302640;
            public static int Sprint310120 = 310120;
            public static int Sprint310038 = 310038;
            public static int ATT310050 = 310050;
            public static int ATT310070 = 310070;
            public static int ATT310090 = 310090;
            public static int ATT310150 = 310150;
            public static int ATT310311 = 310311;
            public static int ATT310380 = 310380;
            public static int ATT310410 = 310410;
            public static int ATT310560 = 310560;
            public static int ATT310670 = 310670;
            public static int ATT310680 = 310680;
            public static int ATT310890 = 310890;
            public static int ATT310980 = 310980;
            public static int ATT310990 = 310990;
            public static int ATT311070 = 311070;
            public static int ATT311180 = 311180;
            public static int Vodafone234015 = 234015;
            public static int VodafoneSweden240008 = 240008;
            public static int VodafoneAlbania276002 = 276002;
            public static int VodafoneAustralia505003 = 505003;
            public static int VodafoneAustralia505007 = 505007;
            public static int VodafoneCzechRepublic230003 = 230003;
            public static int VodafoneIreland272001 = 272001;
            public static int VodafoneEgypt602002 = 602002;
            public static int VodafoneGreece202005 = 202005;
            public static int VodafoneSpain214001 = 214001;
            public static int VodafoneSpain214006 = 214006;
            public static int VodafoneSpain214019 = 214019;
            public static int VodafoneSpain214033 = 214033;
            public static int VodafoneGermany262002 = 262002;
            public static int VodafoneGermany262004 = 262004;
            public static int VodafoneGermany262009 = 262009;
            public static int VodafoneHungary216070 = 216070;
            public static int VodafoneItaly222010 = 222010;
            public static int VodafoneJapan440004 = 440004;
            public static int VodafoneJapan440006 = 440006;
            public static int VodafoneJapan440020 = 440020;
            public static int VodafoneJapan440040 = 440040;
            public static int VodafoneJapan440041 = 440041;
            public static int VodafoneJapan440042 = 440042;
            public static int VodafoneJapan440043 = 440043;
            public static int VodafoneJapan440044 = 440044;
            public static int VodafoneJapan440045 = 440045;
            public static int VodafoneJapan440046 = 440046;
            public static int VodafoneJapan440047 = 440047;
            public static int VodafoneJapan440048 = 440048;
            public static int VodafoneJapan440061 = 440061;
            public static int VodafoneJapan440062 = 440062;
            public static int VodafoneJapan440063 = 440063;
            public static int VodafoneJapan440064 = 440064;
            public static int VodafoneJapan440065 = 440065;
            public static int VodafoneJapan440090 = 440090;
            public static int VodafoneJapan440092 = 440092;
            public static int VodafoneJapan440093 = 440093;
            public static int VodafoneJapan440094 = 440094;
            public static int VodafoneJapan440095 = 440095;
            public static int VodafoneJapan440096 = 440096;
            public static int VodafoneJapan440097 = 440097;
            public static int VodafoneJapan440098 = 440098;
            public static int VodafoneRomania226001 = 226001;
            public static int VodafoneMalta278001 = 278001;
            public static int VodafoneNorway242001 = 242001;
            public static int VodafoneNewZealand530001 = 530001;
            public static int VodafonePortugal268001 = 268001;
            public static int VodafoneNetherlands204004 = 204004;
            public static int OrangeAustria232005 = 232005;
            public static int OrangeDenmark238002 = 238002;
            public static int OrangeDominicanRepublic370001 = 370001;
            public static int OrangeBelgium206020 = 206020;
            public static int OrangeFrance208000 = 208000;
            public static int OrangeFrance208001 = 208001;
            public static int OrangeFrance208002 = 208002;
            public static int OrangeIsrael425001 = 425001;
            public static int OrangePoland260003 = 260003;
            public static int OrangeRomania226010 = 226010;
            public static int OrangeSlovakia231001 = 231001;
            public static int OrangeES214003 = 214003;
            public static int OrangeES214009 = 214009;
            public static int OrangeSwitzerland228003 = 228003;
            public static int OrangeUK234033 = 234033;
            public static int OrangeUK234034 = 234034;
            public static int OrangeHongKong454004 = 454004;
            public static int RelianceIndia404009 = 404009;
            public static int RelianceIndia404017 = 404017;
            public static int RelianceIndia404036 = 404036;
            public static int RelianceIndia404050 = 404050;
            public static int RelianceIndia404052 = 404052;
            public static int RelianceIndia404067 = 404067;
            public static int RelianceIndia404083 = 404083;
            public static int RelianceIndia404085 = 404085;
            public static int RelianceIndia405001 = 405001;
            public static int RelianceIndia405003 = 405003;
            public static int RelianceIndia405004 = 405004;
            public static int RelianceIndia405005 = 405005;
            public static int RelianceIndia405006 = 405006;
            public static int RelianceIndia405007 = 405007;
            public static int RelianceIndia405008 = 405008;
            public static int RelianceIndia405009 = 405009;
            public static int RelianceIndia405010 = 405010;
            public static int RelianceIndia405011 = 405011;
            public static int RelianceIndia405012 = 405012;
            public static int RelianceIndia405013 = 405013;
            public static int RelianceIndia405014 = 405014;
            public static int RelianceIndia405015 = 405015;
            public static int RelianceIndia405017 = 405017;
            public static int RelianceIndia405018 = 405018;
            public static int RelianceIndia405019 = 405019;
            public static int RelianceIndia405020 = 405020;
            public static int RelianceIndia405021 = 405021;
            public static int RelianceIndia405022 = 405022;
            public static int RelianceIndia405023 = 405023;
            public static int TMobile310160 = 310160;
            public static int TMobile310170 = 310170;
            public static int TMobile310200 = 310200;
            public static int TMobile310210 = 310210;
            public static int TMobile310220 = 310220;
            public static int TMobile310230 = 310230;
            public static int TMobile310240 = 310240;
            public static int TMobile310250 = 310250;
            public static int TMobile310260 = 310260;
            public static int TMobile310270 = 310270;
            public static int TMobile310280 = 310280;
            public static int TMobile310290 = 310290;
            public static int TMobile310300 = 310300;
            public static int TMobile310310 = 310310;
            public static int TMobile310330 = 310330;
            public static int TMobile310660 = 310660;
            public static int TMobile310800 = 310800;
            public static int Telstra505001 = 505001;
            public static int Telstra505005 = 505005;
            public static int TelefonicaArgentina722010 = 722010;
            public static int TelefonicaArgentina722070 = 722070;
            public static int MovistarChile730002 = 730002;
            public static int TelefonicaCzechRepublic230002 = 230002;
            public static int TelefonicaElSalvador706004 = 706004;
            public static int TelefonicaIreland272002 = 272002;
            public static int TelefonicaNicaragua710030 = 710030;
            public static int TelefonicaPanama714002 = 714002;
            public static int TelefonicaSpain214007 = 214007;
            public static int TelefonicaUK234010 = 234010;
            public static int TelefonicaUK234011 = 234011;
            public static int TelefonicaNetherlands204012 = 204012;
            public static int TelefonicaNetherlands204018 = 204018;
            public static int TelecomItaliaBrazil724002 = 724002;
            public static int TelecomItaliaBrazil724003 = 724003;
            public static int TelecomItaliaBrazil724004 = 724004;
            public static int TelecomItaliaBrazil724008 = 724008;
            public static int TelecomItaliaItaly222001 = 222001;
            public static int BellHSPA302610 = 302610;
            public static int TelusHSPA302220 = 302220;
            public static int VivoBrazil724006 = 724006;
            public static int VivoBrazil724010 = 724010;
            public static int VivoBrazil724011 = 724011;
            public static int VivoBrazil724023 = 724023;
            public static int ClaroBrazil724005 = 724005;
            public static int PlusGSMPoland260001 = 260001;
            public static int EraPoland260002 = 260002;
            public static int EntelChile730001 = 730001;
            public static int ChinaMobile460000 = 460000;
            public static int ChinaMobile460002 = 460002;
            public static int ChinaMobile460007 = 460007;
            public static int ChinaUnicom460001 = 460001;
            public static int ChinaUnicom460006 = 460006;
            public static int BhartiAirtelIndia404002 = 404002;
            public static int BhartiAirtelIndia404003 = 404003;
            public static int BhartiAirtelIndia404010 = 404010;
            public static int BhartiAirtelIndia404016 = 404016;
            public static int BhartiAirtelIndia404031 = 404031;
            public static int BhartiAirtelIndia404040 = 404040;
            public static int BhartiAirtelIndia404045 = 404045;
            public static int BhartiAirtelIndia404049 = 404049;
            public static int BhartiAirtelIndia404070 = 404070;
            public static int BhartiAirtelIndia404090 = 404090;
            public static int BhartiAirtelIndia404091 = 404091;
            public static int BhartiAirtelIndia404092 = 404092;
            public static int BhartiAirtelIndia404093 = 404093;
            public static int BhartiAirtelIndia404094 = 404094;
            public static int BhartiAirtelIndia404095 = 404095;
            public static int BhartiAirtelIndia404096 = 404096;
            public static int BhartiAirtelIndia404097 = 404097;
            public static int BhartiAirtelIndia404098 = 404098;
            public static int BhartiAirtelIndia405051 = 405051;
            public static int BhartiAirtelIndia405052 = 405052;
            public static int BhartiAirtelIndia405053 = 405053;
            public static int BhartiAirtelIndia405054 = 405054;
            public static int BhartiAirtelIndia405055 = 405055;
            public static int BhartiAirtelIndia405056 = 405056;
            public static int BSNLIndia404034 = 404034;
            public static int BSNLIndia404038 = 404038;
            public static int BSNLIndia404051 = 404051;
            public static int BSNLIndia404053 = 404053;
            public static int BSNLIndia404054 = 404054;
            public static int BSNLIndia404055 = 404055;
            public static int BSNLIndia404057 = 404057;
            public static int BSNLIndia404058 = 404058;
            public static int BSNLIndia404059 = 404059;
            public static int BSNLIndia404062 = 404062;
            public static int BSNLIndia404064 = 404064;
            public static int BSNLIndia404066 = 404066;
            public static int BSNLIndia404071 = 404071;
            public static int BSNLIndia404072 = 404072;
            public static int BSNLIndia404073 = 404073;
            public static int BSNLIndia404074 = 404074;
            public static int BSNLIndia404075 = 404075;
            public static int BSNLIndia404076 = 404076;
            public static int BSNLIndia404077 = 404077;
            public static int BSNLIndia404078 = 404078;
            public static int BSNLIndia404079 = 404079;
            public static int BSNLIndia404080 = 404080;
            public static int BSNLIndia404081 = 404081;
            public static int TelcelMexico334020 = 334020;
            public static int MTSRussia250001 = 250001;
            public static int BeelineRussia250099 = 250099;
            public static int SingTelSingapore525001 = 525001;
            public static int SingTelSingapore525002 = 525002;
            public static int StarhubSingapore525005 = 525005;
            public static int EtisalatUAE424002 = 424002;
            public static int TMobileGermany262001 = 262001;
            public static int TMobileGermany262006 = 262006;
            public static int TurkcellTurkey286001 = 286001;
            public static int Videotron302500 = 302500;
            public static int Videotron302510 = 302510;
            public static int BellTelus302880 = 302880;
        }

        public static class AppQueueType
        {
            public static int AppServerQueueArbitrator = 1; // not a queue,
            // but access it
            // in the same
            // fashion
            public static int TenantEventCounterQueue = 2;
            public static int GroupEventCounterQueue = 3;
            public static int UserEventCounterQueue = 4;
            public static int DeviceEventCounterQueue = 5;
            public static int TenantCounterQueue = 6;
            public static int GroupCounterQueue = 7;
            public static int UserCounterQueue = 8;
            public static int PublishAppPolicyQueue = 9;
            public static int ApplyGroupConfigQueue = 10;
            public static int iOSMessageQueue = 11;
            public static int CheckComplianceQueue = 12;
            public static int DeviceAlertQueue = 13;
            public static int PublishGroupConfigQueue = 14;
            public static int RemoveGroupQueue = 15;
            public static int DeviceCheckinQueue = 16;
            public static int ADComputationQueue = 17;
            //public static int MongoHelperCleaner = 18;
            public static int MQTTMessageQueue = 19;
            public static int MQTTBulkMessageQueue = 20;
            //split queues
            public static int DeviceAlertQueue1 = 22;
            public static int DeviceAlertQueue2 = 23;
            public static int DeviceAlertQueue3 = 24;

            public static int DeviceCheckinQueue1 = 25;
            public static int DeviceCheckinQueue2 = 26;
            public static int DeviceCheckinQueue3 = 27;

            public static int MQTTMessageQueue1 = 28;
            public static int MQTTMessageQueue2 = 29;
            public static int MQTTMessageQueue3 = 30;
            public static int CheckDeviceCheckinComplianceQueue = 31;
            public static int RefreshVppInfoQueue = 32;
        }

        public static class ObjectCounterAction
        {
            public static int Increasae = 1;
            public static int Decrease = 2;
            public static int Update = 3;
        }

        public static class QueueProcessStatus
        {
            public static int ToBeProcessed = 0;
            public static int Completed = 1;
            public static int InProgress = 2;
            public static int Error = 3;
        }

        public static class JobType
        {
            public static int PublishGroupConfig = 1;
            public static int PublishAppPolicy = 2;
            public static int ChangeGroup = 3;
            public static int RemoveGroup = 4;
            public static int ADComputation = 5;
        }

        public static class DataFolderPolicy
        {
            public static String EnableAccess = "Enable Access";
            public static String WipeOnJailbreak = "Wipe On Jailbreak";
            public static String AllowDownload = "Allow Download";
            public static String AllowOpenIn = "Allow Open-In";
            public static String AllowEmail = "Allow Email";
            public static String AllowPrint = "Allow Print";

        }

        public static class DailyEventType
        {
            public static int Tenant = 1;
            public static int Group = 2;
            public static int User = 3;
            public static int Device = 4;
        }

        public static int ADToolMinimumSupportedVersion = 1;
        public static int ADToolLatestVersion = 1;

        public static class ActiveDirectoryMode
        {
            public static int None = 0;
            public static int BulkImport = 1;
            public static int AdSync = 2;
        }

        public static class ActiveDirectoryModeSettings
        {
            public static String BulkImport = "Bulk Import";
            public static String AdSync = "Manual AD Sync";
        }

        public static class ActiveDirectoryJobType
        {
            public static int SyncGroup = 1;
            public static int SyncUser = 2;
        }

        public static class ActiveDirectoryOperationStatus
        {
            public static int Pending = 0;
            public static int Completed = 1;
            public static int Error = 2;
        }

        public static class ADToolCompatibilityToolAction
        {
            public static int None = 0;
            public static int UpgradeAvailable = 1;
            public static int RequiredUpgrade = 2;
        }

        public static class USER_STATUS
        {
            public static int NotExisted = 0;
            public static int ExistedInOtherTenant = 1;
            public static int ExistedSameTenantOnlyAdmin = 2;
            public static int ExistedSameTenantOnlyMobile = 3;
            public static int ExistedSameTenantBothRoles = 4;
        }

        public static class ActiveDirectoryReturnCode
        {
            public static int AD_CONFLICTED_USER = 600;
            public static int AD_ADMIN_ACTIVATION_NOT_ALLOWED = 601;
            public static int AD_USER_INFO_VALIDATION_ERROR = 602;
            public static int AD_SERVER_IS_BUSY = 603;
            public static int AD_OPTIMISTIC_LOCKING = 700;
            public static int AD_SESSION_TIME_OUT = 701;
            public static int AD_TENANT_BAD_LICENSE = 702;
            public static int AD_USER_NOT_ADMIN = 703;
            public static int AD_DATA_NOT_IN_SYNC = 704;
            public static int AD_SESSION_NOT_EXIST = 705;
            public static int AD_LICENSE_EXPIRED = 706;

            public static int AD_USER_ROOT_ADMIN = 707;
            public static int AD_CANNOT_DISABLE_LAST_ADMIN = 708;
            public static int AD_USER_ALREADY_EXIST_ANOTHER_TENANT = 709;
            public static int AD_USER_MANDATORY_FIELD_MISSING = 710;
            public static int AD_USER_SAME_EMAIL = 711;
            public static int AD_MAXIMUM_LICENSE_REACHED = 712;
            public static int AD_OVERRIDE_OFF_EXISTING_LOCAL_USER = 713;

            public static int AD_USER_EMAIL_NOT_VALID = 715;
            public static int AD_USER_PHONE_NUMBER_NOT_VALID = 716;
            public static int AD_USER_TITLE_NOT_VALID = 717;
            public static int AD_USER_UPN_NOT_VALID = 718;
            public static int AD_USER_FIRSTNAME_NOT_VALID = 719;
            public static int AD_USER_LASTNAME_NOT_VALID = 720;
            public static int AD_USER_FULLNAME_NOT_VALID = 721;

            public static int AD_UNKNOWN_ERROR = 730;

        }

        public static class ActiveDirectoryAccountStatus
        {
            public static String ACCOUNT_ACTIVE = "Active";
            public static String ACCOUNT_SUSPENDED = "Suspended";
        }

        public static class PermissionGroupName
        {
            public static String PERMISSION_GROUP_GLOBAL_ADMINISTRATOR = "Global Administrator";
            public static String PERMISSION_GROUP_GLOBAL_VIEWER = "Global Viewer";
            public static String PERMISSION_GROUP_MOBILE_USER = "Mobile User";
        }

        public static class PermissionGroupAction
        {
            public static int RevokePermissionGroup = 0;
            public static int AssignPermissionGroup = 1;
            public static int EditPermissionGroup = 2;
        }

        public static class UserDeviceDailyEventType
        {
            public static int DailyEvent = 1;
            public static int JobOrTask = 2;
            public static int Job = 3;
            public static int Task = 4;
        }

        public static class TENANT_TYPE
        {
            public static int Trial = 1;
            public static int Internal = 2;
            public static int Production = 3;
        }

        public static class TENANT_DERIVED_TYPE
        {
            public static int Trial = 1;
            public static int Internal = 2;
            public static int Starter = 4;
            public static int Professional = 5;
        }

        public static class WINDOWS_TYPE
        {
            public static int Program = 0;
            public static int Update = 1;
            public static int Application = 2;
        }

        public static String PC_AGENT_APPNAME = "Dell Management Portal";
        public static String CHECKOUT_PARAM_REASON = "Reason";
        public static String CHECKOUT_PARAM_INITIATOR = "Initiator";

        public static class CHECKOUT_REASON
        {
            public static int NotAvailable = 0;
            public static int Server = 1;
            public static int User = 2;
            public static int AppUninstall = 3;
        }

        public static long MEGABYTE = 1024L * 1024L;

        public static String MANAGED_APP_PARAM_NAME = "Name";
        public static String MANAGED_APP_PARAM_OSFAMILY = "OSFamily";
        public static String MANAGED_APP_PARAM_SOURCE = "Source";
        public static String MANAGED_APP_PARAM_ENTERPRISE_TYPE = "EnterpriseType";
        public static String MANAGED_APP_PARAM_LINK = "AppLink";
        public static String MANAGED_APP_PARAM_ACTION = "AppPolicyAction";
        public static String MANAGED_APP_PARAM_ALLOWBACKUP = "AllowBackup";
        public static String MANAGED_APP_PARAM_REMOVENOTMANAGED = "RemoveNotManaged";

        public static class AUDIT_COMMAND_NAME
        {
            public static String Unregister = "Unregister";
        }

        public static class BATTERY_STATUS_CODE
        {
            public static String[] label = {
			  "Invalid"	 				// 0
			, "Discharging" 			// 1
			, "Plugged In" 				// 2
			, "Charged" 				// 3
			, "Low" 					// 4
			, "Critical"				// 5
			, "Charging"				// 6
			, "Charging and High"		// 7
			, "Charging and Low"		// 8
			, "Charging and critical"	// 9
			, "Undefined"			    // 10
			, "Partially Charged"		// 11
			};
        }

        public static class TC_REPOSITORY_FILTER
        {
            public static String PARAMETER_GROUP_TYPE = "type";
            public static String FILE_NAME = "name";
            public static String FILE_VERSION = "version";
            public static String DESCRIPTION = "description";
            public static String UPLOADED_ON = "uploadedOn";
            public static String PLATFORM = "platform";
        }

        public static class DEVICE_IMAGE_FAMILY_TYPE
        {
            public static int R10_FAMILY = 1;
            public static int R50_FAMILY = 2;
            public static int R90_FAMILY = 3;
            public static int C10_FAMILY = 4;
            public static int C50_FAMILY = 5;
            public static int C90_FAMILY = 6;
            public static int T10_FAMILY = 7;
            public static int T50_FAMILY = 8;
            public static int T90_FAMILY = 9;
            public static int Z10_FAMILY = 10;
            public static int Z50_FAMILY = 11;
            public static int Z90_FAMILY = 12;
            public static int D10_FAMILY = 13;
            public static int D50_FAMILY = 14;
            public static int D90_FAMILY = 15;
            public static int Xenith_FAMILY = 16;
            public static int Xenith2_FAMILY = 17;
            public static int XenithPro_FAMILY = 18;
            public static int XenithPro2_FAMILY = 19;
            public static int D10DP_FAMILY = 20;
            public static int T10D_FAMILY = 21;
            public static int MapleLeaf_FAMILY = 22;
            public static int Xenith3_FAMILY = 23;
        }

        public static class DEVICE_REGISTRATION_STATUS
        {
            public static int Unknown = 0;
            public static int Initializing = 1;
            public static int RetrievingInfo = 2;
            public static int Completed = 3;

            // Helper funtion to display proper name when debugging
            public static String toString(int status)
            {
                if (status == DEVICE_REGISTRATION_STATUS.Initializing)
                {
                    return "Initializing";
                }
                else if (status == DEVICE_REGISTRATION_STATUS.RetrievingInfo)
                {
                    return "Retrieving Information";
                }
                else if (status == DEVICE_REGISTRATION_STATUS.Completed)
                {
                    return "Completed";
                }
                else if (status == DEVICE_REGISTRATION_STATUS.Unknown)
                {
                    return "Unknown";
                }
                else
                {
                    return "Unknown";
                }
            }
        }

        public static class TC_REMOTE_CONNECTION_TYPE
        {
            public static String[] label = {
			  "None"	// 0
			, "ICA" 	// 1
			, "RDP" 	// 2
			};
        }

        public static String NO_AUDIT_DESCRIPTION = "";

        public static class OnPremComponentNames
        {
            public static String CCMProxy = "CCMProxy";
            public static String SSOAuth = "SSOAuth";
            public static String ADSync = "ADSync";
            public static String Kace = "Kace";
        }

        public static class KeystoneStatus
        {
            public static int Active = 1;
            public static int AdminLocked = 2;
            public static int UserLocked = 3;
            public static int PolicyLocked = 4;
            public static int PendingPolicyUpdate = 5;
            public static int PinLocked = 6;

        }

        public static class KaceStatus
        {
            public static String Pending = "Pending";
            public static String Success = "Success";
            public static String Error = "Error";
            public static String ErrorTimedOut = "Error (TIMED OUT)";
            public static String ErrorBadKey = "Error (BAD PASSWORD)";
            public static String NoSyncRequired = "NoSyncRequired";
        }

        public static class KaceCommand
        {
            public static String SyncNow = "SyncNow";
        }

    }
}
