﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{
    public enum DWStatusCode
    {
        /**
         * Used to indicate success
         */
        DWStatus_SUCCESS = 0,

        /**
         * Usually accompanied by a debug message
         */
        DWStatus_FAIL = 5000,

        /**
         * Associated with asynchronous call
         */
        DWStatus_INPROGRESS = 5001,

        DWStatus_ERROR = 5002,
        DWStatus_TIMEOUT = 5003,
        DWStatus_AUTH_INVALID = 5004,
        DWStatus_RESPONSE_WRONG_FORMAT = 5005,
        DWStatus_SERVER_NOT_REACHABLE = 5006,
        DWStatus_FAILED_LIBRARY_NOT_INITIALIZED = 5007,
        DWStatus_FAILED_ALREADY_INITIALIZED = 5008,
        DWStatus_FAILED_MISSING_MANDATORY_PARAMETER = 5009,
        DWStatus_FAILED_NULL_PARAMETER = 5010,
        DWStatus_CALL_WAS_CANCELED = 5011, //
        DWStatus_CONNECTION_FAILED = 5012,
        DWStatus_NOT_DEFINED = 5013,
        DWStatus_ABORT = 5014,
        DWStatus_FOLDER_NOTFOUND = 5015,
        DWStatus_API_NOT_IMPLEMENTED = 5016,

        //download API failures
        /**
         * Operation not allowed due to security policy.
         */
        DWStatus_FAILED_NOT_PERMITTED_BY_POLICY = 5017,
        DWStatus_NOT_ENOUGH_DISK_SPACE = 5018,
        DWStatus_FILE_ALREADY_EXIST_ON_DEVICE = 5019,
        DWStatus_FILE_DOWNLOAD_ALREADY_EXIST_IN_PROGRESS = 5020,
        DWStatus_FAILED_MAXIMUM_SIMULTANEOUS_DOWNLOADS_REACHED = 5021,
        DWStatus_FAIL_PREVIEW_ALREADY_INPROGRESS = 5022,
        DWStatus_FILE_NOT_ON_DEVICE = 5023,
        DWStatus_FAIL_PROBLEM_DECRYPTING_FILE = 5024,
        DWStatus_REMOTE_APP_NOT_SUPPORTED = 5025,
        DWStatus_FAILED_DOWNLOAD_NOT_IN_PROGRESS = 5026,

        DWStatus_TENANT_EXPIRED = 6000,
        DWStatus_TENANT_DISABLED = 6001,
        DWStatus_USER_DEACTIVATED = 6002,
        DWStatus_LICENSE_LIMIT_REACHED = 6003,
        DWStatus_LICENSE_GROUP_LIMIT_REACHED = 6004,

        DWStatus_LAST_ERROR = 6005
    };
}
