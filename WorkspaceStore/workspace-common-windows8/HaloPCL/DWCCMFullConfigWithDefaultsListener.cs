﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{
    public interface DWCCMFullConfigWithDefaultsListener : DWListener
    {
        /**
         * Callback executed when
         * {@link DWAuth#getFullConfigWithDefaults(DWCCMFullConfigWithDefaultsListener)} has
         * completed execution.
         * 
         * @param[in] state Status code indicating command success or failure
         * @param[in] msg Code description
         * @return Returns 0 if successful.  Returns an error value otherwise.
         */

        int onGetFullConfigWithDefaultsCompleted(DWStatusCode state, String msg);
    }
}
