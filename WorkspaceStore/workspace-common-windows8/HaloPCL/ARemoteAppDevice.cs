﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{
    [DataContract]
    public class ARemoteAppDevice
    {
        [DataMember(Name = "currentStatus", EmitDefaultValue = true)]
        public int currentStatus { get; set; }

        [DataMember(Name = "appVersion", EmitDefaultValue = true)]
        public string appVersion { get; set; }

        [DataMember(Name = "isRemoteAppCompliant", EmitDefaultValue = true)]
        public bool isRemoteAppCompliant { get; set; }

    }
}
