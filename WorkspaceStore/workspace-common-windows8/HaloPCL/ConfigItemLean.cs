﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{
    [DataContract]
    public class ConfigurationItemLean
    {
#if R5_CCM
        [DataMember(Name = "itemValue")]
        public String ItemValue { get; set; }

        [DataMember(Name = "configurationParamCatalogType")]
        public int ConfigurationParamCatalogType { get; set; }

        [DataMember(Name = "configurationParamCatalogName")]
        public String ConfigurationParamCatalogName { get; set; }

        [DataMember(Name = "configurationParamCatalogVersion")]
        public String ConfigurationParamCatalogVersion { get; set; }

        [DataMember(Name = "itemRealValue")]
        public String ItemRealValue { get; set; }
        
        [DataMember(Name = "itemKey")]
        public String ItemKey { get; set; }

        [DataMember(Name = "valueType")]
        public int ValueType { get; set; }

        public void cloneFrom(ConfigurationItemLean item)
        {
            this.ItemValue = item.ItemValue;
            this.ItemRealValue  = item.ItemRealValue;
            this.ValueType = item.ValueType;
            this.ItemKey = item.ItemKey;
            this.ConfigurationParamCatalogName = item.ConfigurationParamCatalogName;
            this.ConfigurationParamCatalogType = item.ConfigurationParamCatalogType;
            this.ConfigurationParamCatalogVersion = item.ConfigurationParamCatalogVersion;
        }
	
        public String toString()
        {
            return String.Format("itemValue{0}\nitemRealValue{1}\nconfigurationParamCatalogType{2}\nconfigurationParamCatalogName{3}\nitemKey{4}", ItemValue, ItemRealValue, ConfigurationParamCatalogType, ConfigurationParamCatalogName, ItemKey);
        }
#else
        /// <summary>
        /// TODO What is this object.  Currently R7 CCM server returns null
        /// </summary>
        [DataMember(Name = "_id")]
        public object InternalId { get; set; }

        [DataMember(Name = "createdAt")]
        public DateTime? CreatedAt { get; set; }

        [DataMember(Name = "Id")]
        public int Id { get; set; }

        [DataMember(Name = "updatedAt")]
        public DateTime? UpdatedAt { get; set; }

        [DataMember(Name = "isActive")]
        public bool IsActive { get; set; }

        [DataMember(Name = "itemValue")]
        public String ItemValue { get; set; }

        [DataMember(Name = "configurationParamCatalogType")]
        public int ConfigurationParamCatalogType { get; set; }

        [DataMember(Name = "configurationParamCatalogName")]
        public String ConfigurationParamCatalogName { get; set; }

        [DataMember(Name = "configurationParamCatalogVersion")]
        public String ConfigurationParamCatalogVersion { get; set; }

        [DataMember(Name = "itemRealValue")]
        public String ItemRealValue { get; set; }

        [DataMember(Name = "itemKey")]
        public String ItemKey { get; set; }

        [DataMember(Name = "valueType")]
        public int ValueType { get; set; }

        public void cloneFrom(ConfigurationItemLean item)
        {
            this.ItemValue = item.ItemValue;
            this.ItemRealValue = item.ItemRealValue;
            this.ValueType = item.ValueType;
            this.ItemKey = item.ItemKey;
            this.ConfigurationParamCatalogName = item.ConfigurationParamCatalogName;
            this.ConfigurationParamCatalogType = item.ConfigurationParamCatalogType;
            this.ConfigurationParamCatalogVersion = item.ConfigurationParamCatalogVersion;
        }

        public String toString()
        {
            return String.Format("itemValue{0}\nitemRealValue{1}\nconfigurationParamCatalogType{2}\nconfigurationParamCatalogName{3}\nitemKey{4}", ItemValue, ItemRealValue, ConfigurationParamCatalogType, ConfigurationParamCatalogName, ItemKey);
        }
#endif
    }
}
