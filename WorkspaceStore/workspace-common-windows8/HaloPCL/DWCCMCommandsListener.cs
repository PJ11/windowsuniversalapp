﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{
    public interface DWCCMCommandsListener : DWListener
    {
        /**
         * This action will be triggered whenever the CCM OS module receives a Keystone profile update event.  This will happen when new or updated profiles are being published within CCM.<br>  
         * 
         * @param[in] jsonProfile JSON structure containing all updated profile(s).
         * @param[in] fullConfig if 1 then contains the full configuration otherwise only the delta(s).
         * @return Returns 0 if successful.  Returns an error value otherwise.
         *
         * @details For more details about the profile information being pushed to the Keystone application see in the example tab file onProfileUpdate.txt.
         */
        int onProfileUpdate(String jsonProfile, bool fullConfig);

        /**
         * This action will be triggered whenever the CCM OS module receives a remote command event from CCM. <br>
         * Currently supported command types are (see {@link DWAuth} for the following definition): <br>
         * 	1) CMD_KeystoneAdminLock <br>
         *  2) CMD_KeystoneUserLock <br>
         *  3) CMD_KeystoneUnlock <br>
         *  4) CMD_ResetPIN <br>
         *  5) CMD_WipeWorkspaceData <br>
         *  6) CMD_SendMessage <br>
         *  7) CMD_Query <br>
         * 
         * @see DWDataIn
         * @see DWDataOut
         * @param[in] type See list of supported command types above.
         * @param[in] dataIn A string.  This parameter can be used for specific types of message.  For example the message type "SendMessage" will use this parameter to pass the message string to display.
         * @return Returns DWDataOut.  DWDataOut.result: Returns 0 if successful.  Returns an error value otherwise.<br>  DWDataOut.jsonData: Related json data.
         */
        DWDataOut onCommand(int type, DWDataIn dataIn);

        /**
         * Unregister the Keystone application.  This action will be triggered whenever the CCM OS module receives an "unregister" event.<br>
         */
        void unregister();

        /**
         * Jailbreak detected .  This action will be triggered whenever the we detect a jailbreak condition.<br>
         * 
         * return 	- false if allow to work in jailbreak condition and no action will be taken, 
         * 			- true if app want to disconnect from CCM Server and a deauthenticate / checkout will be executed.
         *  if the application want not to return the control to Halo library then "deauthenticate()" should be call 
         *  in order to clean disconnected from CCM Server
         */
        bool onJailbreak();

    }
}
