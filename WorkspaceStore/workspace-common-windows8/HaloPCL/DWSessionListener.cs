﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{
    public interface DWSessionListener : DWListener
    {
        /**
         * Callback executed when
         * {@link DWAuth#getPocketCloudConnectionList(final DWSessionListener)} has
         * completed execution.
         * 
         * @param[in] state Status code indicating command success or failure
         * @param[in] json Payload containing the list of Pocket Cloud RDP connections (may be empty).  See in the example tab file pocketCloudConnection.txt.
         * @param[in] description Mainly for debugging purposes.
         */
        void onPocketCloudConnectionListResult(DWStatusCode state,
                String json, String description);
    }
}
