﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{
    [DataContract]
    public sealed class PersonInfoLean
    {
        [DataMember(Name = "firstName")]
        public string firstName { get; set; }
        [DataMember(Name = "LastName")]
        public string lastName { get; set; }
        [DataMember(Name = "phoneNumber")]
        public string phoneNumber { get; set; }
        [DataMember(Name = "title")]
        public string title { get; set; }
        [DataMember(Name = "loginName")]
        public string loginName { get; set; }
    }
}
