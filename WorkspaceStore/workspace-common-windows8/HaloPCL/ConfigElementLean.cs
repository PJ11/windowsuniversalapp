﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{
    [DataContract]
    public sealed class ConfigurationElementLean
    {
#if R5_CCM
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        [DataMember(Name = "IsActive")]
        public bool IsActive { get; set; }

        [DataMember(Name = "parameterGroupType")]
        public int ParameterGroupType { get; set; }

        [DataMember(Name = "parameterGroupVersion")]
        public String ParameterGroupVersion { get; set; }

        [DataMember(Name = "seqNumber")]
        public int SeqNumber { get; set; }

        [DataMember(Name = "configurationItems")]
        public List<ConfigurationItemLean> ConfigurationItems { get; set; }

        [DataMember(Name = "IsRemoved")]
        public bool IsRemoved { get; set; }

        [DataMember(Name = "childConfigElements")]
        public List<ConfigurationElementLean> ChildConfigElements { get; set; }

        public void cloneFrom(ConfigurationElementLean element)
        {
            this.Id = element.Id;
            this.ParameterGroupType = element.ParameterGroupType;
            this.ParameterGroupVersion = element.ParameterGroupVersion;
            this.SeqNumber = element.SeqNumber;
            this.IsActive = element.IsActive;
            this.IsRemoved = element.IsRemoved;
            this.ConfigurationItems = element.ConfigurationItems;
            this.ChildConfigElements = element.ChildConfigElements;
        }
#else

        [DataMember(Name = "parameterGroupType", EmitDefaultValue = true)]
        public int ParameterGroupType { get; set; }

        [DataMember(Name = "parameterGroupKey", EmitDefaultValue = true)]
        public String parameterGroupKey { get; set; }

        [DataMember(Name = "seqNumber", EmitDefaultValue = true)]
        public int SeqNumber { get; set; }

        [DataMember(Name = "isRemoved", EmitDefaultValue = true)]
        public bool IsRemoved { get; set; }

        [DataMember(Name = "parameterGroupVersion", EmitDefaultValue = true)]
        public String ParameterGroupVersion { get; set; }

        [DataMember(Name = "configurationItems", EmitDefaultValue = true)]
        public List<ConfigurationItemLean> ConfigurationItems { get; set; }



        //[DataMember(Name = "_id")]
        //public object _id { get; set; }

        //[DataMember(Name = "id")]
        //public int Id { get; set; }

        //[DataMember(Name = "updatedAt")]
        //public DateTime UpdatedAt { get; set; } 

        //[DataMember(Name = "isActive")]
        //public bool IsActive { get; set; }

        //[DataMember(Name = "parameterGroupType")]
        //public int ParameterGroupType { get; set; }

        //[DataMember(Name = "seqNumber")]
        //public int SeqNumber { get; set; }

        //[DataMember(Name = "isRemoved")]
        //public bool IsRemoved { get; set; }

        //[DataMember(Name = "parameterGroupVersion")]
        //public String ParameterGroupVersion { get; set; }

        ///// <summary>
        ///// TODO What is this object?
        ///// </summary>
        //[DataMember(Name = "parameterGroupKey")]
        //public object ParameterGroupKey { get; set; }

        //[DataMember(Name = "configurationItems")]
        //public List<ConfigurationItemLean> ConfigurationItems { get; set; }

        ///// <summary>
        ///// Version R7 (and possibly R6) of the CCM server returns this as EMPTY.
        ///// </summary>
        //[DataMember(Name = "childConfigElements")]
        //public List<ConfigurationElementLean> ChildConfigElements { get; set; }

        //public void cloneFrom(ConfigurationElementLean element)
        //{
        //    this.Id = element.Id;
        //    this.ParameterGroupType = element.ParameterGroupType;
        //    this.ParameterGroupVersion = element.ParameterGroupVersion;
        //    this.SeqNumber = element.SeqNumber;
        //    this.IsActive = element.IsActive;
        //    this.IsRemoved = element.IsRemoved;
        //    this.ConfigurationItems = element.ConfigurationItems;
        //    this.ChildConfigElements = element.ChildConfigElements;
        //}
#endif
    }
}
