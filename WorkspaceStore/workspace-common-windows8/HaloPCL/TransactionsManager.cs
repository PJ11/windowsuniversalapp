﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{
    public sealed class TransactionsManager
    {
        private static int mTransactionID = 0;
        private readonly static object syncLock = new object();

        public static int getNextID()
        {
            lock (syncLock)
            {
                return mTransactionID++;
            }
        }
    }
}
