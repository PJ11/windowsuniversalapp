﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace HaloPCL
{

    [DataContract]
    public class Parameter
    {
        [DataMember(Name = "name", EmitDefaultValue = true)]
        public String Name { get; set; }
        [DataMember(Name = "value", EmitDefaultValue = true)]
        public String Value { get; set; }

        //    public string toJSON()
        //    {
        //        JSONObject jo = new JSONObject();
        //        try
        //        {
        //            jo.put("id", this.Id);
        //            jo.put("name", this.getName());
        //            jo.put("value", this.getValue());
        //            jo.put("type", this.getType());
        //        }
        //        catch (Exception)
        //        {
        //        }
        //        return JSONObject.SerializeObject(jo);
        //    }
        public string toJSON()
        {
            MemoryStream stream1 = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Parameter));
            ser.WriteObject(stream1, this);
            stream1.Position = 0;
            StreamReader sr = new StreamReader(stream1);
            return sr.ReadToEnd();
        }
    }

    //public sealed class Parameter
    //{
    //    private String name;
    //    private String value;
    //    private int type;

    //    public Parameter()
    //    {
    //    }

    //    public Parameter(String name, String value)
    //    {
    //        this.name = name;
    //        this.value = value;
    //        this.type = 0;// TODO What is VALUE_TYPE.String?
    //    }

    //    public Parameter(String name, String value, int type)
    //    {
    //        this.name = name;
    //        this.value = value;
    //        this.type = type;
    //    }

    //    public long Id { get; set; }

    //    public String getName()
    //    {
    //        return name;
    //    }

    //    public void setName(String name)
    //    {
    //        this.name = name;
    //    }

    //    public String getValue()
    //    {
    //        return value;
    //    }

    //    public void setValue(String value)
    //    {
    //        this.value = value;
    //    }

    //    public int getType()
    //    {
    //        return type;
    //    }

    //    public void setType(int type)
    //    {
    //        this.type = type;
    //    }

    //    public void fromJSON(JSONObject jo)
    //    {
    //        this.Id = (int)jo["id"];
    //        this.setName(jo["name"] as string);
    //        this.setValue(jo["value"] as string);
    //        this.setType((int)jo["type"]);
    //    }

    //    public string toJSON()
    //    {
    //        JSONObject jo = new JSONObject();
    //        try
    //        {
    //            jo.put("id", this.Id);
    //            jo.put("name", this.getName());
    //            jo.put("value", this.getValue());
    //            jo.put("type", this.getType());
    //        }
    //        catch (Exception)
    //        {
    //        }
    //        return JSONObject.SerializeObject(jo);
    //    }

    //}

    //public class Tenant
    //{

    //}

    [DataContract]
    public sealed class DeviceCommand// : StratusType
    {
        [DataMember(Name = "id", EmitDefaultValue = true)]
        public int id { get; set; }

        [DataMember(Name = "priority", EmitDefaultValue = true)]
        public int Priority { get; set; }

        [DataMember(Name = "status", EmitDefaultValue = true)]
        public int status { get; set; }

        [DataMember(Name = "deviceAction", EmitDefaultValue = true)]
        public DeviceAction deviceAction { get; set; }

        [DataMember(Name = "parameters", EmitDefaultValue = true)]
        public List<Parameter> parameters { get; set; }

        public string toJSON()
        {
            MemoryStream stream1 = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(DeviceCommand));
            ser.WriteObject(stream1, this);
            stream1.Position = 0;
            StreamReader sr = new StreamReader(stream1);
            return sr.ReadToEnd();
        }

        public String toString(int i)
        {
            return String.Format("(id:{0} status:{1})", this.id, this.status);
        }

    }

    //public bool HasMoreCommand { get; set; }

    //public Tenant Tenant { get; set; }

    //public String Error { get; set; }

    //public DateTime RequestedOn { get; set; }

    //public DeviceAction DeviceAction { get; set; }

    //public int Priority { get; set; }

    //public int Status { get; set; }

    //public Device Device { get; set; }

    //public Task Task { get; set; }

    //public List<Parameter> Parameters { get; set; }

    //public bool IsGrouped { get; set; }

    //public bool IsGroup { get; set; }

    //public DeviceCommand AndParent { get; set; }

    //public DeviceCommand OrParent { get; set; }

    //public long Sequence { get; set; }

    //public void fromJSON(JSONObject jo)
    //{
    //    this.Id = (int)jo["id"];
    //    DeviceAction da = new DeviceAction();
    //    da.fromJSON(jo["deviceAction"] as string);
    //    this.DeviceAction = da;

    //    List<Parameter> parameters = new List<Parameter>();

    //    List<object> ja = JsonConvert.DeserializeObject <List<object>>(jo["parameters"] as string);

    //    if (ja != null)
    //    {
    //        for (int i = 0; i < ja.Count; ++i)
    //        {
    //            Parameter ap = new Parameter();
    //            JSONObject joo = ja[i] as JSONObject;
    //            if (joo != null)
    //            {
    //                ap.fromJSON(joo);
    //                parameters.Add(ap);
    //            }
    //        }
    //    }
    //    this.Parameters = parameters;


    //    //		{
    //    //		    "orParent": null,
    //    //		    "isGrouped": false,
    //    //		    "id": 1,
    //    //		    "deviceAction": {
    //    //		        "id": 8,
    //    //		        "deviceTypes": [],
    //    //		        "registrationPriority": 100,
    //    //		        "priority": 20,
    //    //		        "description": null,
    //    //		        "name": "DeviceInformation",
    //    //		        "isAutoSuccess": false,
    //    //		        "type": 9
    //    //		    },
    //    //		    "error": null,
    //    //		    "status": 3,
    //    //		    "priority": 20,
    //    //		    "task": null,
    //    //		    "isGroup": false,
    //    //		    "device": {
    //    //		        "networkType": null,
    //    //		        "iTunesStoreAccountIsActive": 0,
    //    //		        "iccid": null,
    //    //		        "macAddress": "a0:0b:ba:cd:6b:dc_13",
    //    //		        "osBuildNumber": null,
    //    //		        "deviceTypeDesc": "Keystone Android",
    //    //		        "isJailBroken": false,
    //    //		        "lastOwnerChangedTime": null,
    //    //		        "lastHeartbeatTime": null,
    //    //		        "installedAppCompliantStatus": 1,
    //    //		        "authenticationCode": "VrenIFaPfn6JkE0TtTWEtg==",
    //    //		        "topic": null,
    //    //		        "isInstalledAppCompliant": true,
    //    //		        "windowsSecurity": null,
    //    //		        "blueToothMacAddress": null,
    //    //		        "imei": null,
    //    //		        "updateStatus": null,
    //    //		        "networkAuthentication": null,
    //    //		        "osVersionDesc": "",
    //    //		        "osServicePack": null,
    //    //		        "isCheckinCompliant": true,
    //    //		        "isPasscodePresent": true,
    //    //		        "agentPackage": null,
    //    //		        "isActive": true,
    //    //		        "configCompliantStatus": 1,
    //    //		        "dayNum": 15960,
    //    //		        "isDeviceLocatorServiceEnabled": 0,
    //    //		        "ownership": 0,
    //    //		        "compliantStatus": 1,
    //    //		        "currentMcc": null,
    //    //		        "nonCompliantConfigParamCatalogs": [],
    //    //		        "lastLoggedInUser": null,
    //    //		        "wifis": null,
    //    //		        "osBuildCodes": null,
    //    //		        "registrationStatus": 3,
    //    //		        "carrierSettingsVersion": null,
    //    //		        "mailAccounts": null,
    //    //		        "lastUpdateCheck": null,
    //    //		        "simMnc": null,
    //    //		        "keyboardLayout": null,
    //    //		        "modelNum": null,
    //    //		        "contact": null,
    //    //		        "isOnline": false,
    //    //		        "token": null,
    //    //		        "deviceType": {
    //    //		            "id": 11,
    //    //		            "parent": null,
    //    //		            "type": 13,
    //    //		            "family": 12,
    //    //		            "isAssignable": false,
    //    //		            "description": "Keystone Android",
    //    //		            "name": "Keystone Android"
    //    //		        },
    //    //		        "osArchitecture": null,
    //    //		        "domain": null,
    //    //		        "osBuildVersion": null,
    //    //		        "checkinInterval": 0,
    //    //		        "deviceCurrentSetting": null,
    //    //		        "udid2": null,
    //    //		        "lastConfigTime": null,
    //    //		        "cpuFamily": null,
    //    //		        "personalHotspotEnabled": 0,
    //    //		        "cpuBusy": null,
    //    //		        "wiFiMacAddress": null,
    //    //		        "site": {
    //    //		            "id": 3,
    //    //		            "name": "Default Site"
    //    //		        },
    //    //		        "tags": null,
    //    //		        "isAssigned": false,
    //    //		        "islocAllow": false,
    //    //		        "vendor": null,
    //    //		        "deviceUsage": null,
    //    //		        "deviceProfiles": [],
    //    //		        "flashSize": 0,
    //    //		        "deviceOsType": null,
    //    //		        "networkInterfaces": null,
    //    //		        "packageInfo": null,
    //    //		        "isDoNotDisturbInEffect": 0,
    //    //		        "notes": null,
    //    //		        "custom1": null,
    //    //		        "custom2": null,
    //    //		        "custom3": null,
    //    //		        "isConfigCompliant": true,
    //    //		        "storages": null,
    //    //		        "deviceCertificates": [],
    //    //		        "isVoiceRoamingEnabled": true,
    //    //		        "deviceLocation": null,
    //    //		        "windowsFirewall": null,
    //    //		        "pushMagic": null,
    //    //		        "phoneNum": null,
    //    //		        "timezone": 0,
    //    //		        "simCarrierNetwork": null,
    //    //		        "osCaption": null,
    //    //		        "ownedBy": "Montreal",
    //    //		        "remoteAppDevice": {
    //    //		            "id": 1,
    //    //		            "registrationTime": 1379001947000,
    //    //		            "deviceType": {
    //    //		                "id": 11,
    //    //		                "parent": null,
    //    //		                "type": 13,
    //    //		                "family": 12,
    //    //		                "isAssignable": false,
    //    //		                "description": "Keystone Android",
    //    //		                "name": "Keystone Android"
    //    //		            },
    //    //		            "name": "Galaxy Nexus",
    //    //		            "device": null,
    //    //		            "udid": null,
    //    //		            "macAddress": "a0:0b:ba:cd:6b:dc",
    //    //		            "lastCheckinTime": 1379001947000
    //    //		        },
    //    //		        "serialNum": null,
    //    //		        "lastCheckinTime": null,
    //    //		        "currentCarrierNetwork": null,
    //    //		        "isUsingLatestFirmware": false,
    //    //		        "harewareEncryptionCaps": 0,
    //    //		        "tanalyticsToken": null,
    //    //		        "gmailUser": null,
    //    //		        "isPasscodeCompliantWithProfiles": true,
    //    //		        "modelName": null,
    //    //		        "owner": {
    //    //		            "registrationPassword": "d7d39D/6u8o3tTKF",
    //    //		            "popUser": "",
    //    //		            "imapUser": "",
    //    //		            "password": null,
    //    //		            "adDn": null,
    //    //		            "isManaged": false,
    //    //		            "adUPN": null,
    //    //		            "userType": null,
    //    //		            "permissionGroups": [
    //    //		                {
    //    //		                    "id": 7,
    //    //		                    "description": "Self Service",
    //    //		                    "name": "SelfService"
    //    //		                }
    //    //		            ],
    //    //		            "id": 8,
    //    //		            "phoneNum": "",
    //    //		            "title": "",
    //    //		            "isSelfServiceAgreementAccepted": false,
    //    //		            "isDefault": false,
    //    //		            "createdAt": 1379001852000,
    //    //		            "adGuid": null,
    //    //		            "isPortalNotificationAck": false,
    //    //		            "isCompliant": false,
    //    //		            "personType": "SelfService",
    //    //		            "firstName": "Ovidiu",
    //    //		            "exchangeUser": "",
    //    //		            "loginName": "1@1.com",
    //    //		            "lastName": "Gafencu",
    //    //		            "profileIcon": "/resources/img/nobody.png",
    //    //		            "remoteAppUsers": null,
    //    //		            "configurationProfile": null,
    //    //		            "pwExpirationDate": null,
    //    //		            "imapEmail": "",
    //    //		            "isRoot": false,
    //    //		            "origin": 0,
    //    //		            "emailAddress": "1@1.com",
    //    //		            "popEmail": "",
    //    //		            "isActive": true,
    //    //		            "useCustomPassword": true,
    //    //		            "dayNum": 15960,
    //    //		            "confirmPassword": null,
    //    //		            "isPasswordChangeForced": false,
    //    //		            "userGroup": {
    //    //		                "configurationPublishPendingOrError": false,
    //    //		                "remoteWipe": true,
    //    //		                "query": true,
    //    //		                "addRemoveConfigurationProfile": true,
    //    //		                "type": 2,
    //    //		                "adDn": null,
    //    //		                "id": 5,
    //    //		                "defaultUser": null,
    //    //		                "groupToken": "montmontmont",
    //    //		                "adGuid": null,
    //    //		                "profileId": "com.Montreal",
    //    //		                "description": "All users and devices are managed under this profile",
    //    //		                "name": "Default Policy Group",
    //    //		                "allowGroupToken": true,
    //    //		                "isActiveDirectoryGroup": false,
    //    //		                "configurationPublishOnQueue": false,
    //    //		                "configurationProfile": null,
    //    //		                "addRemoveApps": true,
    //    //		                "voiceDataRoaming": true,
    //    //		                "parent": null,
    //    //		                "addRemoveProvisioningProfile": true,
    //    //		                "isActive": true,
    //    //		                "dayNum": 0,
    //    //		                "accessRights": 8191,
    //    //		                "useGroupPassword": false,
    //    //		                "generateRandomPassword": true,
    //    //		                "groupPassword": null,
    //    //		                "clearPasscode": true
    //    //		            },
    //    //		            "compliantStatus": 0,
    //    //		            "changePassword": false,
    //    //		            "notes": null,
    //    //		            "generateRandomPassword": false,
    //    //		            "exchangeEmail": ""
    //    //		        },
    //    //		        "isRoaming": false,
    //    //		        "bootImageUrl": null,
    //    //		        "modemFirmwareVersion": null,
    //    //		        "devicePlatformType": null,
    //    //		        "isDataRoamingEnabled": true,
    //    //		        "ccmAgentVersion": null,
    //    //		        "simMcc": null,
    //    //		        "currentlyLoggedInUser": null,
    //    //		        "locale": null,
    //    //		        "isEmployeeOwned": false,
    //    //		        "hasAlerts": false,
    //    //		        "deviceProvisioningProfiles": [],
    //    //		        "screenSaverTimer": null,
    //    //		        "id": 1,
    //    //		        "wyseIdentifier": "wyse5120003478562365354",
    //    //		        "meid": null,
    //    //		        "name": "Galaxy Nexus",
    //    //		        "subscriberId": null,
    //    //		        "isCompliant": true,
    //    //		        "agentLocation": null,
    //    //		        "isPasscodeCompliant": true,
    //    //		        "registrationTime": 1379001947000,
    //    //		        "deviceSignal": null,
    //    //		        "uptime": null,
    //    //		        "deviceStatus": {
    //    //		            "type": 1,
    //    //		            "id": 5,
    //    //		            "description": "Registered",
    //    //		            "name": "Registered"
    //    //		        },
    //    //		        "phoneDeviceId": null,
    //    //		        "udid": "null_13",
    //    //		        "ip": null,
    //    //		        "connectionTypeDesc": "",
    //    //		        "antivirus": null,
    //    //		        "cellularTechnology": 0,
    //    //		        "hardwareSummary": null,
    //    //		        "tagList": null,
    //    //		        "productName": null,
    //    //		        "timezoneName": null,
    //    //		        "currentMnc": null,
    //    //		        "unlockToken": null
    //    //		    },
    //    //		    "andParent": null,
    //    //		    "parameters": [],
    //    //		    "requestedOn": 1379005059000
    //    //		}		


    //}

    //bool isExecuted = true;

    //public void setIsExecuted(bool b)
    //{
    //    isExecuted = b;
    //}

    //public String toJSON() 
    //{
    //    try 
    //    {
    //        JSONObject jo = new JSONObject();
    //        jo.put("id", this.Id);
    //        jo.put("status", this.Status);

    //        List<Parameter> parameters = this.Parameters;

    //        if (parameters != null)
    //        {
    //            JSONArray ja = new JSONArray();

    //            foreach(Parameter pItem in parameters )
    //            {
    //                Parameter ap = (Parameter)pItem;
    //                ja.put(ap.toJSON());
    //            }
    //            jo.put("parameters", ja);	
    //        }
    //        return JSONObject.SerializeObject(jo);
    //    } 
    //    catch (Exception)
    //    {
    //    }
    //    return "{}";
    //}

    //public String toString(int i)
    //{
    //    return String.Format("(id:{0} status:{1})", this.id, this.status);
    //}
    //}
}
