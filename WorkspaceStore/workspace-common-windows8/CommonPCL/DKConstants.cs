﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonPCL
{
   public class DKConstants
    {

        // Boolen to allowdevelopers to include if statements around DKDebug (Note a false static boolean will cause Java compiler to remove the statements.
        public static bool DKDebug = false;
        public static bool ProdServer = false;
        private static readonly String TAG = "DKConstants";
        /*
         * HockeyApp Information
         */
        public static String HOCKEY_APP_ID = "5f7075e66ec26648c541148c0425184a";


        /*
        * Flurry Analytics key
        */
        public static String FLURRY_ANALYTICS_KEY = "VJVJCQJZ4XQD3GRHM4PK";

        /**
         * Server Configuration Stuff for
         * The list of servers can be modified in ccm_servers.xml in Common Resources
         */


        // Used by the application.
        //public static String URL_KEYSTONE_CONFIG_SERVER = "https://sk1.wyselab.com";
        //public static String URL_KEYSTONE_PUSH_SERVER = "sk1-pns.wyselab.com";
        public static int URL_PUSH_SERVER_PORT = 1883;

        public static int DEFAULT_INACTIVITY_TIMEOUT = 5;

        public static readonly String PREFS_HOCKEY_USER_NAME = "HockeyUserName";
        public static readonly String PREFS_WTF_Crash = "KeystoneWTF";
        public static readonly String PREF_LOGIN_MODE = "LoginMode";
        public static readonly String PREF_CCM_MSG = "CCMMsg";
        public static readonly String LOGIN_MODE_FIRST_RUN = "true";

        public static readonly String PREFERENCE_IS_CAL_SYNCABLE = "is_cal_syncable";
        public static readonly String PREFERENCE_IS_EMAIL_SYNCABLE = "is_email_syncable";
        public static readonly String PREFERENCE_IS_CONTACTS_SYNCABLE = "is_contacts_syncable";

        public static String PACKAGE_NAME;

        public static bool syncCalendar = false;
        public static bool syncContacts = true;

        public static readonly String CALENDAR_DATABASE = "dkCalendar.db";
        public static readonly String CALENDAR_CACHE_DATABASE = "dkCalendarCache.db";
        public static readonly String EMAIL_DATABASE = "dkEmailProvider.db";
        public static readonly String EMAIL_BACKUP_DATABASE = "dkEmailProviderBackup.db";
        public static readonly String EMAIL_BODY_DATABASE = "dkEmailBody.db";
        public static readonly String CONTACTS_DATABASE = "dkContacts.db";
        public static readonly String SETTINGS_DATABASE = "dkSettings.db";
        public static readonly String DRAG_DROP_DATABASE = "dkDragDrop.db";
        public static readonly String DK_POLICY_SHARED_PREFS_NAME = "DKPolicyChangeChecker";
        public static readonly String BOOKMARKS_DATABASE = "DKBookmarks.db";
        public static readonly String BROWSER_DOWNLOADS_DATABASE = "DKBrowserDownloads.db";
        public static readonly String BROWSER_HISTORY_DATABASE = "DKBrowserHistory.db";

        public static bool isTablet = false;
        public static readonly String BROWSER_USER_CERT_FILE_NAME = "browserusercert.pfx";

        public static readonly String POLARIS_SDK_EDIT_LICENSE_KEY = "30f6b655-c965-4ffd-a7d2-d7fe0d7b953b";
        public static readonly String POLARIS_SDK_VIEW_LICENSE_KEY = "e1e9032c-70ae-4f5a-ad26-8d0dfcb974d1";

        public static readonly String DK_EAS_DISPLAY_NAME = "Workspace";
        public static void init(String hockeyAppId, bool isDebug)
        {
            //  PACKAGE_NAME = context.getPackageName();
            HOCKEY_APP_ID = hockeyAppId;

            // isTablet = context.getResources().getBoolean(R.bool.isTablet);
#if LOREN_COMMENTED_OUT
        if (isDebug) {
            URL_KEYSTONE_CONFIG_SERVER = "https://sk1.wyselab.com";
            URL_KEYSTONE_PUSH_SERVER = "sk1-pns.wyselab.com";
            DKDebug = isDebug;
        }
        if (ProdServer) {
            URL_KEYSTONE_CONFIG_SERVER = "https://us1.cloudclientmanager.com";
            URL_KEYSTONE_PUSH_SERVER = "us1-pns.cloudclientmanager.com";
            DKDebug = isDebug;
        }
#endif
        }


        // Exchange Errors to be sent through DKAppState
        public static int DK_EXCHANGE_USER_CREDS_ERROR = 101;
        public static int DK_EXCHANGE_DEVICES_LIMIT = 102;
    }
}
