﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AbstractsPCL;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace CommonPCL
{
    public enum CheckinExpirationAction
    {
        CHECKIN_EXPIRATION_WIPE = -2, // kCCMWipeCode in iOS
        CHECKIN_EXPIRATION_LOCK = -1  // kCCMLockCode in iOS
    };

    public enum LoginFailureAction
    {
        LOGIN_FAILURE_WIPE = -2, // kCCMWipeCode in iOS
        LOGIN_FAILURE_LOCK = -1, // kCCMLockCode in iOS
        LOGIN_FAILURE_TO1 = 1,   // Timeout for 1 minute
        LOGIN_FAILURE_TO2 = 2,   // Timeout for 2 minute
        LOGIN_FAILURE_TO3 = 3,   // Timeout for 3 minute
        LOGIN_FAILURE_TO4 = 4,   // Timeout for 4 minute
        LOGIN_FAILURE_TO5 = 5,   // Timeout for 5 minute
    };

    public enum DKCNMailDays
    {
        //-1 is not accepted value in AS request
        //MailDaysNotSet = -1,
        MailDaysNoLimit = 0,
        MailDaysOneDay,
        MailDaysThreeDays,
        MailDaysOneWeek,
        MailDaysTwoWeeks,
        MailDaysOneMonth,       
    };

    public enum DKCNMailFetchFrequency
    {
        MailFetchNotSet = -1,
        MailPush = 0,
        MainManual,
        MailFetchEveryFiveMinutes = 5,
        MailFetchEveryTenMinutes = 10,
        MailFetchEveryFifteenMinutes = 15,
        MailFetchEveryThirtyMinutes = 30,
        MailFetchEveryHour = 60,
        MailFetchEveryTwoHours = 120,
        MailFetchEveryFourHours = 240
    };

    public enum DKCNMailNotificationType
    {
        //MailNotSet = -1,
        MailHighPriority = 0,
        MailAllNew,//selected by default
        MailNone
    }

    public enum DKCNContactsSortBy
    {
        FirstName = 0,
        LastName,
    }

    public enum DKCNContactsViewNamesBy
    {
        FirstNameFirst = 0,
        LastNameFirst,
    }
    public enum DKCNCalendarDays
    {
        CalendarDaysNotSet = 0,       
        CalendarDaysTwoWeeksBack,
        CalendarDaysOneMonthBack,
        CalendarDaysThreeMonthsBack,
        CalendarDaysSixMonthsBack
    };

    public enum DKCNContactDisplayNameOrder
    {
        DKCNContactDisplayNameOrderNotSet = -1,
        DKCNContactDisplayNameOrderFirst,
        DKCNContactDisplayNameOrderLast,
    };

    public enum DKCOSortOrder
    {
        DKCOSortOrderNotSet = -1,
        DKCOSortOrderFirst = 0,
        DKCOSortOrderLast,
    };

    public enum DKCNCalendarNotificationSound
    {
        CalendarNotificationSoundNotSet = -1,
        CalendarNotificationSoundDefault = 0,
        CalendarNotificationSoundAdara,
        CalendarNotificationSoundAlya,
        CalendarNotificationSoundCapella,
        CalendarNotificationSoundCellAopha
    };

    [DataContract]
    public class DKCNSettings
    {
        [DataMember(Name = "Settings")]
        public Dictionary<string, string> Settings { get; set; }
        
        public DKCNSettings(DKCNSettings settings)
        {
            this.Apps = settings.Apps;
            this.Bookmarks = settings.Bookmarks;
            this.BookmarkString = settings.BookmarkString;
            this.CalendarAppGranted = settings.CalendarAppGranted;
            this.CalendarNotificationSound = settings.CalendarNotificationSound;
            this.CalendarNotifyOn = settings.CalendarNotifyOn;
            this.CalendarSyncWindow = settings.CalendarSyncWindow;
            this.CertData = settings.CertData;
            this.CertString = settings.CertString;
            this.ChecinExpiration = settings.ChecinExpiration;
            this.CheckinExpirationAction = settings.CheckinExpirationAction;
            this.ContactDisplaySortOrder = settings.ContactDisplaySortOrder;
            this.ContactsAppGranted = settings.ContactsAppGranted;
            this.ContactSortOrder = settings.ContactSortOrder;
            this.CopyPasteEnabled = settings.CopyPasteEnabled;
            this.DefaultHomePage = settings.DefaultHomePage;
            this.DynamicUserInfo = settings.DynamicUserInfo;
            this.EasDomain = settings.EasDomain;
            this.EasEmailAddress = settings.EasEmailAddress;
            this.EasHost = settings.EasHost;
            this.EasPassword = settings.EasPassword;
            this.EasUserName = settings.EasUserName;
            this.EmailAddress = settings.EmailAddress;
            this.EmailAppGranted = settings.EmailAppGranted;
            this.EmailPassword = settings.EmailPassword;
            this.EmailPush = settings.EmailPush;
            this.EmailSignature = settings.EmailSignature;
            this.EmailServer = settings.EmailServer;
            this.ExternalSharingEnabled = settings.ExternalSharingEnabled;
            this.FileAppGranted = settings.FileAppGranted;
            this.FoldersToSync = settings.FoldersToSync;
            this.InactivityTimeout = settings.InactivityTimeout;
            this.CCMInactivityTimeout = settings.CCMInactivityTimeout;
            this.IsSSOUser = settings.IsSSOUser;
            this.IsSystemTimeZone = settings.IsSystemTimeZone;
            this.JailbrokenDeviceAllowed = settings.JailbrokenDeviceAllowed;
            this.LastAppIndex = settings.LastAppIndex;
            this.LoginAttemptsAllowed = settings.LoginAttemptsAllowed;
            this.LoginFailureAction = settings.LoginFailureAction;
            this.MailDays = settings.MailDays;
            this.MailFetchFrequency = settings.MailFetchFrequency;
            this.MailNotificationType = settings.MailNotificationType;
            this.Message = settings.Message;
            this.PinLength = settings.PinLength;
            this.TenantKey = settings.TenantKey;
            this.UserDisplayName = settings.UserDisplayName;
            this.UserFirstName = settings.UserFirstName;
            this.UserLastName = settings.UserLastName;
            this.UserName = settings.UserName;
            this.UserPhoneNumber = settings.UserPhoneNumber;
            this.UserTitle = settings.UserTitle;
            this.UseSSL = settings.UseSSL;
            this.WebAppGranted = settings.WebAppGranted;
            this.WebProxyIP = settings.WebProxyIP;
            this.WebProxyPort = settings.WebProxyPort;
            this.WebProxyRequired = settings.WebProxyRequired;
        }

        public DKCNSettings(string json)
        {
            parsePolicy(json);
        }

        public DKCNSettings()
        {
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <summary>
        /// We could receive a json of "updates" only
        /// </summary>
        /// <param name="policy"></param>
        protected void parsePolicy(String policy)
        {
            Settings = new Dictionary<string, string>();
            try
            {
                var obj = JArray.Parse(policy);

                for (int i = 0; i < obj.Count; i++ )
                {
                    var type = obj[i]["type"];
                    if(null != type)
                    {
                        int numericType = Convert.ToInt32(type.ToString());
                        switch (numericType)
                        {
                            case 701:
                                Settings.Add(HaloPolicySettingsAbstract.KEY_INACTIVITY_TIMEOUT, obj[i]["items"][HaloPolicySettingsAbstract.KEY_INACTIVITY_TIMEOUT].ToObject<int>().ToString());
                                Settings.Add(HaloPolicySettingsAbstract.CCM_INACTIVITY_TIMEOUT, obj[i]["items"][HaloPolicySettingsAbstract.KEY_INACTIVITY_TIMEOUT].ToObject<int>().ToString());
                                Settings.Add(HaloPolicySettingsAbstract.KEY_PIN_LENGTH, obj[i]["items"][HaloPolicySettingsAbstract.KEY_PIN_LENGTH].ToObject<int>().ToString());
                                Settings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_COPY_PASTE, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ENABLE_COPY_PASTE].ToObject<bool>().ToString());
                                Settings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_EXTERNAL_SHARING, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ENABLE_EXTERNAL_SHARING].ToObject<bool>().ToString());
                                Settings.Add(HaloPolicySettingsAbstract.KEY_ALLOW_JAILBREAK, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ALLOW_JAILBREAK].ToObject<bool>().ToString());
                                Settings.Add(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION, obj[i]["items"][HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION].ToObject<int>().ToString());
                                Settings.Add(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE, obj[i]["items"][HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE].ToObject<int>().ToString());
                                Settings.Add(HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION, obj[i]["items"][HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION].ToObject<int>().ToString());
                                //PJ. this setting is not available in SK1. We need to enforce null check before accessing obj.
                                //Settings.Add(HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION_ACTION, obj[i]["items"][HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION_ACTION].ToObject<int>().ToString());
                                break;
                            case 703:
                                if(obj[i]["items"] != null)
                                {
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_CALENDAR, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ENABLE_CALENDAR].ToObject<bool>().ToString());
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_EMAIL, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ENABLE_EMAIL].ToObject<bool>().ToString());
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_EMAIL_SYNC_FREQUENCY, obj[i]["items"][HaloPolicySettingsAbstract.KEY_EMAIL_SYNC_FREQUENCY].ToObject<int>().ToString());
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_WEBBROWSER, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ENABLE_WEBBROWSER].ToObject<bool>().ToString());
                                    bool webBrowserProxyRequired = obj[i]["items"][HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_REQUIRED].ToObject<bool>();
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_REQUIRED, webBrowserProxyRequired.ToString());
                                    if (webBrowserProxyRequired)
                                    {
                                        Settings.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_CERTIFICATE, obj[i]["items"][HaloPolicySettingsAbstract.KEY_WEBBROWSER_CERTIFICATE].ToObject<string>());
                                        Settings.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_IP, obj[i]["items"][HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_IP].ToObject<string>());
                                        Settings.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_PORT, obj[i]["items"][HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_PORT].ToObject<int>().ToString());
                                    }

                                    Settings.Add(HaloPolicySettingsAbstract.KEY_EMAIL_SIGNATURE, obj[i]["items"][HaloPolicySettingsAbstract.KEY_EMAIL_SIGNATURE].ToObject<string>());
                                    //PJ. this setting is not available in SK1. We need to enforce null check before accessing obj.
                                    //Settings.Add(HaloPolicySettingsAbstract.KEY_DEFAULT_HOMEPAGE, obj[i]["items"][HaloPolicySettingsAbstract.KEY_DEFAULT_HOMEPAGE].ToObject<string>());
                                    if (obj[i]["items"][HaloPolicySettingsAbstract.KEY_DEFAULT_HOMEPAGE] != null)
                                    {
                                        Settings.Add(HaloPolicySettingsAbstract.KEY_DEFAULT_HOMEPAGE, obj[i]["items"][HaloPolicySettingsAbstract.KEY_DEFAULT_HOMEPAGE].ToObject<string>());
                                    }
                                    else
                                    {
                                        Settings.Add(HaloPolicySettingsAbstract.KEY_DEFAULT_HOMEPAGE, "");
                                    }
                                    // Get Bookmarks in there
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_BOOKMARK, obj[i]["items"][HaloPolicySettingsAbstract.KEY_WEBBROWSER_BOOKMARK].ToObject<string>());

                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_CONTACTS, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ENABLE_CONTACTS].ToObject<bool>().ToString());
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_FILEBROWSER, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ENABLE_FILEBROWSER].ToObject<bool>().ToString());
                                }
                                else
                                {
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_CALENDAR, "1");
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_EMAIL, "1");
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_EMAIL_SYNC_FREQUENCY, DKCNMailFetchFrequency.MailFetchNotSet.ToString());
                                    bool webBrowserProxyRequired = false;

                                    Settings.Add(HaloPolicySettingsAbstract.KEY_EMAIL_SIGNATURE, "");
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_DEFAULT_HOMEPAGE, "");

                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_CONTACTS, "1");
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_FILEBROWSER, "1");
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_WEBBROWSER, "1");
                                }
                                break;
                            case 705:
                                if (null != obj[i]["items"])
                                {
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_HOST, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_HOST].ToObject<string>());
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_EMAILADDRESS, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_EMAILADDRESS].ToObject<string>());
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_USERNAME, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_USERNAME].ToObject<string>());
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DOMAIN, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DOMAIN].ToObject<string>());
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_SSLREQUIRED, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_SSLREQUIRED].ToObject<bool>().ToString());
                                    // CCM R7 seems to be missing AGENTIDENTIFIER:  Settings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_AGENTIDENTIFIER, obj[2]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_AGENTIDENTIFIER].ToObject<string>());
                                    bool dynamic = obj[i]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DYNAMICUSERINFO].ToObject<bool>();
                                    Settings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DYNAMICUSERINFO, dynamic.ToString());
                                    if (!dynamic)
                                    {
                                        // Local user
                                        Settings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_PASSWORD, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_PASSWORD].ToObject<string>());
                                        Settings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_ADUPN, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_ADUPN].ToObject<bool>().ToString());
                                    }
                                    //it was throwing exception due to null value
                                    if (obj[i]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_AGENTIDENTIFIER] != null)
                                    {
                                        Settings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_AGENTIDENTIFIER, obj[i]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_AGENTIDENTIFIER].ToObject<string>());
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        // This is personInfoLean
                        Settings.Add(HaloPolicySettingsAbstract.KEY_USER_FIRST_NAME, obj[i]["personInfoLean"][HaloPolicySettingsAbstract.KEY_USER_FIRST_NAME].ToObject<string>());
                        Settings.Add(HaloPolicySettingsAbstract.KEY_USER_LAST_NAME, obj[i]["personInfoLean"][HaloPolicySettingsAbstract.KEY_USER_LAST_NAME].ToObject<string>());
                        Settings.Add(HaloPolicySettingsAbstract.KEY_USER_PHONE, obj[i]["personInfoLean"][HaloPolicySettingsAbstract.KEY_USER_PHONE].ToObject<string>());
                        Settings.Add(HaloPolicySettingsAbstract.KEY_USER_TITLE, obj[i]["personInfoLean"][HaloPolicySettingsAbstract.KEY_USER_TITLE].ToObject<string>());
                    }
                }
            }
            catch (Exception e)
            {
                string msg = e.Message;
                throw new MalformedPolicyException(msg);
            }
        }
        public bool IsValid
        {
            get
            {
                return EasHost != null && EasEmailAddress != null && EasUserName != null;
            }
        }

        #region Properties
        public int InactivityTimeout
        {
            get
            {
                return Convert.ToInt32(Settings[HaloPolicySettingsAbstract.KEY_INACTIVITY_TIMEOUT]);
            }

            set
            {
                Settings[HaloPolicySettingsAbstract.KEY_INACTIVITY_TIMEOUT] = value.ToString();

            }
        }
        public int CCMInactivityTimeout
        {
            get
            {
                return Convert.ToInt32(Settings[HaloPolicySettingsAbstract.CCM_INACTIVITY_TIMEOUT]);
            }

            set
            {
                Settings[HaloPolicySettingsAbstract.CCM_INACTIVITY_TIMEOUT] = value.ToString();

            }
        }
        public int ChecinExpiration
        {
            get
            {
                return Convert.ToInt32(Settings[HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION]);
            }

            protected set { ; }
        }

        public CheckinExpirationAction CheckinExpirationAction
        {
            get
            {
                return (CheckinExpirationAction)Enum.Parse(typeof(CheckinExpirationAction), Settings[HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION_ACTION]);
            }

            protected set { ; }
        }

        public bool DynamicUserInfo
        {
            get
            {
                return Convert.ToBoolean(Settings[HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DYNAMICUSERINFO]);
            }

            protected set { ; }
        }

        public bool JailbrokenDeviceAllowed
        {
            get
            {
                return Convert.ToBoolean(Settings[HaloPolicySettingsAbstract.KEY_ALLOW_JAILBREAK]);
            }

            protected set { ; }
        }

        public bool ExternalSharingEnabled
        {
            get
            {
                return Convert.ToBoolean(Settings[HaloPolicySettingsAbstract.KEY_ENABLE_EXTERNAL_SHARING]);
            }

            protected set { ; }
        }

        public bool CopyPasteEnabled
        {
            get
            {
                return Convert.ToBoolean(Settings[HaloPolicySettingsAbstract.KEY_ENABLE_COPY_PASTE]);
            }

            set
            {
                Settings[HaloPolicySettingsAbstract.KEY_ENABLE_COPY_PASTE] = value.ToString();
            }
        }

        public int LoginAttemptsAllowed
        {
            get
            {
                return Convert.ToInt32(Settings[HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE]);
            }

            protected set { ; }
        }

        public LoginFailureAction LoginFailureAction
        {
            get
            {
                return (LoginFailureAction)Enum.Parse(typeof(LoginFailureAction), Settings[HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION]);
            }

            protected set { ; }
        }

        /// <summary>
        /// Length of PIN (4 or 6).
        /// The only property that can't be encrypted because you need the pin length to decrypt
        /// </summary>
        public int PinLength
        {
            get
            {
                return Convert.ToInt32(Settings[HaloPolicySettingsAbstract.KEY_PIN_LENGTH]);
            }

            protected set { ; }
        }

        public string DefaultHomePage
        {
            get
            {
                return Settings[HaloPolicySettingsAbstract.KEY_DEFAULT_HOMEPAGE];
            }

            protected set { ; }
        }

        public bool EmailAppGranted
        {
            get
            {
                return Convert.ToBoolean(Settings[HaloPolicySettingsAbstract.KEY_ENABLE_EMAIL]);
            }

            set
            {
                Settings[HaloPolicySettingsAbstract.KEY_ENABLE_EMAIL] = value.ToString();
            }
        }

        public bool CalendarAppGranted
        {
            get
            {
                return Convert.ToBoolean(Settings[HaloPolicySettingsAbstract.KEY_ENABLE_CALENDAR]);
            }

            set
            {
                Settings[HaloPolicySettingsAbstract.KEY_ENABLE_CALENDAR] = value.ToString();
            }
        }

        public bool ContactsAppGranted
        {
            get
            {
                return Convert.ToBoolean(Settings[HaloPolicySettingsAbstract.KEY_ENABLE_CONTACTS]);
            }

            set
            {
                Settings[HaloPolicySettingsAbstract.KEY_ENABLE_CONTACTS] = value.ToString();
            }
        }

        public bool FileAppGranted
        {
            get
            {
                return Convert.ToBoolean(Settings[HaloPolicySettingsAbstract.KEY_ENABLE_FILEBROWSER]);
            }

            set
            {
                Settings[HaloPolicySettingsAbstract.KEY_ENABLE_FILEBROWSER] = value.ToString();
            }
        }

        public bool WebAppGranted
        {
            get
            {
                return Convert.ToBoolean(Settings[HaloPolicySettingsAbstract.KEY_ENABLE_WEBBROWSER]);
            }

            set
            {
                Settings[HaloPolicySettingsAbstract.KEY_ENABLE_WEBBROWSER] = value.ToString();
            }
        }

        public bool WebProxyRequired
        {
            get
            {
                return Convert.ToBoolean(Settings[HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_REQUIRED]);
            }

            protected set { ; }
        }

        public int WebProxyPort
        {
            get
            {
                return Convert.ToInt32(Settings[HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_PORT]);
            }

            protected set { ; }
        }

        public string WebProxyIP
        {
            get
            {
                return Settings[HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_IP];
            }

            protected set { ; }
        }

        public string BookmarkString
        {
            get
            {
                return Settings[HaloPolicySettingsAbstract.KEY_WEBBROWSER_BOOKMARK];
            }
            set { ; }

        }
        /// <summary>
        /// 
        /// </summary>
        public string CertString { get; set; }

        /// <summary>
        /// Base64 string
        /// </summary>
        public string CertData
        {
            get
            {
                return Settings[HaloPolicySettingsAbstract.KEY_WEBBROWSER_CERTIFICATE];
            }

            protected set { ; }
        }

        public string UserFirstName
        {
            get
            {
                return Settings[HaloPolicySettingsAbstract.KEY_USER_FIRST_NAME];
            }

            protected set { ; }
        }

        public string UserLastName
        {
            get
            {
                return Settings[HaloPolicySettingsAbstract.KEY_USER_LAST_NAME];
            }

            protected set { ; }
        }

        public string UserTitle 
        { 
            get
            { 
                return Settings[HaloPolicySettingsAbstract.KEY_USER_TITLE];
            }

            protected set { ; }
        }

        public string UserPhoneNumber
        {
            get
            {
                return Settings[HaloPolicySettingsAbstract.KEY_USER_PHONE];
            }
            protected set { ; }
        }

        public string UserDisplayName { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public string EmailPassword { get; set; }

        public string EmailServer { get; set; }

        public string EasUserName
        {
            get
            {
                return Settings[HaloPolicySettingsAbstract.KEY_ACTIVESYNC_USERNAME];
            }

            protected set { ; }
        }

        public string EasEmailAddress
        {
            get
            {
                return Settings[HaloPolicySettingsAbstract.KEY_ACTIVESYNC_EMAILADDRESS];
            }

            protected set { ; }
        }

        public string EasPassword
        {
            get
            {
                return Settings[HaloPolicySettingsAbstract.KEY_ACTIVESYNC_PASSWORD];
            }

            protected set { ; }
        }

        public string EasHost { 
            get
            {
                return Settings[HaloPolicySettingsAbstract.KEY_ACTIVESYNC_HOST];
            }

            protected set { ; }
        }

        public string EasDomain
        {
            get
            {
                return Settings[HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DOMAIN];
            }

            protected set { ; }
        }

        public string TenantKey { get; set; }

        public bool UseSSL
        {
            get
            {
                return Convert.ToBoolean(Settings[HaloPolicySettingsAbstract.KEY_ACTIVESYNC_SSLREQUIRED]);
            }

            protected set { ; }
        }

        /// <summary>
        /// Authenticated with Single Sign On
        /// </summary>
        public bool IsSSOUser { get; set; }

        #region User Settings for Email
        public DKCNMailDays MailDays { get; set; }

        public DKCNMailFetchFrequency MailFetchFrequency
        {
            get
            {
                return (DKCNMailFetchFrequency)Enum.Parse(typeof(DKCNMailFetchFrequency), Settings[HaloPolicySettingsAbstract.KEY_EMAIL_SYNC_FREQUENCY]);
            }

            protected set { ; }
        }

        public bool EmailPush { get; set; }

        public DKCNMailNotificationType MailNotificationType { get; set; }

        /// <summary>
        /// Which folders to sync from EAS
        /// </summary>
        public List<Object> FoldersToSync { get; set; }
        #endregion

        #region User Settings for Calendar
        public DKCNCalendarDays CalendarSyncWindow { get; set; }

        public bool CalendarNotifyOn { get; set; }

        public DKCNCalendarNotificationSound CalendarNotificationSound { get; set; }
        #endregion

        #region User Settings for Contacts
        public DKCOSortOrder ContactSortOrder { get; set; }

        public DKCNContactDisplayNameOrder ContactDisplaySortOrder { get; set; }
        #endregion

        #region Other User Settings
        public bool IsSystemTimeZone { get; set; }

        public List<Object> Apps { get; set; }

        public int LastAppIndex { get; set; }

        /// <summary>
        /// TODO Check implementation LATER
        /// </summary>
        public List<string> Bookmarks { get; set; }

        public string EmailSignature
        {
            get
            {
                return Settings[HaloPolicySettingsAbstract.KEY_EMAIL_SIGNATURE];
            }

            protected set { ; }
        }

        public string Message { get; set; }
        #endregion

        #endregion
    }
}

    



    

