﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Abstracts
{
    /// <summary>
    /// Abstract Class for messagewrapper
    /// </summary>
    public abstract class MessageWrapper
    {
        /// <summary>
        /// Set phone message box with OK button
        /// </summary>
        /// <param name="message">message to be displayed</param>
        public abstract void PhoneMessage(string message);

        /// <summary>
        /// Set phone messagebox 
        /// </summary>
        /// <param name="message">message to be displayed</param>
        /// <param name="caption">caption</param>
        /// <param name="result">"OK" or "OK Cancel" type messagebox</param>
        public abstract void PhoneMessage(string message, string caption);

        /// <summary>
        /// Windows 8.1 message
        /// </summary>
        /// <param name="message">message tp be displayed</param>
        public abstract void WindowsMessage(string message);

        /// <summary>
        /// windows 8.1 message with option
        /// </summary>
        /// <param name="message">message to be displayed</param>
        /// <param name="caption">caption of messagebox</param>
        public abstract void WindowsMessage(string message, string caption);
    }
}
