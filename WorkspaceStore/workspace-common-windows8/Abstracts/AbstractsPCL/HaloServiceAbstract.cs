﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractsPCL
{
    public enum DWStatusCode 
    {
        DWStatus_SUCCESS, 
        DWStatus_FAIL, 
        DWStatus_INPROGRESS,
        DWStatus_ERROR, 
        DWStatus_TIMEOUT, 
        DWStatus_AUTH_INVALID, 
        DWStatus_RESPONSE_WRONG_FORMAT, 
        DWStatus_SERVER_NOT_REACHABLE, 
        DWStatus_FAILED_LIBRARY_NOT_INITIALIZED, 
        DWStatus_FAILED_ALREADY_INITIALIZED, 
        DWStatus_FAILED_MISSING_MANDATORY_PARAMETER, 
        DWStatus_FAILED_NULL_PARAMETER, 
        DWStatus_CALL_WAS_CANCELED, 
        DWStatus_CONNECTION_FAILED, 
        DWStatus_NOT_DEFINED, 
        DWStatus_ABORT, 
        DWStatus_FOLDER_NOTFOUND, 
        DWStatus_API_NOT_IMPLEMENTED, 
        DWStatus_FAILED_NOT_PERMITTED_BY_POLICY, 
        DWStatus_NOT_ENOUGH_DISK_SPACE, 
        DWStatus_FILE_ALREADY_EXIST_ON_DEVICE, 
        DWStatus_FILE_DOWNLOAD_ALREADY_EXIST_IN_PROGRESS, 
        DWStatus_FAILED_MAXIMUM_SIMULTANEOUS_DOWNLOADS_REACHED, 
        DWStatus_FAIL_PREVIEW_ALREADY_INPROGRESS, 
        DWStatus_FILE_NOT_ON_DEVICE, 
        DWStatus_FAIL_PROBLEM_DECRYPTING_FILE, 
        DWStatus_REMOTE_APP_NOT_SUPPORTED, 
        DWStatus_FAILED_DOWNLOAD_NOT_IN_PROGRESS, 
        DWStatus_TENANT_EXPIRED, 
        DWStatus_TENANT_DISABLED, 
        DWStatus_USER_DEACTIVATED, 
        DWStatus_LICENSE_LIMIT_REACHED, 
        DWStatus_LICENSE_GROUP_LIMIT_REACHED, 
        DWStatus_LAST_ERROR
    };

    public class DWDataIn
    {
        public string jsonData;
        public DWDataIn()
        {

        }
    }

    public class DWDataOut
    {
        public int result;
        public string jsonData;
        public DWDataOut()
        {

        }
    }

    public abstract class HaloServiceAbstract
    {
        public abstract int attemptLogin(String user, String pass, String server);

        public abstract void startMQTT();

        public abstract void SetMQTTServer(String server, int port);

        public abstract DWStatusCode checkin();

        public abstract DWStatusCode getFullConfig();

        protected abstract void updateState();

        protected abstract String setQuery(int ksState2);

        protected abstract String setKSStatus(int state);

        protected abstract int lock_callback(int status);

        public abstract int resetPin_callback();

        public abstract int wipeData_callback();

        public abstract int sendMessage_callback(String jsonMsg);

        public abstract int query_callback();
        
        public abstract void reset();
        
        #region Event Handlers from Halo library
        public abstract void onAuthenticateResult(DWStatusCode state, String msg);

        public abstract DWDataOut onCommand(int type, DWDataIn dataIn);

        public abstract int onProfileUpdate( String jsonProfile, bool fullConfig );

        public abstract void unregister();

        public abstract int onChekinCompleted(DWStatusCode state, String msg);

        public abstract int onGetFullConfigCompleted(DWStatusCode state, String msg);
        #endregion
        
        public abstract void Deactivate(); 
    }
}





