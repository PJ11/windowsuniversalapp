﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace AbstractsPCL
{
#if USE_DATA_CONTRACT
    [DataContract]
    public class Items_701
    {
        [DataMember(Name = "INACTIVITY_TIMEOUT")]
        public int InactivityTimeout { get; set; }
        [DataMember(Name = "ENABLE_COPY_PASTE")]
        public bool EnableCopyPaste { get; set; }
        [DataMember(Name = "PIN_LENGTH")]
        public int PinLength { get; set; }
        [DataMember(Name = "ENABLE_EXTERNAL_SHARING")]
        public bool EnableExternalSharing { get; set; }
        [DataMember(Name = "ALLOW_JAILBREAK")]
        public bool AllowJailbreak { get; set; }
        [DataMember(Name = "LOGIN_FAILURE_ACTION")]
        public int LoginFailureAction { get; set; }
        [DataMember(Name = "LOGIN_FAILURE")]
        public int LoginFailure { get; set; }
        [DataMember(Name = "CHECKIN_EXPIRATION")]
        public int CheckinExpiration { get; set; }
        [DataMember(Name = "CHECKIN_EXPIRATION_ACTION")]
        public int CheckinExpirationAction { get; set; }
    }

    [DataContract]
    public class Bookmarks
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "url")]
        public string Url { get; set; }
        [DataMember(Name = "secure")]
        public bool Secure { get; set; }
    }

    [DataContract]
    public class Bookmark
    {
        [DataMember(Name = "bookmarks")]
        public Bookmarks[] Bookmarks { get; set; }
    }

    [DataContract]
    public class Tester
    {
        [DataMember(Name = "BOOKMARK")]
        public Bookmark Bookmark { get; set; }
    }

    [DataContract]
    public class Items_703
    {
        [DataMember(Name = "ENABLE_CALENDAR")]
        public bool EnableCalendar { get; set; }
        [DataMember(Name = "ENABLE_EMAIL")]
        public bool EnableEmail { get; set; }
        [DataMember(Name = "EMAIL_SYNC_FREQUENCY")]
        public int EmailSyncFrequency { get; set; }
        [DataMember(Name = "WEBBROWSER_PROXY_REQUIRED")]
        public bool WebbrowserProxyRequired { get; set; }
        [DataMember(Name = "EMAIL_SIGNATURE")]
        public string EmailSignature { get; set; }
        [DataMember(Name = "ENABLE_WEBBROWSER")]
        public bool EnableWebBrowser { get; set; }
        [DataMember(Name = "DEFAULT_HOMEPAGE")]
        public string DefaultHomePage { get; set; }
        [DataMember(Name = "BOOKMARK")]
        public Bookmark BookMark { get; set; }
        [DataMember(Name = "ENABLE_CONTACTS")]
        public bool EnableContacts { get; set; }
        [DataMember(Name = "ENABLE_FILEBROWSER")]
        public bool EnableFilebrowser { get; set; }
    }

    [DataContract]
    public class Items_705
    {
        [DataMember(Name = "ACTIVESYNC_HOST")]
        public string Host { get; set; }
        [DataMember(Name = "ACTIVESYNC_EMAILADDRESS")]
        public string EmailAddress { get; set; }
        [DataMember(Name = "ACTIVESYNC_USERNAME")]
        public string Username { get; set; }
        [DataMember(Name = "ACTIVESYNC_DOMAIN")]
        public string Domain { get; set; }
        [DataMember(Name = "ACTIVESYNC_SSLREQUIRED")]
        public bool SSLRequired { get; set; }
        [DataMember(Name = "ACTIVESYNC_AGENTIDENTIFIER")]
        public string AgentIdentifier { get; set; }
        [DataMember(Name = "ACTIVESYNC_DYNAMICUSERINFO")]
        public bool DynamicUserInfo { get; set; }
    }

    [DataContract]
    public class PersonInfoLean
    {
        [DataMember(Name = "firstname")]
        public string FirstName { get; set; }
        [DataMember(Name = "phonenumber")]
        public string PhoneNumber { get; set; }
        [DataMember(Name = "lastname")]
        public string LastName { get; set; }
    }

    [DataContract]
    public class CCMObjectBase
    {
        [DataMember(Name = "type")]
        public int Type { get; set; }
        [DataMember(Name = "seq")]
        public int Seq { get; set; }
        [DataMember(Name = "isremoved")]
        public bool IsRemoved { get; set; }
    }

    [DataContract]
    public class CCMPolicy_701 : CCMObjectBase
    {
        [DataMember(Name = "items")]
        public Items_701 Items { get; set;}
    }

    [DataContract]
    public class CCMPolicy_703 : CCMObjectBase
    {
        [DataMember(Name = "items")]
        public Items_703 Items { get; set;}
    }

    [DataContract]
    public class CCMPolicy_705 : CCMObjectBase
    {
        [DataMember(Name = "items")]
        public Items_705 Items { get; set;}
    }

    [DataContract]
    public class CCMPolicyVersion1
    {
        [DataMember]
        public CCMPolicy_701 Prop701 { get; set;}
        [DataMember]
        public CCMPolicy_703 Prop703 { get; set;}
        [DataMember]
        public CCMPolicy_705 Prop705 { get; set;}
        [DataMember(Name="personinfolean")]
        public PersonInfoLean PersonInfo { get; set;}
    }

    [DataContract]
    public class Policies
    {
        [DataMember]
        public CCMPolicyVersion1[] V1 { get; set; }
    }
#endif // USE_DATA_CONTRACT

/*
[
  {
  "type":701,
  "seq":0,
  "items":
  {
    "INACTIVITY_TIMEOUT":5,
	"ENABLE_COPY_PASTE":false,
	"PIN_LENGTH":4,
	"ENABLE_EXTERNAL_SHARING":false,
	"ALLOW_JAILBREAK":true,
	"LOGIN_FAILURE_ACTION":2,
	"LOGIN_FAILURE":0,
	"CHECKIN_EXPIRATION":30,
	"CHECKIN_EXPIRATION_ACTION":-1
  },
  "isremoved":false
  },
  {
  "type":703,
  "seq":0,
  "items":
  {
    "ENABLE_CALENDAR":true,
	"ENABLE_EMAIL":true,
	"EMAIL_SYNC_FREQUENCY":0,
	"WEBBROWSER_PROXY_REQUIRED":false,
	"EMAIL_SIGNATURE":"---------------------------\nWorkspace - Dev SK1",
	"ENABLE_WEBBROWSER":true,
	"DEFAULT_HOMEPAGE":"http:\/\/www.dell.com",
	"BOOKMARK":	"{\"bookmarks\":[{\"name\":\"Dell\",\"url\":\"http:\/\/www.dell.com\",\"secure\":false},{\"name\":\"Yahoo\",\"url\":\"http:\/\/www.yahoo.com\",\"secure\":false},{\"name\":\"Google\",\"url\":\"http:\/\/www.google.com\",\"secure\":false}]}",
	"ENABLE_CONTACTS":true,
	"ENABLE_FILEBROWSER":true
  },
  "isremoved":false
  },
  {
  "type":705,
  "seq":0,
  "items":
  {
    "ACTIVESYNC_HOST":"mail.dmpdev1.com",
	"ACTIVESYNC_EMAILADDRESS":"LorenR@dmpdev1.com",
	"ACTIVESYNC_USERNAME":"LorenR",
	"ACTIVESYNC_DOMAIN":"dmpdev1",
	"ACTIVESYNC_SSLREQUIRED":true,
	"ACTIVESYNC_AGENTIDENTIFIER":"DKWorkSpc",
	"ACTIVESYNC_DYNAMICUSERINFO":true
  },
  "isremoved":false},
  {
    "personInfoLean":
	{
	  "firstName":"Loren",
	  "phoneNumber":"",
	  "lastName":"Rogers",
	  "title":""
	}
  }
]
*/
    public class MalformedPolicyException : Exception
    {
        public MalformedPolicyException() { }

        public MalformedPolicyException(String message)
            : base(message)
        {
        }
    }

    public abstract class HaloPolicySettingsAbstract
    {
        public static  string KEY_PIN_LENGTH = "PIN_LENGTH";
        public static  string KEY_LOGIN_FAILURE = "LOGIN_FAILURE";
        public static  string KEY_LOGIN_FAILURE_ACTION = "LOGIN_FAILURE_ACTION";
        public static  string KEY_CHECKIN_EXPIRATION = "CHECKIN_EXPIRATION";
        public static  string KEY_CHECKIN_EXPIRATION_ACTION = "CHECKIN_EXPIRATION_ACTION";
        public static  string KEY_INACTIVITY_TIMEOUT = "INACTIVITY_TIMEOUT";
        public static string CCM_INACTIVITY_TIMEOUT = "CCM_INACTIVITY_TIMEOUT";
        public static  string KEY_ALLOW_JAILBREAK = "ALLOW_JAILBREAK";
        public static  string KEY_ENABLE_EXTERNAL_SHARING = "ENABLE_EXTERNAL_SHARING";
        public static  string KEY_ENABLE_COPY_PASTE = "ENABLE_COPY_PASTE";
        public static  string KEY_ENABLE_EMAIL = "ENABLE_EMAIL";
        public static  string KEY_EMAIL_USER_DISPLAY_NAME = "EMAIL_USER_DISPLAY_NAME";
        public static  string KEY_EMAIL_SIGNATURE = "EMAIL_SIGNATURE";
        public static  string KEY_EMAIL_SYNC_FREQUENCY = "EMAIL_SYNC_FREQUENCY";
        public static  string KEY_ENABLE_CALENDAR = "ENABLE_CALENDAR";
        public static  string KEY_ENABLE_CONTACTS = "ENABLE_CONTACTS";
        public static  string KEY_ENABLE_FILEBROWSER = "ENABLE_FILEBROWSER";
        public static  string KEY_ENABLE_WEBBROWSER = "ENABLE_WEBBROWSER";
        public static  string KEY_DEFAULT_HOMEPAGE = "DEFAULT_HOMEPAGE";
        public static  string KEY_WEBBROWSER_CERTIFICATE = "WEBBROWSER_CERTIFICATE";
        public static  string KEY_WEBBROWSER_PROXY_REQUIRED = "WEBBROWSER_PROXY_REQUIRED";
        public static  string KEY_WEBBROWSER_PROXY_IP = "WEBBROWSER_PROXY_IP";
        public static  string KEY_WEBBROWSER_PROXY_PORT = "WEBBROWSER_PROXY_PORT";
        public static  string KEY_WEBBROWSER_BOOKMARK = "BOOKMARK";

        public static  string KEY_ACTIVESYNC_HOST = "ACTIVESYNC_HOST";
        public static  string KEY_ACTIVESYNC_SSLREQUIRED = "ACTIVESYNC_SSLREQUIRED";
        public static  string KEY_ACTIVESYNC_DOMAIN = "ACTIVESYNC_DOMAIN";
        public static  string KEY_ACTIVESYNC_DYNAMICUSERINFO = "ACTIVESYNC_DYNAMICUSERINFO";
        public static  string KEY_ACTIVESYNC_USERNAME = "ACTIVESYNC_USERNAME";
        // TODO (Bob) FIx this, the user name and display name need to be separate.
        public static  string KEY_ACTIVESYNC_DISPLAYNAME = "ACTIVESYNC_USERNAME";
        public static  string KEY_ACTIVESYNC_EMAILADDRESS = "ACTIVESYNC_EMAILADDRESS";
        public static  string KEY_ACTIVESYNC_PASSWORD = "ACTIVESYNC_PASSWORD";
        public static  string KEY_ACTIVESYNC_CERTIFICATE = "ACTIVESYNC_CERTIFICATE";
        public static string KEY_ACTIVESYNC_ADUPN = "ACTIVESYNC_ADUPN";
        public static  string KEY_ACTIVESYNC_AGENTIDENTIFIER = "ACTIVESYNC_AGENTIDENTIFIER";

        public static  string KEY_BROWSER_CERTTIFICATES = "BROWSER_CERTTIFICATES";
        public static  string KEY_BROWSER_IDENTITYCERTTIFICATE = "BROWSER_IDENTITYCERTTIFICATE";

        public static  string KEY_USER_FIRST_NAME = "firstName";
        public static  string KEY_USER_LAST_NAME = "lastName";
        public static  string KEY_USER_PHONE = "phoneNumber";
        public static  string KEY_USER_TITLE = "title";

        public string[] haloKeys = {
                KEY_PIN_LENGTH,
                KEY_LOGIN_FAILURE,
                KEY_LOGIN_FAILURE_ACTION,
                KEY_CHECKIN_EXPIRATION,
                KEY_CHECKIN_EXPIRATION_ACTION,
                KEY_INACTIVITY_TIMEOUT,
                CCM_INACTIVITY_TIMEOUT,
                KEY_ALLOW_JAILBREAK,
                KEY_ENABLE_EXTERNAL_SHARING,
                KEY_ENABLE_COPY_PASTE,
                KEY_ENABLE_EMAIL,
                KEY_EMAIL_USER_DISPLAY_NAME,
                KEY_EMAIL_SIGNATURE,
                KEY_EMAIL_SYNC_FREQUENCY,
                KEY_ENABLE_CALENDAR,
                KEY_ENABLE_CONTACTS,
                KEY_ENABLE_FILEBROWSER,
                KEY_ENABLE_WEBBROWSER,
                KEY_DEFAULT_HOMEPAGE,
                KEY_WEBBROWSER_CERTIFICATE,
                KEY_WEBBROWSER_PROXY_REQUIRED,
                KEY_WEBBROWSER_PROXY_IP,
                KEY_WEBBROWSER_PROXY_PORT,
                KEY_WEBBROWSER_BOOKMARK,
                KEY_ACTIVESYNC_HOST,
                KEY_ACTIVESYNC_SSLREQUIRED,
                KEY_ACTIVESYNC_DOMAIN,
                KEY_ACTIVESYNC_DYNAMICUSERINFO,
                KEY_ACTIVESYNC_USERNAME,
                KEY_ACTIVESYNC_EMAILADDRESS,
                KEY_ACTIVESYNC_DISPLAYNAME,
                KEY_ACTIVESYNC_PASSWORD,
                KEY_ACTIVESYNC_CERTIFICATE,
                KEY_ACTIVESYNC_AGENTIDENTIFIER,
                KEY_USER_FIRST_NAME,
                KEY_USER_LAST_NAME,
                KEY_USER_PHONE,
                KEY_USER_TITLE
        };

        public abstract void parsePolicy(string policy);

        public abstract void detectAndSave();

        public abstract void saveToDKSettings();

        public abstract string getValue (string key);

        public abstract void setValue(string key, string value);
    }
}
