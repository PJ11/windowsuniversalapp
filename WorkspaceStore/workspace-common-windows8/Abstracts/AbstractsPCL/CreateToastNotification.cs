﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Abstracts
{
    public abstract class CreateToastNotification
    {
        /// <summary>
        /// Toasts the notification text.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="message">The message.</param>
        public abstract void CreateToastNotificationText(string title, string message);
        /// <summary>
        /// Toasts the notification text image.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="message">The message.</param>
        public abstract void CreateCustomToastNotification(string title, string message);

        /// <summary>
        /// Toasts the notification text_03.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="message">The message.</param>
        public abstract void CreateToastNotificationForVertical(string title, string message);

        /// <summary>
        /// Toasts the notification text_04.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="message">The message.</param>
        public abstract void CreateToastNotificationForHorizontal(string title, string message);

    }
}