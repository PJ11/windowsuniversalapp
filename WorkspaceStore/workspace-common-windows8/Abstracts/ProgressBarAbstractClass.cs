﻿// <copyright file="ProgressBarAbstractClass.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date>10-04-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini             10-04-2014               1.0                 First Version </summary>

namespace Abstracts
{
    #region NAMESPACE
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    #endregion
    public abstract class ProgressBarAbstractClass<T>
    {        
       public abstract T Create_Indeterminate_ProgressBar();
       public abstract T Create_Determinate_ProgressBar();     
    }
}
