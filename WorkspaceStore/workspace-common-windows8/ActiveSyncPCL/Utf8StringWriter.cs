﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveSyncPCL
{
    class Utf8StringWriter : StringWriter
    {
        public Utf8StringWriter()
            : base()
        {

        }
        public Utf8StringWriter(StringBuilder sb)
            : base(sb)
        {
        }
        public override Encoding Encoding { get { return Encoding.UTF8; } }
    }
}
