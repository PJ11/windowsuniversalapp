﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public enum EnumContactCommandType
    {
        Add,
        Delete,
        Change
    }

    public class ASContactClientChangesSyncRequest : ASCommandRequest
    {
       public EnumContactCommandType CommandType { get; set; }
       public string mLastSyncKey { get; set; }
       public string CollectionId { get; set; }
       public string ClientId { get; set; }
       
        /// Delegate declaration.
       public delegate XDocument SetClientChangesInRequestXMLDelegate(ASContactClientChangesSyncRequest obj);
        // Define an Event based on the above Delegate
        public static event SetClientChangesInRequestXMLDelegate AppendClientChangesInRequestXML;
        public ASContactClientChangesSyncRequest()
        {
            Command = "Sync";
           
        }

        // This function generates an ASFolderSyncResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASContactClientChangesSyncResponse(httpResp);
        }


        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASContactClientChangesSyncResponse(statusCode);
        }
      
        // This function generates the XML request body
        // for the FolderSync request.
        protected override void GenerateXMLPayload()
        {

            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            XDocument syncXML = null;
           
           //event will generate xml from container app here
            syncXML = AppendClientChangesInRequestXML(this);
          //xml is generate now
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            syncXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();

        }
    }
}
