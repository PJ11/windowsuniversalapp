﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    // This class represents a "device" and is used to
    // generate a DeviceInformation Xml element, as specified
    // in [MS-ASCMD] section 2.2.3.45.
    public class Device
    {
        const String strSettingsXmlns = "settings";
        const String strSettingsNamespace = "Settings";

        private string deviceID = null;
        private string deviceType = null;
        private string model = null;
        private string IMEINumber = null;
        private string friendlyName = null;
        private string operatingSystem = null;
        private string operatingSystemLanguage = null;
        private string phoneNumber = null;
        private string mobileOperator = null;
        private string userAgent = null;

        #region Property Accessors
        public string DeviceID
        {
            get
            {
                return deviceID;
            }
            set
            {
                deviceID = value;
            }
        }

        public string DeviceType
        {
            get
            {
                return deviceType;
            }
            set
            {
                deviceType = value;
            }
        }

        public string Model
        {
            get
            {
                return model;
            }
            set
            {
                model = value;
            }
        }

        public string IMEI
        {
            get
            {
                return IMEINumber;
            }
            set
            {
                IMEINumber = value;
            }
        }

        public string FriendlyName
        {
            get
            {
                return friendlyName;
            }
            set
            {
                friendlyName = value;
            }
        }

        public string OperatingSystem
        {
            get
            {
                return operatingSystem;
            }
            set
            {
                operatingSystem = value;
            }
        }

        public string OperatingSystemLanguage
        {
            get
            {
                return operatingSystemLanguage;
            }
            set
            {
                operatingSystemLanguage = value;
            }
        }

        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }
            set
            {
                phoneNumber = value;
            }
        }

        public string MobileOperator
        {
            get
            {
                return mobileOperator;
            }
            set
            {
                mobileOperator = value;
            }
        }

        public string UserAgent
        {
            get
            {
                return userAgent;
            }
            set
            {
                userAgent = value;
            }
        }
        #endregion

        // This function generates and returns an XNode for the
        // DeviceInformation element.
        /*
        <settings:Set xmlns:settings="Settings">
          <settings:Model>Sample Model</settings:Model> 
          <settings:FriendlyName>FolderSync/Sync Example</settings:FriendlyName> 
          <settings:OS>Sample OS 1.0</settings:OS> 
          <settings:OSLanguage>English</settings:OSLanguage> 
          <settings:PhoneNumber>425-555-1000</settings:PhoneNumber> 
          <settings:MobileOperator>Phone Company</settings:MobileOperator> 
          <settings:UserAgent>EX2010_activesyncfolder_cs_1.0</settings:UserAgent> 
          </settings:Set>
         * */
        public XElement GetDeviceInformationNode()
        {
            XDocument xmlDoc = new XDocument();

            XNamespace settings = strSettingsNamespace;
            XElement rootElem = new XElement(settings + "DeviceInformation", new XAttribute(XNamespace.Xmlns + strSettingsXmlns, strSettingsNamespace),
                new XElement(settings + "Set"),
                new XText("Fuck you"));

            xmlDoc.Add(new XElement(settings + "DeviceInformation", new XAttribute(XNamespace.Xmlns + strSettingsXmlns, strSettingsNamespace),
                new XElement(settings + "Set"))
                );
            XElement setElement = xmlDoc.Element(settings + "DeviceInformation").Element(settings + "Set");
            {
                setElement.Add(new XElement(settings + "Model", 
                    new XText(Model))
                    );
            }

            if (IMEI != null)
            {
                setElement.Add(new XElement(settings + "IMEI", 
                    new XText(IMEI))
                    );
            }
            if (FriendlyName != null)
            {
                setElement.Add(new XElement(settings + "FriendlyName", 
                    new XText(FriendlyName))
                    );
            }
            if (OperatingSystem != null)
            {
                setElement.Add(new XElement(settings + "OS", 
                    new XText(OperatingSystem))
                    );
            }
            if (OperatingSystemLanguage != null)
            {
                setElement.Add(new XElement(settings + "OSLanguage",
                    new XText(OperatingSystemLanguage))
                    );
            }
            if (PhoneNumber != null)
            {
                setElement.Add(new XElement(settings + "PhoneNumber",
                    new XText(PhoneNumber))
                    );
            }
            if (MobileOperator != null)
            {
                setElement.Add(new XElement(settings + "MobileOperator",
                    new XText(MobileOperator))
                    );
            }
            if (UserAgent != null)
            {
                setElement.Add(new XElement(settings + "UserAgent",
                    new XText(UserAgent))
                    );
            }
            return xmlDoc.Root;
#if OLDWAY
            XmlElement deviceInfoElement = xmlDoc.CreateElement(strSettingsXmlns, "DeviceInformation", strSettingsNamespace);
            xmlDoc.AppendChild(deviceInfoElement);

            XmlElement setElement = xmlDoc.CreateElement(strSettingsXmlns, "Set", strSettingsNamespace);
            deviceInfoElement.AppendChild(setElement);

            if (Model != null)
            {
                XmlElement modelElement = xmlDoc.CreateElement(strSettingsXmlns, "Model", strSettingsNamespace);
                modelElement.InnerText = Model;
                setElement.AppendChild(modelElement);
            }

            if (IMEI != null)
            {
                XmlElement IMEIElement = xmlDoc.CreateElement(strSettingsXmlns, "IMEI", strSettingsNamespace);
                IMEIElement.InnerText = IMEI;
                setElement.AppendChild(IMEIElement);
            }

            if (FriendlyName != null)
            {
                XmlElement friendlyNameElement = xmlDoc.CreateElement(strSettingsXmlns, "FriendlyName", strSettingsNamespace);
                friendlyNameElement.InnerText = FriendlyName;
                setElement.AppendChild(friendlyNameElement);
            }

            if (OperatingSystem != null)
            {
                XmlElement operatingSystemElement = xmlDoc.CreateElement(strSettingsXmlns, "OS", strSettingsNamespace);
                operatingSystemElement.InnerText = OperatingSystem;
                setElement.AppendChild(operatingSystemElement);
            }

            if (OperatingSystemLanguage != null)
            {
                XmlElement operatingSystemLanguageElement = xmlDoc.CreateElement(strSettingsXmlns, "OSLanguage", strSettingsNamespace);
                operatingSystemLanguageElement.InnerText = OperatingSystemLanguage;
                setElement.AppendChild(operatingSystemLanguageElement);
            }

            if (PhoneNumber != null)
            {
                XmlElement phoneNumberElement = xmlDoc.CreateElement(strSettingsXmlns, "PhoneNumber", strSettingsNamespace);
                phoneNumberElement.InnerText = PhoneNumber;
                setElement.AppendChild(phoneNumberElement);
            }

            if (MobileOperator != null)
            {
                XElement mobileOperatorElement = xmlDoc.CreateElement(strSettingsXmlns, "MobileOperator", strSettingsNamespace);
                mobileOperatorElement.InnerText = MobileOperator;
                setElement.AppendChild(mobileOperatorElement);
            }

            if (UserAgent != null)
            {
                XmlElement userAgentElement = xmlDoc.CreateElement(strSettingsXmlns, "UserAgent", strSettingsNamespace);
                userAgentElement.InnerText = UserAgent;
                setElement.AppendChild(userAgentElement);
            }

            return xmlDoc.DocumentElement;
#endif
        }
    }
}
