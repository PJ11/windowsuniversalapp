﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public enum EnumFolderCommandType
    {
        Add,
        Delete
    }
    public class ASFolderOperationRequest : ASCommandRequest
    {
        public EnumFolderCommandType CommandType { get; set; }
        public string SyncKey { get; set; }
        public string NewFoldername { get; set; }
        public ASFolderOperationRequest()
        {
            if(CommandType == EnumFolderCommandType.Delete)
                Command = "FolderDelete";
            else
                Command = "FolderCreate";

        }

        // This function generates an ASFolderSyncResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASFolderOperationResponse(httpResp);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASFolderOperationResponse(statusCode);
        }


        protected override void GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            XDocument commandNodeXML = new XDocument(new XDeclaration("1.0", "utf-8", null));
            XNamespace folderHierarchy = Namespaces.folderHierarchyNamespace;

            

            //<?xml version="1.0" encoding="utf-8"?> <FolderCreate xmlns="FolderHierarchy">   <SyncKey>1</SyncKey>   <ParentId>5</ParentId>   <DisplayName>NewFolder</DisplayName>   <Type>12</Type> </FolderCreate> 
            if (CommandType == EnumFolderCommandType.Add)
            {

                XElement folderCreateNode = new XElement(folderHierarchy + "FolderCreate");
                commandNodeXML.Add(folderCreateNode);
                //This sync key should be previous sync key got in folder sync response
                XElement syncKeyNode = new XElement(folderHierarchy + "SyncKey", "1");
                folderCreateNode.Add(syncKeyNode);

                XElement parentIDNode = new XElement(folderHierarchy + "ParentId", "2");
                folderCreateNode.Add(parentIDNode);
                XElement displayNameNode = new XElement(folderHierarchy + "DisplayName", new XText(NewFoldername));
                folderCreateNode.Add(displayNameNode);
                XElement typeNode = new XElement(folderHierarchy + "Type", "14");
                folderCreateNode.Add(typeNode);
                
            }
            else if (CommandType == EnumFolderCommandType.Delete)
            {
                XElement folderDeleteNode = new XElement(folderHierarchy + "FolderDelete");
                commandNodeXML.Add(folderDeleteNode);
                //This sync key should be previous sync key got in folder sync response
                XElement syncKeyNode = new XElement(folderHierarchy + "SyncKey", "1");
                folderDeleteNode.Add(syncKeyNode);
                XElement serverIDNode = new XElement(folderHierarchy + "ServerId", "13");
                folderDeleteNode.Add(serverIDNode);
            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            commandNodeXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();
        }
    }
}
