﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    // This class represents the Sync command request
    public class ASSyncRequest : ASCommandRequest
    {
        private Int32 wait = 0;                 // 1 - 59 minutes
        private Int32 heartBeatInterval = 0;    // 60 - 3540 seconds
        private Int32 windowSize = 512;           // 1 - 512 changes
        private bool isPartial = false;

        List<Folder> folderList = null;

        #region Property Accessors

        public Int32 Wait
        {
            get
            {
                return wait;
            }
            set
            {
                wait = value;
            }
        }

        public Int32 HeartBeatInterval
        {
            get
            {
                return heartBeatInterval;
            }
            set
            {
                heartBeatInterval = value;
            }
        }

        public Int32 WindowSize
        {
            get
            {
                return windowSize;
            }
            set
            {
                windowSize = value;
            }
        }

        public bool IsPartial
        {
            get
            {
                return isPartial;
            }
            set
            {
                isPartial = value;
            }
        }

        public List<Folder> Folders
        {
            get
            {
                return folderList;
            }
        }

        #endregion

        public ASSyncRequest()
        {
            this.Command = "Sync";
            this.folderList = new List<Folder>();
        }

        // This function generates an ASSyncResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASSyncResponse(httpResp, folderList);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASSyncResponse(statusCode);
        }

        // This function generates the XML request body
        // for the Sync request.
        protected override void GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument syncXML = new XDocument(new XDeclaration("1.0", "utf-8", null));

            XNamespace syncnamespace = Namespaces.airSyncNamespace;
            XNamespace syncbasenamespace = Namespaces.airSyncBaseNamespace;

            XElement syncNode = new XElement(syncnamespace + "Sync",
                new XAttribute(XNamespace.None + Xmlns.airSyncXmlns, Namespaces.airSyncNamespace),
                new XAttribute(XNamespace.Xmlns + Xmlns.airSyncBaseXmlns, Namespaces.airSyncBaseNamespace));

            syncXML.Add(syncNode);

            // Only add a collections node if there are folders in the request.
            // If omitting, there should be a Partial element.

            if (folderList.Count == 0 && isPartial == false)
                throw new ArgumentException(
                    "Sync requests must specify collections or include the Partial element.");

            if (folderList.Count > 0)
            {

                XElement collectionsNode = new XElement(syncnamespace + "Collections");
                syncNode.Add(collectionsNode);
                foreach (Folder folder in folderList)
                {
                    XElement collectionNode = new XElement(syncnamespace + "Collection");
                    collectionsNode.Add(collectionNode);

                    XElement syncKeyNode = new XElement(syncnamespace + "SyncKey", new XText(folder.SyncKey));
                    collectionNode.Add(syncKeyNode);

                    XElement collectionIdNode = new XElement(syncnamespace + "CollectionId", new XText(folder.Id));
                    collectionNode.Add(collectionIdNode);


                    // To override "ghosting", you must include a Supported element here.
                    // This only applies to calendar items and contacts
                    // NOT IMPLEMENTED

                    // If folder is set to permanently delete items, then add a DeletesAsMoves
                    // element here and set it to false.
                    // Otherwise, omit. Per MS-ASCMD, the absence of this element is the same as true.
                    if (folder.AreDeletesPermanent == true)
                    {
                        XElement deletesAsMovesNode = new XElement(syncnamespace  + "DeletesAsMoves", new XText("1"));
                        collectionNode.Add(deletesAsMovesNode);
                    }

                    // In almost all cases the GetChanges element can be omitted. 
                    // It only makes sense to use it if SyncKey != 0 and you don't want 
                    // changes from the server for some reason.
                    if (folder.AreChangesIgnored == false)
                    {
                        XElement getChangesNode = new XElement(syncnamespace + "GetChanges", new XText("1"));
                        collectionNode.Add(getChangesNode);

                    }

                    // If there's a folder-level window size, include it
                    if (folder.WindowSize > 0)
                    {

                        XElement folderWindowSizeNode = new XElement(syncnamespace  + "WindowSize", new XText(folder.WindowSize.ToString()));
                        collectionNode.Add(folderWindowSizeNode);
                    }

                    // If the folder is set to conversation mode, specify that
                    if (folder.UseConversationMode == true)
                    {
                        XElement conversationModeNode = new XElement(syncnamespace  + "ConversationMode", new XText("1"));
                        collectionNode.Add(conversationModeNode);

                    }

                    // Include sync options for the folder
                    // Note that you can include two Options elements, but the 2nd one is for SMS
                    // SMS is not implemented at this time, so we'll only include one.
                    if (folder.Options != null)
                    {
                        folder.GenerateOptionsXml(collectionNode, syncnamespace, syncbasenamespace);
                    }

                    // Include client-side changes
                    // TODO: Implement client side changes on the Folder object
                    //if (folder.Commands != null)
                    //{
                    //    folder.GenerateCommandsXml(collectionNode);
                    //}
                }
            }

            // If a wait period was specified, include it here
            if (wait > 0)
            {
                XElement waitNode = new XElement(syncnamespace  + "Wait", new XText(wait.ToString()));
                syncNode.Add(waitNode);

            }

            // If a heartbeat interval period was specified, include it here
            if (heartBeatInterval > 0)
            {
                XElement heartBeatIntervalNode = new XElement(syncnamespace + "HeartbeatInterval", new XText(heartBeatInterval.ToString()));
                syncNode.Add(heartBeatIntervalNode);

            }

            // If a windows size was specified, include it here
            if (windowSize > 0)
            {
                XElement windowSizeNode = new XElement(syncnamespace +  "WindowSize", new XText(windowSize.ToString()));
                syncNode.Add(windowSizeNode);

            }

            // If this request contains a partial list of collections, include the Partial element
            if (isPartial == true)
            {
                XElement partialNode = new XElement(syncnamespace + "Partial", new XText(windowSize.ToString()));
                syncNode.Add(partialNode);

            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            syncXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();
            
        }
    }
}
