﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ActiveSyncPCL
{
    public static class HttpExtensions 
    {
       
        private static int DefaultRequestTimeout = 120000;
        public static Task<HttpWebResponse> GetResponseAsync(this HttpWebRequest request)
        {
                var taskComplete = new TaskCompletionSource<HttpWebResponse>();
                request.BeginGetResponse(asyncResponse =>
                {
                    try
                    {
                        HttpWebRequest responseRequest = (HttpWebRequest)asyncResponse.AsyncState;
                        var someResponse = (HttpWebResponse)responseRequest.EndGetResponse(asyncResponse);
                        taskComplete.TrySetResult(someResponse);
                    }
                    catch (WebException webExc)
                    {
                        if(webExc.Status == WebExceptionStatus.RequestCanceled)
                        {
                            // Most likely the screen got locked
                            taskComplete.TrySetException(new OperationCanceledException("Request Canceled"));
                        }
                        if (webExc.Response != null)
                        {
                            HttpWebResponse failedResponse = (HttpWebResponse)webExc.Response;
                            using (StreamReader reader = new StreamReader(webExc.Response.GetResponseStream()))
                            {
                                string result = reader.ReadToEnd();
                                taskComplete.TrySetResult(failedResponse);
                            }
                        }
                    }
                    catch (System.Security.SecurityException se)
                    {
                        // The number of retry attempts has been reached for this EAS account
                        // I just got this as I was testing invalid password code
                        // TODO Need to add some functionality.
                        taskComplete.TrySetException(se);
                    }
                    catch (System.Exception)
                    {
                        taskComplete.TrySetResult(null);
                    }
                }, request
                );
                return taskComplete.Task;
            //}
        }
        
        public static Task<HttpWebResponse> GetResponse(this HttpWebRequest request,int waitTime)
        {
            DefaultRequestTimeout= waitTime*1000;
            var dataReady = new AutoResetEvent(false);
            var taskComplete = new TaskCompletionSource<HttpWebResponse>();
            HttpWebResponse response = null;
            try
            {
                var callback = new AsyncCallback(delegate(IAsyncResult asynchronousResult)
                {
                    try
                    {
                        response = (HttpWebResponse)request.EndGetResponse(asynchronousResult); 
                        taskComplete.TrySetResult(response);
                        dataReady.Set();
                    }
                    catch (WebException webExc)
                    {
                        HttpWebResponse failedResponse = (HttpWebResponse)webExc.Response;
                        using (StreamReader reader = new StreamReader(webExc.Response.GetResponseStream()))
                        {
                            string result = reader.ReadToEnd();
                            taskComplete.TrySetResult(failedResponse);
                        }
                       
                    }
                });
                request.BeginGetResponse(callback, request);
                if (dataReady.WaitOne(DefaultRequestTimeout))
                {
                    taskComplete.TrySetResult(response);
                }
                
            }
            catch (WebException webExc)
            {
                HttpWebResponse failedResponse = (HttpWebResponse)webExc.Response;
                using (StreamReader reader = new StreamReader(webExc.Response.GetResponseStream()))
                {
                    string result = reader.ReadToEnd();
                   taskComplete.TrySetResult( failedResponse);
                }
            }
             return taskComplete.Task; 
        }
        public static Stream GetRequestStreamforPing(this HttpWebRequest request, int waitTime)
        {
            DefaultRequestTimeout = waitTime * 1000;           
            var dataReady = new AutoResetEvent(false);
            Stream stream = null;
            var callback = new AsyncCallback(delegate(IAsyncResult asynchronousResult)
            {                
                stream = (Stream)request.EndGetRequestStream(asynchronousResult);
                dataReady.Set();             
            });
            request.BeginGetRequestStream(callback, request);
            if (!dataReady.WaitOne(DefaultRequestTimeout))
            {
                return null;
            }
            return stream;
        }

        public static Task<Stream> GetRequestStream(this HttpWebRequest request)
        {
            var taskComplete = new TaskCompletionSource<Stream>();
            request.BeginGetRequestStream(asyncResponse =>
            {
                try
                {
                    HttpWebRequest responseRequest = (HttpWebRequest)asyncResponse.AsyncState;
                    Stream stream = responseRequest.EndGetRequestStream(asyncResponse);                   
                    taskComplete.TrySetResult(stream);
                }
                catch (WebException)
                {
                    taskComplete.TrySetResult(null);
                }
            }, request);
            return taskComplete.Task;
        }
    } 
}
