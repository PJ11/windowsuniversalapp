﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASContactResponse : ASCommandResponse
    {
        
        public ASContactResponse(HttpWebResponse httpResponse)
            : base(httpResponse)
        {
            TextReader tr = new StringReader(XmlString);
            responseXml = XDocument.Load(tr, LoadOptions.None);

            SetStatus();
        }

        public override void ParseResponse()
        {
            List<ServerSyncCommand> addCommands = GetServerAddsForFolder();
            //ToDo: Pankaj : Need to write parsing logic for response xml
        }


        public override Int32 GetStatus()
        {
            return status;
        }
    }
}
