﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    
    public class ASMoveItemsResponse:ASCommandResponse
    {
        private XDocument mResponseXml = null;
        public Int32 status = 0;
        public ASMoveItemsResponse(HttpWebResponse httpResponse)
            : base(httpResponse)
        {
           
            if (httpResponse.StatusCode == HttpStatusCode.OK)
            {
                if (XmlString != null && XmlString != "")
                {
                    mResponseXml = new XDocument();
                    TextReader tr = new StringReader(XmlString);
                    mResponseXml = XDocument.Load(tr, LoadOptions.None);
                    SetStatus();
                }
            }
        }

        public ASMoveItemsResponse(HttpStatusCode statusCode)
            : base (statusCode)
        {
        }

        private void SetStatus()
        {
            string ns = "{Move}";
            if(mResponseXml!=null)
            { 
            var moveItemsQ = mResponseXml.Descendants().First(p => p.Name == (ns + "Response"));
            if (moveItemsQ != null)
            {
                var statusQ = moveItemsQ.Descendants().First(p => p.Name == (ns + "Status"));
                if (statusQ != null)
                {
                    status = XmlConvert.ToInt32(statusQ.Value);
                }
            }
            }
        }
    }
}
