﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    // This class represents the FolderSync command
    // request specified in MS-ASCMD section 2.2.2.4.1.
    public class ASAttachmentRequest : ASCommandRequest
    {
        private string mfileReference = string.Empty;


        public ASAttachmentRequest(string fileReference)
        {
            Command = "ItemOperations";
            mfileReference = fileReference;
        }

        // This function generates an ASFolderSyncResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASAttachmentResponse(httpResp);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASAttachmentResponse(statusCode);
        }

        // This function generates the XML request body
        // for the FolderSync request.
        protected override void GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument attachmentXML = new XDocument(new XDeclaration("1.0", "utf-8", null));


            XNamespace n1 = "AirSyncBase";
            XNamespace n2 = "ItemOperations";

            //XElement ItemOperationsNode = new XElement("ItemOperations", new XAttribute(XNamespace.Xmlns + "airsyncbase", n1), new XAttribute(n2));



            XElement ItemOperationsNode = new XElement(n2 + "ItemOperations", new XAttribute(XNamespace.Xmlns + "airsyncbase", n1));
            attachmentXML.Add(ItemOperationsNode);


            XElement fetchNode = new XElement(n2 + "Fetch");
            ItemOperationsNode.Add(fetchNode);

            XElement storeNode = new XElement(n2 + "Store");
            storeNode.Value =  "Mailbox";
            fetchNode.Add(storeNode);


            XElement airSyncBase = new XElement(n1 + "FileReference");
            //airSyncBase.SetValue("RgAAAACVAIdXynHtR4UY1cZDuGnDBwCHmMYXVTamQ6ZF6R4yABPvAAAAYgUvAACHmMYXVTamQ6ZF6R4yABPvAAAAbWusAAAJ%3a1");
            airSyncBase.SetValue(mfileReference);
            fetchNode.Add(airSyncBase);

            String a = attachmentXML.ToString();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            attachmentXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();

        }
    }
}
