﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    // This class represents the Provision command
    // response specified in MS-ASPROV section 2.2.
    public class ASProvisionResponse : ASCommandResponse 
    {
        // This enumeration covers the Provision-
        // specific status values that can come from
        // the server.
        public enum ProvisionStatus
        {
            Success = 1,
            SyntaxError = 2,
            ServerError = 3,
            DeviceNotFullyProvisionable = 139,
            LegacyDeviceOnStrictPolicy = 141,
            ExternallyManagedDevicesNotAllowed = 145,
            TooManyDevicesProvisioned = 177
        }

        private bool isPolicyLoaded = false;
        private ASPolicy policy = null;
        private Int32 status = 0;

        public ASProvisionResponse(HttpWebResponse httpResponse) : base (httpResponse)
        {
            policy = new ASPolicy();
            isPolicyLoaded = policy.LoadXML(XmlString);
            SetStatus();
        }

        public ASProvisionResponse(HttpStatusCode statusCode)
            : base (statusCode)
        {
        }

        #region Property Accessors

        public bool IsPolicyLoaded
        {
            get
            {
                return isPolicyLoaded;
            }
        }

        public ASPolicy Policy
        {
            get
            {
                return policy;
            }
        }

        public Int32 Status
        {
            get
            {
                return status;
            }
        }

        #endregion

        // This function parses the response XML for
        // the Status element under the Provision element
        // and sets the status property according to the
        // value.
        private void SetStatus()
        {
            XDocument responseXml = new XDocument();
            TextReader tr = new StringReader(XmlString);
            responseXml = XDocument.Load(tr, LoadOptions.None);
            string ns = "{Provision}";

            var provisionQ = responseXml.Descendants().SingleOrDefault(p => p.Name == (ns + "Provision"));
            if (provisionQ != null)
            {
                var statusQ = provisionQ.Descendants().First(p => p.Name == (ns + "Status"));
                if (statusQ != null)
                {
                    status = XmlConvert.ToInt32(statusQ.Value);
                }
            }
        }
    }
}
