﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASAutoDiscoverRequest
    {
        private const string CONTENT_TYPE = "text/xml";
        private const string USER_AGENT = "Workspace";
        private const string METHOD = "POST";
        public ASAutoDiscoverRequest()
        {
            Command = "Autodiscover";
        }

        public string Command { get; set; }
        
        public string Username { get; set; }
        
        public string Password { get; set; }
        
        public string Url { get; set; }
        
        public string Email { get; set; }
        
        public string EASServerUrl { get; set; }

        protected string GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            XDocument autodiscoverXML = new XDocument();
            string payload = "";

            try
            {
                String auDisXML = String.Format("<Autodiscover xmlns=\"http://schemas.microsoft.com/exchange/autodiscover/mobilesync/requestschema/2006\"><Request><EMailAddress>{0}</EMailAddress><AcceptableResponseSchema>http://schemas.microsoft.com/exchange/autodiscover/mobilesync/responseschema/2006</AcceptableResponseSchema></Request></Autodiscover>",
                                              Email);

                payload = auDisXML;
            }
            catch (Exception e)
            {
                string msg = e.Message;
            }
            return payload;
        }

        public async Task<bool> GetResponse()
        {
            string payload = GenerateXMLPayload();
            HttpWebResponse webResponse = null;

            NetworkCredential credentials = new NetworkCredential
            {
                UserName = Username,
                Password = Password,
            };
            System.Diagnostics.Debug.WriteLine("trying Server: {0}, Username: {1}, Password: {2}", Url, Username, Password);
            string url = Url + "/autodiscover/autodiscover.xml";
            ASAutoDiscoverResponse ad_resp = null;
            try
            {
                System.Net.HttpWebRequest autoDiscoRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(url);
                autoDiscoRequest.ContentType = CONTENT_TYPE;
                autoDiscoRequest.Method = METHOD;
                autoDiscoRequest.Credentials = credentials;

                byte[] bytes = Encoding.UTF8.GetBytes(payload);
                autoDiscoRequest.Headers["ContentLength"] = bytes.Length.ToString();
                autoDiscoRequest.Headers["Translate"] = "F";
                autoDiscoRequest.Headers["UserAgent"] = USER_AGENT;

                // Write to the request stream.
                Stream requestStream = await autoDiscoRequest.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Dispose();

                webResponse = (HttpWebResponse)await autoDiscoRequest.GetResponseAsync();
                ad_resp = new ASAutoDiscoverResponse(webResponse);
                webResponse.Dispose();

            }
            catch (System.Security.SecurityException)
            {
                // This exception will happen when the max allowable retry count on EAS password has been hit.
                return false;
            }
            catch (WebException ex)
            {
                string msg = ex.Message;
            }
            if(null != ad_resp &&
                ad_resp.Succeeded)
            {
                System.Diagnostics.Debug.WriteLine("FOUND! Server: {0}, Username: {1}, Password: {2}", Url, Username, Password);
                EASServerUrl = ad_resp.Url;
                return true;
            }

            return false;
        }

    }
}
