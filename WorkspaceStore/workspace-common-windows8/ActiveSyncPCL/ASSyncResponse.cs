﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    // This class represents the Sync command response
    public class ASSyncResponse : ASCommandResponse
    {
        // This enumeration covers the possible Status
        // values for FolderSync responses.
        public enum SyncStatus
        {
            Success = 1,
            InvalidSyncKey = 3,
            ProtocolError = 4,
            ServerError = 5,
            ClientServerConversionError = 6,
            ServerOverwriteConflict = 7,
            ObjectNotFound = 8,
            SyncCannotComplete = 9,
            FolderHierarchyOutOfDate = 12,
            PartialSyncNotValid = 13,
            InvalidDelayValue = 14,
            InvalidSync = 15,
            Retry = 16
        }

        protected Int32 _status = 0;

        #region Property Accessors
        public Int32 Status
        {
            get
            {
                return _status;
            }
        }
        #endregion

        public ASSyncResponse(HttpWebResponse httpResponse, List<Folder> folders)
            : base(httpResponse)
        {
            if(httpResponse.StatusCode == HttpStatusCode.OK)
            {
                // Sync responses can be empty
                if (XmlString != "")
                {
                    _responseXml = new XDocument();
                    TextReader tr = new StringReader(XmlString);
                    _responseXml = XDocument.Load(tr, LoadOptions.None);


                    SetStatus();
                }
            }
        }

        public ASSyncResponse(HttpStatusCode statusCode)
            : base (statusCode)
        {
        }

        protected ASSyncResponse()
        {

        }

        public XDocument GetResponseXMLDoc()
        {
            return _responseXml;
        }
        
        //PLS Dont DELETE : We might need similar things later on
        // This function extracts the response status from the 
        // XML and sets the status property. We need to check whether we need to save it in folder
        protected virtual void SetStatus()
        {
            string ns = "{AirSync}";
            if (_responseXml != null)
            {
                var airSyncQ = _responseXml.Descendants().SingleOrDefault(p => p.Name == (ns + "Sync"));
                if (airSyncQ != null)
                {
                    var statusQ = airSyncQ.Descendants().SingleOrDefault(p => p.Name == (ns + "Status"));
                    if (statusQ != null)
                    {
                        _status = XmlConvert.ToInt32(statusQ.Value);
                    }
                }
            }
        }
    }
}
