﻿using System;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASSendMailRequest : ASCommandRequest
    {
        public ASSendMailRequest()
        {
            Command = "SendMail";
        }

        public StringBuilder MimeContent { get; set; }

        public string ClientID { get; set; }

        // This function generates an ASFolderSyncResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASSendMailResponse(httpResp);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASSendMailResponse(statusCode);
        }

        // This function generates the XML request body
        protected override void GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument sendMailXML = new XDocument(new XDeclaration("1.0", "utf-8", null));


            XNamespace n1 = "ComposeMail";

            XElement sendMailNode = new XElement(n1 + "SendMail");
            sendMailXML.Add(sendMailNode);


            XElement clientIdNode = new XElement(n1 + "ClientId");
            clientIdNode.Value = ClientID; // Increments for every mail sent
            sendMailNode.Add(clientIdNode);

            XElement saveinSentItemsNode = new XElement(n1 + "SaveInSentItems");
            sendMailNode.Add(saveinSentItemsNode);

            //if (_emailMessage == null)
            //    return;
            //String emailMessageString = _emailMessage.ToString();

            sendMailNode.Add(new XElement(n1 + "Mime", new XCData(MimeContent.ToString())));

            String a = sendMailXML.ToString();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            sendMailXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();

        }
    }
}
