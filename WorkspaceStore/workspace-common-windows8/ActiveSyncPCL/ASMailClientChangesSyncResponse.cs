﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASMailClientChangesSyncResponse:ASCommandResponse
    {
        private XDocument mResponseXml = null;
        public Int32 status = 0;
        private string mAirSyncNS;
        public ASMailClientChangesSyncResponse(HttpWebResponse httpResponse)
               : base(httpResponse)

        {
            mAirSyncNS = "{AirSync}";
            if (httpResponse.StatusCode == HttpStatusCode.OK)
            {
                if (XmlString != null && XmlString != "")
                {
                    mResponseXml = new XDocument();
                    TextReader tr = new StringReader(XmlString);
                    mResponseXml = XDocument.Load(tr, LoadOptions.None);
                    SetStatus();
                }
            }

        }

        public ASMailClientChangesSyncResponse(HttpStatusCode statusCode)
            : base (statusCode)
        {
        }

        private void SetStatus()
        {
            string ns = "{AirSync}";
            if (mResponseXml != null)
            {
                var airSyncQ = mResponseXml.Descendants().SingleOrDefault(p => p.Name == (ns + "Sync"));
                //var responses = mResponseXml.Descendants().SingleOrDefault(p => p.Name == (ns + "Responses"));
                if (airSyncQ != null)
                {
                    //var statusQ = responses.Descendants().Where(p => p.Name == (ns + "Status"));
                    var statusQ = airSyncQ.Descendants().FirstOrDefault(p => p.Name == (ns + "Status"));
                    if (statusQ != null)
                    {
                        status = XmlConvert.ToInt32(statusQ.Value);
                    }
                }
            }
        }

        public string GetSyncKeyForFolder(string folderId)
        {
            string folderSyncKey = "0";

            string ns = "{AirSync}";
            IEnumerable<XElement> collection = mResponseXml.Descendants().Where(p => p.Name == (ns + "Collection")).ToList();
            if (collection != null)
            {
                foreach (XElement coll in collection)
                {
                    XElement j = coll as XElement;
                    if (j != null && (j.Descendants().First(p => p.Name == (ns + "CollectionId")).Value).ToString() == folderId.ToString())
                    {
                        XElement syncKey = j.Descendants().First(p => p.Name == (ns + "SyncKey"));
                        folderSyncKey = (string)syncKey.Value;
                        break;
                    }
                }
            }

            return folderSyncKey;
        }
    }

    
}
