﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public enum EnumMailCommandType
    {
        Add,
        Delete,
        Change
    }
    public class ASMailClientChangesSyncRequest: ASCommandRequest
    {
        public ServerSyncCommand.ServerSyncCommandType CommandType { get; set; }
        public string mLastSyncKey { get; set; }
        public string mCollectionId { get; set; }
        public string mServerID { get; set; }
        public string mRead { get; set; }

        /// Delegate declaration.
        public delegate XDocument SetClientChangesInRequestXMLDelegate(ASMailClientChangesSyncRequest obj);
        // Define an Event based on the above Delegate
        public static event SetClientChangesInRequestXMLDelegate AppendClientChangesInRequestXML;
       
        public ASMailClientChangesSyncRequest()
        {
            Command = "Sync";
            
        }

        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASMailClientChangesSyncResponse(httpResp);
        }


        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASMailClientChangesSyncResponse(statusCode);
        }


        // This function generates the XML request body
        // for the FolderSync request.
        protected override void GenerateXMLPayload()
        {

            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            //We need to get the SyncXML from container App
            XDocument syncXML = AppendClientChangesInRequestXML(this);

            

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            syncXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();

        }
    }
    
}
