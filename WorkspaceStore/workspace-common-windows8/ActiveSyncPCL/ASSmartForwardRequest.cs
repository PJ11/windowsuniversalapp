﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASSmartForwardRequest : ASCommandRequest
    {
        public ASSmartForwardRequest()
        {
            Command = "SmartForward";
        }

        public StringBuilder MimeContent { get; set; }

        public string ClientID { get; set; }

        public string folderID { get; set; }

        public string itemID { get; set; }

        // This function generates an ASFolderSyncResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASSmartForwardResponse(httpResp);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASSmartForwardResponse(statusCode);
        }

        // This function generates the XML request body
        protected override void GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument sendMailXML = new XDocument(new XDeclaration("1.0", "utf-8", null));


            XNamespace n1 = "ComposeMail";

            XElement sendMailNode = new XElement(n1 + "SmartForward");
            sendMailXML.Add(sendMailNode);


            XElement clientIdNode = new XElement(n1 + "ClientId");
            clientIdNode.Value = ClientID; // Increments for every mail sent
            sendMailNode.Add(clientIdNode);

            XElement sourceNode = new XElement(n1 + "Source");
            //clientIdNode.Value = ClientID; // Increments for every mail sent
            sendMailNode.Add(sourceNode);

            XElement folderIDNode = new XElement(n1 + "FolderId");
            folderIDNode.Value = folderID; // Increments for every mail sent
            sourceNode.Add(folderIDNode);

            XElement itemIDNode = new XElement(n1 + "ItemId");
            itemIDNode.Value = itemID; // Increments for every mail sent
            sourceNode.Add(itemIDNode);

            
            XElement saveinSentItemsNode = new XElement(n1 + "SaveInSentItems");
            sendMailNode.Add(saveinSentItemsNode);

            

            

            sendMailNode.Add(new XElement(n1 + "Mime", new XCData(MimeContent.ToString())));

            String a = sendMailXML.ToString();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            sendMailXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();

        }
    }
}

