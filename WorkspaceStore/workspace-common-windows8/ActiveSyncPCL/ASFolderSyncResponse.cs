﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    // This class represents a FolderSync command
    // response as specified in MS-ASCMD section 2.2.2.4.2.
    public class ASFolderSyncResponse : ASCommandResponse
    {
        // This enumeration covers the possible Status
        // values for FolderSync responses.
        public enum FolderSyncStatus
        {
            Success = 1,
            ServerError = 6,
            InvalidSyncKey = 9,
            InvalidFormat = 10,
            UnknownError = 11,
            UnknownCode = 12
        }

        private XDocument responseXml = null;
        private Int32 status = 0;

        #region Property Accessors
        public Int32 Status
        {
            get
            {
                return status;
            }
        }
        #endregion

        public ASFolderSyncResponse(HttpWebResponse httpResponse)
            : base(httpResponse)
        {
            TextReader tr = new StringReader(XmlString);
            responseXml = XDocument.Load(tr, LoadOptions.None);

            SetStatus();
        }

        public ASFolderSyncResponse(HttpStatusCode statusCode)
            : base (statusCode)
        {
        }

        // This function updates a folder tree based on the
        // changes received from the server in the response.
        public void UpdateFolderTree(Folder rootFolder)
        {
            rootFolder.LastSyncTime = DateTime.Now.ToUniversalTime();

            try
            {
                // Get sync key
                string ns = "{FolderHierarchy}";

                var syncKeyQ = responseXml.Descendants().SingleOrDefault(p => p.Name == (ns + "SyncKey"));
                if (syncKeyQ != null)
                {
                    rootFolder.SyncKey = syncKeyQ.Value;
                     
                    // Process adds (new folders) first
                    //XElement addNodeQ = responseXml.Descendants().Take(p => p.Name == (ns + "Add"));
                    IEnumerable<XElement> addNodeQ = from query in responseXml.Descendants().Where(p => p.Name == (ns + "Add")).ToList() select query;

                    //var addNodeQ = responseXml.Descendants().SelectMany(p => p.Name == (ns + "Add")).ToList();
                    if (null != addNodeQ)
                    {
                        foreach (XElement addNode in addNodeQ)
                        {
                            XElement j = addNode as XElement;
                            if (null != j)
                            {
                                XElement nameQ = j.Descendants().First(p => p.Name == (ns + "DisplayName"));
                                XElement serverIdQ = j.Descendants().First(p => p.Name == (ns + "ServerId"));
                                XElement parentIdQ = j.Descendants().First(p => p.Name == (ns + "ParentId"));
                                XElement typeQ = j.Descendants().First(p => p.Name == (ns + "Type"));
                                rootFolder.AddFolder(nameQ.Value, serverIdQ.Value, parentIdQ.Value,
                                    (Folder.FolderType)XmlConvert.ToInt32(typeQ.Value));

                            }
                        }
                    }
                    // Process adds (new folders) first
                    IEnumerable<XElement> deleteQ = from query in responseXml.Descendants().Where(p => p.Name == (ns + "Delete")).ToList() select query;
                    if (null != deleteQ)
                    {
                        foreach (XElement deleteNode in deleteQ.Elements())
                        {
                            XElement j = deleteNode as XElement;
                            if (null != j)
                            {
                                XElement serverIdQ = j.Descendants().First(p => p.Name == (ns + "ServerId"));
                                Folder removeFolder = rootFolder.FindFolderById(j.Value);
                                if(null != removeFolder)
                                {
                                    removeFolder.Remove();
                                }

                            }
                        }
                    }
                    // Finally update existing folders
                    IEnumerable<XElement> updateQ = from query in responseXml.Descendants().Where(p => p.Name == (ns + "Update")).ToList() select query;
                    if (null != updateQ)
                    {
                        foreach (XElement updateNode in updateQ.Elements())
                        {
                            XElement j = updateNode as XElement;
                            if (null != j)
                            {
                                XElement nameQ = j.Descendants().First(p => p.Name == (ns + "DisplayName"));
                                XElement serverIdQ = j.Descendants().First(p => p.Name == (ns + "ServerId"));
                                XElement parentIdQ = j.Descendants().First(p => p.Name == (ns + "ParentId"));
                                XElement typeQ = j.Descendants().First(p => p.Name == (ns + "Type"));
                                Folder updateFolder = rootFolder.FindFolderById(serverIdQ.Value);
                                Folder updateParent = rootFolder.FindFolderById(parentIdQ.Value);

                                updateFolder.Update(nameQ.Value, updateParent,
                                    (Folder.FolderType)XmlConvert.ToInt32(typeQ.Value));
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                // Rather than attempting to recover, reset sync key 
                // and empty folders. The next FolderSync should
                // re-sync folders.
                
                //rootFolder.SyncKey = "0";
                //rootFolder.RemoveAllSubFolders();
            }

            rootFolder.SaveFolderInfo();
        }

        // This function extracts the response status from the 
        // XML and sets the status property.
        private void SetStatus()
        {
            string ns = "{FolderHierarchy}";

            var folderSyncQ = responseXml.Descendants().SingleOrDefault(p => p.Name == (ns + "FolderSync"));
            if (folderSyncQ != null)
            {
                var statusQ = folderSyncQ.Descendants().SingleOrDefault(p => p.Name == (ns + "Status"));
                if (statusQ != null)
                {
                    status = XmlConvert.ToInt32(statusQ.Value);
                }
            }
        }
    }
    class errorList
    {
        string displayname { get; set; }
        string server { get; set; }
    }

}
