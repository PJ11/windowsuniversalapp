﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    // This class represents an <Add>, <Change>,
    // <Delete>, or <SoftDelete> node in a
    // Sync command response.
    public class ServerSyncCommand
    {
        // This enumeration represents the types
        // of commands available.
        public enum ServerSyncCommandType
        {
            Invalid,
            Add,
            Change,
            Delete,
            SoftDelete
        }

        private ServerSyncCommandType type = ServerSyncCommandType.Invalid;
        private string serverId = null;
        private string itemClass = null;
        private XElement appDataXml = null;

        #region Property Accessors
        public ServerSyncCommandType Type
        {
            get
            {
                return type;
            }
        }

        public string ServerId
        {
            get
            {
                return serverId;
            }
        }

        public string ItemClass
        {
            get
            {
                return itemClass;
            }
        }

        public XElement AppDataXml
        {
            get
            {
                return appDataXml;
            }
        }
        #endregion

        public ServerSyncCommand(ServerSyncCommandType commandType, string id, XElement appData,
            string changedItemClass)
        {
            type = commandType;
            serverId = id;
            appDataXml = appData;
            itemClass = changedItemClass;
        }
    }
}
