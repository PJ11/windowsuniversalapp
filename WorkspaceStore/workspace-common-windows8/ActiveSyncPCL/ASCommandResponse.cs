﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    // This class represents a generic Exchange ActiveSync command response.
    public class ASCommandResponse : IDisposable
    {
        protected byte[] _wbxmlBytes = null;
        protected string _xmlString = null;
        protected HttpStatusCode _httpStatus = HttpStatusCode.OK;

        protected XDocument _responseXml = null;

        protected bool mDisposed = false;
        protected HttpWebResponse _httpResponse = null;

        #region Property Accessors
        public byte[] WbxmlBytes
        {
            get
            {
                return _wbxmlBytes;
            }
        }

        public string XmlString
        {
            get
            {
                return _xmlString;
            }
        }

        public HttpStatusCode HttpStatus
        {
            get
            {
                return _httpStatus;
            }
        }
        
        public XDocument ResponseXML
        {
            get
            {
                return _responseXml;
            }
        }

        public HttpWebResponse HttpWebResponse
        {
            get
            {
                return _httpResponse;
            }
        }
        #endregion

        public ASCommandResponse(HttpWebResponse httpResponse)
        {
            _httpResponse = httpResponse;

            _httpStatus = httpResponse.StatusCode;

            if (_httpStatus == HttpStatusCode.OK)
            {
                Stream responseStream = httpResponse.GetResponseStream();
                List<byte> bytes = new List<byte>();
                byte[] byteBuffer = new byte[256];
                int count = 0;

                // Read the WBXML data from the response stream
                // 256 bytes at a time.
                count = responseStream.Read(byteBuffer, 0, 256);
                while (count > 0)
                {
                    // Add the 256 bytes to the List
                    bytes.AddRange(byteBuffer);

                    if (count < 256)
                    {
                        // If the last read did not actually read 256 bytes
                        // remove the extra.
                        int excess = 256 - count;
                        bytes.RemoveRange(bytes.Count - excess, excess);
                    }

                    // Read the next 256 bytes from the response stream
                    count = responseStream.Read(byteBuffer, 0, 256);
                }

                // Dispose of Response Stream.
                responseStream.Dispose();

                _wbxmlBytes = bytes.ToArray();

                // Decode the WBXML
                _xmlString = DecodeWBXML(_wbxmlBytes);
            }
        }

        public ASCommandResponse(HttpStatusCode statusCode)
        {
            _httpResponse = null;
            _httpStatus = statusCode;
            _xmlString = "";
            _wbxmlBytes = null;
        }

        protected ASCommandResponse()
        {

        }

        // This function uses the ASWBXML class to decode
        // a WBXML stream into XML.
        private string DecodeWBXML(byte[] wbxml)
        {
            try
            {
                if(wbxml.Length > 0)
                {
                    ASWBXML decoder = new ASWBXML();
                    decoder.LoadBytes(wbxml);
                    return decoder.GetXml();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                ASError.ReportException(ex);
                return "";
            }
        }

        // This function gets the sync key for 
        // a folder.
        public string GetSyncKeyForFolder(string folderId)
        {
            string folderSyncKey = "0";
            if(null != _responseXml)
            {
                string ns = "{AirSync}";
                IEnumerable<XElement> collection = _responseXml.Descendants().Where(p => p.Name == (ns + "Collection")).ToList();
                if (collection != null)
                {
                    foreach (XElement coll in collection)
                    {
                        XElement j = coll as XElement;
                        if (j != null && (j.Descendants().First(p => p.Name == (ns + "CollectionId")).Value).ToString() == folderId.ToString())
                        {
                            XElement syncKey = j.Descendants().First(p => p.Name == (ns + "SyncKey"));
                            folderSyncKey = (string)syncKey.Value;
                            break;
                        }
                    }
                }
            }

            return folderSyncKey;
        }

        public int GetSyncStatusForFolder(string folderId)
        {
            int folderSyncStatus = 0;

            string ns = "{AirSync}";
            IEnumerable<XElement> collection = _responseXml.Descendants().Where(p => p.Name == (ns + "Collection")).ToList();
            if (collection != null)
            {
                foreach (XElement coll in collection)
                {
                    XElement j = coll as XElement;
                    if (j != null && (j.Descendants().First(p => p.Name == (ns + "CollectionId")).Value).ToString() == folderId.ToString())
                    {
                        XElement statusKey = j.Descendants().First(p => p.Name == (ns + "Status"));
                        folderSyncStatus = int.Parse(statusKey.Value);
                        break;
                    }
                }
            }

            return folderSyncStatus;
        }

        public bool IsMoreAvailable(string folderId)
        {
            bool isMoreAvailable = false;

            string ns = "{AirSync}";

            if (_responseXml != null)
            {
                IEnumerable<XElement> collection = _responseXml.Descendants().Where(p => p.Name == (ns + "Collection")).ToList();
                if (collection != null)
                {
                    foreach (XElement coll in collection)
                    {
                        XElement j = coll as XElement;
                        if (j != null && (j.Descendants().First(p => p.Name == (ns + "CollectionId")).Value).ToString() == folderId.ToString())
                        {
                            //XElement moreAvailableTag = j.Descendants().First(p => p.Name == (ns + "MoreAvailable"));
                            XElement moreAvailableTag = GetXElement(coll, ns + "MoreAvailable");
                            if (moreAvailableTag != null)
                                isMoreAvailable = true;
                        }
                    }
                }
            }
            return isMoreAvailable;
        }

        static protected XElement GetXElement(XElement xe, string tag)
        {
            if (xe != null && xe.HasElements && xe.Element(tag) != null)
                return xe.Element(tag);
            else
                return null;
        }

        public List<ServerSyncCommand> GetServerSyncCommandsForFolder(String folderId, XElement applicationDataNode, ServerSyncCommand.ServerSyncCommandType commandType)
        {

            List<ServerSyncCommand> commands = new List<ServerSyncCommand>();
            string ns = "{AirSync}";
            XElement collectionsNodeQ = _responseXml.Descendants(ns + "Collection").Where(p => p.Element(ns + "CollectionId").Value == folderId).ToList().ElementAt(0);

            IEnumerable<XElement> commandNodeQ = from query in collectionsNodeQ.Descendants().Where(p => p.Name == (ns + commandType)).ToList()
                                                 select query;
            if (null != commandNodeQ)
            {
                bool pullApplicationData = applicationDataNode == null;
                foreach (XElement commandNode in commandNodeQ)
                {
                    XElement j = commandNode as XElement;
                    if (null != j)
                    {
                        XElement serverIdNode = j.Descendants().First(p => p.Name == (ns + "ServerId"));
                        if(pullApplicationData)
                        {
                            applicationDataNode = GetXElement(j, ns + "ApplicationData");
                        }
                        if (serverIdNode != null && applicationDataNode != null && commandType == ServerSyncCommand.ServerSyncCommandType.Add)
                        {
                            ServerSyncCommand command = new ServerSyncCommand(
                                commandType,
                                serverIdNode.Value,
                                applicationDataNode,
                                null);

                            commands.Add(command);
                        }
                        else if (serverIdNode != null && commandType == ServerSyncCommand.ServerSyncCommandType.Delete)
                        {
                            ServerSyncCommand command = new ServerSyncCommand(
                                commandType,
                                serverIdNode.Value,
                                null,
                                null);

                            commands.Add(command);
                        }
                        else if (serverIdNode != null && commandType == ServerSyncCommand.ServerSyncCommandType.SoftDelete)
                        {
                            ServerSyncCommand command = new ServerSyncCommand(
                                commandType,
                                serverIdNode.Value,
                                null,
                                null);

                            commands.Add(command);
                        }
                        else if ((serverIdNode != null && commandType == ServerSyncCommand.ServerSyncCommandType.Change))
                        {
                            ServerSyncCommand command = new ServerSyncCommand(
                                commandType,
                                serverIdNode.Value,
                                applicationDataNode,
                                null);

                            commands.Add(command);

                        }
                    }
                }
            }
            return commands;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(mDisposed)
            {
                return;
            }
            if(disposing)
            {
                _httpResponse.Dispose();
            }
            mDisposed = true;
        }
    }
}
