﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveSyncPCL
{
    public class EASAccount
    {
        public static EASAccount gAccount = null;
        
        /// Delegate declaration.
        public delegate EASAccount GetAccInfoHandler(string email);
        
        // Define an Event based on the above Delegate
        public static event GetAccInfoHandler GetAccountInfo;
        
        public EASAccount()
        {
        }
        
        public static EASAccount accountForEmailAddress(string email)
        {

            // If GetAccountInfo event is not null .
            if (GetAccountInfo != null)
            {
                //Generate event and store account info in gAccount
                gAccount = GetAccountInfo(email);
            }
            return gAccount;
        }
        
        public int AccountId { get; set; }
        
        public string EmailAddress { get; set; }
        
        public string DomainName { get; set; }
        
        public string AuthUser { get; set; }
        
        public string AuthPassword { get; set; }
        
        public string ServerUrl { get; set; }
        
        public uint PolicyKey { get; set; }
        
        public string FolderSyncKey { get; set; }

        public string ProtocolVersion { get; set; }
    }
}
