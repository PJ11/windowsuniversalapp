﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASEmptyFolderContentResponse:ASCommandResponse
    {
        private XDocument responseXml = null;
       
        private Int32 status = 0;

        #region Property Accessors
        public Int32 Status
        {
            get
            {
                return status;
            }
        }
        #endregion

        public ASEmptyFolderContentResponse(HttpWebResponse httpResponse)
            : base(httpResponse)
        {
            TextReader tr = new StringReader(XmlString);
            responseXml = XDocument.Load(tr, LoadOptions.None);

            SetStatus();
        }

        public ASEmptyFolderContentResponse(HttpStatusCode statusCode)
            : base (statusCode)
        {
        }

        // This function extracts the response status from the 
        // XML and sets the status property.
        private void SetStatus()
        {
            var Status = (from d in responseXml.Descendants()
                                  where d.Name.LocalName == "Status"
                                  select d.Value).First();

            status = XmlConvert.ToInt32(Status);
            
        }
    }
}
