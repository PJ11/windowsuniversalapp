﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASFolderOperationResponse: ASCommandResponse
    {
        private XDocument responseXml = null;
        private Int32 status = 0;
        private string mAirSyncNS;

        public ASFolderOperationResponse(HttpWebResponse httpResponse)
               : base(httpResponse)

        {
            mAirSyncNS = "{AirSync}";
            if (XmlString != "")
            {
                responseXml = new XDocument();
                TextReader tr = new StringReader(XmlString);
                responseXml = XDocument.Load(tr, LoadOptions.None);
                SetStatus();
            }

        }

        public ASFolderOperationResponse(HttpStatusCode statusCode)
            : base (statusCode)
        {
        }

        private void SetStatus()
        {
            if (responseXml != null)
            {
                var airSyncQ = responseXml.Descendants().SingleOrDefault(p => p.Name == (mAirSyncNS + "Responses"));
                if (airSyncQ != null)
                {
                    var statusQ = airSyncQ.Descendants().SingleOrDefault(p => p.Name == (mAirSyncNS + "Status"));
                    {
                        status = XmlConvert.ToInt32(statusQ.Value);
                    }
                }
            }

        }
    }
}
