﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASMoveItemsRequest:ASCommandRequest
    {
        List<ASMoveItemsRequest> _items = null;
        // This function generates an ASSyncResponse from an
        // HTTP response.
         #region Property Accessors
        public string msgId { get; set; }
        public string sourceFolderId { get; set; }
        public string destFolderId { get; set; }

        public List<ASMoveItemsRequest> Items
        {
            get;set;
        }

        #endregion
        public ASMoveItemsRequest()
        {
            this.Command = "MoveItems";
           // this._items = moveItems;
        }
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASMoveItemsResponse(httpResp);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASMoveItemsResponse(statusCode);
        }


        // This function generates the XML request body
        // for the Sync request.
        protected override void GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument syncXML = new XDocument(new XDeclaration("1.0", "utf-8", null));

            XNamespace pingNS = Namespaces.moveItemsNamespace;

            XElement syncNode = new XElement(pingNS + "MoveItems"/*,
                new XAttribute(XNamespace.None + Xmlns.pingXmlns, pingNS)*/);

            syncXML.Add(syncNode);

            // Only add a collections node if there are folders in the request.
            // If omitting, there should be a Partial element.


            if (Items.Count == 0)
                throw new ArgumentException(
                    "Sync requests must specify collections or include the Partial element.");

            if (Items.Count > 0)
            {
                foreach (var item in Items)
                {
                    XElement foldersNode = new XElement(pingNS + "Move");
                    syncNode.Add(foldersNode);
                    XElement msgID = new XElement(pingNS + "SrcMsgId", new XText(item.msgId));
                    foldersNode.Add(msgID);

                    XElement srcFolderId = new XElement(pingNS + "SrcFldId", new XText(item.sourceFolderId));
                    foldersNode.Add(srcFolderId);

                    XElement destFolderID = new XElement(pingNS + "DstFldId", new XText(item.destFolderId));
                    foldersNode.Add(destFolderID);
                }
            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            syncXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();

        }
    }
}
