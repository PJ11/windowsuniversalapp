﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveSyncPCL
{
    public class ActiveSyncManager
    {
        static ActiveSyncManager mASMgr = null;
        EASAccount mAccount = null;
        public static int ASPingHeartBeatInterval=60;
        public ActiveSyncManager(string emailAddress)
        {

        }

        public static ActiveSyncManager sharedManagerForEmailAddress(string emailAddress)
        {
            if(mASMgr == null)
            {
                mASMgr = new ActiveSyncManager(emailAddress);
            }
            mASMgr.Account = EASAccount.accountForEmailAddress(emailAddress);
            if(null == mASMgr.Account)
            {
                mASMgr.Account = new EASAccount();
                mASMgr.Account.EmailAddress = emailAddress;
            }
            return mASMgr;
        }

        public async Task<bool> discoverUserInternal(string username, string password, string hostUrl, bool ccmProvidedHostName)
        {
            ASAutoDiscoverRequest req = new ASAutoDiscoverRequest();
            req.Email = Account.EmailAddress;
            req.Password = password;
            req.Username = username;
            req.Url = hostUrl;
            
            bool result = await req.GetResponse();
            System.Diagnostics.Debug.WriteLine("AUTODISCOVER RESULT = {0}", result);
            if(result)
            {
                Account.AuthUser = req.Username;
                Account.AuthPassword = req.Password;
                if (ccmProvidedHostName)
                {
                    Account.ServerUrl = hostUrl + "/Microsoft-Server-ActiveSync";
                }
                else
                {
                    // CCM did NOT provide host name.  It was determined via an autodiscover
                    Account.ServerUrl = req.EASServerUrl;
                }
                Account.EmailAddress = req.Email;
            }
            // TODO Set up SyncKey and PolicyKey
            return result;

        }

        public EASAccount Account 
        {
            get
            {
                return mAccount;
            }
            internal set
            {
                mAccount = value;
            }
        }
    }
}
