﻿using System;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASSearchMessageRequest : ASCommandRequest
    {
        public String Search { get; set; }

        public FolderSyncOptions SyncOptions { get; set; }

        public string CollectionId { get; set; }

        public ASSearchMessageRequest()
        {
            Command = "Search";
        }

        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASSearchMessageResponse(httpResp);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASSearchMessageResponse(statusCode);
        }

        protected override void GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument requestXML = new XDocument(new XDeclaration("1.0", "utf-8", null));
            XNamespace airsyncbasenamespace = Namespaces.airSyncBaseNamespace;
            XNamespace airsyncnamespace = Namespaces.airSyncNamespace;
            XNamespace searchNameSpace = Namespaces.searchNamespace;
            XElement searchNode = new XElement(searchNameSpace + "Search",
                new XAttribute(XNamespace.Xmlns + Xmlns.searchXmlns, Namespaces.searchNamespace)//,
                //new XAttribute(XNamespace.None + Xmlns.airSyncXmlns, Namespaces.airSyncNamespace),
                //new XAttribute(XNamespace.Xmlns + Xmlns.airSyncBaseXmlns, Namespaces.airSyncBaseNamespace)
                );

            requestXML.Add(searchNode);

            // Only add a collections node if there are folders in the request.
            // If omitting, there should be a Partial element.

            XElement storeNode = new XElement(searchNameSpace + "Store");
            searchNode.Add(storeNode);

            XElement nameNode = new XElement(searchNameSpace + "Name", "Mailbox");
            storeNode.Add(nameNode);

            XElement queryNode = new XElement(searchNameSpace + "Query");
            
            {
                // The query
                XElement andNode = new XElement(searchNameSpace + "And");
                XElement freeTextNode = new XElement(searchNameSpace + "FreeText", Search);
                andNode.Add(freeTextNode);
                XElement folderIdNode = new XElement(airsyncnamespace + "CollectionId", new XText(CollectionId));
                andNode.Add(folderIdNode);
                queryNode.Add(andNode);
            }

            storeNode.Add(queryNode);

            XElement optionsNode = new XElement(searchNameSpace + "Options");
            storeNode.Add(optionsNode);
            {
                // build options
                XElement rebuildResultsNode = new XElement(searchNameSpace + "RebuildResults");
                optionsNode.Add(rebuildResultsNode);
                if (SyncOptions.BodyPreference != null && SyncOptions.BodyPreference[0] != null)
                {
                    XElement BodyPreferenceNode = new XElement(airsyncbasenamespace + "BodyPreference");
                    optionsNode.Add(BodyPreferenceNode);

                    int bpType = (int)SyncOptions.BodyPreference[0].Type;
                    XElement bpTypeNode = new XElement(airsyncbasenamespace + "Type", new XText(bpType.ToString()));
                    BodyPreferenceNode.Add(bpTypeNode);

                    if (SyncOptions.BodyPreference[0].TruncationSize > 0)
                    {
                        XElement truncationSizeNode = new XElement(airsyncbasenamespace + "TruncationSize", new XText(SyncOptions.BodyPreference[0].TruncationSize.ToString()));
                        BodyPreferenceNode.Add(truncationSizeNode);
                    }
                }
            }

           
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            requestXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();
        }
    }
}
