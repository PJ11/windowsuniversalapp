﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASAutoDiscoverResponse
    {
        static bool success = false;
        public ASAutoDiscoverResponse(HttpWebResponse webResponse)
        {
            string result = "";
            HttpStatusCode http_result = webResponse.StatusCode;
            if(!success)
            {
                if (http_result == HttpStatusCode.OK && webResponse.Method == "POST")
                {
                    Stream resp_stream = webResponse.GetResponseStream();
                    StreamReader reader = new StreamReader(resp_stream);
                    result = reader.ReadToEnd();
                    //System.Diagnostics.Debug.WriteLine(result);

                    //reader.Dispose();
                    resp_stream.Dispose();

                    string displayName = "";
                    string emailAddress = "";
                    string mobileSyncUrl = "";
                    string culture = "";
                    if (retrieveAutoDiscoverData(result, ref culture, ref displayName, ref emailAddress, ref mobileSyncUrl))
                    {
                        Culture = culture;
                        DisplayName = displayName;
                        EmailAddress = emailAddress;
                        Url = mobileSyncUrl;

                        System.Diagnostics.Debug.WriteLine("DisplayName:{0}  EmailAddress:{1}  Url:{2}  Culture:{3}", displayName, emailAddress, mobileSyncUrl, culture);

                        success = true;
                    }

                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Response = {0}", http_result);
                }
            }
            else
            {

            }
        }

        bool retrieveAutoDiscoverData(string xml, ref string culture, ref string displayName, ref string emailAddress, ref string mobileSyncUrl)
        {
            bool succeeded = false;
            TextReader tr = new StringReader(xml);
            XDocument responseXml = XDocument.Load(tr, LoadOptions.None);
            string ns = "{http://schemas.microsoft.com/exchange/autodiscover/mobilesync/responseschema/2006}";

            try
            {
                XElement cultureQ = responseXml.Descendants().First(p => p.Name == (ns + "Culture"));
                if (null != cultureQ)
                {
                    culture = cultureQ.Value;
                }

                XElement displayNameQ = responseXml.Descendants().First(p => p.Name == (ns + "DisplayName"));
                if (null != displayNameQ)
                {
                    displayName = displayNameQ.Value;
                }

                XElement emailQ = responseXml.Descendants().First(p => p.Name == (ns + "EMailAddress"));
                if (null != emailQ)
                {
                    emailAddress = emailQ.Value;
                }

                // Can there be more than 1 <Server> element? Check <Type>MobileSync</Type>?  iOS doesn't.
                XElement serversQ = responseXml.Descendants().FirstOrDefault(p => p.Name == (ns + "Server"));
                if (null != serversQ)
                {
                    XElement urlQ = serversQ.Descendants().First(p => p.Name == (ns + "Url"));
                    if (null != urlQ)
                    {
                        mobileSyncUrl = urlQ.Value;
                    }
                }
                succeeded = true;
            }
            catch (Exception)
            {

            }
            return succeeded;
        }

        public bool Succeeded
        {
            get
            {
                return success;
            }
        }

        public string Culture { get;  protected set; }
        public string DisplayName { get; protected set; }
        public string EmailAddress { get; protected set; }
        public string Url { get; protected set; }
    }
}
