﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public enum EASSyncFlags
    {
        Unknown = 0,
        InitialSync = 1,
        SyncForWindowSizeChange = 2,
        SyncForPingResponse = 3,
        SyncForEventCreation = 4
    }

    public class EASCollectionChangedEventArgs : EventArgs
    {
        public bool Cancel { get; set; }
        public object Context { get; protected set; }
        public List<ServerSyncCommand> Adds { get; protected set; }
        public List<ServerSyncCommand> Changes { get; protected set; }
        public List<ServerSyncCommand> Deletes { get; protected set; }
        public List<ServerSyncCommand> SoftDeletes { get; protected set; }
        public Folder Folder { get; protected set; }
        public EASSyncFlags Flags { get; protected set; }
        public EASCollectionChangedEventArgs(List<ServerSyncCommand> adds, List<ServerSyncCommand> changes, List<ServerSyncCommand> deletes, List<ServerSyncCommand> softDeletes, Folder folder, EASSyncFlags flags, object context)
        {
            Adds = adds;
            Changes = changes;
            Deletes = deletes;
            SoftDeletes = softDeletes;
            Folder = folder;
            Flags = flags;
            Context = context;
        }

    }

    public class EASCommunicationsErrorEventArgs : EventArgs
    {
        public ASCommandResponse CommandResponse { get; private set; }

        public EASCommunicationsErrorEventArgs(ASCommandResponse commandResponse)
        {
            CommandResponse = commandResponse;
        }
    }

    public class EASTriggerSendPingEventArgs : EventArgs
    {
        public List<ActiveSyncPCL.Folder> Folders { get; private set; }

        public EASTriggerSendPingEventArgs(List<ActiveSyncPCL.Folder> folders)
        {
            Folders = folders;
        }
    }

    public class EASHandlePingResultEventArgs : EventArgs
    {
        public ASCommandResponse CommandResponse { get; private set; }

        public List<ActiveSyncPCL.Folder> Folders { get; private set; }

        public EASHandlePingResultEventArgs(ASCommandResponse commandResponse, List<ActiveSyncPCL.Folder> folders)
        {
            CommandResponse = commandResponse;
            Folders = folders;
        }
    }

    public class EASResyncFolderEventArgs : EventArgs
    {
        public ActiveSyncPCL.Folder Folder { get; private set; }
        
        public EASResyncFolderEventArgs(ActiveSyncPCL.Folder folder)
        {
            Folder = folder;
        }
    }

    public class ConstructOutgoingMessageEventArgs : EventArgs
    {
        public string ClientId { get; private set; }

        public int FolderId { get; private set; }

        public StringBuilder Content { get; private set; }

        public int ActionType { get; private set; }

        public List<object> Attachments { get; private set; }

        public ConstructOutgoingMessageEventArgs(string clientId, int folderId, StringBuilder content, int actionType, List<object> attachments)
        {
            ClientId = clientId;
            FolderId = folderId;
            Content = content;
            ActionType = actionType;
            Attachments = attachments;
        }
    }

    public class DeleteOutgoingMessageEventArgs : EventArgs
    {
        public string ClientId { get; private set; }

        public DeleteOutgoingMessageEventArgs(string clientId)
        {
            ClientId = clientId;
        }
    }

    public delegate void EASCollectionChangedEventHandler(object sender, EASCollectionChangedEventArgs e);

    public delegate void EASCommunicationsErrorHandler(object sender, EASCommunicationsErrorEventArgs e);

    public delegate void EASTriggerSendPingEventHandler(object sender, EASTriggerSendPingEventArgs e);
   
    public delegate void EASHandlePingResultEventHandler(object sender, EASHandlePingResultEventArgs e);

    public delegate void EASResyncFolder(object sender,  EASResyncFolderEventArgs e);

    public delegate void ConstructOutgoingMessageHandler(object sender, ConstructOutgoingMessageEventArgs e);

    public delegate void DeleteOutgoingMessageHandler(object sender, DeleteOutgoingMessageEventArgs e);

    /// <summary>
    /// This delegate will call into the Application in order to retrieve a list of ActiveSyncPCL Folder objects
    /// Required because ActiveSyncPCL should know nothing about client
    /// </summary>
    /// <returns></returns>
    public delegate List<Folder> EASGetRequestFoldersHandler ();
    public /*abstract*/ class ActiveSyncBase
    {
        protected static Device deviceToProvision = null;

        protected static Folder rootFolder = null;
        static ActiveSyncBase instance = null;
        public virtual Task<ASOptionsResponse> getOptions(EASAccount acct) { return null; }
        public virtual Task<ASProvisionResponse> provision(EASAccount acct) { return null; }
        public virtual Task provisionUser(string userName, string password, string fullServerUrl) { return null; }
        public virtual Task<EASAccount> discoverUser(string emailAddressFromCCM, string serverUrlFromCCM, string domainFromCCM, string usernameFromCCM, string password) { return null; }
        public virtual Task<Byte[]> DownloadAttachment(string fileReference) { return null; }
        public virtual Task<ASFolderSyncResponse> SyncFolderStructure() { return null; }
        public virtual Task<bool> SyncCalendar() { return null; }
        public virtual Task<bool> SyncContact() { return null; }
        public virtual Task<bool> SyncMail(object sender) { return null; }
        public virtual Task<bool> SyncMailBasedOnFolder(Folder folder) { return null; }
        public virtual Task<bool> SyncMailBasedOnFolder(string folderName, int folderParentID, object sender = null, EASSyncFlags flags = EASSyncFlags.Unknown) { return null; }
        public virtual Task<string> SendMeetingRequest(Folder folder, ASMeetingRequest asreq) { return null; }
        public virtual Task<List<Object>> SearchMessagesOnServer(int folderId, string searchString) { return null; }
        //public /*abstract*/ Task<XDocument> GetFolderData(List<Folder> folder, EASAccount acct, Device deviceToProvision);
        public virtual Task<bool> GetFolderChanges(Folder folder, EASAccount acct, Device device, object sender = null, EASSyncFlags flags = EASSyncFlags.Unknown) { return null; }
        public virtual Task<ASPingResponse> SendPing(int heartBeatInSeconds, List<Folder> folders) { return null; }
        public virtual Task<ASMoveItemsResponse> MoveEmailItems(List<ASMoveItemsRequest> folders) { return null; }
        public virtual Task<ASMeetingResponseCommandResponse> SendMeetingResponseRequest(string userResponse, string collectionId, string requestId) { return null; }
        public virtual  Task<ASDeleteItemsResponse> DeleteEmaiItems(string folderName, int folderParentID, List<ASDeleteItemsRequest> serverId) { return null; }
        public static ActiveSyncBase Instance
        {
            get
            {
                return instance;
            }
            set
            {
                instance = value;
            }
        }

        protected static CookieContainer _EASCookieJar = new CookieContainer();

        public static CookieContainer EASCookieJar
        {
            get
            {
                return ActiveSyncBase._EASCookieJar;
            }
        }
    }
}
