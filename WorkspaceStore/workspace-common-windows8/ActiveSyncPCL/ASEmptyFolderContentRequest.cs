﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASEmptyFolderContentRequest : ASCommandRequest
    {
        private string mCollectionID = string.Empty;


        public ASEmptyFolderContentRequest(string collectionID)
        {
            Command = "ItemOperations";
            mCollectionID = collectionID;
        }

        // This function generates an ASFolderSyncResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASEmptyFolderContentResponse(httpResp);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASEmptyFolderContentResponse(statusCode);
        }


        // This function generates the XML request body
        // for the FolderSync request.
        protected override void GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument emptyFolderContentXML = new XDocument(new XDeclaration("1.0", "utf-8", null));


            XNamespace airSyncNS = "AirSync";
            XNamespace itemOperationsNS = "ItemOperations";

            
            XElement ItemOperationsNode = new XElement(itemOperationsNS + "ItemOperations", new XAttribute(XNamespace.Xmlns + "airsync", airSyncNS));
            emptyFolderContentXML.Add(ItemOperationsNode);


            XElement emptyFolderNode = new XElement(itemOperationsNS + "EmptyFolderContents");
            ItemOperationsNode.Add(emptyFolderNode);

            XElement collectionIdNode = new XElement(airSyncNS + "CollectionId", new XText(mCollectionID));
            emptyFolderNode.Add(collectionIdNode);


            String a = emptyFolderContentXML.ToString();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            emptyFolderContentXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();

        }
    }
}
