﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    
    public class ASDeleteItemsRequest : ASCommandRequest
    {
  
       public EnumContactCommandType CommandType { get; set; }
       public string SyncKey { get; set; }
       public string CollectionId { get; set; }       
       public string serverId { get; set; }

      //for deleteing item from "deleted items", we need this variable. We cant rely in collection IDs as it keeps chaniging
       public string deletesAsMoves { get; set; }
  
       //List<ASDeleteItemsRequest> itemList = null;
       public List<ASDeleteItemsRequest> Items
       { get; set; }

        public ASDeleteItemsRequest()
        {
            Command = "Sync";
            deletesAsMoves = "1";
            //this.itemList = new List<ASDeleteItemsRequest>();
        }

        // This function generates an ASFolderSyncResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASDeleteItemsResponse(httpResp);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASDeleteItemsResponse(statusCode);
        }

        // This function generates the XML request body
        // for the FolderSync request.
        protected override void GenerateXMLPayload()
        {

            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            // Otherwise, use the properties to build the XML and then WBXML encode it
            
            XDocument syncXML = new XDocument(new XDeclaration("1.0", "utf-8", null));

            XNamespace syncnamespace = Namespaces.airSyncNamespace;
            XNamespace syncbasenamespace = Namespaces.airSyncBaseNamespace;

            XElement syncNode = new XElement(syncnamespace + "Sync",
                new XAttribute(XNamespace.None + Xmlns.airSyncXmlns, Namespaces.airSyncNamespace),
                new XAttribute(XNamespace.Xmlns + Xmlns.airSyncBaseXmlns, Namespaces.airSyncBaseNamespace));

            syncXML.Add(syncNode);

            // Only add a collections node if there are folders in the request.
            // If omitting, there should be a Partial element.

            XElement collectionsNode = new XElement(syncnamespace + "Collections");
            syncNode.Add(collectionsNode);

            XElement collectionNode = new XElement(syncnamespace + "Collection");
            collectionsNode.Add(collectionNode);

            XElement syncKeyNode = new XElement(syncnamespace + "SyncKey", new XText(SyncKey));
            collectionNode.Add(syncKeyNode);

            XElement collectionIdNode = new XElement(syncnamespace + "CollectionId", new XText(CollectionId));
            collectionNode.Add(collectionIdNode);
           
            if (CommandType == EnumContactCommandType.Delete)
            {
                // XElement deleteAsMovesNode = null;
                //check delete items folder id
                //if (CollectionId == "3")
                //{
                //    deleteAsMovesNode = new XElement(syncnamespace + "DeletesAsMoves", "0");
                //    collectionNode.Add(deleteAsMovesNode);
                //}
                //else
                //{
                //    deleteAsMovesNode = new XElement(syncnamespace + "DeletesAsMoves", "1");
                //    collectionNode.Add(deleteAsMovesNode);
                //}

                XElement deleteAsMovesNode = new XElement(syncnamespace + "DeletesAsMoves", deletesAsMoves);
                collectionNode.Add(deleteAsMovesNode);

                XElement getChangesNode = new XElement(syncnamespace + "GetChanges", "1");
                collectionNode.Add(getChangesNode);

                XElement windowSizeNode = new XElement(syncnamespace + "WindowSize", "512");
                collectionNode.Add(windowSizeNode);

                XElement commandsNode = new XElement(syncnamespace + "Commands");
                collectionNode.Add(commandsNode);
                if (Items.Count > 0)
                {
                    foreach (var item in Items)
                    {
                        XElement AddNode = new XElement(syncnamespace + "Delete");
                        commandsNode.Add(AddNode);

                        XElement serverIDNode = new XElement(syncnamespace + "ServerId", item.serverId );
                        AddNode.Add(serverIDNode);
                    }
                }
                               
            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            syncXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();

        }
    }
}

