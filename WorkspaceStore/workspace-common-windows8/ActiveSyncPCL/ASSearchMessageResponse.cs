﻿using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASSearchMessageResponse :  ASCommandResponse
    {
        private XDocument mResponseXml = null;

        public int Status { get; protected set; }

        public ASSearchMessageResponse(HttpWebResponse httpResponse)
            : base (httpResponse)
        {
            if (httpResponse.StatusCode == HttpStatusCode.OK)
            {
                if (XmlString != null && XmlString != "")
                {
                    mResponseXml = new XDocument();
                    TextReader tr = new StringReader(XmlString);
                    mResponseXml = XDocument.Load(tr, LoadOptions.None);
                    SetStatus();
                }
            }
        }

        public ASSearchMessageResponse(HttpStatusCode statusCode)
            : base (statusCode)
        {
        }

        private void SetStatus()
        {
            string ns = "{Search}";

            var folderSyncQ = mResponseXml.Descendants().SingleOrDefault(p => p.Name == (ns + "Search"));
            if (folderSyncQ != null)
            {
                var statusQ = folderSyncQ.Descendants().First(p => p.Name == (ns + "Status"));
                if (statusQ != null)
                {
                    Status = XmlConvert.ToInt32(statusQ.Value);
                }
            }
        }
    }
}
