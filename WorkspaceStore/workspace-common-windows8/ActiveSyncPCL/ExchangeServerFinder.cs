﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveSyncPCL
{
    public class ExchangeServerFinder
    {
        List<string> hostsArray = new List<string>();
        List<string> usernameArray = new List<string>();
        string mPassword;

        int hostsArrayIndex = 0;
        int usernameArrayIndex = 0;
        ActiveSyncManager mManager = null;

        public ExchangeServerFinder(ActiveSyncManager manager, string password)
        {
            mManager = manager;
            mPassword = password;
        }

        public async Task<bool> tumbleExchangeHost(string hostName, string emailAddress, string domain, string userName)
        {
            bool ccmProvidedHostName = false;
            //Build Hostname
            if (hostName != null && hostName.Length > 0)
            {
                // If we are here then the CCM server has provided a valid Server Host.  We need to always use this one.
                hostsArray.Add(hostName);
                //hostsArray.Add("autodiscover." + hostName);
                ccmProvidedHostName = true;
            }
            else
            {
                string mail_domain = emailAddress.Split('@')[1];
                
                if (null != mail_domain && mail_domain.Length > 0)
                {
                    hostsArray.Add(mail_domain);
                    hostsArray.Add("autodiscover." + mail_domain);
                    hostsArray.Add("mail." + mail_domain);
                }
            }

            // Build up the possible usernames
            if(userName != null && userName.Length > 0)
            {
                if (null != domain && domain.Length > 0)
                {
                    usernameArray.Add(domain + "\\" + userName);
                }
                usernameArray.Add(userName);
            }
            else
            {
                string[] comps = userName.Split('@');//if the passed in username is empty.
                
                if (null != comps && comps.Count() > 1)
                {
                    usernameArray.Add(comps[0]);
                    usernameArray.Add(domain + "\\" + comps[0]);//if the passed in username is empty.
                }
            }
            hostsArrayIndex = 0;
            usernameArrayIndex = 0;
            bool result = await attemptNextLogin(ccmProvidedHostName);
            return result;
        }

        async Task<bool> attemptNextLogin(bool ccmProvidedHostName)
        {
            bool discoveredUser = false;
            while(hostsArrayIndex < hostsArray.Count && !discoveredUser)
            {
                if (usernameArrayIndex >= usernameArray.Count)
                {
                    usernameArrayIndex = 0;
                    hostsArrayIndex += 1;
                }
                if (hostsArrayIndex <= hostsArray.Count - 1)
                {
                    string url = "https://" + hostsArray[hostsArrayIndex];
                    discoveredUser = await mManager.discoverUserInternal(usernameArray[usernameArrayIndex], mPassword, url, ccmProvidedHostName);
                    usernameArrayIndex += 1;
                }
            }
            return discoveredUser;
        }
    }
}
