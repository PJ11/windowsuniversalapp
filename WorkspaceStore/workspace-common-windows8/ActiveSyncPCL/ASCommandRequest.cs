﻿//#define OLDWAY

using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Linq;
using System.Net.Http;

namespace ActiveSyncPCL
{
    // This structure is used to store command-specific
    // parameters (MS-ASCMD section 2.2.1.1.1.2.5)
    public struct CommandParameter
    {
        public string Parameter;
        public string Value;
    }

    // This class represents a generic Exchange ActiveSync command request.
    public class ASCommandRequest
    {
        //public delegate void RESTSuccessCallback(Stream stream);
        //public delegate void RESTErrorCallback(String reason);
        private int heartbeatinterval;
        private NetworkCredential credential = null;
        private string server = null;
        private bool useSSL = true;
        private byte[] wbxmlBytes = null;
        private string xmlString = null;
        private string protocolVersion = null;
        private string requestLine = null;
        private bool useEncodedRequestLine = true;
        private string command = null;
        private string user = null;
        private string deviceID = null;
        private string deviceType = null;
        private UInt32 policyKey = 0;
        private CommandParameter[] parameters = null;
        private HttpWebRequest mWebRequest = null;

        #region Property Accessors
        public Int32 HttpWebRequestTimeInterval
        {
            get
            {
                return heartbeatinterval;
            }
            set
            {
                heartbeatinterval = value;
            }
        }
        public NetworkCredential Credentials
        {
            get
            {
                return credential;
            }
            set
            {
                credential = value;
            }
        }

        public string Server
        {
            get
            {
                return server;
            }
            set
            {
                server = value;
            }
        }

        public bool UseSSL
        {
            get
            {
                return useSSL;
            }
            set
            {
                useSSL = value;
            }
        }

        public byte[] WbxmlBytes
        {
            get
            {
                return wbxmlBytes;
            }
            set
            {
                wbxmlBytes = value;
                // Loading WBXML bytes causes immediate decoding
                xmlString = DecodeWBXML(wbxmlBytes);
            }
        }

        public string XmlString
        {
            get
            {
                return xmlString;
            }
            set
            {
                xmlString = value;
                // Loading XML causes immediate encoding
                wbxmlBytes = EncodeXMLString(xmlString);
            }
        }

        public string ProtocolVersion
        {
            get
            {
                return protocolVersion;
            }
            set
            {
                protocolVersion = value;
            }
        }

        public string RequestLine
        {
            get
            {
                // Generate on demand
                BuildRequestLine();
                return requestLine;
            }
            set
            {
                requestLine = value;
            }
        }

        public bool UseEncodedRequestLine
        {
            get
            {
                return useEncodedRequestLine;
            }
            set
            {
                useEncodedRequestLine = value;
            }
        }

        public string Command
        {
            get
            {
                return command;
            }
            set
            {
                command = value;
            }
        }

        public string User
        {
            get
            {
                return user;
            }
            set
            {
                user = value;
            }
        }

        public string DeviceID
        {
            get
            {
                return deviceID;
            }
            set
            {
                deviceID = value;
            }
        }

        public string DeviceType
        {
            get
            {
                return deviceType;
            }
            set
            {
                deviceType = value;
            }
        }

        public UInt32 PolicyKey
        {
            get
            {
                return policyKey;
            }
            set
            {
                policyKey = value;
            }
        }

        public CommandParameter[] CommandParameters
        {
            get
            {
                return parameters;
            }
            set
            {
                parameters = value;
            }
        }

        public string UserAgent { get; set; }
        #endregion

        public void Abort()
        {
            if(null != mWebRequest)
            {
                mWebRequest.Abort();
            }
        }

        // This function sends the request and returns
        // the response.
        public async Task<ASCommandResponse> GetResponse()
        {
            GenerateXMLPayload();

            if (Credentials == null || Server == null || ProtocolVersion == null || WbxmlBytes == null)
                throw new InvalidDataException("ASCommandRequest not initialized.");
            //string uriString = string.Format("{0}//{1}/Microsoft-Server-ActiveSync?{2}",
            //   useSSL ? "https:" : "http:", server, RequestLine);
            // Generate the URI for the request
            string uriString = string.Format("{0}?{1}", server, RequestLine);
            if (Uri.IsWellFormedUriString(uriString, UriKind.RelativeOrAbsolute))
            {
                Uri serverUri = new Uri(uriString);

                try
                {
                    CookieContainer cc = new CookieContainer();
                    mWebRequest = (HttpWebRequest)WebRequest.Create(uriString);
                    mWebRequest.Credentials = this.Credentials;
                    mWebRequest.Method = "POST";
                    // CAUSING A PROBLEM WITH THE TABLET: mWebRequest.AllowReadStreamBuffering = true; // LWR TEST
                    // MS-ASHTTP section 2.2.1.1.2.2
                    mWebRequest.ContentType = "application/vnd.ms-sync.wbxml";
                    // CAUSING A PROBLEM WITH THE TABLET: mWebRequest.Headers["Connection"] = "Keep-Alive";
                    mWebRequest.CookieContainer = ActiveSyncBase.EASCookieJar;
                    if (!UseEncodedRequestLine)
                    {
                        // Encoded request lines include the protocol version
                        // and policy key in the request line. 
                        // Non-encoded request lines require that those
                        // values be passed as headers.
                        mWebRequest.Headers["MS-ASProtocolVersion"] = ProtocolVersion;
                        mWebRequest.Headers["X-MS-PolicyKey"] = PolicyKey.ToString();
                    }
                    
                    // CAUSING A PROBLEM WITH TABLET mWebRequest.Headers["user-agent"] = UserAgent; // "MSFT-WP/8.0.9903.10";
                    Stream requestStream = await mWebRequest.GetRequestStream().ConfigureAwait(false);
                    requestStream.Write(WbxmlBytes, 0, WbxmlBytes.Length);
                    requestStream.Dispose();
                    mWebRequest.Headers["ContentLength"] = WbxmlBytes.Length.ToString();
                    HttpWebResponse webResponse = (HttpWebResponse)await mWebRequest.GetResponseAsync().ConfigureAwait(false);

                    if (null == webResponse)
                    {
                        return null;
                    }
                    else
                    {
                        if (webResponse.StatusCode == HttpStatusCode.NotFound)
                        {
                            return null;
                        }
                        else if (webResponse.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            // Seem to get this a lot.  Got to figure out why
                            //ASCommandResponse errResp = new ASCommandResponse(webResponse);
                            //webResponse.Dispose();
                            //return errResp;

                        }
                        else if (webResponse.StatusCode == HttpStatusCode.BadRequest)
                        {
                            // mail.wyse.com (EAS Version 12.1) is returning this
                        }
                        ASCommandResponse resp = WrapHttpResponse(webResponse);

                        webResponse.Dispose();
                        return resp;
                    }
                }
                catch(OperationCanceledException)
                {
                    ASCommandResponse response = WrapHttpResponse(HttpStatusCode.Continue);
                    return response;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                //This case will come when URI is not valid of request. Though I am not getting the crash
                //but have a IF condition for avaoiding crash
                return null;
            }

            
        }     
    

        // This function generates an ASCommandResponse from an
        // HTTP response.
        protected virtual ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASCommandResponse(httpResp);
        }

        protected virtual ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASCommandResponse(statusCode);
        }

        // This function builds a request line from the class properties.
        protected virtual void BuildRequestLine()
        {
            if (Command == null || User == null || DeviceID == null || DeviceType == null)
                throw new InvalidDataException("ASCommandRequest not initialized.");

            if (UseEncodedRequestLine == true)
            {
                // Use the EncodedRequest class to generate
                // an encoded request line
                EncodedRequest encodedRequest = new EncodedRequest();

                encodedRequest.ProtocolVersion = Convert.ToByte(Convert.ToSingle(ProtocolVersion) * 10);
                encodedRequest.SetCommandCode(Command);
                encodedRequest.SetLocale("en-us");
                encodedRequest.DeviceId = DeviceID;
                encodedRequest.DeviceType = DeviceType;
                encodedRequest.PolicyKey = PolicyKey;

                // Add the User parameter to the request line
                encodedRequest.AddCommandParameter("User", user);

                // Add any command-specific parameters
                if (CommandParameters != null)
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        encodedRequest.AddCommandParameter(CommandParameters[i].Parameter, CommandParameters[i].Value);
                    }
                }

                // Generate the request line
                RequestLine = encodedRequest.GetBase64EncodedString();
            }
            else
            {
                // Generate a plain-text request line.
                RequestLine = string.Format("Cmd={0}&User={1}&DeviceId={2}&DeviceType={3}",
                Command, User, DeviceID, DeviceType);

                if (CommandParameters != null)
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        RequestLine = string.Format("{0}&{1}={2}", RequestLine,
                            CommandParameters[i].Parameter, CommandParameters[i].Value);
                    }
                }
            }
        }

        // This function generates an XML payload.
        protected virtual void GenerateXMLPayload()
        {
            // For the base class, this is a no-op.
            // Classes that extend this class to implement
            // commands override this function to generate
            // the XML payload based on the command's request schema
        }

        // This function uses the ASWBXML class to decode
        // a WBXML stream into XML.
        private string DecodeWBXML(byte[] wbxml)
        {
            try
            {
                ASWBXML decoder = new ASWBXML();
                decoder.LoadBytes(wbxml);
                return decoder.GetXml();
            }
            catch (Exception ex)
            {
                ASError.ReportException(ex);
                return "";
            }
        }

        // This function uses the ASWBXML class to encode
        // XML into a WBXML stream.
        private byte[] EncodeXMLString(string xmlString)
        {
            try
            {
                ASWBXML encoder = new ASWBXML();
                encoder.LoadXml(xmlString);
                return encoder.GetBytes();
            }
            catch (Exception ex)
            {
                ASError.ReportException(ex);
                return null;
            }
        }
    }
}
