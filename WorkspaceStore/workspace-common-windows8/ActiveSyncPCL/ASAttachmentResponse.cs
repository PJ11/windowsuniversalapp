﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    // This class represents a FolderSync command
    // response as specified in MS-ASCMD section 2.2.2.4.2.
    public class ASAttachmentResponse : ASCommandResponse
    {
        // This enumeration covers the possible Status
        // values for FolderSync responses.
        public enum DownloadStatus
        {
            Success = 1,
            ServerError = 6,
            InvalidSyncKey = 9,
            InvalidFormat = 10,
            UnknownError = 11,
            UnknownCode = 12
        }

        private XDocument responseXml = null;
       
        private Int32 status = 0;

        #region Property Accessors
        public Int32 Status
        {
            get
            {
                return status;
            }
        }
        #endregion

        public ASAttachmentResponse(HttpWebResponse httpResponse)
            : base(httpResponse)
        {
            TextReader tr = new StringReader(XmlString);
            responseXml = XDocument.Load(tr, LoadOptions.None);

            SetStatus();
        }

        public ASAttachmentResponse(HttpStatusCode statusCode)
            : base(statusCode)
        {
        }

        // This function updates a folder tree based on the
        // changes received from the server in the response.
        public byte[] GetAttachmentData()
        {
            try
            {
                // Get sync key
                
               // XElement dataNode = responseXml.GetElementsByTagName("Data")[0];

                var attachmentData = (from d in responseXml.Descendants()
                                     where d.Name.LocalName == "Data"
                                     select d.Value).First();

                

                var base64EncodedAttachmentBytes = System.Convert.FromBase64String(attachmentData);
                return base64EncodedAttachmentBytes;
            }
            catch (Exception e)
            {
                return null;

                // Rather than attempting to recover, reset sync key 
                // and empty folders. The next FolderSync should
                // re-sync folders.
                
            }

            
        }

        // This function extracts the response status from the 
        // XML and sets the status property.
        private void SetStatus()
        {
            var downloadStatus = (from d in responseXml.Descendants()
                                  where d.Name.LocalName == "Status"
                                  select d.Value).First();

            status = XmlConvert.ToInt32(downloadStatus);
            
        }
    }
}
