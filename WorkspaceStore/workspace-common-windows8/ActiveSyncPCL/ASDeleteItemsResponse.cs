﻿
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASDeleteItemsResponse : ASCommandResponse
    { 
      
        private XDocument responseXml = null;
        public Int32 status = 0;
        private string mAirSyncNS;
        public ASDeleteItemsResponse(HttpWebResponse httpResponse)
               : base(httpResponse)

        {
            mAirSyncNS = "{AirSync}";
            if (XmlString != "")
            {
                responseXml = new XDocument();
                TextReader tr = new StringReader(XmlString);
                responseXml = XDocument.Load(tr, LoadOptions.None);
                SetStatus();
            }

        }

        public ASDeleteItemsResponse(HttpStatusCode statusCode)
            : base (statusCode)
        {
        }

        // This function gets the sync key for 
        // a folder.
        public string GetSyncKeyForFolder(string folderId)
        {
            string folderSyncKey = "0";

            string ns = "{AirSync}";
            IEnumerable<XElement> collection = responseXml.Descendants().Where(p => p.Name == (ns + "Collection")).ToList();
            if (collection != null)
            {
                foreach (XElement coll in collection)
                {
                    XElement j = coll as XElement;
                    if (j != null && (j.Descendants().First(p => p.Name == (ns + "CollectionId")).Value).ToString() == folderId.ToString())
                    {
                        XElement syncKey = j.Descendants().First(p => p.Name == (ns + "SyncKey"));
                        folderSyncKey = (string)syncKey.Value;
                        break;
                    }
                }
            }

            return folderSyncKey;
        }

        private void SetStatus()
        {
            string ns = "{AirSync}";
            IEnumerable<XElement> collection = responseXml.Descendants().Where(p => p.Name == (ns + "Collection")).ToList();
            if (collection != null)
            {
                foreach (XElement coll in collection)
                {
                    XElement statusQ = coll as XElement;
                    if (statusQ != null)
                    {
                        XElement statusKey = statusQ.Descendants().First(p => p.Name == (ns + "Status"));
                        status = int.Parse(statusKey.Value);
                        break;
                    }
                }
            }

        }
    }

}


