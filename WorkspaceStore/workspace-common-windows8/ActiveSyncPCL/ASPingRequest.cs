﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASPingRequest : ASCommandRequest
    {
        private Int32 heartBeatInterval = 0;    // 60 - 3540 seconds
        List<Folder> folderList = null;

        #region Property Accessors

        public Int32 HeartBeatInterval
        {
            get
            {
                return heartBeatInterval;
            }
            set
            {
                heartBeatInterval = value;
            }
        }

        public List<Folder> Folders
        {
            get
            {
                return folderList;
            }
        }

        #endregion
         public ASPingRequest(List<Folder> folders)
        {
            this.Command = "Ping";
            this.folderList = folders;            
        }

        // This function generates an ASSyncResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASPingResponse(httpResp, folderList);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASPingResponse(statusCode);
        }


        // This function generates the XML request body
        // for the Sync request.
        protected override void GenerateXMLPayload()
        {
            
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument syncXML = new XDocument(new XDeclaration("1.0", "utf-8", null));

            XNamespace pingNS = Namespaces.pingNamespace;

            XElement syncNode = new XElement(pingNS + "Ping"/*,
                new XAttribute(XNamespace.None + Xmlns.pingXmlns, pingNS)*/);

            syncXML.Add(syncNode);

            // Only add a collections node if there are folders in the request.
            // If omitting, there should be a Partial element.

            // If a heartbeat interval period was specified, include it here
            if (heartBeatInterval > 0)
            {
                XElement heartBeatIntervalNode = new XElement(pingNS + "HeartbeatInterval", new XText(heartBeatInterval.ToString()));
                syncNode.Add(heartBeatIntervalNode);
                //set httpwebrequesttime interval value
                HttpWebRequestTimeInterval = HeartBeatInterval;
            }

            if (folderList.Count == 0)
                throw new ArgumentException(
                    "Sync requests must specify collections or include the Partial element.");

            if (folderList.Count > 0)
            {

                XElement foldersNode = new XElement(pingNS + "Folders");
                syncNode.Add(foldersNode);
                foreach (Folder folder in folderList)
                {
                    XElement folderNode = new XElement(pingNS + "Folder");
                    foldersNode.Add(folderNode);

                    XElement syncKeyNode = new XElement(pingNS + "Class", new XText("Email"/*folder.Type.ToString()*/));
                    folderNode.Add(syncKeyNode);

                    XElement collectionIdNode = new XElement(pingNS + "Id", new XText(folder.Id));
                    folderNode.Add(collectionIdNode);

                }
            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            syncXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();
            
        }
    }
}
