﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    // This class represents the FolderSync command
    // request specified in MS-ASCMD section 2.2.2.4.1.
    public class ASFolderSyncRequest : ASCommandRequest
    {
        private string syncKey = "0";

        #region Property Accessors

        public string SyncKey
        {
            get
            {
                return syncKey;
            }
            set
            {
                syncKey = value;
            }
        }

        #endregion

        public ASFolderSyncRequest()
        {
            Command = "FolderSync";
        }

        // This function generates an ASFolderSyncResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
           // return null; // new ASFolderSyncResponse(httpResp);
            return new ASFolderSyncResponse(httpResp);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASFolderSyncResponse(statusCode);
        }


        /*
<?xml version="1.0" encoding="utf-8"?>
<folderhierarchy:FolderSync xmlns:folderhierarchy="FolderHierarchy">
  <folderhierarchy:SyncKey>1</folderhierarchy:SyncKey>
</folderhierarchy:FolderSync>
         */
        // This function generates the XML request body
        // for the FolderSync request.
        protected override void GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            if (syncKey == "")
                syncKey = "0";
            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument folderSyncXML = new XDocument();
            folderSyncXML.Declaration = new XDeclaration("1.0", "utf-8", null);
            XNamespace folderhierarchy = Namespaces.folderHierarchyNamespace;
            
            try
            {
                folderSyncXML.Add( new XElement(folderhierarchy + "FolderSync", 
                    new XAttribute(XNamespace.Xmlns + Xmlns.folderHierarchyXmlns, Namespaces.folderHierarchyNamespace),
                    new XElement(folderhierarchy + "SyncKey", new XText(syncKey.ToString()))
                    ) );

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Async = false;
                settings.Indent = true;
                settings.IndentChars = "  ";
                settings.Encoding = Encoding.UTF8;
                Utf8StringWriter sw = new Utf8StringWriter();
                
                XmlWriter writer = XmlWriter.Create(sw, settings);
                
                folderSyncXML.WriteTo(writer);
                writer.Flush();

                XmlString = sw.ToString();
            }
            catch (Exception e)
            {
                string msg = e.Message;
            }
        }
    }
}
