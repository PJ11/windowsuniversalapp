﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASMeetingResponseCommandRequest:ASCommandRequest
    {
        List<ASMoveItemsRequest> _items = null;
        // This function generates an ASSyncResponse from an
        // HTTP response.
         #region Property Accessors
        public string UserResponse { get; set; }
        public string CollectionId { get; set; }
        public string RequestId { get; set; }


        #endregion
        public ASMeetingResponseCommandRequest()
        {
            this.Command = "MeetingResponse";
           // this._items = moveItems;
        }
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASMeetingResponseCommandResponse(httpResp);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASMeetingResponseCommandResponse(statusCode);
        }


        // This function generates the XML request body
        // for the Sync request.
        protected override void GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument syncXML = new XDocument(new XDeclaration("1.0", "utf-8", null));

            XNamespace meetingResponseNS = Namespaces.meetingResponseNamespace;

            XElement syncNode = new XElement(meetingResponseNS + "MeetingResponse"/*,
                new XAttribute(XNamespace.None + Xmlns.pingXmlns, pingNS)*/);

            syncXML.Add(syncNode);

            // Only add a collections node if there are folders in the request.
            // If omitting, there should be a Partial element.

            XElement requestNode = new XElement(meetingResponseNS + "Request");
            syncNode.Add(requestNode);
                    XElement userResposne = new XElement(meetingResponseNS + "UserResponse", new XText(UserResponse));
                    requestNode.Add(userResposne);

                    XElement collectionId = new XElement(meetingResponseNS + "CollectionId", new XText(CollectionId));
                    requestNode.Add(collectionId);

                    XElement requestId = new XElement(meetingResponseNS + "RequestId", new XText(RequestId));
                    requestNode.Add(requestId);
            
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            syncXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();

        }
    }
}
