﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASMeetingResponse : ASSyncResponse
    { 
        bool createException = false;

        public ASMeetingResponse(HttpWebResponse httpResponse, bool createExcp)
            : base(httpResponse, null)
        {
            createException = createExcp;
        }

        public ASMeetingResponse(ASSyncResponse resp, bool createexception)
        {
            createException = createexception;
            this._httpStatus = resp.HttpStatus;
            this._status = resp.Status;
            this._wbxmlBytes = resp.WbxmlBytes;
            this._xmlString = resp.XmlString;
            this._responseXml = resp.ResponseXML;
            this._httpResponse = resp.HttpWebResponse;
        }

        public ASMeetingResponse(HttpStatusCode statusCode)
            : base (statusCode)
        {
        }

        protected override void SetStatus()
        {
            string ns = "{AirSync}";
            if (_responseXml != null)
            {
                XElement responsesElement = _responseXml.Descendants().SingleOrDefault(p => p.Name == (ns + "Responses"));
                if ( responsesElement == null)
                {
                    var airSyncQ = _responseXml.Descendants().SingleOrDefault(p => p.Name == (ns + "Collections"));
                    if (airSyncQ != null)
                    {
                        var statusQ = airSyncQ.Descendants().SingleOrDefault(p => p.Name == (ns + "Status"));
                        if (statusQ != null)
                        {
                            _status = XmlConvert.ToInt32(statusQ.Value);
                        }
                    }
                }
                else
                {
                    var airSyncQ = _responseXml.Descendants().SingleOrDefault(p => p.Name == (ns + "Responses"));
                    if (airSyncQ != null)
                    {
                        var statusQ = airSyncQ.Descendants().SingleOrDefault(p => p.Name == (ns + "Status"));
                        if (statusQ != null)
                        {
                            _status = XmlConvert.ToInt32(statusQ.Value);
                        }
                    }
                }
            }
        }
    }
}
