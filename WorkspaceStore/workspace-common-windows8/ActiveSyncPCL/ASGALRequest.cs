﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASGALRequest:ASCommandRequest
    {
        string searchString;
        public ASGALRequest()
        {
            Command = "Search";
        }
        public void SetSearchString(string searchString)
        {
            this.searchString = searchString;
        }
        // This function generates an ASFolderSyncResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASGALResponse(httpResp);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASGALResponse(statusCode);
        }


        protected override void GenerateXMLPayload()
        {

            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument requestXML = new XDocument(new XDeclaration("1.0", "utf-8", null));
            //XNamespace airsyncbasenamespace = Namespaces.airSyncBaseNamespace;
            XNamespace searchNameSpace = Namespaces.searchNamespace;
            //XNamespace syncnamespace = Namespaces.contactNamespace;
            XElement searchNode = new XElement(searchNameSpace + "Search",
                new XAttribute(XNamespace.Xmlns + Xmlns.searchXmlns, Namespaces.searchNamespace));

            requestXML.Add(searchNode);

            // Only add a collections node if there are folders in the request.
            // If omitting, there should be a Partial element.

            XElement storeNode = new XElement(searchNameSpace + "Store");
            searchNode.Add(storeNode);

            XElement nameNode = new XElement(searchNameSpace + "Name", "GAL");
            storeNode.Add(nameNode);

            XElement queryNode = new XElement(searchNameSpace + "Query", searchString);
            storeNode.Add(queryNode);

            XElement optionsNode = new XElement(searchNameSpace + "Options");
            storeNode.Add(optionsNode);

            XElement rangeNode = new XElement(searchNameSpace + "Range", "0-99");
            optionsNode.Add(rangeNode);

            XElement rebuildResultsNode = new XElement(searchNameSpace + "RebuildResults");
            optionsNode.Add(rebuildResultsNode);

            XElement deepTraversalNode = new XElement(searchNameSpace + "DeepTraversal");
            optionsNode.Add(deepTraversalNode);

           
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            requestXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();
        }
    }
}
