﻿//#define OLDWAY
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ActiveSyncPCL
{
    // This class represents an OPTIONS request as specified
    // in MS-ASHTTP section 2.2.3.
    public class ASOptionsRequest
    {
        private NetworkCredential credential = null;
        private string server = null;
        private bool useSSL = true;

        #region Property Accessors
        public NetworkCredential Credentials
        {
            get
            {
                return credential;
            }
            set
            {
                credential = value;
            }
        }

        public string Server
        {
            get
            {
                return server;
            }
            set
            {
                server = value;
            }
        }

        public bool UseSSL
        {
            get
            {
                return useSSL;
            }
            set
            {
                useSSL = value;
            }
        }
        #endregion

        // This function sends the OPTIONS request and returns an
        // ASOptionsResponse class that represents the response.
        public async Task<ASOptionsResponse> GetOptions()
        {
            if (credential == null || server == null)
                throw new InvalidDataException("ASOptionsRequest not initialized.");

            string uriString = Server;
            Uri serverUri = new Uri(uriString);

            HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(uriString);
            httpReq.Credentials = Credentials; 
            httpReq.Method = "OPTIONS";
            httpReq.CookieContainer = ActiveSyncBase.EASCookieJar;
            HttpWebResponse webResponse = (HttpWebResponse)await httpReq.GetResponseAsync();

            ASOptionsResponse resp = new ASOptionsResponse(webResponse);

            webResponse.Dispose();

            return resp;
        }
    }
}
