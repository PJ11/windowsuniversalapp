﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Net;

namespace ActiveSyncPCL
{
    // This enumeration covers the allowable
    // body types specified in MS-ASAIRS section
    // 2.2.2.22
    public enum BodyType
    {
        NoType = 0,
        PlainText,
        HTML,
        RTF,
        MIME
    }

    // This enumeration covers the available
    // sync filter types specified in MS-ASCMD
    // section 2.2.3.64.2
    public enum SyncFilterType
    {
        NoFilter = 0,
        OneDayBack,
        ThreeDaysBack,
        OneWeekBack,
        TwoWeeksBack,
        OneMonthBack,
        ThreeMonthsBack,
        SixMonthsBack,
        IncompleteTasks
    }

    // This enumeration covers the possible
    // values for the Conflict element specified
    // in MS-ASCMD 2.2.3.34
    public enum ConflictResolution
    {
        KeepClientVersion = 0,
        KeepServerVersion,
        LetServerDecide
    }

    // This enumeration covers the possible
    // values for the MIMETruncation element
    // specified in MS-ASCMD section 2.2.3.101
    public enum MimeTruncationType
    {
        TruncateAll = 0,
        Truncate4k,
        Truncate5k,
        Truncate7k,
        Truncate10k,
        Truncate20k,
        Truncate50k,
        Truncate100k,
        NoTruncate
    }

    // This enumeration covers the possible
    // values for the MIMESupport element
    // specified in MS-ASCMD section 2.2.3.100.3
    public enum MimeSupport
    {
        NeverSendMime = 0,
        SendMimeForSMime,
        SendMimeForAll
    }

    // This class represents body or body part
    // preferences included in a <BodyPreference> or
    // <BodyPartPreference> element.
    public class BodyPreferences
    {
        public BodyType Type = BodyType.NoType;
        public UInt32 TruncationSize = 0;
        public bool AllOrNone = false;
        public Int32 Preview = -1;
    }

    // This class represents the sync options
    // that are included under the <Options> element
    // in a Sync command request.
    public class FolderSyncOptions
    {
        public SyncFilterType FilterType = SyncFilterType.NoFilter;
        public ConflictResolution ConflictHandling = ConflictResolution.LetServerDecide;
        public MimeTruncationType MimeTruncation = MimeTruncationType.NoTruncate;
        public string Class = null;
        public Int32 MaxItems = -1;
        public BodyPreferences[] BodyPreference = null;
        public BodyPreferences BodyPartPreference = null;
        public bool IsRightsManagementSupported = false;
        public MimeSupport MimeSupportLevel = MimeSupport.NeverSendMime;
    }

    // This class represents a single folder
    // in a mailbox. It is also used to represent
    // the root of the mailbox's folder hierarchy
    public class Folder
    {
        // The folder hierarchy is persisted as an XML file
        // named FolderInfo.xml.
        private static string folderInfoFileName = "FolderInfo.xml";
        // Since the root is not returned in a FolderSync response,
        // we create it with the reserved name "Mailbox".
        private static string rootFolderName = "Mailbox";
        // This is the basic XML structure used to initialize the XDocument
        // if the hiearchy has not been synced before.
        private static string emptyFolderTree = "<RootFolder><Path/><SyncKey/><LastSyncTime/><Folders/></RootFolder>";

        // This enumeration indicates the folder type, and is based on
        // the allowed values for the Type element specified in
        // [MS-ASCMD] section 2.2.3.170.3.
        public enum FolderType
        {
            UserGeneric = 1,
            DefaultInbox,
            DefaultDrafts,
            DefaultDeletedItems,
            DefaultSentItems,
            DefaultOutbox,
            DefaultTasks,
            DefaultCalendar,
            DefaultContacts,
            DefaultNotes,
            DefaultJournal,
            UserMail,
            UserCalendar,
            UserContacts,
            UserTasks,
            UserJournal,
            UserNotes,
            Unknown,
            RecipientInfoCache
        }

        // The name of the folder
        private string name = "";
        // The id of the folder
        private string id = "";
        // The type of the folder
        private FolderType type = FolderType.Unknown;
        // The location on disk where this folder is saved
        private string saveLocation = "";
        // The Folder object that represents this folder's parent
        private Folder parentFolder = null;
        // A list of subfolders
        private List<Folder> subFolders = null;
        // The current sync key for this folder
        string syncKey = "0";
        // The last time the contents of this folder were synced
        private DateTime lastSyncTime = DateTime.MinValue;
        // Should items deleted from this folder
        // be permanently deleted?
        private bool areDeletesPermanent = false;
        // Should changes be ignored?
        private bool areChangesIgnored = false;
        // The max number of changes that should be
        // returned in a sync.
        private Int32 windowSize = 0;
        // Conversation mode
        private bool useConversationMode = false;

        // Optional sync options
        private FolderSyncOptions options = null;

        // Constructor that creates a Folder object based on the basic
        // properties of the folder. This form is used when building
        // the folder hierarchy under the root.
        public Folder(String folderName, string folderId, FolderType folderType, Folder parent)
        {
            name = folderName;
            id = folderId;
            parentFolder = parent;
            type = folderType;

            saveLocation = "Mailbox" + "\\" + folderName;
            
            subFolders = new List<Folder>();
        }

       
       
        // This function saves new items into the
        // local storage for a folder.
        static void SaveItemsInLocalFolder(Folder targetFolder, List<ServerSyncCommand> itemList)
        {
            foreach (ServerSyncCommand newItem in itemList)
            {
                // Build a filename for the new item
                // that is based on server id.
                string itemFileName = newItem.ServerId;

                // Since server ids typically have a
                // ':' in them, replace with a legal character
                itemFileName = itemFileName.Replace(':', '_');
                itemFileName = itemFileName + ".xml";
           
                XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
                
            settings.Indent = true;
             
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();
             // sw=targetFolder.saveLocation + "\\" + itemFileName;
            XmlWriter writer = XmlWriter.Create(sw, settings);
              //  XmlWriter xmlToFile = XmlWriter.Create(targetFolder.saveLocation + "\\" + itemFileName);

                newItem.AppDataXml.WriteTo(writer);
                
                writer.Flush();
                writer.Dispose();

            }
        }
  


       

        private async Task<bool> FolderExists(string fileName)
        {
            return false;
        }


        // Constructor that creates a Folder object based on a local disk
        // path. This is the form used to create a root folder for the
        // mailbox.
        public Folder(string MainfolderName)
        {
            saveLocation = MainfolderName;
            subFolders = new List<Folder>();
            
            name = rootFolderName;
            id = "0";

            // TODO Not sure if this code will work CHECK!
            bool exists = FolderExists(saveLocation).Result;

            if (exists)
            {
                // The directory already exists, so load 
                // the subtree from FolderInfo.xml
                LoadFolderInfo();
            }
            else
            {
                // The directory does not exist, create an empty one.
                //rootDirectory.CreateFolderAsync(saveLocation, CreationCollisionOption.ReplaceExisting);
                //Directory.CreateDirectory(saveLocation);
            }
        }

        #region Property Accessors

        public System.String Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Id
        {
            get
            {
                return id;
            }
        }

        public Folder ParentFolder
        {
            get
            {
                return parentFolder;
            }
        }

        public FolderType Type
        {
            get
            {
                return type;
            }
        }

        public string SaveLocation
        {
            get
            {
                return saveLocation;
            }
        }

        public string SyncKey
        {
            get
            {
                return syncKey;
            }
            set
            {
                syncKey = value;
            }
        }

        public DateTime LastSyncTime
        {
            get
            {
                return lastSyncTime;
            }
            set
            {
                lastSyncTime = value;
            }
        }

        public bool AreDeletesPermanent
        {
            get
            {
                return areDeletesPermanent;
            }
            set
            {
                areDeletesPermanent = value;
            }
        }

        public bool AreChangesIgnored
        {
            get
            {
                return areChangesIgnored;
            }
            set
            {
                areChangesIgnored = value;
            }
        }

        public Int32 WindowSize
        {
            get
            {
                return windowSize;
            }
            set
            {
                windowSize = value;
            }
        }

        public bool UseConversationMode
        {
            get
            {
                return useConversationMode;
            }
            set
            {
                useConversationMode = value;
            }
        }

        public FolderSyncOptions Options
        {
            get
            {
                return options;
            }
            set
            {
                options = value;
            }
        }
        public Folder RootFolder
        {
            get
            {
                if (parentFolder != null)
                    return parentFolder.RootFolder;
                else
                    return this;
            }
        }

        public List<Folder> SubFolders
        {
            get
            {
                return subFolders;
            }
        }
        #endregion

        // Searches subfolders for a folder with the given Id.
        public Folder FindFolderById(string folderId)
        {
            // If the requested Id is 0, return the root.
            if (folderId == "0")
                return this.RootFolder;

            // If this folder matches the requested Id, return it.
            if (folderId == this.id)
                return this;

            // If this folder has no subfolders, there is nothing 
            // to search.
            if (subFolders == null)
                return null;

            Folder returnFolder = null;

            // Check each subfolder. If a subfolder has its own
            // sbufolders, recurse into them looking for a match.
            foreach (Folder subFolder in subFolders)
            {
                if (subFolder.Id == folderId)
                {
                    returnFolder = subFolder;
                    break;
                }
                else
                {
                    returnFolder = subFolder.FindFolderById(folderId);

                    if (returnFolder != null)
                        break;
                }
            }

            return returnFolder;
        }

        // This function is used to add folders to the hierarchy from the root.
        public Folder AddFolder(string folderName, string folderId, string folderParentId, FolderType folderType)
        {
            if (id != "0")
                throw new Exception("This function is only valid on root folders.");

            // Check if a folder with this ID already exists
            Folder existingFolder = FindFolderById(folderId);

            if (existingFolder != null)
            {
                // If a folder with that Id, name, parent, and type already exist,
                // just return it instead of creating a new one.
                if (existingFolder.Name == folderName &&
                        existingFolder.ParentFolder.Id == folderParentId &&
                        existingFolder.Type == folderType)
                    return existingFolder;
                else
                {
                    // There should never be two folders with the same Id, so 
                    // throw an exception.
                    string exceptionString = string.Format(
                        "A folder with the Id = {0} already exists. Name = {1}, ParentId = {2}, Type = {3}.",
                        folderId,
                        existingFolder.Name,
                        existingFolder.ParentFolder.Id,
                        existingFolder.Type);

                    throw new ArgumentException(exceptionString);
                }
            }

            // The folder does not already exist, so find the parent.
            Folder parentFolder = FindFolderById(folderParentId);
            if (parentFolder == null)
                throw new Exception(string.Format("Invalid parent folder Id: {0}", folderParentId));

            // Add a new subfolder to the parent.
            return parentFolder.AddSubFolder(folderName, folderId, folderType);
        }

        // This function creates a new Folder object and adds it to the list of subfolders.
        public Folder AddSubFolder(string folderName, string folderId, FolderType folderType)
        {
            Folder newFolder = new Folder(folderName, folderId, folderType, this);
            subFolders.Add(newFolder);
            return newFolder;
        }

        // This function adds a Folder object to the list of subfolders.
        public void AddSubFolder(Folder newFolder)
        {
            subFolders.Add(newFolder);
        }

        // This function ensures that the options member
        // is initialized.
        public void EnsureOptions()
        {
            if (options == null)
                options = new FolderSyncOptions();
        }

        // This function takes an XNode object that represents
        // a <Folder> element and adds a subfolder to the hierarchy
        // based on the values contained in the element.
        public void AddSubFolderFromXml(XElement folderNode)
        {
            Folder newFolder = null;

            // Load the name, id, and type.
            var nameQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("Name"));
            var idQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("Id"));
            var typeQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("Type"));

            if(null != nameQ && nameQ.Value.Length > 0 &&
                null != idQ && idQ.Value.Length > 0 &&
                null != typeQ && typeQ.Value.Length > 0)
            {
                newFolder = this.AddSubFolder(nameQ.Value, idQ.Value,
                    (FolderType)XmlConvert.ToInt32(typeQ.Value));
                var syncKeyQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("SyncKey"));
                if(null != syncKeyQ && syncKeyQ.Value.Length > 0)
                {
                    newFolder.SyncKey = syncKeyQ.Value;
                }
                var lastSyncQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("LastSyncTime"));
                if (null != lastSyncQ && lastSyncQ.Value.Length > 0)
                {
                    newFolder.LastSyncTime = XmlConvert.ToDateTimeOffset(lastSyncQ.Value).UtcDateTime;
                }

                /* EMPTY TEMPLATE
                var  = folderNode.Descendants().SingleOrDefault(p => p.Name == (""));
                if (null !=  && .Value.Length > 0)
                {
                    newFolder = 
                }
                */
                var permanentDeleteQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("PermanentDelete"));
                if (null != permanentDeleteQ && permanentDeleteQ.Value.Length > 0)
                {
                    newFolder.AreDeletesPermanent = true;
                }
                var ignoreServerChangesQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("IgnoreServerChanges"));
                if (null != ignoreServerChangesQ && ignoreServerChangesQ.Value.Length > 0)
                {
                    newFolder.AreChangesIgnored = true;
                }
                var conversationModeQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("ConversationMode"));
                if (null != conversationModeQ && conversationModeQ.Value.Length > 0)
                {
                    newFolder.UseConversationMode = true;
                }
                var windowSizeQ  = folderNode.Descendants().SingleOrDefault(p => p.Name == ("WindowSize"));
                if (null != windowSizeQ && windowSizeQ.Value.Length > 0)
                {
                    newFolder.WindowSize = XmlConvert.ToInt32(windowSizeQ.Value);
                }
                var filterTypeQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("FilterType"));
                if (null != filterTypeQ && filterTypeQ.Value.Length > 0)
                {
                    newFolder.EnsureOptions();
                    newFolder.options.FilterType = (SyncFilterType)XmlConvert.ToInt32(filterTypeQ.Value);
                }
                var conflictHandlingQ  = folderNode.Descendants().SingleOrDefault(p => p.Name == ("ConflictHandling"));
                if (null != conflictHandlingQ && conflictHandlingQ.Value.Length > 0)
                {
                    newFolder.EnsureOptions();
                    newFolder.options.ConflictHandling = (ConflictResolution)XmlConvert.ToInt32(conflictHandlingQ.Value);
                }
                var mimeTruncationQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("MimeTruncation"));
                if (null != mimeTruncationQ && mimeTruncationQ.Value.Length > 0)
                {
                    newFolder.EnsureOptions();
                    newFolder.options.MimeTruncation = (MimeTruncationType)XmlConvert.ToInt32(mimeTruncationQ.Value);
                }
                var classToSyncQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("ClassToSync"));
                if (null != classToSyncQ && classToSyncQ.Value.Length > 0)
                {
                    newFolder.EnsureOptions();
                    newFolder.options.Class = classToSyncQ.Value; 
                }
                var maxItemsQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("MaxItems"));
                if (null !=  maxItemsQ && maxItemsQ.Value.Length > 0)
                {
                    newFolder.EnsureOptions();
                    newFolder.options.MaxItems = XmlConvert.ToInt32(maxItemsQ.Value);
                }
                var bodyPreferencesQ  = folderNode.Descendants().SingleOrDefault(p => p.Name == ("BodyPreferences"));
                if (null != bodyPreferencesQ)
                {
                    newFolder.EnsureOptions();
                    if(null == newFolder.options.BodyPreference)
                    {
                        newFolder.options.BodyPreference = new BodyPreferences[1];
                        newFolder.options.BodyPreference[0] = new BodyPreferences();
                    }
                    newFolder.options.BodyPreference[0].Type = (BodyType)XmlConvert.ToInt32(bodyPreferencesQ.Value);

                    var truncationSizeQ = bodyPreferencesQ.Descendants().SingleOrDefault(p => p.Name == ("TruncationSize"));
                    if(null != truncationSizeQ)
                    {
                        newFolder.options.BodyPreference[0].TruncationSize = XmlConvert.ToUInt32(truncationSizeQ.Value);
                    }
                    var allOrNoneQ = bodyPreferencesQ.Descendants().SingleOrDefault(p => p.Name == ("AllOrNone"));
                    if(null != allOrNoneQ)
                    {
                        newFolder.options.BodyPreference[0].AllOrNone = true;
                    }
                    var previewSizeQ = bodyPreferencesQ.Descendants().SingleOrDefault(p => p.Name == ("Preview"));
                    if(null != previewSizeQ)
                    {
                        newFolder.options.BodyPreference[0].Preview = XmlConvert.ToInt32(previewSizeQ.Value);
                    }
                }

                var bodyPartPreferencesQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("BodyPartPreferences"));
                if (null != bodyPartPreferencesQ)
                {
                    newFolder.EnsureOptions();
                    if (null == newFolder.options.BodyPartPreference)
                    {
                        newFolder.options.BodyPartPreference = new BodyPreferences();
                    }
                    newFolder.options.BodyPartPreference.Type = (BodyType)XmlConvert.ToInt32(bodyPartPreferencesQ.Value);

                    var truncationSizeQ = bodyPartPreferencesQ.Descendants().SingleOrDefault(p => p.Name == ("TruncationSize"));
                    if (null != truncationSizeQ)
                    {
                        newFolder.options.BodyPartPreference.TruncationSize = XmlConvert.ToUInt32(truncationSizeQ.Value);
                    }
                    var allOrNoneQ = bodyPartPreferencesQ.Descendants().SingleOrDefault(p => p.Name == ("AllOrNone"));
                    if (null != allOrNoneQ)
                    {
                        newFolder.options.BodyPartPreference.AllOrNone = true;
                    }
                    var previewSizeQ = bodyPartPreferencesQ.Descendants().SingleOrDefault(p => p.Name == ("Preview"));
                    if (null != previewSizeQ)
                    {
                        newFolder.options.BodyPartPreference.Preview = XmlConvert.ToInt32(previewSizeQ.Value);
                    }
                }
                var rightManagementSupportedQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("RightManagementSupported"));
                if (null != rightManagementSupportedQ)
                {
                    newFolder.EnsureOptions();
                    newFolder.options.IsRightsManagementSupported = true;
                }
                var mimeSupportQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("MimeSupport"));
                if (null !=  mimeSupportQ)
                {
                    newFolder.EnsureOptions();
                    newFolder.options.MimeSupportLevel = (MimeSupport)XmlConvert.ToInt32(mimeSupportQ.Value); 
                }
                // Add subfolders.  This will be recursive
                XElement subFoldersQ = folderNode.Descendants().SingleOrDefault(p => p.Name == ("Folders"));
                if(null != subFoldersQ)
                {
                    foreach (XElement pVal in subFoldersQ.Elements())
                    {
                        newFolder.AddSubFolderFromXml(pVal);
                    }
                }
            }
        }

        // This function removes a folders local storage and
        // removes the folder from the parent's subfolders.
        public bool Remove()
        {
            return false;
        }

        // This function removes a folder from the list of
        // subfolders.
        public bool RemoveSubFolder(Folder removeFolder)
        {
            return subFolders.Remove(removeFolder);
        }

        // This function removes all subfolders
        public void RemoveAllSubFolders()
        {
            foreach (Folder subFolder in subFolders)
            {
                subFolder.Remove();
            }
        }

        // This function updates an existing folder by either
        // renaming it or moving it.
        public void Update(string folderName, Folder newParent, Folder.FolderType folderType)
        {
            // Set the new name
            name = folderName;

            // Check to see if we are moving the folder.
            if (newParent != parentFolder)
            {
                // Remove this folder from the current parent's
                // subfolders
                parentFolder.RemoveSubFolder(this);

                parentFolder = newParent;
                
                // Add this folder to the new parent's subfolders
                newParent.AddSubFolder(this);
            }

            // Call MoveLocalFolder to move the local data to a new
            // directory.
            MoveLocalFolder(newParent.SaveLocation + "\\" + name);
        }

        // This function moves local storage to a new directory.
        public void MoveLocalFolder(string destination)
        {
            try
            {
                //IFolder destinationFolder = FileSystem.Current.GetFolderFromPathAsync(destination).Result;
                //IFolder sourceFolder = FileSystem.Current.GetFolderFromPathAsync(saveLocation).Result;
            }
            catch (Exception e)
            {
                ASError.ReportException(e);
            }

            saveLocation = destination;
        }

        // This function is used to load a local folder hierarchy from
        // an Xml file saved in the root mailbox directory.
        public void LoadFolderInfo()
        {
            if (this.id != "0")
                throw new Exception("This method is only valid on the root folder.");

            XDocument folderInfo = new XDocument();
            
            try
            {
                folderInfo = XDocument.Load(this.saveLocation + "\\" + folderInfoFileName);
                
            }
            catch (System.IO.FileNotFoundException)
            {

            }
            var syncKeyQ = folderInfo.Descendants().SingleOrDefault(p => p.Name == ("SyncKey"));
            if (null != syncKeyQ)
            {
                if(syncKeyQ.Value.Length > 0)
                {
                    this.syncKey = syncKeyQ.Value;
                }
                else
                {
                    this.syncKey = "0";
                }
            }
            var lastSyncQ = folderInfo.Descendants().SingleOrDefault(p => p.Name == ("LastSyncTime"));
            if (null != lastSyncQ)
            {
                if (lastSyncQ.Value.Length > 0)
                {
                    this.lastSyncTime = XmlConvert.ToDateTimeOffset(lastSyncQ.Value).UtcDateTime;
                }
                else
                {
                    this.lastSyncTime = DateTime.MinValue;
                }
            }
            XElement subFoldersQ = folderInfo.Descendants().SingleOrDefault(p => p.Name == ("Folders"));
            if (null != subFoldersQ)
            {
                foreach (XElement pVal in subFoldersQ.Elements())
                {
                    AddSubFolderFromXml(pVal);
                }
            }
#if OLDWAY
            try
            {
                folderInfo.Load(this.saveLocation + "\\" + folderInfoFileName);
            }
            catch (System.IO.FileNotFoundException)
            {
                // If the folder info XML was not found, create
                // an empty one.
                folderInfo.LoadXml(emptyFolderTree);
            }

            // Load the sync key for the hierarchy. If it is not present,
            // set to 0.
            XNode syncKeyNode = folderInfo.SelectSingleNode(".//SyncKey");
            if (syncKeyNode != null)
            {
                if (syncKeyNode.InnerText != "")
                    this.syncKey = syncKeyNode.InnerText;
                else
                    this.syncKey = "0";
            }

            // Load the last sync time for the hierarchy.
            XNode lastSyncTimeNode = folderInfo.SelectSingleNode(".//LastSyncTime");
            if (lastSyncTimeNode != null)
            {
                if (lastSyncTimeNode.InnerText != "")
                    lastSyncTime = XmlConvert.ToDateTime(lastSyncTimeNode.InnerText, XmlDateTimeSerializationMode.Utc);
                else
                    lastSyncTime = DateTime.MinValue;
            }

            // Load any subfolders
            XNode subFoldersNode = folderInfo.SelectSingleNode(".//Folders");
            if (subFoldersNode != null)
            {
                XmlNodeList subFoldersList = subFoldersNode.SelectNodes("./Folder");

                foreach (XNode folderNode in subFoldersList)
                {
                    AddSubFolderFromXml(folderNode);
                }
            }
#endif
        }

        // This function saves the current folder hierarchy to a local Xml file.
        public void SaveFolderInfo()
        {
            if (this.id != "0")
                throw new Exception("This method is only valid on the root folder.");

            XDocument folderInfo = new XDocument();
#if OLDWAY
            XNode rootNode = folderInfo.CreateElement("RootFolder");
            folderInfo.AppendChild(rootNode);

            XNode pathNode = folderInfo.CreateElement("Path");
            pathNode.InnerText = this.saveLocation;
            rootNode.AppendChild(pathNode);

            XNode syncKeyNode = folderInfo.CreateElement("SyncKey");
            syncKeyNode.InnerText = this.syncKey;
            rootNode.AppendChild(syncKeyNode);

            XNode lastSyncTimeNode = folderInfo.CreateElement("LastSyncTime");
            lastSyncTimeNode.InnerText = XmlConvert.ToString(this.lastSyncTime, XmlDateTimeSerializationMode.Utc);
            rootNode.AppendChild(lastSyncTimeNode);

            XNode foldersNode = folderInfo.CreateElement("Folders");
            rootNode.AppendChild(foldersNode);

            foreach (Folder subFolder in this.subFolders)
            {
                subFolder.GenerateXmlNode(foldersNode);
            }

            folderInfo.Save(this.saveLocation + "\\" + folderInfoFileName);
#endif
        }

        // This function generates an XNode for the <Folder> element
        // for this folder.
        private void GenerateXmlNode(XElement parentNode)
        {
            XElement folderNode = new XElement("Folder");
            parentNode.Add(folderNode);
            folderNode.Add(new XElement("Name", this.name));
            folderNode.Add(new XElement("Id", this.id));
            folderNode.Add(new XElement("Type", ((Int32)this.type).ToString())); // TODO Will this cast work?  CHECK!
            folderNode.Add(new XElement("SyncKey", this.syncKey));
        }

        // This function generates an <Options> node for a Sync
        // command based on the settings for this folder.
        public void GenerateOptionsXml(XElement rootNode, XNamespace syncnamespace, XNamespace syncbasenamespace)
        {
            XElement optionsNode = new XElement( syncnamespace + "Options");
            rootNode.Add(optionsNode);
            
            if(this.options.FilterType != SyncFilterType.NoFilter)
            {
                int ft = (int)options.FilterType;
                XElement filterTypeNode = new XElement(syncnamespace + "FilterType", new XText(ft.ToString()));
                optionsNode.Add(filterTypeNode);
            }
            
            if (this.options.Class != null)
            {
                XElement classNode = new XElement(syncnamespace + "Class", new XText(this.options.Class));
                optionsNode.Add(classNode);
            }

            if (this.options.BodyPreference != null && this.options.BodyPreference[0] != null)
            {
                XElement BodyPreferenceNode = new XElement(syncbasenamespace + "BodyPreference");
                optionsNode.Add(BodyPreferenceNode);

                int bpType = (int)options.BodyPreference[0].Type;
                XElement bpTypeNode = new XElement( syncbasenamespace + "Type", new XText(bpType.ToString()));
                BodyPreferenceNode.Add(bpTypeNode);

                if(options.BodyPreference[0].TruncationSize > 0)
                {
                    XElement truncationSizeNode = new XElement(syncbasenamespace + "TruncationSize", new XText(options.BodyPreference[0].TruncationSize.ToString()));
                    BodyPreferenceNode.Add(truncationSizeNode);
                }

                if (this.options.BodyPreference[0].AllOrNone == true)
                {
                    XElement allOrNoneNode = new XElement(syncbasenamespace + "AllOrNone", new XText("1".ToString()));
                    BodyPreferenceNode.Add(allOrNoneNode);
                }

                if (this.options.BodyPreference[0].Preview > -1)
                {
                    XElement previewNode = new XElement(syncbasenamespace + "Preview", new XText(options.BodyPreference[0].Preview.ToString()));
                    BodyPreferenceNode.Add(previewNode);
                }
            }

            if (this.options.BodyPartPreference != null)
            {
                XElement bodyPartPreferenceNode = new XElement(syncbasenamespace + "BodyPartPreference");
                optionsNode.Add(bodyPartPreferenceNode);

                int bpType = (int)options.BodyPartPreference.Type;
                XElement bpTypeNode = new XElement(syncbasenamespace + "Type", new XText(bpType.ToString()));
                bodyPartPreferenceNode.Add(bpTypeNode);

                if (options.BodyPartPreference.TruncationSize > 0)
                {
                    XElement truncationSizeNode = new XElement(syncbasenamespace + "TruncationSize", new XText(options.BodyPartPreference.TruncationSize.ToString()));
                    bodyPartPreferenceNode.Add(truncationSizeNode);
                }

                if (this.options.BodyPartPreference.AllOrNone == true)
                {
                    XElement allOrNoneNode = new XElement(syncbasenamespace + "AllOrNone", new XText("1".ToString()));
                    bodyPartPreferenceNode.Add(allOrNoneNode);
                }

                if (this.options.BodyPartPreference.Preview > -1)
                {
                    XElement previewNode = new XElement(syncbasenamespace + "Preview", new XText(options.BodyPartPreference.Preview.ToString()));
                    bodyPartPreferenceNode.Add(previewNode);
                }
            }


            if (this.options.MimeSupportLevel != MimeSupport.NeverSendMime)
            {
                int ms = (int)options.MimeSupportLevel;
                XElement mimeSupportNode = new XElement(syncnamespace + "MIMESupport", new XText(ms.ToString()));
                optionsNode.Add(mimeSupportNode);
            }

            if (this.options.MimeTruncation != MimeTruncationType.NoTruncate)
            {
                int mt = (int)options.MimeTruncation;
                XElement mimeTruncationNode = new XElement(syncnamespace + "MIMETruncation", new XText(mt.ToString()));
                optionsNode.Add(mimeTruncationNode);
            }

            if (this.options.MaxItems > -1)
            {
                XElement maxItemsNode = new XElement(syncnamespace + "MaxItems", new XText(options.MaxItems.ToString()));
                optionsNode.Add(maxItemsNode);
            }
            if (this.options.IsRightsManagementSupported)
            {
                XElement maxItemsNode = new XElement(syncnamespace + "RightsManagementSupport", new XText("1".ToString()));
                optionsNode.Add(maxItemsNode);
            }
        }
    }
}
