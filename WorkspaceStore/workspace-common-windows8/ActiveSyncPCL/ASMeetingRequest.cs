﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public enum EnumCalendarCommandType
    {
        Add,
        Delete,
        Change
    }
    public enum CMRecurrenceType
    {
        CMRecurrenceTypeDaily = 0,
        CMRecurrenceTypeWeekly = 1,
        CMRecurrenceTypeMonthly = 2,
        CMRecurrenceTypeMonthlyNDay = 3,
        CMRecurrenceTypeYearly = 5,
        CMRecurrenceTypeYearlyNDay = 6
    };

    public enum CMASCalendarType
    {
        CMCalendarTypeDefault = 0,
        CMCalendarTypeGregorian = 1,
        CMCalendarTypeGregorianUS = 2,
        CMCalendarTypeJapanese = 3,
        CMCalendarTypeTaiwan = 4,
        CMCalendarTypeKorean = 5,
        CMCalendarTypeHijri = 6,
        CMCalendarTypeThai = 7,
        CMCalendarTypeHebrew = 8,
        CMCalendarTypeGRMiddleEast = 9,
        CMCalendarTypeGRArabic = 10,
        CMCalendarTypeGRTransEnglish = 11,
        CMCalendarTypeGRTransFrench = 12,
        CMCalendarTypeJapaneseLunar = 14,
        CMCalendarTypeChineseLunar = 15,
        CMCalendarTypeKoreanLunar = 20,
    };

    [Flags]
    public enum CMDayOfWeekType
    {
        CMDayOfWeekTypeSunday = 1,
        CMDayOfWeekTypeMonday = 2,
        CMDayOfWeekTypeTuesday = 4,
        CMDayOfWeekTypeWednesday = 8,
        CMDayOfWeekTypeThursday = 16,
        CMDayOfWeekTypeFriday = 32,
        CMDayOfWeekTypeWeekdays = 62,
        CMDayOfWeekTypeSaturday = 64,
        CMDayOfWeekTypeWeekends = 65,
        CMDayOfWeekTypeLastDayofMonth = 127
    };

    public enum CMFirstDayOfWeekType
    {
        CMFirstDayOfWeekTypeSunday = 0,
        CMFirstDayOfWeekTypeMonday = 1,
        CMFirstDayOfWeekTypeTuesday = 2,
        CMFirstDayOfWeekTypeWednesday = 3,
        CMFirstDayOfWeekTypeThursday = 4,
        CMFirstDayOfWeekTypeFriday = 5,
        CMFirstDayOfWeekTypeSaturday = 6
    };

    public class CMRecurrence
    {
        public CMRecurrenceType RecurrenceType { get; set; }
        public int Interval { get; set; }
        public int DayOfWeek { get; set; }
        public int DayOfMonth { get; set; }
        public int WeekOfMonth { get; set; }
        public int MonthOfYear { get; set; }
        public CMFirstDayOfWeekType FirstDayOfWeek { get; set; }
        public bool IsLeapMonth { get; set; }
        public CMASCalendarType CalendarType { get; set; }
        public DateTime Until { get; set; }
        public int Occurences { get; set; }
        public string VisibleExceptions { get; set; }
        public string HiddenExceptions { get; set; }
    }

    public enum EventStatus
    {
        None = 0,
        Organizer = 1,
        Tentative = 2,
        Accepted = 3,
        Declined = 4,
        NotResponded = 5
    };


    public enum MeetingStatusType
    {
        Appointment = 0,
        MeetingOrganizer = 1,
        MeetingGuest = 3,
        OrganizerCanceled = 5,
        GuestCanceled = 7
    };

    public sealed class EASAttendee
    {
        public string Email { get; set; }

        public string Name { get; set; }

        public EventStatus AttendeeStatus { get; set; }

        public MeetingStatusType AttendeeType { get; set; }

        public EASAttendee()
        {

        }
    }

    public class ASMeetingRequest : ASCommandRequest
    {
        public FolderSyncOptions SyncOptions { get; set; }

        public XElement ApplicationDataNode { get; private set; }

        public int WindowSize { get; set; }

        public string SyncKey { get; set; }

        public string CollectionId { get; set; }

        public string ClientId { get; set; }

        public string TimeZone { get; set; }

        public string DtStamp { get; set; }

        public string StartTime { get; set; }

        public string Subject { get; set; }

        public string UID { get; set; }

        public string Location { get; set; }

        public string EndTime { get; set; }

        public string Type { get; set; }

        public string Data { get; set; }

        public string Sensitivity { get; set; }

        public string BusyStatus { get; set; }

        public string AllDayEvent { get; set; }

        public string Reminder { get; set; }

        public string MeetingStatus { get; set; }

        public List<EASAttendee> Attendees { get; set; }

        public string OrganizerName { get; set; }

        public string OrganizerEmail { get; set; }

        //PK:Adding recurrence and changes for edit
        public CMRecurrence Recurrence { get; set; }
        public EnumCalendarCommandType CommandType { get; set; }
        public string serverID { get; set; }
        public bool DeleteThisOccurrence { get; set; }
        public bool CreateException { get; set; }
        public string exceptionStartTime { get; set; }
        public string UpdatedStartTime { get; set; }
        public string UpdateEndTime { get; set; }
        public string UpdatedReminder { get; set; }
        public ASMeetingRequest()
        {
            Command = "Sync";
        }

        // This function generates an ASFolderSyncResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            // return null; // new ASFolderSyncResponse(httpResp);
            return new ASMeetingResponse(httpResp, CreateException);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASMeetingResponse(statusCode);
        }


        // This function generates the XML request body
        // for the FolderSync request.
        protected override void GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument syncXML = new XDocument(new XDeclaration("1.0", "utf-8", null));
            XNamespace airsyncbasenamespace = Namespaces.airSyncBaseNamespace;
            XNamespace asyncnamespace = Namespaces.airSyncNamespace;
            XNamespace calendarnamespace = Namespaces.calendarNamespace;
            XElement syncNode = new XElement(asyncnamespace + "Sync",
                new XAttribute(XNamespace.Xmlns + Xmlns.calendarXmlns, Namespaces.calendarNamespace),
                new XAttribute(XNamespace.None + Xmlns.airSyncXmlns, Namespaces.airSyncNamespace),
                new XAttribute(XNamespace.Xmlns + Xmlns.airSyncBaseXmlns, Namespaces.airSyncBaseNamespace)
                );

            syncXML.Add(syncNode);

            // Only add a collections node if there are folders in the request.
            // If omitting, there should be a Partial element.

            XElement collectionsNode = new XElement(asyncnamespace + "Collections");
            syncNode.Add(collectionsNode);
            //foreach (Folder folder in folderList)
            //{
            XElement collectionNode = new XElement(asyncnamespace + "Collection");
            collectionsNode.Add(collectionNode);

            XElement syncKeyNode = new XElement(asyncnamespace + "SyncKey", new XText(SyncKey));
            collectionNode.Add(syncKeyNode);

            XElement collectionIdNode = new XElement(asyncnamespace + "CollectionId", new XText(CollectionId));
            collectionNode.Add(collectionIdNode);
            collectionNode.Add(new XElement(asyncnamespace + "DeletesAsMoves"));
            collectionNode.Add(new XElement(asyncnamespace + "GetChanges"));

            if (WindowSize > 0)
            {
                collectionNode.Add(new XElement(asyncnamespace + "WindowSize", new XText(WindowSize.ToString())));
            }
            if (SyncOptions != null)
            {
                XElement optionsNode = new XElement(asyncnamespace + "Options");
                collectionNode.Add(optionsNode);
                if (SyncOptions.FilterType != SyncFilterType.NoFilter)
                {
                    int ft = (int)SyncOptions.FilterType;
                    XElement filterTypeNode = new XElement(asyncnamespace + "FilterType", new XText(ft.ToString()));
                    optionsNode.Add(filterTypeNode);
                }
                if (SyncOptions.BodyPreference != null && SyncOptions.BodyPreference[0] != null)
                {
                    XElement BodyPreferenceNode = new XElement(airsyncbasenamespace + "BodyPreference");
                    optionsNode.Add(BodyPreferenceNode);

                    int bpType = (int)SyncOptions.BodyPreference[0].Type;
                    XElement bpTypeNode = new XElement(airsyncbasenamespace + "Type", new XText(bpType.ToString()));
                    BodyPreferenceNode.Add(bpTypeNode);

                    if (SyncOptions.BodyPreference[0].TruncationSize > 0)
                    {
                        XElement truncationSizeNode = new XElement(airsyncbasenamespace + "TruncationSize", new XText(SyncOptions.BodyPreference[0].TruncationSize.ToString()));
                        BodyPreferenceNode.Add(truncationSizeNode);
                    }
                }
            }

            XElement commandsNode = new XElement(asyncnamespace + "Commands");
            collectionNode.Add(commandsNode);

            XElement commandNode = null;
            if (CommandType == EnumCalendarCommandType.Add)
            {
                commandNode = new XElement(asyncnamespace + "Add");
                commandsNode.Add(commandNode);
                XElement ClientIdNode = new XElement(asyncnamespace + "ClientId", new XText(ClientId));
                commandNode.Add(ClientIdNode);
            }
            else
            {
                commandNode = new XElement(asyncnamespace + "Change");
                commandsNode.Add(commandNode);

                XElement serverIDNode = new XElement(asyncnamespace + "ServerId", serverID);
                commandNode.Add(serverIDNode);
            }

            // ApplicationData Node
            ApplicationDataNode = new XElement(asyncnamespace + "ApplicationData");

            commandNode.Add(ApplicationDataNode);

            //if (CommandType == EnumCalendarCommandType.Change)
            //{
            //    XElement UIDNode = new XElement(asyncnamespace + "UID", UID);
            //    ApplicationDataNode.Add(UIDNode);
            //}


            XElement CalendarDtStampNode = new XElement(calendarnamespace + "DtStamp", DtStamp);
            ApplicationDataNode.Add(CalendarDtStampNode);
            XElement CalendarStartTimeNode = new XElement(calendarnamespace + "StartTime", StartTime);
            ApplicationDataNode.Add(CalendarStartTimeNode);
            XElement CalendarEndTimeNode = new XElement(calendarnamespace + "EndTime", EndTime);
            ApplicationDataNode.Add(CalendarEndTimeNode);
            XElement CalendarSubjectNode = new XElement(calendarnamespace + "Subject", Subject);
            ApplicationDataNode.Add(CalendarSubjectNode);
            XElement CalendarUIDNode = new XElement(calendarnamespace + "UID", UID);
            ApplicationDataNode.Add(CalendarUIDNode);
            //XElement OrganizerNameNode = new XElement(calendarnamespace + "OrganizerName", OrganizerName);
            //ApplicatioDataNode.Add(OrganizerNameNode);
            XElement OrganizerEmailNode = new XElement(calendarnamespace + "OrganizerEmail", OrganizerEmail);
            ApplicationDataNode.Add(OrganizerEmailNode);

            if (null != Attendees && Attendees.Count > 0)
            {
                XElement CalendarAttendeesNode = new XElement(calendarnamespace + "Attendees");
                ApplicationDataNode.Add(CalendarAttendeesNode);
                foreach (EASAttendee att in Attendees)
                {
                    XElement CalendarAttendeeNode = new XElement(calendarnamespace + "Attendee");
                    CalendarAttendeesNode.Add(CalendarAttendeeNode);
                    XElement CalendarAttendeeEmailNode = new XElement(calendarnamespace + "Email", att.Email);
                    CalendarAttendeeNode.Add(CalendarAttendeeEmailNode);
                    XElement CalendarAttendeetNameNode = new XElement(calendarnamespace + "Name", att.Name);
                    CalendarAttendeeNode.Add(CalendarAttendeetNameNode);
                    //XElement CalendarAttendeeStatusNode = new XElement(calendarnamespace + "AttendeeStatus", Convert.ToString((int)att.AttendeeStatus));
                    //CalendarAttendeeNode.Add(CalendarAttendeeStatusNode);
                    //XElement CalendarAttendeeTypeNode = new XElement(calendarnamespace + "AttendeeType",Convert.ToString((int)att.AttendeeType));
                    //CalendarAttendeeNode.Add(CalendarAttendeeTypeNode);
                }

            }

            if (Location != null && Location.Length > 0)
            {
                XElement CalendarLocationNode = new XElement(calendarnamespace + "Location", Location);
                ApplicationDataNode.Add(CalendarLocationNode);
            }

            // The 'Body' element is a required element.
            if (null != Data && Data.Length > 0)
            {
                XElement AirsyncbaseBodyNode = new XElement(airsyncbasenamespace + "Body");
                ApplicationDataNode.Add(AirsyncbaseBodyNode);

                // Add the child elements below the "Body" element
                XElement AirsyncbaseTypeNode = new XElement(airsyncbasenamespace + "Type", "1");
                AirsyncbaseBodyNode.Add(AirsyncbaseTypeNode);

                //byte[] dataAsBytes = Encoding.UTF8.GetBytes(Data);
                //string base64EncodedData = Convert.ToBase64String(dataAsBytes);

                XElement AirsyncbaseDataNode = new XElement(airsyncbasenamespace + "Data", Data);
                AirsyncbaseBodyNode.Add(AirsyncbaseDataNode);
            }


            XElement CalendarSensitivityNode = new XElement(calendarnamespace + "Sensitivity", "1"/*Sensitivity*/);
            ApplicationDataNode.Add(CalendarSensitivityNode);
            XElement CalendarBusyStatusNode = new XElement(calendarnamespace + "BusyStatus", "2"/*BusyStatus*/);
            ApplicationDataNode.Add(CalendarBusyStatusNode);
            XElement CalendarAllDayEventNode = new XElement(calendarnamespace + "AllDayEvent", AllDayEvent);
            ApplicationDataNode.Add(CalendarAllDayEventNode);

            //PK: We should send recurrence as well
            //<calendar:Recurrence>
            //<calendar:Type>1</calendar:Type>
            //<calendar:Interval>1</calendar:Interval>
            //<calendar:Occurrences>3</calendar:Occurrences>
            //<calendar:DayOfWeek>32</calendar:DayOfWeek>
            //</calendar:Recurrence>

            if (Recurrence != null)
            {
                XElement CalendarRecurrenceNode = new XElement(calendarnamespace + "Recurrence");//recurrence;
                ApplicationDataNode.Add(CalendarRecurrenceNode);

                //CMRecurrenceType recurType = Recurrence.RecurrenceType;
                //if ((int)Recurrence.RecurrenceType == 0)
                {
                    string recurrenceTypeString = Convert.ToString((int)Recurrence.RecurrenceType);
                    XElement CalendarRecurrenceTypeNode = new XElement(calendarnamespace + "Type", recurrenceTypeString);
                    CalendarRecurrenceNode.Add(CalendarRecurrenceTypeNode);

                    //int interval = Recurrence.Interval;
                    string intervalString = Convert.ToString(Recurrence.Interval);
                    XElement CalendarRecurrenceIntervalNode = new XElement(calendarnamespace + "Interval", intervalString);
                    CalendarRecurrenceNode.Add(CalendarRecurrenceIntervalNode);
                }
                //int occurences = Recurrence.Occurences;
                //string occurenceString = Convert.ToString(Recurrence.Occurences);
                //XElement CalendarRecurrenceOccurenceNode = new XElement(calendarnamespace + "Occurrences", occurenceString);
                //CalendarRecurrenceNode.Add(CalendarRecurrenceOccurenceNode);

                if ((int)Recurrence.RecurrenceType == 1 || (int)Recurrence.RecurrenceType == 3 || (int)Recurrence.RecurrenceType == 6)
                {
                    string daysOfWeekString = Convert.ToString(Recurrence.DayOfWeek);
                    XElement CalendarRecurrenceDayOfWeekNode = new XElement(calendarnamespace + "DayOfWeek", daysOfWeekString);
                    CalendarRecurrenceNode.Add(CalendarRecurrenceDayOfWeekNode);
                }

                if ((int)Recurrence.RecurrenceType == 2 || (int)Recurrence.RecurrenceType == 5)
                {
                    string dayOfMonthString = Convert.ToString(Recurrence.DayOfMonth);
                    XElement CalendarRecurrenceDayOfMonthNode = new XElement(calendarnamespace + "DayOfMonth", dayOfMonthString);
                    CalendarRecurrenceNode.Add(CalendarRecurrenceDayOfMonthNode);
                }

                if ((int)Recurrence.RecurrenceType == 3 || (int)Recurrence.RecurrenceType == 6)
                {
                    string weekOfMonthString = Convert.ToString(Recurrence.WeekOfMonth);
                    XElement CalendarRecurrenceWeekOfMonthNode = new XElement(calendarnamespace + "WeekOfMonth", weekOfMonthString);
                    CalendarRecurrenceNode.Add(CalendarRecurrenceWeekOfMonthNode);
                }

                if ((int)Recurrence.RecurrenceType == 5 || (int)Recurrence.RecurrenceType == 6)
                {
                    string monthOfYearString = Convert.ToString(Recurrence.MonthOfYear);
                    XElement CalendarRecurrenceMonthOfYearStringNode = new XElement(calendarnamespace + "MonthOfYear", monthOfYearString);
                    CalendarRecurrenceNode.Add(CalendarRecurrenceMonthOfYearStringNode);
                }
                //string FirstDayOfWeekString = Convert.ToString((int)Recurrence.FirstDayOfWeek);
                //XElement CalendarRecurrencefirstDayOfWeekStringNode = new XElement(calendarnamespace + "FirstDayOfWeek", FirstDayOfWeekString);
                //CalendarRecurrenceNode.Add(CalendarRecurrencefirstDayOfWeekStringNode);

                //PK: Do we have any way to set it?
                //string isLeapMonthString = "0";
                //if (Recurrence.IsLeapMonth == true)
                //    isLeapMonthString = "1";
                //XElement CalendarRecurrenceIsleapMonthStringNode = new XElement(calendarnamespace + "FirstDayOfWeek", isLeapMonthString);
                //CalendarRecurrenceNode.Add(CalendarRecurrenceIsleapMonthStringNode);
                if ((int)Recurrence.RecurrenceType == 2 || (int)Recurrence.RecurrenceType == 3 || (int)Recurrence.RecurrenceType == 6)
                {
                    string calendarTypeString = Convert.ToString((int)Recurrence.CalendarType);
                    XElement CalendarRecurrenceCalendarTypeStringNode = new XElement(calendarnamespace + "FirstDayOfWeek", calendarTypeString);
                    CalendarRecurrenceNode.Add(CalendarRecurrenceCalendarTypeStringNode);
                }

            }

            if (!"0".Equals(Reminder))
            {
                XElement CalendarReminderNode = new XElement(calendarnamespace + "Reminder", Reminder);
                ApplicationDataNode.Add(CalendarReminderNode);
            }
            if (CreateException)
            {
                XElement ExceptionsNode = new XElement(calendarnamespace + "Exceptions");
                ApplicationDataNode.Add(ExceptionsNode);
                XElement ExceptionNode = new XElement(calendarnamespace + "Exception");
                ExceptionsNode.Add(ExceptionNode);
                if (DeleteThisOccurrence)
                {
                    XElement DeleteNode = new XElement(calendarnamespace + "Deleted", "1");
                    ExceptionNode.Add(DeleteNode);
                    XElement ExceptionStartTimeNode = new XElement(calendarnamespace + "ExceptionStartTime", exceptionStartTime);
                    ExceptionNode.Add(ExceptionStartTimeNode);
                }
                else
                {
                    XElement DtStampN = new XElement(calendarnamespace + "DtStamp", DtStamp);
                    ExceptionNode.Add(DtStampN);
                    XElement StartTimeN = new XElement(calendarnamespace + "StartTime", UpdatedStartTime);
                    ExceptionNode.Add(StartTimeN);
                    XElement EndTimeN = new XElement(calendarnamespace + "EndTime", UpdateEndTime);
                    ExceptionNode.Add(EndTimeN);
                    XElement ExceptionStartTimeNode = new XElement(calendarnamespace + "ExceptionStartTime", exceptionStartTime);
                    ExceptionNode.Add(ExceptionStartTimeNode);
                    XElement SubjectNode = new XElement(calendarnamespace + "Subject", Subject);
                    ExceptionNode.Add(SubjectNode);
                    XElement LocationNode = new XElement(calendarnamespace + "Location", Location);
                    ExceptionNode.Add(LocationNode);
                    XElement ReminderNode = new XElement(calendarnamespace + "Reminder", UpdatedReminder);
                    ExceptionNode.Add(ReminderNode);

                    if (null != Data)
                    {
                        XElement AirsyncbaseBodyNode = new XElement(airsyncbasenamespace + "Body");
                        ExceptionNode.Add(AirsyncbaseBodyNode);
                        XElement AirsyncbaseTypeNode = new XElement(airsyncbasenamespace + "Type", "3");
                        AirsyncbaseBodyNode.Add(AirsyncbaseTypeNode);
                        XElement AirsyncbaseDataNode = new XElement(airsyncbasenamespace + "Data", Data);
                        AirsyncbaseBodyNode.Add(AirsyncbaseDataNode);
                    }
                    if (null != Attendees && Attendees.Count > 0)
                    {
                        XElement CalendarAttendeesNode = new XElement(calendarnamespace + "Attendees");
                        ExceptionNode.Add(CalendarAttendeesNode);
                        foreach (EASAttendee att in Attendees)
                        {
                            XElement CalendarAttendeeNode = new XElement(calendarnamespace + "Attendee");
                            CalendarAttendeesNode.Add(CalendarAttendeeNode);
                            XElement CalendarAttendeeEmailNode = new XElement(calendarnamespace + "Email", att.Email);
                            CalendarAttendeeNode.Add(CalendarAttendeeEmailNode);
                            XElement CalendarAttendeetNameNode = new XElement(calendarnamespace + "Name", att.Name);
                            CalendarAttendeeNode.Add(CalendarAttendeetNameNode);
                            XElement CalendarAttendeeStatusNode = new XElement(calendarnamespace + "AttendeeStatus", Convert.ToString((int)att.AttendeeStatus));
                            CalendarAttendeeNode.Add(CalendarAttendeeStatusNode);
                            XElement CalendarAttendeeTypeNode = new XElement(calendarnamespace + "AttendeeType", Convert.ToString((int)att.AttendeeType));
                            CalendarAttendeeNode.Add(CalendarAttendeeTypeNode);
                        }

                    }
                    XElement BusyStatusNode = new XElement(calendarnamespace + "BusyStatus", BusyStatus);
                    ExceptionNode.Add(BusyStatusNode);
                }
            }

            XElement CalendarMeetingStatusNode = new XElement(calendarnamespace + "MeetingStatus", MeetingStatus);
            ApplicationDataNode.Add(CalendarMeetingStatusNode);
            // If a wait period was specified, include it here          

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Async = false;
            settings.Indent = true;
            settings.IndentChars = "  ";
            settings.Encoding = Encoding.UTF8;
            Utf8StringWriter sw = new Utf8StringWriter();

            XmlWriter writer = XmlWriter.Create(sw, settings);

            syncXML.WriteTo(writer);
            writer.Flush();

            XmlString = sw.ToString();
        }
    }
}
