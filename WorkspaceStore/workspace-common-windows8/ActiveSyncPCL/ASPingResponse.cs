﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASPingResponse : ASCommandResponse
    {
        public enum PingStatus
        {
            ExpiredWithNoChanges = 1,   // The heartbeat interval expired before any changes occurred in the folders being monitored.
            Changes = 2,                //Changes occurred in at least one of the monitored folders. The response specifies the changed folders.
            MissingParams = 3,          //The Ping command request omitted required parameters.
            SyntaxError = 4,            //Syntax error in Ping command request.
            HeartbeatOutOfRange = 5,    //The specified heartbeat interval is outside the allowed range. For intervals that were too short, the response contains the shortest allowed interval. For intervals that were too long, the response contains the longest allowed interval.
            TooManyFolders = 6,         //The Ping command request specified more than the allowed number of folders to monitor. The response indicates the allowed number in the MaxFolders element (section 2.2.3.92).
            HierarchySyncRequired = 7,  //Folder hierarchy sync required.
            ServerError = 8
        }

        private XDocument _responseXml = null;

        private Int32 status = 0;
        private List<Folder> folderList = null;
        private Int32 maxFolders = 0;
        private Int32 heartbeatInterval = 0;
        
        #region Property Accessors
        public Int32 Status
        {
            get
            {
                return status;
            }
        }
        public Int32 MaxFolders
        {
            get
            {
                return maxFolders;
            }
        }
        public Int32 HeartbeatInterval
        {
            get
            {
                return heartbeatInterval;
            }
        }
        #endregion

        public ASPingResponse(HttpWebResponse httpResponse, List<Folder> folders)
            : base(httpResponse)
        {
            folderList = folders;

            // Sync responses can be empty
            if (null != XmlString && XmlString != "")
            {
                _responseXml = new XDocument();
                TextReader tr = new StringReader(XmlString);
                _responseXml = XDocument.Load(tr, LoadOptions.None);
                
                SetStatus();
            }
        }

        public ASPingResponse(HttpStatusCode statusCode)
            : base (statusCode)
        {
        }

        static private XElement GetXElement(XElement xe, string tag)
        {
            if (xe != null && xe.HasElements && xe.Element(tag) != null)
                return xe.Element(tag);
            else
                return null;
        }


        public XDocument GetResponseXMLDoc()
        {
            return _responseXml;
        }

        //PLS Dont DELETE : We might need similar things later on
        // This function extracts the response status from the 
        // XML and sets the status property. We need to check whether we need to save it in folder
        private void SetStatus()
        {
            string ns = "{Ping}";
            if (_responseXml != null)
            {
                var airPingQ = _responseXml.Descendants().SingleOrDefault(p => p.Name == (ns + "Ping"));
                if (airPingQ != null)
                {
                    var statusQ = airPingQ.Descendants().SingleOrDefault(p => p.Name == (ns + "Status"));
                    if (statusQ != null)
                    {
                        status = XmlConvert.ToInt32(statusQ.Value);
                    }
                }
            }
        }
    }
}
