﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    // This class represents a Provision command request
    // as specified in MS-ASPROV section 2.2.
    public class ASProvisionRequest : ASCommandRequest
    {
        // This enumeration covers the acceptable values
        // of the Status element in a Provision request
        // when acknowledging a policy, as specified
        // in MS-ASPROV section 3.1.5.1.2.1.
        public enum PolicyAcknowledgement
        {
            Success = 1,
            PartialSuccess = 2,
            PolicyIgnored = 3,
            ExternalManagement = 4
        }

        private static string policyType = "MS-EAS-Provisioning-WBXML";

        private bool isAcknowledgement = false;
        private bool isRemoteWipe = false;
        private Int32 status = 0;

        private Device provisionDevice = null;

        #region Property Accessors

        public bool IsAcknowledgement
        {
            get
            {
                return isAcknowledgement;
            }
            set
            {
                isAcknowledgement = value;
            }
        }

        public bool IsRemoteWipe
        {
            get 
            {
                return isRemoteWipe;
            }
            set
            {
                isRemoteWipe = value;
            }
        }

        public Device ProvisionDevice
        {
            get
            {
                return provisionDevice;
            }
            set
            {
                provisionDevice = value;
            }
        }

        public Int32 Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        #endregion

        public ASProvisionRequest()
        {
            Command = "Provision";
        }

        // This function generates an ASProvisionResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return new ASProvisionResponse(httpResp);
        }

        protected override ASCommandResponse WrapHttpResponse(HttpStatusCode statusCode)
        {
            return new ASProvisionResponse(statusCode);
        }

        /*
          <?xml version="1.0" encoding="utf-8" ?> 
        - <provision:Provision xmlns:provision="Provision">
        -   <settings:DeviceInformation xmlns:settings="Settings">
        -     <settings:Set>
                <settings:Model>Sample Model</settings:Model> 
                <settings:FriendlyName>FolderSync/Sync Example</settings:FriendlyName> 
                <settings:OS>Sample OS 1.0</settings:OS> 
                <settings:OSLanguage>English</settings:OSLanguage> 
                <settings:PhoneNumber>425-555-1000</settings:PhoneNumber> 
                <settings:MobileOperator>Phone Company</settings:MobileOperator> 
                <settings:UserAgent>EX2010_activesyncfolder_cs_1.0</settings:UserAgent> 
              </settings:Set>
            </settings:DeviceInformation>
        - <provision:Policies>
        -   <provision:Policy>
              <provision:PolicyType>MS-EAS-Provisioning-WBXML</provision:PolicyType> 
            </provision:Policy>
          </provision:Policies>
          </provision:Provision>
         */
        // This function generates the XML request body
        // for the Provision request.
        protected override void GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;
            XDocument provisionXML = new XDocument();
            provisionXML.Declaration = new XDeclaration("1.0", "utf-8", null);
            XNamespace provision = Namespaces.provisionNamespace;

            try
            {
                provisionXML.Add(new XElement(provision + "Provision",
                    new XAttribute(XNamespace.Xmlns + Xmlns.provisionXmlns, Namespaces.provisionNamespace)
                    ));
                XElement provisionNode = provisionXML.Element(provision + "Provision");
                if(isRemoteWipe)
                {

                }
                else
                {
                    string[] majorAndMinorProtocolVersions = ProtocolVersion.Split('.');

                    int protocolVersionMajor = Convert.ToInt32(majorAndMinorProtocolVersions[0]);
                    int protocolVersionMinor = 0;
                    if (majorAndMinorProtocolVersions.Length > 1)
                    {
                        protocolVersionMinor = Convert.ToInt32(majorAndMinorProtocolVersions[1]);
                    }
                    if (!isAcknowledgement && protocolVersionMajor > 12)
                    {
                        // A DeviceInformation node is only included in the initial
                        // request.
                        XElement deviceNode = ProvisionDevice.GetDeviceInformationNode(); // provisionXML.ImportNode(provisionDevice.GetDeviceInformationNode(), true);
                        provisionNode.Add(deviceNode);
                    }

                    provisionNode.Add(new XElement(provision + "Policies"
                        ));

                    // [TODO] Just one policy?
                    XElement policiesNode = provisionNode.Element(provision + "Policies");
                    policiesNode.Add(new XElement(provision + "Policy"
                        ));

                    XElement policyNode = policiesNode.Element(provision + "Policy");
                    policyNode.Add(new XElement(provision + "PolicyType",
                        new XText(policyType)
                        ));

                    if (isAcknowledgement)
                    {
                        // Need to also include policy key and status
                        // when acknowledging
                        policyNode.Add(new XElement(provision + "PolicyKey",
                            new XText(PolicyKey.ToString())
                            ));
                        policyNode.Add(new XElement(provision + "Status",
                            new XText(status.ToString())
                            ));
                    }
                }
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Async = false;
                settings.Indent = false;
                settings.Encoding = Encoding.UTF8;
                Utf8StringWriter sw = new Utf8StringWriter();

                XmlWriter writer = XmlWriter.Create(sw, settings);

                provisionXML.WriteTo(writer);
                writer.Flush();

                XmlString = sw.ToString();
            }
            catch (Exception e)
            {
                string msg = e.Message;
            }

#if OLDWAY
            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument provisionXML = new XDocument();

            XmlDeclaration xmlDeclaration = provisionXML.CreateXmlDeclaration("1.0", "utf-8", null);
            provisionXML.InsertBefore(xmlDeclaration, null);

            XNode provisionNode = provisionXML.CreateElement(Xmlns.provisionXmlns, "Provision", Namespaces.provisionNamespace);
            provisionNode.Prefix = Xmlns.provisionXmlns;
            provisionXML.AppendChild(provisionNode);

            // If this is a remote wipe acknowledgment, use
            // the remote wipe acknowledgment format
            // specified in MS-ASPROV section 3.1.5.1.2.2.
            if (isRemoteWipe)
            {
                // Build response to RemoteWipe request
                XNode remoteWipeNode = provisionXML.CreateElement(Xmlns.provisionXmlns, "RemoteWipe", Namespaces.provisionNamespace);
                remoteWipeNode.Prefix = Xmlns.provisionXmlns;
                provisionNode.AppendChild(remoteWipeNode);

                // Always return success for remote wipe
                XNode statusNode = provisionXML.CreateElement(Xmlns.provisionXmlns, "Status", Namespaces.provisionNamespace);
                statusNode.Prefix = Xmlns.provisionXmlns;
                statusNode.InnerText = "1";
                remoteWipeNode.AppendChild(statusNode);
            }

            // The other two possibilities here are
            // an initial request or an acknowledgment
            // of a policy received in a previous Provision
            // response.
            else
            {
                if (!isAcknowledgement)
                {
                    // A DeviceInformation node is only included in the initial
                    // request.
                    XNode deviceNode = provisionXML.ImportNode(provisionDevice.GetDeviceInformationNode(), true);
                    provisionNode.AppendChild(deviceNode);
                }

                // These nodes are included in both scenarios.
                XNode policiesNode = provisionXML.CreateElement(Xmlns.provisionXmlns, "Policies", Namespaces.provisionNamespace);
                policiesNode.Prefix = Xmlns.provisionXmlns;
                provisionNode.AppendChild(policiesNode);

                XNode policyNode = provisionXML.CreateElement(Xmlns.provisionXmlns, "Policy", Namespaces.provisionNamespace);
                policyNode.Prefix = Xmlns.provisionXmlns;
                policiesNode.AppendChild(policyNode);

                XNode policyTypeNode = provisionXML.CreateElement(Xmlns.provisionXmlns, "PolicyType", Namespaces.provisionNamespace);
                policyTypeNode.Prefix = Xmlns.provisionXmlns;
                policyTypeNode.InnerText = policyType;
                policyNode.AppendChild(policyTypeNode);

                if (isAcknowledgement)
                {
                    // Need to also include policy key and status
                    // when acknowledging
                    XNode policyKeyNode = provisionXML.CreateElement(Xmlns.provisionXmlns, "PolicyKey", Namespaces.provisionNamespace);
                    policyKeyNode.Prefix = Xmlns.provisionXmlns;
                    policyKeyNode.InnerText = PolicyKey.ToString();
                    policyNode.AppendChild(policyKeyNode);

                    XNode statusNode = provisionXML.CreateElement(Xmlns.provisionXmlns, "Status", Namespaces.provisionNamespace);
                    statusNode.Prefix = Xmlns.provisionXmlns;
                    statusNode.InnerText = status.ToString();
                    policyNode.AppendChild(statusNode);
                }
            }

            StringWriter stringWriter = new StringWriter();
            XmlTextWriter xmlWriter = new XmlTextWriter(stringWriter);
            xmlWriter.Formatting = Formatting.Indented;
            provisionXML.WriteTo(xmlWriter);
            xmlWriter.Flush();

            XmlString = stringWriter.ToString();
#endif
        }
    }
}
