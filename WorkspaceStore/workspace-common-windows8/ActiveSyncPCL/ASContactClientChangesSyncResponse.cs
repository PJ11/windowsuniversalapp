﻿﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASContactClientChangesSyncResponse : ASCommandResponse
    { 
      
        private XDocument mResponseXml = null;
        private int mSyncStatus = 0;
        private int mItemSyncStatus = 0;
        private string mAirSyncNS;
        public ASContactClientChangesSyncResponse(HttpWebResponse httpResponse)
               : base(httpResponse)

        {
            mAirSyncNS = "{AirSync}";
            if (XmlString != "")
            {
                mResponseXml = new XDocument();
                TextReader tr = new StringReader(XmlString);
                mResponseXml = XDocument.Load(tr, LoadOptions.None);
                SetStatus();
            }

        }

        public ASContactClientChangesSyncResponse(HttpStatusCode statusCode)
            : base(statusCode)
        {
        }

        public int SyncStatus
        {
            get
            {
                return mSyncStatus;
            }
        }

        public int ItemSyncStatus
        {
            get
            {
                return mItemSyncStatus;
            }
        }

        public string GetSyncKeyForFolder(string folderId)
        {
            string folderSyncKey = "0";

            string ns = "{AirSync}";
            IEnumerable<XElement> collection = mResponseXml.Descendants().Where(p => p.Name == (ns + "Collection")).ToList();
            if (collection != null)
            {
                foreach (XElement coll in collection)
                {
                    XElement j = coll as XElement;
                    if (j != null && (j.Descendants().First(p => p.Name == (ns + "CollectionId")).Value).ToString() == folderId.ToString())
                    {
                        XElement syncKey = j.Descendants().First(p => p.Name == (ns + "SyncKey"));
                        folderSyncKey = (string)syncKey.Value;
                        break;
                    }
                }
            }

            return folderSyncKey;
        }
    

        private void SetStatus()
        {
             if (mResponseXml != null)
             {
                 var collectionElement = mResponseXml.Descendants().SingleOrDefault(p => p.Name == (mAirSyncNS + "Collection"));
                 if (collectionElement != null)
                 {
                    var statusKey = collectionElement.Descendants().First(p => p.Name == (mAirSyncNS + "Status"));
                    {
                        mSyncStatus = XmlConvert.ToInt32(statusKey.Value);
                    }
                 }
            }
        }

        public string GetServerIdOfAddedContact(string clientID)
        {
            if (mResponseXml != null)
            {
                
                var addElement = mResponseXml.Element(mAirSyncNS + "Sync").
                                                    Element(mAirSyncNS + "Collections").
                                                    Element(mAirSyncNS + "Collection").
                                                    Element(mAirSyncNS + "Responses").
                                                    Elements(mAirSyncNS + "Add").
                                                    Where(x => x.Element(mAirSyncNS + "ClientId").Value == clientID).SingleOrDefault();


                if (addElement != null)
                {
                    var statusElement = addElement.Element(mAirSyncNS + "ServerId");
                    string statusOfElement = statusElement.Value;
                    return statusOfElement;
                }
                return null;

            }
            return null;
        }

        public int GetStatusOfAddedContact(string clientID)
        {
            if (mResponseXml != null)
            {
                var addElement = mResponseXml.Element(mAirSyncNS + "Sync").
                                                    Element(mAirSyncNS + "Collections").
                                                    Element(mAirSyncNS + "Collection").
                                                    Element(mAirSyncNS + "Responses").
                                                    Elements(mAirSyncNS + "Add").
                                                    Where(x => x.Element(mAirSyncNS + "ClientId").Value == clientID).SingleOrDefault();

                if (addElement != null)
                {
                    var statusElement = addElement.Element(mAirSyncNS + "Status");
                    int statusOfElement = XmlConvert.ToInt32(statusElement.Value);
                    return statusOfElement;
                }
                return -1;
            }
            return -1;
        }
    }

}

