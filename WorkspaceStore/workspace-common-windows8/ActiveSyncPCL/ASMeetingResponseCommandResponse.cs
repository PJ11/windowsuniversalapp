﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    public class ASMeetingResponseCommandResponse : ASCommandResponse
    {
        private XDocument mResponseXml = null;
        public Int32 status = 0;
        public ASMeetingResponseCommandResponse(HttpWebResponse httpResponse)
            : base(httpResponse)
        {
           
            if (httpResponse.StatusCode == HttpStatusCode.OK)
            {
                if (XmlString != null && XmlString != "")
                {
                    mResponseXml = new XDocument();
                    TextReader tr = new StringReader(XmlString);
                    mResponseXml = XDocument.Load(tr, LoadOptions.None);
                    SetStatus();
                }
            }
        }

        public ASMeetingResponseCommandResponse(HttpStatusCode statusCode)
            : base (statusCode)
        {
        }

        private void SetStatus()
        {
            string ns = "{MeetingResponse}";
            if (mResponseXml != null)
            {
                var meetingResposneResultQ = mResponseXml.Descendants().First(p => p.Name == (ns + "Result"));
                if (meetingResposneResultQ != null)
                {
                    var statusQ = meetingResposneResultQ.Descendants().First(p => p.Name == (ns + "Status"));
                    if (statusQ != null)
                    {
                        status = XmlConvert.ToInt32(statusQ.Value);
                    }
                }
            }
        }
    }
}

