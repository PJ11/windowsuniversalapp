﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Linq;


namespace ActiveSyncPCL
{
    // This class represents an Exchange
    // ActiveSync policy.
    public class ASPolicy
    {
        #region Enumerations
        public enum EncryptionAlgorithm
        {
            TripleDES = 0,
            DES = 1,
            RC2_128bit = 2,
            RC2_64bit = 3,
            RC2_40bit = 4
        }

        public enum SigningAlgorithm
        {
            SHA1 = 0,
            MD5 = 1
        }

        public enum CalendarAgeFilter
        {
            ALL = 0,
            TWO_WEEKS = 4,
            ONE_MONTH = 5,
            THREE_MONTHS = 6,
            SIX_MONTHS = 7
        }

        public enum MailAgeFilter
        {
            ALL = 0,
            ONE_DAY = 1,
            THREE_DAYS = 2,
            ONE_WEEK = 3,
            TWO_WEEKS = 4,
            ONE_MONTH = 5
        }

        public enum PolicyStatus
        {
            Success = 1,
            NoPolicyDefined = 2,
            PolicyTypeUnknown = 3,
            PolicyDataCorrupt = 4,
            PolicyKeyMismatch = 5
        }
        #endregion

        private Int32 status = 0;
        private UInt32 policyKey = 0;
        private byte allowBlueTooth = 0;
        private bool allowBrowser = false;
        private bool allowCamera = false;
        private bool allowConsumerEmail = false;
        private bool allowDesktopSync = false;
        private bool allowHTMLEmail = false;
        private bool allowInternetSharing = false;
        private bool allowIrDA = false;
        private bool allowPOPIMAPEmail = false;
        private bool allowRemoteDesktop = false;
        private bool allowSimpleDevicePassword = false;
        private Int32 allowSMIMEEncryptionAlgorithmNegotiation = 0;
        private bool allowSMIMESoftCerts = false;
        private bool allowStorageCard = false;
        private bool allowTextMessaging = false;
        private bool allowUnsignedApplications = false;
        private bool allowUnsignedInstallationPackages = false;
        private bool allowWifi = false;
        private bool alphanumericDevicePasswordRequired = false;
        private bool attachmentsEnabled = false;
        private bool devicePasswordEnabled = false;
        private UInt32 devicePasswordExpiration = 0;
        private UInt32 devicePasswordHistory = 0;
        private UInt32 maxAttachmentSize = 0;
        private UInt32 maxCalendarAgeFilter = 0;
        private UInt32 maxDevicePasswordFailedAttempts = 0;
        private UInt32 maxEmailAgeFilter = 0;
        private Int32 maxEmailBodyTruncationSize = -1;
        private Int32 maxEmailHTMLBodyTruncationSize = -1;
        private UInt32 maxInactivityTimeDeviceLock = 0;
        private byte minDevicePasswordComplexCharacters = 1;
        private byte minDevicePasswordLength = 1;
        private bool passwordRecoveryEnabled = false;
        private bool requireDeviceEncryption = false;
        private bool requireEncryptedSMIMEMessages = false;
        private Int32 requireEncryptionSMIMEAlgorithm = 0;
        private bool requireManualSyncWhenRoaming = false;
        private Int32 requireSignedSMIMEAlgorithm = 0;
        private bool requireSignedSMIMEMessages = false;
        private bool requireStorageCardEncryption = false;

        private string[] approvedApplicationList = null;
        private string[] unapprovedInROMApplicationList = null;

        private bool remoteWipeRequested = false;
        private bool hasPolicyInfo = false;


        #region Property Accessors
        public Int32 Status
        {
            get
            {
                return status;
            }
        }

        public UInt32 PolicyKey
        {
            get
            {
                return policyKey;
            }
        }

        public byte AllowBlueTooth 
        {
            get
            {
                return allowBlueTooth;
            }
        }

        public bool AllowBrowser
        {
            get 
            {
                return allowBrowser;
            }
        }

        public bool AllowCamera
        {
            get
            {
                return allowCamera;
            }
        }

        public bool AllowConsumerEmail
        {
            get
            {
                return allowConsumerEmail;
            }
        }

        public bool AllowDesktopSync
        {
            get
            {
                return allowDesktopSync;
            }
        }

        public bool AllowHTMLEmail
        {
            get
            {
                return allowHTMLEmail;
            }
        }

        public bool AllowInternetSharing
        {
            get
            {
                return allowInternetSharing;
            }
        }

        public bool AllowIrDA
        {
            get
            {
                return allowIrDA;
            }
        }

        public bool AllowPOPIMAPEmail
        {
            get
            {
                return allowPOPIMAPEmail;
            }
        }

        public bool AllowRemoteDesktop
        {
            get
            {
                return allowRemoteDesktop;
            }
        }

        public bool AllowSimpleDevicePassword
        {
            get
            {
                return allowSimpleDevicePassword;
            }
        }

        public Int32 AllowSMIMEEncryptionAlgorithmNegotiation
        {
            get
            {
                return allowSMIMEEncryptionAlgorithmNegotiation;
            }
        }

        public bool AllowSMIMESoftCerts
        {
            get
            {
                return allowSMIMESoftCerts;
            }
        }

        public bool AllowStorageCard
        {
            get
            {
                return allowStorageCard;
            }
        }

        public bool AllowTextMessaging
        {
            get
            {
                return allowTextMessaging;
            }
        }

        public bool AllowUnsignedApplications
        {
            get
            {
                return allowUnsignedApplications;
            }
        }

        public bool AllowUnsignedInstallationPackages
        {
            get
            {
                return allowUnsignedInstallationPackages;
            }
        }

        public bool AllowWifi
        {
            get
            {
                return allowWifi;
            }
        }

        public bool AlphanumericDevicePasswordRequired
        {
            get
            {
                return alphanumericDevicePasswordRequired;
            }
        }

        public bool AttachmentsEnabled
        {
            get
            {
                return attachmentsEnabled;
            }
        }

        public bool DevicePasswordEnabled
        {
            get
            {
                return devicePasswordEnabled;
            }
        }

        public UInt32 DevicePasswordExpiration
        {
            get
            {
                return devicePasswordExpiration;
            }
        }

        public UInt32 DevicePasswordHistory
        {
            get
            {
                return devicePasswordHistory;
            }
        }

        public UInt32 MaxAttachmentSize
        {
            get
            {
                return maxAttachmentSize;
            }
        }

        public UInt32 MaxCalendarAgeFilter
        {
            get
            {
                return maxCalendarAgeFilter;
            }
        }

        public UInt32 MaxDevicePasswordFailedAttempts
        {
            get
            {
                return maxDevicePasswordFailedAttempts;
            }
        }

        public UInt32 MaxEmailAgeFilter
        {
            get
            {
                return maxEmailAgeFilter;
            }
        }

        public Int32 MaxEmailBodyTruncationSize
        {
            get
            {
                return maxEmailBodyTruncationSize;
            }
        }

        public Int32 MaxEmailHTMLBodyTruncationSize
        {
            get
            {
                return maxEmailHTMLBodyTruncationSize;
            }
        }

        public UInt32 MaxInactivityTimeDeviceLock
        {
            get
            {
                return maxInactivityTimeDeviceLock;
            }
        }

        public byte MinDevicePasswordComplexCharacters
        {
            get
            {
                return minDevicePasswordComplexCharacters;
            }
        }

        public byte MinDevicePasswordLength
        {
            get
            {
                return minDevicePasswordLength;
            }
        }

        public bool PasswordRecoveryEnabled
        {
            get
            {
                return passwordRecoveryEnabled;
            }
        }

        public bool RequireDeviceEncryption
        {
            get
            {
                return requireDeviceEncryption;
            }
        }

        public bool RequireEncryptedSMIMEMessages
        {
            get
            {
                return requireEncryptedSMIMEMessages;
            }
        }

        public Int32 RequireEncryptionSMIMEAlgorithm
        {
            get
            {
                return requireEncryptionSMIMEAlgorithm;
            }
        }

        public bool RequireManualSyncWhenRoaming
        {
            get
            {
                return requireManualSyncWhenRoaming;
            }
        }

        public Int32 RequireSignedSMIMEAlgorithm
        {
            get
            {
                return requireSignedSMIMEAlgorithm;
            }
        }

        public bool RequireSignedSMIMEMessages
        {
            get
            {
                return requireSignedSMIMEMessages;
            }
        }

        public bool RequireStorageCardEncryption
        {
            get
            {
                return requireStorageCardEncryption;
            }
        }

        public string[] ApprovedApplicationList
        {
            get
            {
                return approvedApplicationList;
            }
        }

        public string[] UnapprovedInROMApplicationList
        {
            get
            {
                return unapprovedInROMApplicationList;
            }
        }

        public bool RemoteWipeRequested
        {
            get
            {
                return remoteWipeRequested;
            }
        }

        public bool HasPolicyInfo
        {
            get
            {
                return hasPolicyInfo;
            }
        }

        #endregion 
        
        // This function parses a Provision command
        // response (as specified in MS-ASPROV section 2.2)
        // and extracts the policy information.
        public bool LoadXML(string policyXml)
        {
            /*
 <?xml version="1.0" encoding="utf-8" ?> 
- <Provision xmlns="Provision" xmlns:settings="Settings">
- <settings:DeviceInformation>
  <settings:Status>1</settings:Status> 
  </settings:DeviceInformation>
  <Status>1</Status> 
- <Policies>
- <Policy>
  <PolicyType>MS-EAS-Provisioning-WBXML</PolicyType> 
  <Status>1</Status> 
  <PolicyKey>1081347313</PolicyKey> 
- <Data>
- <EASProvisionDoc>
  <DevicePasswordEnabled>0</DevicePasswordEnabled> 
  <AlphanumericDevicePasswordRequired>0</AlphanumericDevicePasswordRequired> 
  <PasswordRecoveryEnabled>0</PasswordRecoveryEnabled> 
  <RequireStorageCardEncryption>0</RequireStorageCardEncryption> 
  <AttachmentsEnabled>1</AttachmentsEnabled> 
  <MinDevicePasswordLength>4</MinDevicePasswordLength> 
  <MaxInactivityTimeDeviceLock>900</MaxInactivityTimeDeviceLock> 
  <MaxDevicePasswordFailedAttempts>8</MaxDevicePasswordFailedAttempts> 
  <MaxAttachmentSize /> 
  <AllowSimpleDevicePassword>1</AllowSimpleDevicePassword> 
  <DevicePasswordExpiration /> 
  <DevicePasswordHistory>0</DevicePasswordHistory> 
  <AllowStorageCard>1</AllowStorageCard> 
  <AllowCamera>1</AllowCamera> 
  <RequireDeviceEncryption>0</RequireDeviceEncryption> 
  <AllowUnsignedApplications>1</AllowUnsignedApplications> 
  <AllowUnsignedInstallationPackages>1</AllowUnsignedInstallationPackages> 
  <MinDevicePasswordComplexCharacters>3</MinDevicePasswordComplexCharacters> 
  <AllowWiFi>1</AllowWiFi> 
  <AllowTextMessaging>1</AllowTextMessaging> 
  <AllowPOPIMAPEmail>1</AllowPOPIMAPEmail> 
  <AllowBluetooth>2</AllowBluetooth> 
  <AllowIrDA>1</AllowIrDA> 
  <RequireManualSyncWhenRoaming>0</RequireManualSyncWhenRoaming> 
  <AllowDesktopSync>1</AllowDesktopSync> 
  <MaxCalendarAgeFilter>0</MaxCalendarAgeFilter> 
  <AllowHTMLEmail>1</AllowHTMLEmail> 
  <MaxEmailAgeFilter>0</MaxEmailAgeFilter> 
  <MaxEmailBodyTruncationSize>-1</MaxEmailBodyTruncationSize> 
  <MaxEmailHTMLBodyTruncationSize>-1</MaxEmailHTMLBodyTruncationSize> 
  <RequireSignedSMIMEMessages>0</RequireSignedSMIMEMessages> 
  <RequireEncryptedSMIMEMessages>0</RequireEncryptedSMIMEMessages> 
  <RequireSignedSMIMEAlgorithm>0</RequireSignedSMIMEAlgorithm> 
  <RequireEncryptionSMIMEAlgorithm>0</RequireEncryptionSMIMEAlgorithm> 
  <AllowSMIMEEncryptionAlgorithmNegotiation>2</AllowSMIMEEncryptionAlgorithmNegotiation> 
  <AllowSMIMESoftCerts>1</AllowSMIMESoftCerts> 
  <AllowBrowser>1</AllowBrowser> 
  <AllowConsumerEmail>1</AllowConsumerEmail> 
  <AllowRemoteDesktop>1</AllowRemoteDesktop> 
  <AllowInternetSharing>1</AllowInternetSharing> 
  <UnapprovedInROMApplicationList /> 
  <ApprovedApplicationList /> 
  </EASProvisionDoc>
  </Data>
  </Policy>
  </Policies>
  </Provision>
             */
            XDocument xmlDoc = null; // new XDocument();

            try
            {
                TextReader tr = new StringReader(policyXml);
                xmlDoc = XDocument.Load(tr);
                var reader = xmlDoc.CreateReader();
                
                // Element.Name.LocalName is the text after the colon in Namespace: e.g. ns_id:LocalName
                string ns = "{Provision}";
                
                var remoteWipeQ = xmlDoc.Descendants().SingleOrDefault(p => p.Name == (ns + "RemoteWipe"));
                if (remoteWipeQ != null)
                {
                    remoteWipeRequested = true;
                    return true;
                }
                
                var policyTypeQ = xmlDoc.Descendants().SingleOrDefault(p => p.Name == (ns + "PolicyType"));
                if (policyTypeQ != null)
                {
                    if ((policyTypeQ as XElement).Value == "MS-EAS-Provisioning-WBXML")
                    {
                        var policyStatusQ = xmlDoc.Descendants().First(p => p.Name == (ns + "Status"));
                        if (null != policyStatusQ)
                        {
                            status = XmlConvert.ToInt32(policyStatusQ.Value);
                        }
                        var policyKeyQ = xmlDoc.Descendants().First(p => p.Name == ns + "PolicyKey");
                        if (null != policyKeyQ)
                        {
                            policyKey = XmlConvert.ToUInt32(policyKeyQ.Value);
                        }

                        XElement policyValsQ = xmlDoc.Descendants().FirstOrDefault(p => p.Name == (ns + "EASProvisionDoc"));
                        if(null != policyValsQ)
                        {
                            foreach (XElement pVal in policyValsQ.Elements())
                            {
                                XElement j = pVal as XElement;
                                if (null != j)
                                {
                                    // Loop through the child nodes and
                                    // set the corresponding property.
                                    switch (j.Name.LocalName)
                                    {
                                        case ("AllowBluetooth"):
                                            allowBlueTooth = XmlConvert.ToByte(j.Value);
                                            break;
                                        case ("AllowBrowser"):
                                            allowBrowser = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowCamera"):
                                            allowCamera = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowConsumerEmail"):
                                            allowConsumerEmail = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowDesktopSync"):
                                            allowDesktopSync = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowHTMLEmail"):
                                            allowHTMLEmail = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowInternetSharing"):
                                            allowInternetSharing = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowIrDA"):
                                            allowIrDA = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowPOPIMAPEmail"):
                                            allowPOPIMAPEmail = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowRemoteDesktop"):
                                            allowRemoteDesktop = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowSimpleDevicePassword"):
                                            allowSimpleDevicePassword = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowSMIMEEncryptionAlgorithmNegotiation"):
                                            if (j.Value != "")
                                                allowSMIMEEncryptionAlgorithmNegotiation = XmlConvert.ToInt32(j.Value);
                                            break;
                                        case ("AllowSMIMESoftCerts"):
                                            if (j.Value != "")
                                                allowSMIMESoftCerts = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowStorageCard"):
                                            if (j.Value != "")
                                                allowStorageCard = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowTextMessaging"):
                                            if (j.Value != "")
                                                allowTextMessaging = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowUnsignedApplications"):
                                            if (j.Value != "")
                                                allowUnsignedApplications = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowUnsignedInstallationPackages"):
                                            if (j.Value != "")
                                                allowUnsignedInstallationPackages = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AllowWiFi"):
                                            if (j.Value != "")
                                                allowWifi = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("AlphanumericDevicePasswordRequired"):
                                            if (j.Value != "")
                                                alphanumericDevicePasswordRequired = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("ApprovedApplicationList"):
                                            if (j.Value != "")
                                            {
                                                // TODO Later approvedApplicationList = ParseAppList(policySettingNode);
                                            }
                                            break;
                                        case ("AttachmentsEnabled"):
                                            if (j.Value != "")
                                                attachmentsEnabled = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("DevicePasswordEnabled"):
                                            if (j.Value != "")
                                                devicePasswordEnabled = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("DevicePasswordExpiration"):
                                            if (j.Value != "")
                                                devicePasswordExpiration = XmlConvert.ToUInt32(j.Value);
                                            break;
                                        case ("DevicePasswordHistory"):
                                            if (j.Value != "")
                                                devicePasswordHistory = XmlConvert.ToUInt32(j.Value);
                                            break;
                                        case ("MaxAttachmentSize"):
                                            if (j.Value != "")
                                                maxAttachmentSize = XmlConvert.ToUInt32(j.Value);
                                            break;
                                        case ("MaxCalendarAgeFilter"):
                                            if (j.Value != "")
                                                maxCalendarAgeFilter = XmlConvert.ToUInt32(j.Value);
                                            break;
                                        case ("MaxDevicePasswordFailedAttempts"):
                                            if (j.Value != "")
                                                maxDevicePasswordFailedAttempts = XmlConvert.ToUInt32(j.Value);
                                            break;
                                        case ("MaxEmailAgeFilter"):
                                            if (j.Value != "")
                                                maxEmailAgeFilter = XmlConvert.ToUInt32(j.Value);
                                            break;
                                        case ("MaxEmailBodyTruncationSize"):
                                            if (j.Value != "")
                                                maxEmailBodyTruncationSize = XmlConvert.ToInt32(j.Value);
                                            break;
                                        case ("MaxEmailHTMLBodyTruncationSize"):
                                            if (j.Value != "")
                                                maxEmailHTMLBodyTruncationSize = XmlConvert.ToInt32(j.Value);
                                            break;
                                        case ("MaxInactivityTimeDeviceLock"):
                                            if (j.Value != "")
                                                maxInactivityTimeDeviceLock = XmlConvert.ToUInt32(j.Value);
                                            break;
                                        case ("MinDevicePasswordComplexCharacters"):
                                            if (j.Value != "")
                                                minDevicePasswordComplexCharacters = XmlConvert.ToByte(j.Value);
                                            break;
                                        case ("MinDevicePasswordLength"):
                                            if (j.Value != "")
                                                minDevicePasswordLength = XmlConvert.ToByte(j.Value);
                                            break;
                                        case ("PasswordRecoveryEnabled"):
                                            if (j.Value != "")
                                                passwordRecoveryEnabled = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("RequireDeviceEncryption"):
                                            if (j.Value != "")
                                                requireDeviceEncryption = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("RequireEncryptedSMIMEMessages"):
                                            if (j.Value != "")
                                                requireEncryptedSMIMEMessages = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("RequireEncryptionSMIMEAlgorithm"):
                                            if (j.Value != "")
                                                requireEncryptionSMIMEAlgorithm = XmlConvert.ToInt32(j.Value);
                                            break;
                                        case ("RequireManualSyncWhenRoaming"):
                                            if (j.Value != "")
                                                requireManualSyncWhenRoaming = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("RequireSignedSMIMEAlgorithm"):
                                            if (j.Value != "")
                                                requireSignedSMIMEAlgorithm = XmlConvert.ToInt32(j.Value);
                                            break;
                                        case ("RequireSignedSMIMEMessages"):
                                            if (j.Value != "")
                                                requireSignedSMIMEMessages = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("RequireStorageCardEncryption"):
                                            if (j.Value != "")
                                                requireStorageCardEncryption = XmlConvert.ToBoolean(j.Value);
                                            break;
                                        case ("UnapprovedInROMApplicationList"):
                                            if (j.Value != "")
                                            {
                                                // TODO Later: unapprovedInROMApplicationList = ParseAppList(policySettingNode);
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch(System.InvalidOperationException)
            {
                // Coulnd't find the EASProvisionDoc which is okay for the second response
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // This function parses the contents of the 
        // ApprovedApplicationList and the UnapprovedInROMApplicationList
        // nodes.
        private string[] ParseAppList(XElement appListNode)
        {
            List<string> appList = new List<string>();

            foreach (XElement appNode in appListNode.Descendants())
            {
                appList.Add(appNode.Value);
            }

            return appList.ToArray();
        }
    }
}
