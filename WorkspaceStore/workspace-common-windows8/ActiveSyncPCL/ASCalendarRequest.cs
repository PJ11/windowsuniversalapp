using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ActiveSyncPCL
{
    /* This class represents Calendar Class Protocol enables the communication of calendar data between a mobile device and the server in the ActiveSync protocol.
     request specified @http://msdn.microsoft.com/en-us/library/dn338917(v=exchg.80).aspx */

    public class ASCalendarRequest : ASCommandRequest
    {
        private string syncKey = "0";
        private string CollectionID = "1";
        public ASCalendarRequest()
        {
            Command = "Calendar";
            
        }

        public string EASServerUrl { get; set; }

        // This function generates an ASCalendarSyncResponse from an
        // HTTP response.
        protected override ASCommandResponse WrapHttpResponse(HttpWebResponse httpResp)
        {
            return null;// new ASCalendarResponse(httpResp);
        }

        /* <?xml version="1.0" encoding="utf-8"?>
     <Sync xmlns="AirSync:">
       <Collections>
         <Collection>
           <SyncKey>850479756</SyncKey>
           <CollectionId>1</CollectionId>
           <DeletesAsMoves/>
           <GetChanges/>
         </Collection>
       </Collections>
     </Sync>*/

        // This function generates the XML request body
        // for the CalendarSync request.
        protected override void GenerateXMLPayload()
        {
            // If WBXML was explicitly set, use that
            if (WbxmlBytes != null)
                return;

            if (syncKey == "")
                syncKey = "0";

            // Otherwise, use the properties to build the XML and then WBXML encode it
                XDocument syncXML = new XDocument(new XDeclaration("1.0", "utf-8", null));
                XNamespace Sync = Namespaces.airSyncNamespace;
                XElement syncNode = new XElement("Sync", new XAttribute(XNamespace.Xmlns + Xmlns.airSyncXmlns, Namespaces.airSyncNamespace));
                       
                try
                {
                    syncXML.Add(syncNode);
                    XElement collectionsNode = new XElement(/*Sync +*/"Collections");
                    syncNode.Add(collectionsNode);
                    XElement collectionNode = new XElement( /*Sync +*/"Collection");
                    collectionsNode.Add(collectionNode);
                    XElement syncKeyNode = new XElement( /*Sync +*/"SyncKey", new XText(syncKey.ToString()));
                    collectionNode.Add(syncKeyNode);
                    XElement collectionIdNode = new XElement(/*Sync +*/"CollectionId", new XText(CollectionID.ToString()));
                    collectionNode.Add(collectionIdNode);
                    XElement deletesAsMovesNode = new XElement( /*Sync +*/"DeletesAsMoves", new XText("0"));
                    collectionNode.Add(deletesAsMovesNode);
                    XElement getChangesNode = new XElement(/*Sync +*/"GetChanges", new XText("0"));
                    collectionNode.Add(getChangesNode);

                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Async = false;
                    settings.Indent = true;
                    settings.IndentChars = "  ";
                    settings.Encoding = Encoding.UTF8;
                    Utf8StringWriter sw = new Utf8StringWriter();
                    XmlWriter writer = XmlWriter.Create(sw, settings);
                    syncXML.WriteTo(writer);
                    writer.Flush();
                    XmlString = sw.ToString();
                }
                catch (Exception e)
                {
                    string msg = e.Message;
                }      


#if OLDWAY
            // Otherwise, use the properties to build the XML and then WBXML encode it
            XDocument airSyncXML = new XDocument();

            XmlDeclaration xmlDeclaration = airSyncXML.CreateXmlDeclaration("1.0", "utf-8", null);
            airSyncXML.InsertBefore(xmlDeclaration, null);

            XNode airSyncNode = airSyncXML.CreateElement(Xmlns.airSyncXmlns, "airSync", Namespaces.airSyncNamespace);
            airSyncNode.Prefix = Xmlns.airSyncXmlns;
            airSyncXML.AppendChild(airSyncNode);          
                    

            StringWriter stringWriter = new StringWriter();
            XmlTextWriter xmlWriter = new XmlTextWriter(stringWriter);
            xmlWriter.Formatting = Formatting.Indented;
            airSyncXML.WriteTo(xmlWriter);
            xmlWriter.Flush();
            XmlString = stringWriter.ToString();
#endif
        }
    }
}