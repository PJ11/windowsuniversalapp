﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmLight1.Helpers;
using FileHandlingPCL;

namespace MvvmLight1.ViewModel
{
   
    /// <summary>
    /// Fetches the downloaded attachment and encrypts it.
    /// </summary>
    class EncryptDownloadedFile
    {
       private byte[] byteFile = null;
    //  ActiveSync active = new ActiveSync();       
       FileManager file = new FileManager();
       string encryptionKey = ""; 

       /// <summary>
       /// Downloads the and encrypt.
       /// </summary>
       /// <param name="fileReference">The file reference.</param>
       /// <param name="fileName">Name of the file.</param>
       /// <param name="filePath">The file path.</param>
       public async void DownloadAndEncrypt(String fileReference, string fileName, string filePath, ActiveSync active)
       {
           //The fileReference, fileName and filePath has to be retrived from UI/AppInfrastructure.
           //When the attachment is downloaded, fetch the fileName and user selected path and pass it to this Method.
           try
           {
               byteFile = await active.DownloadAttachment(fileReference);
           }
        catch(Exception e)
           {
            Console.WriteLine("Could not download the attachment", e.Message);
           }
           try
           {
             await  file.CreateFile(fileName,filePath, byteFile, encryptionKey);
           }
          catch(Exception e)
           {
               Console.WriteLine("Error in storing the File Downloaded", e.Message);
           }
       }


    }
}
