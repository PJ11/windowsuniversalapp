﻿using AbstractsPCL;
using CommonPCL;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using FileHandlingPCL;

namespace Common
{
    public class DKCNAppManager
    {
        protected static DKCNAppManager _instance = null;

        public DKCNSettings settingsUserOptions = null;
        public DKCNSettings settingsOptions = null;
        public DKCNSettings settingsPendingOptions = null;
        
        FileHandlingPCL.EncryptionEngine encryptionEng;
        protected FileManager fileMan = new FileManager();
        protected Dictionary<string, string> mSettings = new Dictionary<string, string>();
        protected string mTempKey256 = null;
        
        public static DKCNAppManager Instance()
        {
            if(null == _instance)
            {
                _instance = new DKCNAppManager();
            }
            return _instance;
        }
        
        public DKCNAppManager()
        {
            // Make default constructor private.
        }

        public void parsePolicyToDictionary(String policy)
        {
            try
            {
                var obj = JArray.Parse(policy);

                mSettings.Add(HaloPolicySettingsAbstract.KEY_INACTIVITY_TIMEOUT, obj[0]["items"][HaloPolicySettingsAbstract.KEY_INACTIVITY_TIMEOUT].ToObject<int>().ToString());
                mSettings.Add(HaloPolicySettingsAbstract.CCM_INACTIVITY_TIMEOUT, obj[0]["items"][HaloPolicySettingsAbstract.KEY_INACTIVITY_TIMEOUT].ToObject<int>().ToString());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_PIN_LENGTH, obj[0]["items"][HaloPolicySettingsAbstract.KEY_PIN_LENGTH].ToObject<int>().ToString());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_COPY_PASTE, obj[0]["items"][HaloPolicySettingsAbstract.KEY_ENABLE_COPY_PASTE].ToObject<bool>().ToString());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_EXTERNAL_SHARING, obj[0]["items"][HaloPolicySettingsAbstract.KEY_ENABLE_EXTERNAL_SHARING].ToObject<bool>().ToString());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_ALLOW_JAILBREAK, obj[0]["items"][HaloPolicySettingsAbstract.KEY_ALLOW_JAILBREAK].ToObject<bool>().ToString());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION, obj[0]["items"][HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION].ToObject<int>().ToString());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE, obj[0]["items"][HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE].ToObject<int>().ToString());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION, obj[0]["items"][HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION].ToObject<int>().ToString());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION_ACTION, obj[0]["items"][HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION_ACTION].ToObject<int>().ToString());

                mSettings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_CALENDAR, obj[1]["items"][HaloPolicySettingsAbstract.KEY_ENABLE_CALENDAR].ToObject<bool>().ToString());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_EMAIL, obj[1]["items"][HaloPolicySettingsAbstract.KEY_ENABLE_EMAIL].ToObject<bool>().ToString());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_EMAIL_SYNC_FREQUENCY, obj[1]["items"][HaloPolicySettingsAbstract.KEY_EMAIL_SYNC_FREQUENCY].ToObject<int>().ToString());
                bool webBrowserProxyRequired = obj[1]["items"][HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_REQUIRED].ToObject<bool>();
                mSettings.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_REQUIRED, webBrowserProxyRequired.ToString());
                if (webBrowserProxyRequired)
                {
                    mSettings.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_CERTIFICATE, obj[1]["items"][HaloPolicySettingsAbstract.KEY_WEBBROWSER_CERTIFICATE].ToObject<string>());
                    mSettings.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_IP, obj[1]["items"][HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_IP].ToObject<string>());
                    mSettings.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_PORT, obj[1]["items"][HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_PORT].ToObject<int>().ToString());
                }

                mSettings.Add(HaloPolicySettingsAbstract.KEY_EMAIL_SIGNATURE, obj[1]["items"][HaloPolicySettingsAbstract.KEY_EMAIL_SIGNATURE].ToObject<string>());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_DEFAULT_HOMEPAGE, obj[1]["items"][HaloPolicySettingsAbstract.KEY_DEFAULT_HOMEPAGE].ToObject<string>());
                // TODO: Get Bookmarks in there
                mSettings.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_BOOKMARK, obj[1]["items"][HaloPolicySettingsAbstract.KEY_WEBBROWSER_BOOKMARK].ToObject<string>());

                mSettings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_CONTACTS, obj[1]["items"][HaloPolicySettingsAbstract.KEY_ENABLE_CONTACTS].ToObject<bool>().ToString());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_ENABLE_FILEBROWSER, obj[1]["items"][HaloPolicySettingsAbstract.KEY_ENABLE_FILEBROWSER].ToObject<bool>().ToString());

                mSettings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_HOST, obj[2]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_HOST].ToObject<string>());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_EMAILADDRESS, obj[2]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_EMAILADDRESS].ToObject<string>());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_USERNAME, obj[2]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_USERNAME].ToObject<string>());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DOMAIN, obj[2]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DOMAIN].ToObject<string>());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_SSLREQUIRED, obj[2]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_SSLREQUIRED].ToObject<bool>().ToString());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_AGENTIDENTIFIER, obj[2]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_AGENTIDENTIFIER].ToObject<string>());
                bool dynamic = obj[2]["items"][HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DYNAMICUSERINFO].ToObject<bool>();
                mSettings.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DYNAMICUSERINFO, dynamic.ToString());
                if (dynamic)
                {

                }
                mSettings.Add(HaloPolicySettingsAbstract.KEY_USER_FIRST_NAME, obj[3]["personInfoLean"][HaloPolicySettingsAbstract.KEY_USER_FIRST_NAME].ToObject<string>());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_USER_LAST_NAME, obj[3]["personInfoLean"][HaloPolicySettingsAbstract.KEY_USER_LAST_NAME].ToObject<string>());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_USER_PHONE, obj[3]["personInfoLean"][HaloPolicySettingsAbstract.KEY_USER_PHONE].ToObject<string>());
                mSettings.Add(HaloPolicySettingsAbstract.KEY_USER_TITLE, obj[3]["personInfoLean"][HaloPolicySettingsAbstract.KEY_USER_TITLE].ToObject<string>());
            }
            catch (Exception e)
            {
                string msg = e.Message;
                throw new MalformedPolicyException(msg);
            }
        }

        public Dictionary<string, string> getChanges(DKCNSettings newSettings)
        {
            Dictionary<string, string> changes = new Dictionary<string, string>();

            #region Adding Change

            if (settingsUserOptions.InactivityTimeout != newSettings.InactivityTimeout)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_INACTIVITY_TIMEOUT, newSettings.InactivityTimeout.ToString());
               
            }
            if (settingsUserOptions.CCMInactivityTimeout != newSettings.CCMInactivityTimeout)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_INACTIVITY_TIMEOUT, newSettings.CCMInactivityTimeout.ToString());

            }
     
            if (settingsUserOptions.PinLength != newSettings.PinLength)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_PIN_LENGTH, newSettings.PinLength.ToString());
            }

            if (settingsUserOptions.CopyPasteEnabled != newSettings.CopyPasteEnabled)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_ENABLE_COPY_PASTE, newSettings.CopyPasteEnabled.ToString());
            }

            if (settingsUserOptions.ExternalSharingEnabled != newSettings.ExternalSharingEnabled)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_ENABLE_EXTERNAL_SHARING, newSettings.ExternalSharingEnabled.ToString());
            }

            if (settingsUserOptions.JailbrokenDeviceAllowed != newSettings.JailbrokenDeviceAllowed)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_ALLOW_JAILBREAK, newSettings.JailbrokenDeviceAllowed.ToString());
            }

            if (settingsUserOptions.LoginFailureAction != newSettings.LoginFailureAction)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION, newSettings.LoginFailureAction.ToString());
            }

            if (settingsUserOptions.LoginAttemptsAllowed != newSettings.LoginAttemptsAllowed)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE, newSettings.LoginAttemptsAllowed.ToString());
            }

            //if (settingsUserOptions.ChecinExpiration != newSettings.ChecinExpiration)
            //{
            //    changes.Add(HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION, newSettings.ChecinExpiration.ToString());
            //}

            //if (settingsUserOptions.CheckinExpirationAction != newSettings.CheckinExpirationAction)
            //{
            //    changes.Add(HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION_ACTION, newSettings.CheckinExpirationAction.ToString());
            //}

            if (settingsUserOptions.CalendarAppGranted != newSettings.CalendarAppGranted)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_ENABLE_CALENDAR, newSettings.CalendarAppGranted.ToString());
            }

            if (settingsUserOptions.EmailAppGranted != newSettings.EmailAppGranted)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_ENABLE_EMAIL, newSettings.EmailAppGranted.ToString());
            }

            if (settingsUserOptions.MailFetchFrequency != newSettings.MailFetchFrequency)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_EMAIL_SYNC_FREQUENCY, newSettings.MailFetchFrequency.ToString());
            }
            if (settingsUserOptions.BookmarkString != newSettings.BookmarkString)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_BOOKMARK, newSettings.BookmarkString.ToString());
            }
            if (settingsUserOptions.WebProxyRequired != newSettings.WebProxyRequired)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_REQUIRED, newSettings.WebProxyRequired.ToString());
            }

            if (settingsUserOptions.CertData != newSettings.CertData)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_CERTIFICATE, newSettings.CertData.ToString());
            }

            if (settingsUserOptions.WebProxyIP != newSettings.WebProxyIP)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_IP, newSettings.WebProxyIP.ToString());
            }

            if (settingsUserOptions.WebProxyPort != newSettings.WebProxyPort)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_PORT, newSettings.WebProxyPort.ToString());
            }

            if (settingsUserOptions.EmailSignature != newSettings.EmailSignature)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_EMAIL_SIGNATURE, newSettings.EmailSignature.ToString());
            }

            //if (settingsUserOptions.DefaultHomePage != newSettings.DefaultHomePage)
            //{
            //    changes.Add(HaloPolicySettingsAbstract.KEY_DEFAULT_HOMEPAGE, newSettings.DefaultHomePage.ToString());
            //}

            if (settingsUserOptions.ContactsAppGranted != newSettings.ContactsAppGranted)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_ENABLE_CONTACTS, newSettings.ContactsAppGranted.ToString());
            }

            if (settingsUserOptions.FileAppGranted != newSettings.FileAppGranted)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_ENABLE_FILEBROWSER, newSettings.FileAppGranted.ToString());
            }

            if (settingsUserOptions.EasHost != newSettings.EasHost)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_HOST, newSettings.EasHost.ToString());
            }

            if (settingsUserOptions.EasEmailAddress != newSettings.EasEmailAddress)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_EMAILADDRESS, newSettings.EasEmailAddress.ToString());
            }

            if (settingsUserOptions.EasUserName != newSettings.EasUserName)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_USERNAME, newSettings.EasUserName.ToString());
            }

            if (settingsUserOptions.EasDomain != newSettings.EasDomain)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DOMAIN, newSettings.EasDomain.ToString());
            }

            if (settingsUserOptions.UseSSL != newSettings.UseSSL)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_SSLREQUIRED, newSettings.UseSSL.ToString());
            }

            if (settingsUserOptions.DynamicUserInfo != newSettings.DynamicUserInfo)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DYNAMICUSERINFO, newSettings.DynamicUserInfo.ToString());
            }


            if (settingsUserOptions.UserFirstName != newSettings.UserFirstName)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_USER_FIRST_NAME, newSettings.UserFirstName.ToString());
            }


            if (settingsUserOptions.UserLastName != newSettings.UserLastName)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_USER_LAST_NAME, newSettings.UserLastName.ToString());
            }

            if (settingsUserOptions.UserPhoneNumber != newSettings.UserPhoneNumber)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_USER_PHONE, newSettings.UserPhoneNumber.ToString());
            }
            if (settingsUserOptions.UserTitle != newSettings.UserTitle)
            {
                changes.Add(HaloPolicySettingsAbstract.KEY_USER_TITLE, newSettings.UserTitle.ToString());
            }


            #endregion

            return changes;
        }

        /// <summary>
        /// Store the user settings (Must also encrypt)
        /// </summary>
        public async void storeUserSettings()
        {

            DataContractSerializer serializer = new DataContractSerializer(typeof(DKCNSettings));
            // I think we can probably serialize the settingsUserOptions and then encrypt
            if (null != settingsUserOptions)
            {
                // KEY_PIN_LENGTH, ACTION_TIMEOUT, FAILED_ATTEMPTS cannot be encrypted because we need then immediately
                if (!ApplicationData.Current.LocalSettings.Values.ContainsKey(HaloPolicySettingsAbstract.KEY_PIN_LENGTH))
                {
                    ApplicationData.Current.LocalSettings.Values.Add(HaloPolicySettingsAbstract.KEY_PIN_LENGTH, settingsUserOptions.PinLength);  // save key length in isolated storage as needed non encrypted
                }

                if (ApplicationData.Current.LocalSettings.Values.ContainsKey(HaloPolicySettingsAbstract.KEY_PIN_LENGTH))
                {
                    if ((int)ApplicationData.Current.LocalSettings.Values[HaloPolicySettingsAbstract.KEY_PIN_LENGTH] != settingsUserOptions.PinLength)
                    {
                        ApplicationData.Current.LocalSettings.Values[HaloPolicySettingsAbstract.KEY_PIN_LENGTH] = settingsUserOptions.PinLength;  // save key length in isolated storage as needed non encrypted
                    }
                }

                if (!ApplicationData.Current.LocalSettings.Values.ContainsKey(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION))
                {
                    ApplicationData.Current.LocalSettings.Values.Add(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION, (int)settingsUserOptions.LoginFailureAction);  // save key length in isolated storage as needed non encrypted
                }

                if (ApplicationData.Current.LocalSettings.Values.ContainsKey(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION))
                {
                    if ((int)ApplicationData.Current.LocalSettings.Values[HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION] != (int)settingsUserOptions.LoginFailureAction)
                    {
                        ApplicationData.Current.LocalSettings.Values[HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION] = (int)settingsUserOptions.LoginFailureAction;  // save key length in isolated storage as needed non encrypted
                    }
                }

                if (!ApplicationData.Current.LocalSettings.Values.ContainsKey(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE))
                {
                    ApplicationData.Current.LocalSettings.Values.Add(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE, (int)settingsUserOptions.LoginAttemptsAllowed);  // save key length in isolated storage as needed non encrypted
                }

                if (ApplicationData.Current.LocalSettings.Values.ContainsKey(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE))
                {
                    if ((int)ApplicationData.Current.LocalSettings.Values[HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE] != (int)settingsUserOptions.LoginAttemptsAllowed)
                    {
                        ApplicationData.Current.LocalSettings.Values[HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE] = settingsUserOptions.LoginAttemptsAllowed;  // save key length in isolated storage as needed non encrypted
                    }
                }
                // Serialize the settingsUserOptions into a byte array and then encrypt  and then store
                byte[] byteArr = null;

                //seralize DKCNSettings object 
                using (var ms = new System.IO.MemoryStream())
                {
                    serializer.WriteObject(ms, settingsUserOptions);
                    byteArr = ms.ToArray();
                }

                //encrypt and store using APi in FileHandlingPLC
                if (byteArr != null)
                {
                    if(null == mTempKey256)
                    {
                        mTempKey256 = "Dell1234Dell1234"; // Encoding.UTF8.GetString(ConstantData.DBKey, 0, 32);
                    }
                    //encrybyteArr= encryptionEng.EncryptByteArray(byteArr, "");
                    bool result = await fileMan.CreateFile("ccm_settings", "", byteArr, mTempKey256);
                    int j;
                    j = 10;
                }

            }

        }

        public bool loadUnencryptedUserSettings(ref int pinLength, ref int loginFailureAction, ref int loginAttemptsAllowed )
        {
            bool success = true;

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey(HaloPolicySettingsAbstract.KEY_PIN_LENGTH))
            {
                pinLength = (int)ApplicationData.Current.LocalSettings.Values[HaloPolicySettingsAbstract.KEY_PIN_LENGTH];
            }
            else
                success = false;

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION))
            {
                loginFailureAction = (int)ApplicationData.Current.LocalSettings.Values[HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION];
            }
            else
            {
                success = false;
            }

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE))
            {
                loginAttemptsAllowed = (int)ApplicationData.Current.LocalSettings.Values[HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE];
            }
            else
            {
                success = false;
            }
            return success;
        }

        /// <summary>
        /// Load login attempts allowed settings
        /// </summary>
        /// <returns>Boolean</returns>
        public bool loadLoginAttemptsAllowedSettings(ref int loginAttemptsAllowed)
        {
            bool success = true;
#if WP_80_SILVERLIGHT
            if (phoneSetting.Contains(HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE))
            {
                loginAttemptsAllowed = (int)phoneSetting[HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE];
            }
            else
            {
                success = false;
            }
#endif
            return success;
        }

        public async Task<bool> loadUserSettings()
        {
            byte[] arr = null;
            if(null == fileMan)
            {
                return false;
            }
            try
            {
                if (null == mTempKey256)
                {
                    mTempKey256 = "Dell1234Dell1234"; // Encoding.UTF8.GetString(ConstantData.DBKey, 0, 32);
                }
                arr = await fileMan.ReadFile("ccm_settings", "", mTempKey256);
                if(null != arr)
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(DKCNSettings));
                    using (var ms = new System.IO.MemoryStream(arr))
                    {
                        settingsUserOptions = (DKCNSettings)serializer.ReadObject(ms);
                    }
                    return true;
                }
                return false;
            }
            catch(Exception e)
            {
                LoggingWP8.WP8Logger.LogMessage("Error in reading SettingsUserOptions", e.Message);
                // I assume the only exception is file does not exist, so return false
                return false;
            }
        }

        public bool CalendarAppGranted
        {
            get
            {
                return settingsUserOptions.CalendarAppGranted;
            }

        }
        public bool MailAppGranted
        {
            get
            {
                return settingsUserOptions.EmailAppGranted;
            }

        }
        public bool ContactsAppGranted
        {
            get
            {
                return settingsUserOptions.ContactsAppGranted;
            }

        }
        public bool FilesAppGranted
        {
            get
            {
                return settingsUserOptions.FileAppGranted;
            }

        }
        public bool BrowerAppGranted
        {
            get
            {
                return settingsUserOptions.WebAppGranted;
            }

        }


    }
}
