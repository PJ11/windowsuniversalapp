﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace PhoneAppTest
{
    
    public sealed partial class fileOpenPicker : Page
    {
        public fileOpenPicker()
        {
            this.InitializeComponent();
            /*Button click from the xaml*/
            FilePickAndOpen.Click += new RoutedEventHandler(FilePickAndOpen_Click);
            FileSaveInDir.Click += new RoutedEventHandler(FileSaveInDIR_Click);
            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /*This function will be called when user wants to open a file to attach to a email or any other purpose. 
         File picker will open WindowsPhone8.1 directory structure. USer can navigate and select the required file from the DIR*/
        private  void FilePickAndOpen_Click(object sender, RoutedEventArgs e)
        {
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.SuggestedStartLocation = PickerLocationId.Downloads;
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.FileTypeFilter.Add("*");
            openPicker.PickSingleFileAndContinue();
            
                /*TODO uncomment below function to open and read file : wrapper function call General I/O operation */
                //public async Task<Byte[]>ReadFile(string filename,string path, string encryptionKey);             
            
        }

        /*The Code OnActivated should be written in the App.xaml.cs file
          PickSingleFileAndContinue will suspend the app when the picker is open and  resume it when it is done.*/
        protected override void OnActivated(IActivatedEventArgs args)
        {
            var root = Window.Current.Content as Frame;
            var mainPage = root.Content as MainPage;
            if (mainPage != null && args is FileOpenPickerContinuationEventArgs)
            {
                mainPage.ContinueFileOpenPicker(args as FileOpenPickerContinuationEventArgs);
            }
        }

        public void ContinueFileOpenPicker(FileOpenPickerContinuationEventArgs fileOpenPickerContinuationEventArgs)
        {
            if (fileOpenPickerContinuationEventArgs.Files != null)
            {
                /*TODO uncomment below function to open and read file : wrapper function call General I/O operation */
                //public async Task<Byte[]>ReadFile(string filename,string path, string encryptionKey);  
            }
        }

        /*This function will be called when user wants to save a file in WindowsPhone8.1 directory structure. 
         USer can navigate and save the required file in the DIR*/
        private void FileSaveInDIR_Click(object sender, RoutedEventArgs e)
        {
            FileSavePicker savePicker = new FileSavePicker();
            savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            // Dropdown of file types the user can save the file as
            savePicker.FileTypeChoices.Add("Plain Text", new List<string>() { ".txt" });
            // Default file name if the user does not type one in or select a file to replace. 
            savePicker.SuggestedFileName = "New Document";
            savePicker.PickSaveFileAndContinue();
        }


    }
}
