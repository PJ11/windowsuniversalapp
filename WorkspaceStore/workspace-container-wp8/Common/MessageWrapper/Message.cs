﻿using System;
using System.Windows;
using Windows.UI.Core;
using Windows.UI.Popups;
using Abstracts;

namespace Common
{
    public class Message: MessageWrapper
    {
        /// <summary>
        /// Phone message with message only implementation 
        /// </summary>
        /// <param name="message"></param>
        public override async void PhoneMessage(string message)
        {
            CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            await dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                MessageDialog msgDialog = new MessageDialog(message);
                msgDialog.Commands.Add(new UICommand("Ok"));
                await msgDialog.ShowAsync();
            });
        }

        /// <summary>
        /// Phone message with message and caption implementation
        /// </summary>
        /// <param name="message"></param>
        /// <param name="caption"></param>
        public override async void PhoneMessage(string message, string caption)
        {
            CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            await dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                MessageDialog msgDialog = new MessageDialog(message);
                msgDialog.Commands.Add(new UICommand("Ok", async (e) =>
                {
                    MessageDialog okDialog = new MessageDialog("Ok pressed");
                    await okDialog.ShowAsync();
                }));
                msgDialog.Commands.Add(new UICommand("Cancel", async (e) =>
                {
                    MessageDialog okDialog = new MessageDialog("Cancel pressed");
                    await okDialog.ShowAsync();
                }));
                await msgDialog.ShowAsync();
            });
        }

        public override void WindowsMessage(string message)
        {
            throw new NotImplementedException();
        }

        public override void WindowsMessage(string message, string caption)
        {
            throw new NotImplementedException();
        }
    }
}
