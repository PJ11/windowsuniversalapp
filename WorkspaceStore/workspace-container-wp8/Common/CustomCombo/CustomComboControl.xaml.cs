﻿//Author:Dell Mobility
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.ComponentModel;
using System.Collections;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace combobox
{
    public class ComboItem
    {
        public string FirstString { get; set; }
        public string SecondString { get; set; }
    }
    public partial class CustomComboControl : UserControl
    {

        bool isListOpen = false;

        public CustomComboControl()
        {
            InitializeComponent();


            //This is sample code. We need to setItemSource from consumer class
            IList listComboItem = new List<ComboItem>()
            { 
                new ComboItem(){FirstString = "1 Minute", SecondString = "first"},
                new ComboItem(){FirstString = "2 Minutes", SecondString = "second"},
                new ComboItem{FirstString = "5 Minutes", SecondString = "third"},
                new ComboItem{FirstString = "10 Minutes", SecondString = "fourth"},
                new ComboItem{FirstString = "30 Minutes", SecondString = "fifth"},
                new ComboItem(){FirstString = "60 Minutes", SecondString = "sixth"},
                new ComboItem(){FirstString = "2 Hours", SecondString = "seventh"},
                new ComboItem(){ FirstString = "24 Hours", SecondString = "eighth" }
            };

            SetItemSource(listComboItem);

            buttonContent.Content = "Please Select:";
        }

#if WINDOWS_PHONE_APP
        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            ChangeVisibility();
            e.Handled = true;
        }
#else
        private void OnBackKeyPress(object sender, CancelEventArgs e)
        {
            ChangeVisibility();
            e.Cancel = true;
        }
#endif

        //need to create pass object list in this function is comboitem
        public void SetItemSource(IList itemList)
        {
            contentList.ItemsSource = itemList;
        }

        public String GetComboControlData()
        {
            return buttonContent.Content.ToString();
        }

        private void contentList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var item = e.AddedItems[0];
                buttonContent.Content = ((ComboItem)item).FirstString;
                ChangeVisibility();
            }

        }

        private void ChangeVisibility()
        {
            #if WP_80_SILVERLIGHT
            var page = (Window.Current.Content as Frame).Content as Page;
            if (contentList.Visibility == Visibility.Visible)
            {
                contentList.Visibility = Visibility.Collapsed;
                isListOpen = false;
                if (page != null)
                    HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            }
            else
            {
                contentList.Visibility = Visibility.Visible;
                isListOpen = true;
                if (page != null)
                {
                    HardwareButtons.BackPressed += HardwareButtons_BackPressed;

                }
            }
#endif

        }
        private void buttonContent_Tap(object sender, TappedRoutedEventArgs tappedRoutedEventArgs)
        {
            ChangeVisibility();
        }

        private void LayoutRoot_LostFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            if (isListOpen)
                ChangeVisibility();
        }

    }
}