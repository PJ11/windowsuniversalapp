﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Microsoft.Phone.Controls;


namespace Common.AutoCompleteBoxWrapper
{
    public class WAutoCompleteBox : AutoCompleteBox
    {
        public event Action<object> EnterKeyDown;

        public TextBox mInnerTextBox { get; set; }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            mInnerTextBox = GetTemplateChild("Text") as TextBox;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.Key == Key.Enter) RaiseEnterKeyDownEvent();
        }

        private void RaiseEnterKeyDownEvent()
        {
            var handler = EnterKeyDown;
            if (handler != null) handler(this);
        }
    }
}