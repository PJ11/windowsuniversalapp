﻿// <copyright file="AEKGenerationClass.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date>24-04-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini             24-04-2014               1.0                 First Version </summary>

using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;
using Windows.System.Profile;

namespace AEKGenerationForPhone
{
    #region NAMESPACE
    using System;
    using System.Net;
    using System.Windows;
    using System.Windows.Input;
    #endregion

    /// <summary>
    /// Class to contain the methods to generate Advanced Encryption Key.
    /// </summary>
    public class AEKGenerationClass
    {
        /// <summary>
        /// Creates the AEK.
        /// </summary>
        /// <param name="userPin">The user pin.</param>      
        /// <returns>byte array of AEK</returns>
        public Rfc2898DeriveBytes Create_AEK(string userPin)
        {

            //To store PBKDF2 KeyValue. 
            Rfc2898DeriveBytes key = null;

            //Iteration Count
            int iterations = 1000;

            //hash User PIN
            string hashUPIN = Convert.ToBase64String(Hash_String(userPin));

            //hash Username
            //string hashUname = Convert.ToBase64String(Hash_String(uName));

            ////hash password
            //string hashpswd = Convert.ToBase64String(Hash_String(password));

            // Get the Device Specific Device ID 
#if WP_80_SILVERLIGHT
            var deviceID = Windows.Phone.System.Analytics.HostInformation.PublisherHostId;
#else
            var deviceID = GetHardwareId();
#endif

            //Hash the Device ID
            string hashDeviceID = Convert.ToBase64String(Hash_String(deviceID));

            //Call Method to create the SALT
            byte[] salt = Create_Salt(hashUPIN, hashDeviceID);

            try
            {
                //PBKDF2 implementation with Rfc2898DeriveBytes 
                //Key Generation algorithm to generate the Advanced Encryption Key (AEK).
#if WP_80_SILVERLIGHT
                key = new Rfc2898DeriveBytes(hashUPIN, salt, iterations);
#endif

            }

            catch (Exception e)
            {
                LoggingWP8.WP8Logger.LogMessage("Error in encryption " + e.Message);
            }
            return key;
        }

        /// <summary>
        /// Create_s the salt.
        /// </summary>
        /// <param name="userPin">The user pin.</param>       
        /// <param name="deviceID">The device identifier.</param>
        /// <returns>Hashed Salt</returns>
        public byte[] Create_Salt(string userPin, string deviceID)
        {
            //Including a temporary bytes.
            string tempkey = "DellKeystone987654321KeystoneDell";
            string hashedTempkey = Convert.ToBase64String(Hash_String(tempkey));
            byte[] hashedSalt = null;
            var saltString = userPin + deviceID + hashedTempkey;
            try
            {
                hashedSalt = Hash_String(saltString);
            }
            catch (Exception e)
            {
                LoggingWP8.WP8Logger.LogMessage("Error in Hashing " + e.Message);
            }

            return hashedSalt;
        }

        /// <summary>
        /// Hash_s the string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Hash String</returns>
        public byte[] Hash_String(string value)
        {
            byte[] result = null;

            if (value != null)
            {

                HashAlgorithmProvider hashProvider = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha1);
                IBuffer hash = hashProvider.HashData(CryptographicBuffer.ConvertStringToBinary(value, BinaryStringEncoding.Utf8));
                string hashValue = CryptographicBuffer.EncodeToBase64String(hash);

                //var sha = new SHA1Managed();
                //var bytes = System.Text.Encoding.UTF8.GetBytes(value);
                //byte[] resultHash = sha.ComputeHash(bytes);
                //return resultHash;
            }
            else
            {
                throw new ArgumentNullException("value");
            }

            return result;
        }

        private string GetHardwareId()
        {
            var token = HardwareIdentification.GetPackageSpecificToken(null);
            var hardwareId = token.Id;
            var dataReader = Windows.Storage.Streams.DataReader.FromBuffer(hardwareId);

            byte[] bytes = new byte[hardwareId.Length];
            dataReader.ReadBytes(bytes);

            return Convert.ToBase64String(bytes);
        }

    }
}
