﻿using System.Collections.Generic;
using System.Text;
using Common;
using Windows.Networking.Connectivity;

using System.Collections;
using HaloWP8;
using System;
using HaloWP8.Utils;
using FileHandlingPCL;
using System.Threading;
using System.Windows;
using AuthenticatingUser;

#if WP_80_SILVERLIGHT
using Workspace.KeyGenerationForPhone;
using AuthenticatingUser;
using KeyGenerationForPhone;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
#endif

namespace Common
{

    public class DKAppState
    {
        private readonly static string PREF_NAME = "KeystoneCommon";
        private static readonly string PREF_APP_STATE = "AppState";
        // static Context mContext = null;
        // private static string TAG = DKAppState.class.getSimpleName();
        private static DKAppState _instance = null;
        readonly List<OnAppStateChangeListener> mCallbacks = new List<OnAppStateChangeListener>();
        readonly List<DKAppStatus> mAppStatus = new List<DKAppStatus>();

        // Synchronization object for key management
        private static readonly Object sKeyLock = new Object();
        byte[] mAEK; // advanced encryption key which is used to encrypt/decrypt the actual encryption key
        string mEEK; // encrypted encryption key
        public static string ccmReturnedJSON = null;
        protected Windows.Storage.ApplicationDataContainer keySetting = Windows.Storage.ApplicationData.Current.LocalSettings;
        public static string CCM_USERNAME_KEY = "CCM_USERNAME_KEY";
        public static string CCM_PASSWORD_KEY = "CCM_PASSWORD_KEY";


        protected DKAppState(Context aContext)
        {
            //restorePersistentAppState();
        }


        public void CreateMasterKey( String PIN)
        {
            object userName = "";
            object password = "";
            try
            {
                keySetting.Values.TryGetValue(CCM_USERNAME_KEY, out userName);
                keySetting.Values.TryGetValue(CCM_PASSWORD_KEY, out password);

            }
            catch (Exception e)
            {
                LoggingWP8.WP8Logger.LogMessage("Create Master Key method from PIN Failed: {0}", e.Message);
            }
#if WP_80_SILVERLIGHT
            EncryptionKeyGeneration keyGenerator = new EncryptionKeyGeneration();
            byte[] ek = keyGenerator.Create_EncryptionKey(userName as string, password as string);

            MasterKeyGeneration masterKeyGenerator = new MasterKeyGeneration();
            masterKeyGenerator.Create_MasterKey(ek, PIN);
#endif
        }

        public void SaveCredentials(String userName, String password)
        {
            try
            {
#if WP_80_SILVERLIGHT
                if (!keySetting.Contains(CCM_USERNAME_KEY))
                {
                    keySetting.Add(CCM_USERNAME_KEY, userName);
                }// save key in isolated storage 
                if (!keySetting.Contains(CCM_PASSWORD_KEY))
                {
                    keySetting.Add(CCM_PASSWORD_KEY, password);
                }// save key in isolated storage 
                keySetting.Save();
#else
                keySetting.Values.Add(CCM_USERNAME_KEY, userName);  // save key in isolated storage 
                keySetting.Values.Add(CCM_PASSWORD_KEY, password);  // save key in isolated storage 
#endif
            }
            catch (Exception e)
            {
                LoggingWP8.WP8Logger.LogMessage("Save credentials Failed: {0}", e.Message);
                
            }
           

        }

        public byte [] ValidatePin(String PIN)
        {

            UserAuthentication authentication = new UserAuthentication();
            return authentication.GetDecryptedEncryptionKey(PIN);
        }


        //public static Context getContext() {
        //    if (mContext == null)
        //        mContext = KeystoneApp.getAppContext();
        //    return mContext;
        //}

        public static DKAppState getInstance() 
        {
            if (_instance == null) 
            {
                _instance = new DKAppState(null);
            }
            return _instance;
        }

        //public static DKAppState getInstance() {
        //    if (mContext == null)
        //        mContext = KeystoneApp.getAppContext();
        //    return _instance;
        //}

        /**
         * Public method to reset the singleton.
         */

        public static void reset()
        {
            _instance = null;
            // mContext = null;

        }



        /**
         * Public method to determine if Device is connected to the internet..
         */
        //public static bool isOnline() {
        //    if (DKAppState.getContext() != null) {
        //        ConnectivityManager cm = (ConnectivityManager) DKAppState.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        //        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
        //            return true;
        //        }
        //        // So try all the others
        //        NetworkInfo[] netInfoArray = cm.getAllNetworkInfo();
        //        for (int i = 0; i < netInfoArray.length; i++) {
        //            netInfo = netInfoArray[i];
        //            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
        //                return true;
        //            }
        //        }
        //    }
        //    return false;
        //}

        /**
         * Public method to return current application status in.
         *
         * @param state application state to set in
         */
        public void setAppStatus(DKAppStatus state)
        {
            //
            setAppStatus(state, null);
        }

        public void setAppStatus(DKAppStatus state, Message msg)
        {
            if (mAppStatus != null && !mAppStatus.Contains(state))
                mAppStatus.Add(state);

            savePersistentAppState(state);
        }

        /**
         * Public method to check and see if an App Status is true..
         *
         * @param state application state to set in
         */
        public bool checkAppStatus(DKAppStatus state)
        {
            ////if (state == DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE)
            ////    return getInstance().getMasterKey() != null;
            if (mAppStatus != null && mAppStatus.Contains(state))
                return true;
            return false;

        }
        public byte[] getMasterKey()
        {

            lock (sKeyLock)
            {
                if (mAEK != null)
                {
                    byte[] ek1 = EncryptionEngine.EncryptionEngineInstance.DecryptByteArray(mAEK, mEEK);
                    string ek = ek1.ToString();
                    if (ek != null)
                    {
                        byte[] keyBytes = Convert.FromBase64CharArray(ek.ToCharArray(), 0, ek.Length);

                        return keyBytes;
                    }
                }
            }
            LoggingWP8.WP8Logger.LogMessage("Security Key is null");
            return null;
        }

        /**
         * Public method to return the current App Status.
         *
         * @return state array of the application
         */
        public List<DKAppStatus> getAppStatus()
        {
            return mAppStatus;
        }

        private void savePersistentAppState()
        {
            if (checkAppStatus(DKAppStatus.DK_APP_STATE_ADMIN_LOCKED))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_ADMIN_LOCKED.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_ADMIN_LOCKED.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_ADMIN_LOCKED.ToString()] = 1;
            }
            else
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_ADMIN_LOCKED.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_ADMIN_LOCKED.ToString(), 0);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_ADMIN_LOCKED.ToString()] = 0;
            }
            if (checkAppStatus(DKAppStatus.DK_APP_STATE_UNINITIALIZED))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_UNINITIALIZED.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_UNINITIALIZED.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_UNINITIALIZED.ToString()] = 1;
            }


            if (checkAppStatus(DKAppStatus.DK_APP_STATE_APP_INITIALIZED))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_APP_INITIALIZED.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_APP_INITIALIZED.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_APP_INITIALIZED.ToString()] = 1;
            }
            if (checkAppStatus(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS.ToString()] = 1;
            }
            if (checkAppStatus(DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED.ToString()] = 1;
            }

            if (checkAppStatus(DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE.ToString()] = 1;
            }

            // Do not uncomment these, purpusefully done so that if device restarts then user should be
            // able to log into app using old pin
            // again policy change screen will be shown and this state will come to comply with the policy
            if (checkAppStatus(DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE.ToString()] = 1;
            }
            if (checkAppStatus(DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP.ToString()] = 1;
            }
            if (checkAppStatus(DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE.ToString()] = 1;
            }
            else
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE.ToString(), 0);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE.ToString()] = 0;
            }
            if (checkAppStatus(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS_PERSISTED))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS_PERSISTED.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS_PERSISTED.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS_PERSISTED.ToString()] = 1;
            }
            if (checkAppStatus(DKAppStatus.DK_APP_STATE_POLICY_PENDING))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_POLICY_PENDING.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_POLICY_PENDING.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_POLICY_PENDING.ToString()] = 1;
            }
            else
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_POLICY_PENDING.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_POLICY_PENDING.ToString(), 0);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_POLICY_PENDING.ToString()] = 0;
            }
            if (checkAppStatus(DKAppStatus.DK_APP_STATUS_USER_LOCKED))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATUS_USER_LOCKED.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATUS_USER_LOCKED.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATUS_USER_LOCKED.ToString()] = 1;
            }
            else
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATUS_USER_LOCKED.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATUS_USER_LOCKED.ToString(), 0);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATUS_USER_LOCKED.ToString()] = 0;
            }

            /*
             * CCM server sent up a PIN length change (e.g. 4 to 6 digits or 6 to 4 digits
             */
            if (checkAppStatus(DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE.ToString()] = 1;
            }
            else
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE.ToString(), 0);
                else
                    DKSharedPreferences.mySettings.Values[DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE.ToString()] = 0;
            }
        }

        public void restorePersistentAppState()
        {
            bool foundAppState = false;
            //SharedPreferences settings = mContext.getSharedPreferences(DKSettingsMgr.DK_SHARED_PREFS_NAME, Activity.MODE_PRIVATE);
            object settingsValObj = new object();
            int settingsVal = -1;
            if (DKSharedPreferences.mySettings.Values.TryGetValue(DKAppStatus.DK_APP_STATE_ADMIN_LOCKED.ToString(), out settingsValObj))
            {
                settingsVal = (int)settingsValObj;
                foundAppState = true;
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATE_ADMIN_LOCKED) && (settingsVal == 1))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATE_ADMIN_LOCKED);
                // setAppStatus(DKAppStatus.DK_APP_STATE_ADMIN_LOCKED);
            }
            if (DKSharedPreferences.mySettings.Values.TryGetValue(DKAppStatus.DK_APP_STATE_APP_INITIALIZED.ToString(), out settingsValObj))
            {
                settingsVal = (int)settingsValObj;
                foundAppState = true;
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATE_APP_INITIALIZED) && (settingsVal == 1))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATE_APP_INITIALIZED);
                // setAppStatus(DKAppStatus.DK_APP_STATE_APP_INITIALIZED);
            }
            if (DKSharedPreferences.mySettings.Values.TryGetValue(DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE.ToString(), out settingsValObj))
            {
                settingsVal = (int)settingsValObj;
                foundAppState = true;
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE) && (settingsVal == 1))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE);
                // setAppStatus(DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE);
            }
            if (DKSharedPreferences.mySettings.Values.TryGetValue(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS.ToString(), out settingsValObj))
            {
                settingsVal = (int)settingsValObj;
                foundAppState = true;
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS) && (settingsVal == 1))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS);
                //setAppStatus(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS);
            }
            if (DKSharedPreferences.mySettings.Values.TryGetValue(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS_PERSISTED.ToString(), out settingsValObj))
            {
                settingsVal = (int)settingsValObj;
                foundAppState = true;
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS_PERSISTED) && (settingsVal == 1))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS_PERSISTED);
                //setAppStatus(DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS_PERSISTED);
            }
            if (DKSharedPreferences.mySettings.Values.TryGetValue(DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP.ToString(), out settingsValObj))
            {
                settingsVal = (int)settingsValObj;
                foundAppState = true;
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP) && (settingsVal == 1))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP);
                //setAppStatus(DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP);
            }
            if (DKSharedPreferences.mySettings.Values.TryGetValue(DKAppStatus.DK_APP_STATE_POLICY_PENDING.ToString(), out settingsValObj))
            {
                settingsVal = (int)settingsValObj;
                foundAppState = true;
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATE_POLICY_PENDING) && (settingsVal == 1))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATE_POLICY_PENDING);
                //setAppStatus(DKAppStatus.DK_APP_STATE_POLICY_PENDING);
            }
            if (DKSharedPreferences.mySettings.Values.TryGetValue(DKAppStatus.DK_APP_STATUS_USER_LOCKED.ToString(), out settingsValObj))
            {
                settingsVal = (int)settingsValObj;
                foundAppState = true;
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATUS_USER_LOCKED) && (settingsVal == 1))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATUS_USER_LOCKED);
                //setAppStatus(DKAppStatus.DK_APP_STATUS_USER_LOCKED);
            }
            if (DKSharedPreferences.mySettings.Values.TryGetValue(DKAppStatus.DK_APP_STATE_UNINITIALIZED.ToString(), out settingsValObj))
            {
                settingsVal = (int)settingsValObj;
                foundAppState = true;
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATE_UNINITIALIZED) && (settingsVal == 1))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATE_UNINITIALIZED);
                // setAppStatus(DKAppStatus.DK_APP_STATE_UNINITIALIZED);
            }
            if (DKSharedPreferences.mySettings.Values.TryGetValue(DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED.ToString(), out settingsValObj))
            {
                settingsVal = (int)settingsValObj;
                foundAppState = true;
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED) && (settingsVal == 1))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED);
                //setAppStatus(DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED);
            }
            if (DKSharedPreferences.mySettings.Values.TryGetValue(DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE.ToString(), out settingsValObj))
            {
                settingsVal = (int)settingsValObj;
                foundAppState = true;
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE) && (settingsVal == 1))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE);
                //setAppStatus(DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE);
            }
            if (DKSharedPreferences.mySettings.Values.TryGetValue(DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE.ToString(), out settingsValObj))
            {
                settingsVal = (int)settingsValObj;
                foundAppState = true;
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE) && (settingsVal == 1))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE);
                //setAppStatus(DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE);
            }
            if (DKSharedPreferences.mySettings.Values.TryGetValue(DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE.ToString(), out settingsValObj))
            {
                settingsVal = (int)settingsValObj;
                foundAppState = true;
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE) && (settingsVal == 1))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE);
                //setAppStatus(DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE);
            }
            // Do not uncomment these, purpusefully done so that if device restarts then user should be
            // able to log into app using old pin
            // again policy change screen will be shown and this state will come to comply with the policy
            /*settingsVal = settings.getInt(DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE.toString(), -1);
            if (settingsVal > 0) {
                foundAppState = true;
                setAppStatus(DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE);
            }*/
            // Do not uncomment these
            // Purposefully done to make sure mandatory pin change is not required if device restarts
            // user should be able to log in to app with old pin and create keys
            /*settingsVal = settings.getInt(DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE.toString(), -1);
            if (settingsVal > 0) {
                foundAppState = true;
                setAppStatus(DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE);
            }*/

            if (!foundAppState)
            {
                if (mAppStatus != null && !mAppStatus.Contains(DKAppStatus.DK_APP_STATE_UNINITIALIZED))
                    mAppStatus.Add(DKAppStatus.DK_APP_STATE_UNINITIALIZED);
                //setAppStatus(DKAppStatus.DK_APP_STATE_UNINITIALIZED);
            }
        }

        public void resetAppStatus()
        {
            if (mAppStatus != null)
            {
                mAppStatus.Clear();
                DKSharedPreferences.mySettings.Values.Clear();
            }
        }

        /**
         * Public method to reset a particular status in the App State.
         *
         * @param state application state to reset
         */
        public void resetAppStatus(DKAppStatus state)
        {
            if (mAppStatus != null && mAppStatus.Contains(state))
                mAppStatus.Remove(state);

            savePersistentAppState(state);
        }

        private void savePersistentAppState(DKAppStatus state)
        {
            if (checkAppStatus(state))
            {
                if (!DKSharedPreferences.mySettings.Values.ContainsKey(state.ToString()))
                    DKSharedPreferences.mySettings.Values.Add(state.ToString(), 1);
                else
                    DKSharedPreferences.mySettings.Values[state.ToString()] = 1;
            }
            else
            {
                if (DKSharedPreferences.mySettings.Values.ContainsKey(state.ToString()))
                    DKSharedPreferences.mySettings.Values.Remove(state.ToString());
            }
        }

        /**
         * Public method to send an application event.
         *
         * @param //appEvent application event to send
         */
        public void sendAppEvent(DKAppEvent appEvent)
        {
            sendAppEvent(appEvent, null);
        }

        public void sendAppEvent(DKAppEvent appEvent, Message msg)
        {

            /* send notifications to registered clients */
            //OnAppStateChangeListener[] callbacks;
            //lock (mCallbacks)
            //{
            //    callbacks = new OnAppStateChangeListener[mCallbacks.Count];
            //    mCallbacks.CopyTo(callbacks);
            //}

            switch (appEvent)
            {
                case DKAppEvent.DK_APP_EVENT_ADMIN_LOCK:
                    setAppStatus(DKAppStatus.DK_APP_STATE_ADMIN_LOCKED);
#if WP_80_SILVERLIGHT
                    System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri("/Workspace;component/PIN_View/PINScreen.xaml", UriKind.Relative));
                    });
#else
#endif
                 
                    break;
                case DKAppEvent.DK_APP_EVENT_ADMIN_UNLOCK:
                    resetAppStatus(DKAppStatus.DK_APP_STATE_ADMIN_LOCKED);
                    break;
                case DKAppEvent.DK_APP_EVENT_USER_LOCK:
                    setAppStatus(DKAppStatus.DK_APP_STATUS_USER_LOCKED);
                    break;
                case DKAppEvent.DK_APP_EVENT_USER_UNLOCK:
                    resetAppStatus(DKAppStatus.DK_APP_STATUS_USER_LOCKED);
                    break;
                case DKAppEvent.DK_APP_EVENT_DEVICE_LOCK:
                    setAppStatus(DKAppStatus.DK_APP_STATE_DEVICE_LOCKED);
                    break;
                case DKAppEvent.DK_APP_EVENT_DEVICE_UNLOCK:
                    resetAppStatus(DKAppStatus.DK_APP_STATE_DEVICE_LOCKED);
                    break;
                case DKAppEvent.DK_APP_EVENT_KEYS_AVAILABLE:
                    setAppStatus(DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE);
                    break;
                case DKAppEvent.DK_APP_EVENT_KEYS_UNAVAILABLE:
                    resetAppStatus(DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE);
                    break;
                case DKAppEvent.DK_APP_EVENT_CCM_AUTHENTICATE:
                    setAppStatus(DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED);
                    break;
                case DKAppEvent.DK_APP_EVENT_CHANGE_PIN:
                    setAppStatus(DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE);
                    break;
                case DKAppEvent.DK_APP_EVENT_ACCOUNTS_SETUP:
                    setAppStatus(DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP);
                    break;
                case DKAppEvent.DK_APP_EVENT_EXCHANGE_ERROR:
                    setAppStatus(DKAppStatus.DK_APP_STATE_EXCHANGE_ERROR);
                    break;
            }

            //foreach (OnAppStateChangeListener c in callbacks)
            //{

            //    switch (appEvent)
            //    {
            //        case DKAppEvent.DK_APP_EVENT_DEVICE_LOCK:
            //            c.onDeviceLocked(msg);
            //            break;
            //        case DKAppEvent.DK_APP_EVENT_DEVICE_UNLOCK:
            //            c.onDeviceUnlocked(msg);
            //            break;
            //        case DKAppEvent.DK_APP_EVENT_ADMIN_LOCK:
            //            c.onAdminlocked(msg);
            //            break;
            //        case DKAppEvent.DK_APP_EVENT_ADMIN_UNLOCK:
            //            c.onAdminUnlocked(msg);
            //            break;
            //        case DKAppEvent.DK_APP_EVENT_USER_LOCK:
            //            c.onUserlocked(msg);
            //            break;
            //        case DKAppEvent.DK_APP_EVENT_USER_UNLOCK:
            //            c.onUserUnlocked(msg);
            //            break;
            //        default:
            //            break;
            //    }
            //    // Notify of any state change
            //    //c.onAppEvent(appEvent, msg);
               

            //}
        }

        //     /**
        // * Public method to send an application event delayed.
        // *
        // * @param //appEvent application event to send
        // */
        //public void sendAppEventDelayed(DKAppEvent appEvent, long delay) {
        //    Message msg = Message.obtain();
        //    msg.obj = appEvent;
        //    sendAppEventHandler.sendMessageDelayed(msg, delay);
        //}

        //Handler sendAppEventHandler = new Handler(Looper.getMainLooper()) {
        //    @Override
        //    public void handleMessage(Message msg) {
        //        DKAppEvent event = (DKAppEvent) msg.obj;
        //        sendAppEvent(event);
        //    }
        //};



        public bool isSyncAccountSetup()
        {
            bool retVal = false;

            return retVal;
        }

        /**
         * Public method to register inidicate if the application has every been initialized.
         *
         * When the app first starts the user must login and set up a PIN Code, at this point the app
         * will be initialized.
         */
        public bool isAppInitialized()
        {
            //DKSecurityMgrInternal aDKSecurityMgrInternal = DKSecurityMgrInternal.getInstance(mContext);

            //// check if already logged in or need to login

            //return aDKSecurityMgrInternal.hasInitializedApp();
            return true;

        }

        /**
         * Public method to register app state change listener.
         *
         * @param //AppStateChangeListener instance of app state change listener.
         */
        public void registerAppStateChangeListener(OnAppStateChangeListener appStateChangeListener)
        {
            // This is a linear search, but in practice we'll
            // have only a couple callbacks, so it doesn't matter.
            if (mCallbacks.Contains(appStateChangeListener) == false)
            {
                mCallbacks.Add(appStateChangeListener);
            }
        }

        /**
         * Public method to unregister already registered app state change listener.
         *
         * @param //AppStateChangeListener instance app state change listener.
         */
        public void unregisterAppStateChangeListener(OnAppStateChangeListener appStateChangeListener)
        {
            if (mCallbacks.Contains(appStateChangeListener))
            {
                mCallbacks.Remove(appStateChangeListener);
            }
        }

        /* Section to Communicate App Status.
        *
        */

        public enum DKAppStatus
        {
            DK_APP_STATE_UNINITIALIZED,  // The User Has Never Logged In. (Persistent across runs of the app)
            DK_APP_STATE_APP_INITIALIZED, // The User Has Logged in and Set PIN Code (This is the normal state after initial setup run until Container is wiped. (Persistent across runs of the app)
            DK_APP_STATE_ACCOUNTS_SETUP, // The system has successfully setup accounts
            DK_APP_STATE_ADMIN_LOCKED, // The Admin has locked the device. (Persistent across runs of the app)
            DK_APP_STATUS_USER_LOCKED, // The User has locked the device through self service portal. (Persistent across runs of the app)
            DK_APP_STATE_DEVICE_LOCKED, //For when the screens are locked from a Timeout of use or restart of the app.
            DK_APP_STATE_KEYS_AVAILABLE, // The PIN Code has been entered and the Encryption keys are available
            DK_APP_STATE_CCM_HAS_AUTHENTICATED, // Halo has retrieved the settings from the CCM Server
            DK_APP_STATE_CCM_HAS_SETTINGS, // Halo has retrieved the settings from the CCM Server
            DK_APP_STATE_CCM_HAS_SETTINGS_PERSISTED, // Halo has retrieved the settings from the CCM Server and saved to DKSettings.
            DK_APP_STATE_CCM_CHECKED_IN, //A checkin with the CCM Service has been done.
            DK_APP_STATE_POLICY_VIOLATED, // Wnen any of the Policies are violated, may result in app not running.
            DK_APP_STATE_POLICY_PENDING, // When new policies are receive in onUpdateProfile, there needs to be a period to comply.
            DK_APP_STATE_JAILBROKEN,
            DK_APP_START_ADMIN_WIPED,
            DK_APP_STATE_PIN_NEEDS_CHANGE,
            DK_APP_STATE_MANDATORY_PIN_CHANGE,
            DK_APP_STATE_USER_LOCKED_PAUSE, //user crossed number of failed attempt and action is pause
            DK_APP_STATE_EXCHANGE_ERROR, // If the Exchange Server has an error
            DK_APP_STATE_INCOMING_CALL
        }

        public enum DKAppEvent
        {
            DK_APP_EVENT_DEVICE_LOCK,
            DK_APP_EVENT_DEVICE_UNLOCK,
            DK_APP_EVENT_ADMIN_LOCK,
            DK_APP_EVENT_USER_LOCK, // USer lock event from CCM console, self service
            DK_APP_EVENT_USER_UNLOCK,// USer unlock event from CCM console, self service
            DK_APP_EVENT_ADMIN_UNLOCK,
            DK_APP_EVENT_KEYS_AVAILABLE,
            DK_APP_EVENT_KEYS_UNAVAILABLE,
            DK_APP_EVENT_MEMORY_LOW,
            DK_APP_EVENT_JAILBROKEN,
            DK_APP_EVENT_ENROLLED,
            DK_APP_EVENT_CCM_AUTHENTICATE,
            DK_APP_EVENT_CCM_BAD_POLICY, // If the CCM Server sends us a bad policy, we should stop the startup configuration
            DK_APP_EVENT_EXCHANGE_ERROR, // If the Exchange Server has an error
            DK_APP_EVENT_WIPE_CONTAINER,
            DK_APP_EVENT_CHANGE_PIN,
            DK_APP_EVENT_SHUTDOWN, // All Activities should finish();
            DK_APP_EVENT_KEYSTONE_ACCOUNT_CHANGE,
            DK_APP_EVENT_EMAIL_ACCOUNTS_CHANGE,
            DK_APP_EVENT_POLICY_CHANGE, //New policies received from CCM
            DK_APP_EVENT_ACCOUNTS_SETUP, // The system has successfully setup accounts
            DK_APP_EVENT_SHOW_CCM_MSG, // Show async msg from CCM
            DK_APP_EVENT_UNREAD_COUNT, // Inbox Unread Count
            DK_APP_EVENT_USER_CHANGE_PIN_CANCEL, // User just canceled the PIN change
            DK_APP_EVENT_USER_INACTIVITY_TIMEOUT_CHANGE
        }

        public enum DKActiveSyncStatus
        {
            DK_APP_ACTIVE_SYNC_RUNNING, //The email...sync is running
            DK_APP_ACTIVE_SYNC_WAITING, //The sync is in wating mode
            DK_APP_ACTIVE_SYNC_CONFIGURED, //May take out, the presence of Accounts in the email provider shows this.
        }

        /**
         * Represents App State change listener interface
         * Calling applications needs to register the interface to get callbacks
         */
        public interface OnAppStateChangeListener
        {
            /**
             * Called when App is locked out due to timeout.
             */
            void onDeviceLocked(Message msg);

            /**
             * Called when app unlocked after lock out due to timeout and user unlocks again.
             */
            void onDeviceUnlocked(Message msg);

            /**
             * Called when IT Admin locks the app remotely.
             */
            void onAdminlocked(Message msg);

            /**
             * Called when IT Admin unlocks the app remotely.
             */
            void onAdminUnlocked(Message msg);

            /**
             * Called when IT Admin locks the app remotely.
             */
            void onUserlocked(Message msg);

            /**
             * Called when IT Admin unlocks the app remotely.
             */
            void onUserUnlocked(Message msg);
            /**
             * Called when IT Admin unlocks the app remotely.
             */
            void onMemoryLow(Message msg);

            /**
             * Called when the status changes.
             */
            void onStatusChanged(DKAppStatus status, Message msg);

            /**
             * Called when the status changes.
             */
            void onAppEvent(DKAppEvent event1, Message msg);

        }

        public static bool detectJailbreak()
        {
            DWAuthImpl dwAuth = new DWAuthImpl();
            DKAppState dkp = new DKAppState(null);


            //  if(!DKSettingsMgr.getNonGenuineOsAllowed())
            if (!dkp.getNonGenuineOsAllowed())
            {

                // jailbroken device not allowed.
                if (dwAuth.getJailbreak())
                {
                    // show warning to user if device is jailbroken
                    //Intent i = new Intent(mContext,DKPolicyViolation.class);
                    //i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    //i.putExtra(DKPolicyViolation.Extra_launch_Type,DKPolicyViolation.DKPOLICY_VIOLATION_TYPE_JAILBROKEN);
                    //mContext.startActivity(i);
                    LoggingWP8.WP8Logger.LogMessage("Device is Jailbroken");
                    return true;
                }
            }

            return false;
        }

        public bool getNonGenuineOsAllowed()
        {
            bool result;
            try
            {
                string val = DKSharedPreferences.mySettings.Values["KeystoneSettingConstants.ALLOW_NON_GENUINE_DEVICE_OS"].ToString();
                bool.TryParse(val, out result);
                return result;
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}