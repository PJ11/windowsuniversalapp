﻿/*
 * MessageObject object is used to pass the messages to the UI Thread.
 * It is referenced in the HaloService.cs file
 * Created By Sajini
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloLibrarySupportClasses
{
   public class MessageObject
    {
      public string body { get; set; }
      public int header { get; set; }

      public int arg { get; set; }
    }
}
