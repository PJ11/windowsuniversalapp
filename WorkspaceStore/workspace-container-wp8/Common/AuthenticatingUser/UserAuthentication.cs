﻿// <copyright file="UserAuthentication.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date>05-05-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini             05-05-2014               1.0                 First Version </summary>

using Windows.Storage;

public class Rfc2898DeriveBytes
{
    public Rfc2898DeriveBytes()
    {

    }

    public static Rfc2898DeriveBytes Create_AEK(string userPin)
    {
        return new Rfc2898DeriveBytes();
    }

    public byte[] GetBytes(int bytesToGet)
    {
        return new byte[0];
    }
}

namespace AuthenticatingUser
{
    #region N A M E S P A C E
    using AEKGenerationForPhone;
    using KeyGenerationForPhone;
    using System;
    using Workspace.KeyGenerationForPhone;
    #endregion


    /// <summary>
    /// User Authentication Class Containing Methods to 
    /// Authenticate User and retrieve the Encryption Key.
    /// </summary>
    public class UserAuthentication
    {
        //Creating the instances of the required Classes
        EncryptionandDecryption decrypt = new EncryptionandDecryption();
        AEKGenerationClass aekClass = new AEKGenerationClass();

        //Variable to hold the AEK generated.
        Rfc2898DeriveBytes aekGenerated = null;

        //Variable to hold the Encryption Key after Decryption
        byte[] mDecryptedEncryptionKey = null;

        //Variable to hold the EEK retrieved from the database.
        byte[] mEncryptedEncryptionKey = null;

        /// <summary>
        /// Authenticates the specified user pin.
        /// </summary>
        /// <param name="userPin">The user pin.</param>        
        /// <returns>True if success, else False</returns>
        protected bool Authenticate(string userPin)
        {
            mEncryptedEncryptionKey = null;
            mDecryptedEncryptionKey = null;
            try
            {
                //Generate AEK
                try
                {
                    aekGenerated = aekClass.Create_AEK(userPin);

                }
                catch (Exception e)
                {
                    LoggingWP8.WP8Logger.LogMessage("AEK generation Failed", e.Message);
                }

                //Get the Key Value.
                var KeyValue = aekGenerated.GetBytes(32);
                //Get the Initialisation Vector.
                var Iv = aekGenerated.GetBytes(16);

                //Derive EEK from Database
                try
                {
                    var value = ApplicationData.Current.LocalSettings.Values[MasterKeyGeneration.GLOBAL_ENCRYPTION_KEY];
                    if (value != null) mEncryptedEncryptionKey = (byte[])value;

                }
                catch (Exception e)
                {
                    LoggingWP8.WP8Logger.LogMessage("Retrievaing EEK Failed", e.Message);
                }

                //Decrypt EEK
                //if decryption fails authentication fails.               
                try
                {
                    mDecryptedEncryptionKey = decrypt.DecryptByteArrayFromBytes(mEncryptedEncryptionKey, KeyValue, Iv);

                }
                catch (Exception e)
                {
                    LoggingWP8.WP8Logger.LogMessage("Decryption Failed", e.Message);
                    mEncryptedEncryptionKey = null;
                    mDecryptedEncryptionKey = null;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                LoggingWP8.WP8Logger.LogMessage("Authentication Failed", e.Message);
                return false;
            }
        }

        /// <summary>
        /// Get_s the encryption key.
        /// This method can be used to get the Encryption Key after the authentication.
        /// </summary>
        /// <returns>Encryption Key</returns>
        public byte[] GetDecryptedEncryptionKey(string userPin)
        {
            if (Authenticate(userPin))
            {
                return mDecryptedEncryptionKey;
            }
            else
            {
                LoggingWP8.WP8Logger.LogMessage("Authentication Failed And Encryption Key Not Returned.");
                return null;
            }
        }
    }
}
