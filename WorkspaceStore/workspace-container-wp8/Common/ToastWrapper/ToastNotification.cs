﻿//using Coding4Fun.Toolkit.Controls;
using System;
using System.Net;
using System.Windows;
using Abstracts;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Media;
using Windows.UI;
using Windows.UI.Xaml.Controls;

namespace Common
{
    public class ToastNotification : CreateToastNotification
    {
        /// <summary>
        /// Toasts the notification text.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public override void CreateToastNotificationText(string title, string message)
        {
            var toast = new ToastPrompt
            {
                Title = title,
                Message = message,
                ImageSource = new BitmapImage(new Uri("ApplicationIcon.png", UriKind.RelativeOrAbsolute))
            };
        }

        /// <summary>
        /// Toasts the notification text image.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public override void CreateCustomToastNotification(string title, string message)
        {
            SolidColorBrush gray = new SolidColorBrush(Colors.LightGray);
            SolidColorBrush orange = new SolidColorBrush(Color.FromArgb(200, 255, 117, 24));

            ToastPrompt toast = new ToastPrompt
            {
                Background = gray,
                Foreground = orange,
                Title = title,
                Message = message,
                FontSize = 30,
                TextOrientation = Orientation.Vertical,
                ImageSource = new BitmapImage(new Uri("ApplicationIcon.png", UriKind.RelativeOrAbsolute))
            };
            toast.Show();
        }
        /// <summary>
        /// Toasts the notification text_03.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public override async void CreateToastNotificationForVertical(string title, string message)
        {
            Windows.UI.Core.CoreDispatcher dispatcher = Windows.UI.Core.CoreWindow.GetForCurrentThread().Dispatcher;
            await dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, async () =>
            {
                ToastPrompt toast = new ToastPrompt();
                toast.Title = title;
                toast.Message = message;
                toast.FontSize = 22;
                toast.TextOrientation = Orientation.Vertical;
                toast.Background = new SolidColorBrush(Color.FromArgb(255, 0, 133, 195));
                // toast.ImageSource = new BitmapImage(new Uri("ApplicationIcon.png", UriKind.RelativeOrAbsolute));
                toast.Show();
            });
        }

        /// <summary>
        /// Toasts the notification text_04.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public override void CreateToastNotificationForHorizontal(string title, string message)
        {
            ToastPrompt toast = new ToastPrompt();
            toast.FontSize = 30;
            toast.Title = title;
            toast.Message = message;
            toast.TextOrientation = Orientation.Horizontal;
            toast.ImageSource = new BitmapImage(new Uri("ApplicationIcon.png", UriKind.RelativeOrAbsolute));
            toast.Show();
        }
    }
}