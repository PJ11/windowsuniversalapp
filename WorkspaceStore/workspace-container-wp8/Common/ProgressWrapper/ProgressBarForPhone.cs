﻿// <copyright file="ProgressBarForPhone.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date>10-04-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini             10-04-2014               1.0                 First Version </summary>

namespace Common
{
    #region NAMESPACE   
    using System;
    using System.Net;
    using System.Windows;
    using System.Windows.Input;
    using Abstracts;
    using Windows.UI.Xaml.Controls;
    #endregion
    
    public  class ProgressBarForPhone : ProgressBarAbstractClass<ProgressBar>
    {
        /// <summary>
        /// Create_s the indeterminate_ progress bar.
        /// </summary>
        /// <returns>Progress Bar Object</returns>
        public override ProgressBar Create_Indeterminate_ProgressBar()
        {
            ProgressBar progressBar = new ProgressBar();
            progressBar.IsIndeterminate = true;
            return progressBar;
        }

        /// <summary>
        /// Create_s the determinate_ progress bar.
        /// </summary>
        /// <returns>Progress Bar Object</returns>
        public override ProgressBar Create_Determinate_ProgressBar()
        {
            ProgressBar progressBar = new ProgressBar();
            progressBar.IsIndeterminate = false;
            progressBar.Maximum = 100;
            progressBar.Minimum = 0;
            return progressBar;
        }
    }
}
