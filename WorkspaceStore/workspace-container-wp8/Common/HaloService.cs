﻿#define DEBUG_AGENT
using CommonPCL;
using HaloPCL;
//using HaloPCL.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Windows.ApplicationModel.Background;
using Windows.Devices.Geolocation;
using Windows.UI.Core;
using Windows.UI.Popups;
using HaloPCL.Utils;
using HaloWP8;

namespace Common
{
//
    public class CCMCommandEventArgs : EventArgs
    {
        public CCMCommandEventArgs(HaloPCL.DWStatusCode code, string reason)
        {
            Code = code;
            Reason = reason;
        }

        public HaloPCL.DWStatusCode Code { get; private set; }

        public string Reason { get; private set; }
    }

    public class ProfileUpdateEventArgs : EventArgs
    {
        public ProfileUpdateEventArgs(bool fullConfig, DKCNAppManager am)
        {
            FullConfiguration = fullConfig;
            DKCNAppManager = am;
        }

        public bool FullConfiguration { get; set; }

        public DKCNAppManager DKCNAppManager { get; set; }
    }
    public class CCMWipeContainerEventArgs : EventArgs
    {
        public CCMWipeContainerEventArgs(DKAppState.DKAppEvent e, Message msg)
        {
             Event = e;
             MSG = msg;
        }

        public DKAppState.DKAppEvent Event { get; set; }

        public Message MSG { get; set; }
    }

    public delegate void ProfileUpdateHandler(object sender, ProfileUpdateEventArgs e);
    public delegate void CCMWipeContainerHandler(object sender, CCMWipeContainerEventArgs e);
    public delegate void CCMAuthenticationFailureHandler(object sender, int error, string strError);

    public class HaloService : HaloPCL.DWAuthListener,
        HaloPCL.DWCCMCommandsListener,
        HaloPCL.DWCCMCheckinListener,
        HaloPCL.DWCCMFullConfigListener
    {
        #region Events
        public event ProfileUpdateHandler profileUpdateHandler;
        public event CCMWipeContainerHandler ccmWipeContainerHandler;
        public event CCMAuthenticationFailureHandler AuthenticationFailureHandler;
        #endregion

        protected Geolocator myGeoLocator;
        protected string mUrl;
        protected string mUsername;
        protected string mPassword;
        protected string mEmail;
        protected string mTenantCode;

    

        protected string mMQTTServer = "";
        protected int mMQTTPort = 1883;

        int dsState_halo = HaloPCL.StratusConstants.KeystoneStatus.Active;
        bool ssoUser = false;

      

        DKAppState mAppState = DKAppState.getInstance();
        private const string TASK_NAME = "CheckinAlarm";
        public HaloService()
        {
            DWAuth dwAuth = DWAuth.getInstance();
            if (dwAuth.isauthenticated())
            {
                dwAuth.setCCMCommandsListener(this);
            }
        }

        public int attemptLogin(String user, String pass, String server)
        {
            mUrl = server;
            mEmail = user;
            mPassword = pass;
            mUsername = user;
            DWAuth dwAuth = DWAuth.getInstance();

            if (user.Contains("/"))
            {
                String[] splitSlash = user.Split('/');
                if (splitSlash.Length == 2)
                {
                    String[] splitEmail = splitSlash[1].Split('@');
                    mTenantCode = splitSlash[0];
                    mEmail = splitSlash[1];
                    mUsername = splitSlash[1];

                    if (splitEmail.Length == 2)
                        mUsername = /*"dmpdev1\\" +*/ splitEmail[0];
                    ssoUser = true;
                    return dwAuth.authenticateSSO(mUrl, mUsername, mPassword, mTenantCode, this);
                }
                else
                {
                    onAuthenticateResult(HaloPCL.DWStatusCode.DWStatus_ABORT, "Malformed User name");
                    return -1;
                }
            }
            else
            {
                this.ssoUser = false;
                var ret = dwAuth.authenticate(this.mUrl, this.mEmail, this.mPassword, this);
                return ret;
            }
        }

        public async void onAuthenticateResult(HaloPCL.DWStatusCode state, string reason)
        {
            if(reason=="Fail")
            {
                reason = "Incorrect username/password";
            }

            //Put on main thread
            if (state != HaloPCL.DWStatusCode.DWStatus_SUCCESS)
            {
                Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                {
                    if (null != AuthenticationFailureHandler)
                    {
                        AuthenticationFailureHandler(this, (int)state, reason);
                    }

                    MessageDialog msgDialog = new MessageDialog("Please try again.", reason);
                    msgDialog.Commands.Add(new UICommand("Ok"));
                    await msgDialog.ShowAsync();
                });

                //CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
                //await dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                //{
                //    if (null != AuthenticationFailureHandler)
                //    {
                //        AuthenticationFailureHandler(this, (int)state, reason);
                //    }

                //    MessageDialog msgDialog = new MessageDialog("Please try again.",reason);
                //    msgDialog.Commands.Add(new UICommand("Ok"));
                //    await msgDialog.ShowAsync();
                //});

                //System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                //{
                //    if(null != AuthenticationFailureHandler)
                //    {
                //        AuthenticationFailureHandler(this, (int)state, reason);
                //    }
                //    WCustomMessageBox messageBox = new WCustomMessageBox()
                //    {
                //        Caption = reason,
                //        Message = "Please try again.",
                //        LeftButtonContent = "OK",
                        
                //    };                   
                //    messageBox.Show();
                //});
            }
            else
            {
                mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED);
                mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_UNINITIALIZED);
            }
            switch (state)
            {
                case HaloPCL.DWStatusCode.DWStatus_SUCCESS:
                    startMQTT();
                    getFullConfig();
                    break;
                default:
                    break;
            }
        }

        public void startMQTT()
        {
            DWAuth dwAuth = DWAuth.getInstance();
            dwAuth.setCCMCommandsListener(mMQTTServer, mMQTTPort, this);
        }

        public void stopMQTT()
        {
            DWAuth dwAuth = DWAuth.getInstance();
            dwAuth.setCCMCommandsListener(null);
        }

        public void SetMQTTServer(String server, int port)
        {
            mMQTTServer = server;
            mMQTTPort = port;
        }

        public HaloPCL.DWStatusCode checkin()
        {
            DWAuth dwAuth = DWAuth.getInstance();
            return dwAuth.checkin(this);
        }

        public HaloPCL.DWStatusCode getFullConfig()
        {
            DWAuth dwAuth = DWAuth.getInstance();
            return dwAuth.getFullConfig(this);
        }

        public int onProfileUpdate(string jsonProfile, bool fullConfig)
        {
            DKCNSettings dkcn = new DKCNSettings(jsonProfile);
            // mgr.parsePolicyToDictionary(jsonProfile);
            DKCNAppManager mgr = DKCNAppManager.Instance(); // This will ultimately set the App.mManager object
            if (fullConfig)
            {
                mgr.settingsUserOptions = dkcn;
                
                // LWR Will ultimately use a temp key for now 
                // TOO EARLY: Comment out: mgr.storeUserSettings();

                mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS);

                if (null != profileUpdateHandler)
                {
                    profileUpdateHandler(this, new ProfileUpdateEventArgs(fullConfig, mgr));
                }
            }
            else
            {
                mgr.settingsPendingOptions = dkcn;
                if (null != profileUpdateHandler)
                {
                    profileUpdateHandler(this, new ProfileUpdateEventArgs(fullConfig, mgr));
                }
            }

            return 0;
        }

        public HaloPCL.DWDataOut onCommand(int type, HaloPCL.DWDataIn dataIn)
        {
            HaloPCL.DWDataOut ret = new HaloPCL.DWDataOut();
            ret.result = 0;

            if (type == DWAuth.CMD_KeystoneAdminLock)
            {
                dsState_halo = HaloPCL.StratusConstants.KeystoneStatus.AdminLocked;
                this.lock_callback(dsState_halo);
                ret.jsonData = setKSStatus(dsState_halo);

            }
            else if (type == DWAuth.CMD_KeystoneUserLock)
            {
                dsState_halo = HaloPCL.StratusConstants.KeystoneStatus.UserLocked;
                this.lock_callback(dsState_halo);
                ret.jsonData = setKSStatus(dsState_halo);

            }
            else if (type == DWAuth.CMD_KeystoneUnlock)
            {
                dsState_halo = HaloPCL.StratusConstants.KeystoneStatus.Active;
                this.lock_callback(dsState_halo);
                ret.jsonData = setKSStatus(dsState_halo);

            }
            else if (type == DWAuth.CMD_ResetPIN)
            {
                this.resetPin_callback();

            }
            else if (type == DWAuth.CMD_WipeWorkspaceData)
            {
                this.wipeData_callback();

            }
            else if (type == DWAuth.CMD_Query)
            {
                this.query_callback();
                ret.jsonData = setQuery(dsState_halo);

            }
            else if (type == DWAuth.CMD_SendMessage)
            {
                if (dataIn != null)
                {
                    try
                    {
                        var obj = JArray.Parse(dataIn.jsonData);
                        foreach (JArray ja in obj)
                        {
                            if (ja != null)
                            {
                                string name = ja[0]["name"].ToObject<string>().ToString();
                                if (name.Equals("message"))
                                {
                                    sendMessage_callback(ja[0]["value"].ToObject<string>().ToString());
                                }
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        Log.w("Exceptipn", e.Message);
                    }
                }

            }


            return ret;
        }

        private int sendMessage_callback(string jsonMsg)
        {
            DKSharedPreferences.mySettings.Values[DKConstants.PREF_CCM_MSG] = jsonMsg;
            mAppState.sendAppEvent(DKAppState.DKAppEvent.DK_APP_EVENT_SHOW_CCM_MSG);
            return 0;

        }

        private string setQuery(int dsState_halo)
        {
            //
            JSONObject jo = new JSONObject();
            try
            {
                updateState();

                String versionString = "";
                DKCNAppManager mgr = DKCNAppManager.Instance(); // This will ultimately set the App.mManager object
                DKCNSettings settingsMgr = mgr.settingsUserOptions;

                Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation deviceInfo = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();

                jo.put("KEYSTONE_STATUS", dsState_halo);
                jo.put("KEYSTONE_VERSION", "" + versionString);
                jo.put("DEVICE_NAME", deviceInfo.SystemProductName);
                jo.put("DEVICE_MODEL", deviceInfo.SystemManufacturer);
                jo.put("DEVICE_OS", "WindowsPhone8");
#if WP_80_SILVERLIGHT
                jo.put("DEVICE_FIRMWARE_VERSION", deviceInfo.SystemFirmwareVersion);
#endif
                if (settingsMgr != null)
                {

                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_PIN_LENGTH, "" + settingsMgr.PinLength);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE, "" + settingsMgr.LoginAttemptsAllowed);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_LOGIN_FAILURE_ACTION, "" + settingsMgr.LoginFailureAction);
                    //  jo.put("SETTINGS_FAILED_LOGIN_ACTION_TIME", "" + settingsMgr.tim);
                    //jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION, "" + settingsMgr.ChecinExpiration);
                    //jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_CHECKIN_EXPIRATION_ACTION, "" + settingsMgr.CheckinExpirationAction);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_INACTIVITY_TIMEOUT, "" + settingsMgr.InactivityTimeout);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_ALLOW_JAILBREAK, "" + settingsMgr.JailbrokenDeviceAllowed);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_ENABLE_EXTERNAL_SHARING, "" + settingsMgr.ExternalSharingEnabled);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_ENABLE_COPY_PASTE, "" + settingsMgr.CopyPasteEnabled);

                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_ACTIVESYNC_HOST, "" + settingsMgr.EasHost);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DOMAIN, "" + settingsMgr.EasDomain);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_ACTIVESYNC_USERNAME, "" + settingsMgr.UserDisplayName);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_EMAIL_USER_DISPLAY_NAME, "" + settingsMgr.UserDisplayName);
                    //   jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_ACTIVESYNC_EMAILputRESS, "" + emailConfig.getEmailputress());
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_ACTIVESYNC_DYNAMICUSERINFO, "" + settingsMgr.DynamicUserInfo);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_ACTIVESYNC_SSLREQUIRED, "" + settingsMgr.UseSSL);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_EMAIL_SYNC_FREQUENCY, "" + settingsMgr.MailFetchFrequency);
                    //  jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_ACTIVESYNC_AGENTIDENTIFIER, "" + emailConfig.getAgentIdentifier());
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_EMAIL_SIGNATURE, "" + settingsMgr.EmailSignature);

                    //jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_DEFAULT_HOMEPAGE, "" + settingsMgr.DefaultHomePage);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_USER_FIRST_NAME, "" + settingsMgr.UserFirstName);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_USER_LAST_NAME, "" + settingsMgr.UserLastName);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_USER_PHONE, "" + settingsMgr.UserPhoneNumber);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_USER_TITLE, "" + settingsMgr.UserTitle);

                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_REQUIRED, "" + settingsMgr.WebProxyRequired);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_IP, "" + settingsMgr.WebProxyIP);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_WEBBROWSER_PROXY_PORT, "" + settingsMgr.WebProxyPort);
                    jo.put(AbstractsPCL.HaloPolicySettingsAbstract.KEY_WEBBROWSER_BOOKMARK, "" + settingsMgr.Bookmarks);
                }
            }
            catch (Exception e)
            {
            }
            return jo.ToString();
        }

        private void updateState()
        {
            if (mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_ADMIN_LOCKED))
                dsState_halo = HaloPCL.StratusConstants.KeystoneStatus.AdminLocked;
            else if (mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATUS_USER_LOCKED))
                dsState_halo = HaloPCL.StratusConstants.KeystoneStatus.UserLocked;
            else if (mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_POLICY_VIOLATED))
                dsState_halo = HaloPCL.StratusConstants.KeystoneStatus.PolicyLocked;
            else if (mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_JAILBROKEN))
                dsState_halo = HaloPCL.StratusConstants.KeystoneStatus.Active;
            else
                dsState_halo = HaloPCL.StratusConstants.KeystoneStatus.Active;

        }

        private int query_callback()
        {
            Log.i("HaloService", "Device Information");
            // getFullConfig();
            return 0;
        }

        private int wipeData_callback()
        {
            Log.i("HaloService", "Wipe Data");
            DWAuth dwAuth = DWAuth.getInstance();
            updateState();
            checkin();
            dwAuth.deauthenticate();

            //ToastNotification ToastWipe = new ToastNotification();
            //ToastWipe.CreateToastNotificationForVertical("Remote Wipe/Unregister", "This Application has been wiped and data deleted.");            //

            ccmWipeContainerHandler(DKAppState.DKAppEvent.DK_APP_EVENT_WIPE_CONTAINER,null);
            //DKAppState.getInstance().sendAppEvent(DKAppState.DKAppEvent.DK_APP_EVENT_WIPE_CONTAINER);
            //mAppState.sendAppEvent(DKAppState.DKAppEvent.DK_APP_EVENT_WIPE_CONTAINER);
            //Intent i = new Intent(getBaseContext(),DKPolicyViolation.class);
            //i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //i.putExtra(DKPolicyViolation.Extra_launch_Type,DKPolicyViolation.DKPOLICY_REMOTE_WIPE);
            //startActivity(i);

            /** find how to wipe data clean**/

            return 0;
        }

        private int resetPin_callback()
        {
            Log.i("HaloService", "Reset Pin");
            mAppState.sendAppEvent(DKAppState.DKAppEvent.DK_APP_EVENT_CHANGE_PIN);
            return 0;
        }

        private int lock_callback(int status)
        {
            Log.i("Halolibrary", "Lock Message");

            if (status == HaloPCL.StratusConstants.KeystoneStatus.AdminLocked)
            {
                // just lock the device
                mAppState.sendAppEvent(DKAppState.DKAppEvent.DK_APP_EVENT_ADMIN_LOCK);
            }
            else if (status == HaloPCL.StratusConstants.KeystoneStatus.UserLocked)
            {
                // just lock the device
                mAppState.sendAppEvent(DKAppState.DKAppEvent.DK_APP_EVENT_USER_LOCK);
            }
            else if (status == HaloPCL.StratusConstants.KeystoneStatus.Active)
            {
                if (mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_ADMIN_LOCKED))
                    mAppState.sendAppEvent(DKAppState.DKAppEvent.DK_APP_EVENT_ADMIN_UNLOCK);
                else if (mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATUS_USER_LOCKED))
                    mAppState.sendAppEvent(DKAppState.DKAppEvent.DK_APP_EVENT_USER_UNLOCK);
            }

            updateState();
            this.checkin();
            return 0;
        }

        private string setKSStatus(int state)
        {
            JSONObject jo = new JSONObject();
            try
            {
                jo.put("KEYSTONE_STATUS", state);
            }
            catch (Exception e)
            {
            }
            return jo.ToString();
        }

        public void unregister()
        {
            Log.i("HaloService", "Device UnRegistered");
            mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS);

            if (null != ccmWipeContainerHandler)
            {
                ccmWipeContainerHandler(this, new CCMWipeContainerEventArgs(DKAppState.DKAppEvent.DK_APP_EVENT_WIPE_CONTAINER, null));
            }
           // mAppState.sendAppEvent(DKAppState.DKAppEvent.DK_APP_EVENT_WIPE_CONTAINER);
            /**Later complete it**/
            //DKCNAppManager.Instance().
             // DKAppLauncher.showWipeScreen(DKPolicyViolation.DKPOLICY_REMOTE_WIPE);
            

        }

        public bool onJailbreak()
        {
            Log.i("HaloService", "Phone is Jailbroken");
            return DKAppState.detectJailbreak();
        }

        public int onChekinCompleted(HaloPCL.DWStatusCode state, string msg)
        {
            switch (state)
            {
                case HaloPCL.DWStatusCode.DWStatus_SUCCESS:
                    Log.i("HaloService", "Checkin success!");
                    //if (mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP))
                    //    StartAgent();
                    break;
                default:
                    Log.i("HaloService", "Checkin failed!");
                    break;
            }
            return 0;
        }

        public int onGetFullConfigCompleted(HaloPCL.DWStatusCode state, string msg)
        {
            checkin();
            return (int)HaloPCL.DWStatusCode.DWStatus_SUCCESS;
            // Success!
        }
    }
}