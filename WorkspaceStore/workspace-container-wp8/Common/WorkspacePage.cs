﻿using Microsoft.Phone.Controls;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BasicPage
{
    public class WorkspacePage: PhoneApplicationPage
    {
        /// <summary>
        /// Navigation to Home Page
        /// </summary>
        /// <param name="viewName">Home page name</param>
        public void NavigateToHome(string viewName)
        {
           this.NavigationService.Navigate(new Uri("/"+viewName, UriKind.Relative));
        }




    }
}
