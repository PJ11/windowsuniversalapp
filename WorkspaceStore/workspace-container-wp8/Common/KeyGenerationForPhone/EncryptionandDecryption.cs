﻿// <copyright file="EncryptionandDecryption.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date>30-04-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini             30-04-2014               1.0                 First Version </summary>

namespace Workspace.KeyGenerationForPhone
{
    #region N A M E S P A C E
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    #endregion
    /// <summary>
    /// Class To Perform Encryption and Decryption
    /// </summary>
    class EncryptionandDecryption
    {
        /// <summary>
        /// Encrypts the string to bytes.
        /// </summary>
        /// <param name="plainText">The plain text.</param>
        /// <param name="Key">The key.</param>
        /// <param name="IV">The iv.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// plainText
        /// or
        /// Key
        /// or
        /// Key
        /// </exception>
        public byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            //Byte array to hold the Encrypted Data.
            byte[] encrypted = null;

            // Create an Aes object 
            // with the specified key and IV. 
#if WP_80_SILVERLIGHT
            using (Aes aesAlg = new AesManaged())
            {
                //Key Value
                aesAlg.Key = Key;
                //Initialisation Vector Value
                aesAlg.IV = IV;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
#endif

            // Return the encrypted bytes from the memory stream. 
            return encrypted;
        }

        /// <summary>
        /// Decrypts the string from bytes.
        /// </summary>
        /// <param name="cipherText">The cipher text.</param>
        /// <param name="Key">The key.</param>
        /// <param name="IV">The iv.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// cipherText
        /// or
        /// Key
        /// or
        /// Key
        /// </exception>
       public string DecryptStringFromBytes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the string used to hold the decrypted text.             
            string plainText = null;

            // Create an Aes object 
            // with the specified key and IV. 
#if WP_80_SILVERLIGHT
           /*
            using (Aes aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plainText = srDecrypt.ReadToEnd();
                        }
                        csDecrypt.Flush();
                    }
                    msDecrypt.Flush();
                }
            }
            * */
#endif
            return plainText;

        }

       public byte [] DecryptByteArrayFromBytes(byte[] cipherText, byte[] Key, byte[] IV)
       {
           string plainText = DecryptStringFromBytes(cipherText, Key, IV);
           if(null != plainText)
           {
               return Encoding.UTF8.GetBytes(plainText);
           }
           return null;
       }
    }
}
