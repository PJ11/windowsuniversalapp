﻿// <copyright file="MasterKeyGeneration.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date>30-04-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini             30-04-2014               1.0                 First Version </summary>

using Windows.Storage;
using Common.EncryptionHelpers;

namespace KeyGenerationForPhone
{
    #region N A M E S P A C E
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AEKGenerationForPhone;
    using System.IO;
    using Workspace.KeyGenerationForPhone;

    #endregion
    public class MasterKeyGeneration
    {
        //Instance of AEKGenerationClass to access Create_AEK method.
        AEKGenerationClass aek = new AEKGenerationClass();
        //Instance of EncryptionandDecryption Class.
        EncryptionandDecryption encryptDecypt = new EncryptionandDecryption();

        public static string GLOBAL_ENCRYPTION_KEY = "ENCRYPTION_KEY";

        /// <summary>
        /// Create_s the master key.
        /// Encrypts the Encryption Key (Master Key) and generates Encrypted Encryption Key (EEK)
        /// </summary>
        /// <param name="encryptedKey">The encrypted key.</param>
        /// <returns>byte array of EEK</returns>
        //public void Create_MasterKey(byte[] encryptionKey, string uPin)
        //{

        //    // Check arguments. 
        //    if (encryptionKey == null || encryptionKey.Length <= 0)
        //        throw new ArgumentNullException("encryptedKey");
        //    if (uPin == null || uPin.Length <= 0)
        //        throw new ArgumentNullException("uPin");

        //    //Convert byte array of encryptedKey to String.
        //    string encrpytionKeyAsString = Convert.ToBase64String(encryptionKey);

        //    //Call the Create_AEK method to generate AEK.
        //    Rfc2898DeriveBytes rfckey = aek.Create_AEK(uPin);

        //    //Get the Key Value.
        //    var KeyValue = rfckey.GetBytes(32);

        //    //Get the Initialisation Vector.
        //    var Iv = rfckey.GetBytes(16);

        //    try
        //    {
        //        //Call the EncryptStringToBytes method to Encrypt the EncryptedKey and Generate the Master Key
        //        byte[] EEK = encryptDecypt.EncryptStringToBytes(encrpytionKeyAsString, KeyValue, Iv);

        //        ApplicationData.Current.LocalSettings.Values.Add(GLOBAL_ENCRYPTION_KEY, EEK);  // save key in application data local settings
        //        // InsertKey(EEK);
        //    }
        //    catch (Exception e)
        //    {
        //        LoggingWP8.WP8Logger.LogMessage("Error in encryption ", e.Message);
        //    }
        //}

        /// <summary>
        /// Create_s the master key.
        /// Encrypts the Encryption Key (Master Key) and generates Encrypted Encryption Key (EEK)
        /// </summary>
        /// <param name="encryptedKey">The encrypted key.</param>
        /// <returns>byte array of EEK</returns>
        public void Create_MasterKey(byte[] encryptionKey, string uPin)
        {

            // Check arguments. 
            if (encryptionKey == null || encryptionKey.Length <= 0)
                throw new ArgumentNullException("encryptedKey");
            if (uPin == null || uPin.Length <= 0)
                throw new ArgumentNullException("uPin");

            //Convert byte array of encryptedKey to String.
            string encrpytionKeyAsString = Convert.ToBase64String(encryptionKey);

            try
            {
                //Call the EncryptStringToBytes method to Encrypt the EncryptedKey and Generate the Master Key
                byte[] EEK = EncryptDecryptHelper.Encrypt(encrpytionKeyAsString, encryptionKey);

                ApplicationData.Current.LocalSettings.Values.Add(GLOBAL_ENCRYPTION_KEY, EEK);  // save key in application data local settings
                // InsertKey(EEK);
            }
            catch (Exception e)
            {
                LoggingWP8.WP8Logger.LogMessage("Error in encryption ", e.Message);
            }
        }
    }
}
