﻿// <copyright file="PinChangeSupport.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date>02-05-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini             02-05-2014               1.0                 First Version </summary>

using Windows.Storage;

namespace KeyGenerationForPhone
{
    #region N A M E S P A C E
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AEKGenerationForPhone;
    using Workspace.KeyGenerationForPhone;
    using System.Text.RegularExpressions;
    using System.Threading;
    using FileHandlingPCL;

    #endregion
    public class PinChangeSupport
    {
        // Instance Of EncryptionandDecryption
        EncryptionandDecryption encryptdecrypt = new EncryptionandDecryption();

        // Instance of AEKGenerationClass
        AEKGenerationClass aek = new AEKGenerationClass();

        /// <summary>
        /// Users the pin change.
        /// </summary>
        /// <param name="oldPin">The old pin.</param>
        /// <param name="newPin">The new pin.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="paswd">The paswd.</param>
        public async Task<bool> UserPinChange(string oldPin, string newPin)
        {
            //Byte array to store the key Retrieved from DB.
            byte[] EEK = null;
            try
            {
                try
                {
                    //retrieve EEK from DB   mySett.TryGetValue(HaloPolicySettingsAbstract.KEY_PIN_LENGTH, out pinLength);
                    var value = ApplicationData.Current.LocalSettings.Values[MasterKeyGeneration.GLOBAL_ENCRYPTION_KEY];
                    if (value != null) EEK = (byte[])value;
                }
                catch (Exception e)
                {
                    LoggingWP8.WP8Logger.LogMessage("Error in reading the Key", e.Message);
                }

                //Generate AEK from Old PIN
                Rfc2898DeriveBytes rfckey = aek.Create_AEK(oldPin);
                var KeyValue = rfckey.GetBytes(32); //Retrieve the Key Value          
                var Iv = rfckey.GetBytes(16); // Retrieve the Initialisation Vector

                //Decrypt the EEK using Old AEK generated from old User PIN.
                string decryptedEncryptionKeyAsString = encryptdecrypt.DecryptStringFromBytes(EEK, KeyValue, Iv);

                // TEST VARIABLES
                byte[] decryptedEncryptionKeyOriginal = encryptdecrypt.DecryptByteArrayFromBytes(EEK, KeyValue, Iv);


                //Generate AEK from New PIN
                Rfc2898DeriveBytes rfcNewkey = aek.Create_AEK(newPin);
                var newKeyValue = rfcNewkey.GetBytes(32);
                var newIv = rfcNewkey.GetBytes(16);

                //Encrypt using new AEK generated from new User PIN.
                byte[] newEEK = encryptdecrypt.EncryptStringToBytes(decryptedEncryptionKeyAsString, newKeyValue, newIv);

                {
                    // TEST VARIABLES
                    byte[] decryptedEncryptionKeyChanged = encryptdecrypt.DecryptByteArrayFromBytes(newEEK, newKeyValue, newIv);
                    string decryptedEncryptionKeyAsStringChanged = encryptdecrypt.DecryptStringFromBytes(newEEK, newKeyValue, newIv);
                }
                try
                {
                    ApplicationData.Current.LocalSettings.Values.Remove(MasterKeyGeneration.GLOBAL_ENCRYPTION_KEY);
                    ApplicationData.Current.LocalSettings.Values.Add(MasterKeyGeneration.GLOBAL_ENCRYPTION_KEY, newEEK);   //now store this new EEK
                    WriteKeyToFileForBackgroundTask(newEEK);
                }
                catch (Exception e)
                {
                    LoggingWP8.WP8Logger.LogMessage("Error in inserting the Key", e.Message);
                    return false;
                }

#if NOT_USING_THIS_SQLCIPHER_METHOD
                //change password key after changing the PIN
                string stroldEEK = Convert.ToBase64String(EEK);
                string oldKey = Regex.Replace(stroldEEK, @"[^a-zA-Z]+", "");
                string strnewEEK = Convert.ToBase64String(newEEK);
                string newKey = Regex.Replace(strnewEEK, @"[^a-zA-Z]+", "");
                //call database method to change password key
                workspace_datastore.SqlcipherManager.ChangeDBPwd(oldKey, newKey);
#endif
                return true;
            }
            catch (Exception e)
            {
                LoggingWP8.WP8Logger.LogMessage("Error in Pin Change", e.Message);
                return false;
            }
        }

        private async Task WriteKeyToFileForBackgroundTask(byte[] mEncryptedEncryptionKey)
        {
            if (mEncryptedEncryptionKey != null)
            {
                using (Mutex mutex = new Mutex(true, "XApplicationData"))
                {
                    mutex.WaitOne();
                    try
                    {
                        FileManager fileMan = new FileManager();
                        await fileMan.CreateFile("x_app_info", "", mEncryptedEncryptionKey);
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                    }
                    finally
                    {
                        mutex.ReleaseMutex();
                    }
                }
            }
        }
    }
}
