﻿// <copyright file="EncryptionKeyGeneration.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date>30-04-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini             30-04-2014               1.0                 First Version </summary>

using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;

namespace Workspace.KeyGenerationForPhone
{
    #region N A M E S P A C E
    using System;
    using System.Net;
    using System.Windows;
    using System.Windows.Input;
    using AEKGenerationForPhone;
    #endregion

    public class EncryptionKeyGeneration
    {
        /// <summary>
        /// Create_s the encryption key.
        /// </summary>
        /// <param name="uName">Name of the u.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public byte[] Create_EncryptionKey(string uName, string password)
        {
            byte[] encryptionKey = null;

            //Iteration Count
            int iteration = 1000;

            //Salt Length
            int saltLength = 8;

            //Key Length
            int keyLength = 256;

            //String to generate the Key
            string userId = uName + password;
            
            //Hash the String
            byte[] hashedUserId = Hash_String(userId);

            //To generate Salt
#if WP_80_SILVERLIGHT
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buffer = new byte[saltLength];          
            rng.GetBytes(buffer);
#endif

            try
            {
#if WP_80_SILVERLIGHT
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(hashedUserId, buffer, iteration);
                encryptionKey = key.GetBytes(keyLength);
#endif
            }
            catch(Exception e)
            {
                LoggingWP8.WP8Logger.LogMessage("Error in encryption ", e.Message);
            }

            return encryptionKey;
        }

        /// <summary>
        /// Hash_s the string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Hash String</returns>
        public byte[] Hash_String(string value)
        {
            byte[] result = null;

            if (value != null)
            {
                HashAlgorithmProvider hashProvider = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha1);
                IBuffer hash = hashProvider.HashData(CryptographicBuffer.ConvertStringToBinary(value, BinaryStringEncoding.Utf8));
                string hashValue = CryptographicBuffer.EncodeToBase64String(hash);

                //var sha = new SHA1Managed();
                //var bytes = System.Text.Encoding.UTF8.GetBytes(value);
                //byte[] resultHash = sha.ComputeHash(bytes);
                //return resultHash;
            }
            else
            {
                throw new ArgumentNullException("value");
            }

            return result;
        }
    }
}
