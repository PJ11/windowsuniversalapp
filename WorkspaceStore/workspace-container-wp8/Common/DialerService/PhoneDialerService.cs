﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DialerService
{
   public class PhoneDialerService
    {
        public void DialNumber(string phoneNumber, string name)
        {
            #if WP_80_SILVERLIGHT
            Windows.ApplicationModel.Calls.PhoneCallManager.ShowPhoneCallUI(phoneNumber, name);
#endif
        }
    }
}
