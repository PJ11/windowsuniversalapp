﻿using HaloWP8;
using HaloWP8.Utils;
using HaloPCL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Windows.Devices.Geolocation;
using Windows.Networking.Connectivity;
//using Windows.Services.Maps;
using Windows.UI.Core;
using Windows.UI.Popups;

namespace Common
{
    public class LocationMonitor
    {


        private static LocationMonitor _inst;
        private static Geoposition mInitialized;
        private Context ctx;

        private LocationMonitor(Context context)
        {
            HaloLog.i("LocationMonitor() called.");
            //ctx = context;
            // Acquire a reference to the system Location Manager.
            // locationManager = (LocationManager)ctx.getSystemService(Context.LOCATION_SERVICE);
        }


        //public static LocationMonitor Instance(Context mqttService)
        //{
        //    if (_inst == null)
        //    {
        //        _inst = new LocationMonitor(mqttService);

        //    }
        //    if (!mInitialized)
        //        mInitialized = _inst.Init();
        //    return _inst;
        //}

        public static List<MapLocation> addresses;

        private Geoposition Init()
        {


            // Avoid setting up location listener for Ethernet type of connection to prevent
            // illegal argument exception. 
            if (isDeviceOnlyEthernetCapableForConnection() == false)
            {
                return gpsLocation().Result;
            }
            else
            {
                return null;
            }
        }
        //GPS location
        public async Task<Geoposition> gpsLocation()
        {
            string expMsg = string.Empty;
            bool isExp = false;

            Geoposition positionGPS = null;
            if (Windows.Storage.ApplicationData.Current.LocalSettings.Values.ContainsKey("LocationConsent"))
            {
                var val = Windows.Storage.ApplicationData.Current.LocalSettings.Values["LocationConsent"];
                //user has  for in or out gps
                if ((bool)val == true)
                {
                    // The user has opted out of Location.
                    Geolocator geolocator = new Geolocator();
                    geolocator.DesiredAccuracy = PositionAccuracy.High;

                    geolocator.PositionChanged += geolocator_PositionChanged;
                    geolocator.StatusChanged += myGeoLocator_StatusChanged;
                    try
                    {

                        positionGPS = await geolocator.GetGeopositionAsync(
               maximumAge: TimeSpan.FromMinutes(5),
               timeout: TimeSpan.FromSeconds(10)
               );
                        //// get the async task
                        //var asyncResult = geolocator.GetGeopositionAsync();
                        //var task = asyncResult.AsTask();

                        //// add a race condition - task vs timeout task
                        //var readyTask = await Task.WhenAny(task, Task.Delay(10000));
                        //if (readyTask != task) // timeout wins
                        //    throw new TimeoutException();

                        //// position found within timeout
                        //positionGPS = await task; 
                        SaveLocationToShare(positionGPS);
                    }
                    catch (Exception ex)
                    {
                        isExp = true;
                        expMsg = "Error in getting location:" + ex.StackTrace;
                    }

                    if (isExp)
                    {
                        CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
                        await dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                        {
                            MessageDialog msgDialogLat = new MessageDialog(expMsg);
                            msgDialogLat.Commands.Add(new UICommand("Ok"));
                            await msgDialogLat.ShowAsync();
                        });
                    }

                }
            }
            else
            {
                MessageDialog msgDialog = new MessageDialog("This app accesses your phone's location. Is that ok?",
                    "Location");

                msgDialog.Commands.Add(new UICommand("Ok", (c) =>
                {
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values["LocationConsent"] = true;
                }));
                msgDialog.Commands.Add(new UICommand("Cancel", (c) =>
                {
                    Windows.Storage.ApplicationData.Current.LocalSettings.Values["LocationConsent"] = false;
                }));
                await msgDialog.ShowAsync();

                //MessageBoxResult result =
                //    MessageBox.Show("This app accesses your phone's location. Is that ok?",
                //    "Location",
                //    MessageBoxButton.OKCancel);

                //if (result == MessageBoxResult.OK)
                //{
                //    Windows.Storage.ApplicationData.Current.LocalSettings.Values["LocationConsent"] = true;
                //}
                //else
                //{
                //    Windows.Storage.ApplicationData.Current.LocalSettings.Values["LocationConsent"] = false;
                //}
            }

            return positionGPS;


        }

        private async void geolocator_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            await dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                MessageDialog msgDialogLat = new MessageDialog(args.Position.Coordinate.Latitude.ToString("0.00"));
                msgDialogLat.Commands.Add(new UICommand("Ok"));
                await msgDialogLat.ShowAsync();

                MessageDialog msgDialogLon = new MessageDialog(args.Position.Coordinate.Longitude.ToString("0.00"));
                msgDialogLon.Commands.Add(new UICommand("Ok"));
                await msgDialogLon.ShowAsync();
            });

            //System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
            //{
            //    MessageBox.Show(args.Position.Coordinate.Latitude.ToString("0.00"));
            //    MessageBox.Show(args.Position.Coordinate.Longitude.ToString("0.00"));
            //});
        }

        private async void myGeoLocator_StatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            string status = "";

            switch (args.Status)
            {
                case PositionStatus.Disabled:
                    // the application does not have the right capability or the location master switch is off
                    status = "location is disabled in phone settings";
                    break;
                case PositionStatus.Initializing:
                    // the geolocator started the tracking operation
                    status = "initializing";
                    break;
                case PositionStatus.NoData:
                    // the location service was not able to acquire the location
                    status = "no data";
                    break;
                case PositionStatus.Ready:
                    // the location service is generating geopositions as specified by the tracking parameters
                    status = "ready";
                    break;
                case PositionStatus.NotAvailable:
                    status = "not available";
                    // not used in WindowsPhone, Windows desktop uses this value to signal that there is no hardware capable to acquire location information
                    break;
                case PositionStatus.NotInitialized:
                    // the initial state of the geolocator, once the tracking operation is stopped by the user the geolocator moves back to this state

                    break;
            }

            CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            await dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                MessageDialog msgDialog = new MessageDialog(status);
                msgDialog.Commands.Add(new UICommand("Ok"));
                await msgDialog.ShowAsync();
            });

            //System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
            //{
            //    MessageBox.Show(status);
            //});
        }

        private void SaveLocationToShare(Geoposition location)
        {
            HaloLog.i("SaveLocationToShare() called.");

            if (location != null)
            {
                String latitude = location.Coordinate.Latitude.ToString();
                String longitude = location.Coordinate.Longitude.ToString();

                HaloLog.i("Latitude returned from system: " + latitude);
                HaloLog.i("Longitude returned from system: " + longitude);

                getLocation2(location.Coordinate.Latitude, location.Coordinate.Longitude);
                LocalStorage.setLastKnownLocation(ToJSONString(latitude, longitude, addresses));
            }
            else
            {
                HaloLog.i("Longitude and Latitude were not retrieved from system.");
                LocalStorage.setLastKnownLocation(ToJSONString("0", "0", null));
            }
            HaloLog.i("Device's location saved to share.");
        }

        private string ToJSONString(string latitude, string longitude, List<MapLocation> addresses)
        {
            JSONObject joLoc = new JSONObject();
            try
            {
                DateTime crtTime = new DateTime();
                joLoc.Add("lastKnownTime", crtTime.TimeOfDay.ToString());
                joLoc.Add("longitude", longitude);
                joLoc.Add("latitude", latitude);

                if (addresses != null)
                {

                    String LocationNotAvailableMsg = "";
                    String NotAvailablecurrently = "Not available currently";

                    String currentAddress = "";
                    String defaultStr = null;

                    String city = defaultStr;
                    String state = defaultStr;
                    String country = defaultStr;
                    String countryCode = defaultStr;
                    String streetAddress1 = defaultStr;
                    String streetAddress2 = defaultStr;
                    String postalCode = defaultStr;

                    foreach (MapLocation addr in addresses)
                    {
                        // Get current city.
                        city = addr.Address.Town;
                        if (city == null || city.Contains(NotAvailablecurrently))
                            city = LocationNotAvailableMsg;

                        // Get current state.
                        state = addr.Address.Region;
                        if (state == null || state.Contains(NotAvailablecurrently))
                            state = LocationNotAvailableMsg;

                        // Get current country.
                        country = addr.Address.Country;
                        if (country == null || country.Contains(NotAvailablecurrently))
                            country = LocationNotAvailableMsg;

                        // Get the current country code.
                        countryCode = addr.Address.CountryCode;
                        if (countryCode == null || countryCode.Contains(NotAvailablecurrently))
                            countryCode = LocationNotAvailableMsg;

                        // Get current street address.
                        streetAddress1 = addr.Address.Street;
                        if (streetAddress1 == null || streetAddress1.Contains(NotAvailablecurrently))
                            streetAddress1 = LocationNotAvailableMsg;

                        // Get alternate version of the current street address.
                        streetAddress2 = addr.Address.Street;
                        if (streetAddress2 == null || streetAddress2.Contains(NotAvailablecurrently))
                            streetAddress2 = LocationNotAvailableMsg;

                        // Get current postal code.
                        postalCode = addr.Address.PostCode;
                        if (postalCode == null || postalCode.Contains(NotAvailablecurrently))
                            postalCode = LocationNotAvailableMsg;
                    }

                    joLoc.Add("countryCode", countryCode);
                    joLoc.Add("countryName", country);
                    joLoc.Add("formattedAddress", streetAddress1);
                    joLoc.Add("state", state);
                    joLoc.Add("city", city);
                    joLoc.Add("zipCode", postalCode);

                    ////				joLoc.accumulate("providerName", getProviderName());
                    ////				joLoc.accumulate("isBestProvider", getIsBestProvider());
                    //						joLoc.accumulate("gpsStatus", getGpsStatus());
                }
            }
            catch (Exception e)
            {
                HaloLog.e(e.StackTrace);
            }
            return joLoc.ToString();
        }

        //address from lat long
        public async void getLocation2(double latitude, double longitude)
        {
            BasicGeoposition myLocation = new BasicGeoposition
            {
                Longitude = -77.036,
                Latitude = 38.895
            };

            Geopoint pointToReverseGeocode = new Geopoint(myLocation);

            MapLocationFinderResult result = await MapLocationFinder.FindLocationsAtAsync(pointToReverseGeocode);

            if (result.Status == MapLocationFinderStatus.Success)
            {
                if (result.Locations.Any())
                    addresses = result.Locations as List<MapLocation>;
            }

            //  List<MapLocation> locations = null;
            //ReverseGeocodeQuery query = new ReverseGeocodeQuery();
            //query.GeoCoordinate = new GeoCoordinate(38.895, -77.036);
            //query.QueryCompleted += (s, e) =>
            //{
            //    if (e.Error == null && e.Result.Count > 0)
            //    {
            //        addresses = e.Result as List<MapLocation>;
            //        // Do whatever you want with returned locations. 
            //        // e.g. MapAddress address = locations[0].Information.Address;
            //    }
            //};
            //query.QueryAsync();
        }


        private bool isDeviceOnlyEthernetCapableForConnection()
        {

            HaloLog.i("isDeviceOnlyEthernetCapableForConnection() called.");


            var profile = NetworkInformation.GetInternetConnectionProfile();
            if (profile != null)
            {
                var interfaceType = profile.NetworkAdapter.IanaInterfaceType;
                return interfaceType == 6;
            }
            return false;

        }

    }
}
