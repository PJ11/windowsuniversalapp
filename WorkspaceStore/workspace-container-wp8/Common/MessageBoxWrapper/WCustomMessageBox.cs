﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Microsoft.Phone.Controls;

namespace Common
{
    public class WCustomMessageBox : CustomMessageBox
    {
        private Grid mGrid;

        public bool IsLightDismissEnabled { get; set; }

        public bool IsCallFromPopup { get; set; }

        public WCustomMessageBox()
            : base()
        {
            IsLightDismissEnabled = true;
            DefaultStyleKey = typeof(WCustomMessageBox);
        }

        /// <summary>
        /// Reveals the message box by inserting it into a popup and opening it.
        /// </summary>
        public new void Show()
        {
            base.Show();

            if (IsCallFromPopup)
            {
                PhoneApplicationFrame frame = Application.Current.RootVisual as PhoneApplicationFrame;
                if (frame != null)
                {
                    PhoneApplicationPage page = frame.Content as PhoneApplicationPage;

                    if (page != null && page.ApplicationBar != null)
                    {
                        // Hide Application bar
                        page.ApplicationBar.IsVisible = false;

                        var prop = GetInstance(typeof(CustomMessageBox), "_hasApplicationBar");
                        if (prop != null) prop.SetValue(this, true);
                    }
                }
            }

            if (IsLightDismissEnabled)
            {
                // Focus message box
                this.Focus();

                mGrid = GetInstanceField(typeof(CustomMessageBox), this, "_container") as Grid;
                if (mGrid != null) mGrid.LostFocus += MGrid_LostFocus;
            }
        }

        /// <summary>
        /// Close message box on grid lost focus
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event information.</param>
        void MGrid_LostFocus(object sender, RoutedEventArgs e)
        {
            if (mGrid != null) mGrid.LostFocus -= MGrid_LostFocus;
            Dismiss();
        }

        /// <summary>
        /// Uses reflection to get the field value from an object.
        /// </summary>
        ///
        /// <param name="type">The instance type.</param>
        /// <param name="instance">The instance object.</param>
        /// <param name="fieldName">The field's name which is to be fetched.</param>
        ///
        /// <returns>The field value from the object.</returns>
        internal static object GetInstanceField(Type type, object instance, string fieldName)
        {
            BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic
                | BindingFlags.Static;
            FieldInfo field = type.GetField(fieldName, bindFlags);
            return field.GetValue(instance);
        }

        /// <summary>
        /// Uses reflection to get the field from an object.
        /// </summary>
        ///
        /// <param name="type">The instance type.</param>
        /// <param name="instance">The instance object.</param>
        /// <param name="fieldName">The field's name which is to be fetched.</param>
        ///
        /// <returns>The field value from the object.</returns>
        internal static FieldInfo GetInstance(Type type, string fieldName)
        {
            BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic
                | BindingFlags.Static;
            FieldInfo field = type.GetField(fieldName, bindFlags);
            return field;
        }
    }
}