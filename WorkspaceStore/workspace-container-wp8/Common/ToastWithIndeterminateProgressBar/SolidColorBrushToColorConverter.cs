﻿///////////////////////////////////////////////////////////////////////////////
// SolidColorBrushToColorConverter.cs
// Copied from Coding4Fun.Toolkit.Controls v2.0.7
// Microsoft License (https://coding4fun.codeplex.com/license)
///////////////////////////////////////////////////////////////////////////////
using System;
using System.Globalization;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace Common.ToastWithIndeterminateProgressBar
{
    public abstract class ValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Convert(value, targetType, parameter, culture, null);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ConvertBack(value, targetType, parameter, culture, null);
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return Convert(value, targetType, parameter, null, language);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return ConvertBack(value, targetType, parameter, null, language);
        }

        public virtual object Convert(object value, Type targetType, object parameter, CultureInfo culture, string language)
        {
            throw new NotImplementedException();
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class SolidColorBrushToColorConverter : ValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture, string language)
        {
            var brush = value as SolidColorBrush;

            if (brush != null)
                return brush.Color;

            return Colors.Transparent;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture, string language)
        {
            throw new NotImplementedException();
        }
    }
}
