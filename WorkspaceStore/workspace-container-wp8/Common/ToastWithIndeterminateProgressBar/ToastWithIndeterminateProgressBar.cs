﻿///////////////////////////////////////////////////////////////////////////////
// ToastPrompt.cs
// Copied from Coding4Fun.Toolkit.Controls v2.0.7
// Microsoft License (https://coding4fun.codeplex.com/license)
///////////////////////////////////////////////////////////////////////////////
using System;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Windows.UI.Core;
using Windows.UI.Input;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace Common.ToastWithIndeterminateProgressBar
{
    public interface IImageSource
    {
        Stretch Stretch { get; set; }
        ImageSource ImageSource { get; set; }
    }

    public interface IImageSourceFull : IImageSource
    {
        double ImageWidth { get; set; }
        double ImageHeight { get; set; }
    }

    public sealed class ToastWithIndeterminateProgressBar : PopUp<string, PopUpResult>, IDisposable, IImageSourceFull
    {
        protected Image ToastImage;
        private const string ToastImageName = "ToastImage";
        private Timer _timer;

        private TranslateTransform _translate;

        public ToastWithIndeterminateProgressBar(bool userDismissable = true)
        {
            DefaultStyleKey = typeof(ToastWithIndeterminateProgressBar);

            IsAppBarVisible = true;
            IsBackKeyOverride = true;
            IsCalculateFrameVerticalOffset = true;

            IsOverlayApplied = false;

            AnimationType = DialogService.AnimationTypes.SlideHorizontal;

            if (userDismissable)
            {
                ManipulationStarted += ToastPromptManipulationStarted;
                ManipulationDelta += ToastPromptManipulationDelta;
                ManipulationCompleted += ToastPromptManipulationCompleted;
            }

            Opened += ToastPromptOpened;
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            SetRenderTransform();

            ToastImage = GetTemplateChild(ToastImageName) as Image;

            if (ToastImage != null && ImageSource != null)
            {
                ToastImage.Source = ImageSource;
                SetImageVisibility(ImageSource);
            }

            SetTextOrientation(TextWrapping);
        }

        public override void Show()
        {
            if (!IsTimerEnabled)
                return;

            base.Show();

            SetRenderTransform();
            PreventScrollBinding.SetIsEnabled(this, true);
        }

        public void Dispose()
        {
            if (_timer != null)
            {
                _timer.Dispose();
                _timer = null;
            }
        }

        #region Control Events
        void ToastPromptManipulationStarted(object sender, Windows.UI.Xaml.Input.ManipulationStartedRoutedEventArgs e)
        {
            PauseTimer();
        }

        void ToastPromptManipulationDelta(object sender, Windows.UI.Xaml.Input.ManipulationDeltaRoutedEventArgs e)
        {
            _translate.X += e.Delta.Translation.X;

            if (_translate.X < 0)
                _translate.X = 0;
        }

        void ToastPromptManipulationCompleted(object sender, Windows.UI.Xaml.Input.ManipulationCompletedRoutedEventArgs e)
        {
            if (e.Cumulative.Translation.X > 200 || e.Velocities.Linear.X > 1000)
            {
                OnCompleted(new PopUpEventArgs<string, PopUpResult> { PopUpResult = PopUpResult.UserDismissed });
            }
            else if (e.Cumulative.Translation.X < 20)
            {
                OnCompleted(new PopUpEventArgs<string, PopUpResult> { PopUpResult = PopUpResult.Ok });
            }
            else
            {
                _translate.X = 0;
                StartTimer();
            }
        }

        void ToastPromptOpened(object sender, EventArgs e)
        {
            StartTimer();
        }

        async void TimerTick(object state)
        {
            CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
             {
                 OnCompleted(new PopUpEventArgs<string, PopUpResult> { PopUpResult = PopUpResult.NoResponse });
             });

            //Dispatcher.BeginInvoke(() => OnCompleted(new PopUpEventArgs<string, PopUpResult> { PopUpResult = PopUpResult.NoResponse }));
        }

        public override void OnCompleted(PopUpEventArgs<string, PopUpResult> result)
        {
            PreventScrollBinding.SetIsEnabled(this, false);

            PauseTimer();
            Dispose();

            base.OnCompleted(result);
        }
        #endregion

        #region helper methods
        private void SetImageVisibility(ImageSource source)
        {
            ToastImage.Visibility = (source == null) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void SetTextOrientation(TextWrapping value)
        {
            if (value == TextWrapping.Wrap)
            {
                TextOrientation = Orientation.Vertical;
            }
        }

        private void StartTimer()
        {
            if (_timer == null)
                _timer = new Timer(TimerTick, null, TimeSpan.FromMilliseconds(MillisecondsUntilHidden), TimeSpan.FromMilliseconds(-1));

            _timer.Change(TimeSpan.FromMilliseconds(MillisecondsUntilHidden), TimeSpan.FromMilliseconds(-1));
        }

        private void PauseTimer()
        {
            if (_timer != null)
                _timer.Change(TimeSpan.FromMilliseconds(-1), TimeSpan.FromMilliseconds(-1));
        }

        private void SetRenderTransform()
        {
            _translate = new TranslateTransform();
            RenderTransform = _translate;
        }
        #endregion

        #region Dependency Property Callbacks
        private static void OnTextWrapping(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var sender = o as ToastWithIndeterminateProgressBar;

            if (sender == null || sender.ToastImage == null)
                return;

            sender.SetTextOrientation((TextWrapping)e.NewValue);
        }

        private static void OnImageSource(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var sender = o as ToastWithIndeterminateProgressBar;

            if (sender == null || sender.ToastImage == null)
                return;

            sender.SetImageVisibility(e.NewValue as ImageSource);
        }
        #endregion

        #region Dependency Properties / Properties

        public int MillisecondsUntilHidden
        {
            get { return (int)GetValue(MillisecondsUntilHiddenProperty); }
            set { SetValue(MillisecondsUntilHiddenProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MillisecondsUntilHidden.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MillisecondsUntilHiddenProperty =
            DependencyProperty.Register("MillisecondsUntilHidden", typeof(int), typeof(ToastWithIndeterminateProgressBar), new PropertyMetadata(4000));

        public bool IsTimerEnabled
        {
            get { return (bool)GetValue(IsTimerEnabledProperty); }
            set { SetValue(IsTimerEnabledProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsTimerEnabled.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsTimerEnabledProperty =
            DependencyProperty.Register("IsTimerEnabled", typeof(bool), typeof(ToastWithIndeterminateProgressBar), new PropertyMetadata(true));

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(ToastWithIndeterminateProgressBar), new PropertyMetadata(""));


        public string SubTitle
        {
            get { return (string)GetValue(SubTitleProperty); }
            set { SetValue(SubTitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SubTitle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SubTitleProperty =
            DependencyProperty.Register("SubTitle", typeof(string), typeof(ToastWithIndeterminateProgressBar), new PropertyMetadata(""));

        public ImageSource ImageSource
        {
            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImageSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(ToastWithIndeterminateProgressBar),
            new PropertyMetadata(new PropertyChangedCallback(OnImageSource)));

        public Stretch Stretch
        {
            get { return (Stretch)GetValue(StretchProperty); }
            set { SetValue(StretchProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Stretch.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StretchProperty =
            DependencyProperty.Register("Stretch", typeof(Stretch), typeof(ToastWithIndeterminateProgressBar),
            new PropertyMetadata(Stretch.None));

        public double ImageWidth
        {
            get { return (double)GetValue(ImageWidthProperty); }
            set { SetValue(ImageWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImageWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageWidthProperty =
            DependencyProperty.Register("ImageWidth", typeof(double), typeof(ToastWithIndeterminateProgressBar), new PropertyMetadata(double.NaN));

        public double ImageHeight
        {
            get { return (double)GetValue(ImageHeightProperty); }
            set { SetValue(ImageHeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ImageWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageHeightProperty =
            DependencyProperty.Register("ImageHeight", typeof(double), typeof(ToastWithIndeterminateProgressBar), new PropertyMetadata(double.NaN));

        public Orientation TextOrientation
        {
            get { return (Orientation)GetValue(TextOrientationProperty); }
            set { SetValue(TextOrientationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextOrientation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextOrientationProperty =
            DependencyProperty.Register("TextOrientation", typeof(Orientation), typeof(ToastWithIndeterminateProgressBar), new PropertyMetadata(Orientation.Horizontal));

        public TextWrapping TextWrapping
        {
            get { return (TextWrapping)GetValue(TextWrappingProperty); }
            set { SetValue(TextWrappingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextWrapping.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextWrappingProperty =
            DependencyProperty.Register("TextWrapping", typeof(TextWrapping), typeof(ToastWithIndeterminateProgressBar), new PropertyMetadata(TextWrapping.NoWrap, OnTextWrapping));

        #endregion
    }
}