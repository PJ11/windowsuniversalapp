﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Common.Helpers
{
    /* Use TextValueChanged method instead of TextChanged */

    /// <summary>
    /// Represent a textbox control that provides a value
    /// when typing is stopped on textbox
    /// </summary>
    public class CustomDelayTextBox : TextBox
    {
        /// <summary>
        /// Gets or sets the DispatcherTimer used for the MinimumValueChangeDelay 
        /// condition for auto completion.
        /// </summary>
        private DispatcherTimer _delayTimer;

        private TextChangedEventArgs _textBoxArgs;

        /// <summary>
        /// Occurs when content changes in the text box.
        /// </summary>
        public event TextChangedEventHandler TextValueChanged;

        #region public int MinimumValueChangeDelay
        /// <summary>
        /// Gets or sets the minimum delay, in milliseconds, after text is typed
        /// in the text box before the
        /// <see cref="T:Common.Helpers.CustomDelayTextBox" /> control
        /// populates the list of possible matches in the drop-down.
        /// </summary>
        /// <value>The minimum delay, in milliseconds, after text is typed in
        /// the text box, but before the
        /// <see cref="T:Common.Helpers.CustomDelayTextBox" /> populates
        /// the list of possible matches in the drop-down. The default is 0.</value>
        /// <exception cref="T:System.ArgumentException">The set value is less than 0.</exception>
        public int MinimumValueChangeDelay
        {
            get { return (int)GetValue(MinimumValueChangeDelayProperty); }
            set { SetValue(MinimumValueChangeDelayProperty, value); }
        }

        /// <summary>
        /// Identifies the
        /// <see cref="P:Microsoft.Phone.Controls.CustomDelayTextBox.MinimumValueChangeDelay" />
        /// dependency property.
        /// </summary>
        /// <value>The identifier for the
        /// <see cref="P:Microsoft.Phone.Controls.CustomDelayTextBox.MinimumValueChangeDelay" />
        /// dependency property.</value>
        public static readonly DependencyProperty MinimumValueChangeDelayProperty =
            DependencyProperty.Register(
                "MinimumValueChangeDelay",
                typeof(int),
                typeof(CustomDelayTextBox),
                new PropertyMetadata(null, OnMinimumValueChangeDelayPropertyChanged));

        /// <summary>
        /// MinimumValueChangeDelayProperty property changed handler. Any current 
        /// dispatcher timer will be stopped. The timer will not be restarted 
        /// until the next TextUpdate call by the user.
        /// </summary>
        /// <param name="d">AutoCompleteTextBox that changed its 
        /// MinimumValueChangeDelay.</param>
        /// <param name="e">Event arguments.</param>
        [SuppressMessage("Microsoft.Usage", "CA2208:InstantiateArgumentExceptionsCorrectly", Justification = "The exception is most likely to be called through the CLR property setter.")]
        private static void OnMinimumValueChangeDelayPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CustomDelayTextBox source = d as CustomDelayTextBox;

            int newValue = (int)e.NewValue;
            if (newValue < 0)
            {
                d.SetValue(e.Property, e.OldValue);
            }

            // Stop any existing timer
            if (source._delayTimer != null)
            {
                source._delayTimer.Stop();

                if (newValue == 0)
                {
                    source._delayTimer = null;
                }
            }

            // Create or clear a dispatcher timer instance
            if (newValue > 0 && source._delayTimer == null)
            {
                source._delayTimer = new DispatcherTimer();
                source._delayTimer.Tick += source.TextBoxValueChanged;
            }

            // Set the new tick interval
            if (newValue > 0 && source._delayTimer != null)
            {
                source._delayTimer.Interval = TimeSpan.FromMilliseconds(newValue);
            }
        }

        #endregion public int MinimumValueChangeDelay

        public void TextBoxValueChanged(object sender, object e)
        {
            _delayTimer.Stop();

            if (TextValueChanged != null)
            {
                TextValueChanged(this, _textBoxArgs);
            }
        }

        protected override void OnApplyTemplate()
        {
            this.TextChanged -= OnTextChanged;

            base.OnApplyTemplate();

            this.TextChanged += OnTextChanged;
        }

        private void OnTextChanged(object sender, TextChangedEventArgs textChangedEventArgs)
        {
            _textBoxArgs = textChangedEventArgs;

            if (_delayTimer != null)
            {
                _delayTimer.Start();
            }
            else
            {
                if (TextValueChanged != null)
                {
                    TextValueChanged(this, _textBoxArgs);
                }
            }
        }
    }
}