﻿using System;
using System.ComponentModel;
using System.Windows;
using Windows.Foundation;
using Windows.Graphics.Display;

using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

namespace Common.Helpers
{
    public enum PopupTextBlockSelectorResult
    {
        TextBlockOne,
        TextBlockTwo,
        TextBlockThree,
        None
    }

    public class PopupTextBlockSelectorDismissingEventArgs : CancelEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the DismissingEventArgs class.
        /// </summary>
        /// <param name="result">The result value.</param>
        public PopupTextBlockSelectorDismissingEventArgs(PopupTextBlockSelectorResult result)
        {
            Result = result;
        }

        /// <summary>
        /// Gets or sets the result value.
        /// </summary>
        public PopupTextBlockSelectorResult Result { get; private set; }
    }

    public class PopupTextBlockSelectorDismissedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the DismissingEventArgs class.
        /// </summary>
        /// <param name="result">The result value.</param>
        public PopupTextBlockSelectorDismissedEventArgs(PopupTextBlockSelectorResult result)
        {
            Result = result;
        }

        /// <summary>
        /// Gets or sets the result value.
        /// </summary>
        public PopupTextBlockSelectorResult Result { get; private set; }
    }

    [TemplatePart(Name = TextBlockOne, Type = typeof(TextBlock))]
    [TemplatePart(Name = TextBlockTwo, Type = typeof(TextBlock))]
    [TemplatePart(Name = TextBlockThree, Type = typeof(TextBlock))]
    public class PopupTextBlockSelector : Control
    {
        /// <summary>
        /// Holds a weak reference to the message box that is currently displayed.
        /// </summary>
        private static WeakReference _currentInstance = null;

        /// <summary>
        /// The current screen width.
        /// </summary>
        private double _screenWidth = Window.Current.Bounds.Width;

        /// <summary>
        /// The current screen height.
        /// </summary>
        private double _screenHeight = Window.Current.Bounds.Height;

        /// <summary>
        /// The height of the system tray in pixels when the page
        /// is in portrait mode.
        /// </summary>
        private const double _systemTrayHeightInPortrait = 32.0;

        /// <summary>
        /// The width of the system tray in pixels when the page
        /// is in landscape mode.
        /// </summary>
        private const double _systemTrayWidthInLandscape = 72.0;

        /// <summary>
        /// Text block one template part name.
        /// </summary>
        private const string TextBlockOne = "TextBlockOne";

        /// <summary>
        /// Text block two template part name.
        /// </summary>
        private const string TextBlockTwo = "TextBlockTwo";

        /// <summary>
        /// Text block three template part name.
        /// </summary>
        private const string TextBlockThree = "TextBlockThree";

        /// <summary>
        /// Identifies whether the application bar and the system tray
        /// must be restored after the message box is dismissed. 
        /// </summary>
        private static bool _mustRestore = true;

        /// <summary>
        /// Text block one template part.
        /// </summary>
        private TextBlock _textBlockOne;

        /// <summary>
        /// Text block two template part.
        /// </summary>
        private TextBlock _textBlockTwo;

        /// <summary>
        /// Text block three template part.
        /// </summary>
        private TextBlock _textBlockThree;

        /// <summary>
        /// The popup used to display the message box.
        /// </summary>
        private Popup _popup;

        /// <summary>
        /// The child container of the popup.
        /// </summary>
        private Grid _container;

        /// <summary>
        /// The root visual of the application.
        /// </summary>
        private Frame _frame;

        /// <summary>
        /// The current application page.
        /// </summary>
        private Page _page;

        /// <summary>
        /// Identifies whether the application bar is visible or not before
        /// opening the message box.
        /// </summary>
        private bool _hasApplicationBar;

        /// <summary>
        /// The current color of the system tray.
        /// </summary>
        private Color _systemTrayColor;

        /// <summary>
        /// Whether the message box is currently in the process of being 
        /// dismissed.
        /// </summary>
        private bool _isBeingDismissed = false;

        public bool ShowOtherPopup { get; set; }

        /// <summary>
        /// Called when the message box is about to be dismissed.
        /// </summary>
        public event EventHandler<PopupTextBlockSelectorDismissingEventArgs> Dismissing;

        /// <summary>
        /// Called when the message box is dismissed.
        /// </summary>
        public event EventHandler<PopupTextBlockSelectorDismissedEventArgs> Dismissed;

        #region TextBlockOneContent DependencyProperty

        /// <summary>
        /// Gets or sets textblock one.
        /// </summary>
        public string TextBlockOneContent
        {
            get { return (string)GetValue(TextBlockOneContentProperty); }
            set { SetValue(TextBlockOneContentProperty, value); }
        }

        /// <summary>
        /// Identifies the TextBlockOneContent dependency property.
        /// </summary>
        public static readonly DependencyProperty TextBlockOneContentProperty =
            DependencyProperty.Register("TextBlockOneContent", typeof(string), typeof(PopupTextBlockSelector), new PropertyMetadata(string.Empty, OnTextBlockOneContentPropertyChanged));

        /// <summary>
        /// Modifies the TextBlockOneContent visiblity depending
        /// on whether it should show or not.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        private static void OnTextBlockOneContentPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PopupTextBlockSelector target = (PopupTextBlockSelector)obj;

            if (target._textBlockOne != null)
            {
                string value = (string)e.NewValue;
                target._textBlockOne.Visibility = GetVisibilityFromObject(value);
            }
        }

        #endregion

        #region TextBlockTwoContent DependencyProperty

        public string TextBlockTwoContent
        {
            get { return (string)GetValue(TextBlockTwoContentProperty); }
            set { SetValue(TextBlockTwoContentProperty, value); }
        }

        public static readonly DependencyProperty TextBlockTwoContentProperty =
            DependencyProperty.Register("TextBlockTwoContent", typeof(string), typeof(PopupTextBlockSelector), new PropertyMetadata(string.Empty, OnTextBlockTwoContentPropertyChanged));

        private static void OnTextBlockTwoContentPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PopupTextBlockSelector target = (PopupTextBlockSelector)obj;

            if (target._textBlockTwo != null)
            {
                string value = (string)e.NewValue;
                target._textBlockTwo.Visibility = GetVisibilityFromString(value);
            }
        }

        #endregion

        #region TextBlockThreeContent DependencyProperty

        public string TextBlockThreeContent
        {
            get { return (string)GetValue(TextBlockThreeContentProperty); }
            set { SetValue(TextBlockThreeContentProperty, value); }
        }

        public static readonly DependencyProperty TextBlockThreeContentProperty =
            DependencyProperty.Register("TextBlockThreeContent", typeof(string), typeof(PopupTextBlockSelector), new PropertyMetadata(string.Empty, OnTextBlockThreeContentPropertyChanged));

        private static void OnTextBlockThreeContentPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PopupTextBlockSelector target = (PopupTextBlockSelector)obj;

            if (target._textBlockThree != null)
            {
                string value = (string)e.NewValue;
                target._textBlockThree.Visibility = GetVisibilityFromObject(value);
            }
        }

        #endregion

#if WP_80_SILVERLIGHT
        /// <summary>
        /// Called when the back key is pressed. This event handler cancels
        /// the backward navigation and dismisses the message box.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event information.</param>
        private void OnBackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Dismiss(PopupTextBlockSelectorResult.None, true);
        }
#else
        /// <summary>
        /// Called when the back key is pressed. This event handler cancels
        /// the backward navigation and dismisses the message box.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event information.</param>
        /// 
        #if WP_80_SILVERLIGHT
        void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            Dismiss(PopupTextBlockSelectorResult.None, true);
        }
#endif
#endif

        /// <summary>
        /// Called when the application frame is navigating.
        /// This event handler dismisses the message box.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event information.</param>
        private void OnNavigating(object sender, NavigatingCancelEventArgs e)
        {
            Dismiss(PopupTextBlockSelectorResult.None, false);
        }

        /// <summary>
        /// Gets the template parts and attaches event handlers.
        /// Animates the message box when the template is applied to it.
        /// </summary>
        protected override void OnApplyTemplate()
        {
            if (_textBlockOne != null)
            {
                _textBlockOne.Tapped -= TextBlockOne_Tap;
            }

            if (_textBlockTwo != null)
            {
                _textBlockTwo.Tapped -= TextBlockTwo_Tap;
            }

            if (_textBlockThree != null)
            {
                _textBlockThree.Tapped -= TextBlockThree_Tap;
            }

            base.OnApplyTemplate();

            _textBlockOne = base.GetTemplateChild(TextBlockOne) as TextBlock;
            _textBlockTwo = base.GetTemplateChild(TextBlockTwo) as TextBlock;
            _textBlockThree = base.GetTemplateChild(TextBlockThree) as TextBlock;

            if (_textBlockOne != null)
            {
                _textBlockOne.Visibility = GetVisibilityFromObject(TextBlockOneContent);
                _textBlockOne.Tapped += TextBlockOne_Tap;
            }

            if (_textBlockTwo != null)
            {
                _textBlockTwo.Visibility = GetVisibilityFromObject(TextBlockTwoContent);
                _textBlockTwo.Tapped += TextBlockTwo_Tap;
            }

            if (_textBlockThree != null)
            {
                _textBlockThree.Visibility = GetVisibilityFromObject(TextBlockThreeContent);
                _textBlockThree.Tapped += TextBlockThree_Tap;
            }
        }

        /// <summary>
        /// Initializes a new instance of the PopupTextBlockSelector class.
        /// </summary>
        public PopupTextBlockSelector()
            : base()
        {
            DefaultStyleKey = typeof(PopupTextBlockSelector);
        }

        /// <summary>
        /// Reveals the PopupTextBlockSelector by inserting it into a popup and opening it.
        /// </summary>
        public void Show()
        {
            if (_popup != null)
            {
                if (_popup.IsOpen)
                {
                    return;
                }
            }

            LayoutUpdated += PopUpSelectorBox_LayoutUpdated;

            _frame = Window.Current.Content as Frame;
            _page = _frame.Content as Page;

            // Hide the application bar if necessary.
            if (_page.BottomAppBar != null)
            {
                // Cache the original visibility of the system tray.
                _hasApplicationBar = _page.BottomAppBar.Visibility == Visibility.Visible;

                // Hide it.
                if (_hasApplicationBar)
                {
                    _page.BottomAppBar.Visibility = Visibility.Collapsed;
                    _page.Margin = new Thickness(0, 0, 0, 72);
                }
            }
            else
            {
                _hasApplicationBar = false;
            }

            // Dismiss the current message box if there is any.
            try
            {
                if (_currentInstance != null)
                {
                    _mustRestore = false;

                    PopupTextBlockSelector target = _currentInstance.Target as PopupTextBlockSelector;

                    if (target != null)
                    {
                        target.Dismiss();
                    }
                }
            }
            catch (Exception)
            {

            }

            // Insert the overlay.
            Rectangle overlay = new Rectangle();
            Color backgroundColor = (Color)Application.Current.Resources["PhoneBackgroundColor"];
            overlay.Fill = new SolidColorBrush(Color.FromArgb(0x99, backgroundColor.R, backgroundColor.G, backgroundColor.B));
            overlay.Tapped += overlay_Tap;
            _container = new Grid();
            _container.Children.Add(overlay);

            _container.Children.Add(this);

            _screenHeight = Window.Current.Bounds.Height;
            _screenWidth = Window.Current.Bounds.Width;

            // Create and open the popup.
            _popup = new Popup();
            _popup.Child = _container;
            SetSizeAndOffset();
            _popup.IsOpen = true;

            _currentInstance = new WeakReference(this);

            // Attach event handlers.
            #if WP_80_SILVERLIGHT
            if (_page != null)
            {
                HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            }
#endif
            if (_frame != null)
            {
                _frame.Navigating += OnNavigating;
            }
        }

        void overlay_Tap(object sender, TappedRoutedEventArgs e)
        {
            Dismiss(PopupTextBlockSelectorResult.None, true);
        }

        public void Hide()
        {
            this._popup.IsOpen = false;
        }

        /// <summary>
        /// Dismisses the message box.
        /// </summary>
        public void Dismiss()
        {
            Dismiss(PopupTextBlockSelectorResult.None, true);
        }

        /// <summary>
        /// Dismisses the PopupTextBlockSelector.
        /// </summary>
        /// <param name="source">
        /// The source that caused the dismission.
        /// </param>
        /// <param name="useTransition">
        /// If true, the PopupTextBlockSelector is dismissed after swiveling
        /// backward and out.
        /// </param>
        private void Dismiss(PopupTextBlockSelectorResult source, bool useTransition)
        {
            // Ensure only a single Dismiss is being handled at a time
            if (_isBeingDismissed)
            {
                return;
            }
            _isBeingDismissed = true;

            // Handle the dismissing event.
            var handlerDismissing = Dismissing;
            if (handlerDismissing != null)
            {
                PopupTextBlockSelectorDismissingEventArgs args = new PopupTextBlockSelectorDismissingEventArgs(source);
                handlerDismissing(this, args);

                if (args.Cancel)
                {
                    _isBeingDismissed = false;
                    return;
                }
            }

            // Handle the dismissed event.
            var handlerDismissed = Dismissed;
            if (handlerDismissed != null)
            {
                PopupTextBlockSelectorDismissedEventArgs args = new PopupTextBlockSelectorDismissedEventArgs(source);
                handlerDismissed(this, args);
            }

            // Set the current instance to null.
            _currentInstance = null;

            // Cache this variable to avoid a race condition.
            bool restoreOriginalValues = _mustRestore;

            // Close popup.
            if (useTransition)
            {
#if WP_80_SILVERLIGHT
                SwivelTransition backwardOut = new SwivelTransition { Mode = SwivelTransitionMode.BackwardOut };
                ITransition swivelTransition = backwardOut.GetTransition(this);
                swivelTransition.Completed += (s, e) =>
                {
                    swivelTransition.Stop();
                    ClosePopup(restoreOriginalValues);
                };
                swivelTransition.Begin();
#endif
                ClosePopup(restoreOriginalValues);
            }
            else
            {
                ClosePopup(restoreOriginalValues);
            }

            _isBeingDismissed = false;
        }

        /// <summary>
        /// Closes the pop up.
        /// </summary>
        private void ClosePopup(bool restoreOriginalValues)
        {
            // Remove the popup.
            if (_popup != null)
            {
                _popup.IsOpen = false;
                _popup = null;
            }

            // If there is no other PopupTextBlockSelector displayed.  
            if (restoreOriginalValues)
            {
                // Set the system tray back to its original 
                // color if necessary.

                // Bring the application bar if necessary.
                if (_hasApplicationBar)
                {
                    _hasApplicationBar = false;

                    // Application bar can be nulled during the Dismissed event
                    // so a null check needs to be performed here.
                    if (_page.BottomAppBar != null && !ShowOtherPopup)
                    {
                        _page.BottomAppBar.Visibility = Visibility.Visible;
                    }

                    _page.Margin = new Thickness(0, 0, 0, 0);
                }
            }

            // Dettach event handlers.
            if (_page != null)
            {
                #if WP_80_SILVERLIGHT
                HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
#endif
                _page = null;
            }

            if (_frame != null)
            {
                _frame.Navigating -= OnNavigating;
                _frame = null;
            }
        }

        /// <summary>
        /// Called when the visual layout changes.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event information.</param>
        private void PopUpSelectorBox_LayoutUpdated(object sender, object o)
        {
#if WP_80_SILVERLIGHT
            SwivelTransition backwardIn = new SwivelTransition { Mode = SwivelTransitionMode.BackwardIn };
            ITransition swivelTransition = backwardIn.GetTransition(this);
            swivelTransition.Completed += (s1, e1) => swivelTransition.Stop();
            swivelTransition.Begin();
#endif
            LayoutUpdated -= PopUpSelectorBox_LayoutUpdated;
        }

        /// <summary>
        /// Dismisses the popup box with the textblockone.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event information.</param>
        private void TextBlockOne_Tap(object sender, RoutedEventArgs e)
        {
            Dismiss(PopupTextBlockSelectorResult.TextBlockOne, true);
        }

        /// <summary>
        /// Dismisses the popup box with the textblock two.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event information.</param>
        private void TextBlockTwo_Tap(object sender, RoutedEventArgs e)
        {
            Dismiss(PopupTextBlockSelectorResult.TextBlockTwo, true);
        }

        /// <summary>
        /// Dismisses the popup box with the textblock three.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event information.</param>
        private void TextBlockThree_Tap(object sender, RoutedEventArgs e)
        {
            Dismiss(PopupTextBlockSelectorResult.TextBlockThree, true);
        }

#if WP_80_SILVERLIGHT
        /// <summary>
        /// Called when the current page changes orientation.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event information.</param>
        private void OnOrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            SetSizeAndOffset();
        }
#endif

        /// <summary>
        /// Sets The vertical and horizontal offset of the popup,
        /// as well as the size of its child container.
        /// </summary>
        private void SetSizeAndOffset()
        {
            // Set the size the container.
            Rect client = GetTransformedRect();
            if (_container != null)
            {
                _container.RenderTransform = GetTransform();

                _container.Width = client.Width;
                _container.Height = client.Height;
            }

            // Set the vertical and horizontal offset of the popup.
#if WP_80_SILVERLIGHT
            if (SystemTray.IsVisible && _popup != null)
            {
                PageOrientation orientation = GetPageOrientation();

                switch (orientation)
                {
                    case PageOrientation.PortraitUp:
                        _popup.HorizontalOffset = 0.0;
                        _popup.VerticalOffset = _systemTrayHeightInPortrait;
                        _container.Height -= _systemTrayHeightInPortrait;
                        break;
                    case PageOrientation.LandscapeLeft:
                        _popup.HorizontalOffset = 0.0;
                        _popup.VerticalOffset = _systemTrayWidthInLandscape;
                        break;
                    case PageOrientation.LandscapeRight:
                        _popup.HorizontalOffset = 0.0;
                        _popup.VerticalOffset = 0.0;
                        break;
                }
            }
#endif
        }

        /// <summary>
        /// Gets a rectangle that occupies the entire page.
        /// </summary>
        /// <returns>The width, height and location of the rectangle.</returns>
        private Rect GetTransformedRect()
        {
            bool isLandscape = IsLandscape(GetPageOrientation());

            return new Rect(0, 0,
                isLandscape ? _screenHeight : _screenWidth,
                isLandscape ? _screenWidth : _screenHeight);
        }

        /// <summary>
        /// Determines whether the orientation is landscape.
        /// </summary>
        /// <param name="orientation">The orientation.</param>
        /// <returns>True if the orientation is landscape.</returns>
        private static bool IsLandscape(DisplayOrientations orientation)
        {
            return (orientation == DisplayOrientations.Landscape) || (orientation == DisplayOrientations.LandscapeFlipped);
        }

        /// <summary>
        /// Gets a transform for popup elements based
        /// on the current page orientation.
        /// </summary>
        /// <returns>
        /// A composite transform determined by page orientation.
        /// </returns>
        private Transform GetTransform()
        {
            DisplayOrientations orientation = GetPageOrientation();

            switch (orientation)
            {
                case DisplayOrientations.Landscape:
                    return new CompositeTransform() { Rotation = 90, TranslateX = _screenWidth };
                case DisplayOrientations.LandscapeFlipped:
                    return new CompositeTransform() { Rotation = -90, TranslateY = _screenHeight };
                default:
                    return null;
            }
        }

        /// <summary>
        /// Gets the current page orientation.
        /// </summary>
        /// <returns>
        /// The current page orientation.
        /// </returns>
        private static DisplayOrientations GetPageOrientation()
        {
            Frame frame = Window.Current.Content as Frame;

            if (frame != null)
            {
                Page page = frame.Content as Page;

                if (page != null)
                {
                    return DisplayInformation.GetForCurrentView().CurrentOrientation;
                }
            }

            return DisplayOrientations.None;
        }

        /// <summary>
        /// Returns a visibility value based on the value of an bool.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>
        /// Visibility.Collapsed if false.
        /// Visibility.Visible otherwise.
        /// </returns>
        private static Visibility GetVisibilityFromBool(bool obj)
        {
            return obj ? Visibility.Visible : Visibility.Collapsed;
        }

        /// <summary>
        /// Returns a visibility value based on the value of an object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>
        /// Visibility.Collapsed if the object is null.
        /// Visibility.Visible otherwise.
        /// </returns>
        private static Visibility GetVisibilityFromObject(Object obj)
        {
            if (obj == null || string.IsNullOrEmpty(obj as string))
            {
                return Visibility.Collapsed;
            }
            else
            {
                return Visibility.Visible;
            }
        }

        /// <summary>
        /// Returns a visibility value based on the value of a string.
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>
        /// Visibility.Collapsed if the string is null or empty.
        /// Visibility.Visible otherwise.
        /// </returns>
        private static Visibility GetVisibilityFromString(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return Visibility.Collapsed;
            }
            else
            {
                return Visibility.Visible;
            }
        }
    }
}