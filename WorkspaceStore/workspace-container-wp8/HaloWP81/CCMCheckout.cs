﻿using HaloPCL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HaloWP8
{
    class CCMCheckout
    {
        public DWStatusCode result;

        public CCMCheckout()
        {
        }

        public void CCMCheckoutRun()
        {
            result = CCMCommands.PerformCheckout(TransactionsManager.getNextID());
        }
    }
}
