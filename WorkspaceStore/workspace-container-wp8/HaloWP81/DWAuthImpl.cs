﻿using HaloWP8.Utils;
using HaloPCL;
using HaloPCL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloWP8
{
    public class DWAuthImpl : DWAuth
    {
        private DWCommandService mCommandService = null;
        private static string mMqttUrl = "";
        public Context mContext;     
        private readonly string serverEnv = "https://us1.cloudclientmanager.com";
        
        public DWAuthImpl()
        {
            int j;
            j = 10;
            //TODO : create connection to the service
            //android uses 'Service Connection'
        }

        protected override void close()
        {
            HaloLog.v("close");

            try
            {
                if (mCommandService != null)
                {
                    mCommandService.close();
                    mCommandService.unInitializeMQTT();
                    mCommandService = null;
                }
            }
            catch (Exception e)
            {
                // no-op, service didn't start yet
                HaloLog.e("MQTT Service stop error.");
            }
        }

        public override string version()
        {
            HaloLog.v("version");
            return DeviceUtils.getVersion();
          
        }

        public override string haloSDKVersion()
        {
            HaloLog.v("haloSDKVersion");
            return DeviceUtils.getVersion();
           
        }

        public override int authenticate(string username, string password, DWAuthListener cb)
        {
            LocalStorage.setCallback(cb);
            LocalStorage.setUrl(serverEnv.Trim());
            return (int)DWCommandService.authenticate(username.Trim(), password);
         
        }

        public override int authenticate(string url, string username, string password, DWAuthListener cb)
        {
            LocalStorage.setCallback(cb);
            LocalStorage.setUrl(url.Trim());
            return (int)DWCommandService.authenticate(username.Trim(), password);
         
        }

        public override int authenticateSSO(string username, string password, string tenantCode, DWAuthListener cb)
        {
            LocalStorage.setCallback(cb);
            LocalStorage.setUrl(serverEnv.Trim());
            return (int)DWCommandService.authenticate(username.Trim(), password);
            throw new NotImplementedException();
        }

        public override int authenticateSSO(string url, string username, string password, string tenantCode, DWAuthListener cb)
        {
            LocalStorage.setCallback(cb);
            LocalStorage.setUrl(url.Trim());
            return (int)DWCommandService.authenticateSSO(username.Trim(), password, tenantCode);
          
        }

        public override bool isauthenticated()
        {
            LoggingWP8.WP8Logger.LogMessage("isauthenticated");
            return DWCommandService.isauthenticated();
          
        }

        public override int deauthenticate()
        {
            HaloLog.v("deauthenticate");
            SchedulerManager.stopAll();
            return (int)DWCommandService.deauthenticate();
           // throw new NotImplementedException();
        }

        public override int getPocketCloudConnectionList(DWSessionListener cb)
        {
            HaloLog.v("getPocketCloudConnectionList");
            if (!isauthenticated())
                return (int)DWStatusCode.DWStatus_AUTH_INVALID;

            LocalStorage.setDWSessionListener(cb);
            return (int)DWCommandService.getPocketCloudConnectionList();
          
        }

        public override int setCCMCommandsListener(DWCCMCommandsListener cb)
        {
            HaloLog.v("setCCMCommandsListener");
            return setCCMCommandsListener(LocalStorage.getMqttUrl(), URLStore.MQTTDEFAULT_PORT, cb);
           
        }

        public override int setCCMCommandsListener(string mqttHost, int mqttPort, DWCCMCommandsListener cb)
        {
            if (cb != null)
            {
                try
                {
                    mCommandService = DWCommandService.getInstance();
                    LocalStorage.setMqttUrl(mqttHost);
                    mCommandService.start(mContext, mqttHost.Trim(), mqttPort, cb, this);
                }
                catch (Exception)
                {
                    HaloLog.e("Failed to start Command Service!");
                }
            }
            else
            {
                try
                {
                    mCommandService.close();
                }
                catch (Exception)
                {
                    HaloLog.e("MQTT Service stop error.");
                }
            }
            return (int)DWStatusCode.DWStatus_SUCCESS;
        
        }

        public override DWStatusCode checkin(DWCCMCheckinListener cb)
        {
            HaloLog.v("checkin");
            if (!isauthenticated())
                return DWStatusCode.DWStatus_AUTH_INVALID;
            LocalStorage.setDWCCMCheckinListener(cb);
            if (mCommandService != null)
            {
                return mCommandService.checkinTh();
            }
            return DWStatusCode.DWStatus_FAIL;
         
        }

        public override DWStatusCode getFullConfig(DWCCMFullConfigListener cb)
        {
            HaloLog.v("getFullConfig");
            if (!isauthenticated())
                return DWStatusCode.DWStatus_AUTH_INVALID;
            LocalStorage.setDWCCMFullConfigListener(cb);
            if (mCommandService != null)
            {
                return mCommandService.getFullConfigTh();
            }
            return DWStatusCode.DWStatus_FAIL; 
          
        }

        public override DWStatusCode getFullConfigWithDefaults(DWCCMFullConfigWithDefaultsListener cb)
        {
            HaloLog.v("getFullConfigWithDefaults");
            if (!isauthenticated())
                return DWStatusCode.DWStatus_AUTH_INVALID;
            LocalStorage.setDWCCMFullConfigWithDefaultsListener(cb);
            if (mCommandService != null)
            {
                return mCommandService.getFullConfigWithDefaultsTh();
            }
            return DWStatusCode.DWStatus_FAIL; 
         
        }

        public override void setLogLevel(int level)
        {
            HaloLog.v("setLogLevel");
            HaloLog.setLogLevel(level);
           
        }

        public override void setSendLocation(bool value)
        {
            HaloLog.v("enableLocationManager:" + value);
            LocalStorage.setLocationEnabled(value);
            if (mCommandService != null)
            {
                mCommandService.enableLocationManager(value);
            }		
        
        }

        public override bool getSendLocation()
        {
            return LocalStorage.isLocationEnabled();
           
        }

        public override string getUDID()
        {
            return DeviceUtils.getDeviceIdString();
          
        }

        public override bool getJailbreak()
        {
            return DeviceUtils.getDeviceRootedStatus();
            
        }
    }
}
