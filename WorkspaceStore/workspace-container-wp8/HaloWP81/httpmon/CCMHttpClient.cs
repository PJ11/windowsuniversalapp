﻿#define OVIDIU
using HaloWP8.Utils;
using HaloPCL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http; // For HttpClient and related.
using System.Text;
using System.Threading.Tasks;
using HaloPCL.httpmon;
using HaloPCL.Utils;
using System.Net.Http.Headers;

namespace HaloWP8.httpmon
{

    public class CcmConnectionInfo
    {
        public String Url;
        private String mWyseID;
        private String mAuthCode;
        private String mUserAgent;

        public CcmConnectionInfo(Context ctx)
        {
            Url = LocalStorage.getCHECKOUT_URL();
            mWyseID = LocalStorage.getWyseID();
            mAuthCode = LocalStorage.getAuthCode();
            mUserAgent = LocalStorage.getUserAgent();
        }

        public String getWyseID()
        {
            return mWyseID;
        }

        public String getAuthCode()
        {
            return mAuthCode;
        }

        public String getUserAgent()
        {
            return mUserAgent;
        }
    }

    public class StratusHeader
    {
        public static String STRATUS_DEVICE_OWNER_ID = "X-Stratus-device-owner-id";
        public static String STRATUS_DEVICE_ID = "X-Stratus-device-id";
        public static String STRATUS_DEVICE_AUTHENTICATION_CODE = "X-Stratus-device-authentication-code";
        // added for windows agent as new API on agent side does not allow to set Date header in a format expected by server
        // also it is good to have customized date header 
        public static String STRATUS_DATE = "X-Stratus-date";
        public static String STRATUS_DEVICE_CHECKOUT_USER = "X-Stratus-device-checkout-user";
        public static String STRATUS_DEVICE_CHECKOUT_REASON = "X-Stratus-device-checkout-reason";
        //On premise
        public static String STRATUS_ONPREMISE_DOWNLOAD_ID = "X-Stratus-onpremise-download-id";
        public static String STRATUS_ON_PREMISE_AUTHENTICATION_CODE = "X-Stratus-onpremise-authentication-code";
        public static String STRATUS_ON_PREMISE_MACADDRESS = "X-Stratus-onpremise-macaddress";
        // Remote App
        public static String STRATUS_REMOTEAPP_ID = "X-Stratus-remoteApp-wyseId";
        public static String STRATUS_REMOTEAPP_AUTHENTICATION_CODE = "X-Stratus-remoteApp-authentication-code";
        public static String USER_AGENT = "User-Agent";
        public static String STRATUS_MINIMUM_SUPPORTED_SERVER_VERSION = "X-Stratus-minimum-supported-server-version";
    }

    public class CCMHttpClient
    {
        private CookieContainer mCookiesToSendBack = null;
        private bool mRequireCookies = false;
        public void setCookie(CookieContainer cc)
        {
            mCookiesToSendBack = cc;
            if (null != mCookiesToSendBack)
            {
                mRequireCookies = true;
            }
            else
            {
                mRequireCookies = false;
            }
        }

        public CookieContainer getCookieStore()
        {
            return mCookiesToSendBack;
        }

        public HttpResponseHeaders Headers { get; set; }

        public Uri ResponseUri = null;
        private static int HOST_REACH_TIMEOUT = 120000;
        public static String kUserAgent = "User-Agent";
        public static String kDate = "Date";

        static CCMHttpClient _inst = null;

        public static CCMHttpClient createStratusHttpClient()
        {
            _inst = new CCMHttpClient();
            return _inst;
        }

        public CCMHttpClient()
        {
        }

        private async void handleHttpResponseError(HttpResponseMessage response, CcmHttpDataRet msgRet)
        {
            msgRet.mStrData = await response.Content.ReadAsStringAsync();
        }

        private async void handleHttpResponseSuccess(HttpResponseMessage response, CcmHttpDataRet msgRet)
        {
            msgRet.mStrData = await response.Content.ReadAsStringAsync();
        }

        private void handleHttpResponseError(HttpWebResponse response, CcmHttpDataRet msgRet, int size)
        {
            Stream responseStream = response.GetResponseStream();
            byte[] bytes = new byte[size];
            int count = 0;

            count = responseStream.Read(bytes, 0, size);
            if (count > 0)
            {
                msgRet.mStrData = new String(System.Text.Encoding.UTF8.GetChars(bytes), 0, count);
            }
            else
            {
                msgRet.mStrData = "HTTP error: " + msgRet.statusCode;
            }
        }

        private void handleHttpResponseSuccess(HttpWebResponse response, CcmHttpDataRet msgRet, int size)
        {
            Stream responseStream = response.GetResponseStream();

            byte[] bytes = new byte[size];
            int count = 0;

            count = responseStream.Read(bytes, 0, size);
            if (count > 0)
            {
                String line = "";
                msgRet.mStrData = "";
                string result = new String(System.Text.Encoding.UTF8.GetChars(bytes), 0, count);

                StringReader rdr = new StringReader(result);
                while ((line = rdr.ReadLine()) != null)
                {
                    msgRet.mStrData += line;
                }
            }
        }

        public async Task<CcmHttpDataRet> postJsonDataToServer(String url, String body)
        {
#if ANDROID
            try 
            {
                StringEntity stre = new StringEntity(body, "utf-8");
                stre.setContentType("application/json;charset=UTF-8");
                return postDataToServer(url, stre, null, true, false);
            } 
            catch (UnsupportedEncodingException e)
            {			
                HaloLog.e( "UnsupportedEncodingException: ", e);
                CcmHttpDataRet msgRet = new CcmHttpDataRet();
                msgRet.statusCode = 400;
                msgRet.mResult = DWStatusCode.DWStatus_ERROR;
                msgRet.mStrData = e.getMessage();
                return msgRet;
            }
#else
            try
            {
                return await postDataToServer(url, body, null, true, false);
            }
            catch (EncoderFallbackException e)
            {
                //HaloLog.e( "UnsupportedEncodingException: ", e);
                CcmHttpDataRet msgRet = new CcmHttpDataRet();
                msgRet.statusCode = 400;
                msgRet.mResult = DWStatusCode.DWStatus_ERROR;
                msgRet.mStrData = e.Message;
                return msgRet;
            }
#endif
        }

        public async Task<CcmHttpDataRet> postDataToServer(String url, String body)
        {
#if ANDROID
            try 
            {
                StringEntity stre = new StringEntity(body, "utf-8");
                stre.setContentType("application/json;charset=UTF-8");
                return postDataToServer(url, stre, null, true, false);
            } 
            catch (UnsupportedEncodingException e)
            {			
                HaloLog.e( "UnsupportedEncodingException: ", e);
                CcmHttpDataRet msgRet = new CcmHttpDataRet();
                msgRet.statusCode = 400;
                msgRet.mResult = DWStatusCode.DWStatus_ERROR;
                msgRet.mStrData = e.getMessage();
                return msgRet;
            }
#else
            try
            {
                byte[] content = System.Text.Encoding.UTF8.GetBytes(body);
                return await postDataToServer(url, content, null, true, false);
            }
            catch (EncoderFallbackException e)
            {
                //HaloLog.e( "UnsupportedEncodingException: ", e);
                CcmHttpDataRet msgRet = new CcmHttpDataRet();
                msgRet.statusCode = 400;
                msgRet.mResult = DWStatusCode.DWStatus_ERROR;
                msgRet.mStrData = e.Message;
                return msgRet;
            }
#endif
        }

        public async Task<CcmHttpDataRet> postDataToServer(String url, String body, bool dontAutoRedirect)
        {
#if ANDROID
            try
            {
                StringEntity stre = new StringEntity(body, "utf-8");
                stre.setContentType("application/json;charset=UTF-8");
                return postDataToServer(url, stre, null, true, dontAutoRedirect);
            } 
            catch (EncoderFallbackException e) 
            {
                HaloLog.e( "UnsupportedEncodingException: ", e);
                CcmHttpDataRet msgRet = new CcmHttpDataRet();
                msgRet.statusCode = 400;
                msgRet.mResult = DWStatusCode.DWStatus_ERROR;
                msgRet.mStrData = e.getMessage();
                return msgRet;
            }
#else
            try
            {
                byte[] content = System.Text.Encoding.UTF8.GetBytes(body);
                return await postDataToServer(url, content, null, true, dontAutoRedirect);
            }
            catch (EncoderFallbackException e)
            {
                //HaloLog.e( "UnsupportedEncodingException: ", e);
                CcmHttpDataRet msgRet = new CcmHttpDataRet();
                msgRet.statusCode = 400;
                msgRet.mResult = DWStatusCode.DWStatus_ERROR;
                msgRet.mStrData = e.Message;
                return msgRet;
            }
#endif
        }

        public async Task<CcmHttpDataRet> postDataToServer(String url, List<KeyValuePair<string, object>> nameValuePairs, bool dontAutoRedirect)
        {
            // Should this method use ContentType = "application/x-www-form-urlencoded"?	
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                int counter = 0;

                foreach (KeyValuePair<string, object> pair in nameValuePairs)
                {
                    stringBuilder.Append(pair.Key + "=" + WebUtility.UrlEncode(pair.Value as string));
                    if (counter < nameValuePairs.Count - 1)
                    {
                        stringBuilder.Append("&");
                    }
                    counter++;
                }

                byte[] content = System.Text.Encoding.UTF8.GetBytes(stringBuilder.ToString());
                return await postDataToServer(url + "?" + stringBuilder.ToString(), content, null, false, true);
            }
            catch (EncoderFallbackException e)
            {
                //HaloLog.e( "UnsupportedEncodingException: ", e);
                CcmHttpDataRet msgRet = new CcmHttpDataRet();
                msgRet.statusCode = 400;
                msgRet.mResult = DWStatusCode.DWStatus_ERROR;
                msgRet.mStrData = e.Message;
                return msgRet;
            }
        }

        public async Task<CcmHttpDataRet> postDataToServer(String url, byte[] body, CookieContainer cookieJar, bool requireHeader, bool dontAutoRedirect)
        {
            HaloLog.i("postDataToServer() called.");
            CcmHttpDataRet msgRet = new CcmHttpDataRet();

            // The getHttpClient will set the Headers: ContentType, UserAgent
            HttpClient request = getHttpClient(url, requireHeader, dontAutoRedirect);

            if (null == request)
            {
                return null;
            }


            ByteArrayContent theContent = null;

            if (null != body)
            {
                request.DefaultRequestHeaders.Add("ContentLength", body.Length.ToString());
                theContent = new ByteArrayContent(body);
            }
            else
            {
                theContent = new ByteArrayContent(new byte[] { });
            }
            HttpResponseMessage webResponse = await request.PostAsync(url, theContent);

            HttpStatusCode sc = webResponse.StatusCode;
            msgRet.statusCode = (int)sc;

#if OVIDIU
            msgRet.statusCode = (int)sc;
            if (sc == HttpStatusCode.OK || (dontAutoRedirect && sc == HttpStatusCode.Found))
            {
                using (var sr = new StreamReader(await webResponse.Content.ReadAsStreamAsync()))
                {
                    msgRet.mStrData = sr.ReadToEnd();
                }

                if (mRequireCookies)
                {
                    if (webResponse.Headers.Contains("Set-Cookie"))
                    {
                        CookieContainer cookies = new CookieContainer();
                        var cookieHeader = webResponse.Headers.GetValues("Set-Cookie");
                        cookies.SetCookies(webResponse.RequestMessage.RequestUri, cookieHeader.ToString());
                        mCookiesToSendBack = cookies;
                        Headers = webResponse.Headers;
                    }
                }
                Headers = webResponse.Headers;
                msgRet.mResult = DWStatusCode.DWStatus_SUCCESS;
            }
            else
            {
                HaloLog.e("Error: ");
                msgRet.mResult = (webResponse.StatusCode == HttpStatusCode.BadRequest) ? DWStatusCode.DWStatus_CONNECTION_FAILED : DWStatusCode.DWStatus_ERROR;
                msgRet.mStrData = "Fail";
            }
            return msgRet;
#else

            if (dontAutoRedirect)
            {
                if (HttpStatusCode.Redirect != sc)
                {
                    handleHttpResponseError(webResponse, msgRet, 8192);
                }
                else
                {
                    string redirectLocation = webResponse.Headers["Location"];
                    msgRet.mStrData = redirectLocation;
                }
            }
            else
            {
                if (HttpStatusCode.OK != sc)
                {
                    handleHttpResponseError(webResponse, msgRet, 8192);
                }
                else
                {
                    handleHttpResponseSuccess(webResponse, msgRet, (int)webResponse.ContentLength);
                }
            }
            // Get the response.
            webResponse.Dispose();
            msgRet.mResult = DWStatusCode.DWStatus_SUCCESS;
            return msgRet;
#endif
        }

        public async Task<CcmHttpDataRet> postDataToServer(String url, string body, CookieContainer cookieJar, bool requireHeader, bool dontAutoRedirect)
        {
            HaloLog.i("postDataToServer() called.");
            CcmHttpDataRet msgRet = new CcmHttpDataRet();

            // The getHttpClient will set the Headers: ContentType, UserAgent
            HttpClient request = getHttpClient(url, requireHeader, dontAutoRedirect);

            if (null == request)
            {
                return null;
            }

            StringContent theContent = null;

            if (null != body)
            {
                request.DefaultRequestHeaders.Add("ContentLength", body.Length.ToString());
                theContent = new StringContent(body, Encoding.UTF8, "application/json");
            }
            else
            {
                theContent = new StringContent(string.Empty);
            }

            //request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url);
            //request.Content = content;

            HttpResponseMessage webResponse = await request.PostAsync(url, theContent);

            HttpStatusCode sc = webResponse.StatusCode;
            msgRet.statusCode = (int)sc;

#if OVIDIU
            msgRet.statusCode = (int)sc;
            if (sc == HttpStatusCode.OK || (dontAutoRedirect && sc == HttpStatusCode.Found))
            {
                using (var sr = new StreamReader(await webResponse.Content.ReadAsStreamAsync()))
                {
                    msgRet.mStrData = sr.ReadToEnd();
                }

                if (mRequireCookies)
                {
                    if (webResponse.Headers.Contains("Set-Cookie"))
                    {
                        CookieContainer cookies = new CookieContainer();
                        var cookieHeader = webResponse.Headers.GetValues("Set-Cookie");
                        cookies.SetCookies(webResponse.RequestMessage.RequestUri, cookieHeader.ToString());
                        mCookiesToSendBack = cookies;
                        Headers = webResponse.Headers;
                    }
                }
                Headers = webResponse.Headers;
                msgRet.mResult = DWStatusCode.DWStatus_SUCCESS;
            }
            else
            {
                HaloLog.e("Error: ");
                msgRet.mResult = (webResponse.StatusCode == HttpStatusCode.BadRequest) ? DWStatusCode.DWStatus_CONNECTION_FAILED : DWStatusCode.DWStatus_ERROR;
                msgRet.mStrData = "Fail";
            }
            return msgRet;
#else

            if (dontAutoRedirect)
            {
                if (HttpStatusCode.Redirect != sc)
                {
                    handleHttpResponseError(webResponse, msgRet, 8192);
                }
                else
                {
                    string redirectLocation = webResponse.Headers["Location"];
                    msgRet.mStrData = redirectLocation;
                }
            }
            else
            {
                if (HttpStatusCode.OK != sc)
                {
                    handleHttpResponseError(webResponse, msgRet, 8192);
                }
                else
                {
                    handleHttpResponseSuccess(webResponse, msgRet, (int)webResponse.ContentLength);
                }
            }
            // Get the response.
            webResponse.Dispose();
            msgRet.mResult = DWStatusCode.DWStatus_SUCCESS;
            return msgRet;
#endif
        }

        public async Task<CcmHttpDataRet> getDataFromServer(String url)
        {
            return await getDataFromServer(url, true, null, false).ConfigureAwait(false);
        }

        public async Task<CcmHttpDataRet> getDataFromServer(String url, bool requireHeader, CookieContainer cookieJar)
        {
            return await getDataFromServer(url, requireHeader, cookieJar, false).ConfigureAwait(false);
        }

        public async Task<CcmHttpDataRet> getDataFromServer(String url, bool requireHeader, CookieContainer cookieJar, bool dontAutoRedirect)
        {
            HaloLog.i("getDataFromServer() called.");
            CcmHttpDataRet msgRet = new CcmHttpDataRet();

            // The getHttpClient will set the Headers: ContentType, UserAgent
            HttpClient request = getHttpClient(url, requireHeader, dontAutoRedirect);

            if (null == request)
            {
                return null;
            }

            HttpResponseMessage webResponse = await request.GetAsync(url);
            HttpStatusCode sc = webResponse.StatusCode;
            msgRet.statusCode = (int)sc;
            if (dontAutoRedirect)
            {
                if (HttpStatusCode.Redirect != sc)
                {
                    handleHttpResponseError(webResponse, msgRet);
                }
                else
                {
                    string redirectLocation = webResponse.Headers.Location.AbsoluteUri;
                    msgRet.mStrData = redirectLocation;
                }
            }
            else
            {
                if (HttpStatusCode.OK != sc)
                {
                    handleHttpResponseError(webResponse, msgRet);
                }
                else
                {
                    ResponseUri = webResponse.RequestMessage.RequestUri;
                    handleHttpResponseSuccess(webResponse, msgRet);
                }
            }
            // Get the response.
            webResponse.Dispose();
            msgRet.mResult = DWStatusCode.DWStatus_SUCCESS;
            return msgRet;
        }

        public async Task<CcmHttpDataRet> deleteFromServer(String url)
        {
            HaloLog.i("deleteFromServer() called.");
            CcmHttpDataRet msgRet = new CcmHttpDataRet();

            HttpWebRequest request = getDeleteRequest(url);
            if (null == request)
            {
                return null;
            }

            HttpWebResponse webResponse = (HttpWebResponse)await request.GetResponseAsync().ConfigureAwait(false);
            HttpStatusCode sc = webResponse.StatusCode;

            if (HttpStatusCode.OK != sc)
            {
                handleHttpResponseError(webResponse, msgRet, 8192);
            }
            else
            {
                handleHttpResponseSuccess(webResponse, msgRet, (int)webResponse.ContentLength);
            }

            webResponse.Dispose();
            msgRet.mResult = DWStatusCode.DWStatus_SUCCESS;
            return msgRet;
        }

        public async Task<CcmHttpDataRet> deleteFromServer(CcmConnectionInfo ci)
        {
            HaloLog.i("deleteFromServer() called.");
            CcmHttpDataRet msgRet = new CcmHttpDataRet();

            HttpWebRequest request = getDeleteRequest(ci);
            if (null == request)
            {
                return null;
            }

            HttpWebResponse webResponse = (HttpWebResponse)await request.GetResponseAsync().ConfigureAwait(false);
            HttpStatusCode sc = webResponse.StatusCode;

            if (HttpStatusCode.OK != sc)
            {
                handleHttpResponseError(webResponse, msgRet, 8192);
            }
            else
            {
                handleHttpResponseSuccess(webResponse, msgRet, (int)webResponse.ContentLength);
            }

            webResponse.Dispose();
            msgRet.mResult = DWStatusCode.DWStatus_SUCCESS;
            return msgRet;
        }

        private static String performMD5Operation(String WyseId, String AuthCode, Int64 nmilliseconds)
        {
            if ((WyseId == null) || (WyseId.Length <= 0) ||
            (AuthCode == null) || (AuthCode.Length <= 0))
            {
                return null;
            }
            try
            {
#if SHA256
                SHA256Managed sha256 = new SHA256Managed();
                sha256.Initialize();
                String str = deviceId + dateSent + authCode;
                byte[] bytes = new byte[str.Length * sizeof(char)];
                System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
                byte[] messageDigest = sha256.ComputeHash(bytes);
                return Convert.ToBase64String(messageDigest).Trim();
#else
                StringBuilder sbval = new StringBuilder();
                sbval.Append(WyseId);
                sbval.Append(nmilliseconds);
                sbval.Append(AuthCode);

                MD5.MD5 md5 = new MD5.MD5();
                byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(sbval.ToString());
                byte[] hash = md5.ComputeHash(inputBytes);
                return Convert.ToBase64String(hash);
#endif
            }
            catch (Exception)
            {
            }
            return null;
        }

        //public bool addHeaders(AbstractHttpMessage request)
        public bool addHeaders(HttpWebRequest request, bool addContentType)
        {
            String deviceId = LocalStorage.getWyseID();
            String authCode = LocalStorage.getAuthCode();

            DateTime dateNow = DateTime.UtcNow;
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan ts = dateNow - origin;

            Int64 nmilliseconds = (Int64)(ts.TotalSeconds) * 1000;

            String msgSentStr = dateNow.ToString("yyyy-MM-dd HH:mm:ss UTC");

            String hashCode = performMD5Operation(deviceId, authCode, nmilliseconds);
            if (hashCode != null)
            {
                request.Headers[StratusHeader.STRATUS_DEVICE_ID] = deviceId;
                request.Headers[kDate] = msgSentStr;
                request.Headers[StratusHeader.STRATUS_DEVICE_AUTHENTICATION_CODE] = hashCode;
                request.Headers[StratusHeader.STRATUS_REMOTEAPP_ID] = deviceId;
                request.Headers[StratusHeader.STRATUS_REMOTEAPP_AUTHENTICATION_CODE] = hashCode;
            }
            else
            {
                request.Accept = "application/json";
            }

            if (addContentType)
            {
                request.ContentType = "application/json";
            }
            if (request.Method.Equals("GET"))
            {
                request.Headers[HttpRequestHeader.IfModifiedSince] = DateTime.UtcNow.ToString();
            }
            request.Headers["user-agent"] = LocalStorage.getUserAgent();
            return true;
        }

        private bool addHeaders(HttpWebRequest request, CcmConnectionInfo ci, bool addContentType)
        {
            String deviceId = ci.getWyseID();
            String authCode = ci.getAuthCode();

            DateTime dateNow = DateTime.UtcNow;
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan ts = dateNow - origin;

            Int64 nmilliseconds = (Int64)(ts.TotalSeconds) * 1000;

            String msgSentStr = dateNow.ToString("yyyy-MM-dd HH:mm:ss UTC");

            String hashCode = performMD5Operation(deviceId, authCode, nmilliseconds);
            if (hashCode != null)
            {
                request.Headers[StratusHeader.STRATUS_DEVICE_ID] = deviceId;
                request.Headers[kDate] = msgSentStr;
                request.Headers[StratusHeader.STRATUS_DEVICE_AUTHENTICATION_CODE] = hashCode;
                request.Headers[StratusHeader.STRATUS_REMOTEAPP_ID] = deviceId;
                request.Headers[StratusHeader.STRATUS_REMOTEAPP_AUTHENTICATION_CODE] = hashCode;
            }
            else
            {
                request.Accept = "application/json";
            }

            if (addContentType)
                request.ContentType = "application/json";
            request.Headers["user-agent"] = ci.getUserAgent();

            return true;
        }

        private bool addHttpClientHeaders(HttpClient request, bool addContentType)
        {
            String deviceId = LocalStorage.getWyseID();
            String authCode = LocalStorage.getAuthCode();

            DateTime dateNow = DateTime.UtcNow;
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan ts = dateNow - origin;

            Int64 nmilliseconds = (Int64)(ts.TotalSeconds) * 1000;

            String msgSentStr = dateNow.ToString("yyyy-MM-dd HH:mm:ss UTC");
            //String msgSentStr = dateNow.ToString("ddd, dd MMM yyyy HH:mm:ss zzzz");

            String hashCode = performMD5Operation(deviceId, authCode, nmilliseconds);
            if (hashCode != null)
            {
                request.DefaultRequestHeaders.Add(StratusHeader.STRATUS_DEVICE_ID, deviceId);
                request.DefaultRequestHeaders.TryAddWithoutValidation("Date", msgSentStr);
                request.DefaultRequestHeaders.Add(StratusHeader.STRATUS_DEVICE_AUTHENTICATION_CODE, hashCode);
                request.DefaultRequestHeaders.Add(StratusHeader.STRATUS_REMOTEAPP_ID, deviceId);
                request.DefaultRequestHeaders.Add(StratusHeader.STRATUS_REMOTEAPP_AUTHENTICATION_CODE, hashCode);
            }
            else
            {
                //request.DefaultRequestHeaders.Add("Accept", "application/json");
                request.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }

            if (addContentType)
            {
                request.DefaultRequestHeaders.Add("ContentType", "application/json");
            }

            request.DefaultRequestHeaders.UserAgent.TryParseAdd(LocalStorage.getUserAgent());

            return true;
        }

        public HttpClient getHttpClient(String url, bool requireHeader, bool dontAutoRedirect)
        {
            HttpClientHandler clientHandler = new HttpClientHandler();

            if (null != mCookiesToSendBack)
            {
                // If we want to use cookies
                clientHandler.CookieContainer = mCookiesToSendBack;
            }

            if (dontAutoRedirect)
            {
                clientHandler.AllowAutoRedirect = false;
            }
            else
            {
                clientHandler.AllowAutoRedirect = true;
            }

            HttpClient client = new HttpClient(clientHandler);

            if (requireHeader)
            {
                if (addHttpClientHeaders(client, true))
                {
                    return client;
                }
            }
            else
            {
                // Only add the user agent
                client.DefaultRequestHeaders.Add("user-agent", LocalStorage.getUserAgent());
                return client;
            }

            return null;
        }

#if USE_HTTP_WEB_REQUEST_INSTEAD_OF_HTTP_CLIENT
        public HttpWebRequest getHttpPost(String url, bool requireHeader, bool dontAutoRedirect)
        {
            HttpWebRequest postRequest = (HttpWebRequest)System.Net.HttpWebRequest.Create(url);
            postRequest.Method = "POST";
            if (dontAutoRedirect)
            {
                postRequest.AllowAutoRedirect = false;
            }
            else
            {
                postRequest.AllowAutoRedirect = true;
            }
            if (requireHeader)
            {
                if (addHeaders(postRequest, true))
                {
                    return postRequest;
                }
            }
            else
            {
                // Only add the user agent
                postRequest.Headers["user-agent"] = LocalStorage.getUserAgent();
                return postRequest;
            }
            return null;
        }
#endif
        public HttpWebRequest getDeleteRequest(String url)
        {
            System.Net.HttpWebRequest deleteRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(url);
            deleteRequest.Method = "DELETE";
            if (addHeaders(deleteRequest, true))
                return deleteRequest;
            return deleteRequest;
        }

        private HttpWebRequest getDeleteRequest(CcmConnectionInfo ci)
        {
            System.Net.HttpWebRequest deleteRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(ci.Url);
            deleteRequest.Method = "DELETE";
            if (addHeaders(deleteRequest, ci, true))
                return deleteRequest;
            return deleteRequest;
        }

#if USE_HTTP_WEB_REQUEST_INSTEAD_OF_HTTP_CLIENT
        public HttpWebRequest getHttpGetRequest(String cmdUrl, bool requireHeader, bool dontAutoRedirect)
        {
            HttpWebRequest getRequest = (HttpWebRequest)System.Net.HttpWebRequest.Create(cmdUrl);
            getRequest.Method = "GET";
            if (dontAutoRedirect)
            {
                getRequest.AllowAutoRedirect = false;
            }
            else
            {
                getRequest.AllowAutoRedirect = true;
            }
            if (requireHeader)
            {
                if (addHeaders(getRequest, false))
                {
                    return getRequest;
                }
            }
            else
            {
                return getRequest;
            }
            return null;
        }
#endif
    }
}