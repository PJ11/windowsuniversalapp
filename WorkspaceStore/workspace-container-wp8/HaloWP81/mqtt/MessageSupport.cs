﻿using HaloWP81.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HaloWP8.mqtt
{
    class MessageSupport
    {
        public static byte[] writeMsgLength(int len)
        {
            MemoryStream ms = new MemoryStream();
            int msgLength = len;
            int val = msgLength;
            int pos = 1;
            do
            {
                byte b = (byte)(val & 0x7F);
                val >>= 7;
                if (val > 0)
                {
                    b |= 0x80;
                }
                pos++;
                ms.WriteByte(b);
            } while (val > 0);
            return ms.ToArray();
        }

        internal static void writeUTF(ref StreamWriter sw, string buffer)
        {
            byte[] bb = Encoding.UTF8.GetBytes(buffer);
            byte[] bLen = BitConverter.GetBytes((short)bb.Length);
            sw.Write(bLen[1]);
            sw.Write(bLen[0]);
            //sw.Write((short)bb.Length));
            sw.Write(buffer);
        }

        internal static void writeUTF(ref MemoryStream ms, string buffer)
        {
            byte[] bb = Encoding.UTF8.GetBytes(buffer);

            byte[] bLen = BitConverter.GetBytes((short)bb.Length);
            ms.WriteByte(bLen[1]);
            ms.WriteByte(bLen[0]);
            //ms.Write(BitConverter.GetBytes((short)bb.Length), 0, 2);
            ms.Write(bb, 0, bb.Length);
        }

        public static String readUTF(BinaryReader iss)
        {
            short size = IPAddress.HostToNetworkOrder(iss.ReadInt16());
            byte[] buffer = iss.ReadBytes(size);
            if (buffer == null && buffer.Length != size)
            {
                throw new System.Exception("Invalid message encoding");
            }
            return Encoding.UTF8.GetString(buffer, 0, buffer.Length);
        }


        public class HeaderBase
        {
            public virtual byte header { get; set; }
            public byte messageType()
            {
                return (byte)((header & 0xF0) >> 4);
            }
            public HeaderBase commandType(int type)
            {
                this.header &= 0x0F;
                this.header |= (byte)((type << 4) & 0xF0);
                return this;
            }
            public QoS qos()
            {
                return (QoS)(((header & 0x06) >> 1));
            }
            public HeaderBase qos(QoS qos)
            {
                this.header &= 0xF9;
                this.header |= (byte)(((byte)qos << 1) & 0x06);
                return this;
            }
            public bool dup()
            {
                return (header & 0x08) > 0;
            }
            public HeaderBase dup(bool dup)
            {
                if (dup)
                {
                    this.header |= 0x08;
                }
                else
                {
                    this.header &= 0xF7;
                }
                return this;
            }

            public bool retain()
            {
                return (header & 0x01) > 0;
            }
            public HeaderBase retain(bool retain)
            {
                if (retain)
                {
                    this.header |= 0x01;
                }
                else
                {
                    this.header &= 0xFE;
                }
                return this;
            }

        }


        internal static int writeUTF(byte[] buffers, int offset, byte[] bb)
        {
            short bLen = (short)bb.Length;
            buffers[offset] = (byte)(bLen >> 8);
            buffers[offset + 1] = (byte)(bLen & 0xFF);
            Buffer.BlockCopy(bb, 0, buffers, offset + 2, bb.Length);
            return offset + 2 + bb.Length;
        }
    }
}
