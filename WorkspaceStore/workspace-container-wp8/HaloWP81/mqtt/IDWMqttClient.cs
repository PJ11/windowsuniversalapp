﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloWP8.mqtt
{
    public interface IDWMqttClient
    {
        Task<bool> disconnect();

        Task<bool> start(String host, int port, String deviceID, bool b, int i, IDWMqttCallback cb);

        void notifyNetworkChange();

        void Subscribe();
    }
}
