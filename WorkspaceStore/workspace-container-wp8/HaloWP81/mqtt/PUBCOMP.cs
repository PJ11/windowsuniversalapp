﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using HaloWP81.Utils;

namespace HaloWP8.mqtt
{
    public class PUBCOMP : MqttObj
    {
        public static byte TYPE = (byte)CmdType.PUBCOMP;
        public short MessageID { get; set; }

        public PUBCOMP()
            : base(TYPE)
        { }

        public byte[] Encode()
        {
            byte[] ret = new byte[4];
            ret[0] = (byte)((TYPE << 4) & 0xF0);
            ret[1] = 2;
            ret[2] = (byte)(MessageID >> 8);
            ret[3] = (byte)(MessageID & 0xFF);
            return ret;
        }

        public bool Decode(BinaryReader br, int len)
        {
            if (len < 2)
                return false;
            MessageID = IPAddress.HostToNetworkOrder(br.ReadInt16());
            return true;
        }

        public override bool Decode(byte[] buf, int offset)
        {
            if (Length < 2)
                return false;
            offset += 2;
            MessageID = (short)(buf[offset++] << 8);
            MessageID += buf[offset];
            return true;
        }
    }
}
