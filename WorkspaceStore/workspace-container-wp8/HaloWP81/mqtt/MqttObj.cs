﻿using HaloWP81.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace HaloWP8.mqtt
{
    public abstract class MqttObj
    {
        public byte Header { get; set; }
        public byte Length { get; set; }

        public MqttObj(byte type)
        {
            commandType(type);
        }

        public CmdType CmdT { get { return (CmdType)((short)((Header >> 4) & 0x0f)); } }

        public MqttObj(BinaryReader br)
        {
            Header = br.ReadByte();
            Length = br.ReadByte();
        }

        public MqttObj commandType(int type)
        {
            this.Header &= 0x0F;
            this.Header |= (byte)((type << 4) & 0xF0);
            return this;
        }
        public QoS qos
        {
            get
            {
                return (QoS)(((Header & 0x06) >> 1));
            }
            set
            {
                this.Header &= 0xF9;
                this.Header |= (byte)(((byte)value << 1) & 0x06);
            }
        }

        public static byte[] writeMsgLength(int len)
        {
            MemoryStream ms = new MemoryStream();
            int msgLength = len;
            int val = msgLength;
            int pos = 1;
            do
            {
                byte b = (byte)(val & 0x7F);
                val >>= 7;
                if (val > 0)
                {
                    b |= 0x80;
                }
                pos++;
                ms.WriteByte(b);
            } while (val > 0);
            return ms.ToArray();
        }

        public static void writeUTF(ref StreamWriter sw, string buffer)
        {
            byte[] bb = Encoding.UTF8.GetBytes(buffer);
            byte[] bLen = BitConverter.GetBytes((short)bb.Length);
            sw.Write(bLen[1]);
            sw.Write(bLen[0]);
            sw.Write(bb);
        }
        public static void writeUTF(ref List<byte> sw, string buffer)
        {
            byte[] bb = Encoding.UTF8.GetBytes(buffer);
            byte[] bLen = BitConverter.GetBytes((short)bb.Length);
            sw.Add(bLen[1]);
            sw.Add(bLen[0]);
            sw.AddRange(bb);
        }

        public static void writeUTF(ref MemoryStream ms, string buffer)
        {
            byte[] bb = Encoding.UTF8.GetBytes(buffer);

            byte[] bLen = BitConverter.GetBytes((short)bb.Length);
            ms.WriteByte(bLen[1]);
            ms.WriteByte(bLen[0]);
            //ms.Write(BitConverter.GetBytes((short)bb.Length), 0, 2);
            ms.Write(bb, 0, bb.Length);
        }

        public static int writeUTF(byte[] buffers, int offset, byte[] bb)
        {
            short bLen = (short)bb.Length;
            buffers[offset] = (byte)(bLen >> 8);
            buffers[offset + 1] = (byte)(bLen & 0xFF);
            Buffer.BlockCopy(bb, 0, buffers, offset + 2, bb.Length);
            return offset + 2 + bb.Length;
        }

        public static String readUTF(BinaryReader iss)
        {
            short size = IPAddress.HostToNetworkOrder(iss.ReadInt16());
            byte[] buffer = iss.ReadBytes(size);
            if (buffer == null && buffer.Length != size)
            {
                throw new Exception("Invalid message encoding");
            }
            return Encoding.UTF8.GetString(buffer, 0, buffer.Length);
        }

        static public int readUTF(byte[] buf, ref int offset, ref string sOut)
        {
            short size = (short)(buf[offset++] << 8);
            size += buf[offset++];
            if (buf.Length < size)
            {
                throw new Exception("Invalid message encoding");
            }
            sOut = Encoding.UTF8.GetString(buf, offset, size);
            offset += size;
            return size + 2;
        }

        static public string readUTF(byte[] buf, int offset)
        {
            short size = (short)(buf[offset++] << 4);
            size += buf[offset++];
            if (buf.Length < size)
            {
                throw new Exception("Invalid message encoding");
            }
            return Encoding.UTF8.GetString(buf, offset, size);
        }

        public static int readUTF(BinaryReader iss, ref String sOut)
        {
            short size = IPAddress.HostToNetworkOrder(iss.ReadInt16());
            byte[] buffer = iss.ReadBytes(size);
            if (buffer == null && buffer.Length != size)
            {
                throw new Exception("Invalid message encoding");
            }
            sOut = Encoding.UTF8.GetString(buffer,0,buffer.Length);
            return size + 2;
        }

        public abstract bool Decode(byte[] buf, int offset);
    }
}
