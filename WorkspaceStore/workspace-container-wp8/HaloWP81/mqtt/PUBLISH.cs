﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using HaloWP81.Utils;

namespace HaloWP8.mqtt
{
    public class PUBLISH : MqttObj
    {
        public static byte TYPE = (byte)CmdType.PUBLISH;

        //public int Length { get; set; }

        public short MessageID { get; set; }
        public Topic Topics { get; set; }

        public bool MReturn { get; set; }

        public String Message { get { return Encoding.UTF8.GetString(Payload, 0, Payload.Length); } set { Payload = Encoding.UTF8.GetBytes(value); } }
        public byte[] Payload { get; set; }

        public PUBLISH() : base(TYPE)
        {
            qos = QoS.AT_LEAST_ONCE;
            MessageID = 0x1234;
        }

        public PUBLISH(byte cmd) : base(TYPE)
        {
            qos = QoS.AT_LEAST_ONCE;
            MessageID = 0x1234;
            Header = cmd;
        }

        public PUBLISH(PUBLISH r)
            : base(TYPE)
        {
            qos = QoS.AT_LEAST_ONCE;
            MessageID = r.MessageID;
            Header = r.Header;
            Topics = r.Topics;
            Payload = r.Payload;
        }

        public PUBLISH(Notification n)
            : base(TYPE)
        {
            qos = QoS.AT_LEAST_ONCE;
            MessageID = n.NID;
            Topics = new Topic(n.UDID, QoS.AT_LEAST_ONCE);
            Payload = n.Payload;
        }

        public PUBLISH(MqttObj r)
            : base(TYPE)
        {
            qos = QoS.AT_LEAST_ONCE;
            Header = r.Header;
        }

        public byte[] Encode()
        {
            QoS _qos = qos;
            qos = QoS.AT_LEAST_ONCE;
            int len = 0;
            if (_qos != QoS.AT_MOST_ONCE)
            {
                len += 2; // MessageId
            }
            
            len += Topics.LenghtBytes - 1;
            len += Payload.Length;

            byte[] bbLen = writeMsgLength((int)len);
            len += bbLen.Length;

            byte[] buffers = new byte[1 + len];
            int offset = 0;
            buffers[offset++] = Header;
            Buffer.BlockCopy(bbLen, 0, buffers, offset, bbLen.Length);
            offset += bbLen.Length;

            offset = writeUTF(buffers, offset, Encoding.UTF8.GetBytes(Topics.name));
            //buffers[offset++] = (byte)Topics.qoS;

            if (_qos != QoS.AT_MOST_ONCE)
            {
                buffers[offset++] = (byte)(MessageID >> 8);
                buffers[offset++] = (byte)(MessageID & 0xFF);
            }

            Array.Copy(Payload, 0, buffers, offset, Payload.Length);
            return buffers;

            //long len = 0;
            //byte[] buffers;
            
            //using (MemoryStream ms = new MemoryStream(new byte[500], 0, 500, true, true))
            //{
            //    using (BinaryWriter sw = new BinaryWriter(ms))
            //    {
            //        QoS _qos = base.qos;
            //        byte[] bb = Encoding.UTF8.GetBytes(Topics.name);
            //        sw.Write(IPAddress.HostToNetworkOrder((short)bb.Length));
            //        sw.Write(bb);
            //        sw.Write((byte)Topics.qoS);
            //        sw.Write(IPAddress.HostToNetworkOrder((short)MessageID));

            //        if (Message != null && Message.Length > 0) 
            //        {
            //            sw.Write(Encoding.UTF8.GetBytes(Message));
            //        }
            //        len = ms.Position;
            //    }
            //    ms.Flush();
            //    buffers = new byte[2 + len];
            //    buffers[0] = header;
            //    byte[] bbLen = MessageSupport.writeMsgLength((int)len);
            //    Array.Copy(bbLen, 0, buffers, 1, bbLen.Length);
            //    Array.Copy(ms.GetBuffer(), 0, buffers, bbLen.Length + 1, len);
            //}
            //return buffers;
        }

        public bool Decode(BinaryReader br, int len)
        {
            try
            {
                MReturn = ((Header & 0x01) == 1) ? true : false;
                QoS subQoS = (QoS)(((Header >> 1) & 0x03));

                string s = null;
                len -= readUTF(br, ref s);

                Topics = new Topic(s, subQoS);
                MessageID = 0;
                if (subQoS != QoS.AT_MOST_ONCE)
                {
                    MessageID = IPAddress.HostToNetworkOrder(br.ReadInt16());
                    len -= 2;
                }
                Payload = br.ReadBytes(len);
                return true;
            }
            catch (Exception/*ProtocolException*/)
            {
                return false;
            }
        }

        public override bool Decode(byte[] buf, int offset )
        {
            try
            {
                Header = buf[offset];
                MReturn = ((Header & 0x01) == 1) ? true : false;
                QoS subQoS = (QoS)(((Header >> 1) & 0x03));

                int len = Length;
                offset += 2;
                String s = String.Empty;
                len -= readUTF(buf, ref offset, ref s);
                Topics = new Topic(s, subQoS);

                MessageID = 0;
                if (subQoS != QoS.AT_MOST_ONCE)
                {
                    MessageID = (short)(buf[offset++] << 8);
                    MessageID += buf[offset++];
                    len -= 2;
                }
                Payload = new byte[len];
                Buffer.BlockCopy(buf, offset, Payload, 0, len);
                return true;
            }
            catch (Exception/*ProtocolException*/)
            {
                return false;
            }
        }
    }
}
