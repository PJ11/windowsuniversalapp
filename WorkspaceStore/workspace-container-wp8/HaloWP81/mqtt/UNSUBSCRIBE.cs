﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using HaloWP81.Utils;

namespace HaloWP8.mqtt
{
    public class UNSUBSCRIBE : MqttObj
    {
        public static byte TYPE = (byte)CmdType.UNSUBSCRIBE;

        public static Topic[] NO_TOPICS = new Topic[0];
        public List<String> topics = new List<String>();

        public short MessageId = 10;

        public UNSUBSCRIBE() : base(TYPE) { }

        public byte[] Encode()
        {
            long len = 0;
            qos = QoS.AT_LEAST_ONCE;
            byte[] buffers;
            using (MemoryStream ms = new MemoryStream(new byte[500], 0, 500, true))
            {
                using (BinaryWriter sw = new BinaryWriter(ms))
                {
                    QoS _qos = base.qos;
                    if (_qos != QoS.AT_MOST_ONCE)
                    {
                        sw.Write(IPAddress.HostToNetworkOrder((short)MessageId));
                    }
                    foreach (string topicname in topics)
                    {
                        byte[] bb = Encoding.UTF8.GetBytes(topicname);
                        sw.Write(IPAddress.HostToNetworkOrder((short)bb.Length));
                        sw.Write(bb);
                    }
                    len = ms.Position;
                }
                ms.Flush();
                buffers = new byte[2 + len];
                buffers[0] = Header;
                byte[] bbLen = writeMsgLength((int)len);
                Array.Copy(bbLen, 0, buffers, 1, bbLen.Length);
                
                byte [] streamBytes = new byte[ms.Length];
                ms.Read(streamBytes, 0, (int)ms.Length);
                Array.Copy(streamBytes, 0, buffers, bbLen.Length + 1, (int)len);
            }
            return buffers;
        }

        public bool Decode(BinaryReader br, int len)
        {
            MessageId = IPAddress.HostToNetworkOrder(br.ReadInt16());
            while (br.BaseStream.Position < br.BaseStream.Length)
            {
                topics.Add(readUTF(br));
            }
            return true;
        }

        public override bool Decode(byte[] buf, int offset)
        {
            offset += 2;
            if (Length < 2)
                return false;
            int len = Length;
            MessageId = (short)(buf[offset++] << 8);
            MessageId += (buf[offset++]);
            len -= 2;

            while (len > 0)
            {
                String s = String.Empty;
                len -= readUTF(buf, ref offset, ref s);
                topics.Add(s);
            }
            return true;
        }
    }
}
