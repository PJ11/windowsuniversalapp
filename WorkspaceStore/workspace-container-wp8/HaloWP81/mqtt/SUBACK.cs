﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace HaloWP8.mqtt
{
    public class SUBACK : MqttObj
    {
        public static byte TYPE = (byte)CmdType.SUBACK;
        public ConnectReturnCode ConnRetCode { get; set; }

        //public int Length { get; set; }
        public List<Topic> Topics { get; set; }
        public short MessageID { get; set; }

        public SUBACK()
            : base(TYPE)
        { }


        public byte[] Encode()
        {
            byte[] ret = new byte[4 + Topics.Count];
            ret[0] = Header;
            ret[1] = (byte)(2 + Topics.Count);
            ret[2] = (byte)(MessageID >> 8);
            ret[3] = (byte)(MessageID & 0xFF);

            int i=4;
            foreach (var it in Topics)
            {
                ret[i++] = (byte)it.qoS;
            }
            return ret;
        }

        public bool Decode(BinaryReader br)
        {
            //int len = br.ReadByte() - 2;
            MessageID = (short)br.ReadInt16();
            Length -= 2; 
            Topics = new List<Topic>();
            while (Length-- > 0)
            {
                Topic to = new Topic("", (QoS)(br.ReadByte() >> 1));
                Topics.Add(to);
            }
            return true;
        }
        public bool Decode(BinaryReader br, int len)
        {

            //offset += 2;
            //if (length < 2)
            //    return false;
            //int len = length;
            //MessageID = (int)(buf[offset++] << 8);
            //MessageID |= (int)(buf[offset++]);
            //len -= 2;
            //Topics = new ArrayList<Topic>();
            //while (len > 0)
            //{
            //    Topic to = new Topic("", QoS.values()[(buf[offset++] >> 1)]);
            //    len--;
            //    Topics.add(to);
            //}
            //return true;

            if (len < 2)
                return false;

            MessageID = (short)br.ReadInt16();
            len -= 2;
            Topics = new List<Topic>();
            while (len-- > 0)
            {
                Topic to = new Topic("", (QoS)(br.ReadByte() >> 1));
                Topics.Add(to);
            }
            return true;
        }

        public static byte[] Encode(SUBSCRIBE subb)
        {
            SUBACK sa = new SUBACK();
            sa.MessageID = subb.MessageId;
            sa.Topics = subb.topicsList;
            return sa.Encode();
        }


        public override bool Decode(byte[] buf, int offset)
        {
            offset += 2;
            if (Length < 2)
                return false;
            int len = Length;
            MessageID = (short)(buf[offset++] << 8);
            MessageID += (buf[offset++]);
            len -= 2;
            Topics = new List<Topic>();
            while (len > 0)
            {
                Topic to = new Topic("", (QoS)(buf[offset++] >> 1));
                len--;
                Topics.Add(to);
            }
            return true;
        }
    }
}
