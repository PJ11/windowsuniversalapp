﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace HaloWP8.mqtt
{
    public class DISCONNECT : MqttObj
    {
        public static byte TYPE = (byte)CmdType.DISCONNECT;

        public DISCONNECT()
            : base(TYPE) { }

        public bool Decode(BinaryReader br, int len)
        {
            if ( len == 0)
                return true;
            return false;   
        }

        public byte[] Encode()
        {
            byte[] ret = new byte[2];
            ret[0] = Header;
            ret[1] = 0;
            return ret;
        }

        public override bool Decode(byte[] buf, int offset)
        {
            return (Length == 0);
        }
    }
}
