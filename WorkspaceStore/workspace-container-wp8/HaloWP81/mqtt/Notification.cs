﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace HaloWP8.mqtt
{
    public class Notification
    {
        public short NID { get; private set;}
        public string UDID { get; private set; }
        //public string Message { get; private set; }
        public byte[] Payload { get; set; }

        private static long mNextNID = 0;
        public static short NextNID { get { return (short)Interlocked.Increment(ref mNextNID ); } }

        public Notification(PUBLISH pub)
        {
            this.NID = NextNID;
            this.UDID = pub.Topics.name;
            this.Payload = pub.Payload;
        }
    }
}
