﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HaloWP8.mqtt
{
    public class PINGRESP : MqttObj
    {
        public static byte TYPE = (byte)CmdType.PINGRESP;

        public PINGRESP()
            : base(TYPE)
        { }

        public byte[] Encode()
        {
            byte[] ret = new byte[2];
            ret[0] = Header;
            ret[1] = (byte)(0);
            return ret;
        }
        static public byte[] Encode(bool d)
        {
            byte[] ret = new byte[2];
            ret[0] = (byte)(TYPE << 4); 
            ret[1] = (byte)(0);
            return ret;
        }

        public override bool Decode(byte[] buf, int offset)
        {
            return Length == 0;
        }

        internal bool Decode(BinaryReader br, int len)
        {
            if (len == 0)
                return true;
            return false;   
        }
    }
}
