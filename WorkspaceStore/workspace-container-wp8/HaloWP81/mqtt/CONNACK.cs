﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HaloWP8.mqtt
{
    public class CONNACK : MqttObj
    {
        public static byte TYPE = (byte)CmdType.CONNACK;
        public ConnectReturnCode ConnRetCode { get; set; }

        public CONNACK()
            : base(TYPE)
        { }

        public byte[] Encode()
        {
            byte[] ret = new byte[4];
            ret[0] = (byte)(TYPE << 4);
            ret[1] = 2;
            ret[2] = 0;
            ret[3] = (byte)ConnRetCode;
            return ret;
        }


        public bool Decode(BinaryReader br, int count)
        {
            if (count != 2)
                return false;
            if (br.ReadByte() != 0)
                return false;
            ConnRetCode = (ConnectReturnCode)br.ReadByte();
            return true;
        }

        public static byte[] Encode(ConnectReturnCode connectReturnCode)
        {
            byte[] ret = new byte[4];
            ret[0] = (byte)(TYPE << 4);
            ret[1] = 2;
            ret[2] = 0;
            ret[3] = (byte)connectReturnCode;
            return ret;
        }

        public override bool Decode(byte[] buf, int offset)
        {
            offset += 2;
            if (buf[offset++] != 0)
                return false;
            ConnRetCode = (ConnectReturnCode)buf[offset];
            return true;
        }
    }
}
