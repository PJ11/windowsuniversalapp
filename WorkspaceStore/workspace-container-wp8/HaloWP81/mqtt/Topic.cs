﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HaloWP8.mqtt
{
    public class Topic
    {
        public string name { get; set; }
        public QoS qoS { get; set; }

        public Topic(string p, QoS qoS)
        {
            this.name = p;
            this.qoS = qoS;
        }

        public int LenghtBytes
        {
            get { return 3 + name.Length; }
        }
    }
}
