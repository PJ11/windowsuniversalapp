﻿using System;

namespace HaloWP8.mqtt
{
    public interface IDWMqttCallback
    {
        bool getIsRegister();

        String getDeviceID();

        void connectionLost();

        void closeMqttWorker();

        bool publishArrived(String name, String payload, QoS qos, bool mReturn);

        void Connected();
    }
}
