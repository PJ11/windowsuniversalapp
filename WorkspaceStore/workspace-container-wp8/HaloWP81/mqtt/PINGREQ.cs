﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace HaloWP8.mqtt
{
    public class PINGREQ : MqttObj
    {
        public static byte TYPE = (byte)CmdType.PINGREQ;

        public PINGREQ()
            : base(TYPE)
        { }

        public bool Decode(BinaryReader br, int len)
        {
            if (len == 0)
                return true;
            return false;   
        }

        public byte[] Encode()
        {
            byte[] bb = new byte[2];
            bb[0] = Header;
            bb[1] = 0;
            return bb;
        }

        public override bool Decode(byte[] buf, int offset)
        {
            return Length == 0;
        }
    }
}
