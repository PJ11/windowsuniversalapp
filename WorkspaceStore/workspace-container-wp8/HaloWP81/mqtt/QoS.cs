﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloWP8.mqtt
{
    public enum QoS
    {
        AT_MOST_ONCE,
        AT_LEAST_ONCE,
        EXACTLY_ONCE
    }
   	public enum CmdType {
		Reserved0, 
		CONNECT,// 1 Client request to connect to Server 
		CONNACK,// 2 Connect Acknowledgment 
		PUBLISH,// 3 Publish message 
		PUBACK,// 4 Publish Acknowledgment 
		PUBREC,// 5 Publish Received (assured delivery part 1) 
		PUBREL,// 6 Publish Release (assured delivery part 2) 
		PUBCOMP,// 7 Publish Complete (assured delivery part 3) 
		SUBSCRIBE,// 8 Client Subscribe request 
		SUBACK,// 9 Subscribe Acknowledgment 
		UNSUBSCRIBE,// 10 Client Unsubscribe request 
		UNSUBACK,// 11 Unsubscribe Acknowledgment 
		PINGREQ,// 12 PING Request 
		PINGRESP,// 13 PING Response 
		DISCONNECT,// 14 Client is Disconnecting 
		Reserved15// 15 Reserved 
	}

	public enum ConnectReturnCode
	{
		ConnectionAccepted,
		unacceptableprotocolversion,
		identifierrejected,
		serverunavailable,
		badusernameorpassword,
		notauthorized,
		Reserved
	}

    ////public static final CmdType CmdT = CmdType.Reserved0;
    //public CmdType CmdT() { return CmdType.values()[((short)((header >> 4) & 0x0f))]; }	
	
	
    //public byte header;
    //public byte length;
    
    //public MqttObj(byte type) {
    //    commandType(type);
    //}

    //public MqttObj commandType(int type) {
    //    this.header &= 0x0F;
    //    this.header |= (byte)((type << 4) & 0xF0);
    //    return this;
    //}
    //public QoS qos;
    //public QoS getQos() {
    //    return QoS.values()[((header & 0x06) >> 1)];
    //}
    //public void setQos(QoS v){
    //    this.header &= 0xF9;
    //    this.header |= (byte)(((byte)v.ordinal() << 1) & 0x06);
    //}
    
    //public static void writeUTF( ByteArrayOutputStream bb, String buffer) throws IOException {
    //    int len = buffer.length();
    //    bb.write((byte)(len >> 8));
    //    bb.write((byte)(len & 0xFF));
    //    bb.write(buffer.getBytes());
    //}
    
    //public static String readUTF(byte[] buf, int offset) throws IOException{
    //    short size = (short)(buf[offset++] << 8);
    //    size += buf[offset++];
    //    if (buf.length < size)
    //    {
    //        throw new IOException("Invalid message encoding");
    //    }
    //    return new String(buf,offset,size);
    //}    
    
    //public abstract boolean Decode(byte[] buf, int offset);  
}
