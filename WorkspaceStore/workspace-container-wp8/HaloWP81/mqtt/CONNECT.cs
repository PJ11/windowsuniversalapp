﻿using HaloWP81.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace HaloWP8.mqtt
{
    public class CONNECT : MqttObj
    {
        public static byte TYPE = (byte)CmdType.CONNECT;

        public static byte[] PROTOCOL_NAME = Encoding.UTF8.GetBytes("\x00\x06MQIsdp");
        public static byte PROTOCOL_VERSION = 3;
        public static byte PROTOCOL_BRIDGE_VERSION = 1;

        public short keepAlive { get; set; }
        public String clientId { get; set; }
        public String willTopic { get; set; }
        public String willMessage { get; set; }
        public String userName { get; set; }
        public String password { get; set; }
        public int version { get; set; }
        public byte ConnectionFlags { get; set; }

        public bool CleanSession
        {
            get
            {
                return (ConnectionFlags & 0x2) != 0;
            }
            set
            {
                if (value)
                {
                    ConnectionFlags |= 0x2;
                }
                else
                {
                    ConnectionFlags &= 0xFD;
                }
            }
        }

        public byte willQos
        {
            get
            {
                return (byte)((ConnectionFlags & 0x18) >> 3);
            }
        }

        public bool willRetain
        {
            get
            {
                return (ConnectionFlags & 0x20) != 0;
            }
        }

        public CONNECT()
            : base(TYPE)
        {
            keepAlive = 1000;
            CleanSession = true;
            qos = QoS.AT_LEAST_ONCE;
        }

        public CONNECT(MqttObj cmd)
            : base(TYPE)
        {
            this.Length = cmd.Length;
        }

        public byte[] Encode()
        {
            List<byte> buffers = new List<byte>(37);
            buffers.Add(Header);
            buffers.Add(0);
            buffers.AddRange(PROTOCOL_NAME);
            buffers.Add(PROTOCOL_VERSION);
            buffers.Add(ConnectionFlags);
            buffers.Add((byte)(keepAlive >> 8));
            buffers.Add((byte)(keepAlive & 0xFF));
            writeUTF(ref buffers, clientId);
            buffers[1] = (byte)(buffers.Count - 2);
            return buffers.ToArray();
        }

        static bool ByteArrayCompare(byte[] a1, byte[] a2, int offset)
        {
            if (a2.Length < a1.Length)
                return false;

            for (int i = 0; i < a1.Length; i++)
                if (a1[i] != a2[offset + i])
                    return false;

            return true;
        }

        public bool Decode(BinaryReader br)
        {
            //            if (readUTF(br) != CONNECT.PROTOCOL_NAME)
            //              return false;
            version = br.ReadByte();
            if (version != CONNECT.PROTOCOL_VERSION && version != CONNECT.PROTOCOL_BRIDGE_VERSION)
                return false;
            ConnectionFlags = br.ReadByte();


            //userName
            //password

            keepAlive = IPAddress.HostToNetworkOrder(br.ReadInt16());

            // variable Header
            clientId = readUTF(br);
            return true;
        }

        public override bool Decode(byte[] buf, int offset)
        {
            offset += 2;
            if (!ByteArrayCompare(PROTOCOL_NAME, buf, offset))
                return false;
            offset += 8;
            byte version = buf[offset++];
            if (version != CONNECT.PROTOCOL_VERSION && version != CONNECT.PROTOCOL_BRIDGE_VERSION)
                return false;
            ConnectionFlags = buf[offset++];

            ////userName
            ////password

            keepAlive = (short)(buf[offset++] << 8);
            keepAlive += (buf[offset++]);

            //// variable Header
            clientId = readUTF(buf, offset);
            return true;
        }



    }
}
