﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloWP8.mqtt
{
    public abstract class MqttObj
    {
        public enum QoS
        {
            AT_MOST_ONCE,
            AT_LEAST_ONCE,
            EXACTLY_ONCE
        }

        public enum CmdType
        {
            Reserved0,
            CONNECT,// 1 Client request to connect to Server 
            CONNACK,// 2 Connect Acknowledgment 
            PUBLISH,// 3 Publish message 
            PUBACK,// 4 Publish Acknowledgment 
            PUBREC,// 5 Publish Received (assured delivery part 1) 
            PUBREL,// 6 Publish Release (assured delivery part 2) 
            PUBCOMP,// 7 Publish Complete (assured delivery part 3) 
            SUBSCRIBE,// 8 Client Subscribe request 
            SUBACK,// 9 Subscribe Acknowledgment 
            UNSUBSCRIBE,// 10 Client Unsubscribe request 
            UNSUBACK,// 11 Unsubscribe Acknowledgment 
            PINGREQ,// 12 PING Request 
            PINGRESP,// 13 PING Response 
            DISCONNECT,// 14 Client is Disconnecting 
            Reserved15// 15 Reserved 
        }

        public enum ConnectReturnCode
        {
            ConnectionAccepted,
            unacceptableprotocolversion,
            identifierrejected,
            serverunavailable,
            badusernameorpassword,
            notauthorized,
            Reserved
        }

        //public static final CmdType CmdT = CmdType.Reserved0;
        public CmdType CmdT()
        {
            short itemEntry = ((short)((header >> 4) & 0x0f));
            return (CmdType)itemEntry;
        }


        public byte header;
        public byte length;

        public MqttObj(byte type)
        {
            commandType(type);
        }

        public MqttObj commandType(int type)
        {
            this.header &= 0x0F;
            this.header |= (byte)((type << 4) & 0xF0);
            return this;
        }
        public QoS qos;
        public QoS getQos()
        {
            int itemEntry = ((header & 0x06) >> 1);
            return (QoS)itemEntry;
        }
        public void setQos(QoS v)
        {
            byte qosOrdinal = (byte)v;
            this.header &= 0xF9;
            this.header |= (byte)((qosOrdinal << 1) & 0x06);
        }

        public static void writeUTF(BinaryWriter bb, String buffer)
        {
            int len = buffer.Length;
            //using (MemoryStream stream = new MemoryStream())
            {
                //using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    bb.Write((byte)(len >> 8));
                    bb.Write((byte)(len & 0xFF));
                    byte[] bytes = Encoding.BigEndianUnicode.GetBytes(buffer);  // TODO: Encoding.BigEndianUnicode or just Encoding.UTF8?
                    bb.Write(bytes);
                }
                //finalBytesToSend = stream.ToArray();
            }
#if ANDROID
        bb.write((byte)(len >> 8));
        bb.write((byte)(len & 0xFF));
        bb.write(buffer.getBytes());
#endif
        }

        public static String readUTF(byte[] buf, int offset)
        {
            short size = (short)(buf[offset++] << 8);
            size += buf[offset++];
            if (buf.Length < size)
            {
                throw new IOException("Invalid message encoding");
            }

            return Encoding.UTF8.GetString(buf, offset, size);  // Comes from Java.  Do I need Encoding.BigEndianUnicode?
        }

        public abstract bool Decode(byte[] buf, int offset);
    }

    public class Topic 
    {
        public string name;
        public HaloWP8.mqtt.MqttObj.QoS mQoS;

        public Topic()
        { 
        }

        public Topic(string p, HaloWP8.mqtt.MqttObj.QoS qoS)
        {
            this.name = p;
            this.mQoS = qoS;
        }

        public int LenghtBytes() 
        {
            return 3 + name.Length;
        }	
    }

    public class PUBACK : MqttObj
    {

        public static byte TYPE = (byte)CmdType.PUBACK;

        public int MessageID;

        public PUBACK()
            : base(TYPE)
        {
        }

        public byte[] Encode()
        {
            byte[] ret = new byte[4];
            ret[0] = (byte)(TYPE << 4);
            ret[1] = 2;
            ret[2] = (byte)(MessageID >> 8);
            ret[3] = (byte)(MessageID & 0xFF);
            return ret;
        }

        public override bool Decode(byte[] buf, int offset)
        {
            offset += 2;
            if (length < 2)
                return false;
            MessageID = ((buf[offset++] & 0xFF) << 8) | (buf[offset++] & 0xFF);
            return true;
        }
    }

    public class DISCONNECT : MqttObj
    {
        public static byte TYPE = (byte)CmdType.DISCONNECT;

        public DISCONNECT()
            : base(TYPE)
        {
        }

        public byte[] Encode()
        {
            qos = QoS.AT_LEAST_ONCE;
            byte[] ret = new byte[2];
            ret[0] = header;
            ret[1] = 0;
            return ret;
        }

        public override bool Decode(byte[] buf, int offset)
        {
            // TODO Auto-generated method stub
            return false;
        }
    }

    public class CONNECT : MqttObj
    {
        public static byte TYPE = 1;
    
        public static byte[] PROTOCOL_NAME = new byte[] { 0x00, 0x06, (byte)'M', (byte)'Q', (byte)'I', (byte)'s', (byte)'d', (byte)'p' };
        public static byte PROTOCOL_VERSION = 3;

        private int keepAlive = 30;
        private String clientId;
    
        public byte ConnectionFlags;
    
        public CONNECT()
            :base(TYPE)
        {
        }
    
        public byte[] Encode()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(header);
                    writer.Write((byte)0); //place for big length
                    writer.Write(PROTOCOL_NAME);
                    writer.Write(PROTOCOL_VERSION);
                    writer.Write(ConnectionFlags);
                    writer.Write((byte)(keepAlive >> 8));
                    writer.Write((byte)(keepAlive & 0xFF));
                    writeUTF(writer, clientId);
                    byte[] bOut = stream.ToArray();
                    bOut[1] = (byte)(bOut.Length - 2);
                    return bOut;
                }
            }
#if ANDROID
    	    ByteArrayOutputStream buffers = new ByteArrayOutputStream(39);
            buffers.write(header);
            buffers.write((byte)0); //place for big length
            buffers.write(PROTOCOL_NAME);
            buffers.write(PROTOCOL_VERSION);
            buffers.write(ConnectionFlags);
            buffers.write((byte)(keepAlive >> 8));
            buffers.write((byte)(keepAlive & 0xFF));
            writeUTF(buffers, clientId);
            byte[] bOut = buffers.toByteArray();
            bOut[1] = (byte)(bOut.Length - 2); 
            return bOut;
#endif
        }

        public CONNECT cleanSession(bool value) 
        {
            if (value)
            {
                ConnectionFlags |= 0x2;
            }
            else {
                ConnectionFlags &= 0xFD;
            }
            return this;
        }

        public CONNECT setClientId(String clientId) 
        {
            this.clientId = clientId;
            return this;
        }

        public CONNECT setKeepAlive(int keepAlive) 
        {
            this.keepAlive = keepAlive;
            return this;
        }
    
        public static byte setHeader() 
        {
            return (byte)((byte)CmdType.CONNECT << 4);
        }

        public override bool Decode(byte[] buf, int offset) 
        {
            return false;
        }
    }

    public class ByteBuffer : List<byte>
    {
        protected int currentIndex;
        protected ByteBuffer inst = null;

        public ByteBuffer allocate(int Capacity)
        {
            if(inst == null)
            {
                inst = new List<byte>(Capacity) as ByteBuffer;
                currentIndex = 0;
            }
            return inst;
        }

        protected ByteBuffer()
        {
            currentIndex = 0;
        }

        public ByteBuffer put(byte by)
        {
            inst.Add(by);
            currentIndex++;
            return this;
        }

        public byte get()
        {
            byte result = this[currentIndex];
            this.RemoveAt(currentIndex);
            currentIndex--;
            return result;
        }
    }

    public class CONNACK : MqttObj
    {
        public static byte TYPE = (byte)CmdType.CONNACK;
        public ConnectReturnCode ConnRetCode;

        public CONNACK()
            : base(TYPE)
        {
        }

        public bool Decode(ByteBuffer bbWr)
        {
            if (bbWr.get() != 0)
                return false;
            ConnectReturnCode connRet = (ConnectReturnCode)bbWr.get();
            if (connRet != ConnectReturnCode.ConnectionAccepted)
                return false;
            return true;
        }

        public override bool Decode(byte[] buf, int offset)
        {
            offset += 2;
            if (buf[offset++] != 0)
                return false;
            ConnRetCode = (ConnectReturnCode)buf[offset];
            return true;
        }
    }

    public class PUBLISH : MqttObj
    {
        public static byte TYPE = (byte)CmdType.PUBLISH;
        public int MessageID;
        public Topic Topics;
        public byte[] Payload;
        public bool MReturn;
	
        public PUBLISH() 
            : base(TYPE)
        {
            qos = QoS.AT_LEAST_ONCE;
            MessageID = 0x1234;
        }

        public override bool Decode(byte[] buf, int offset) 
        {
            header = buf[offset];
            MReturn = ((header & 0x01) == 1) ? true : false;
            QoS subQoS = (QoS)((header >> 1) & 0x03);

            int len = length;
            offset += 2;
            string s;
            try
            {
                s = readUTF(buf, offset);
                int len2 = s.Length + 2; 
                len -= len2;
                offset += len2;
            }    
            catch (IOException) 
            {
                return false; 
            }

            Topics = new Topic(s, subQoS);

            MessageID = 0;
            if (subQoS != QoS.AT_MOST_ONCE)
            {
                MessageID = ((int)(buf[offset++])) << 8;
                MessageID |= (int)(buf[offset++] & 0xFF);
                len -= 2;
            }
        
            Payload = new byte[len];
            Array.Copy(buf, offset, Payload, 0, len);
            return true;
        }
    }

    public class SUBSCRIBE : MqttObj 
    {
        public static byte TYPE = (byte) CmdType.SUBSCRIBE;
        public static Topic[] NO_TOPICS = new Topic[0];

        private int MessageId = 10;
        private Topic [] topics = NO_TOPICS;

        public SUBSCRIBE() 
            : base (TYPE)
        {
            setQos(QoS.AT_LEAST_ONCE);
        }

        public byte messageType() 
        {
            return TYPE;
        }

        public byte[] Encode()
        {
            QoS _qos = qos;
            
#if ANDROID
            ByteArrayOutputStream buffers = new ByteArrayOutputStream(39);
            buffers.write(header);
            buffers.write((byte)0); //place for big length
            if (_qos != QoS.AT_MOST_ONCE)
            {
                buffers.write((byte)(MessageId >> 8));
                buffers.write((byte)(MessageId & 0xFF));
            }

            foreach (Topic topic in topics)
            {
                writeUTF(buffers, topic.name);
                buffers.write((byte)topic.qoS.ordinal());
            }
	    
            byte[] bOut = buffers.toByteArray();
            bOut[1] = (byte)(bOut.Length - 2);
            return bOut; 
#else
            using (MemoryStream stream = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(header);
                    writer.Write((byte)0); //place for big length

                    if (_qos != QoS.AT_MOST_ONCE)
                    {
                        writer.Write((byte)(MessageId >> 8));
                        writer.Write((byte)(MessageId & 0xFF));
                    }

                    foreach (Topic topic in topics)
                    {
                        writeUTF(writer, topic.name);
                        writer.Write((byte)topic.mQoS);
                    }
                    
                    byte[] bOut = stream.ToArray();
                    bOut[1] = (byte)(bOut.Length - 2);
                    return bOut;
                }
            }
#endif
        }    
    
    
        public Topic[] getTopics() 
        {
            return topics;
        }

        public SUBSCRIBE setTopics(Topic[] topics)
        {
            this.topics = topics;
            return this;
        }

        public override bool Decode(byte[] buf, int offset) {
        // TODO Auto-generated method stub
        return false;
        }
    }

    public class PUBREC : MqttObj
    {
        public static byte TYPE = (byte)CmdType.PUBREC;
	
        public PUBREC() 
            : base(TYPE)
        {
        }
	
        public int MessageID;

        public byte[] Encode() 
        {
            byte[] ret = new byte[4];
            ret[0] = (byte)(TYPE << 4);
            ret[1] = 2;
            ret[2] = (byte)(MessageID >> 8);
            ret[3] = (byte)(MessageID & 0xFF);
            return ret;
        }

        public override bool Decode(byte[] buf, int offset) 
        {
            offset += 2;
            if (length < 2)
                return false;
            MessageID = ((buf[offset++] & 0xFF) << 8) | (buf[offset++] & 0xFF);
            return true;
        }
    }

    public class PINGREQ : MqttObj
    {
        public static byte TYPE = (byte)CmdType.PINGREQ;
	
        public PINGREQ() 
            :base (TYPE)
        {
        }
	
        public byte[] Encode() 
        {
            byte[] bb = new byte[2];
            bb[0] = header;
            bb[1] = 0;
            return bb;
        }

        public override bool Decode(byte[] buf, int offset) 
        {
            // TODO Auto-generated method stub
            return false;
        }
    }

    public class PINGRESP : MqttObj
    {
        public static byte TYPE = (byte)CmdType.PINGREQ;
        
        public PINGRESP()
            : base(TYPE)
        {
        }

        public override bool Decode(byte[] buf, int offset)
        {
            return length == 0;
        }
    }

    public class SUBACK : MqttObj
    {
        public static byte TYPE = (byte)CmdType.SUBACK;
        public int MessageID;
        public List<Topic> Topics;
        
        public SUBACK()
            : base(TYPE)
        {
            setQos(QoS.AT_LEAST_ONCE);
        }

        public override bool Decode(byte[] buf, int offset)
        {
            offset += 2;
            if (length < 2)
                return false;
            int len = length;
            MessageID = (int)(buf[offset++] << 8);
            MessageID |= (int)(buf[offset++]);
            len -= 2;
            Topics = new List<Topic>();
            while (len > 0)
            {
                Topic to = new Topic("", (QoS)(buf[offset++] >> 1));
                len--;
                Topics.Add(to);
            }
            return true;
        }
    }

    public class UNSUBACK : MqttObj
    {
        public static byte TYPE = (byte)CmdType.UNSUBACK;
        public UNSUBACK()
            : base(TYPE)
        {
            // TODO Auto-generated constructor stub
        }

        public override bool Decode(byte[] buf, int offset)
        {
            // TODO Auto-generated method stub
            return false;
        }
    }
}
