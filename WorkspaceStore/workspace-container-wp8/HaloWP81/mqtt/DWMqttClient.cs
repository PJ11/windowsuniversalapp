﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;

namespace HaloWP8.mqtt
{
    public class DWMqttClient : IDWMqttClient, IDisposable
    {
        public enum ConnectionState
        {
            NotConnected,
            Connecting,
            Connected,
            Fail
        }


        static ManualResetEvent _clientDone = new ManualResetEvent(false);
        static ManualResetEvent _clientDoneSend = new ManualResetEvent(false);

        private StreamSocket mSocket = null;
        const int TIMEOUT_MILLISECONDS = 5000;
        const int MAX_BUFFER_SIZE = 2048;

        private short keepAliveSeconds = 120;
        private String mDeviceID;

        public delegate void ConnAckHandle();
        public event ConnAckHandle ConnAckEv;
        public delegate void PublishHandle(string str, byte[] payload, QoS subQoS, bool ret);
        public event PublishHandle PublishEv;
        public delegate void SubAckHandle();
        public event SubAckHandle SubAckEv;
        public delegate void UnsubAckHandle(short id);
        public event UnsubAckHandle UnsubAckEv;
        public delegate void ConnectionLostHandle();
        public event ConnectionLostHandle ConnectionLostEv;
        public delegate void OnExitHandle();
        public event OnExitHandle OnExitEv;

        //.......................

        ManualResetEvent mStopEvent = new ManualResetEvent(false);

        AutoResetEvent mNetworkChange = new AutoResetEvent(false);

        Task mTask = null;
        int mTimeToWait = 15000;
        int mConnectTried = 0;
        
        IDWMqttCallback mCallback;
        bool bCleanSession = true;
        bool bExit = false;
        private String SERVERIP;
        private int SERVERPORT;
        private int keepAliveTime = 30000;
        private static int DEFAULT_KA = 120000;




        private ConnectionState MQTTConnectionState { get; set; }


        private static DWMqttClient _inst;

        internal static IDWMqttClient getInstance()
        {
            if (_inst == null)// || !_inst.isAlive())
            {
                _inst = new DWMqttClient();

                _inst.ConnAckEv += _inst._inst_ConnAckEv;

            }
            return _inst;
        }

        internal static void clearInstance()
        {
            if(_inst != null)
            {
                _inst.Dispose();
                _inst = null;
            }
        }

        void _inst_ConnAckEv()
        {
            if (mCallback!=null) {
                mCallback.Connected();
            }

        }

        public async Task<bool> start(string host, int port, string deviceID, bool cleanSt, int kaSec, IDWMqttCallback cb) 
        {
            mDeviceID = deviceID;
            mCallback = cb;
            SERVERIP = host;
            SERVERPORT = port;

            keepAliveTime = (kaSec - 10) * 1000;
            if (keepAliveTime < 0)
            {
                keepAliveTime = DEFAULT_KA;
            }

            if (! await disconnect())
                return false;

            bExit = false;
            mStopEvent = new ManualResetEvent(false);

            mTask = new Task(async () =>
                {
                    do
                    {
                        try
                        {
                            MQTTConnectionState = ConnectionState.Connecting;

                            string result = string.Empty;

                            mSocket = new StreamSocket();

                            // I guess StreamSocket.ConnectAsync() throws an exception if error, so if we don't get an exception
                            //     then it successfully connected!
                            await mSocket.ConnectAsync(new Windows.Networking.HostName(SERVERIP), SERVERPORT.ToString());
                            
                            MQTTConnectionState = ConnectionState.Connected;

                            _clientDone.WaitOne(TIMEOUT_MILLISECONDS);

                            if (MQTTConnectionState == ConnectionState.Connected)
                            {
                                if (SendConnect())
                                {
                                    mTimeToWait = 3000;
                                    ReadDataFromStream();
                                }
                            }
                        }
                        catch (IOException e)
                        {
                            //TNTrace.Instance.E("IOError1", e);
                        }
                        catch (Exception e)
                        {
                            MQTTConnectionState = ConnectionState.NotConnected;
                        }
                        finally
                        {
                            try
                            {
                                if (mSocket != null)
                                    mSocket.Dispose();
                            }
                            catch (IOException e)
                            {
                            }
                            catch (Exception e)
                            {
                            }
                        }

                        WaitHandle[] whs = new WaitHandle[] { mStopEvent, mNetworkChange };
                        int retEv = WaitHandle.WaitAny(whs, mTimeToWait);
                        if (retEv == 0)
                            break;
                        if (retEv == 1)
                        {
                            mConnectTried = 0;
                            mTimeToWait = 3000;
                        }

                        ++mConnectTried;
                        if (mConnectTried > 8)
                            mTimeToWait *= 2;
                        MQTTConnectionState = ConnectionState.NotConnected;

                        ConnectionLostHandle ev = ConnectionLostEv;
                        if (ev != null)
                        {
                            try
                            {
                                ev();
                            }
                            catch (Exception e)
                            {
                                //TNTrace.Instance.E("Error3", e);
                            }
                        }
                        if (mStopEvent.WaitOne(3000))
                            break;
                    } while (true);

                    OnExitHandle evExit = OnExitEv;
                    if (evExit != null)
                    {
                        evExit();
                    }
                });

            return true;
        }

        public async Task<bool> disconnect()
        {
            bExit = true;
            SendDisconnect();

            notyfyMainTh();

            int retry = 50;
            while (isMQTTAlive() || retry-- > 0)
            {

                if (!isMQTTAlive())
                    break;
                try
                {
                    await Task.Delay(100);
                }
                catch (Exception e)
                {
                    //
                }
            }
            return !isMQTTAlive();
        }

        private bool isMQTTAlive()
        {
            if(mTask != null)
                return !mTask.IsCanceled && !mTask.IsCompleted && !mTask.IsFaulted;
            return false;
        }

        private void notyfyMainTh()
        {
            mStopEvent.Set();
            _clientDone.Set();
        }

        public void notifyNetworkChange()
        {
            mNetworkChange.Set();
            _clientDone.Set();
        }

        public void run()
        {
        }

        public async Task<int> Receive(byte[] response)
        {
            int nRespCount = 0;
            while (mSocket != null)
            {
                using (DataReader dataReader = new DataReader(mSocket.InputStream))
                {
                    // Read first 4 bytes (length of the subsequent bytes).
                    uint sizeFieldCount = await dataReader.LoadAsync(sizeof(uint));
                    if (sizeFieldCount != sizeof(uint))
                    {
                        // The underlying socket was closed before we were able to read the whole data.
                        nRespCount = 0;
                        return -1;
                    }

                    // Read the string.
                    uint stringLength = dataReader.ReadUInt32();
                    uint actualStringLength = await dataReader.LoadAsync(stringLength);
                    if (stringLength != actualStringLength)
                    {
                        // The underlying socket was closed before we were able to read the whole data.
                        nRespCount = 0;
                        return -1;
                    }
                    dataReader.ReadBytes(response);
                }
            }
            _clientDone.Reset();
            return 0;
        }

        public async Task<MemoryStream> ReadDataFromStream()
        {
            byte[] mBuffer = new byte[MAX_BUFFER_SIZE];
            int BytesTransferred = 0;

            while (true)
            {
                if (mStopEvent.WaitOne(0))
                    return null;

                int bRead = await Receive(mBuffer);
                if (bRead <= 0)
                {
                    return null;
                }

                int off = 0;
                BytesTransferred += bRead;
                do
                {
                    if (off >= BytesTransferred)
                        break;

                    byte cmd = mBuffer[off]; off++;
                    if (cmd < 0x10 || cmd >= 0xf0)
                    {
                        //CloseRecClientSocket(receiveEventArgs);
                        return null;
                    }

                    int multiplier = 1;
                    int Length = 0;
                    byte c = 0;
                    do
                    {
                        c = mBuffer[off]; off++;
                        Length += (c & 0x7f) * multiplier;
                        multiplier *= 0x80;
                    } while ((c & 0x80) != 0);

                    if (Length > BytesTransferred - off)
                        break;

                    using (MemoryStream ms = new MemoryStream(mBuffer, off, Length))
                    {
                        using (BinaryReader br = new BinaryReader(ms))
                        {
                            CmdType cmdA = (CmdType)((short)((cmd >> 4) & 0x0f));

                            switch (cmdA)
                            {
                                case CmdType.CONNECT:
                                    {
                                        CONNECT conn = new CONNECT();
                                        if (!conn.Decode(br))
                                        {
                                            return null;
                                        }
                                    }
                                    break;
                                case CmdType.CONNACK:// 2 Connect Acknowledgment 
                                    {
                                        CONNACK connack = new CONNACK();
                                        if (!connack.Decode(br, Length))
                                            return null;
                                        MQTTConnectionState = ConnectionState.Connected;
                                        ConnAckHandle eh = ConnAckEv;
                                        if (eh != null)
                                        {
                                            eh();
                                        }
                                        else
                                        {
                                            SendSubscribe(this.mDeviceID, QoS.AT_LEAST_ONCE);
                                        }
                                    }
                                    break;
                                case CmdType.PUBLISH:// 3 Publish message 
                                    {
                                        PUBLISH pub = new PUBLISH(cmd);
                                        if (!pub.Decode(br,Length))
                                        {
                                            return null;
                                        }
                                        PublishHandle eh = PublishEv;
                                        if (eh != null)
                                        {
                                            eh(pub.Topics.name, pub.Payload, pub.qos, pub.MReturn);
                                        }
                                        if (mCallback != null)
                                        {
                                            if (!mCallback.publishArrived(pub.Topics.name, pub.Message, pub.qos, pub.MReturn))
                                                mStopEvent.Set();
                                        }

                                        if (pub.qos == QoS.AT_LEAST_ONCE)
                                            SendPubAck(pub.MessageID);
                                        else if (pub.qos == QoS.EXACTLY_ONCE)
                                            SendPubRec(pub.MessageID);
                                    }
                                    break;
                                case CmdType.PUBACK:// 4 Publish Acknowledgment 
                                    break;
                                case CmdType.PUBREC:// 5 Publish Received (assured delivery part 1) 
                                    break;
                                case CmdType.PUBREL:// 6 Publish Release (assured delivery part 2) 
                                    break;
                                case CmdType.PUBCOMP:// 7 Publish Complete (assured delivery part 3) 
                                    break;
                                case CmdType.SUBSCRIBE:// 8 Client Subscribe request 
                                    {
                                        SUBSCRIBE subb = new SUBSCRIBE();
                                        if (!subb.Decode(br,Length))
                                        {
                                            return null;
                                        }
                                    }
                                    break;
                                case CmdType.SUBACK:// 9 Subscribe Acknowledgment 
                                    {
                                        SUBACK suback = new SUBACK();
                                        if (!suback.Decode(br, Length))
                                            return null;
                                        SubAckHandle eh = SubAckEv;
                                        if (eh != null)
                                        {
                                            eh();
                                        }
                                    }
                                    break;
                                case CmdType.UNSUBSCRIBE:// 10 Client Unsubscribe request 
                                    {
                                        UNSUBSCRIBE usub = new UNSUBSCRIBE();
                                        if (!usub.Decode(br,Length))
                                        {
                                            return null;
                                        }
                                    }
                                    break;
                                case CmdType.UNSUBACK:// 11 Unsubscribe Acknowledgment 
                                    break;
                                case CmdType.PINGREQ:// 12 PING Request 
                                    {
                                        PINGREQ ping = new PINGREQ();
                                        if (!ping.Decode(br,Length))
                                        {
                                            return null;
                                        }
                                    }
                                    break;
                                case CmdType.PINGRESP:// 13 PING Response 
                                    {
                                        PINGRESP presp = new PINGRESP();
                                        if (!presp.Decode(br, Length))
                                        {
                                            return null;
                                        }
                                    }
                                    break;
                                case CmdType.DISCONNECT:// 14 Client is Disconnecting 
                                    {
                                        DISCONNECT ping = new DISCONNECT();
                                        if (!ping.Decode(br,Length))
                                        {
                                            return null;
                                        }
                                    }
                                    break;
                            }
                        }
                    }

                    off += Length;
                    BytesTransferred -= Length + 2;

                } while (true);
            }
        }

        private bool SendData(byte[] cmdBytes)
        {
            if (cmdBytes != null)
            {
                bool response = false;
#if OLDWAY
                if (mSocket != null)
                {
                    SocketAsyncEventArgs socketEventArg = new SocketAsyncEventArgs();

                    socketEventArg.RemoteEndPoint = mSocket.RemoteEndPoint;
                    socketEventArg.UserToken = null;
                    socketEventArg.Completed += new EventHandler<SocketAsyncEventArgs>(delegate(object s, SocketAsyncEventArgs e)
                    {
                        response = true;
                        _clientDoneSend.Set();
                    });

                    socketEventArg.SetBuffer(cmdBytes, 0, cmdBytes.Length);
                    _clientDoneSend.Reset();
                    mSocket.SendAsync(socketEventArg);
                    _clientDoneSend.WaitOne(TIMEOUT_MILLISECONDS);
                }
                else
                {
                    return false;
                }
#else
#endif
                return response;
            }
            return false;
        }

        private bool SendConnect()
        {
            try
            {
                if (mSocket != null /*&& mSocket.Connected*/)
                {
                    CONNECT connect = new CONNECT();
                    connect.keepAlive = keepAliveSeconds;
                    connect.clientId = mDeviceID;
                    connect.CleanSession = bCleanSession;
                    return SendData(connect.Encode());
                }
            }
            catch (Exception e) { }
            return false;
        }

        private bool SendSubscribe(string id, QoS qoS)
        {
            if (mSocket != null /*&& mSocket.Connected*/)
            {
                SUBSCRIBE subscr = new SUBSCRIBE();
                subscr.topicsList = new List<Topic> { new Topic(id, qoS) };
                try
                {
                    return SendData(subscr.Encode());
                }
                catch (Exception) { }
            }
            return false;
        }

        private bool SendPubAck(short pubId)
        {
            try
            {
                if (mSocket != null/* && mSocket.Connected*/)
                {
                    PUBACK pa = new PUBACK();
                    pa.MessageID = pubId;
                    return SendData(pa.Encode());
                }
            }
            catch (Exception) { }
            return false;
        }

        private bool SendPubRec(short pubId)
        {
            try
            {
                if (mSocket != null/* && mSocket.Connected*/)
                {
                    PUBREC pa = new PUBREC();
                    pa.MessageID = pubId;
                    return SendData(pa.Encode());
                }
            }
            catch (Exception) { }
            return false;

        }

        private bool SendPingReq()
        {
            try
            {
                if (mSocket != null /*mSocket != null && mSocket.Connected*/)
                {
                    PINGREQ prq = new PINGREQ();
                    return SendData(prq.Encode());
                }
            }
            catch (Exception) { }
            return false;
        }

        private bool SendDisconnect()
        {
            try
            {
                if (mSocket == null)
                    return true;

                if (mSocket != null/*mSocket.Connected*/)
                {
                    DISCONNECT dis = new DISCONNECT();
                    return SendData(dis.Encode());
                }
            }
            catch (Exception e)
            {
            }
            return false;
        }

        public void Subscribe()
        {
            SendSubscribe(this.mDeviceID, QoS.AT_LEAST_ONCE);
        }

        #region IDisposable Members

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    mStopEvent.Dispose();
                    mStopEvent = null;

                    mNetworkChange.Dispose();
                    mNetworkChange = null;

                    if (mSocket != null)
                    {
                        mSocket.Dispose();
                        mSocket = null;
                    }
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~DWMqttClient()
        {
            Dispose(false);
        }
        #endregion
    }
}
