﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using HaloWP81.Utils;

namespace HaloWP8.mqtt
{
    public class SUBSCRIBE : MqttObj
    {
        public static byte TYPE = (byte)CmdType.SUBSCRIBE;
        public List<Topic> topicsList = new List<Topic>();
        public short MessageId = 10;

        public SUBSCRIBE()
            : base(TYPE)
        {
            qos = QoS.AT_LEAST_ONCE;
        }
        public byte[] Encode()
        {
            QoS _qos = qos;
            int len = 0;
            if (_qos != QoS.AT_MOST_ONCE)
            {
                len += 2; // MessageId
            }
            foreach (Topic topic in topicsList)
            {
                len += topic.LenghtBytes;
            }

            byte[] bbLen = writeMsgLength((int)len);
            len += bbLen.Length;
            byte[] buffers = new byte[1 + len];
            int offset = 0;
            buffers[offset++] = Header;
            Buffer.BlockCopy(bbLen, 0, buffers, offset, bbLen.Length);
            offset += bbLen.Length;
            if (_qos != QoS.AT_MOST_ONCE)
            {
                buffers[offset++] = (byte)(MessageId >> 8);
                buffers[offset++] = (byte)(MessageId & 0xFF);
            }
            foreach (Topic topic in topicsList)
            {
                offset = writeUTF(buffers, offset, Encoding.UTF8.GetBytes(topic.name));
                buffers[offset++] = (byte)topic.qoS;
            }
            return buffers;
        }

        public bool Decode(BinaryReader br, int len)
        {
            MessageId = IPAddress.HostToNetworkOrder(br.ReadInt16());
            len -= 2;
            while (len > 0) //br.BaseStream.Position < br.BaseStream.Length
            {
                String s = "";
                len -= readUTF(br, ref s);
                Topic to = new Topic(s, (QoS)(br.ReadByte() >> 1));
                len--;
                topicsList.Add(to);
            }
            return true;
        }

        public override bool Decode(byte[] buf, int offset)
        {
            offset += 2;
            if (Length < 2)
                return false;
            int len = Length;
            MessageId = (short)(buf[offset++] << 8);
            MessageId += (buf[offset++]);
            len -= 2;
            while (len > 0) 
            {
                String s = String.Empty;
                len -= readUTF(buf, ref offset, ref s);
                Topic to = new Topic(s, (QoS)(buf[offset++] >> 1));
                len--;
                topicsList.Add(to);
            }
            return true;
        }
    }
}
