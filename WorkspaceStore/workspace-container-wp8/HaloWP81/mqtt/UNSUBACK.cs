﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HaloWP8.mqtt
{
    public class UNSUBACK : MqttObj
    {
        public static byte TYPE = (byte)CmdType.UNSUBACK;
        public List<String> Topics { get; set; }
        public short MessageID { get; set; }

        public UNSUBACK()
            : base(TYPE)
        { }


        public byte[] Encode()
        {
            byte[] ret = new byte[4];
            ret[0] = Header;
            ret[1] = (byte)(2);
            ret[2] = (byte)(MessageID >> 8);
            ret[3] = (byte)(MessageID & 0xFF);
            return ret;
        }

        public override bool Decode(byte[] buf, int offset)
        {
            return true;
        }
    }
}
