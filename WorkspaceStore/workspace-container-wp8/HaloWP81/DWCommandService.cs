﻿using HaloWP8.httpmon;
using HaloWP8.mqtt;
using HaloWP8.Utils;
using HaloPCL;
using HaloPCL.httpmon;
using HaloPCL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HaloWP8
{
    /// <summary>
    /// TODO WP8 equivalent of Context object?
    /// </summary>
    public class Context
    {
        // Empty class.  REMOVE LATER!
    }

    public class DWCommandService : IDWMqttCallback
    {
        private IDWMqttClient mqttClient;

        private String mqttHost = "us1-pns.cloudclientmanager.com";
        private int mqttPort = 1883;

        private Context mContext;
	
        private static DWCommandService _inst = null;
        private readonly static object syncLock = new object();
	
        public static DWCommandService getInstance() 
        {
            if(_inst == null)
            {
                _inst = new DWCommandService();
            }
            return _inst;
        }
	
        public async Task<bool> start(Context ctx, String h, int p, DWCCMCommandsListener cb, DWAuth dwAuth)
        {
            if(!await close())
            {
                return false;
            }
		
            mContext = ctx;
            LocalStorage.setDWCCMCommandsListener(cb);
            mqttHost = h;
            mqttPort = p;

            if(!LocalStorage.isRegister())
                return false;
#if ANDROID
            if(LocalStorage.isLocationEnabled())
            {
                LocationMonitor.Instance(ctx);
            }
#endif
            initializeMQTT();
#if ANDROID
            BAlarmService.setAlarms(ctx, !LocalStorage.IsAlarmUpdated());
#endif
            return true;
        }

        public async Task<bool> close()
        {
            bool bRet = true;
            try
            {
                if(mqttClient != null)
                {
                    bRet = await mqttClient.disconnect();
                }
            }
            catch (Exception e)
            {
                // no-op, service didn't start yet
                String message = String.Format("MQTT Service stop error: {0}", e.Message);
                HaloLog.e(message);
            }
        
            return bRet;
        }	

        private DWCommandService()
        {
        }
	
        public  int initializeMQTT() 
        {
            mqttClient = DWMqttClient.getInstance();
            if(mqttClient != null)
            {
                mqttClient.start(mqttHost, mqttPort, getDeviceID(), true, 120, this);
            }

            return 0;
        }

        public void unInitializeMQTT()
        {
            if(null != mqttClient)
            {
                DWMqttClient.clearInstance();
            }
            if(null != _inst)
            {
                _inst = null;
            }
        }

        public bool handleMessage(String messageBody) 
        {
            HaloLog.v("handleMessage");
            lock(syncLock)
            {
                try
                {
                    if (!"alert".Equals(messageBody, StringComparison.CurrentCultureIgnoreCase))
                    { 
                        HaloLog.i( "Not handling event " + messageBody + " for now");
                        return true;
                    }
                    if (!ProcessCommands(new CCMCommandManager().getInProgressCommands()))
                        return false;

                    if (!ProcessCommands(new CCMCommandManager().getPendingCommands()))
                        return false;
                }
                catch(Exception ex)
                {
                    HaloLog.e(String.Format("Error......{0}", ex.Message));
                }
            }
            return true;
        } // End of handleMessage().	

        private bool ProcessCommands(List<DeviceCommand> listCmd) 
        {
            HaloLog.v("ProcessCommands");
            if(listCmd == null)
                return true;
		
            DWCCMCommandsListener commandsListener = LocalStorage.getDWCCMCommandsListener();
		
            foreach(DeviceCommand adc in listCmd)
            {
                int commandType = adc.deviceAction.type;
                if (commandType == DEVICE_ACTION.GroupChanged
                    || commandType == DEVICE_ACTION.InstallProfile
                    || commandType == DEVICE_ACTION.Query
                    || commandType == DEVICE_ACTION.DeviceInformation)
                {							
                    if (commandType == DEVICE_ACTION.GroupChanged)
                    {
                        HaloLog.i( "**** Received >>>> GroupChanged <<<< command. ****");
                        getFullConfig();
                    }
                    if (commandType == DEVICE_ACTION.DeviceInformation
                        || commandType == DEVICE_ACTION.Query)
                    {
                        HaloLog.i( "**** Received >>>> DeviceInformation <<<< command. ****");
                        checkin(mContext);
                    }
                }
                else if (commandType == DEVICE_ACTION.UnEnrollDevice)
                {
                    HaloLog.i( "**** Received >>>> UnEnrollDevice <<<< command. Un-registering the agent now... ****");
                    SendACK(adc);
                    Unregister();
                    return false;
                }
                else if (commandType == DEVICE_ACTION.WipeWorkspaceData)
                {
                    SendACK(adc);
                    // - this should disappear after WifData become only wipe data and not connection lost
                    //CCMCheckoutSinc ccmco = new CCMCheckoutSinc(mContext);
                    //BAlarmService.removeAlarms(mContext);
                    LocalStorage.setIsRegister(false);
                    LocalStorage.setAlarmUpdated(false);
                    //ccmco.start();
                    close();
                    //
                    SendCommand(adc, commandsListener);
                    return false;
                } 
                else
                {
                    SendCommand(adc, commandsListener);
                }
			
                SendACK(adc);			
            }
            return true;
        }

        public void Unregister() 
        {
            DWCCMCommandsListener commandsListener = LocalStorage.getDWCCMCommandsListener();
#if NOTYET
            CCMCheckoutSinc ccmco = new CCMCheckoutSinc(mContext);
            BAlarmService.removeAlarms(mContext);
#endif
            LocalStorage.setIsRegister(false);
            LocalStorage.setAlarmUpdated(false);
            if (commandsListener != null)
            {
                commandsListener.unregister();
            }
#if NOTYET
            ccmco.start();
#endif
            close();
		
           
        }

        public bool SendCommand(DeviceCommand adc, DWCCMCommandsListener commandsListener) 
        {
            String command = adc.deviceAction.name;
            HaloLog.i( String.Format("**** Received >>>> {0} <<<< command.", command) );
            try
            {
                DWDataIn dIn = new DWDataIn();
                JSONArray ja = new JSONArray();

                foreach  (Parameter param in adc.parameters)
                {
                    Parameter p = (Parameter)param; 
                    ja.put(p.toJSON());
                }
                dIn.jsonData = ja.ToString();   // TODO Not sure what the result is for WP8?
	        
                List<Parameter> pOut = null;
                if(commandsListener !=null)
                {
                    DWDataOut ret = commandsListener.onCommand(adc.deviceAction.type, dIn);
                    pOut = convertToParam(ret.jsonData);
                }
                if(pOut !=null)
                {
                    adc.parameters = pOut;
                }
                return true;
            }
            catch(Exception)
            {
                HaloLog.e( String.Format("- {0} Execution error.", command ));
            }
            return false;
        }

        private void SendACK(DeviceCommand adc)
        {
            //adc.setIsExecuted(true);
            adc.status = StratusConstants.DEVICE_COMMAND_STATUS.Success;

            try
            {
                if (sendAckToServer(adc, TransactionsManager.getNextID()) == true)
                {
                    HaloLog.i( "**** Acknowledgement successfully sent to server. ****");
                } 
                else
                {
                    HaloLog.e( "**** Acknowledgement failed to send to server. ****");
                }
            }
            catch(Exception)
            {
			
            }
        }

        private List<Parameter> convertToParam(String jsonData)
        {
            if(jsonData == null)
                return null;
		
            JSONObject joOut;
            try 
            {
                joOut = JSONObject.DeserializeObject(jsonData);
            } 
            catch (Exception)
            {
                return null;
            }

            List<Parameter> param = new List<Parameter>();
            Parameter par = new Parameter();
            par.Name = "currentStatus";
            int ksS = joOut.optInt("KEYSTONE_STATUS", 0);
            par.Value = ksS.ToString();
            param.Add(par);
            return param;
        }

        private bool sendAckToServer(DeviceCommand adc, int trId) 
        {
            HaloLog.v("sendAckToServer:" + adc.toString(0));
            CcmHttpDataRet dRet = CCMCommands.PerformACK(mContext, adc, trId);
            return dRet.mResult == DWStatusCode.DWStatus_SUCCESS && dRet.statusCode == (int)HttpStatusCode.OK;
        }

//	private void sendMessage(ADeviceCommand adc) {
//		
//		DWCCMCommandsListener commandsListener = LocalStorage.getDWCCMCommandsListener();
//		
//		for (Parameter param : adc.getParameters()) {
//			if(param.getName().contains("message")){
//				if(commandsListener !=null){
//					commandsListener.sendMessage(param.getValue());
//				}
//			}
//		}
//	}

        public String getDeviceID()
        {
            //	if(mAuth !=null && !mAuth.isauthenticated())
            //		return null;
            return LocalStorage.getWyseID();		
        }

        public bool publishArrived(String name, String payload, QoS qos, bool mReturn)
        {
            return handleMessage(payload);
        }
	
        public bool getIsRegister()
        {
            return LocalStorage.isRegister();
        }

        public void connectionLost() 
        {
            // TODO Auto-generated method stub
	    }

        public void closeMqttWorker() 
        {
            // TODO Auto-generated method stub
        }
        //
        public static DWStatusCode authenticate(String use, String psw) 
        {
            int id = TransactionsManager.getNextID();
		
            LocalStorage.Instance();
		
            CmdManager.addCommand(id, LocalStorage.getCallback());
		
            List<String> lo = new List<String>();
            lo.Add(use);
            lo.Add(psw);
           
            return BAlarmService.queueWork(null, BAlarmWorker.BK_AUTHENTICATE, lo, id);
        }
	
        public static DWStatusCode authenticateSSO(String use, String psw, String tenantCode) 
        {
            int id = TransactionsManager.getNextID();
		
            LocalStorage.Instance();
		
            CmdManager.addCommand(id, LocalStorage.getCallback());
		
            List<String> lo = new List<String>();
            lo.Add(use);
            lo.Add(psw);
            lo.Add(tenantCode);

            return BAlarmService.queueWork(null, BAlarmWorker.BK_AUTHENTICATE_SSO, lo, id);
        }

        public void checkin(Context ctx) 
        {
            CCMCommands.PerformCheckIn(ctx, TransactionsManager.getNextID());	
        }
	
        public DWStatusCode checkinTh() 
        {
            int id = TransactionsManager.getNextID();
            CmdManager.addCommand(id, LocalStorage.getDWCCMCommandsListener());
#if ANDROID
            return BAlarmService.startWorker(mContext, BAlarmWorker.BK_CMD_CHECKIN, null, id);
#else
            return BAlarmService.queueWork(mContext, BAlarmWorker.BK_CMD_CHECKIN, null, id);
#endif
        }

        public void getFullConfig()
        {
            CCMCommands.PerformGetFullConfig(null, TransactionsManager.getNextID());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DWStatusCode getFullConfigTh() 
        {
            int id = TransactionsManager.getNextID();
            CmdManager.addCommand(id, LocalStorage.getDWCCMCommandsListener());
#if ANDROID
            return BAlarmService.startWorker(BAlarmWorker.BK_CMD_GETFULLCONFIG, null, id);
#else
            return BAlarmService.queueWork(null, BAlarmWorker.BK_CMD_GETFULLCONFIG, null, id);
#endif
        }
        //
        public DWStatusCode getFullConfigWithDefaultsTh() 
        {
            int id = TransactionsManager.getNextID();
            CmdManager.addCommand(id, LocalStorage.getDWCCMCommandsListener());
#if ANDROID
            return BAlarmService.startWorker( BAlarmWorker.BK_CMD_GETFULLCONFIGWITHDEFAULTS, null, id);
#else
            return BAlarmService.queueWork(null, BAlarmWorker.BK_CMD_GETFULLCONFIGWITHDEFAULTS, null, id);
#endif
        }
	
        public static DWStatusCode getPocketCloudConnectionList() 
        {
            int id = TransactionsManager.getNextID();
            CmdManager.addCommand(id, LocalStorage.getDWSessionListener());
#if ANDROID
            return BAlarmService.startWorker(BAlarmWorker.BK_CMD_GETPOCKETCLOUD_CONNECTIONLIST, null, id);
#else
            return DWStatusCode.DWStatus_FAIL;
#endif
        }
	
	
        public static bool isauthenticated() 
        {
            return (LocalStorage.getAuthCode().Length > 0 && 
            LocalStorage.getWyseID().Length > 0 && 
            LocalStorage.isRegister());
        }

        public static DWStatusCode deauthenticate()
        {
#if ANDROID
            BAlarmService.removeAlarms();
#endif
            LocalStorage.setIsRegister(false);
            LocalStorage.setAlarmUpdated(false);
#if ANDROID		
            CCMCheckout ccmco = new CCMCheckout();
            ccmco.start();
#endif

            DWCommandService.getInstance().close();

            CCMCheckout ccmco = new CCMCheckout();
            ccmco.CCMCheckoutRun();


            LocalStorage.setAuthCode(""); 
            LocalStorage.setWyseID(""); 

            return DWStatusCode.DWStatus_SUCCESS;
        }

        public DWStatusCode enableLocationManager( bool value) 
        {
#if ANDROID
            if(LocalStorage.isLocationEnabled())
            {
                LocationMonitor.Instance();
            }
            else
            {
                LocationMonitor.Release();
            }
#endif
            return DWStatusCode.DWStatus_FAIL;
        }

        #region IDWMqttCallback Members


        public void Connected()
        {
            if (mqttClient != null)
                mqttClient.Subscribe();
        }

        #endregion
    }
}
