﻿using HaloWP8.httpmon;
using HaloWP8.Utils;
using HaloPCL;
using HaloPCL.httpmon;
using HaloPCL.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace HaloWP8
{
    public class CCMCommandManager
    {
	    private const int TYPE_STRING = 1;
	    private const int TYPE_INT = 2;
	    private const int TYPE_BOOL = 3;
	    private const int TYPE_STRINGCOMMA = 8;
	    private const int TYPE_DUO = 10;
	
        private DeviceMetrics generateDeviceJsonStr(bool bRegistration) 
        {
            DeviceMetrics dm = new DeviceMetrics(); 
            dm.collectRequiredMetrics();
		
            if (bRegistration == false) 
            {
                dm.setAuthenticationCode(LocalStorage.getAuthCode());
                dm.setWyseIdentifier(LocalStorage.getWyseID());
            }
		
            DWCCMCommandsListener commandsListener = LocalStorage.getDWCCMCommandsListener();
            if(commandsListener !=null)
            {
                DWDataOut dOut = commandsListener.onCommand(DWAuthImpl.CMD_Query, null);
                if(dOut != null)
                {
                    //??dOut.result = 0;
                    dm.fillCustomInfo(dOut.jsonData);
                }
            }
            return dm;//.toJSON();				
        }

        public CcmHttpDataRet authenticate(Context ctx, String user, String psw, int trId) 
        {
            String strAuth = generateLoginJsonStr(user, psw);
            //HaloLog.v("authenticate:%s", strAuth);
		
            CCMHttpClient client = new CCMHttpClient();
            CcmHttpDataRet msgRet = client.postJsonDataToServer( LocalStorage.getREMOTELOGIN_URL(), strAuth).Result;
            
            if(msgRet.statusCode == (int)HttpStatusCode.OK)
            {
                HaloLog.v("authenticate success");
                if (getLoginInfo(msgRet.mStrData, false) == true) 
                {
                }
            }
            else
            {
                msgRet = getLoginErrorInfo(msgRet, msgRet.mStrData, false);
                LocalStorage.setWyseID("");
                LocalStorage.setAuthCode("");
                LocalStorage.setIsRegister(false);
			
                //msgRet.mResult = DWStatusCode.DWStatus_AUTH_INVALID;
            }

            return msgRet;
        }

        public CcmHttpDataRet authenticateSSO(Context ctx, String user, String psw, String tenantCode, int trId)
        
        {
            try
            {
                String strAuth = generateLoginJsonStr(null, null);
                //HaloLog.v("authenticate SSO:%s", strAuth);

                CookieContainer cookiesReq = new CookieContainer();
                CookieContainer cookiesToSendBack = null;

                CCMHttpClient client = new CCMHttpClient();
                client.setCookie(cookiesReq);
                cookiesToSendBack = client.getCookieStore();

                bool jsonResponseOK = true;
                byte[] content = System.Text.Encoding.UTF8.GetBytes(strAuth);
                //CcmHttpDataRet msgRet = client.postDataToServer(LocalStorage.getREMOTELOGINSSO_URL(tenantCode), content, null, true, true).Result;
                CcmHttpDataRet msgRet = client.postDataToServer(LocalStorage.getREMOTELOGINSSO_URL(tenantCode), strAuth, null, true, true).Result;

                if(client.Headers != null)
                {
                    String redirectLocationLoginPage = client.Headers.Location.AbsoluteUri;
                    if (redirectLocationLoginPage != null && msgRet.statusCode == (int)HttpStatusCode.Found)
                    {
                        LoggingWP8.WP8Logger.LogMessage("Authenticate redirect... OK");
                        int idxReqID = redirectLocationLoginPage.IndexOf("/ccm-auth/") + 10;
                        if (idxReqID != -1 && (redirectLocationLoginPage.Length - idxReqID > 36))
                        {
                            String requestId = redirectLocationLoginPage.Substring(idxReqID, 36);

                            // Connect to the redirected page
                            CCMHttpClient clientLogin = new CCMHttpClient(); // CCMHttpClient.createStratusHttpClient(ctx);// new CCMHttpClient(ctx);
                            msgRet = clientLogin.getDataFromServer(redirectLocationLoginPage).Result;
                            if (msgRet.statusCode == (int)HttpStatusCode.OK)
                            {
                                LoggingWP8.WP8Logger.LogMessage("authenticate AD Page OK");

                                //submit user AD credentials
                                List<KeyValuePair<string, object>> nameValuePairs = new List<KeyValuePair<string, object>>(4);
                                nameValuePairs.Add(new KeyValuePair<String, object>("username", user));
                                nameValuePairs.Add(new KeyValuePair<String, object>("password", psw));
                                nameValuePairs.Add(new KeyValuePair<String, object>("tenantCode", tenantCode));
                                nameValuePairs.Add(new KeyValuePair<String, object>("requestId", requestId));

                                string currentHost_toURI = ""; // I have no clue what this looks like
                                string redirectedHost = "";
                                if (null != clientLogin.ResponseUri)
                                {
                                    currentHost_toURI = clientLogin.ResponseUri.Scheme + "://" + clientLogin.ResponseUri.DnsSafeHost + ":" + clientLogin.ResponseUri.Port.ToString();
                                    redirectedHost = clientLogin.ResponseUri.AbsoluteUri;
                                    clientLogin.ResponseUri = null;
                                }
                                String currentUrl = currentHost_toURI + URLStore.LOGIN_SSO_SUMBIT_URL;

                                CCMHttpClient clientPostCredential = new CCMHttpClient(); // CCMHttpClient.createStratusHttpClient(ctx);//new CCMHttpClient(ctx);

                                msgRet = clientPostCredential.postDataToServer(currentUrl, nameValuePairs, true).Result;

                                if (msgRet.statusCode == (int)HttpStatusCode.Found)
                                {
                                    HaloLog.v("authenticate SSO redirect 2...OK");
                                    // Connect to the redirected page
                                    string redirectLocation = clientPostCredential.Headers.Location.AbsoluteUri;
                                    CCMHttpClient clientFinal = new CCMHttpClient();
                                    clientFinal.setCookie(cookiesToSendBack);
                                    msgRet = clientFinal.getDataFromServer(redirectLocation, true, null, false).Result;

                                    if (msgRet.statusCode == (int)HttpStatusCode.OK)
                                    {
                                        HaloLog.v("authenticate SSO ...OK");
                                        jsonResponseOK = getLoginInfo(msgRet.mStrData, false);
                                    }

                                    if ((msgRet.statusCode != (int)HttpStatusCode.OK) || !jsonResponseOK)
                                    {
                                        HaloLog.v("authenticate SSO failed. Remove previous registration information.");
                                        msgRet = getLoginErrorInfo(msgRet, msgRet.mStrData, false);
                                        LocalStorage.setWyseID("");
                                        LocalStorage.setAuthCode("");
                                        LocalStorage.setIsRegister(false);
                                        return msgRet;
                                    }

                                    return msgRet;
                                }
                                else
                                {
                                    HaloLog.v("authenticate SSO redirection 2 failed");
                                }
                            }
                        }

                    }
                    else
                    {
                        LoggingWP8.WP8Logger.LogMessage("authenticate SSO redirection failed");
                    }

                    msgRet.statusCode = (int)HttpStatusCode.NotFound;
                    msgRet.mStrData = "Cannot Sign In. Incorrect email or password. Please try again or contact your administrator.";
                    msgRet.mResult = DWStatusCode.DWStatus_AUTH_INVALID;
                    LocalStorage.setWyseID("");
                    LocalStorage.setAuthCode("");
                    LocalStorage.setIsRegister(false);

                    return msgRet;
                }
                else
                {
                    // Something bad with the network just happened right out of the gate...
                    string msg = string.Format("Authenticate SSO Received error: {0} from the server.  Please try again or contact your administrator", msgRet.statusCode);
                    LoggingWP8.WP8Logger.LogMessage(msg);
                    msgRet.mStrData = msg;;
                    msgRet.mResult = DWStatusCode.DWStatus_SERVER_NOT_REACHABLE;
                    LocalStorage.setWyseID("");
                    LocalStorage.setAuthCode("");
                    LocalStorage.setIsRegister(false);

                    return msgRet;

                }
            }
            catch(Exception)
            {
                return null;
            }
        }	

        private bool getLoginInfo(String sData, bool b)
        {
            Dictionary<string, string> jo = JsonConvert.DeserializeObject<Dictionary<string, string>>(sData);
            LocalStorage.setWyseID(jo["wyseRemoteAppIdentifier"]);
            LocalStorage.setAuthCode(jo["authenticationCode"]);
            LocalStorage.setIsRegister(true);
            return true;
        }
        [DataContract]
        public class LoginError
        {
            [DataMember(Name = "Error")]
            public string Error { get; set; }
        }

        [DataContract]
        public class LoginMessage
        {
            [DataMember(Name = "messages")]
            public LoginError Messages { get; set; }
        }

        public static LoginMessage LoginErrorfromJSON(Stream inputStream, string message)
        {
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(LoginMessage));
            LoginMessage jsonResponse = null;
            try
            {
                //StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8);
                //string text = reader.ReadToEnd(); 
                object objResponse = jsonSerializer.ReadObject(inputStream);
                jsonResponse = (LoginMessage)objResponse;
            }
            catch (Exception e)
            {
                // The inputStream is not in JSON format.  I've seen this with errors like HttpStatus: 404
                String msg = e.Message;
                jsonResponse = new LoginMessage();
                jsonResponse.Messages = new LoginError();
                jsonResponse.Messages.Error = message;
                //string text = reader.ReadToEnd();
            }
            finally
            {
                inputStream.Dispose();
            }
            return jsonResponse;
        }

        private CcmHttpDataRet getLoginErrorInfo(CcmHttpDataRet msgRet, String sData, bool b) 
        {
            try 
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(sData);
                Stream strm = new MemoryStream(byteArray);

                LoginMessage jsonErr = LoginErrorfromJSON(strm, sData);
                if (null != jsonErr && null != jsonErr.Messages)
                {
                    msgRet.mStrData = jsonErr.Messages.Error;
                    string err = msgRet.mStrData.ToLower();

                    if (err.Contains("bad credentials") || err.Contains("invalid username or password"))
                    {
                        msgRet.mStrData = "Cannot Sign In. Incorrect email or password. Please try again or contact your administrator.";
                        msgRet.mResult = DWStatusCode.DWStatus_AUTH_INVALID;
                    }
                    else if (err.Contains("license is expired"))
                    {
                        msgRet.mStrData = "Registration Failed: Portal license expired. Contact your administrator.";
                        msgRet.mResult = DWStatusCode.DWStatus_TENANT_EXPIRED;
                    }
                    else if (err.Contains("tenant is disabled"))
                    {
                        msgRet.mStrData = "Registration Failed: Portal access disabled. Contact your administrator.";
                        msgRet.mResult = DWStatusCode.DWStatus_TENANT_DISABLED;
                        //}else if(err.contains("no device found for wyseidentifier")){
                        //	msgRet.mResult = DWStatusCode.DWStatus_AUTH_INVALID;
                    }
                    else if (err.Contains("owner is disabled") || err.Contains("user is disabled"))
                    {
                        msgRet.mStrData = "Registration Failed: User account disabled. Contact your administrator.";
                        msgRet.mResult = DWStatusCode.DWStatus_USER_DEACTIVATED;
                    }
                    else if (err.Contains("keystone device limit reached. please increase the license limit."))
                    {
                        msgRet.mStrData = "Registration Failed: Workspace license limit reached. Contact your administrator.";
                        msgRet.mResult = DWStatusCode.DWStatus_LICENSE_LIMIT_REACHED;
                    }
                    else if (err.Contains("maximum number of keystone apps has been reached"))
                    {
                        msgRet.mStrData = "Registration Failed due to Workspace restriction policy. Contact your administrator.";
                        msgRet.mResult = DWStatusCode.DWStatus_LICENSE_GROUP_LIMIT_REACHED;
                    }
                    else if (err.Contains("404"))
                    {
                        msgRet.mResult = DWStatusCode.DWStatus_CONNECTION_FAILED;
                    }
                    else
                    {
                        msgRet.mResult = DWStatusCode.DWStatus_AUTH_INVALID;
                    }
                    return msgRet;
                }
            }
            catch(Newtonsoft.Json.JsonReaderException e)
            {
                msgRet.mResult = DWStatusCode.DWStatus_FAIL;
                msgRet.mStrData = "Some JSON Reader Exception";
            }
            return msgRet;
        }
	
	
        private String generateLoginJsonStr(String user, String psw) 
        {
            Dictionary<string, Object> jo = new Dictionary<string, Object>();
            jo.Add("remoteAppId", DeviceUtils.getRemoteAppId());
            jo.Add("deviceType", DeviceUtils.getDeviceType());
            jo.Add("installedPlatformType", DeviceUtils.getInstalledPlatformType());
            jo.Add("remoteAppVersion", DeviceUtils.getRemoteAppVersion());
            if(user != null)
            {
                jo.Add("loginName", user );
            }
            if(psw != null)
            {
                jo.Add("password", psw );
            }
            jo.Add("deviceName", DeviceUtils.getDeviceName() );
            jo.Add("macAddress", DeviceUtils.getMacAddress() );
            jo.Add("udid", DeviceUtils.getDeviceIdString() );
            
            return JsonConvert.SerializeObject(jo);
        }

        public CcmHttpDataRet checkInToServer(Context ctx, bool bRegistration, int trId) 
        {
            DeviceMetrics dm = generateDeviceJsonStr(bRegistration);
            DWCCMCommandsListener commandsListener = LocalStorage.getDWCCMCommandsListener();
            
            if(commandsListener != null)
            {
                if(dm.getIsJailBroken())
                {
                    HaloLog.v("Jailbreak detected!");
                    if(commandsListener.onJailbreak())
                    {
                        HaloLog.v("Jailbreak condition - unregister");
                        CcmHttpDataRet ret = new CcmHttpDataRet();
                        try
                        {
                            DWCommandService.getInstance().Unregister();
                            HaloLog.e("Unregister on jailbreak");
                            ret.mResult = DWStatusCode.DWStatus_SUCCESS;
                        }
                        catch(Exception)
                        {
                            HaloLog.e("fail to unregister on jailbreak");
                            ret.mResult = DWStatusCode.DWStatus_FAIL;
                        }
                        return ret;
                    }
                }
            }
		
            // TODO: dm.toJSON needs to be fixed.
            String strCheckin = dm.toJSON();
            //HaloLog.v("Check-in:%s", strCheckin);
            CcmHttpDataRet msgRet =  new CCMHttpClient().postJsonDataToServer( LocalStorage.getCHECKIN_URL(), strCheckin).Result;
            
            if(msgRet.statusCode == (int)HttpStatusCode.OK )
            {
                HaloLog.v("Check-in to server succeeded.");
                if (GetConfigurationItems(msgRet.mStrData, false, trId) == true) 
                {
                }
            }
            else
            {
                HandleErrorMessage(msgRet);
            }

            return msgRet;
        }
	
        public CcmHttpDataRet getFullConfig(Context ctx, int trId) 
        {
            HaloLog.v("getFullConfig");
        
            CcmHttpDataRet msgRet = new CCMHttpClient().postJsonDataToServer( LocalStorage.getFULLCONFIG_URL(), "{}").Result;
        
            if(msgRet.statusCode == (int)HttpStatusCode.OK)
            {
                HaloLog.v("getFullConfig success");
        
                if (GetConfigurationItems(msgRet.mStrData, true, trId) == true)
                {
                }
            }
            else
            {
                HandleErrorMessage(msgRet);
            }
        
            return msgRet;
        }

        public CcmHttpDataRet getFullConfigWithDefaults(Context ctx, int trId)
        {
            HaloLog.v("getFullConfigWithDefaults");
            CcmHttpDataRet msgRet = new CCMHttpClient().postDataToServer( LocalStorage.getFULLCONFIGWITHDEFAULTS_URL(), "{}").Result;
            if(msgRet.statusCode == (int)HttpStatusCode.OK)
            {
                HaloLog.v("getFullConfigWithDefaults success");
            
                if (GetConfigurationItems(msgRet.mStrData, true, trId) == true)
                {
                }
            }
            else
            {
                HandleErrorMessage(msgRet);
            }
            return msgRet;
        }
	
        public CcmHttpDataRet getPocketCloudConnectionList(Context ctx, int trId) 
        {
            HaloLog.v("getPocketCloudConnectionList");
            DWSessionListener sl = LocalStorage.getDWSessionListener();
            
            if(sl != null)
            {
                CcmHttpDataRet msgRet = new CCMHttpClient().getDataFromServer( LocalStorage.getPOKETCLOUD_URL()).Result;
            
                if(msgRet.statusCode == (int)HttpStatusCode.OK )
                {
                    HaloLog.v("getPocketCloudConnectionList success");
            
                    try
                    {
                        JSONArray ja = JSONArray.DeserializeArray(msgRet.mStrData);// new JSONArray(msgRet.mStrData); 
                        JSONArray listElements = new JSONArray();
                        for(int i = 0; i < ja.Count; i++)
                        {
                            ConfigurationElementLean item = new ConfigurationElementLean();
#if FIXME   // TODO Fix this
                            item.fromJSON(ja[i] as JSONObject);
                            JSONObject objToSend = null;
                            objToSend = parseRDPObject2(objToSend, item);
                            listElements.put(objToSend);
#endif // FIXME
                        }
            
                        sl.onPocketCloudConnectionListResult( msgRet.mResult, listElements.ToString(), "");
                        return msgRet;
                    }
                    catch(Exception)
                    {
                        HaloLog.e("getPocketCloudConnectionList error");
                    }
            
                    sl.onPocketCloudConnectionListResult(msgRet.mResult, "[]", msgRet.mStrData);
                }
                else
                {
                    HandleErrorMessage(msgRet);
                    sl.onPocketCloudConnectionListResult(msgRet.mResult, "[]", msgRet.mStrData);
                }
            } 
            return null;
        }
	
        public List<DeviceCommand> getInProgressCommands() 
        {
            HaloLog.v("getInProgressCommands");
            CcmHttpDataRet msgRet = new CCMHttpClient().getDataFromServer( LocalStorage.getINPROGRESS_COMMANDS() + "?wyseId=" + LocalStorage.getWyseID()).Result;
            
            if(msgRet.statusCode == (int)HttpStatusCode.OK)
            {
                HaloLog.v("getInProgressCommands success");
                try
                {
                    try
                    {
                        MemoryStream sr = new MemoryStream(Encoding.UTF8.GetBytes(msgRet.mStrData));
                        DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<DeviceCommand>));
                        List<DeviceCommand> listCmd = (List<DeviceCommand>)ser.ReadObject(sr);
                        return listCmd;
                    }
                    catch (Exception e)
                    {
                        HaloLog.e("Exception parsing JSON.", e);
                    }


                    //List<DeviceCommand> listCmd = new List<DeviceCommand>();
                    //JSONArray ja = JSONArray.DeserializeArray(msgRet.mStrData);
        
                    //for(int i = 0; i < ja.Count; i++)
                    //{
                    //    JSONObject jo = JSONObject.DeserializeObject(ja[i] as string);
                    //    DeviceCommand adc = new DeviceCommand();
                    //    adc.fromJSON(jo);
                    //    listCmd.Add(adc);
                    //}
        
                    //return listCmd;
                } 
                catch (Exception)
                {
                    HaloLog.e("Exception parsing JSON.");
                } 
            }
            else
            {
                HandleErrorMessage(msgRet);
            }
        
            return null;
        }

        public List<DeviceCommand> getPendingCommands() 
        {
            HaloPCL.Utils.HaloLog.v("getPendingCommands");
        
            CcmHttpDataRet msgRet =  new CCMHttpClient().getDataFromServer( LocalStorage.getCOMMAND_URL() + "?wyseId=" + LocalStorage.getWyseID()).Result;
        
            if(msgRet.statusCode == (int)HttpStatusCode.OK)
            {

                HaloLog.v("getPendingCommands success");
                try
                {
                    MemoryStream sr = new MemoryStream(Encoding.UTF8.GetBytes(msgRet.mStrData));
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<DeviceCommand>));
                    List<DeviceCommand> listCmd = (List<DeviceCommand>)ser.ReadObject(sr);
                    return listCmd;
                }
                catch (Exception e)
                {
                    HaloLog.e("Exception parsing JSON.", e);
                }



                //HaloLog.v("getPendingCommands success");
                //try
                //{
                //    List<DeviceCommand> listCmd = new List<DeviceCommand>();
        
                //    JSONArray ja = JSONArray.DeserializeArray(msgRet.mStrData);
                //    for(int i = 0; i < ja.Count(); i++)
                //    {
                //        JSONObject jo = JSONObject.DeserializeObject(ja[i] as string);
                //        DeviceCommand adc = new DeviceCommand();
                //        adc.fromJSON(jo);
                //        listCmd.Add(adc);
                //    }
                //    return listCmd;
                //} 
                //catch (Exception)
                //{
                //    HaloLog.e("Exception parsing JSON.");
                //} 
            }
            else
            {
                HandleErrorMessage(msgRet);
            }
            return null;
        }
	
        private void HandleErrorMessage(CcmHttpDataRet msgRet) 
        {
            if(msgRet!= null && msgRet.statusCode == 400)
            {
                if(msgRet.mStrData != null)
                {
                    String err = msgRet.mStrData.ToLower();
                    
                    bool bToUnregister = false;

                    if(err.Contains("tenant is disabled"))
                    {
                        bToUnregister = true;
                    }
                    else if(err.Contains("license is expired"))
                    {
                        bToUnregister = true;
                    }
                    else if(err.Contains("no device found for wyseidentifier"))
                    {
                        bToUnregister = true;
                    }
                    else if(err.Contains("owner is disabled"))
                    {
                        bToUnregister = true;
                    }
                    if(bToUnregister) 
                    {
                        HaloLog.w("Tenant of device has been disabled at the backend.");
                        HaloLog.w("Resetting device state to initial user registration status.");                        	
                        DWCommandService.getInstance().Unregister();
                    }                	                	                	                	
                }
            }
        }
	
        private bool GetConfigurationItems(String strData, bool bFull, int trId) 
        {
            if (strData != null)
            {
                HaloLog.v("GetConfigurationItems:{0}", strData);

                var stream = new MemoryStream(Encoding.UTF8.GetBytes(strData));
                try
                {
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(DeviceCheckinResponse));
                    DeviceCheckinResponse deviceCheckinResponse = (DeviceCheckinResponse)ser.ReadObject(stream);
                    return ProcessConfigurationItems(deviceCheckinResponse, bFull, trId);
                }
                catch(Exception e) 
                {
                
                }
            }
            return false;



            //String message = String.Format("GetConfigurationItems: {0}", strData);
            //HaloLog.v(message);
            //byte[] byteArray = Encoding.UTF8.GetBytes(strData);
            //Stream strm = new MemoryStream(byteArray);
            
            //DeviceCheckinResponse deviceCheckinResponse = DeviceCheckinResponse.fromJSON(strm);
            //return ProcessConfigurationItems(deviceCheckinResponse, bFull, trId);
        }

        private bool ProcessConfigurationItems( DeviceCheckinResponse deviceCheckinResponse, bool bFullConfig, int trId) 
        {
            HaloLog.v("ProcessConfigurationItems");
            DWCCMCommandsListener dl = LocalStorage.getDWCCMCommandsListener();   		   	
            if(dl != null)
            {
                if(deviceCheckinResponse == null)
                {
                    String message = String.Format("ProcessConfigurationItems no update found.fullConfig:%b send []",bFullConfig);
                    HaloLog.v(message);
                    dl.onProfileUpdate("[]", bFullConfig);
                    return false;
                }
            
                List<ConfigurationElementLean> listElements = deviceCheckinResponse.deviceElements;
                //			HaloLog.v("ProcessConfigurationItems no update found(2).fullConfig:%b send []",bFullConfig);
                JSONArray arrayToSend = new JSONArray();
                bool bFull = bFullConfig ? true : deviceCheckinResponse.fullConfiguration;
            
                if (listElements != null) 
                {
                    foreach (ConfigurationElementLean element in listElements)
                    {
                        arrayToSend.put(parseRDPObject(element));
                    }
                }
            
                JSONObject globalInfo = new JSONObject();
                try
                {
                    if(deviceCheckinResponse.lastUpdatedAt !=null)
                        globalInfo.put("lastUpdatedAt", deviceCheckinResponse.lastUpdatedAt.ToString());

                    PersonInfoLean pil = (PersonInfoLean)deviceCheckinResponse.personInfoLean;
                    if(pil != null)
                    {

                        JSONObject pInfo = new JSONObject();
                        pInfo.put("firstName", pil.firstName);
                        pInfo.put("lastName", pil.lastName);
                        pInfo.put("phoneNumber", pil.phoneNumber);
                        pInfo.put("title", pil.title);
                        globalInfo.put("personInfoLean", pInfo);
                    }
                    arrayToSend.put(globalInfo);
                }    
                catch (Exception) 
                {
                    HaloLog.e("personInfoLean error");
                }
   				 
                String dataToSend = arrayToSend.ToString();
                String msg = String.Format("ProcessConfigurationItems fullConfig:{0} JSON: {1}",bFull, dataToSend);
                HaloLog.v(msg);
                dl.onProfileUpdate(dataToSend, bFull);
            }
            return true;		
        }
	
	
        private JSONObject parseRDPObject(ConfigurationElementLean element) {
            JSONObject groupItem = new JSONObject();
            try 
            {
                JSONObject cfgItem = new JSONObject();
			
                groupItem.put("isremoved", element.IsRemoved);
                groupItem.put("seq", element.SeqNumber);
                groupItem.put("type", element.ParameterGroupType);
                //groupItem.put("version", element.getParameterGroupVersion());
			
                String name;
                int type;
                foreach(ConfigurationItemLean item in element.ConfigurationItems)
                {
                    name = item.ItemKey;
                    if (name != null) 
                    {
                        type = item.ValueType;
                        switch (type)
                        {
                            case TYPE_STRING:
                            case TYPE_STRINGCOMMA:
                                cfgItem.put(name, item.ItemValue);
                                break;
                            case TYPE_INT:
                                if(item.ItemValue.Length > 0)
                                    cfgItem.put(name, getIntValue(item.ItemValue));
                                break;
                            case TYPE_BOOL:
                                if(item.ItemValue.Length > 0)
                                    cfgItem.put(name, getBooleanValue(item.ItemValue));
                                break;
                            case TYPE_DUO:
                            {
                                if(item.ItemRealValue != null)
                                {
                                    JSONObject v = new JSONObject();
                                    v.put("NAME", item.ItemValue);
                                    v.put("VALUE", item.ItemRealValue);

                                    // Browser certificates seem to crap the bed here
                                    JSONArray ja = null;
                                    
                                    try
                                    {
                                        ja = JSONArray.DeserializeArray(name);
                                    }
                                    catch(JsonReaderException)
                                    {
                                        ja = null;
                                    }
                                    finally
                                    {

                                    }
                                    if (ja == null)
                                    {
                                        ja = new JSONArray();
                                        ja.put(v);
                                        cfgItem.put(name, ja);
                                    }
                                    else
                                    {
                                        ja.put(v);
                                    }
                                }
                                break;
                            }
                            default:
                                HaloLog.e("Unknown type " + type + " for " + name);
                                continue;
                        }
                    }
                }
                groupItem.put("items", cfgItem);
            } 
            catch (Exception e)
            {
                String msg = String.Format("Field parse failed: {0}", e.Message);
                HaloLog.e(msg);
            }
            return groupItem;
        }

#if ANDROID // NOT USED
        private JSONObject parseRDPObjectO(ConfigurationElementLean element) 
        {
        JSONObject groupItem = new JSONObject();
        try {
        JSONObject cfgItem = new JSONObject();
			
        groupItem.put("isremoved", element.getIsRemoved());
        groupItem.put("seq", element.getSeqNumber());
        groupItem.put("type", element.getParameterGroupType());
        //groupItem.put("version", element.getParameterGroupVersion());
			
        String name;
        int type;
        for(ConfigurationItemLean item : element.getConfigurationItems()) {
        name = item.getItemKey();
        if (name != null) {
        type = item.getValueType();
        switch (type) {
        case TYPE_STRING:
        case TYPE_STRINGCOMMA:
        cfgItem.put(name, item.getItemRealValue());
        break;
        case TYPE_INT:
        cfgItem.put(name, getIntValue(item.getItemRealValue()));
        break;
        case TYPE_BOOL:
        cfgItem.put(name, getBooleanValue(item.getItemRealValue()));
        break;
        case TYPE_DUO:{
        JSONObject v = new JSONObject();  
        v.put("NAME", item.getItemValue());
        v.put("VALUE", item.getItemRealValue());
						
        JSONArray ja = cfgItem.optJSONArray(name);
        if(ja == null){
        ja = new JSONArray();
        ja.put(v);
        cfgItem.put(name, ja);
        }else{
        ja.put(v);
        }
        }
        break;
        default:
        HaloLog.e("Unknown type " + type + " for " + name);
        continue;
        }
        }
        }
        groupItem.put("items", cfgItem);
        } catch (Exception e) {
        HaloLog.e("Field parse failed.", e);
        }
        return groupItem;
        }
#endif
        private Object getIntValue(String realValue) 
        {
            int i = 0;
            try
            {
                i = Convert.ToInt32(realValue);
            }
            catch(FormatException){}  
            return i;
        }

        private Object getBooleanValue(String realValue) 
        {
            if(realValue.Equals("true", StringComparison.CurrentCultureIgnoreCase))
                return true;
            if (realValue.Equals("yes", StringComparison.CurrentCultureIgnoreCase))
                return true;
            return false;
        }

        private JSONObject parseRDPObject2(JSONObject prev, ConfigurationElementLean element) 
        {
            if(prev == null)
                prev = new JSONObject();
            
            String name, value;
            int type;
            
            foreach(ConfigurationItemLean item in element.ConfigurationItems)
            {
                try
                {
                    name = item.ItemKey;
                    value = item.ItemValue;
                    type = item.ValueType;
            
                    switch (type)
                    {
                        case TYPE_STRING:
                        case TYPE_STRINGCOMMA:
                            name = name + ":s:";
                            break;
                        case TYPE_INT:
                            name = name + ":i:";
                            break;
                        case TYPE_BOOL:
                            name = name + ":b:";
                            break;
                        default:
                            HaloLog.e("Unknown type " + type + " for " + name + ":" + value);
                            break;
                    }
				
                    if (name != null)
                    {
                        prev.put(name, value);
                    }
                } 
                catch (Exception)
                {
                    HaloLog.e("Field parse failed:");
                }
            }
            return prev;
        }

        public CcmHttpDataRet performAck(Context ctx, DeviceCommand deviceCommand, int trId) 
        {
            CcmHttpDataRet msgRet = null;
            try
            {
                String ackData = deviceCommand.toJSON();
                String message = String.Format("performAck: {0}", ackData);
                HaloLog.v(message);
                return new CCMHttpClient().postDataToServer( LocalStorage.getSTATUS_URL(), ackData).Result;
            }
            catch (Throwable t)
            {
                HaloLog.e( "Throwable: ", t);
            }
        
            return msgRet;
        }

        public DWStatusCode performHeartBeat(Context ctx, bool b, int trId) 
        {
            try
            {
                String jsonString = "{}";
                HaloLog.v("performHeartBeat");
                CcmHttpDataRet msgRet = new CCMHttpClient().postDataToServer( LocalStorage.getHEARTBEAT_URL(), jsonString).Result;
                if(msgRet.statusCode == (int)HttpStatusCode.OK)
                {
                    HaloLog.v("performHeartBeat success");
                }
                else
                {
                    HandleErrorMessage(msgRet);				
                }

                return DWStatusCode.DWStatus_SUCCESS;
            } 
            catch (Throwable t)
            {
                HaloLog.e( "Throwable: ", t);
            }
            return DWStatusCode.DWStatus_ERROR;
        }

	
        public CcmHttpDataRet performCheckout(Context ctx) 
        {
            try 
            {
                HaloLog.v("performCheckout");
                
                CcmHttpDataRet msgRet = new CCMHttpClient().deleteFromServer( LocalStorage.getCHECKOUT_URL()).Result;
                if(msgRet.statusCode == (int)HttpStatusCode.OK)
                {
                    HaloLog.v("performCheckout success");
                }
                return msgRet;
            } 
            catch (Throwable t)
            {
                HaloLog.e( "Throwable: ", t);
            }
            return null;
        }

        public CcmHttpDataRet performCheckout(Context ctx, CcmConnectionInfo ci) 
        {
            try
            {
                HaloLog.v("performCheckout");
            
                CcmHttpDataRet msgRet = new CCMHttpClient().deleteFromServer( ci).Result;

                if(msgRet.statusCode == (int)HttpStatusCode.OK)
                {
                    HaloLog.v("performCheckout success");
                }
                return msgRet;
            } 
            catch (Throwable t)
            {
                HaloLog.e( "Throwable: ", t);
            }
            return null;
        }
    }
}
