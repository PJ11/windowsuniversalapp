﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloWP81.Utils
{
    public sealed class IPAddress
    {
        /// <summary>Converts a long value from host byte order to network byte order.</summary>
        /// <returns>A long value, expressed in network byte order.</returns>
        /// <param name="host">The number to convert, expressed in host byte order. </param>
        public static long HostToNetworkOrder(long host)
        {
            unchecked
            {
                return ((long)IPAddress.HostToNetworkOrder((int)host) & (long)((ulong)-1)) << 32 | ((long)IPAddress.HostToNetworkOrder((int)(host >> 32)) & (long)((ulong)-1));
            }
        }
        /// <summary>Converts an integer value from host byte order to network byte order.</summary>
        /// <returns>An integer value, expressed in network byte order.</returns>
        /// <param name="host">The number to convert, expressed in host byte order. </param>
        public static int HostToNetworkOrder(int host)
        {
            return ((int)IPAddress.HostToNetworkOrder((short)host) & 65535) << 16 | ((int)IPAddress.HostToNetworkOrder((short)(host >> 16)) & 65535);
        }
        /// <summary>Converts a short value from host byte order to network byte order.</summary>
        /// <returns>A short value, expressed in network byte order.</returns>
        /// <param name="host">The number to convert, expressed in host byte order. </param>
        public static short HostToNetworkOrder(short host)
        {
            return (short)((int)(host & 255) << 8 | (host >> 8 & 255));
        }
    }
}
