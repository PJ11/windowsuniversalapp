﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloWP8.Utils
{
    public class Throwable : Exception
    {

    }

    public class Log
    {
        protected static void logmsg(string msg)
        {
            System.Diagnostics.Debug.WriteLine(msg);
        }

        public static void v(string tag, string msg)
        {
            Log.logmsg(msg);
        }
        
        public static void i(string tag, string msg)
        {
            Log.logmsg(msg);
        }
        
        public static void w(string tag, string msg)
        {
            Log.logmsg(msg);
        }

        internal static void e(string msg, Exception ex)
        {
            Log.logmsg(msg);
        }

        public static void e(string tag, string msg)
        {
            Log.logmsg(msg);
        }
        public static void d(string tag, string msg)
        {
            Log.logmsg(msg);
        }
        public const int NONE = 0;
        public const int ERROR = 1;
        public const int VERBOSE = 2;
        public const int INFO = 3;
        public const int WARN = 4;
        public const int DEBUG = 5;
    };

    public class HaloLog
    {
	    private static String TAG = "Halo";
	    public static int MESSAGE_LOGGING = Log.ERROR;
	
	    public static void setLogLevel(int level) 
        {
		    MESSAGE_LOGGING = level;		
	    }
	
	    public static void v(String msg) 
        {
		    if (MESSAGE_LOGGING <= Log.VERBOSE)
			    Log.v(TAG, msg);
	    }    
       
	    public static void v(String format, Object[] args) 
        {
		    if (MESSAGE_LOGGING <= Log.VERBOSE) 
			    Log.v(TAG, String.Format(format, args) );
	    }

        public static void v(String format, Object args)
        {
            if (MESSAGE_LOGGING <= Log.VERBOSE)
                Log.v(TAG, String.Format(format, args));
        }           

	    public static void v(String msg, Throwable e) {
		    if (MESSAGE_LOGGING <= Log.VERBOSE) 
			    Log.v(TAG, msg/*, e*/);
	    }
	    public static void i(String msg) {
		    if (MESSAGE_LOGGING <= Log.INFO)
			    Log.i(TAG, msg);
	    }           
	    public static void i(String format, Object[] args) {
		    if (MESSAGE_LOGGING <= Log.INFO) 
			    Log.i(TAG, String.Format(format, args) );
	    }
	    public static void i(String msg, Throwable e) {
		    if (MESSAGE_LOGGING <= Log.INFO)
                Log.i(TAG, msg/*, e*/);
	    }
	    public static void w(String msg) {
		    if (MESSAGE_LOGGING <= Log.WARN)
			    Log.w(TAG, msg);
	    }           
	    public static void w(String format, Object[] args) {
		    if (MESSAGE_LOGGING <= Log.WARN) 
			    Log.w(TAG, String.Format(format, args) );
	    }
	    public static void w(String msg, Throwable e) {
		    if (MESSAGE_LOGGING <= Log.WARN)
                Log.w(TAG, msg/*, e*/);
	    }           
	    public static void e(String msg) {
		    if (MESSAGE_LOGGING <= Log.ERROR)
			    Log.e(TAG, msg);
	    }

        public static void e(String msg, Exception ex) {
            if (MESSAGE_LOGGING <= Log.ERROR)
                Log.e(msg, ex);
        }

	    public static void e(String format, Object[] args) {
		    if (MESSAGE_LOGGING <= Log.ERROR) 
			    Log.e(TAG, String.Format(format, args) );
	    }
	    public static void e(String msg, Throwable e) {
		    if (MESSAGE_LOGGING <= Log.ERROR)
                Log.e(TAG, msg/*, e*/);
	    }

	    public static void d(String msg) {
		    if (MESSAGE_LOGGING <= Log.DEBUG) 
			    Log.d(TAG, msg);
	    } 
	    public static void d(String format, Object[] args) {
		    if (MESSAGE_LOGGING <= Log.DEBUG) 
			    Log.d(TAG, String.Format(format, args) );
	    }
	    public static void d(String msg, Throwable e) {
		    if (MESSAGE_LOGGING <= Log.DEBUG)
                Log.d(TAG, msg/*, e*/);
	    }          
    }
}
