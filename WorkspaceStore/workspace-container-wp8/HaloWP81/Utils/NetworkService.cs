﻿using HaloWP8.mqtt;
using HaloPCL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using System.Net.NetworkInformation;

/*
 * http://firstfloorsoftware.com/Media/DiffLists/Windows%20Phone%208.1%20%28Silverlight%29-vs-Windows%20Phone%208.1.html
 */
namespace HaloWP8.Utils
{
    public class NetworkService
    {
        private bool mIsBound = false;

        protected static NetworkState networkState = NetworkState.NETWORK_OFFLINE;
        protected static NetworkType networkType = NetworkType.NETWORK_UNKNOWN;

        private static DWMqttClient mMQTTService;

        /**
         * A list of network types associated to the device.<br>
         * {@link #NETWORK_UNKNOWN} may indicate no connectivity or the interface is
         * not defined (e.g.: bluetooth).
         */
        public enum NetworkType
        {
            NETWORK_LAN,

            /**
             * Associated to mobile data plans.
             */
            NETWORK_WWAN,

            /**
             * An unknown network interface. (New API?)
             */
            NETWORK_UNKNOWN
        }

        /**
         * A list of network states.
         */
        public enum NetworkState
        {
            NETWORK_OFFLINE, NETWORK_ONLINE, NETWORK_ADDRESS_CHANGED
        }

        public NetworkService()
        {
            NetworkInformation.NetworkStatusChanged += NetworkInformation_NetworkStatusChanged;
            #if WP_80_SILVERLIGHT
            NetworkChange.NetworkAddressChanged += NetworkChange_NetworkAddressChanged;
#endif
        }

        void NetworkInformation_NetworkStatusChanged(object sender)
        {
            if (!mIsBound)
            {
                return;
            }

            NetworkType tType = getNetworkIface();
            NetworkState tState = NetworkState.NETWORK_OFFLINE;

            // check network state
            if (isOnline())
            {
                HaloLog.d("Network is online.");
                tState = NetworkState.NETWORK_ONLINE;
            }
            else
            {
                tState = NetworkState.NETWORK_OFFLINE;
            }

            if (tType != networkType || tState != networkState)
            {
                networkType = tType;
                networkState = tState;

                if (mMQTTService != null)
                {
                    //??mMQTTService.connectionChanged();
                }
            }
            else
            {
                HaloLog.v("Ignoring signal - already consumed.");
            }
        }

        void NetworkChange_NetworkAddressChanged(object sender, EventArgs e)
        {
        }

        public static void setMQTTConnectionListener(DWMqttClient dwMqttClient)
        {
            mMQTTService = dwMqttClient;
        }

        public bool isOnline()
        {
            // First try wifi, then cellular
            bool isOnline = false;
            foreach (ConnectionProfile networkInterfaceInfo in NetworkInformation.GetConnectionProfiles())
            {
                if(networkInterfaceInfo.IsWlanConnectionProfile)  // WiFi
                {
                    // TODO: How do we determine if connected?
                }
                if(networkInterfaceInfo.IsWwanConnectionProfile)   // Mobile
                {
                }
            }
	    
            return isOnline;
        }

        /**
         * Synonymous with getNetworkType() but contains logic to get the proper
         * generalized network interface.
         */
        private NetworkType getNetworkIface()
        {
            foreach (ConnectionProfile networkInterfaceInfo in NetworkInformation.GetConnectionProfiles())
            {
                if (networkInterfaceInfo.IsWlanConnectionProfile)  // WiFi
                {
                    // TODO: How do we determine if connected?
                    return NetworkType.NETWORK_LAN;
                }
                if (networkInterfaceInfo.IsWwanConnectionProfile)   // Mobile
                {
                    return NetworkType.NETWORK_WWAN;
                }
            }
            return NetworkType.NETWORK_UNKNOWN;
        }

        // ------------------------------------------------------------------------
        // Native callback functions below
        // ------------------------------------------------------------------------

        public static int getNetworkType()
        {
            return (int)networkType;
        }

        public static int getNetworkState()
        {
            return (int)networkState;
        }
    }
}
