﻿#define REG_ANDROID

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using System.Globalization;
using System.Reflection;
using Windows.Storage.Streams;
using HaloPCL.Utils;
using Windows.System.Profile;

namespace HaloWP8.Utils
{
    class DeviceUtils
    {
	    public static int KEYSTONE_PLATFORM = 9;
	    public static int POCKETCLOUD_PLATFORM = 0;
	
	    /**
	     * For debug
	    */
	    public static String APP_ID = "com.dell.workspace";
        //public static String APP_ID = "";
	
	    /**
	     * For debug
	     */
	    public static String APP_VERSION = ""; //"1.0.5";

	    public static int APP_INSTPLATFORM = KEYSTONE_PLATFORM = 9;
	
	    public static int ETHERNET_MAC_ADDRESS_LENGTH = 17;	
	
	    private static String mDeviceId = ""; 
	
	    public static String getUserLocale()
        {
#if ANDROID
		    return Locale.getDefault().getDisplayName();
#else
            CultureInfo ci = CultureInfo.CurrentCulture;
            return ci.Name.ToLower();
#endif
        }

	    public static String getDeviceName() 
        {
            Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation di = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();
            string DeviceName =  di.FriendlyName;

            if (String.IsNullOrEmpty(DeviceName))
            {
                HaloLog.w("Unknown device name!");
            }
            else
            {
                HaloLog.i("Device name: " + DeviceName);
            }
            
            return DeviceName;
	    }

	    public static String getMachineName() 
        {
		    return getDeviceName();
	    }

        /// <summary>
        /// Get's the NET Address from ASHWID https://msdn.microsoft.com/en-us/library/windows/apps/jj553431.aspx
        /// The problem is it's only giving 2 bytes when it should be 6.
        /// </summary>
        /// <returns></returns>
        public static String getMacAddress() 
        {
            HardwareToken token = HardwareIdentification.GetPackageSpecificToken(null);
            IBuffer hardwareId = token.Id;
            string macAddress = "";
            using (DataReader dataReader = DataReader.FromBuffer(token.Id))
            {
                int offset = 0;
                while (offset < token.Id.Length)
                {
                    byte[] hardwareEntry = new byte[4];
                    dataReader.ReadBytes(hardwareEntry);

                    // CPU ID of the processor || Size of the memory || Serial number of the disk device || BIOS
                    if (hardwareEntry[0] == 4)
                    {
                        macAddress += string.Format("{0}.{1}", hardwareEntry[2], hardwareEntry[3]);
                    }
                    offset += 4;
                }
            }
            
            return macAddress;
	    }

        public static String getRemoteAppId()
        {
            if (APP_ID != "")
            {
                //HaloLog.d("App id = " + APP_ID);
                return APP_ID;
            }

            HaloLog.v("App ID is defaulting.");
#if ANDROID
            return LocalStorage.getAppContext().getPackageName();
#else
            return "com.wyse.halotest"; // TODO What do I do for WP8?
#endif
        }

        // TODO Determine what device type is
        public static int getDeviceType()
        {
#if REG_ANDROID
            return 4;
#else
            return 18; 
#endif
        }
//PJ. Need to know what it is as of now getting the assembly version.
        public static String getRemoteAppVersion() 
        {
           return getSystemVersion();
            
            //String version = "0";
#if ANDROID
            Context ctx = LocalStorage.getAppContext();
            if (ctx != null) 
            {
                try
                {
                    PackageInfo pInfo = ctx.getPackageManager()
                    .getPackageInfo(ctx.getPackageName(), 0);
                    version = pInfo.versionName;
                } 
                catch (NullPointerException npe)
                {
                    HaloLog.e("Class not initialize.", npe);
                } 
                catch (Exception e)
                {
                    HaloLog.e("Failed to get package remote app id.", e);
                }
            } 
            else 
            {
                HaloLog.w("Invalid context!");
            }
#endif
           // HaloLog.v("App version is defaulting");
           // return version;
        }

        public static String getRemoteAppName() 
        {
#if ANDROID
            Context ctx = LocalStorage.getAppContext();
            final PackageManager pm = ctx.getPackageManager();
            ApplicationInfo ai;
            try 
            {
                ai = pm.getApplicationInfo( ctx.getPackageName(), 0);
            } 
            catch (final NameNotFoundException e)
            {
                ai = null;
            }
            if(ai != null)
            {
                return (String) pm.getApplicationLabel(ai);
            }
#endif
            return getSystemName();
            //return "Workspace";
        }

        // Locale formatted date
        public static String getNowDate() 
        {
            DateTime now = DateTime.Now;
            return now.ToLocalTime().ToString();
        }

        public static String MD5Hash(byte[] bytes) 
        {
#if ANDROID
            try
            {
                MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
                digest.update(bytes);
                byte messageDigest[] = digest.digest();
                String hashCode = Base64.encodeToString(messageDigest, Base64.DEFAULT);
                return hashCode.trim();
            } 
            catch (NoSuchAlgorithmException e) 
            {
                HaloLog.e("NoSuchAlgorithmException: ", e);
            } 
            catch (Throwable t)
            {
                HaloLog.e("Throwable: ", t);
            }
#else
            HashAlgorithmProvider hashProvider = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha256);
            IBuffer buff = CryptographicBuffer.CreateFromByteArray(bytes);
            var hashed = hashProvider.HashData(buff);
            var res = CryptographicBuffer.EncodeToBase64String(hashed);
            return res;

            //SHA256Managed sha256 = new SHA256Managed();
            //sha256.Initialize();
            //byte [] messageDigest = sha256.ComputeHash(bytes);
            //return Convert.ToBase64String(messageDigest).Trim();
#endif
        }

        public static String MD5Hash(String str)
        {
#if ANDROID
            try 
            {
                MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
                digest.update(str.getBytes());
                byte messageDigest[] = digest.digest();
                String hashCode = Base64.encodeToString(messageDigest, Base64.DEFAULT);
                return hashCode.trim();
            } 
            catch (NoSuchAlgorithmException e)
            {
                HaloLog.e("NoSuchAlgorithmException: ", e);
            } 
            catch (Throwable t)
            {
                HaloLog.e("Throwable: ", t);
            }
#else
            HashAlgorithmProvider hashProvider = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha256);
            IBuffer buff = CryptographicBuffer.ConvertStringToBinary(str, BinaryStringEncoding.Utf8);
            var hashed = hashProvider.HashData(buff);
            var res = CryptographicBuffer.EncodeToBase64String(hashed);
            return res;

            //SHA256Managed sha256 = new SHA256Managed();
            //sha256.Initialize();
            //byte[] bytes = new byte[str.Length * sizeof(char)];
            //System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            //byte[] messageDigest = sha256.ComputeHash(bytes);
            //return Convert.ToBase64String(messageDigest).Trim();

#endif
        }

        public static String Base64Encode(byte[] bytesToEncode) 
        {
#if ANDROID
            return Base64.encodeToString(bytesToEncode, Base64.DEFAULT);
#else
            return Convert.ToBase64String(bytesToEncode);
#endif
        }

        public static byte[] Base64Decode(String encodedstring) 
        {
#if ANDROID
            return Base64.decode(encodedstring, Base64.DEFAULT);
#else
            return Convert.FromBase64String(encodedstring);
#endif
        }
	
        public static String getSystemName() 
        {
            TypeInfo ti = typeof(DeviceUtils).GetTypeInfo();
            return ti.Assembly.ManifestModule.Assembly.GetName().Name;
        }

        public static String getSystemVersion() 
        {
            if (string.IsNullOrEmpty(APP_VERSION))
            {
                TypeInfo ti = typeof(DeviceUtils).GetTypeInfo();
                foreach(CustomAttributeData cad in ti.Assembly.ManifestModule.Assembly.ManifestModule.Assembly.CustomAttributes)
                {
                    if(cad.AttributeType == typeof(AssemblyFileVersionAttribute))
                    {
                    	APP_VERSION = cad.ConstructorArguments[0].Value as string;

                        HaloLog.d("App version = " + APP_VERSION);
                    }
                }
            }
            return APP_VERSION;
        }

        public static String getDeviceModel() 
        {
            Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation di = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();
            return di.SystemManufacturer + " " + di.SystemProductName;
            //e.g. "NOKIA Lumia 928";
        }
	
        public static String getDeviceIdString() 
        {
            if(mDeviceId.Length > 0)
                return mDeviceId;
#if ANDROID
            mDeviceId = LocalStorage.getDeviceID();
            if(mDeviceId != null && mDeviceId.length() > 0)
                return mDeviceId;
		
            HaloLog.i("getDeviceIdString() called.");
		
            Context ctx = LocalStorage.getAppContext();
            TelephonyManager tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
            if (tm != null)
            {
                HaloLog.i("-- TelephonyManager is not NULL.");
                // **** 1st attempt ****
                //
                mDeviceId = tm.getDeviceId();			
            }
            else
            {
                HaloLog.i("-- TelephonyManager is NULL.");
            }
		
            if (mDeviceId == null || mDeviceId.length() == 0)
            {
                HaloLog.i("-- TelephonyManager.getDeviceId() is not available.");
			
                // **** 2nd attempt ****
                //
                // Secure.ANDROID_ID - A 64-bit number (as a hex string) that is randomly generated on the
                // device's first boot and should remain constant for the lifetime of the device.
                // (The value may change if a factory reset is performed on the device.)
                //
                mDeviceId = Secure.getString(ctx.getContentResolver(), Secure.ANDROID_ID);			
                if (mDeviceId != null && mDeviceId.length() > 0)
                {			
                    //HaloLog.i("-- Secure.ANDROID_ID: " + mDeviceId);
                }
                else
                {
                    HaloLog.i("-- Secure.ANDROID_ID is not available.");				
                }
            }
            else
            {
                //HaloLog.i("-- TelephonyManager.getDeviceId(): " + mDeviceId);						
            }			
	
            if (mDeviceId == null || (mDeviceId != null && mDeviceId.length() == 0))
            {
						
                // **** 3rd attempt ****
                //
                String ethernetMacAddress = getEthernetMACAddress(); 			
                if (ethernetMacAddress != null && ethernetMacAddress.length() > 0)
                {
                    //HaloLog.i("-- Ethernet (LAN) MAC address: " + ethernetMacAddress);
                    mDeviceId = ethernetMacAddress.replace(":", "0");
                    //HaloLog.i("-- Ethernet (LAN) MAC address used as ID: " + mDeviceId);				
                }			
                else
                {
                    HaloLog.i("-- Ethernet (LAN) MAC address is not available.");
				
                    // **** 4th attempt ****				
                    String WIFIMacAddress = getMacAddress();
                    if (WIFIMacAddress != null && WIFIMacAddress.length() > 0)
                    {
                        //HaloLog.i("-- WIFI MAC address: " + WIFIMacAddress);
                        mDeviceId = WIFIMacAddress.replace(":", "0");
                        //HaloLog.i("-- WIFI MAC address used as ID: " + mDeviceId);				
                    }
                    else
                    {
                        HaloLog.i("-- WIFI MAC address is not available.");
					
                        // **** 5th attempt ****						
                        String hwdSerial = android.os.Build.SERIAL;					
                        if (hwdSerial != null && hwdSerial.length() > 0) 
                        {
                            HaloLog.i("-- android.os.Build.SERIAL: " + hwdSerial);
                            mDeviceId = hwdSerial;
                        }
                        else
                        {
                            HaloLog.i("-- android.os.Build.SERIAL is not available.");					
                            // deviceId = "1928374655";
                            mDeviceId = UUID.randomUUID().toString();
                        }															
                    }											
                }			
            }
									
            //HaloLog.i("-- Device ID produced: " + mDeviceId);
	                     
            if(mDeviceId == null || 
                mDeviceId.length() == 0 || 
                mDeviceId.indexOf("00000000000000") != -1)
            {
                mDeviceId = UUID.randomUUID().toString();
            }
            LocalStorage.setDeviceID(mDeviceId);
#else
            mDeviceId = getUUID();
#endif
            return mDeviceId;
        }
	
        public static int getInstalledPlatformType()
        {
#if REG_ANDROID
            return 7;
#else
            return APP_INSTPLATFORM;
#endif
        }
	
        public static bool checkSU()
        {
#if ANDROID
            if (new DoCmd().exec(SHELL_CMD.check_su_binary) != null){
                return true;
            }else{
                return false;
            }
#else
            return false;
#endif
        }
	
        public static bool getDeviceRootedStatus() 
        {
#if ANDROID
            // Get from build info.
            String buildTags = android.os.Build.TAGS;
            if (buildTags != null && buildTags.contains("test-keys")) {
                HaloLog.w("---- Device is rooted.");
                return true;
            }		
            // Check if the file "/system/app/Superuser.apk" is present.
            try 
            {
                File file = new File("/system/app/Superuser.apk");
                if ( file.exists() ) {
                    HaloLog.w( "---- Device is rooted.");	    		
                    return true;
                }
            }
            catch (Throwable e1)
            {
            }	
#else
#endif
            if(checkSU())
            {
                HaloLog.w( "---- Device is rooted.");
                return true;
            }
	    
            HaloLog.w( "---- Device is not rooted.");	    
            return false;
        }	

#if ANDROID
    /**
	 * Returns the encryption status of the device. Prior to Honeycomb, whole device encryption was
	 * not supported by Android, and this method returns ENCRYPTION_STATUS_UNSUPPORTED.
	 * 
	 * @return One of the following constants from DevicePolicyManager:
	 *         ENCRYPTION_STATUS_UNSUPPORTED, ENCRYPTION_STATUS_INACTIVE,
	 *         ENCRYPTION_STATUS_ACTIVATING, or ENCRYPTION_STATUS_ACTIVE.
	 */
	//@TargetApi(11)
	public static int getDeviceEncryptionStatus() {
	
	    int status = DevicePolicyManager.ENCRYPTION_STATUS_UNSUPPORTED;
	
	    if (Build.VERSION.SDK_INT >= 11) {
	    	Context ctx = LocalStorage.getAppContext();
	        final DevicePolicyManager dpm = (DevicePolicyManager) ctx.getSystemService(Context.DEVICE_POLICY_SERVICE);
	        if (dpm != null) {
	            status = dpm.getStorageEncryptionStatus();
	        }
	    }
	
	    return status;
	}
#endif
        public static String getKernelVersion()
        {
            Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation di = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();
            //return di.SystemFirmwareVersion;
            return di.SystemSku;
        }

        /// <summary>
        /// Returns the version of this HaloWP library
        /// </summary>
        /// <returns></returns>
        public static String getVersion()
        {
            string haloAppVersion = "0.0.0.0";
            TypeInfo ti = typeof(DeviceUtils).GetTypeInfo();
            foreach (CustomAttributeData cad in ti.Assembly.CustomAttributes)
            {
                if (cad.AttributeType == typeof(AssemblyFileVersionAttribute))
                {
                    haloAppVersion = cad.ConstructorArguments[0].Value as string;

                    HaloLog.d("App version = " + haloAppVersion);
                }
            }

            return haloAppVersion;
        }

        public static String getUUID()
        {
            HardwareToken token = HardwareIdentification.GetPackageSpecificToken(null);
            IBuffer hardwareId = token.Id;

            HashAlgorithmProvider hasher = HashAlgorithmProvider.OpenAlgorithm("MD5");
            IBuffer hashed = hasher.HashData(hardwareId);

            string hashedString = CryptographicBuffer.EncodeToHexString(hashed);
            return hashedString;
        }

        public static String getPhoneNum()
        {
#if ANDROID
            String phoneNum = null;
            Context ctx = LocalStorage.getAppContext();
            if (ctx != null)
            {
                TelephonyManager tm = (TelephonyManager)ctx.getSystemService(Context.TELEPHONY_SERVICE);
                if (tm != null)
                {
                    phoneNum = tm.getLine1Number();
                }
            }
            if (phoneNum == null || phoneNum.length() == 0)
                phoneNum = "N/A";
            return phoneNum;
#else
           
            return "N/A";
#endif
        }

        internal static string getDeviceManufacture()
        {
            Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation di = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();
            return di.SystemManufacturer;
            //e.g. "NOKIA;
        }
    }
}
