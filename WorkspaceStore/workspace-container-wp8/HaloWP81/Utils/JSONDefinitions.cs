﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloWP8.Utils
{
    public sealed class JSONObject : Dictionary<string, object>
    {
        public static JSONObject DeserializeObject(string json)
        {
            return JsonConvert.DeserializeObject<JSONObject>(json);
        }

        public static string SerializeObject(JSONObject jo)
        {
            string result = JsonConvert.SerializeObject(jo/*, Formatting.Indented*/);
            return result;
        }

        public void put(string key, object value)
        {
            this.Add(key, value);
        }

        public int optInt(string key)
        {
            return (int)this[key];
        }

        public int optInt(string key, int defaultValue)
        {
            int value = defaultValue;
            try
            {
                value = (int)this[key];
            }
            catch(KeyNotFoundException)
            {
                value = defaultValue;
            }
            return value;
        }

        public string optString(string key)
        {
            return this[key] as string;
        }

        public bool optBoolean(string key)
        {
            return (bool)this[key];
        }
    }

    public sealed class JSONArray : List<object>
    {
        public static JSONArray DeserializeArray(string json)
        {
            return JsonConvert.DeserializeObject<JSONArray>(json);
        }

        public static string SerializeArray(JSONArray jo)
        {
            return JsonConvert.SerializeObject(jo);
        }

        public void put(int index, object value)
        {
            this[index] = value;
        }

        public void put(object value)
        {
            this.Add(value);
        }

        public int optInt(int index)
        {
            return (int)this[index];
        }

        public string optString(int index)
        {
            return this[index] as string;
        }

        public bool optBoolean(int index)
        {
            return (bool)this[index];
        }

        public new string ToString()
        {
            string retString = "[";
            int count = 0;
            foreach (JSONObject obj in this)
            {
                if(count > 0)
                {
                    retString += ",";
                }
                retString += JSONObject.SerializeObject(obj);
                count++;
            }
            retString += "]";
            return retString;
        }
    }
}
