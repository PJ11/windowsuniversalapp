﻿using HaloWP8.Utils;
using HaloPCL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloWP8
{
	public static class DEVICE_ACTION 
    {
		// public static int Unknown = 0;
		//public static int WipeData = 1;
        public static int WipeData { get { return 1; } }
        public static int Lock { get { return 2; } }
        public static int UnEnrollDevice { get { return 3; } }
        public static int SecurityInfo { get { return 4; } }
		public static int Alert{ get { return 5; } }
        public static int ApplyRedemptionCode { get { return 6; } }
        public static int CertificateList { get { return 7; } }
        public static int ClearPasscode { get { return 8; } }
        public static int DeviceInformation { get { return 9; } }
        public static int InstallMissingApps { get { return 10; } }
        public static int InstallApplication { get { return 11; } }
        public static int InstallApplicationList { get { return 12; } }
        public static int InstallProfile { get { return 13; } }
        public static int InstallWebclip { get { return 14; } }
        public static int ManagedApplicationList { get { return 15; } }
        public static int ProfileList { get { return 16; } }
        public static int ProvisioningProfileList { get { return 17; } }
        public static int RemoveMissingApps { get { return 18; } }
        public static int RemoveApplication { get { return 19; } }
        public static int RemoveProfile { get { return 20; } }
        public static int Restrictions { get { return 21; } }
        public static int Restart { get { return 22; } }
        public static int Query { get { return 23; } }

		// new command for Cloud client - when user's group is changed and
		// cloud client device need to re-register automatically with new group
		// token in the command
		public static int GroupChanged{ get { return 25; } }
        public static int ApplyFilePolicy { get { return 26; } }

		//Ophelia
        public static int OpheliaCheckUpdate { get { return 27; } }
        public static int OpheliaForceCheckUpdate { get { return 28; } }

        public static int GetScreenCapture { get { return 35; } }

        public static int Shutdown { get { return 29; } }
        public static int SendMessage { get { return 30; } }
        public static int GetProcesses { get { return 31; } }
        public static int GetServices { get { return 32; } }
        public static int KillProcess { get { return 33; } }

        public static int StartService { get { return 34; } }
        public static int StopService { get { return 36; } }
        public static int RestartService { get { return 37; } }
        public static int GetPerformanceMetrics { get { return 38; } }
        public static int SetDeviceLocation { get { return 39; } }
		// Keystone related actions
        public static int KeystoneAdminLock { get { return 40; } }
        public static int KeystoneUserLock { get { return 41; } }
        public static int KeystoneUnlock { get { return 42; } }
        public static int ResetPIN { get { return 43; } }
        public static int WipeWorkspaceData { get { return 44; } }

		// OnPremise commands
        public static int OnPremiseCommand { get { return 45; } }
	}

    public abstract class DWAuth
    {
        //public static int CMD_KeystoneAdminLock = DEVICE_ACTION.KeystoneAdminLock;
        //public static int CMD_KeystoneUserLock = DEVICE_ACTION.KeystoneUserLock;
        //public static int CMD_KeystoneUnlock = DEVICE_ACTION.KeystoneUnlock;
        //public static int CMD_ResetPIN = DEVICE_ACTION.ResetPIN;
        //public static int CMD_WipeWorkspaceData = DEVICE_ACTION.WipeWorkspaceData;
        //public static int CMD_SendMessage = DEVICE_ACTION.SendMessage;
        //public static int CMD_Query = DEVICE_ACTION.DeviceInformation;

        public static int CMD_KeystoneAdminLock { get { return DEVICE_ACTION.KeystoneAdminLock; } }
        public static int CMD_KeystoneUserLock { get { return DEVICE_ACTION.KeystoneUserLock; } }
        public static int CMD_KeystoneUnlock { get { return DEVICE_ACTION.KeystoneUnlock; } }
        public static int CMD_ResetPIN { get { return DEVICE_ACTION.ResetPIN; } }
        public static int CMD_WipeWorkspaceData { get { return DEVICE_ACTION.WipeWorkspaceData; } }
        public static int CMD_SendMessage { get { return DEVICE_ACTION.SendMessage; } }
        public static int CMD_Query { get { return DEVICE_ACTION.DeviceInformation; } }
	
	    private static DWAuth _inst;
        private static readonly object instLocker = new object();
        
        public static DWAuth getInstance()
        {
            if (_inst == null) 
            {
                try
                {
                    lock(instLocker)
                    {
                        _inst = new DWAuthImpl();
                    }
                } 
                catch (Exception)
                {
                    //HaloLog.e("Failed to initialize DWAuth!", e);
                }
            }
            return _inst;
        }

        /**
        * Creates an instance of DWAuth.  Mainly used for debugging purposes.<br>
        * 
        * @param[in] applicationContext Application context.<br>
        * @param[in] appId Application package id.<br>
        * @param[in] appVersion Application version.<br>
        * @return A DWAuth instance.
        */
        public static DWAuth getInstance(String appId, String appVersion) 
        {
            if (_inst == null) 
            {
                try
                {
                    DeviceUtils.APP_ID = appId;
                    DeviceUtils.APP_VERSION = appVersion;
                    _inst = new DWAuthImpl();
                }
                catch (Exception)
                {
                    //HaloLog.e("Failed to initialize DWAuth!", e);
                }
            }

            return _inst;
        }

        protected DWAuth()
        {
            //throw new InvalidOperationException("Use singleton");
        }
   
        /**
        * <strong>Required call</strong><br>
        * Cleans up all services and sessions at the end of the application
        * lifecycle.
        */
        public static void destroyInstance() 
        {
            lock(instLocker)
            {
                if (_inst != null) 
                {
                    _inst.close();
                    _inst = null;
                }
            }
        }

        protected abstract void close();
	
        /**
        * Version of the Keystone SDK.<br> This method is obsolete and was replaced by DWAuth::haloSDKVersion
        * 
        * @return Returns the current version of the Keystone SDK
        */
        public abstract String version();
        
        /**
        * Version of the Keystone SDK.<br>
        * 
        * @return Returns the current version of the Keystone SDK
        */
        public abstract String haloSDKVersion();
	
        /**
        * Authenticate user with CCM.<br>
        * 
        * @see DWStatusCode
        * @see DWAuthListener
        * @param[in] username CCM username (e.g. Corporate email).
        * @param[in] password User password.
        * @param[in] cb Authentication listener that will be used for callbacks.
        * @return Returned values are documented in DWStatusCode.   On success will return DWStatus_SUCCESS(0).
        */
        public abstract int authenticate(String username, String password,  DWAuthListener cb);

        /**
        * Authenticate user with CCM.  Mainly used for debugging purposes.<br> 
        * 
        * @see DWStatusCode
        * @see DWAuthListener
        * @param[in] url CCM Server URL. 
        * @param[in] username CCM username (e.g. Corporate email).
        * @param[in] password User password.
        * @param[in] cb Authentication listener that will be used for callbacks.
        * @return Returned values are documented in DWStatusCode.   On success will return DWStatus_SUCCESS(0). 
        */
        public abstract int authenticate(String url, String username, String password, DWAuthListener cb);
	
        /**
        * Authenticate user to CCM using domain credentials.<br>
        * 
        * @see DWStatusCode
        * @see DWAuthListener
        * @param[in] username Domain username.
        * @param[in] password Domain user password.
        * @param[in] tenantCode Tenant code - usually a four digits code. <br>This code will be used to fetch the tenant SSO configuration.  From that configuration,  the login process will be redirected to the onpremise authentication service to validate user domain credentials.
        * @param[in] cb Authentication listener that will be used for callbacks.
        * @return Returned values are documented in DWStatusCode.   On success will return DWStatus_SUCCESS(0).
        */
        public abstract int authenticateSSO(String username, String password, String tenantCode, DWAuthListener cb);

        /**
        * Authenticate user to CCM using domain credentials.  Mainly used for debugging purposes.<br> 
        * 
        * @see DWStatusCode
        * @see DWAuthListener
        * @param[in] url CCM Server URL. 
        * @param[in] username Domain username.
        * @param[in] password Domain user password.
        * @param[in] tenantCode Tenant code - usually a four digits code. <br>This code will be used to fetch the tenant SSO configuration.  From that configuration,  the login process will be redirected to the onpremise authentication service to validate user domain credentials.
        * @param[in] cb Authentication listener that will be used for callbacks.
        * @return Returned values are documented in DWStatusCode.   On success will return DWStatus_SUCCESS(0). 
        */
        public abstract int authenticateSSO(String url, String username, String password, String tenantCode, DWAuthListener cb);
	
        /**
        * Is the current user authenticated with CCM.<br>
        *
        * @return true if authenticated otherwise false
        */
        public abstract bool isauthenticated();
	
        /**
        * Logout user.  Removes all information and files associated to the logged in user account.
        *
        * @return Returned values are documented in DWStatusCode.  On success will return DWStatus_SUCCESS(0). 
        */
        public abstract int deauthenticate();

        /**
        * This function is used to request from CCM the the list of RDP connections that users can launch within Pocketcloud.<br> On completion, callback {@link DWSessionListener#onPocketCloudConnectionListResult()} will be called.
        * 
        * @see DWSessionListener
        * @param[in] cb Listener that will be handling the CCM List of Pocket Cloud RDP connections callback.  See DWSessionListener.
        * @return Returned values are documented in DWStatusCode.  On success will return DWStatus_SUCCESS(0).
        *
        * @details For more details about the pocket cloud list of RDP connections see in the example tab file pocketCloudConnection.txt.
        */
        public abstract int getPocketCloudConnectionList(DWSessionListener cb) ;

        /**
        * This function is used to setup the listener for incoming CCM commands.<br>
        * 
        * @see DWCCMCommandsListener
        * @param[in] cb Listener that will be handling CCM command callbacks.  See DWCCMCommandsListener.
        * @return Returned values are documented in DWStatusCode.  On success will return DWStatus_SUCCESS(0).
        */
        public abstract int setCCMCommandsListener(DWCCMCommandsListener cb);
	
        /**
        * This function is used to setup the listener for incoming CCM commands.  Mostly for debugging purposes if we need to setup a particular MQTT server.<br>
        * 
        * @see DWCCMCommandsListener
        * @param[in] mqttHost MQTT host name.
        * @param[in] mqttPort MQTT port (e.g. 1883).
        * @param[in] cb Listener that will be handling CCM command callbacks.  See DWCCMCommandsListener.
        * @return Returned values are documented in DWStatusCode.  On success will return DWStatus_SUCCESS(0).
        */
        public abstract int setCCMCommandsListener(String mqttHost, int mqttPort, DWCCMCommandsListener cb);
	
        /*
        public int getPocketCloudConnectionList(DWSessionListener cb) 
        {
            if(!isauthenticated())
                return (int)DWStatusCode.DWStatus_AUTH_INVALID;

            LocalStorage.setDWSessionListener(cb);
            if(mCommandService != null)
            {
                return mCommandService.getPocketCloudConnectionList().code;
            }		
            return (int)DWStatusCode.DWStatus_FAIL; 
        }
        */

        /**
        * This function is used to inform CCM whenever the Keystone application checks in.<br>
        * 
        * @see DWCCMCheckinListener
        * @param[in] cb Listener that will be handling the CCM checkin callback.  See DWCCMCheckinListener.
        * @return Returned values are documented in DWStatusCode.  On success will return DWStatus_SUCCESS(0).
        *
        * @details For more details about the checkin information being pushed to CCM see in the example tab file checkindata.txt.
        */
        public abstract DWStatusCode checkin(DWCCMCheckinListener cb);
	
        /** @example checkindata.txt
        * This is an example of data being pushed to CCM during checkin.
        */ 

        /** @example onProfileUpdate.txt
        * This is an example of data being pushed by CCM when Keystone profiles are being published/updated.
        */ 

        /** @example pocketCloudConnection.txt
        * This is an example of a list of Pocket Cloud RDP connections being returned following a call to getPocketCloudConnections.
        */ 

        /**
        * This function is used to request from CCM the current Keystone configuration.<br> On completion, callback {@link DWCCMFullConfigListener#onGetFullConfigCompleted()} will be called.
        * The configuration itself will be returned by the callback onProfileUpdate() from DWCCMCommandsListener.
        * 
        * @see DWCCMFullConfigListener
        * @see DWCCMCommandsListener
        * @param[in] cb Listener that will be handling the CCM configuration callback.  See DWCCMFullConfigListener.
        * @return Returned values are documented in DWStatusCode.  On success will return DWStatus_SUCCESS(0).
        *
        * @details For more details about the profile information being pushed to the Keystone application see in the example tab file onProfileUpdate.txt.
        */
        public abstract DWStatusCode getFullConfig(DWCCMFullConfigListener cb);

        /**
        * This function is used to request from CCM the current Keystone configuration.<br> On completion, callback {@link DWCCMFullConfigWithDefaultsListener#onGetFullConfigWithDefaultsCompleted()} will be called.
        * The configuration itself will be returned by the callback onProfileUpdate() from DWCCMCommandsListener.
        * 
        * 
        * @see DWCCMFullConfigWithDefaultsListener
        * @see DWCCMCommandsListener
        * @param[in] cb Listener that will be handling the CCM configuration callback.  See DWCCMFullConfigWithDefaultsListener.
        * @return Returned values are documented in DWStatusCode.  On success will return DWStatus_SUCCESS(0).
        *
        * @details For more details about the profile information being pushed to the Keystone application see in the example tab file onProfileUpdate.txt.
        */
        public abstract DWStatusCode getFullConfigWithDefaults(DWCCMFullConfigWithDefaultsListener cb);
	

        /**
        * This function is used to set the logging level within the API. 
        * 
        * @param[in] level Logging level.  Possible values are: DEBUG(3), ERROR(6), INFO(4), VERBOSE(2) & WARN(5).  Default is ERROR.
        */
        public abstract void setLogLevel(int level);

        /**
        * This function is used to enable Location tracking. By default is false. 
        * 
        * @param[in] value Set to true - Location info is sent periodically at checkin time. Set to false -  no location information are extracted from system
        * 
        */
        public abstract void setSendLocation(bool value);

        /**
        * This function is used to retrieve Location tracking configuration.  
        * 
        * @return return true if Location Manager is active and location is sent to CCM Server periodically.    
        */
        public abstract bool getSendLocation();
	
        /**
        * @return return String unique identifier
        * 
        * This function is used to retrieve UDID - unique identifier used to track the same device. On GSM/CDMA device this is the same with IMEI/MEID.  
        * from wifi we use Android serial number if available or MAC address if not.     
        */	
        public abstract String getUDID();

        /**
        * @return return true if rooted device is detected. false - otherwise  
        * 
        */	
        public abstract bool getJailbreak();
    }
}
