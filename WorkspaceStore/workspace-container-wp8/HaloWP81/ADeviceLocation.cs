﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using Windows.Devices.Geolocation;

namespace HaloWP8
{
    [DataContract]
    public class ADeviceLocation
    {
        [DataMember(Name = "longitude", EmitDefaultValue = true)]
        private String longitude;
        [DataMember(Name = "latitude", EmitDefaultValue = true)]
        private String latitude;
        [DataMember(Name = "lastKnownTime", EmitDefaultValue = false)]
        private String lastKnownTime;

        public async void getLocationNow()
        {
            try
            {
                MemoryStream sr = new MemoryStream(Encoding.UTF8.GetBytes(LocalStorage.getLastKnownLocation()));
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(ADeviceLocation));
                ADeviceLocation dl = (ADeviceLocation)ser.ReadObject(sr);
                longitude = dl.longitude;
                latitude = dl.latitude;
                lastKnownTime = dl.lastKnownTime;
            }
            catch (Exception e)
            {
                //HaloLog.e("Exception parsing JSON.", e);
            }
            try
            {
                Geolocator geoLocator = new Geolocator();
                Geoposition position = await geoLocator.GetGeopositionAsync();

                Double lat = position.Coordinate.Point.Position.Latitude;
                Double longtitude = position.Coordinate.Point.Position.Longitude;
                if (!lat.Equals(Double.NaN) && !longtitude.Equals(Double.NaN))
                {
                    longitude = longtitude.ToString();
                    latitude = lat.ToString();

                    TimeSpan span = position.Coordinate.Timestamp.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
                    lastKnownTime = Convert.ToString((long)(span.TotalMilliseconds));

                    MemoryStream stream1 = new MemoryStream();
                    DataContractJsonSerializer serOut = new DataContractJsonSerializer(typeof(ADeviceLocation));
                    serOut.WriteObject(stream1, this);
                    stream1.Position = 0;
                    StreamReader srOut = new StreamReader(stream1);
                    LocalStorage.setLastKnownLocation(srOut.ReadToEnd());
                }
            }
            catch (Exception e)
            {

            }
        }
    }
}
