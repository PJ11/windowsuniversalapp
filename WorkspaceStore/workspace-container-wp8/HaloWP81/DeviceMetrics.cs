﻿using HaloWP8.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Json;
using HaloPCL;
using HaloPCL.Utils;

namespace HaloWP8
{
    [DataContract]
    public sealed class Device
    {
        [DataMember(Name = "authenticationCode", EmitDefaultValue = true)]
        public string AuthenticationCode { get; set; }

        [DataMember(Name = "wyseIdentifier", EmitDefaultValue = true)]
        public string WyseIdentifier { get; set; }

        [DataMember(Name = "name", EmitDefaultValue = true)]
        public string Name { get; set; }

        [DataMember(Name = "productName", EmitDefaultValue = true)]
        public string ProductName { get; set; }

        [DataMember(Name = "networkType", EmitDefaultValue = true)]
        public string NetworkType { get; set; }

        [DataMember(Name = "phoneNum", EmitDefaultValue = true)]
        public string PhoneNum { get; set; }

        [DataMember(Name = "macAddress", EmitDefaultValue = true)]
        public string MacAddress { get; set; }

        [DataMember(Name = "wiFiMacAddress", EmitDefaultValue = true)]
        public string WiFiMacAddress { get; set; }

        [DataMember(Name = "modelNum", EmitDefaultValue = true)]
        public string ModelNum { get; set; }

        [DataMember(Name = "osBuildCodes", EmitDefaultValue = true)]
        public string OsBuildCodes { get; set; }

        [DataMember(Name = "osBuildVersion", EmitDefaultValue = true)]
        public string OsBuildVersion { get; set; }

        [DataMember(Name = "serialNum", EmitDefaultValue = true)]
        public string SerialNum { get; set; }

        [DataMember(Name = "vendor", EmitDefaultValue = true)]
        public string Vendor { get; set; }

        [DataMember(Name = "checkinInterval", EmitDefaultValue = true)]
        public int CheckinInvterval { get; set; }

        [DataMember(Name = "udid", EmitDefaultValue = true)]
        public string Udid { get; set; }

        [DataMember(Name = "isRoaming", EmitDefaultValue = true)]
        public bool Roaming { get; set; }

        [DataMember(Name = "isJailBroken", EmitDefaultValue = true)]
        public bool IsJailBroken { get; set; }

        [DataMember(Name = "uptime", EmitDefaultValue = true)]
        public string Uptime { get; set; }

        [DataMember(Name = "remoteAppDevice", EmitDefaultValue = true)]
        public ARemoteAppDevice remoteAppDevice { get; set; }

        [DataMember(Name = "deviceLocation", EmitDefaultValue = true)]
        public ADeviceLocation DeviceLocation { get; set; }

        public string PhoneDeviceId { get; set; }
        
        public int HardwareEncryptionCaps { get; set; }
        //public string DeviceLocation { get; set; }
        //public string Tags { get; set; }

        //jo.accumulate("tags", getTags());
        //jo.accumulate("deviceProperties", deviceProperties);

    }

    public sealed class DeviceType
    {
        public int type { get; set; }
        public int family { get; set; }
    }

    public sealed class RemoteApp
    {
        private static long serialVersionUID = 1L;
        private String appId;
        private String version;
        private int installedPlatformType;
        private DeviceType deviceType;
        private bool isPseudoDeviceRequired = false;
    }

    public sealed class RemoteAppDevice
    {
        private String name;
        private String macAddress;
        private DeviceType deviceType;
        private DateTime registrationTime;
        private DateTime lastCheckinTime;
        private String udid;
        private Device device;
        private String identifierForVendor;
        private int currentStatus;
        private String appVersion;
        private bool isRemoteAppCompliant = true;
        private RemoteApp remoteApp;
        //private Tenant tenant;  Needed for SSO
    }
    class DeviceMetrics
    {
        Device mDevice = new Device();

        public void setAuthenticationCode(string code)
        {
            mDevice.AuthenticationCode = code;
        }

        public void setWyseIdentifier(string id)
        {
            mDevice.WyseIdentifier = id;
        }

        public bool getIsJailBroken()
        {
            return mDevice.IsJailBroken;
        }

        public void collectRequiredMetrics()
        {
            HaloLog.i("collectDeviceMetrics() called.");
#if ANDROID
            try
            {
                setName(DeviceUtils.getDeviceName());
                setMacAddress(DeviceUtils.getMacAddress());
                setWiFiMacAddress(DeviceUtils.getMacAddress());
                setModelNum(android.os.Build.MODEL);
                setOsBuildCodes(android.os.Build.VERSION.RELEASE);
                setOsBuildVersion(DeviceUtils.getKernelVersion());
                setPhoneNum(DeviceUtils.getPhoneNum());
                setPhoneDeviceId(DeviceUtils.getDeviceIdString());
                setSerialNum(DeviceUtils.getDeviceIdString());
                setVendor(android.os.Build.MANUFACTURER);
                setCheckinInterval(LocalStorage.CheckInTimeInterval / 1000);
                setUdid(DeviceUtils.getDeviceIdString());
                setIsJailBroken(DeviceUtils.getDeviceRootedStatus());
                setHarewareEncryptionCaps(DeviceUtils.getDeviceEncryptionStatus());
                setProductName(android.os.Build.PRODUCT);
                setUptime(String.valueOf(SystemClock.elapsedRealtime() / 1000l));

                if (LocalStorage.isLocationEnabled())
                {
                    setDeviceLocation(ADeviceLocation.getDeviceLocation());
                }
            }
            catch (Exception e)
            {
                HaloLog.e("DeviceMetrics error:", e);
            }
#else
            mDevice.Name = DeviceUtils.getDeviceName();
            mDevice.MacAddress = DeviceUtils.getMacAddress();//PJ. Need to check what is expected by server
            mDevice.WiFiMacAddress = DeviceUtils.getMacAddress();//PJ. Need to check what is expected by server
            mDevice.ModelNum = DeviceUtils.getDeviceModel();
            mDevice.OsBuildCodes = "8.10.65535.65535"; // TODO
            mDevice.OsBuildVersion =  DeviceUtils.getKernelVersion();
            mDevice.PhoneNum = DeviceUtils.getPhoneNum();//cannot be retrieved untill we use TAPI api.
            mDevice.PhoneDeviceId = DeviceUtils.getDeviceIdString();//PJ. Need to check what is expected by server why duplicate data in serialNum
            mDevice.SerialNum = DeviceUtils.getDeviceIdString();
            mDevice.Vendor = DeviceUtils.getDeviceManufacture();//E.g. NOKIA
            mDevice.CheckinInvterval = LocalStorage.CheckInTimeInterval / 1000;//Need to get details of checkintimeinterval.
            mDevice.Udid = DeviceUtils.getDeviceIdString();
            mDevice.IsJailBroken = (DeviceUtils.getDeviceRootedStatus());//Not supported by Microsoft to find that from any API.
            mDevice.HardwareEncryptionCaps = 0; // TODO What to do here?
            mDevice.ProductName = DeviceUtils.getSystemName();//( "Workspace"; // TODO What to do here?
            mDevice.Uptime = "a long time"; // TODO What to do here?
            if (mDevice.remoteAppDevice ==null)
            { mDevice.remoteAppDevice = new ARemoteAppDevice(); }
            mDevice.remoteAppDevice.currentStatus = 1;//"Active"
            mDevice.remoteAppDevice.appVersion = DeviceUtils.getSystemVersion();
            mDevice.remoteAppDevice.isRemoteAppCompliant = true;
#endif
        }

        public String toJSON()
        {
#if ANDROID
            JSONObject jo = new JSONObject();
            try
            {
                jo.accumulate("authenticationCode", getAuthenticationCode());
                jo.accumulate("wyseIdentifier", getWyseIdentifier());

                jo.accumulate("name", getName());
                jo.accumulate("productName", getProductName());
                jo.accumulate("networkType", getNetworkType());
                jo.accumulate("tags", getTags());

                jo.accumulate("phoneNum", getPhoneNum());

                jo.accumulate("macAddress", getMacAddress());
                jo.accumulate("wiFiMacAddress", getWiFiMacAddress());
                jo.accumulate("modelNum", getModelNum());
                jo.accumulate("osBuildCodes", getOsBuildCodes());
                jo.accumulate("osBuildVersion", getOsBuildVersion());
                jo.accumulate("serialNum", getSerialNum());
                jo.accumulate("vendor", getVendor());
                jo.accumulate("checkinInterval", getCheckinInterval());
                jo.accumulate("udid", getUdid());
                jo.accumulate("isRoaming", getIsRoaming());

                jo.accumulate("isJailBroken", getIsJailBroken());
                jo.accumulate("uptime", this.getUptime());

                ARemoteAppDevice rapp = (ARemoteAppDevice)this.getRemoteAppDevice();
                jo.accumulate("remoteAppDevice", (rapp != null) ? rapp.toJSON() : null);

                ADeviceLocation dl = (ADeviceLocation)getDeviceLocation();
                jo.accumulate("deviceLocation", (dl != null) ? dl.toJSON() : null);

                jo.accumulate("deviceProperties", deviceProperties);

                return jo.toString();

            }
            catch (JSONException e)
            {

                return null;
            }
#else

            MemoryStream stream1 = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Device));
            ser.WriteObject(stream1, mDevice);
            stream1.Position = 0;
            StreamReader sr = new StreamReader(stream1);
            return sr.ReadToEnd();
#endif
        } // End of collectRequiredMetrics().

        public void fillCustomInfo(String jsonData) 
        {
#if ANDROID
            HaloLog.i("fillCustomInfo called:{0}", jsonData);
		
            JSONObject jo;
            try 
            {
                if(StringUtils.isEmpty(jsonData))
                    jo = new JSONObject();
                else
                {
                    try
                    {
                        jo = new JSONObject(jsonData);
                    }
                    catch (JSONException e)
                    {
                        jo = new JSONObject();
                    }
                }
			
                ARemoteAppDevice remoteAppDevice = new ARemoteAppDevice();
                remoteAppDevice.setCurrentStatus(0);
                remoteAppDevice.setAppVersion(DeviceUtils.getRemoteAppVersion());    	
                remoteAppDevice.setIsRemoteAppCompliant(true);

                deviceProperties = new JSONArray();
			
                Iterator<?> keys = jo.keys();
                while(keys.hasNext())
                {
                    String key = (String) keys.next();
		        
                    if(key.equals("KEYSTONE_STATUS"))
                    {
                        remoteAppDevice.setCurrentStatus(jo.optInt("KEYSTONE_STATUS"));
                    }
                    else if(key.equals("KEYSTONE_VERSION"))
                    {
                        remoteAppDevice.setAppVersion(jo.optString(key, DeviceUtils.getRemoteAppVersion()));    	
                    }
                    else if(key.equals("KEYSTONE_COMPLIANT"))
                    {
                        remoteAppDevice.setIsRemoteAppCompliant(jo.optBoolean(key, true));
                    }
                    else
                    {
                        String val = null;
                        try
                        {
                            JSONObject value = jo.getJSONObject(key);
                            val = value.toString();
                        }
                        catch(Exception e)
                        {
                            try
                            {
                                JSONArray value = jo.getJSONArray(key);
                                val = value.toString();
                            }
                            catch(Exception e1)
                            {
                                val = jo.getString(key);
                            }
                        }
		        	
                        JSONObject sjo = new JSONObject();
                        sjo.put("name", key);
                        sjo.put("value", val);
                        deviceProperties.put(sjo);
                    }
                }			

                this.setRemoteAppDevice(remoteAppDevice);
			
                //this.setde
                this.setName(jo.optString("DEVICE_NAME", this.getName()));
                this.setModelName(jo.optString("DEVICE_MODEL", this.getModelName()));
                this.setOsArchitecture(jo.optString("DEVICE_OS", this.getOsArchitecture()));
                this.setOsBuildVersion(jo.optString("DEVICE_FIRMWARE_VERSION", this.getOsBuildVersion()));

                boolean isJailBroken = jo.optBoolean("DEVICE_JAILBROKEN", this.getIsJailBroken());
                this.setIsJailBroken(isJailBroken);
            } 
            catch (JSONException e)
            {
                HaloLog.i("fillCustomInfo called:%s",jsonData);
            }
#else
#endif
        }
    }
}
