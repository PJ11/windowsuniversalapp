﻿using HaloPCL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HaloWP8
{
    class QueueItem
    {
        public List<string> Params { get; set; }
        public int Command { get; set; }
        public int ThrID { get; set; }
    }

    /// <summary>
    ///Worker thread that queues and processes Communication events
    /// </summary>
    class BAlarmWorker
    {
        public const int BK_CMD_CHECKIN = 0;
        public const int BK_CMD_HEARTBEAT = 1;
        public const int BK_CMD_GPS = 2;
        public const int BK_AUTHENTICATE = 3;
        public const int BK_CMD_GETFULLCONFIG = 4;
        public const int BK_CMD_CHECKOUT = 5;
        public const int BK_CMD_GETPOCKETCLOUD_CONNECTIONLIST = 6;
        public const int BK_AUTHENTICATE_SSO = 7;
        public const int BK_CMD_GETFULLCONFIGWITHDEFAULTS = 8;
    
        protected Queue<QueueItem> mItems = new Queue<QueueItem>();
        private readonly static object syncLock = new object();

        private bool bExit = false;

        public BAlarmWorker()
        {
        }

        public async void RunWorkerAsync()
        {
            Task.Run<DWStatusCode>(() =>
            {
                try
                {
                    QueueItem item = null;
                    do
                    {
                        lock (syncLock)
                        {
                            if (bExit)
                                break;
                        }

                        lock (mItems)
                        {
                            if (mItems.Count == 0)
                                break;
                            item = mItems.Dequeue();
                            if (item == null)
                                break;
                        }
                        processNextCommand(item);
                        item = null;
                    } while (true);
                }
                catch (Exception)
                {
                    //HaloLog.e("Error Worker", t);
                }
                finally
                {
                }

                return DWStatusCode.DWStatus_INPROGRESS;
            });
        }

        private void processNextCommand(QueueItem item)
        {
            switch(item.Command)
            {
                case BK_CMD_CHECKIN:
                    CCMCommands.PerformCheckIn(null, item.ThrID);
                    break;
                case BK_CMD_HEARTBEAT:
                    CCMCommands.PerformHeartBeat(null, item.ThrID);
                    break;
                case BK_CMD_GPS:
                    break;
                case BK_AUTHENTICATE:
                    CCMCommands.PerformAuthenticate(null, item.Params[0], item.Params[1], item.ThrID);
                    break;
                case BK_CMD_GETFULLCONFIG:
                    CCMCommands.PerformGetFullConfig(null, item.ThrID);
                    break;
                case BK_CMD_CHECKOUT:
                    break;
                case BK_CMD_GETPOCKETCLOUD_CONNECTIONLIST:
                    break;
                case BK_AUTHENTICATE_SSO:
                    CCMCommands.PerformAuthenticateSSO(null, item.Params[0], item.Params[1], item.Params[2], item.ThrID);
                    break;
                case BK_CMD_GETFULLCONFIGWITHDEFAULTS:
                    CCMCommands.PerformGetFullConfigWithDefaults(null, item.ThrID);
                    break;
                default:
                    break;
            }
        }

        private void wait(int time)
        {
            try
            {
                //Monitor.Wait(mThread, time);
                Monitor.Wait(mItems, time);
            }
            catch (System.Threading.SynchronizationLockException e)
            {
                string message = e.Message;
            }
        }

        private void notify()
        {
            // Wake the thread up
            //Monitor.Pulse(mThread);
        }

        public void EnqueueItem(QueueItem item)
        {
            // Request the lock, and block until it is obtained.
            Monitor.Enter(mItems);
            try
            {
                // When the lock is obtained, add an element.
                mItems.Enqueue(item);
            }
            finally
            {
                // Ensure that the lock is released.
                Monitor.Exit(mItems);
            }
            notifyKATh(false);
        }

        public bool TryEnqueue(QueueItem item)
        {
            // Request the lock.
            if (Monitor.TryEnter(mItems))
            {
                try
                {
                    mItems.Enqueue(item);
                }
                finally
                {
                    // Ensure that the lock is released.
                    Monitor.Exit(mItems);
                }
                
                notifyKATh(false);
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool TryEnqueue(QueueItem item, int waitTime)
        {
            // Request the lock.
            if (Monitor.TryEnter(mItems, waitTime))
            {
                try
                {
                    mItems.Enqueue(item);
                }
                finally
                {
                    // Ensure that the lock is released.
                    Monitor.Exit(mItems);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        // Lock the queue and dequeue an element.
        public QueueItem Dequeue()
        {
            QueueItem retval;

            // Request the lock, and block until it is obtained.
            Monitor.Enter(mItems);
            try
            {
                // When the lock is obtained, dequeue an element.
                retval = mItems.Dequeue();
            }
            finally
            {
                // Ensure that the lock is released.
                Monitor.Exit(mItems);
            }

            return retval;
        }

        // Delete all elements that equal the given object.
        public int Remove(QueueItem item)
        {
            int removedCt = 0;

            // Wait until the lock is available and lock the queue.
            Monitor.Enter(mItems);
            try
            {
                int counter = mItems.Count;
                while (counter > 0)
                //Check each element.
                {
                    QueueItem elem = mItems.Dequeue();
                    if (!elem.Equals(item))
                    {
                        mItems.Enqueue(elem);
                    }
                    else
                    {
                        // Keep a count of items removed.
                        removedCt += 1;
                    }
                    counter = counter - 1;
                }
            }
            finally
            {
                // Ensure that the lock is released.
                Monitor.Exit(mItems);
            }

            return removedCt;
        }


        /// <summary>
        /// Will happen on the Main UI Thread
        /// </summary>
        /// <param name="t"></param>
        public void QueueItem(QueueItem item)
        {
            lock(syncLock)
            {
                mItems.Enqueue(item);
            }
            notifyKATh(false);
        }

        /// <summary>
        /// Will happen on the Main UI Thread
        /// </summary>
        /// <param name="t"></param>
        public void notifyKATh(bool b)
        {
            lock (syncLock)
            {
                bExit = b;
            }
            try
            {
                // tell the thread about the changes
                this.notify();
            }
            catch (Exception)
            {
            }
        }
    }

    public class BAlarmService
    {
        static BAlarmWorker worker = null;
        
        public BAlarmService()
        {
        }

        //
        public static DWStatusCode queueWork(Context context, int type, List<String> data, int trId)
        {
            QueueItem item = new QueueItem();
            item.Command = type;
            item.Params = data;
            item.ThrID = trId;

            if(worker == null)
            {
                worker = new BAlarmWorker();
            }
            lock (worker)
            {
                worker.QueueItem(item);   
            }

            worker.RunWorkerAsync();

            return DWStatusCode.DWStatus_INPROGRESS;
        }
    }
}
