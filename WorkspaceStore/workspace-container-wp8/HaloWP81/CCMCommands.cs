﻿using HaloWP8.httpmon;
using HaloPCL;
using HaloPCL.httpmon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloWP8
{
    public sealed class CCMCommands
    {
        public static void PerformHeartBeat(Context applicationContext, int trId)
        {
            new CCMCommandManager().performHeartBeat(applicationContext, false, trId);
        }

        public static void PerformAuthenticate(Context applicationContext, String user, String psw, int trId)
        {
            CcmHttpDataRet state = new CCMCommandManager().authenticate(applicationContext, user, psw, trId);
            DWAuthListener al = (DWAuthListener)CmdManager.getCommand(trId);
            if (al != null)
            {
                if (state.mResult == DWStatusCode.DWStatus_SUCCESS)
                    al.onAuthenticateResult(state.mResult, "");
                else
                    al.onAuthenticateResult(state.mResult, state.mStrData);
            }
        }

        public static void PerformAuthenticateSSO(Context applicationContext, String user, String psw, String tenantCode, int trId)
        {
            CcmHttpDataRet state = new CCMCommandManager().authenticateSSO(applicationContext, user, psw, tenantCode, trId);
            DWAuthListener al = (DWAuthListener)CmdManager.getCommand(trId);
            if (state != null && al != null)
            {
                if (state.mResult == DWStatusCode.DWStatus_SUCCESS)
                    al.onAuthenticateResult(state.mResult, "");
                else
                    al.onAuthenticateResult(state.mResult, state.mStrData);
            }
        }

        public static void PerformCheckIn(Context applicationContext, int trId)
        {
            CcmHttpDataRet state = new CCMCommandManager().checkInToServer(applicationContext, false, trId);
            DWCCMCheckinListener al = (DWCCMCheckinListener)CmdManager.getCommand(trId);
            if (al != null)
            {
                if (state.mResult == DWStatusCode.DWStatus_SUCCESS)
                    al.onChekinCompleted(state.mResult, "");
                else
                    al.onChekinCompleted(state.mResult, state.mStrData);
            }

        }

        public static CcmHttpDataRet PerformACK(Context ctx, DeviceCommand adc, int trId)
        {
            return new CCMCommandManager().performAck(ctx, adc, trId);
        }

        public static void PerformGetFullConfig(Context ctx, int trId)
        {
            CcmHttpDataRet state = new CCMCommandManager().getFullConfig(ctx, trId);
            DWCCMFullConfigListener al = (DWCCMFullConfigListener)CmdManager.getCommand(trId);
            if (al != null)
            {
                if (state.mResult == DWStatusCode.DWStatus_SUCCESS)
                    al.onGetFullConfigCompleted(state.mResult, "");
                else
                    al.onGetFullConfigCompleted(state.mResult, state.mStrData);
            }
        }

        public static void PerformGetFullConfigWithDefaults(Context ctx, int trId)
        {
            CcmHttpDataRet state = new CCMCommandManager().getFullConfigWithDefaults(ctx, trId);
            DWCCMFullConfigWithDefaultsListener al = (DWCCMFullConfigWithDefaultsListener)CmdManager.getCommand(trId);
            if (al != null)
            {
                if (state.mResult == DWStatusCode.DWStatus_SUCCESS)
                    al.onGetFullConfigWithDefaultsCompleted(state.mResult, "");
                else
                    al.onGetFullConfigWithDefaultsCompleted(state.mResult, state.mStrData);
            }
        }

        public static void PerformGPSUpdate(Context applicationContext, int trId)
        {
            //		if(GlobalConstants.Instance().getAllowLocation())
            //		{
            //			BAlarmService.PerformGPSUpdate(this.getApplicationContext());
            //		}
        }

        public static DWStatusCode PerformCheckout(Context ctx, int trId)
        {
            CcmHttpDataRet state = new CCMCommandManager().performCheckout(ctx);
            return state.mResult;
        }

        public static void PerformGetPocketCloudConnectionList(Context ctx, int trId)
        {
            CcmHttpDataRet state = new CCMCommandManager().getPocketCloudConnectionList(ctx, trId);
            //DWSessionListener al = (DWSessionListener)CmdManager.getCommand(trId);
            //if(al!= null){
            //if(state.mResult == DWStatusCode.DWStatus_SUCCESS)
            //al.onPocketCloudConnectionListResult(state.mResult, state.mStrData, "" );
            //else
            //al.onPocketCloudConnectionListResult(state.mResult, "", state.mStrData );
            //}
        }


        internal static DWStatusCode PerformCheckout(int p)
        {
            CcmHttpDataRet state = new CCMCommandManager().performCheckout(null);
            return state.mResult;
        }

    }
}
