﻿using HaloWP8.Utils;
using HaloPCL;
using System;
using System.Collections.Generic;
using Windows.Storage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaloWP8
{
    public static class URLStore
    {
        public static String[] ServerCommandsList =
        {
            "/ccm-web/open/deviceRegister",	        // 0
            "/ccm-web/open/deviceLogin",		    // 1	
            "/ccm-web/device/checkin",			    // 2
            "/ccm-web/device/heartbeat",		    // 3
            "/ccm-web/device/status",			    // 4
            "/ccm-web/device/getCommand",		    // 5
            "/ccm-web/device/unregister",		    // 6
            "/ccm-web/device/managedAppPolicy",     // 7
            "/ccm-web/device/userIcon", 		    // 8
            "/ccm-web/device/fullConfig", 		    // 9
            "/ccm-web/device/getInProgressCommand", //10	 
            "/ccm-web/device/genericAudit",	        //11
            "",	                                    //12
            "/ccm-web/sso/{TenantCode}/device/android"  //13
        };

        public static String MQTTDEFAULT_URL = "10.148.211.136";
        public static int MQTTDEFAULT_PORT = 1883;
        public static String DEFAULT_URL = "http://10.148.211.136:8080";

        public static String REGISTRATION_URL = "/ccm-web/open/deviceRegister";
        public static String CHECKIN_URL = "/ccm-web/device/checkin";
        public static String LOGIN_URL = "/ccm-web/open/deviceLogin";
        public static String HEARTBEAT_URL = "/ccm-web/device/heartbeat";
        public static String STATUS_URL = "/ccm-web/device/status";
        public static String COMMAND_URL = "/ccm-web/device/getCommand";
        public static String CHECKOUT_URL = "/ccm-web/device/unregister";
        public static String MANAGEDAPP_URL = "/ccm-web/device/managedAppPolicy";
        public static String PHOTO_TAKING_URL = "/ccm-web/device/userIcon";
        public static String FULLCONFIG_URL = "/ccm-web/device/fullConfig";
        public static String INPROGRESSCOMMANDS_URL = "/ccm-web/device/getInProgressCommand";
        public static String REALTIMEEVENTS = "/ccm-web/device/genericAudit";
        public static String REMOTELOGIN_URL = "/ccm-web/remoteApp/remoteAppLogin";
        public static String POKETCLOUD_URL = "/ccm-web/remoteApp/fullconfig/401?noFilter=yes";
        public static String REMOTELOGINSSO_URL = "/ccm-web/sso/{TenantCode}/device/remoteAppLogin";
        public static String LOGIN_SSO_SUMBIT_URL = "/ccmauth/onPremise/ccmauthLogin";
        public static String FULLCONFIGWITHDEFAULTS_URL = "/ccm-web/device/fullConfigWithDefaults";
    }

    public class SharedPreferences
    {

        public int getInt(string name, int defaultValue)
        {
            object val = 0;
            if (ApplicationData.Current.LocalSettings.Values.TryGetValue(name, out val))
            {
                return (int)val;
            }
            return defaultValue;
        }

        public string getString(string name, string defaultValue)
        {
            object val = string.Empty;
            if (ApplicationData.Current.LocalSettings.Values.TryGetValue(name, out val))
            {
                return (string)val;
            }
            return defaultValue;
        }

        public void putInt(string name, int val)
        {
            StoreValueInLocalSetting(name, val);
        }
        public void putString(string name, string val)
        {
            StoreValueInLocalSetting(name, val);
        }

        private void StoreValueInLocalSetting(string name, object val)
        {
            try
            {
                //check if key already exists
                if (ApplicationData.Current.LocalSettings.Values[name] == null)
                {
                    ApplicationData.Current.LocalSettings.Values[name] = val;
                }
                else
                {
                    // Key already exists
                    ApplicationData.Current.LocalSettings.Values[name] = val;
                }
            }
            catch (ArgumentException)
            {
                // Key already exists
                //commenting, no need of this
                //settings[name] = val;
            }
        }
    }

    public class LocalStorage
    {
        public static int ConnectionTimeout = 30000; // in ms
        public static int SocketTimeout = 30000; // in ms

        public static int LogInConnectionTimeout = 10000; // in ms
        public static int LogInSocketTimeout = 10000; // in ms

        public static int CheckInTimeInterval = 8 * 3600 * 1000;
        public static int HearBeatTimeInterval = 3600 * 1000;
        //  public static  int CheckInTimeInterval = 300 * 1000;
        //  public static  int HearBeatTimeInterval = 150 * 1000;
        //  public static  int CheckInTimeInterval = 15 * 1000;
        //  public static  int HearBeatTimeInterval = 10 * 1000;


        public static String APP_ID = "com.wyse.halo.data";

        private static String kIsRegistered = "isRegistered";
        private static String kWyseID = "WyseID";
        private static String kAuthCode = "AuthCode";
        private static String kSavedUrl = "SavedUrl";
        private static String kSavedMqttUrl = "SavedMqttUrl";


        private static String kLastKnownLocation = "LastLocation";
        private static String kIsLocationEnabled = "IsLocationEnabled";
        private static String kAlarmUpdated = "AlarmUpdated";
        private static String kDeviceID = "WyseDeviceID";

        private LocalStorage()
        {

        }

        static LocalStorage _inst;

        public static LocalStorage Instance()
        {
            if (_inst == null)
            {
                _inst = new LocalStorage();
                initCheck();
            }
            return _inst;
        }

        private static DWAuthListener mCallback = null;
        public static DWCCMCommandsListener mCommandsListener = null;
        public static DWCCMCheckinListener mCheckinListener = null;
        public static DWCCMFullConfigListener mFullConfigListener = null;
        private static DWSessionListener mSessionCallback = null;
        private static DWCCMFullConfigWithDefaultsListener mFullConfigWithDefaultsListener = null;

        private readonly static object syncLock = new object();

        public static void setCallback(DWAuthListener cb)
        {
            mCallback = cb;
        }

        public static DWAuthListener getCallback()
        {
            return mCallback;
        }

        public static DWCCMCommandsListener getDWCCMCommandsListener()
        {
            lock (syncLock)
            {
                return mCommandsListener;
            }
        }

        public static void setDWCCMCommandsListener(DWCCMCommandsListener cb)
        {
            lock (syncLock)
            {
                mCommandsListener = cb;
            }
        }

        public static DWCCMCheckinListener getDWCCMCheckinListener()
        {
            return mCheckinListener;
        }

        public static void setDWCCMCheckinListener(DWCCMCheckinListener cb)
        {
            mCheckinListener = cb;
        }

        public static DWCCMFullConfigListener getDWCCMFullConfigListener()
        {
            return mFullConfigListener;
        }

        public static void setDWCCMFullConfigListener(DWCCMFullConfigListener cb)
        {
            mFullConfigListener = cb;
        }

        public static DWCCMFullConfigWithDefaultsListener getDWCCMFullConfigWithDefaultsListener()
        {
            return mFullConfigWithDefaultsListener;
        }

        public static void setDWCCMFullConfigWithDefaultsListener(DWCCMFullConfigWithDefaultsListener cb)
        {
            mFullConfigWithDefaultsListener = cb;
        }

        public static DWSessionListener getDWSessionListener()
        {
            return mSessionCallback;
        }

        public static void setDWSessionListener(DWSessionListener cb)
        {
            mSessionCallback = cb;
        }

        public static Context mContext = null;
        public static Context getAppContext()
        {
            return mContext;
        }

        public static void updateAppContext(Context c)
        {
            if (mContext == null)
                mContext = c;
        }

        public static void setAppContext(Context c)
        {
            mContext = c;
        }

        private static SharedPreferences mSettings = null;
        private static bool initCheck()
        {
            lock (syncLock)
            {
                mSettings = new SharedPreferences();
            }
            return true;
        }


        private static int mIsRegistered = -1;
        public static bool isRegister()
        {
            if (mIsRegistered == -1)
            {
                initCheck();
                mIsRegistered = mSettings.getInt(kIsRegistered, 0);
            }
            return mIsRegistered != 0;
        }

        public static void setIsRegister(bool b)
        {
            int val = b ? 1 : 0;
            if (mIsRegistered != val)
            {
                initCheck();
                mIsRegistered = val;
                mSettings.putInt(kIsRegistered, val);
            }
        }

        private static String agentFormat = "";
        private static String locale = DeviceUtils.getUserLocale();
        private static String systemName = DeviceUtils.getSystemName();
        private static String systemversion = DeviceUtils.getSystemVersion();
        private static String model = DeviceUtils.getDeviceModel();
        private static String LIBRARY_VERSION = "1.0";

        public static String getUserAgent()
        {
            if (agentFormat.Length == 0)
            {
                agentFormat = "Stratus /1.0.5 (" + systemName + " " + systemversion + "; " + locale + "; " + model + "; Revision: " + LIBRARY_VERSION + "; cls: D)";
            }
            return agentFormat;
        }

        private static String mUrl = "";

        public static void setUrl(String url)
        {
            mUrl = url;
            setSavedUrl(mUrl);
        }

        public static String getUrl()
        {
            if (mUrl.Length > 0)
                return mUrl;

            initCheck();

            mUrl = getSavedUrl();

            if (mUrl.Length > 0)
                return mUrl;

            mUrl = URLStore.DEFAULT_URL;
            return mUrl;
        }

        public static String getCHECKIN_URL()
        {
            return getUrl() + URLStore.CHECKIN_URL;
        }
        public static String getFULLCONFIG_URL()
        {
            return getUrl() + URLStore.FULLCONFIG_URL;
        }
        public static String getFULLCONFIGWITHDEFAULTS_URL()
        {
            return getUrl() + URLStore.FULLCONFIGWITHDEFAULTS_URL;
        }
        public static String getCOMMAND_URL()
        {
            return getUrl() + URLStore.COMMAND_URL;
        }
        public static String getINPROGRESS_COMMANDS()
        {
            return getUrl() + URLStore.INPROGRESSCOMMANDS_URL;
        }
        public static String getLOGIN_URL()
        {
            return getUrl() + URLStore.LOGIN_URL;
        }
        public static String getREMOTELOGIN_URL()
        {
            return getUrl() + URLStore.REMOTELOGIN_URL;
        }

        public static String getREMOTELOGINSSO_URL(String tenantCode)
        {
            String url = URLStore.REMOTELOGINSSO_URL.Replace("{TenantCode}", tenantCode);
            return getUrl() + url;
        }

        public static String getSTATUS_URL()
        {
            return getUrl() + URLStore.STATUS_URL;
        }
        public static String getHEARTBEAT_URL()
        {
            return getUrl() + URLStore.HEARTBEAT_URL;
        }
        public static String getCHECKOUT_URL()
        {
            return getUrl() + URLStore.CHECKOUT_URL;
        }
        public static String getPOKETCLOUD_URL()
        {
            return getUrl() + URLStore.POKETCLOUD_URL;
        }



        private static String mMqttUrl = "";
        public static void setMqttUrl(String url)
        {
            mMqttUrl = url;
            setSavedMqttUrl(mMqttUrl);
        }
        public static String getMqttUrl()
        {
            if (mMqttUrl.Length > 0)
                return mMqttUrl;
            initCheck();
            mMqttUrl = getSavedMqttUrl();
            if (mMqttUrl.Length > 0)
                return mMqttUrl;

            mMqttUrl = URLStore.MQTTDEFAULT_URL;
            return mMqttUrl;
        }



        private static String mWyseID = "";
        public static String getWyseID()
        {
            if (mWyseID.Length == 0)
            {
                initCheck();
                mWyseID = mSettings.getString(kWyseID, "");
            }
            return mWyseID;
        }

        public static void setWyseID(String id)
        {
            if (!mWyseID.Equals(id))
            {
                initCheck();
                mWyseID = id;
                mSettings.putString(kWyseID, id);
            }
        }


        private static String mAuthCode = "";
        public static String getAuthCode()
        {
            if (mAuthCode.Length == 0)
            {
                initCheck();
                mAuthCode = mSettings.getString(kAuthCode, "");
            }
            return mAuthCode;
        }

        public static void setAuthCode(String id)
        {
            if (!mAuthCode.Equals(id))
            {
                mAuthCode = id;
                initCheck();
                mSettings.putString(kAuthCode, id);
            }
        }

        private static int mIsLocationEnabled = -1;
        public static bool isLocationEnabled()
        {
            if (mIsLocationEnabled == -1)
            {
                initCheck();
                mIsLocationEnabled = mSettings.getInt(kIsLocationEnabled, 0);
            }
            return mIsLocationEnabled == 1;
        }

        public static void setLocationEnabled(bool value)
        {
            if ((mIsLocationEnabled == 1) != value)
            {
                mIsLocationEnabled = value ? 1 : 0;
                initCheck();
                mSettings.putInt(kIsLocationEnabled, mIsLocationEnabled);
            }
        }

        private static int mAlarmUpdated = -1;
        public static bool IsAlarmUpdated()
        {
            if (mAlarmUpdated == -1)
            {
                initCheck();
                mAlarmUpdated = mSettings.getInt(kAlarmUpdated, 0);
            }
            return mAlarmUpdated == 1;
        }

        public static void setAlarmUpdated(bool value)
        {
            if ((mAlarmUpdated == 1) != value)
            {
                mAlarmUpdated = value ? 1 : 0;
                initCheck();
                mSettings.putInt(kAlarmUpdated, mAlarmUpdated);
            }
        }

        private static String mSavedUrl = "";
        public static String getSavedUrl()
        {
            if (mSavedUrl.Length == 0)
            {
                initCheck();
                mSavedUrl = mSettings.getString(kSavedUrl, "");
            }
            return mSavedUrl;
        }

        public static void setSavedUrl(String id)
        {
            if (!mSavedUrl.Equals(id))
            {
                mSavedUrl = id;
                initCheck();
                mSettings.putString(kSavedUrl, id);
            }
        }

        private static String mSavedMqttUrl = "";
        public static String getSavedMqttUrl()
        {
            if (mSavedMqttUrl.Length == 0)
            {
                initCheck();
                mSavedMqttUrl = mSettings.getString(kSavedMqttUrl, "");
            }
            return mSavedMqttUrl;
        }

        public static void setSavedMqttUrl(String id)
        {
            if (!mSavedMqttUrl.Equals(id))
            {
                mSavedMqttUrl = id;
                initCheck();
                mSettings.putString(kSavedMqttUrl, id);
            }
        }

        private static String mLastKnownLocation = "";
        public static String getLastKnownLocation()
        {
            if (mLastKnownLocation.Length == 0)
            {
                initCheck();
                mLastKnownLocation = mSettings.getString(kLastKnownLocation, "{}");
            }
            return mLastKnownLocation;
        }

        public static void setLastKnownLocation(String id)
        {
            if (!mLastKnownLocation.Equals(id))
            {
                mLastKnownLocation = id;
                initCheck();
                mSettings.putString(kLastKnownLocation, id);
            }
        }

        private static String mDeviceID = "";
        public static String getDeviceID()
        {
            if (mDeviceID.Length == 0)
            {
                initCheck();
                mDeviceID = mSettings.getString(kDeviceID, "");
            }
            return mDeviceID;
        }

        public static void setDeviceID(String id)
        {
            if (!mDeviceID.Equals(id))
            {
                mDeviceID = id;
                initCheck();
                mSettings.putString(kDeviceID, id);
            }
        }
    }
}
