﻿// <copyright file="LoginScreen.xaml.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Loren R, Sajini PM</author>
// <email>Lorenr@dmpdev1.com,Sajini_PM@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary>Loren R, Sajini                           1.0                 First Version </summary>

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows;
using AbstractsPCL;
using Common;
using WorkspaceSurface;
using System.Linq;
using System.Net.NetworkInformation;
using Windows.UI.Core;
using Windows.UI.Notifications;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WorkspaceSurface;


namespace WorkspaceSurface
{
    #region NAMESPACE



    #endregion
    /// <summary>
    /// Login Screen 
    /// </summary>
    public partial class LoginScreen : Page
    {
        #region VARIABLES
        /// <summary>
        /// The The CCMServerObject- last CCM server item
        /// </summary>
        /// 
        ///  /// <summary>
        /// The Halo Service Object- halo
        /// </summary>
        private HaloService halo = null;

         #if WP_80_SILVERLIGHT
        private static CCMServerObject lastCCMServerItem = null;

       
        /// <summary>
        /// The CCMServerObject- item
        /// </summary>
        private CCMServerObject item;

        /// <summary>
        /// The List of CCM server Object CCM list
        /// </summary>
        private List<CCMServerObject> ccmList;
#endif
        /// <summary>
        /// The user name string
        /// </summary>
        private string userNameString = string.Empty;

        /// <summary>
        /// The password string
        /// </summary>
        private string passwordString = string.Empty;

        /// <summary>
        /// The m use enterprise credentials
        /// </summary>
        private bool useEnterpriseCredentials = false;
        #endregion

        #region CONSTRUCTOR
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginScreen"/> class.
        /// </summary>
        public LoginScreen()
        {
            this.InitializeComponent();
            //this.halo = App.mHaloService;
            this.signIn.IsEnabled = false;

            #if WP_80_SILVERLIGHT
            this.ccmList = new List<CCMServerObject>()
            {
                new CCMServerObject { ccm = "https://sk1.wyselab.com", mqtt = "sk1-pns.wyselab.com", mqttPort = 1883 },               
                new CCMServerObject { ccm = "https://sk2.wyselab.com", mqtt = "sk2-pns.wyselab.com", mqttPort = 1883 },
                new CCMServerObject { ccm = "https://sk3.wyselab.com", mqtt = "sk3-pns.wyselab.com", mqttPort = 1883 },
                new CCMServerObject { ccm = "https://us1.cloudclientmanager.com", mqtt = "us1-pns.cloudclientmanager.com", mqttPort = 1883 },
            };
            this.item = this.ccmList[3];
            this.halo.profileUpdateHandler += this.Halo_profileUpdateHandler;
            halo.AuthenticationFailureHandler += halo_AuthenticationFailureHandler;
            App.mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_UNINITIALIZED);
            this.CCmLongList.ItemsSource = this.ccmList;

            SSOSelection.Visibility = Visibility.Collapsed;
            this.useEnterpriseCredentials = false;
            LoginSelections.SetValue(Grid.RowProperty, 2);

#endif

            FileHandlingPCL.FileManager objFileMngr = new FileHandlingPCL.FileManager();
            objFileMngr.CreateRootFolders();

#if WP_80_SILVERLIGHT
            MyScroller.MouseMove += MyScroller_MouseMove;
#else
            MyScroller.PointerMoved += MyScroller_MouseMove;
#endif
        }

#if WP_80_SILVERLIGHT
        private void MyScroller_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            IconVisible.Visibility = Visibility.Visible;
            this.Focus();
        }
#else
        private void MyScroller_MouseMove(object sender, PointerRoutedEventArgs e)
        {
            IconVisible.Visibility = Visibility.Visible;
            this.Focus(FocusState.Programmatic);
        }
#endif
        #endregion


        /// <summary>
        /// Called when a page is no longer the active page in a frame.
        /// </summary>
        /// <param name="e">An object that contains the event data.</param>
        //   protected override void OnNavigatedFrom(NavigationEventArgs e)
        //   {
        //   if (checkbox.IsChecked == true)
        //      checkbox.IsChecked = true;

        //  else
        //  checkbox.IsChecked = false;

        //   }

        #if WP_80_SILVERLIGHT
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }
#endif


#if WP_80_SILVERLIGHT
        /// <summary>
        /// Called when the hardware Back button is pressed.
        /// </summary>
        /// <param name="e">Set e.Cancel to true to indicate that the request was handled by the application.</param>
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (ccmPopoUp.IsOpen)
            {
                e.Cancel = true;
                ccmPopoUp.IsOpen = false;
            }

            LayoutRoot.Opacity = 1;
        }


        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            if (ccmPopoUp.IsOpen)
            {
                e.Handled = true;
                ccmPopoUp.IsOpen = false;
            }

            LayoutRoot.Opacity = 1;
        }
#endif

        /// <summary>
        /// Recursively get the item.
        /// </summary>
        /// <typeparam name="T">The item to get.</typeparam>
        /// <param name="parents">Parent container.</param>
        /// <param name="objectList">Item list</param>
        private static void GetItemsRecursive<T>(DependencyObject parents, ref List<T> objectList) where T : DependencyObject
        {
            var childrenCount = VisualTreeHelper.GetChildrenCount(parents);

            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parents, i);

                if (child is T)
                {
                    objectList.Add(child as T);
                }

                GetItemsRecursive<T>(child, ref objectList);
            }

            return;
        }
#if WP_80_SILVERLIGHT
        void halo_AuthenticationFailureHandler(object sender, int error, string strError)
        {
            signIn.IsEnabled = true;
        }

#endif
        #if WP_80_SILVERLIGHT
        /// <summary>
        /// Handles the profileUpdateHandler event of the halo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ProfileUpdateEventArgs"/> instance containing the event data.</param>
        private async void Halo_profileUpdateHandler(object sender, ProfileUpdateEventArgs e)
        {
            if (e.FullConfiguration)
            {
                ////App.mManager = e.DKCNAppManager;
                App.mAppState.sendAppEvent(DKAppState.DKAppEvent.DK_APP_EVENT_CCM_AUTHENTICATE);

                App.mAppState.SaveCredentials(this.userNameString, this.passwordString);
                var url = string.Format("/Workspace;component/PIN_View/PINScreen.xaml?AppStartOrWakeup=false", this.passwordString);
                if (this.useEnterpriseCredentials)
                {
                    url = string.Format("/Workspace;component/PIN_View/PINScreen.xaml?EnterprisePassword={0}&AppStartOrWakeup=false", this.passwordString);
                }

                CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
                await dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                {

                });

                //// Not sure if this is thread-safe, but put the handling of profile update on the main thread.
                //System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                //{
                //    NavigationService.Navigate(new Uri(url, UriKind.Relative));
                //});
            }
            else
            {
                var itemsChanged = string.Empty;
                var changePin = false;
                //// Only partial update.
                ////Dictionary<string, string> pendingSettings = new Dictionary<string, string>();
                var savedSettings = e.DKCNAppManager.settingsUserOptions.Settings;

                // find out what's changed
                foreach (var key in e.DKCNAppManager.settingsPendingOptions.Settings.Keys)
                {
                    var pendingValue = e.DKCNAppManager.settingsPendingOptions.Settings[key];
                    var savedValue = savedSettings[key];
                    if (pendingValue != savedValue)
                    {
                        itemsChanged += key + " ";
                        savedSettings[key] = pendingValue;
                        if (key == HaloPolicySettingsAbstract.KEY_PIN_LENGTH)
                        {
                            changePin = true;
                        }
                    }
                }

                if (itemsChanged.Length > 0)
                {
                    //// Save the items
                    e.DKCNAppManager.storeUserSettings();

                    var message = string.Format("The following items have changed: {0}", itemsChanged);
                    //// Throw up a toast
                    var toast = new ToastNotification();
                    toast.CreateToastNotificationForVertical("CCM Server", message);

                    if (changePin)
                    {
                        //NavigationService.Navigate(new Uri("/Workspace;component/ChangePin.xaml", UriKind.Relative));
                        Common.Navigation.NavigationService.NavigateToPage("ChangePin");
                    }
                }
            }
        }
#endif
        /// <summary>
        /// Handles the Check event of the CheckBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void CheckBox_Check(object sender, RoutedEventArgs e)
        {
            this.SignInEnableCheck();
        }

        /// <summary>
        /// Handles the Unchecked event of the checkbox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Checkbox_Unchecked(object sender, RoutedEventArgs e)
        {
            signIn.IsEnabled = false;

        }

        /// <summary>
        /// Handles the Click event of the SignIn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private async void SignIn_Click(object sender, RoutedEventArgs e)
        {
#if WP_80_SILVERLIGHT
            bool isNetwork = App.CheckInternetConnectivity();
            if (isNetwork)
            {
                #region login
                var email = userName.Text;
                var isChecked = checkbox.IsChecked;
                ////Code for validation     
                if ((userName.Text != null || passwordBox.Password != null) && (isChecked != null && isChecked == true))
                {
                    signIn.IsEnabled = false;
                    this.passwordString = passwordBox.Password;
                    this.userNameString = userName.Text;

                    //// Set the MQTT for CCM server callback
                    this.halo.SetMQTTServer(this.item.mqtt, this.item.mqttPort);

                    if (this.useEnterpriseCredentials)
                    {
                        var ssoUsernName = tenantCode.Text + "/";
                        ssoUsernName += userName.Text;
                        this.halo.attemptLogin(ssoUsernName, this.passwordString, this.item.ccm);
                    }
                    else
                    {
                        this.halo.attemptLogin(this.userNameString, this.passwordString, this.item.ccm);
                    }
                }
                else
                {
                    ////    Pop up Message Box with Error message  
                    if (!(isChecked != null && isChecked == true))
                    {
                        ////Message of selecting the eula
                        var toast = new ToastNotification();
                        toast.CreateToastNotificationForVertical("Required", "Accept Terms and Condition to Login");
                    }
                }
                #endregion
            }
            else
            {
                //CustomMessageBox messageBox = new CustomMessageBox()
                //{
                //    Caption = "Connectivity Error",
                //    Message = "No Internet Connection.",
                //    LeftButtonContent = "OK",
                //};
                //messageBox.Show();

                CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
                await dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                {
                    MessageDialog msgDialog = new MessageDialog("No Internet Connection", "Connectivity Error");
                    msgDialog.Commands.Add(new UICommand("Ok"));
                    await msgDialog.ShowAsync();
                });
            }
#endif
        }

        /// <summary>
        /// Validates the email.
        /// </summary>
        /// <param name="targetEmail">The target email.</param>
        /// <returns>Boolean value</returns>
        private bool ValidateEmail(string targetEmail)
        {
            var emailRegExp = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                              @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
            return Regex.IsMatch(targetEmail, emailRegExp);
        }

        /// <summary>
        /// Checks if the SignIn button is enabled.
        /// </summary>
        private void SignInEnableCheck()
        {
            var isChecked = checkbox.IsChecked;
            var isUssoChecked = useSSO.IsChecked;
            bool isEmail = ValidateEmail(this.userName.Text);
            if (userName.Text.Length > 0 && passwordBox.Password.Length > 0 && (isChecked != null && isChecked == true))
            {
                if (isUssoChecked == true && tenantCode.Text.Length > 3)
                {
                    signIn.IsEnabled = true;
                }
                else if (isUssoChecked == false && isEmail)
                {
                    signIn.IsEnabled = true;
                }
                else
                {
                    signIn.IsEnabled = false;
                }
            }
            else
            {
                signIn.IsEnabled = false;
            }
        }

        /// <summary>
        /// Username Textbox key up event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void UsernameKeyUp(object sender, KeyRoutedEventArgs keyRoutedEventArgs)
        {
            this.SignInEnableCheck();
        }

        /// <summary>
        /// Password Textbox key up event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void PasswordKeyUp(object sender, KeyRoutedEventArgs keyRoutedEventArgs)
        {
            this.SignInEnableCheck();
        }

        /// <summary>
        /// Handles the Page event of the Go_Eula control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.GestureEventArgs"/> instance containing the event data.</param>
        private void Go_Eula_Page(object sender, TappedRoutedEventArgs tappedRoutedEventArgs)
        {
            //NavigationService.Navigate(new Uri("/Workspace;component/WorkspaceWebBrowserWP8/BrowserView.xaml?IsEulaView=true", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("BrowserView");
        }

        /// <summary>
        /// Handles the LongTap event of the DellIcon control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.GestureEventArgs"/> instance containing the event data.</param>
        private void DellIcon_LongTap(object sender, HoldingRoutedEventArgs holdingRoutedEventArgs)
        {
            ////List<LongListUserControl> userControlList = new List<LongListUserControl>();
            ////GetItemsRecursive<LongListUserControl>(CCmLongList, ref userControlList);
#if NOWAY
            foreach (LongListUserControl userControl in userControlList)
            {
                {
                    VisualStateManager.GoToState(userControl, "Normal", true);
                }
            }
            foreach (UserControl userControl in userControlList)
            {
                if (item.Equals(userControl.DataContext))
                {
                    VisualStateManager.GoToState(userControl, "Selected", true);
                }
            }
#else
            /*  foreach (UserControl userControl in userControlList)
            {
                if (item.Equals(userControl.DataContext))
                {
                    VisualStateManager.GoToState(userControl, "Selected", true);
                }
                else
                {
                    VisualStateManager.GoToState(userControl, "Normal", true);
                }
            }*/
#endif
            LayoutRoot.Opacity = 0.25;
            //ccmPopoUp.IsOpen = true;
        }

        /// <summary>
        /// CCMs the list select.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void CcmListSelect(object sender, SelectionChangedEventArgs selectionChangedEventArgs)
        {
            //this.item = CCmLongList.SelectedItem as CCMServerObject;
            //lastCCMServerItem = this.item;
            //SuspensionManager.SessionState["CurrentCCMServer"] = this.item.ccm;
            /* Code to High Light the Colour of Selected Item*/
            //// Get item of LongListSelector.
            /*  List<LongListUserControl> userControlList = new List<LongListUserControl>();
              GetItemsRecursive<LongListUserControl>(CCmLongList, ref userControlList);

              // Selected.
              if (e.AddedItems.Count > 0 && e.AddedItems[0] != null)
              {
                  foreach (LongListUserControl userControl in userControlList)
                  {
                      if (e.AddedItems[0].Equals(userControl.DataContext))
                      {
                          VisualStateManager.GoToState(userControl, "Selected", true);
                      }
                  }
              }
              if (e.RemovedItems.Count > 0 && e.RemovedItems[0] != null)
              {
                  foreach (UserControl userControl in userControlList)
                  {
                      if (e.RemovedItems[0].Equals(userControl.DataContext))
                      {
                          VisualStateManager.GoToState(userControl, "Normal", true);
                      }
                  }
              } */
            //// Selecting a server will automatically close this popup
            //ccmPopoUp.IsOpen = false;
            LayoutRoot.Opacity = 1;
#if WP_80_SILVERLIGHT
            var toast = new ToastNotification();
            
            toast.CreateToastNotificationForVertical("Select CCM Server", this.item.ccm);
#endif
        }

        /// <summary>
        /// Handles the Check event of the UseSSO control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void UseSSO_Check(object sender, RoutedEventArgs e)
        {
            var isChecked = useSSO.IsChecked;
            if (null != isChecked)
            {
                if (isChecked.Value)
                {
                    SSOSelection.Visibility = Visibility.Visible;
                    this.useEnterpriseCredentials = true;
                    LoginSelections.SetValue(Grid.RowProperty, 3);
                    if (checkbox.IsChecked == true && userName.Text.Length > 0 && passwordBox.Password.Length > 0 && tenantCode.Text.Length > 0)
                    {
                        signIn.IsEnabled = true;
                    }
                }
                else
                {
                    SSOSelection.Visibility = Visibility.Collapsed;
                    this.useEnterpriseCredentials = false;
                    LoginSelections.SetValue(Grid.RowProperty, 2);
                }
            }
        }

        /// <summary>
        /// Handles the Uncheck event of the UseSSO control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void UseSSO_Uncheck(object sender, RoutedEventArgs e)
        {
            SSOSelection.Visibility = Visibility.Collapsed;
            this.useEnterpriseCredentials = false;
            LoginSelections.SetValue(Grid.RowProperty, 2);
            if (checkbox.IsChecked == true && userName.Text.Length > 0 && passwordBox.Password.Length > 0 && ValidateEmail(userName.Text))
            {
                signIn.IsEnabled = true;
            }
            else
            {
                signIn.IsEnabled = false;
            }
        }

        /// <summary>
        /// Handles the GotFocus event of the userName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void UserName_GotFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            IconVisible.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Handles the LostFocus event of the userName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void UserName_LostFocus(object sender, RoutedEventArgs e)
        {
            IconVisible.Visibility = Visibility.Visible;
        }
    }
}