﻿#define PRODUCTION
using System;
using System.Linq;
using System.Windows;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace workspace.Helpers
{
    public static class WebBrowserHelper
    {
        static string gHTML = @"";
        public static readonly DependencyProperty HtmlProperty = DependencyProperty.RegisterAttached(
            "Html", typeof(string), typeof(WebBrowserHelper), new PropertyMetadata (string.Empty/*(OnHtmlChanged)*/));

        static string NotifyScript
        {
            get
            {
                return @"<script>
                    window.onload = function(){
                        a = document.getElementsByTagName('a');
                        for(var i=0; i < a.length; i++){                            
                            a[i].onclick = function() {notify(this.href);};
                        }
                    }
                    function notify(msg) {
	                    window.external.Notify(msg); 
	                    event.returnValue=false;
	                    return false;
                    }
                    </script>";
            }
        }

        static async void OpenBrowser(string url)
        {
#if TODO_FOR_WP81_SDK
            var frame = Window.Current.Content as Frame;
            if (frame != null)
                frame.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceWebBrowserWP8/BrowserView.xaml?msg=" + url, UriKind.Relative));
            else
            {
                await Windows.System.Launcher.LaunchUriAsync(new Uri(url, UriKind.Absolute));
            }
#endif
        }


        public static WebView browser;
        public static string GetHtml(DependencyObject dependencyObject)
        {
            return (string)dependencyObject.GetValue(HtmlProperty);
        }

        public static void SetHtml(DependencyObject dependencyObject, string value)
        {
            dependencyObject.SetValue(HtmlProperty, value);
        }

        private static void OnHtmlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            browser = d as WebView;
            browser.LoadCompleted += browser_LoadCompleted;
            //browser.IsScriptEnabled = true;

            if (browser == null)
                return;

            var html = e.NewValue.ToString();

            System.Text.StringBuilder fullHtml = new System.Text.StringBuilder(html)
                .Replace("</head>", string.Format("{0}{1}</head>", Environment.NewLine, NotifyScript));

#if !PRODUCTION
            Uri uri;
            uri = new Uri("WorkspaceEmailWP8/TestHTML.html", UriKind.Relative);
            StreamResourceInfo str_info = Application.GetResourceStream(uri);
            Stream stream = str_info.Stream;
            using (StreamReader reader = new StreamReader(stream))
            {
                browser.NavigateToString(reader.ReadToEnd());
            }
#else
            browser.NavigateToString(fullHtml.ToString());
#endif
            browser.ScriptNotify += browser_ScriptNotify;
        }

        static void browser_LoadCompleted(object sender, Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            string extraHtml = "if (navigator.userAgent.match(\"IEMobile/10.0\")) {var meta = document.createElement(\"meta\");meta.name=\"viewport\";meta.content=\"height=device-height,width=device-width,initial-scale=1.0,user-scalable=no\";document.getElementsByTagName(\"head\")[0].appendChild(meta);}";
            browser.InvokeScript("execScript", new string[] { extraHtml });
#if WP_80_SILVERLIGHT
            string htmlText = browser.SaveToString();
#endif
            // The 'cmd' will attempt to pull the correct height from the would-be rendered html and send that value to browser_ScriptNotify hanlder for adjustment
            //string cmd = "var d = document;function getDocumentHeight(){var mx = Math.max; return mx(mx(d.body.scrollHeight, d.documentElement.scrollHeight),mx(d.body.offsetHeight, d.documentElement.offsetHeight),mx(d.body.clientHeight, d.documentElement.clientHeight));}window.external.notify(getDocumentHeight().toString());";
            string cmd = "window.external.notify(\"height\"+document.body.firstChild.offsetHeight.toString());";
            try
            {
                browser.InvokeScript("execScript", new string[] { cmd });
            }
            catch (Exception ex)
            {
                string message = ex.Message;

            }
        }

        static void browser_Loaded(object sender, RoutedEventArgs e)
        {
#if !PRODUCTION
            Uri uri;
            uri = new Uri("WorkspaceEmailWP8/TestHTML.html", UriKind.Relative);
            StreamResourceInfo str_info = Application.GetResourceStream(uri);
            Stream stream = str_info.Stream;
            using (StreamReader reader = new StreamReader(stream))
            {
                browser.NavigateToString(reader.ReadToEnd());
            }
#endif
        }

        private static void browser_ScriptNotify(object sender, NotifyEventArgs e)
        {
#if TODO_FOR_WP81_SDK
            if (e.Value.StartsWith("height"))
            {
                browser.Height = (Convert.ToDouble(e.Value.Replace("height", String.Empty)) + 10) * 0.40;
            }
            else if (e.Value.Trim().Any() && e.Value.StartsWith("mailto:", StringComparison.CurrentCultureIgnoreCase))
            {
                var phoneApplicationFrame = Application.Current.RootVisual as Frame;
                if (phoneApplicationFrame != null)
                    phoneApplicationFrame.Navigate(new Uri(string.Format("/WorkspaceEmailWP8/View/CreateNewView.xaml?emailFromClickEventNote_param=" + e.Value), UriKind.Relative));
            }
            else if (!e.Value.Trim().Any() || e.Value.ToLower().Contains("about:blank"))
            {
                // if url contains about:blank or is empty
                browser.Dispatcher.BeginInvoke(
                    () =>
                    {
                        CustomMessageBox messageBox = new CustomMessageBox()
                        {
                            Caption = "Link Disabled",
                            Message = "This link has been disabled to protect\nyour security.",
                            LeftButtonContent = "OK",
                            IsRightButtonEnabled = false

                        };
                        messageBox.Dismissed += (s1, e1) =>
                        {
                            switch (e1.Result)
                            {
                                case CustomMessageBoxResult.LeftButton:
                                    break;
                                case CustomMessageBoxResult.None:
                                    break;
                                default:
                                    break;
                            }
                        };

                        messageBox.Show();
                    });
            }
            else
            {
                browser.Dispatcher.BeginInvoke(
                    () => OpenBrowser(e.Value)
                    );
            }
#endif
        }
    }
}