﻿using System;
using System.Windows;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace workspace.Helpers
{
    public class NavigationService : INavigationService
    {
        private Frame _mainFrame;
       
        public void GoBack()
        {
            if (EnsureMainFrame()
                && _mainFrame.CanGoBack)
            {
                _mainFrame.GoBack();
            }
        }

        public void NavigateTo(Type page)
        {
            if (EnsureMainFrame())
            {
                _mainFrame.Navigate(page);
            }
        }

        public void NavigateTo(Type sourcePageType, object parameter)
        {
            if (EnsureMainFrame())
            {
                _mainFrame.Navigate(sourcePageType, parameter);
            }
        }

        private bool EnsureMainFrame()
        {
            if (_mainFrame != null)
            {
                return true;
            }

            _mainFrame = Window.Current.Content as Frame;

            if (_mainFrame != null)
            {
                return true;
            }

            return false;
        }
    }
}