﻿#define USE_ASYNC
using ActiveSyncPCL;
using workspace.XMLParsing;
using Common;
using LoggingWP8;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using workspace_datastore.Managers;
using workspace_datastore.models;
using System;
using System.Windows;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace workspace.Helpers
{
    public class EASAccountSetupHelper
    {
        public enum ResultActions
        {
            NoAction,
            UseManualLoginScreen,
            UsePasswordOnlyLoginScreen
        };

        public delegate void errorCallback(ResultActions ra);

        errorCallback ErrorCallbackMethod;
        public string Domain { get; set; }

        Account mAccount = null;
        bool mUseSSO = false;
        int count = 0;

        public EASAccountSetupHelper(Account accountModel, errorCallback cb)
        {
            mAccount = accountModel;
            ErrorCallbackMethod = cb;
        }

        public async Task<int> authenticateUserSSO()
        {
            mUseSSO = true;
            return await authneticateUser();
        }

        /// <summary>
        /// Attempts to authenticate a user to EAS
        /// -1 is Generic failure
        /// -2 database encrypted error
        /// -3 some other SQL error
        /// -4 is Exchange credentials are incorrect.
        /// > 0 is EAS error (177 for example)
        /// </summary>
        /// <returns></returns>
        public async Task<int> authneticateUser()
        {
            int resultOfAuthentication = -1;
            bool authenticatedUser = false;
            try
            {
                // WP8Logger.LogMessage("ManualSignInViewModel->AuthneticateUser->Start");
                InitialSync sync = new InitialSync();
                ToastNotification toast = new ToastNotification();
                toast.CreateToastNotificationForVertical("Please wait", "Autodiscover and provision in progress..");

                ActiveSync activeSync = ActiveSync.GetInstance();
                EASAccount acct = await activeSync.discoverUser(mAccount.emailAddress, mAccount.serverUrl, mAccount.domainName, mAccount.authUser, mAccount.authPass);
                if (null != acct)
                {
                    //WP8Logger.LogMessage("Authenticated User!");
                    authenticatedUser = true;
                    ASOptionsResponse resp = await activeSync.getOptions(acct);
                    if (null != resp)
                    {
                        // Set the EAS protocol version AFTER we receive a successful options
                        mAccount.protocolVersion = activeSync.ProtocolVersion;

                        ASProvisionResponse provision_response = await activeSync.provision(acct);
                        if (null != provision_response)
                        {
                            if (provision_response.Status == (int)ASProvisionResponse.ProvisionStatus.Success)
                            {
                                acct.PolicyKey = provision_response.Policy.PolicyKey;
                                
                                mAccount.authUser = acct.AuthUser;
                                mAccount.authPass = acct.AuthPassword;
                                mAccount.policyKey = acct.PolicyKey.ToString();
                                mAccount.serverUrl = acct.ServerUrl; // Need to take the server url from the EAS sync once it's discovered.
                                //Add/update account info in database  
                                AccountManager objAcc = new AccountManager();
#if USE_ASYNC
                                objAcc.AddUpdateAccountInfoAsync(mAccount);
#else
                                objAcc.AddUpdateAccountInfo(mAccount);
#endif
                                var response = await activeSync.SyncFolderStructure();
                                // await SqlcipherManager.CerateDBTables();

                                sync.Parse(response);
                                //Setting App Status as Account setup done
                                App.mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP);

                                return 0;
                            }
                            else
                            {
                                resultOfAuthentication = provision_response.Status;
                            }
                        }
                    }
                    else
                    {
                        WP8Logger.LogMessage("Unable to retrieve Options");
                    }
                }
                else
                {
                    WP8Logger.LogMessage("Unable to authenticate user");
                }
            }
            catch (SQLite.SQLiteException sqe)
            {
                resultOfAuthentication = -3;

                if (sqe.Message.ToLower().Contains("is encrypted"))
                {
#if WP_80_SILVERLIGHT
                    // Database corruption
                    CustomMessageBox messageBox = new CustomMessageBox()
                    {
                        Caption = "Database Error",
                        Message = "Database has been corrupted.",
                        LeftButtonContent = "OK",

                    };
                    messageBox.Dismissed += (s1, e1) =>
                    {
                        switch (e1.Result)
                        {
                            case CustomMessageBoxResult.LeftButton:
                                break;
                            case CustomMessageBoxResult.RightButton:
                                break;
                            case CustomMessageBoxResult.None:
                                break;
                            default:
                                break;
                        }
                    };
                    messageBox.Show();
#endif
                    resultOfAuthentication = -2;
                }
            }
            catch (Exception ex)
            {
            }
            
            if(!authenticatedUser)
            {
                count += 1;

                if (count >= 2 || mUseSSO)
                {
#if WP_80_SILVERLIGHT
                    CustomMessageBox messageBox = new CustomMessageBox()
                    {
                        Caption = "Trouble Signing In",
                        Message = "Tap OK to sign in to Exchange manually. Contact your Exchange account administrator if sign in trouble persist.",
                        LeftButtonContent = "OK",

                    };
                    messageBox.Dismissed += (s1, e1) =>
                    {
                        switch (e1.Result)
                        {
                            case CustomMessageBoxResult.LeftButton:
                                if (null != ErrorCallbackMethod)
                                {
                                    ErrorCallbackMethod(ResultActions.UseManualLoginScreen);
                                }
                                break;
                            case CustomMessageBoxResult.RightButton:
                                // Do something.
                                break;
                            case CustomMessageBoxResult.None:
                                // Do something.
                                break;
                            default:
                                break;
                        }
                    };
                    messageBox.Show();
#endif
                }
                else
                {
                    resultOfAuthentication = -4;
#if WP_80_SILVERLIGHT
                    CustomMessageBox messageBox = new CustomMessageBox()
                    {
                        Caption = "Exchange Server",
                        Message = "Password is incorrect. Please try again.",
                        LeftButtonContent = "OK",

                    };
                    messageBox.Dismissed += (s1, e1) =>
                    {
                        switch (e1.Result)
                        {
                            case CustomMessageBoxResult.LeftButton:
                                if (null != ErrorCallbackMethod)
                                {
                                    ErrorCallbackMethod(ResultActions.UsePasswordOnlyLoginScreen);
                                }
                                // Do something.
                                break;
                            case CustomMessageBoxResult.RightButton:
                                // Do something.
                                break;
                            case CustomMessageBoxResult.None:
                                // Do something.
                                break;
                            default:
                                break;
                        }
                    };
                    messageBox.Show();
#endif
                }
            }
            return resultOfAuthentication;
        }
    }
}
