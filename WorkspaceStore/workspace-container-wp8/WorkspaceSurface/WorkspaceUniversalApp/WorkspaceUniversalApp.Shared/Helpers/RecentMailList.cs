﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace workspace.Helpers
{
    public class RecentMailList
    {
        static Queue<string> mRecentMailQueue = new System.Collections.Generic.Queue<string>(10);
        static readonly string RECENT_MAIL_LIST = "RecentMailList";

        static public void AddMailInList(string mail)
        {
            //PK: We are not concerned for order of same element, so if string is present in list,
            //we dont need to do any thing

            if (!mRecentMailQueue.Contains(mail))
            {
                if (mRecentMailQueue.Count == 10)
                    mRecentMailQueue.Dequeue();
                mRecentMailQueue.Enqueue(mail);
                SaveRecentList();
            }
            
        }

        static public Queue<string> GetRecentMailList()
        {
            ApplicationDataContainer localStorage = ApplicationData.Current.LocalSettings;
            object temp = null;
            localStorage.Values.TryGetValue(RECENT_MAIL_LIST, out temp);
            if (temp != null)
            {
                mRecentMailQueue = new Queue<string>(temp as Queue<string>);
            }

            return mRecentMailQueue;
            
        }

        static public void SaveRecentList()
        {
            ApplicationDataContainer localStorage = ApplicationData.Current.LocalSettings;
            List<string> temp = mRecentMailQueue.ToList();
            if (localStorage.Values.ContainsKey(RECENT_MAIL_LIST))
            {
                localStorage.Values.Remove(RECENT_MAIL_LIST);
            }
            localStorage.Values.Add(RECENT_MAIL_LIST, temp);
        }


    }
}
