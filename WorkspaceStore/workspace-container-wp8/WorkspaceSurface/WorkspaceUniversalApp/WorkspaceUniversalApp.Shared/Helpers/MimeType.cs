﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace workspace.Helpers
{
    public class MimeType
    {
       
        public static string GetMimeType(string fileName)
        {
            int lastIndexofDot= fileName.LastIndexOf(".");
            string extension = fileName.Substring(lastIndexofDot+1, fileName.Length - (lastIndexofDot+1));

            //added some common file content type
            if (extension == "txt")
                return "text/plain";
            else if (extension == "jpg" || extension == "jpe" || extension == "jpeg")
                return "image/jpeg";
            else if (extension == "png")
                return "image/png";
            else if (extension == "tif")
                return "image/tiff";
            else if(extension == "mp3")
                return "audio/mpeg3";//PK:we need to think about video mp3 files
            else if (extension == "zip")
                return "application/zip"; //PK: we need to see other mimi of zip well...testing required
            
                
            return null;
 
        }

        
        public static Uri GetImageUri(string fileName)
        {
             int lastIndexofDot= fileName.LastIndexOf(".");
            string extension = fileName.Substring(lastIndexofDot+1, fileName.Length - (lastIndexofDot+1));

            if (extension == "pdf")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_pdf.png", UriKind.Relative);
            else if (extension == "png")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_png.png", UriKind.Relative);
            else if(extension == "gif")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_gif.png", UriKind.Relative);
            else if(extension == "jpg" || extension == "jpeg")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_jpg.png", UriKind.Relative);
            else if(extension == "doc")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_doc.png", UriKind.Relative);
            else if(extension =="exl")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_exl.png", UriKind.Relative);
            else if(extension == "ppt")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_ppt.png", UriKind.Relative);
            else if (extension == "mpg")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_mpg.png", UriKind.Relative);
            else if (extension == "flv")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_flv.png", UriKind.Relative);
            else if (extension == "wma")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_ppt.wma", UriKind.Relative);
            else if (extension == "zip")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_zip.png", UriKind.Relative);
            else if (extension == "rar")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_rar.png", UriKind.Relative);
            else if (extension == "exe")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_ppt.exe", UriKind.Relative);
            else if (extension == "dmg")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_dmg.png", UriKind.Relative);
            else if (extension == "mov")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_ppt.mov", UriKind.Relative);
            else if (extension == "avi")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_avi.png", UriKind.Relative);
            else if (extension == "psd")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_psd.png", UriKind.Relative);
            else if (extension == "ai")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_ai.png", UriKind.Relative);
            else if (extension == "dll")
                return new Uri("/Assets/CM_Assets/Phone/ic_file_dll.png", UriKind.Relative);
            else
                return new Uri("/Assets/CM_Assets/Phone/ic_file_all.png", UriKind.Relative); ;
        }
    }

    
}
