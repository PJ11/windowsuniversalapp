﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace workspace.Helpers
{
    public class ViewManager
    {
        public static Uri uriMailViewOne = new Uri(string.Format("/EMail/View/MailView1.xaml"), UriKind.Relative);
        public static Uri uriMailViewTwo = new Uri(string.Format("/EMail/View/MailView2.xaml"), UriKind.Relative);
        public static Uri uriMailViewThree = new Uri(string.Format("/EMail/View/MailView3.xaml"), UriKind.Relative);
        public static Uri settingPage = new Uri(string.Format("/WorkspaceWP81;component/WorkspaceEmailWP8/View/MailSettings.xaml"), UriKind.Relative);


        public static Uri uriFileBrowserViewOne = new Uri(string.Format("/FileBrowser/View/FileBrowserView1.xaml"), UriKind.Relative);
        public static Uri uriFileBrowserViewTwo = new Uri(string.Format("/FileBrowser/View/FileBrowserView2.xaml"), UriKind.Relative);
        public static Uri uriFileBrowserViewThree = new Uri(string.Format("/FileBrowser/View/FileBrowserView3.xaml"), UriKind.Relative);

        public static Uri uriCalendarViewOne = new Uri(string.Format("/Calendar/View/CalendarView1.xaml"), UriKind.Relative);
        public static Uri uriCalendarViewTwo = new Uri(string.Format("/Calendar/View/CalendarView2.xaml"), UriKind.Relative);
        public static Uri uriCalendarViewThree = new Uri(string.Format("/Calendar/View/CalendarView3.xaml"), UriKind.Relative);
        public static Uri uriHomeView = new Uri(string.Format("/MainPage.xaml"), UriKind.Relative);

        public static List<Uri> _mailViewHistory = new List<Uri>();
        public static List<Uri> _filebrowserViewHistory = new List<Uri>();
        public static List<Uri> _calenderViewHistory = new List<Uri>();
    }
}
