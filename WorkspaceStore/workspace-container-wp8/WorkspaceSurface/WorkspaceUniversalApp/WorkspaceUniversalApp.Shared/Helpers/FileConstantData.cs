﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace workspace.Helpers
{
   public class FileConstantData
    {
       public static string RootPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "root");
       public static string CurrentPath = RootPath;
    }
}
