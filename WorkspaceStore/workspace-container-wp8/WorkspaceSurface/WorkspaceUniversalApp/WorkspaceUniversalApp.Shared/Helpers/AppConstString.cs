﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//We can add all const stringg being used in app here in one place
//It will avoid typo errors.
namespace workspace.Helpers
{
    public class AppConstString
    {
        //Mail Setting 
        public static string KEY_MAIL_NOTIFICATION = "KEY_MAIL_NOTIFICATION";
        public static string KEY_DEFAULT_SIGNATURE = "KEY_DEFAULT_SIGNATURE";
        public static string KEY_MAIL_SYNC_DAYS = "KEY_MAIL_SYNC_DAYS";
        public static string KEY_MAIL_FOLDER_SYNC = "MAIL_FOLDER_SYNC_SETTING";
        public static string DEFAULT_SIGNATURE_MAIL = "(sent via DellWorkspace)";
        public static string CALENDAR_SYNC_WINDOW = "KEY_CALENDAR_SYNC_WINDOW";
        public static string NUM_Of_WEEK_VIEW_BOX = "7";

        //AS Folder Name Defaults
        public static string AS_INBOX_NAME = "Inbox";
        public static string AS_SENT_ITEMS_NAME = "Sent Items";
        public static string AS_DRAFTS_NAME = "Drafts";
        public static string AS_CONTACTS_NAME = "Contacts";
        public static string AS_CALENDAR_NAME = "Calendar";
        public static string AS_DELETED_ITEMS_NAME = "Deleted Items";
        public static string AS_JUNK_EMAIL_NAME = "Junk E-Mail";

        //Contact Settings
        public static string KEY_CONTACT_SORTING = "KEY_CONTACT_SORTING";
        public static string KEY_CONTACT_VIEW_NAMES = "KEY_CONTACT_VIEW_NAMES";

    }
}
