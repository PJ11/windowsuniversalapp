/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:workspace.ViewModel"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using workspace.WorkspaceEmailWP8.ViewModel;
#if WP_80_SILVERLIGHT
using workspace.EmailLogin.ViewModel;
using workspace.Login.ViewModel;
using workspace.PIN_View;
using workspace.EmailLogin;
using workspace.WorkspaceContactsWP8.ViewModel;
using workspace.WorkspaceCalendarWP8.ViewModel;
#endif
using workspace.Helpers;


namespace workspace.Helpers
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
                SimpleIoc.Default.Register<IDataService, DesignDataService>();
            }
            else
            {
                SimpleIoc.Default.Register<IDataService, DataService>();
            }

            SimpleIoc.Default.Register<INavigationService, NavigationService>();
            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<CreateNewViewModel>();
#if WP_80_SILVERLIGHT
            SimpleIoc.Default.Register<ManualSignInViewModel>();
            SimpleIoc.Default.Register<EmailItemsViewModel>();
            SimpleIoc.Default.Register<EmailScreenViewModel>();
            SimpleIoc.Default.Register<PinViewModel>();
            SimpleIoc.Default.Register<EmailLoginViewModel>();
            SimpleIoc.Default.Register<MailFolderSync>();
            SimpleIoc.Default.Register<ContactsDetailsViewModel>();
            SimpleIoc.Default.Register<UnrecognizedContactViewModel>();
            SimpleIoc.Default.Register<AddContactOrEditViewModel>();
            SimpleIoc.Default.Register<GlobalContactDetailViewModel>();
            SimpleIoc.Default.Register<MailSearchViewModel>();
            SimpleIoc.Default.Register<DayViewViewModel>();
#endif
        }

        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }
        int Count = 0;

#if WP_80_SILVERLIGHT
        public MailFolderSync SyncEmailFolder
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MailFolderSync>();
            }
        }
        public EmailItemsViewModel ViewEmailItems
        {
            get
            {
                return ServiceLocator.Current.GetInstance<EmailItemsViewModel>();
            }
        }

        public MailSearchViewModel SearchEmailItems
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MailSearchViewModel>();
            }
        }

        public EmailScreenViewModel EmailScreen
        {
            get
            {
                return ServiceLocator.Current.GetInstance<EmailScreenViewModel>((++Count).ToString());
            }
        }

        public ContactsDetailsViewModel ContactDetails
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ContactsDetailsViewModel>();
            }
        }
        public GlobalContactDetailViewModel GlobalContactDetails
        {
            get
            {
                return ServiceLocator.Current.GetInstance<GlobalContactDetailViewModel>();
            }
        }

        public UnrecognizedContactViewModel UnrecognizedContactDetails
        {
            get
            {
                return ServiceLocator.Current.GetInstance<UnrecognizedContactViewModel>();
            }
        }
        public AddContactOrEditViewModel AddOrEditContact
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AddContactOrEditViewModel>();
            }
        }
#endif
        public CreateNewViewModel EmailCreate
        {
            get
            {
                return ServiceLocator.Current.GetInstance<CreateNewViewModel>((++Count).ToString());
            }
        }

#if WP_80_SILVERLIGHT
        public ManualSignInViewModel ManualSign
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ManualSignInViewModel>();
            }
        }

        public EmailLoginViewModel EmailSign
        {
            get
            {
                return ServiceLocator.Current.GetInstance<EmailLoginViewModel>();
            }
        }
        public PinViewModel PinScreenView
        {
            get
            {
                return ServiceLocator.Current.GetInstance<PinViewModel>();
            }
        }

        public DayViewViewModel DayViewViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<DayViewViewModel>();
            }
        }
#endif
        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
            SimpleIoc.Default.Reset();
        }
    }
}