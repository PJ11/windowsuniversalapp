﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
//using ActiveSyncPCL;
using Common;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.UI;
using Windows.UI.Core;
using System.Threading;
using Windows.Networking.Connectivity;
using FileHandlingPCL;
//using workspace.Helpers;
using Windows.UI.Popups;
using HaloWP8.Utils;
using ActiveSyncPCL;
//using workspace.CCMLoginScreen;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

using WorkspaceUniversalApp;

namespace WorkspaceUniversalApp
{
    public class NetworkChangedEventArgs : EventArgs
    {
        public bool IsOnline { get; protected set; }

        public NetworkChangedEventArgs(bool isOnline)
        {
            IsOnline = isOnline;
        }
    }

    public delegate void NetworkStatusChangeEvent(object sender, NetworkChangedEventArgs e);
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App : Application
    {
        public const string mAppIdDevelopment = "3c3ab0e3a0f2eb84df5ef969c231b361";
        public const string mAppIdQA = "89f99eac3041b4efb14f8a4311b30b8d";

        public static HaloService mHaloService = new HaloService();
        public static DKCNAppManager mManager = DKCNAppManager.Instance();
        public static DKAppState mAppState = DKAppState.getInstance();
        // LWR Once Common for WP81 is building::     public static KeystoneApp mKeystoneApp = KeystoneApp.getInstance();
        public static ActiveSyncManager mActiveSyncManager = null;
        public static NetworkService mNetworkService = null;

        private static bool _applicationActive = false;
        private static bool mNetworkActive = true;
        // LWR Once Common for WP81 is building::     NamedEvent agentStartedEvent;
        DispatcherTimer timer;
        const string BG_TASK_NAME = "RandomUpdateEmailCalendarTask";

        public static event NetworkStatusChangeEvent NetworkStatusChanged;
#if WINDOWS_PHONE_APP
        private TransitionCollection transitions;
#endif

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += this.OnSuspending;
        }

        public static bool CheckInternetConnectivity()
        {
#if WP_80_SILVERLIGHT
            var networkInterface = NetworkInterface.NetworkInterfaceType;
            bool isConnected = false;
            if ((networkInterface == NetworkInterfaceType.Wireless80211) || (networkInterface == NetworkInterfaceType.MobileBroadbandGsm) || (networkInterface == NetworkInterfaceType.MobileBroadbandCdma))
                isConnected = true;
            else if (networkInterface == NetworkInterfaceType.None)
                isConnected = false;
            return isConnected;
#else
            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                return false;
            }

            return NetworkInformation.GetConnectionProfiles().Any(p => IsInternetProfile(p));
#endif

        }

        private static bool IsInternetProfile(ConnectionProfile connectionProfile)
        {
            if (connectionProfile == null)
            {
                return false;
            }

            var connectivityLevel = connectionProfile.GetNetworkConnectivityLevel();
            return connectivityLevel != NetworkConnectivityLevel.None;
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif

            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                // TODO: change this value to a cache size that is appropriate for your application
                rootFrame.CacheSize = 1;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    // TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            Common.Navigation.NavigationService.Initialize(this.GetType(), rootFrame);

            if (rootFrame.Content == null)
            {
#if WINDOWS_PHONE_APP
                // Removes the turnstile navigation for startup.
                if (rootFrame.ContentTransitions != null)
                {
                    this.transitions = new TransitionCollection();
                    foreach (var c in rootFrame.ContentTransitions)
                    {
                        this.transitions.Add(c);
                    }
                }

                rootFrame.ContentTransitions = null;
                rootFrame.Navigated += this.RootFrame_FirstNavigated;
#endif

                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                PageObject pageObject = NavigationURIMapper.MapUri();

                if (!rootFrame.Navigate(pageObject.PageType, e.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }

            // Ensure the current window is active
            Window.Current.Activate();
        }

#if WINDOWS_PHONE_APP
        /// <summary>
        /// Restores the content transitions after the app has launched.
        /// </summary>
        /// <param name="sender">The object where the handler is attached.</param>
        /// <param name="e">Details about the navigation event.</param>
        private void RootFrame_FirstNavigated(object sender, NavigationEventArgs e)
        {
            var rootFrame = sender as Frame;
            rootFrame.ContentTransitions = this.transitions ?? new TransitionCollection() { new NavigationThemeTransition() };
            rootFrame.Navigated -= this.RootFrame_FirstNavigated;
        }
#endif

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();

            // TODO: Save application state and stop any background activity
            deferral.Complete();
        }
    }
}