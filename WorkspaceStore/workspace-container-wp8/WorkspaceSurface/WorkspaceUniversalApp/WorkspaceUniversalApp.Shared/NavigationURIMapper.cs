﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
//using workspace.Helpers;
//using Workspace.PIN_View;
using WorkspaceUniversalApp;
using WorkspaceUniversalApp.PIN_View;



namespace WorkspaceUniversalApp
{
    public class PageObject
    {
        public Type PageType { get; set; }
        public Object dataToSend { get; set; }
    }

#if WP_80_SILVERLIGHT
    public class NavigationURIMapper : UriMapperBase
#else
    public class NavigationURIMapper
#endif
    {
        private static string originalString = "//LaunchPage.xaml";
        private NavigationURIMapper()
        {
        }

#if WP_80_SILVERLIGHT
        public override Uri MapUri(Uri uri)
#else
        static public PageObject MapUri()
#endif
        {
#if WP_80_SILVERLIGHT
            //basically to check that it is application startup and get the page to load.
            if (uri.OriginalString == "//LaunchPage.xaml")
            {
                if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_UNINITIALIZED))
                {
                    if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED))
                    {
                        if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE))
                        {
                            if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE))
                            {
                                string url = string.Format("/WorkspaceWP81;component/ChangePin.xaml");
                                uri = new Uri(url, UriKind.Relative);
                            }
                            else
                            { 
                                string url = string.Format("/WorkspaceWP81;component/PIN_View/PINScreen.xaml?NumberOfPinDigits={0}&AppStartOrWakeup=true", -1);
                                uri = new Uri(url, UriKind.Relative);
                            }
                        }
                        else
                        {
                            // The user has already authenticated with CCM and for some reason exited App before inputing PIN.
                            string url = string.Format("/WorkspaceWP81;component/PIN_View/PINScreen.xaml?NumberOfPinDigits={0}&AppStartOrWakeup=false", -1);
                            uri = new Uri(url, UriKind.Relative);
                        }
                    }
                    else
                    {
                        uri = new Uri("/WorkspaceWP81;component/CCMLoginScreen/LoginScreen.xaml", UriKind.Relative);
                    }
                }
                else if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED))
                {
                    string url = string.Format("/WorkspaceWP81;component/PIN_View/PINScreen.xaml?NumberOfPinDigits={0}&AppStartOrWakeup=false", -1);
                    uri = new Uri(url, UriKind.Relative);
                }
            }
            return uri;
#else

            PageObject pageObject = new PageObject();
            if (originalString == "//LaunchPage.xaml")
            {
                if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_UNINITIALIZED))
                {
                    if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED))
                    {
                        if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE))
                        {
                            if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE))
                            {
                                //Need to uncomment below when changepin is enabled
                                //pageToOpen = typeof (ChangePin);
                            }
                            else
                            {
                                //SuspensionManager.SessionState.Add("NumberOfPinDigits", -1);
                                //SuspensionManager.SessionState.Add("AppStartOrWakeup", true);
                                pageObject.PageType = typeof(PINScreen);
                                pageObject.dataToSend = (int)0;
                            }
                        }
                        else
                        {
                            // The user has already authenticated with CCM and for some reason exited App before inputing PIN.
                            //SuspensionManager.SessionState.Add("NumberOfPinDigits", -1);
                            //SuspensionManager.SessionState.Add("AppStartOrWakeup", false);
                            pageObject.PageType = typeof(PINScreen);
                            pageObject.dataToSend = (int)0;
                        }
                    }
                    else
                    {
                        pageObject.PageType = typeof(MainPage);
                        pageObject.dataToSend = (int)0;                        
                    }
                }
                else if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED))
                {
                    //string url = string.Format("/WorkspaceWP81;component/PIN_View/PINScreen.xaml?NumberOfPinDigits={0}&AppStartOrWakeup=false", -1);
                    //uri = new Uri(url, UriKind.Relative);
                    //SuspensionManager.SessionState.Add("NumberOfPinDigits", -1);
                    //SuspensionManager.SessionState.Add("AppStartOrWakeup", false);
                    pageObject.PageType = typeof(PINScreen);
                    pageObject.dataToSend = (int)0;
                }
                //adding this as its not getting intial state
                else
                {
                    pageObject.PageType = typeof(MainPage);
                    pageObject.dataToSend = (int)0;                   
                }
            }

            return pageObject;

#endif

        }
    }
}