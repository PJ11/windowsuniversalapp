﻿// <copyright file="CCMServerObject.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini                           1.0                 First Version </summary>

namespace workspace.CCMLoginScreen
{
    /// <summary>
    /// CCM Server Class
    /// </summary>
  public class CCMServerObject
    {
        /// <summary>
        /// Gets or sets the CCM.
        /// </summary>
        /// <value>
        /// The CCM.
        /// </value>
       public string ccm { get; set; }

       /// <summary>
       /// Gets or sets the MQTT.
       /// </summary>
       /// <value>
       /// The MQTT.
       /// </value>
       public string mqtt { get; set; }

       /// <summary>
       /// Gets or sets the MQTT port.
       /// </summary>
       /// <value>
       /// The MQTT port.
       /// </value>
       public int mqttPort { get; set; }
    }
}
