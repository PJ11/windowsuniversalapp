﻿#define USE_ASYNC
using Windows.UI.Popups;
using Windows.UI.Xaml;
using workspace.Helpers;

// <copyright file="ManualSignInViewModel.xaml.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Dhruv</author>
// <email>Dhruvaraj_Jaiswal@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> dhruv                        1.0                 First Version </summary>

namespace workspace.Login.ViewModel
{
    #region NAMESPACE
    using System;
    using System.Linq;
    using System.Windows.Input;
    using ActiveSyncPCL;
    using XMLParsing;
    using Common;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using LoggingWP8;
    using workspace_datastore.Managers;
    using workspace_datastore.models;
    #endregion
    /// <summary>
    /// Manual SignIn page
    /// </summary>
    public class ManualSignInViewModel : ViewModelBase
    {

        #region LOCAL VARIABLE
        /// <summary>
        /// Create all local variable here
        /// </summary>
        private readonly IDataService dataService;

        /// <summary>
        /// The navigation service
        /// </summary>
        private readonly INavigationService navigationService;

        /// <summary>
        /// The manual options visibility property name
        /// </summary>
        public const string ManualOptionsVisibilityPropertyName = "ManualOptionsVisibility";

        /// <summary>
        /// The domain property name
        /// </summary>
        public const string DomainPropertyName = "Domain";

        /// <summary>
        /// The SSL property name
        /// </summary>
        public const string SslPropertyName = "UseSSL";

        /// <summary>
        /// The account property name
        /// </summary>
        public const string AccountPropertyName = "Account";

        /// <summary>
        /// The pass property name
        /// </summary>
        public const string PassPropertyName = "PassEnabled";

        /// <summary>
        /// The visibility
        /// </summary>
        private Visibility visibility = Visibility.Collapsed;

        /// <summary>
        /// The domain
        /// </summary>
        private string domain = string.Empty;

        /// <summary>
        /// The account
        /// </summary>
        private Account account = null;

        /// <summary>
        /// The use SSL
        /// </summary>
        private bool useSSL = true;

        /// <summary>
        /// The pass
        /// </summary>
        private bool pass = true;
        #endregion

        #region Create property

        /// <summary>
        /// Gets or sets the manual options visibility.
        /// </summary>
        /// <value>
        /// The manual options visibility.
        /// </value>
        public Visibility ManualOptionsVisibility
        {
            get
            {
                return visibility;
            }

            set
            {
                if (visibility == value)
                {
                    return;
                }

                visibility = value;
                RaisePropertyChanged(ManualOptionsVisibilityPropertyName);
            }
        }

        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        /// <value>
        /// The domain.
        /// </value>
        public string Domain
        {
            get
            {
                return domain;
            }

            set
            {
                if (domain == value)
                {
                    return;
                }

                domain = value;
                RaisePropertyChanged(DomainPropertyName);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [pass enabled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [pass enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool PassEnabled
        {
            get
            {
                return pass;
            }

            set
            {
                if (pass == value)
                {
                    return;
                }

                pass = value;
                RaisePropertyChanged(PassPropertyName);
            }
        }

        /// <summary>
        /// Gets or sets the account.
        /// </summary>
        /// <value>
        /// The account.
        /// </value>
        public Account Account
        {
            get
            {
                return account;
            }

            set
            {
                if (account == value)
                {
                    return;
                }

                account = value;
                RaisePropertyChanged(AccountPropertyName);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [use SSL].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [use SSL]; otherwise, <c>false</c>.
        /// </value>
        public bool UseSSL
        {
            get
            {
                return useSSL;
            }

            set
            {
                if (useSSL == value)
                {
                    return;
                }

                useSSL = value;
                RaisePropertyChanged(SslPropertyName);
            }
        }

        /// <summary>
        /// Gets the navigate to home page.
        /// </summary>
        /// <value>
        /// The navigate to home page.
        /// </value>
        public ICommand NavigateToHomePage { get; private set; }

        /// <summary>
        /// Gets the navigate back.
        /// </summary>
        /// <value>
        /// The navigate back.
        /// </value>
        public ICommand NavigateBack { get; private set; }

        /// <summary>
        /// Gets the login authenticate.
        /// </summary>
        /// <value>
        /// The login authenticate.
        /// </value>
        public ICommand LoginAuthenticate { get; private set; }
        #endregion



        /// <summary>
        /// Initializes a new instance of the <see cref="ManualSignInViewModel"/> class.
        /// </summary>
        /// <param name="dataServiceParam">The data service parameter.</param>
        /// <param name="navigationServiceParam">The navigation service parameter.</param>
        public ManualSignInViewModel(IDataService dataServiceParam, INavigationService navigationServiceParam)
        {
            PassEnabled = true;
            navigationService = navigationServiceParam;
            dataService = dataServiceParam;
            //// WP8Logger.LogMessage("ManualSignInViewModel Constructor");

            NavigateToHomePage = new RelayCommand(ShowHomeView);
            NavigateBack = new RelayCommand(GoBack);
            LoginAuthenticate = new RelayCommand(AuthneticateUser);
            Account = new Account();
            if (App.mManager.settingsUserOptions != null)
            {
                Domain = App.mManager.settingsUserOptions.EasDomain;
                Account.authUser = App.mManager.settingsUserOptions.EasUserName;
                Account.emailAddress = App.mManager.settingsUserOptions.EasEmailAddress;
                Account.serverUrl = App.mManager.settingsUserOptions.EasHost;
            }
            else
            {
                ManualOptionsVisibility = Visibility.Visible;
                Account.emailAddress = "LorenR@dmpdev1.com";
                Account.serverUrl = "mail.dmpdev1.com";
            }

            //Sybscribe the funcction GetAccountInfo
            // EASAccount.GetAccountInfo += new EASAccount.GetAccInfoHandler(GetAccountInfo);          
        }


        /// <summary>
        /// Authneticates the user.
        /// </summary>
        private async void AuthneticateUser()
        {
            bool isException = false;
            string excepCaption = string.Empty;
            string excepMessage = string.Empty;

            var authenticatedUser = false;
            try
            {
                // WP8Logger.LogMessage("ManualSignInViewModel->AuthneticateUser->Start");
                var sync = new InitialSync();
                var toast = new ToastNotification();
                toast.CreateToastNotificationForVertical("Please wait", "Autodiscover and provision in progress..");

                var activeSync = ActiveSync.GetInstance();
                var acct = await activeSync.discoverUser(Account.emailAddress, Account.serverUrl, Account.domainName, Account.authUser, Account.authPass);
                if (null != acct)
                {
                    //WP8Logger.LogMessage("Authenticated User!");
                    authenticatedUser = true;
                    var resp = await activeSync.getOptions(acct);
                    if (null != resp)
                    {
                        var provision_response = await activeSync.provision(acct);
                        if (null != provision_response)
                        {
                            acct.PolicyKey = provision_response.Policy.PolicyKey;
                            Account.policyKey = acct.PolicyKey.ToString();
                            //Add/update account info in database  
                            var objAcc = new AccountManager();
#if USE_ASYNC
                            objAcc.AddUpdateAccountInfoAsync(Account);
#else
                            objAcc.AddUpdateAccountInfo(Account);
#endif
                            var response = await activeSync.SyncFolderStructure();
                            // await SqlcipherManager.CerateDBTables();

                            sync.Parse(response);
                            //Setting App Status as Account setup done
                            App.mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP);
                            //navigationService.NavigateTo(new Uri(string.Format("/Workspace;component/UserWelcomeScreen.xaml"), UriKind.Relative));
                            Common.Navigation.NavigationService.NavigateToPage("UserWelcomeScreen");
                        }
                    }
                    else
                    {
                        WP8Logger.LogMessage("Unable to retrieve Options");
                    }
                }
                else
                {
                    WP8Logger.LogMessage("Unable to authenticate user");
                }
            }
            catch (SQLite.SQLiteException sqe)
            {
                if (sqe.Message.ToLower().Contains("is encrypted"))
                {
                    // Database corruption
#if WP_80_SILVERLIGHT
                    var messageBox = new CustomMessageBox()
                    {
                        Caption = "Database Error",
                        Message = "Database has been corrupted.",
                        LeftButtonContent = "OK",

                    };
                    messageBox.Dismissed += (s1, e1) =>
                    {
                        switch (e1.Result)
                        {
                            case CustomMessageBoxResult.LeftButton:
                                break;
                            case CustomMessageBoxResult.RightButton:
                                break;
                            case CustomMessageBoxResult.None:
                                break;
                            default:
                                break;
                        }
                    };
                    messageBox.Show();
#else
                    isException = true;
                    excepCaption = "Database Error";
                    excepMessage = "Database has been corrupted.";
#endif
                }
            }
            catch (Exception)
            {
            }

            if (isException)
            {
                MessageDialog messageDialog = new MessageDialog(excepMessage, excepCaption);
                messageDialog.Commands.Add(new UICommand("Ok"));
                await messageDialog.ShowAsync();
            }

            if (!authenticatedUser)
            {
#if WP_80_SILVERLIGHT
                var messageBox = new CustomMessageBox()
                {
                    Caption = "Exchange Server",
                    Message = "Password is incorrect.",
                    LeftButtonContent = "OK",

                };
                messageBox.Dismissed += (s1, e1) =>
                {
                    switch (e1.Result)
                    {
                        case CustomMessageBoxResult.LeftButton:
                            // Do something.
                            break;
                        case CustomMessageBoxResult.RightButton:
                            // Do something.
                            break;
                        case CustomMessageBoxResult.None:
                            // Do something.
                            break;
                        default:
                            break;
                    }
                };
                messageBox.Show();
#else
                MessageDialog messageDialog = new MessageDialog("Password is incorrect.", "Exchange Server");
                messageDialog.Commands.Add(new UICommand("Ok"));
                await messageDialog.ShowAsync();
#endif

                ManualOptionsVisibility = Visibility.Visible;
                PassEnabled = true;
                //throw exception
                //WP8Logger.LogMessage("ManualSignInViewModel->AuthneticateUser", ex.ToString());
                // throw ex;
            }
        }

        /// <summary>
        /// Get acccount details based on email id
        /// </summary>
        /// <param name="email">user email id</param>
        /// <returns>EASAccount</returns>
        private EASAccount GetAccountInfo(string email)
        {
            try
            {
                //Create object of account manager to get account details from database
                var objAcc = new AccountManager();
                //Create EASAccount object to set account value
                EASAccount objEAS = null;

                //call method to get account info
#if USE_ASYNC
                var list = objAcc.GetAccountInfoAsync(email).Result;
#else
                var list = objAcc.GetAccountInfo(email).ToList();
#endif
                if (list != null && list.Count > 0)
                {
                    objEAS = new EASAccount();
                    //get single result
                    var acc = list.Single();
                    if (acc != null)
                    {
                        //set EASAccount object
                        objEAS.DomainName = acc.domainName;
                        objEAS.AuthUser = acc.authUser;
                        objEAS.AuthPassword = acc.authPass;
                        objEAS.EmailAddress = acc.emailAddress;
                        objEAS.ServerUrl = acc.serverUrl;
                        objEAS.PolicyKey = Convert.ToUInt32(acc.policyKey);
                        objEAS.ProtocolVersion = acc.protocolVersion;
                    }
                }
                //return object
                return objEAS;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Goes the back.
        /// </summary>
        private void GoBack()
        {
            navigationService.GoBack();

        }

        /// <summary>
        /// Shows the home view.
        /// </summary>
        private void ShowHomeView()
        {
            Common.Navigation.NavigationService.NavigateToPage(ViewManager.uriHomeView.GetType());
        }

#if NOWAY
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Terminate();
        }
#endif

    }
}
