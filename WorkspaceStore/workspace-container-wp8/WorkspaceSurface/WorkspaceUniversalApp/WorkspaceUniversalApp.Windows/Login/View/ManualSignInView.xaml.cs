﻿// <copyright file="ManualSignInView.xaml.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Dhruv</author>
// <email>Dhruvaraj_Jaiswal@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary>Dhruv                          1.0                 First Version </summary

namespace workspace.Login.View
{
    #region NAMESPACE
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Navigation;
    #endregion
    /// <summary>
    /// Screen for Manual Login
    /// </summary>
    public partial class ManualSignInView : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManualSignInView"/> class.
        /// </summary>
        public ManualSignInView()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
#if WP_80_SILVERLIGHT
            NavigationService.RemoveBackEntry();
#else
            Common.Navigation.NavigationService.RemoveBackEntry();
#endif
        }
    }
}