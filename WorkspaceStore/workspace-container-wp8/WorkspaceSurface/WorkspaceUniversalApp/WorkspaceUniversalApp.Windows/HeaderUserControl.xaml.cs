﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using Windows.UI.Xaml.Controls;


namespace workspace
{
    public partial class HeaderUserControl : UserControl
    {
        public HeaderUserControl()
        {
            InitializeComponent();
            txtTime.Text = String.Format("{0:t}", DateTime.Now);// DateTime.Now.TimeOfDay.Hours.ToString();
        }
    }
}