﻿// <copyright file="StringToFileImageConverter.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Dhruvaraj Jaiswal</author>
// <email>dhruvaraj_jaiswal@dell.com</email>
// <date>14-07-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary>Dhruv           14-07-2014    1.0            First Version </summary>

namespace WorkspaceUniversalApp.Converters
{
    #region NAMESPACE
    using System;
    using Windows.UI.Xaml.Data;
    using CommonPCL;
    #endregion

    /// <summary>
    /// Class to convert Substring
    /// </summary>
    public class SubStringConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Modifies the source data before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is long)
            {
                var dt = DateTime.FromBinary((long)value);
                return dt.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);
            }
            else if (value is DateTime)
            {
                var dt = (DateTime)value;

                return dt.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);
            }

            return null;

            ////Potential Substring parameters
            int startIndex;
            int length;

            ////Input as String
            var val = (string)value;

            ////Attempt to split parameters by comma
            var parameters = ((string)parameter).Split(',');
            ////invalid or no parameters, return full string
            if (parameters == null || parameters.Length == 0)
            { return val; }

            ////return remaining string after startIndex
            if (parameters.Length == 1)
            {
                startIndex = int.Parse(parameters[0]);
                string temp3;

                var temp = val.Substring(startIndex);
                var temp1 = temp.Substring(0,2);
                var temp2 = temp.Substring(2);
                if (temp.Substring(6) == "PM" && Int32.Parse(temp.Substring(0,2)) > 12)
                {
                   temp3 = (Int32.Parse(temp1) - 12).ToString("D2");
              
                }
                else
                {
                    temp3 = temp1;
                }

                return (temp3 + temp2);
            }

            ////return length characters of string after startIndex
            if (parameters.Length >= 2)
            {
                startIndex = int.Parse(parameters[0]);
                length = int.Parse(parameters[1]);

                if (length >= 0)
                {  return val.Substring(startIndex, length); }

                ////negative length was provided
                return val.Substring(startIndex, val.Length + length);
            }

            return val;
        }

        /*
         * Not Implemented.
         */

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="langauge">Language</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, string langauge)
        {
            return null;
        }

        #endregion
    }
}