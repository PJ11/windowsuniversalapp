﻿// <copyright file="attendeeResponseToImageConverter.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Saurabh</author>
// <email>Saurabh_Chandel@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> saurabh                        1.0                 First Version </summary>

using System;
using Windows.UI.Xaml.Data;

namespace WorkspaceUniversalApp.Converters
{
    public class attendeeResponseToImageConverter : IValueConverter
    {
        /// <summary>
        /// Change image according to attendee response
        /// </summary>
        /// <param name="value">attendee status</param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>path to image file</returns>
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            int attendeeResponse = (int)value;
            string imagePath = "";
            switch (attendeeResponse)
            {
                case 0:
                    imagePath = "";
                    break;
                case 2:
                    imagePath = "/Assets/CM_Assets/Phone/ic_rsvp_tenative.png";
                    break;
                case 3:
                    imagePath = "/Assets/CM_Assets/Phone/ic_rsvp_accepted.png";
                    break;
                case 4:
                    imagePath = "/Assets/CM_Assets/Phone/ic_rsvp_decline.png";
                    break;
                case 5:
                    imagePath = "";
                    break;

                default:
                    break;
            }

            return imagePath;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            throw new NotImplementedException();
        }
    }
}