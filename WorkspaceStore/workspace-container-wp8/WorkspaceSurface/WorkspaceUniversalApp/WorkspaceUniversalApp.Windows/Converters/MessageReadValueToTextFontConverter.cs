﻿// <copyright file="MessageReadValueToTextFontConverter.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Dhruvaraj Jaiswal</author>
// <email>dhruvaraj_jaiswal@dell.com</email>
// <date>14-07-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary>Dhruv           14-07-2014    1.0            First Version </summary>

namespace WorkspaceUniversalApp.Converters
{
    #region NAMESPACE
    using System;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Data;
    using Windows.UI.Xaml.Media;
    #endregion

    /// <summary>
    /// Class to covert message read value to text font
    /// </summary>
    public class MessageReadValueToTextFontConverter : IValueConverter
    {
        /// <summary>
        /// Modifies the source data before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var read = (int)value;
            var retObject = Application.Current.Resources["MuseoSansForDell_700"] as FontFamily; // Application.Current.Resources["PhoneFontFamilyBold"];
            if (read == 1)
            {
                //retObject = Application.Current.Resources["PhoneFontFamilyNormal"];
            }
            return retObject as FontFamily;
        }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}