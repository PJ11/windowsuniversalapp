﻿// <copyright file="SpecialCharcterConverter.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Dhruvaraj Jaiswal</author>
// <email>dhruvaraj_jaiswal@dell.com</email>
// <date>14-07-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary>Dhruv           14-07-2014    1.0            First Version </summary>

namespace WorkspaceUniversalApp.Converters
{
    #region NAMESPACE
    using System;
    using System.Text;
    using Windows.UI.Xaml.Data;
    #endregion

    /// <summary>
    /// Class to convert Special character
    /// </summary>
    public class SpecialCharcterConverter : IValueConverter
    {
        /// <summary>
        /// return actual string
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, string language)
       {
           var result = string.Empty;
            if (value == null) { return result; }
            if (string.IsNullOrEmpty(value.ToString())) { return result; }
            var b = Encoding.GetEncoding("ISO-8859-1").GetBytes(value.ToString()); // b = `E2 80 A2`
            result = Encoding.UTF8.GetString(b, 0, b.Length);
            return result;
       }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
       {
           return null;
       }
    }
}