﻿// <copyright file="StringToFileImageConverter.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Dhruvaraj Jaiswal</author>
// <email>dhruvaraj_jaiswal@dell.com</email>
// <date>14-07-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary>Dhruv           14-07-2014    1.0            First Version </summary>

namespace WorkspaceUniversalApp.Converters
{
    #region NAMESPACE
    using System;
    using System.IO;
    using Windows.UI.Xaml.Data;
    using Windows.UI.Xaml.Media.Imaging;
    using workspace.Helpers;
    #endregion

    /// <summary>
    /// Class to convert String To Folder Image
    /// </summary>
    public class StringToFolderImageConverter : IValueConverter
    {
        /// <summary>
        /// Modifies the source data before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var val = value.ToString();

            switch (val.ToLower())
            {
                case "documents":
                {
                    var uri = new Uri("/Assets/CM_Assets/Phone/ic_documents.png", UriKind.Relative);
                    return new BitmapImage(uri);
                }

                case "downloads":
                {
                    var uri = new Uri("/Assets/CM_Assets/Phone/ic_downloads.png", UriKind.Relative);
                    return new BitmapImage(uri);
                }

                case "photos":
                {
                    var uri = new Uri("/Assets/CM_Assets/Phone/ic_photos.png", UriKind.Relative);
                    return new BitmapImage(uri);
                }

                default:
                {
                    var uri = new Uri("/Assets/CM_Assets/Phone/ic_folder.png", UriKind.Relative);
                    return new BitmapImage(uri);
                }

            }
            return null;
        }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Class to convert Directory name to opacity
    /// </summary>
   public class DirectoryNameToOpacityConverter : IValueConverter
   {
        /// <summary>
        /// Modifies the source data before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, string language)
       {
           if (null != value)
           {
               var val = value.ToString();
               var path = Path.GetDirectoryName(val);
               var itemName = Path.GetFileName(val);
               if (path != null && (string.Equals(path, FileConstantData.RootPath, StringComparison.CurrentCultureIgnoreCase) &&
                                    (itemName.ToLower() == "documents" ||
                                     itemName.ToLower() == "downloads" ||
                                     itemName.ToLower() == "photos"))) 
               {
                   return 0.4;
               }

               return 1.0;
           }

           return 1.0;
       }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
       {
           throw new NotImplementedException();
       }
   }
}