﻿// <copyright file="floatToDatePanaroma.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Saurabh</author>
// <email>Saurabh_Chandel@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> dhruv                        1.0                 First Version </summary>

using Windows.UI.Xaml.Data;

namespace WorkspaceUniversalApp.Converters
    {
        #region  NAMESPACE
        using System;
        using System.Globalization;
        using workspace_datastore.Helpers;

        #endregion
        /// <summary>
        /// Class to Convert Float to Date Time Converter.
        /// </summary>
        public class floatToDatePanaroma : IValueConverter
        {
            /// <summary>
            /// Determines whether the specified now is today.
            /// </summary>
            /// <param name="now">The now.</param>
            /// <param name="recvd">The recvd.</param>
            /// <returns>bool value</returns>
            private bool IsToday(DateTime now, DateTime recvd)
            {
                var isToday = false;
                if (recvd.Date==now.Date)
                {
                    isToday = true;
                }
                return isToday;
            }
            private bool IsYesterday(DateTime now, DateTime recvd)
            {
                bool isYesterday = false;
                if (recvd.Date.AddDays(1) == now.Date)
                {
                    isYesterday = true;
                }
                return isYesterday;
            }
            private bool IsWithin7Days(DateTime now, DateTime recvd)
            {
                var todayAtMidnight = now.Subtract(now.TimeOfDay);
                var diff = todayAtMidnight.Subtract(recvd);
                return diff.TotalMilliseconds > 0 && diff.TotalMilliseconds < 168.0 * 3600 * 1000;
            }
       
            public object Convert(object value, Type targetType, object parameter, string language)
            {
                long val = System.Convert.ToInt32(value);
                var dt =   DateTimeExtensions.FromOADate(val);

                if (IsToday(DateTime.Now, dt))
                {
                    return dt.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);
                }
                else if (IsYesterday(DateTime.Now, dt))
                {
                    return "Yesterday";
                }
                else if (IsWithin7Days(DateTime.Now, dt))
                {
                    return dt.DayOfWeek.ToString();
                }
                else 
                {
                    return dt.ToString("M/d/yy", CultureInfo.InvariantCulture);
                }             
            }

            /// <summary>
            /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
            /// </summary>
            /// <param name="value">The target data being passed to the source.</param>
            /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
            /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
            /// <param name="culture">The culture of the conversion.</param>
            /// <param name="language"></param>
            /// <returns>
            /// The value to be passed to the source object.
            /// </returns>
            public object ConvertBack(object value, Type targetType,
                object parameter, string language)
            {
                return null;
            }
        }
    }