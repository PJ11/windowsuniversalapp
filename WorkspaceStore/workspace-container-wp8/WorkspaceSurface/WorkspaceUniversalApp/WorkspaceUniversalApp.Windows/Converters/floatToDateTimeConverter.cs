﻿// <copyright file="floatToDateTimeConverter.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Dhruv</author>
// <email>Dhruvaraj_Jaiswal@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> dhruv                        1.0                 First Version </summary>

using Windows.UI.Xaml.Data;

namespace WorkspaceUniversalApp.Converters
{
    #region  NAMESPACE
    using System;
    using System.Globalization;
    #endregion
    /// <summary>
    /// Class to Convert Float to Date Time Converter.
    /// </summary>
    public class FloatToDateTimeConverter : IValueConverter
    {
        /// <summary>
        /// Determines whether the specified now is today.
        /// </summary>
        /// <param name="now">The now.</param>
        /// <param name="recvd">The recvd.</param>
        /// <returns>bool value</returns>
        private bool IsToday(DateTime now, DateTime recvd)
        {
            var isToday = false;
            if(now.Year == recvd.Year && now.Month == recvd.Month && now.Day == recvd.Day)
            {
                isToday = true;
            }
            return isToday;
        }

        private bool IsYesterday(DateTime now, DateTime recvd)
        {
            bool isYesterday = false;
            TimeSpan diff = now.Subtract(recvd);
            if (diff.TotalHours >= 24.0 && diff.TotalHours < 48.0)
            {
                isYesterday = true;
            }

            return isYesterday;
        }

        private bool IsWithin7Days(DateTime now, DateTime recvd)
        {
            var todayAtMidnight = now.Subtract(now.TimeOfDay);
            var diff = todayAtMidnight.Subtract(recvd);
            return diff.TotalMilliseconds > 0 && diff.TotalMilliseconds < 168.0 * 3600 * 1000;
        }

        /// <summary>
        /// Determines whether [is within current year] [the specified now].
        /// </summary>
        /// <param name="now">The now.</param>
        /// <param name="recvd">The recvd.</param>
        /// <returns>bool value</returns>
        private bool IsWithinCurrentYear(DateTime now, DateTime recvd)
        {
            return now.Year == recvd.Year;
        }

        /// <summary>
        /// Gets the name of the month.
        /// </summary>
        /// <param name="month">The month.</param>
        /// <returns>bool value</returns>
        private string GetMonthName(int month)
        {
            var mnth = string.Empty;
            switch (month)
            {
                case 1:
                    mnth = "Jan";
                    break;
                case 2:
                    mnth = "Feb";
                    break;
                case 3:
                    mnth = "Mar";
                    break;
                case 4:
                    mnth = "Apr";
                    break;
                case 5:
                    mnth = "May";
                    break;
                case 6:
                    mnth = "Jun";
                    break;
                case 7:
                    mnth = "Jul";
                    break;
                case 8:
                    mnth = "Aug";
                    break;
                case 9:
                    mnth = "Sep";
                    break;
                case 10:
                    mnth = "Oct";
                    break;
                case 11:
                    mnth = "Nov";
                    break;
                case 12:
                    mnth = "Dec";
                    break;
            }
            return mnth;

        }

        string GetShortDay(string longday)
        {
            var shortDay = "";
            switch (longday.ToLower())
            {
                case "sunday":
                    shortDay = "Sun ";
                    break;
                case "monday":
                    shortDay = "Mon ";
                    break;
                case "tuesday":
                    shortDay = "Tue ";
                    break;
                case "wednesday":
                    shortDay = "Wed ";
                    break;
                case "thursday":
                    shortDay = "Thu ";
                    break;
                case "friday":
                    shortDay = "Fri ";
                    break;
                case "saturday":
                    shortDay = "Sat ";
                    break;
            }

            return shortDay;
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            long val = System.Convert.ToInt32(value);
            var dt = DateTime.FromBinary(val);

            // TODO Verify that dt is in UTC time

            /* The format is this: Assume today is July 30, 2014
             * If the email was received Today, it's just the time in hh:mm
             * If email was received 1 day ago, it's Yesterday
             * If email was received 2 - 7 days ago, it's the day name (e.g. Monday)
             * If email received > 7 days ago and within the current year it's Month 3 char name and then the numeric day (e.g. Jul 23)
             * If email received before 1/1/Current Year, it's 9/13/2012
             */
            if(IsToday(DateTime.Now, dt))
            {
                return dt.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);
            }
            else if (IsWithin7Days(DateTime.Now, dt))
            {
                return GetShortDay(dt.DayOfWeek.ToString()) + dt.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);
            }
            else if (IsWithinCurrentYear(DateTime.Now, dt))
            {
                return GetMonthName(dt.Month) + " " + dt.Day;
            }
            else
            {
                return dt.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern);
            }
        }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}
