﻿// <copyright file="eventToVisibilityConvertor.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Saurabh</author>
// <email>Saurabh_Chandel@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> saurabh                        1.0                 First Version </summary>

using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace WorkspaceUniversalApp.Converters
{
    /// <summary>
    /// Converts visibilty of "ALL DAY" or normal time view in dayview events
    /// </summary>
   public class eventToVisibilityConvertor : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            int allDay = (int)value;
            string sentParameter = parameter as string;
            if (1 == allDay)
            {
                if (sentParameter == "stack")
                {
                    return Visibility.Collapsed;
                }
                else
                {
                    return Visibility.Visible;
                }
            }
            else
            {
                if (sentParameter == "stack")
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }

            return Visibility.Collapsed;
        }

     
        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            throw new NotImplementedException();
        }
    }
}