﻿// <copyright file="StringToDate.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Dhruvaraj Jaiswal</author>
// <email>dhruvaraj_jaiswal@dell.com</email>
// <date>14-07-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary>Dhruv           14-07-2014    1.0            First Version </summary>

namespace WorkspaceUniversalApp.Converters
{
    #region NAMESPACE
    using System;
    using Windows.UI.Xaml.Data;
    using CommonPCL;
    #endregion

    /// <summary>
    /// Convert date to today tommorrow or day of week in calendar pivot
    /// </summary>
    public class StringToDate : IValueConverter
    {
        /// <summary>
        /// Determines whether the specified now is today.
        /// </summary>
        /// <param name="now">The now.</param>
        /// <param name="recvd">The recvd.</param>
        /// <returns></returns>
        private bool IsToday(DateTime now, DateTime recvd)
        {
            ////  TimeSpan diff = now.Subtract(recvd);
            return (now.Date == recvd.Date);
        }

        /// <summary>
        /// Determines whether the specified now is yesterday.
        /// </summary>
        /// <param name="now">The now.</param>
        /// <param name="recvd">The recvd.</param>
        /// <returns></returns>
        private bool IsYesterday(DateTime now, DateTime recvd)
        {
            return now.Date.AddDays(-1) == recvd.Date;
        }

        /// <summary>
        /// Determines whether the specified now is tomorrow.
        /// </summary>
        /// <param name="now">The now.</param>
        /// <param name="recvd">The recvd.</param>
        /// <returns></returns>
        private bool IsTomorrow(DateTime now, DateTime recvd)
        {
            return now.Date.AddDays(1) == recvd.Date;
        }

        /// <summary>
        /// Determines whether [is within7 days] [the specified now].
        /// </summary>
        /// <param name="now">The now.</param>
        /// <param name="recvd">The recvd.</param>
        /// <returns></returns>
        private bool IsWithin7Days(DateTime now, DateTime recvd)
        {
            return (recvd.Date >= now.Date.AddDays(-3)) && (recvd.Date <= now.Date.AddDays(3));
        }

        /// <summary>
        /// Determines whether [is within current year] [the specified now].
        /// </summary>
        /// <param name="now">The now.</param>
        /// <param name="recvd">The recvd.</param>
        /// <returns></returns>
        private bool IsWithinCurrentYear(DateTime now, DateTime recvd)
        {
            return now.Year == recvd.Year;
        }

        /// <summary>
        /// Gets the name of the month.
        /// </summary>
        /// <param name="month">The month.</param>
        /// <returns></returns>
        private string GetMonthName(int month)
        {
            var mnth = string.Empty;
            switch (month)
            {
                case 1:
                    mnth = "Jan";
                    break;
                case 2:
                    mnth = "Feb";
                    break;
                case 3:
                    mnth = "Mar";
                    break;
                case 4:
                    mnth = "Apr";
                    break;
                case 5:
                    mnth = "May";
                    break;
                case 6:
                    mnth = "Jun";
                    break;
                case 7:
                    mnth = "Jul";
                    break;
                case 8:
                    mnth = "Aug";
                    break;
                case 9:
                    mnth = "Sep";
                    break;
                case 10:
                    mnth = "Oct";
                    break;
                case 11:
                    mnth = "Nov";
                    break;
                case 12:
                    mnth = "Dec";
                    break;
            }

            return mnth; 
        }

        /// <summary>
        /// Modifies the source data before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var valDate = (DateTime)value;
            var dt = valDate.Date; // DateTime.ParseExact(value.ToString().Substring(0, 10), "MM dd yyyy", CultureInfo.InvariantCulture);

            // TODO Verify that dt is in UTC time

            /* The format is this: Assume today is July 30, 2014
             * If the email was received Today, it's just the time in hh:mm
             * If email was received 1 day ago, it's Yesterday
             * If email was received 2 - 7 days ago, it's the day name (e.g. Monday)
             * If email received > 7 days ago and within the current year it's Month 3 char name and then the numeric day (e.g. Jul 23)
             * If email received before 1/1/Current Year, it's 9/13/2012
             */
            if (IsToday(DateTime.UtcNow, dt))
            {
                return "Today";
            }
            else if (IsYesterday(DateTime.UtcNow, dt))
            {
                return "Yesterday";
            }
            else if (IsTomorrow(DateTime.UtcNow, dt))
            {
                return "Tomorrow";
            }
            else if (IsWithin7Days(DateTime.UtcNow, dt))
            {
                return dt.DayOfWeek.ToString();
            }
            else if (IsWithinCurrentYear(DateTime.UtcNow, dt))
            {
                return GetMonthName(dt.Month) + " " + dt.Day;
            }
            else
            {
                return dt.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);
            }

            return dt.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern) + " " + dt.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);
        }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}