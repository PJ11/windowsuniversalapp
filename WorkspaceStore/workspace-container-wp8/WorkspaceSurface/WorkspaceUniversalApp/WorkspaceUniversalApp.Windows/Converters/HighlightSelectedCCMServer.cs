﻿// <copyright file="HighlightSelectedCCMServer.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Dhruv</author>
// <email>Dhruvaraj_Jaiswal@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> dhruv                        1.0                 First Version </summary>

using Windows.UI.Xaml.Data;
using workspace.Helpers;

namespace WorkspaceUniversalApp.Converters
{
    #region NAMESPACE
    using System;
    #endregion
    /// <summary>
    /// Class to High Light The Selected CCM Server 
    /// </summary>
    public class HighlightSelectedCcmServer : IValueConverter
    {
        /// <summary>
        /// Modifies the source data before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language.</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            string val = value.ToString();
            string color = "White";
            string ccm = string.Empty;
            if (SuspensionManager.SessionState.ContainsKey("CurrentCCMServer"))
            {
                ccm = (string)SuspensionManager.SessionState["CurrentCCMServer"];
                SuspensionManager.SessionState.Remove("CurrentCCMServer");
            }

            if (val == ccm)
            {
                color = "#0085C3";
            }

            return color;          
        }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}