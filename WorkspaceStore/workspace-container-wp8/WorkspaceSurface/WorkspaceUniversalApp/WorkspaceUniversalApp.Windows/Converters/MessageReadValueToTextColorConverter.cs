﻿// <copyright file="MessageReadValueToTextColorConverter.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Dhruvaraj Jaiswal</author>
// <email>dhruvaraj_jaiswal@dell.com</email>
// <date>14-07-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary>Dhruv           14-07-2014    1.0            First Version </summary>

namespace WorkspaceUniversalApp.Converters
{
    #region NAMESPACE
    using System;
    using Windows.UI.Text;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Data;
    using Windows.UI.Xaml.Media;
    #endregion

    /// <summary>
    /// Class to convert FROM and SUBJECT font weight
    /// </summary>
    public class MessageReadValueToFontWeightConverter : IValueConverter
    {
        /// <summary>
        /// Modifies the source data before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var read = (int)value;
            if (read == 1)
            {
                return FontWeights.Normal;
            }

            return FontWeights.Bold;
        }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }

    /// <summary>
    /// Converter class to change the color of the SUBJECT TEXT to RED if it's a high priority or 
    ///    gray for read and blue for unread.
    /// </summary>
    public class MessageSubjectTextColorConverter : IValueConverter
    {
        /// <summary>
        /// Modifies the brush color before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            int read = ((int)value >> 16) & 0xFFFF;
            int importance = (int)value & 0x0000FFFF;
            SolidColorBrush brush = Application.Current.Resources["DELL_WORKSPACE_DK_GRAY"] as SolidColorBrush;
            if(importance == 2)
            {
                brush = Application.Current.Resources["DELL_WORKSPACE_RED"] as SolidColorBrush;
            }
            else if(read == 0)
            {
                brush = Application.Current.Resources["DELL_WORKSPACE_BLUE"] as SolidColorBrush;
            }

            return brush;
        }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }


    /// <summary>
    /// Converter class to change the color of the SUBJECT FROM (OR TO IF DRAFT) to RED if it's a high priority or 
    ///    dark gray for all else.
    /// </summary>
    public class MessageFromTextColorConverter : IValueConverter
    {
        /// <summary>
        /// Modifies the brush color before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            int read = ((int)value >> 16) & 0xFFFF;
            int importance = (int)value & 0x0000FFFF;
            SolidColorBrush brush = Application.Current.Resources["DELL_WORKSPACE_DK_GRAY"] as SolidColorBrush;
            if (importance == 2)
            {
                brush = Application.Current.Resources["DELL_WORKSPACE_RED"] as SolidColorBrush;
            }

            return brush;
        }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}