﻿// <copyright file="cancelTovisibilityConverter.cs" company="Dell Inc">
// Copyright (c) 2015 All Right Reserved
// </copyright>
// <author>Saurabh</author>
// <email>Saurabh_Chandel@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> saurabh                        1.0                 First Version </summary>

using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace WorkspaceUniversalApp.Converters
{
    public class cancelTovisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Change image according to attendee response
        /// </summary>
        /// <param name="value">attendee status</param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>path to image file</returns>
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            int attendeeResponse = (int)value;
            if (attendeeResponse == 7)
                return Visibility.Collapsed;
            else
            {
                return Visibility.Visible;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            throw new NotImplementedException();
        }
    }
}