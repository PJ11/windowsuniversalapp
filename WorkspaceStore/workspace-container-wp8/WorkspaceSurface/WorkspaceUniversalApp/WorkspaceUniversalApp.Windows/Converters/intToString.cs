﻿// <copyright file="IntToString.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Dhruv</author>
// <email>Dhruvaraj_Jaiswal@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> dhruv                        1.0                 First Version </summary>

namespace WorkspaceUniversalApp.Converters
{
    #region NAMESPACE
    using System;
    using System.Globalization;
    using Windows.UI.Xaml.Data;
    #endregion
    /// <summary>
    /// Class to convert Int to String
    /// </summary>
    public class IntToString : IValueConverter
    {
        /// <summary>
        /// Modifies the source data before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {

            var enumValue = System.Convert.ToInt32(value);
            var returnValue = "";
            if (value.GetType() == typeof(CommonPCL.DKCNMailDays))
            {
                returnValue = ConvertSyncDaysToString(enumValue);
            }
            else if (value.GetType() == typeof(CommonPCL.DKCNMailNotificationType))
            {
                returnValue = ConvertMailNotiToString(enumValue);
            }
            else if (value.GetType() == typeof(CommonPCL.DKCNCalendarDays))
            {
                returnValue = ConvertCalendarSyncDaysToString(enumValue);
            }
            return returnValue;
        }

        /// <summary>
        /// Converts the mail noti to string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private string ConvertMailNotiToString(int value)
        {
            String mailNotification;
            switch (value)
            {
                //case 0:
                //    mailNotification = "MailNotSet";
                //    break;
                case 0:
                    mailNotification = "High Priority Emails";
                    break;
                case 1:
                    mailNotification = "All New Emails";
                    break;
                case 2:
                    mailNotification = "None";
                    break;
                default:
                    mailNotification = "None";
                    break;
            }
            return mailNotification;
        }

        /// <summary>
        /// Converts the synchronize days to string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private string ConvertSyncDaysToString(int value)
        {
            String syncTime;
            switch (value)
            {
                case 0:
                    syncTime = "Sync All";
                    break;
                case 1:
                    syncTime = "Sync One Day";
                    break;
                case 2:
                    syncTime = "Sync Three Days";
                    break;
                case 3:
                    syncTime = "Sync One Week";
                    break;
                case 4:
                    syncTime = "Sync Two Weeks";
                    break;
                case 5:
                    syncTime = "Sync One Month";
                    break;
                //it doesn't work for email
                //case 6:
                //    SyncTime = "Three Months";
                //    break;
                //case 7:
                //    SyncTime = "Six Months";
                //    break;
                default:
                    syncTime = "Sync All";
                    break;
            }
            return syncTime;
        }
        /// <summary>
        /// Converts the calendar synchronize days to string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private string ConvertCalendarSyncDaysToString(int value)
        {
            String syncTime;
            switch (value)
            {
                case 0:
                    syncTime = "All Events";
                    break;
                //As Doc, it doesn't work for calednar
                //case 1:
                //    SyncTime = "1 Day Back";
                //    break;
                //case 2:
                //    SyncTime = "3 Days Back";
                //    break;
                //case 3:
                //    SyncTime = "1 Week Back";
                //    break;
                case 1:
                    syncTime = "2 Weeks Back";
                    break;
                case 2:
                    syncTime = "1 Month Back";
                    break;
                case 3:
                    syncTime = "3 Months Back";
                    break;
                case 4:
                    syncTime = "6 Months Back";
                    break;
                default:
                    syncTime = "All Events";
                    break;
            }
            return syncTime;
        }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }
    }
}