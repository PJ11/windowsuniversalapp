﻿// <copyright file="statusToFillRectangleConvertor.cs" company="Dell Inc">
// Copyright (c) 2015 All Right Reserved
// </copyright>
// <author>Saurabh</author>
// <email>Saurabh_Chandel@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Saurabh                        1.0                 First Version </summary>

using System;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace WorkspaceUniversalApp.Converters
{
    public class statusToFillRectangleConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            int myResponse = 0;
            Int32.TryParse(value.ToString(), out myResponse);

            if (myResponse == 2)
            {
                LinearGradientBrush linear = new LinearGradientBrush();
                //linear.StartPoint = new Point(0, 0);
                //linear.EndPoint = new Point(.01, 0.01);
                linear.Transform = new RotateTransform { Angle = -45 };
                linear.SpreadMethod = GradientSpreadMethod.Repeat;
                linear.GradientStops.Add(new GradientStop() { Color = Color.FromArgb(0xff, 0, 133, 195), Offset = 0.4 });
                linear.GradientStops.Add(new GradientStop() { Color = Colors.Transparent, Offset = 0.7 });
                //linear.MappingMode = BrushMappingMode.;
                linear.RelativeTransform = new ScaleTransform { ScaleX = 0.1, ScaleY = 0.1 };
                return linear;
            }
            else
            {
                SolidColorBrush linear = new SolidColorBrush();
                linear.Color = Color.FromArgb(0xff, 0, 133, 195);
                return linear;
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            throw new NotImplementedException();
        }
    }
}