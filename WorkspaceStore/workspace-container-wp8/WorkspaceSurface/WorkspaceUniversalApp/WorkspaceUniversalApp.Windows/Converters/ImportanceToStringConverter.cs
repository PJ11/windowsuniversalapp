﻿using System;
using Windows.UI.Xaml.Data;

namespace WorkspaceUniversalApp.Converters
{
	public class ImportanceToStringConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			string importanceString = "Low";
			int impValue = System.Convert.ToInt32(value);
			if (impValue == 2)
				importanceString = "High";
			else if (impValue == 1)
				importanceString = "Normal";
			return importanceString;
		}

        public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			return null;
		}
	}
}