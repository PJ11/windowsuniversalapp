﻿// <copyright file="BytesToImageConverter.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Dhruv</author>
// <email>Dhruvaraj_Jaiswal@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> dhruv                        1.0                 First Version </summary>

using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;

namespace workspace.Converters
{
    #region NAMESPACE
    using System;
    using System.IO;
    using Microsoft.Phone;
    using Windows.Graphics.Imaging;
    #endregion
    /// <summary>
    /// Class to Convert Byte to Image
    /// </summary>
    public class BytesToImageConverter : IValueConverter
    {
        /// <summary>
        /// Modifies the source data before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language"></param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null || !(value is byte[])) return null;
            var bytes = value as byte[];
            var stream = new MemoryStream(bytes);
            var image = new BitmapImage
            {
                DecodePixelType = DecodePixelType.Logical,
                CreateOptions = BitmapCreateOptions.None
            };
            var bitmapImage = BitmapDecoder.DecodeJpeg(stream, 480, 856);
            if (bitmapImage.PixelHeight > bitmapImage.PixelWidth)
            {
                image.DecodePixelWidth = 56;
                image.DecodePixelHeight = 100;
            }
            else
            {
                image.DecodePixelWidth = 100;
                image.DecodePixelHeight = 56;
            }

            image.SetSource(stream);
            return image;
        }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language"></param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
