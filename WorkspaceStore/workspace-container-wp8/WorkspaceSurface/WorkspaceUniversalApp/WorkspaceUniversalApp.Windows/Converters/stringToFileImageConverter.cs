﻿// <copyright file="StringToFileImageConverter.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Dhruvaraj Jaiswal</author>
// <email>dhruvaraj_jaiswal@dell.com</email>
// <date>14-07-2014</date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary>Dhruv           14-07-2014    1.0            First Version </summary>

namespace WorkspaceUniversalApp.Converters
{
    #region NAMESPACE
    using System;
    using Windows.UI.Xaml.Data;
    using Windows.UI.Xaml.Media.Imaging;
    #endregion

    /// <summary>
    /// Class to convert String to File Image
    /// </summary>
    public class StringToFileImageConverter : IValueConverter
    {
        /// <summary>
        /// Modifies the source data before passing it to the target for display in the UI.
        /// </summary>
        /// <param name="value">The source data being passed to the target.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the target dependency property.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the target dependency property.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var val = value.ToString();
            var name = val.Split('.');
            if (name.Length > 1)
            {
                var fileExt = name[1];

               switch (fileExt.ToLower())
                {
                    case "*.pdf":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_pdf.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.doc":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_doc.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.ppt":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_ppt.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.png":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_png.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.gif":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_gif.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.jpg":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_jpg.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.exl":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_exl.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.csv":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_csv.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.mpg":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_mpg.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.flv":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_flv.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.wma":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_wma.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.zip":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_zip.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.rar":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_rar.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.exe":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_exe.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.dmg":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_dmg.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.mov":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_mov.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.avi":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_avi.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.psd":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_psd.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.ai":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_ai.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.dll":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_dll.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.mp3":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_mp3.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    case "*.wav":
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_wav.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }

                    default:
                    {
                        var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_all.png", UriKind.Relative);
                        return new BitmapImage(uri);
                    }
                }
            }
            else
            {
                var uri = new Uri("/Assets/CM_Assets/Phone/ic_file_large_all.png", UriKind.Relative);
                return new BitmapImage(uri);
            }

            return null;
        }

        /// <summary>
        /// Modifies the target data before passing it to the source object.  This method is called only in <see cref="F:System.Windows.Data.BindingMode.TwoWay" /> bindings.
        /// </summary>
        /// <param name="value">The target data being passed to the source.</param>
        /// <param name="targetType">The <see cref="T:System.Type" /> of data expected by the source object.</param>
        /// <param name="parameter">An optional parameter to be used in the converter logic.</param>
        /// <param name="language">Language</param>
        /// <returns>
        /// The value to be passed to the source object.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}