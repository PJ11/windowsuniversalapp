﻿using ActiveSyncPCL;
//using com.dell.workspace.WorkspaceEmailWP8.View;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Notifications;
using Windows.UI.Xaml.Controls;
using workspace.Helpers;
using WorkspaceUniversalApp.WorkspaceEmailWP8.View;
using workspace_datastore.Helpers;
using workspace_datastore.Managers;
using workspace_datastore.models;

namespace WorkspaceUniversalApp
{
    class BackgroundWorkerForSync
    {
        
        ActiveSync mActiveSync = null;

        workspace_datastore.models.Folder mCurrentVisibleMailFolder = null;
#if WP_80_SILVERLIGHT
        ICalendarItemsView eventsScreen = null;
#endif
        IEmailItemsView eMailScreen = null;
        PanoramaPage mPanoramScreenObject = null;
#if WP_80_SILVERLIGHT
        BackgroundWorker mEmailSyncBackgroundWorker = new BackgroundWorker();
#endif
        bool mIsBusy = false; // We can't seem to rely on the BackgroundWorker.IsBusy flag so make one of our own.
        private static BackgroundWorkerForSync instance;

        Dictionary<int, List<workspace_datastore.models.Folder>> mPingFolderSyncQueue = new Dictionary<int, List<workspace_datastore.models.Folder>>();
        //ProgressIndicator mProgressIndicator = null;
        CoreDispatcher dispatcher = CoreApplication.MainView.CoreWindow.Dispatcher;

        public bool SyncingThreadBusy
        {
            get
            {
                return mIsBusy;
            }
            set
            {
                if (mIsBusy != value)
                    mIsBusy = value;
            }
        }

        private BackgroundWorkerForSync()
        {
            FolderManager folderManager = new FolderManager();
            mCurrentVisibleMailFolder = folderManager.GetFolderInfoAsync("Inbox", 0).Result;

        }

        public static BackgroundWorkerForSync GetInstance(EmailItemsView eMailScreenObject = null)
        {
            if (instance == null)
            {
                instance = new BackgroundWorkerForSync();
                instance.InitializeActiveSyncObject();

                if (eMailScreenObject != null)
                {
                    instance.eMailScreen = eMailScreenObject;
                }

                instance.mIsBusy = false;
            }
            return instance;
        }

#if WP_80_SILVERLIGHT
        public void SetCalendarScreen(ICalendarItemsView screen)
        {
            if (null != screen)
            {
                instance.eventsScreen = screen;
            }
        }

#endif
        public void SetEmailScreen(IEmailItemsView screen)
        {
            if (null != screen && instance.eMailScreen != screen)
            {
                instance.eMailScreen = screen;
            }
        }

        /// <summary>
        /// This will happen when the device's screen is locked.  Currently not working properly.
        /// </summary>
        public void Abort()
        {
            #if WP_80_SILVERLIGHT
            if (null != mEmailSyncBackgroundWorker && mEmailSyncBackgroundWorker.WorkerSupportsCancellation)
            {
                mEmailSyncBackgroundWorker.CancelAsync();
            }
            #endif
        }

        private void InitializeActiveSyncObject()
        {
            if (mActiveSync == null)
            {
                mActiveSync = ActiveSync.GetInstance();

                if (!mActiveSync.EventHandlersAttached)
                {
                    mActiveSync.EASCollectionChangedEvent += activeSync_EASCollectionChangedEvent;
                    mActiveSync.EASCommunicationsErrorEvent += mActiveSync_EASCommunicationsErrorEvent;
                    //mActiveSync.EASTriggerSendPingEvent += mActiveSync_EASTriggerSendPingEvent;
                    //mActiveSync.EASHandlePingResultEvent += mActiveSync_EASHandlePingResultEvent;
                    //mActiveSync.EASHandleResyncEvent += mActiveSync_EASHandleResyncEvent;
                    //mActiveSync.ConstructOutgoingMessage += mActiveSync_ConstructOutgoingMessage;

                    //App.NetworkStatusChanged += App_NetworkStatusChanged;
                    mActiveSync.EventHandlersAttached = true;
                }


            }

        }

        public void SetScreenObject(Page panoramaScreenObject = null)
        {
            if (panoramaScreenObject != null)
            {
                instance.mPanoramScreenObject = (PanoramaPage)panoramaScreenObject;
            }
        }

        //public bool InitializeAndDoSync(workspace_datastore.models.Folder SyncParam)
        //{
        //    bool isSyncStarted = !mIsBusy;
        //    if (!mEmailSyncBackgroundWorker.IsBusy && !mIsBusy)
        //    {
        //        mIsBusy = true;
        //        mEmailSyncBackgroundWorker.WorkerSupportsCancellation = true;
        //        mEmailSyncBackgroundWorker.WorkerReportsProgress = true;

        //        mEmailSyncBackgroundWorker.DoWork -= bg_DoWork;
        //        mEmailSyncBackgroundWorker.RunWorkerCompleted -= bg_RunWorkerCompleted;


        //        mEmailSyncBackgroundWorker.DoWork += bg_DoWork;
        //        mEmailSyncBackgroundWorker.RunWorkerCompleted += bg_RunWorkerCompleted;
        //        mEmailSyncBackgroundWorker.RunWorkerAsync(SyncParam);
        //    }
        //    return isSyncStarted;
        //}

        public bool InitializeAndDoSync(workspace_datastore.models.Folder SyncParam)
        {
            bool isSyncStarted = !mIsBusy;
            if (!mIsBusy)
            {
                mIsBusy = true;
                Task.Run(async () =>
                {
                    {
                        bool result = true;
                        bool forcePingRequest = false;
                        #region sync basis of DBFolder, trying to remove the older way of syncing on basis of string name
                        {
                            string syncTypeOrFolderNameToSync = string.Empty;
                            mCurrentVisibleMailFolder = SyncParam;
                            syncTypeOrFolderNameToSync = mCurrentVisibleMailFolder.displayName;

                            //mCurrentVisibleMailFolder = folderToSync;
                            workspace_datastore.models.Folder folderToSync = mCurrentVisibleMailFolder;

                            result = await mActiveSync.SyncMailBasedOnFolder(mCurrentVisibleMailFolder.displayName, mCurrentVisibleMailFolder.parentFolder_id);

                            // Syncing is done.  Let's do post-sync processing
                            CoreDispatcher dispatcher = CoreApplication.MainView.CoreWindow.Dispatcher;
                            dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                            {
                                //mProgressIndicator.ProgressIndicator.HideAsync();
                                //eMailScreen.UpdateSource(DateTime.Now.ToOADate(), "", EmailItemsView.Order.Descending);
                                #if TO_FIX_FOR_TABLET
                                ToastNotification t = new ToastNotification();
                                string message = string.Format("{0} Sync done for selected folder.", ((result) ? "Successful" : "Failed"));
                                //t.CreateToastNotificationForVertical("Info", message);
#endif
                            });

                            if (null != mPanoramScreenObject)
                            {
                                //await mPanoramScreenObject.SetupPingRequest(false);
                            }

                            // Now that we are done syncing, we need to check to see if there are changes and/or deletes to messages
                            Message_UpdateManager mum = new Message_UpdateManager();
                            List<Message_Updates> changes = await mum.GetUpdatedMsgInfoAsync(0);
                            if (null != changes && changes.Count > 0)
                            {
                                List<MessageManager> messagesToUpdate = new List<MessageManager>();
                                foreach (Message_Updates msgUpdate in changes)
                                {
                                    MessageManager mm = new MessageManager();
                                    mm.messageId = msgUpdate.id;
                                    mm.flag = msgUpdate.flag;
                                    mm.read = msgUpdate.read;
                                    mm.folderId = msgUpdate.folder_id;
                                    mm.serverID = msgUpdate.serverIdentifier;

                                    messagesToUpdate.Add(mm);
                                }
                                bool changeDone = await ActiveSync.GetInstance().SyncMailClientChanges(mCurrentVisibleMailFolder, ServerSyncCommand.ServerSyncCommandType.Change, messagesToUpdate);

                                // Delete database items
                                mum.DeleteAllChangedMessagesAsync();
                            }

                            Message_DeleteManager mdm = new Message_DeleteManager();
                            List<Message_Deletes> deletes = await mdm.GetDeletedMsgInfoAsync(0);
                            if (null != deletes && deletes.Count > 0)
                            {
                                List<ASDeleteItemsRequest> deleteRequests = new List<ASDeleteItemsRequest>();
                                foreach (Message_Deletes itemsToDelete in deletes)
                                {
                                    deleteRequests.Add(new ASDeleteItemsRequest { serverId = itemsToDelete.serverIdentifier });
                                }
                                await ActiveSync.GetInstance().DeleteEmaiItems(mCurrentVisibleMailFolder.displayName, mCurrentVisibleMailFolder.parentFolder_id, deleteRequests);

                                // Delete database items
                                mdm.DeleteAllToDeleteMessagesAsync();
                            }
                        }
                        #endregion
                        // Complated EAS Network communications
                        mIsBusy = false;

                        if (null != mPanoramScreenObject)
                        {
                            // Set up the pinger again after sync(s) have completed
                            await mPanoramScreenObject.SetupPingRequest(false, forcePingRequest);
                        }
                    }
                });
            }
            return isSyncStarted;
        }

        public bool InitializeAndDoSync(string SyncParam)
        {
            bool isSyncStarted = !mIsBusy;
            if (!mIsBusy)
            {
                mIsBusy = true;


                Task.Run(async () =>
                {
                    {
                        bool result = true;
                        bool forcePingRequest = false;
                        #region Sync on the basis of string name
                        {
                            //workspace_datastore.models.Folder folderToSync = mCurrentVisibleMailFolder;
                            string syncTypeOrFolderNameToSync = string.Empty;


                            syncTypeOrFolderNameToSync = SyncParam;


                            //if its "InitalSync" then we will do sync of all Mail, calender and contacts

                            string message = syncTypeOrFolderNameToSync == "INITIALSYNC" ? "Initial Sync" : string.Format("Syncing folder {0}", syncTypeOrFolderNameToSync);
                            #if WP_80_SILVERLIGHT
                            mProgressIndicator.BackgroundOpacity = 1.0;
                            mProgressIndicator.ProgressIndicator.Text = message;
                            #endif
                            CoreDispatcher dispatcher = CoreApplication.MainView.CoreWindow.Dispatcher;
                            dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                            {
                                //mProgressIndicator.ProgressIndicator.ShowAsync();
                            });

                            if (syncTypeOrFolderNameToSync == "INITIALSYNC")
                            {
                                await DoInitialSync(null);
                            }
                            else if (syncTypeOrFolderNameToSync == "Calendar")
                            {
                                await mActiveSync.SyncCalendar();
                                dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                                {
                                    #if WP_80_SILVERLIGHT
                                    mProgressIndicator.ProgressIndicator.HideAsync();
                                    #endif
                                });
                            }
                            else if (syncTypeOrFolderNameToSync == "Contacts")
                            {
                                await mActiveSync.SyncContact();
                                dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                                {
                                    #if WP_80_SILVERLIGHT
                                    mProgressIndicator.ProgressIndicator.HideAsync();
                                    #endif
                                });
                            }
                            else if (syncTypeOrFolderNameToSync == "Inbox")
                            {
                                 #if WP_80_SILVERLIGHT
                                await mActiveSync.SyncMail();
                                dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                                {
                                   
                                    mProgressIndicator.ProgressIndicator.HideAsync();
                                   
                                });
#endif
                            }
                        }
                        #endregion
                        // Complated EAS Network communications
                        mIsBusy = false;

                        if (null != mPanoramScreenObject)
                        {
                            // Set up the pinger again after sync(s) have completed
                            await mPanoramScreenObject.SetupPingRequest(false, forcePingRequest);
                        }
                    }
                });
            }
            return isSyncStarted;
        }


        /// <summary>
        /// Method that will sync >= 1 folders
        /// </summary>
        /// <param name="foldersToSync"></param>
        public bool InitializeAndDoSync(List<workspace_datastore.models.Folder> foldersToSync)
        {
            int indexToAdd = mPingFolderSyncQueue.Count;
            bool initializedSync = !mIsBusy;
            //mSyncingForPingResult = true;

            if (!mIsBusy)
            {
                mIsBusy = true;
                LoggingWP8.WP8Logger.LogMessage("PING Adding folders to Index {0}", indexToAdd);
                mPingFolderSyncQueue.Add(indexToAdd, foldersToSync);

                Task.Run(async () =>
                {
                    {
                        bool result = true;
                        bool forcePingRequest = false;
                        #region Sync on the basis of string name
                        {
                            try
                            {
                                do
                                {
                                    int count = mPingFolderSyncQueue.Count;
                                    KeyValuePair<int, List<workspace_datastore.models.Folder>>[] arValues = mPingFolderSyncQueue.ToArray();
                                    KeyValuePair<int, List<workspace_datastore.models.Folder>> arValue = arValues[count - 1];

                                    foreach (workspace_datastore.models.Folder folderToSync in arValue.Value)
                                    {
                                        LoggingWP8.WP8Logger.LogMessage("PING BACKGROUND WORKER Sync Folder {0}", folderToSync);
                                        result = await mActiveSync.SyncMailBasedOnFolder(folderToSync.displayName, folderToSync.parentFolder_id);
                                    }
                                    LoggingWP8.WP8Logger.LogMessage("PING BACKGROUND WORKER Remove Key {0}", arValue.Key);
                                    mPingFolderSyncQueue.Remove(arValue.Key);
                                } while (mPingFolderSyncQueue.Count > 0);
                            }
                            catch (Exception ec)
                            {
                                LoggingWP8.WP8Logger.LogMessage("Exception {0} Message: {1}", ec.GetType().ToString(), ec.Message);
                            }


                            // Now that we are done syncing, we need to check to see if there are changes and/or deletes to messages
                            Message_UpdateManager mum = new Message_UpdateManager();
                            List<Message_Updates> changes = await mum.GetUpdatedMsgInfoAsync(0);
                            if (null != changes && changes.Count > 0)
                            {
                                List<MessageManager> messagesToUpdate = new List<MessageManager>();
                                foreach (Message_Updates msgUpdate in changes)
                                {
                                    MessageManager mm = new MessageManager();
                                    mm.messageId = msgUpdate.id;
                                    mm.flag = msgUpdate.flag;
                                    mm.read = msgUpdate.read;
                                    mm.folderId = msgUpdate.folder_id;
                                }
                                bool changeDone = await ActiveSync.GetInstance().SyncMailClientChanges(mCurrentVisibleMailFolder, ServerSyncCommand.ServerSyncCommandType.Change, messagesToUpdate);
                            }

                            Message_DeleteManager mdm = new Message_DeleteManager();
                            List<Message_Deletes> deletes = await mdm.GetDeletedMsgInfoAsync(0);
                            if (null != deletes && deletes.Count > 0)
                            {
                                List<ASDeleteItemsRequest> deleteRequests = new List<ASDeleteItemsRequest>();
                                foreach (Message_Deletes itemsToDelete in deletes)
                                {
                                    deleteRequests.Add(new ASDeleteItemsRequest { serverId = itemsToDelete.serverIdentifier });
                                }
                                await ActiveSync.GetInstance().DeleteEmaiItems(mCurrentVisibleMailFolder.displayName, mCurrentVisibleMailFolder.parentFolder_id, deleteRequests);
                            }

                            if (null != mPanoramScreenObject)
                            {
                                // Set up the pinger again after processing changes.
                                //await mPanoramScreenObject.SetupPingRequest(false, true);
                                forcePingRequest = true;
                            }

                        }
                        #endregion
                        // Complated EAS Network communications
                        mIsBusy = false;

                        if (null != mPanoramScreenObject)
                        {
                            // Set up the pinger again after sync(s) have completed
                            await mPanoramScreenObject.SetupPingRequest(false, forcePingRequest);
                        }
                    }
                });
            }
            else
            {
                LoggingWP8.WP8Logger.LogMessage("InitializeAndDoSync() cannot proceed because sync is busy.");
            }
            return initializedSync;
        }

        private async Task<bool> DoInitialSync(object sender)
        {
            bool result = false;

            //We will be getting call back in same function for all, so no need to declare individual callback
            //Mail Sync
            result = await mActiveSync.SyncMail(sender);

            if (result)
            {
                //calendar Sync
                result = false; // await mActiveSync.SyncCalendar();

                if (result)
                {
                    //Contact Sync
                    result = await mActiveSync.SyncContact();
                }
            }

            // TEST TEST TEST FOR TABLET!
            result = true;
            // END TEST TEST TEST FOR TABLET

            if (!result)
            {
                // Something failed in communications (possibly screen locked)
            }
            else
            {
                //call method to create default folders
                FileHandlingPCL.FileManager objFileMngr = new FileHandlingPCL.FileManager();
                await objFileMngr.CreateRootFolders();

                // Syncing is done.  Let's do post-sync processing
                dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                {
                    //mProgressIndicator.IsVisible = false;
#if TO_FIX_FOR_TABLET
                    ToastNotification t = new ToastNotification();
                    string message = string.Format("Initial Sync Complete");
                    t.CreateToastNotificationForVertical("Info", message);
#endif
                });

                // Now that we are done syncing, we need to check to see if there are changes and/or deletes to messages
                Message_UpdateManager mum = new Message_UpdateManager();
                List<Message_Updates> changes = await mum.GetUpdatedMsgInfoAsync(0);
                if (null != changes && changes.Count > 0)
                {
                    List<MessageManager> messagesToUpdate = new List<MessageManager>();
                    foreach (Message_Updates msgUpdate in changes)
                    {
                        MessageManager mm = new MessageManager();
                        mm.messageId = msgUpdate.id;
                        mm.flag = msgUpdate.flag;
                        mm.read = msgUpdate.read;
                        mm.folderId = msgUpdate.folder_id;
                        mm.serverID = msgUpdate.serverIdentifier;

                        messagesToUpdate.Add(mm);
                    }
                    bool changeDone = await ActiveSync.GetInstance().SyncMailClientChanges(mCurrentVisibleMailFolder, ServerSyncCommand.ServerSyncCommandType.Change, messagesToUpdate);

                    // Delete database items
                    mum.DeleteAllChangedMessagesAsync();
                }

                Message_DeleteManager mdm = new Message_DeleteManager();
                List<Message_Deletes> deletes = await mdm.GetDeletedMsgInfoAsync(0);
                if (null != deletes && deletes.Count > 0)
                {
                    List<ASDeleteItemsRequest> deleteRequests = new List<ASDeleteItemsRequest>();
                    foreach (Message_Deletes itemsToDelete in deletes)
                    {
                        deleteRequests.Add(new ASDeleteItemsRequest { serverId = itemsToDelete.serverIdentifier });
                    }
                    await ActiveSync.GetInstance().DeleteEmaiItems(mCurrentVisibleMailFolder.displayName, mCurrentVisibleMailFolder.parentFolder_id, deleteRequests);

                    // Delete database items
                    mdm.DeleteAllToDeleteMessagesAsync();
                }
            }
            // Set up the Pinger
            if (null != mPanoramScreenObject)
                mPanoramScreenObject.FirstTimeNavigateToPanoramaTask(true);

            return result;
        }

        #if WP_80_SILVERLIGHT
        async void bg_DoWork(object sender, DoWorkEventArgs e)
        {
            bool result = true;
            bool forcePingRequest = false;
            BackgroundWorker bgWorker = sender as BackgroundWorker;
            bool restartSync = false;

            #region Sync on the basis of string name
            if (e.Argument is string)
            {
                //workspace_datastore.models.Folder folderToSync = mCurrentVisibleMailFolder;
                string syncTypeOrFolderNameToSync = string.Empty;

                syncTypeOrFolderNameToSync = e.Argument as string;
                //if its "InitalSync" then we will do sync of all Mail, calender and contacts

                string message = syncTypeOrFolderNameToSync == "INITIALSYNC" ? "Initial Sync" : string.Format("Syncing folder {0}", syncTypeOrFolderNameToSync);
                SetSystemTrayVisible(message);
                if (syncTypeOrFolderNameToSync == "INITIALSYNC")
                {
                    eMailScreen.IsSyncing = true;
                    result = await DoInitialSync(sender);
                    eMailScreen.IsSyncing = false;
                    if (result)
                    {
                        if (PhoneApplicationService.Current.State.ContainsKey("DataStoreFolder"))
                        {
                            PhoneApplicationService.Current.State.Remove("DataStoreFolder");
                        }
                    }
                    else
                    {
                        if (bgWorker.CancellationPending)
                        {
                            // Probably screen locked
                            restartSync = true;
                        }
                    }
                }
                else if (syncTypeOrFolderNameToSync == "Calendar")
                {
                    await mActiveSync.SyncCalendar();
                    dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                    {
                        mProgressIndicator.IsVisible = false;
                    });
                }
                else if (syncTypeOrFolderNameToSync == "Contacts")
                {
                    await mActiveSync.SyncContact();
                    dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                    {
                        mProgressIndicator.IsVisible = false;
                    });
                }
                else if (syncTypeOrFolderNameToSync == "Inbox")
                {
                    await mActiveSync.SyncMail(sender);
                    if (PhoneApplicationService.Current.State.ContainsKey("DataStoreFolder"))
                    {
                        PhoneApplicationService.Current.State.Remove("DataStoreFolder");
                    }
                    System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        mProgressIndicator.IsVisible = false;
                    });
                }
            }
            #endregion
            #region sync basis of DBFolder, trying to remove the older way of syncing on basis of string name
            else if (e.Argument is workspace_datastore.models.Folder)
            {
                string syncTypeOrFolderNameToSync = string.Empty;
                mCurrentVisibleMailFolder = e.Argument as workspace_datastore.models.Folder;
                syncTypeOrFolderNameToSync = mCurrentVisibleMailFolder.displayName;

                SetSystemTrayVisible("Syncing " + syncTypeOrFolderNameToSync);
                //mCurrentVisibleMailFolder = folderToSync;
                workspace_datastore.models.Folder folderToSync = mCurrentVisibleMailFolder;
                eMailScreen.IsSyncing = true;

                result = await mActiveSync.SyncMailBasedOnFolder(mCurrentVisibleMailFolder.displayName, mCurrentVisibleMailFolder.parentFolder_id, sender);
                eMailScreen.IsSyncing = false;

                if (result == false)
                {
                    // Communication failure (Possibly because screen got locked)
                    #if WP_80_SILVERLIGHT
                    dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                    {
                        if (mProgressIndicator != null)
                            mProgressIndicator.IsVisible = false;
                    });
#endif
                    if (bgWorker.CancellationPending)
                    {
                        // Screen locked
                        restartSync = true;
                    }
                }
                else
                {
                    if (SuspensionManager.SessionState.ContainsKey("DataStoreFolder") && !bgWorker.CancellationPending)
                    {
                        SuspensionManager.SessionState.Remove("DataStoreFolder");
                    }

                    // Syncing is done.  Let's do post-sync processing
                    dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                    {
                        if (mProgressIndicator != null)
                            mProgressIndicator.IsVisible = false;
                        //eMailScreen.UpdateSource(DateTime.Now.ToOADate(), "", EmailItemsView.Order.Descending);
                        ToastNotification t = new ToastNotification();
                        string message = string.Format("{0} Sync done for selected folder.", ((result) ? "Successful" : "Failed"));
                        //t.CreateToastNotificationForVertical("Info", message);
                    });

                    // Now that we are done syncing, we need to check to see if there are changes and/or deletes to messages
                    Message_UpdateManager mum = new Message_UpdateManager();
                    List<Message_Updates> changes = await mum.GetUpdatedMsgInfoAsync(0);
                    if (null != changes && changes.Count > 0)
                    {
                        List<MessageManager> messagesToUpdate = new List<MessageManager>();
                        foreach (Message_Updates msgUpdate in changes)
                        {
                            MessageManager mm = new MessageManager();
                            mm.messageId = msgUpdate.id;
                            mm.flag = msgUpdate.flag;
                            mm.read = msgUpdate.read;
                            mm.folderId = msgUpdate.folder_id;
                            mm.serverID = msgUpdate.serverIdentifier;

                            messagesToUpdate.Add(mm);
                        }
                        bool changeDone = await ActiveSync.GetInstance().SyncMailClientChanges(mCurrentVisibleMailFolder, ServerSyncCommand.ServerSyncCommandType.Change, messagesToUpdate);

                        // Delete database items
                        mum.DeleteAllChangedMessagesAsync();
                    }

                    Message_DeleteManager mdm = new Message_DeleteManager();
                    List<Message_Deletes> deletes = await mdm.GetDeletedMsgInfoAsync(0);
                    if (null != deletes && deletes.Count > 0)
                    {
                        List<ASDeleteItemsRequest> deleteRequests = new List<ASDeleteItemsRequest>();
                        foreach (Message_Deletes itemsToDelete in deletes)
                        {
                            deleteRequests.Add(new ASDeleteItemsRequest { serverId = itemsToDelete.serverIdentifier });
                        }
                        await ActiveSync.GetInstance().DeleteEmaiItems(mCurrentVisibleMailFolder.displayName, mCurrentVisibleMailFolder.parentFolder_id, deleteRequests);

                        // Delete database items
                        mdm.DeleteAllToDeleteMessagesAsync();
                    }
                }
            }
            #endregion
            #region Ping list sync
            else if (e.Argument is List<workspace_datastore.models.Folder>)
            {
                try
                {
                    eMailScreen.IsSyncing = true;
                    do
                    {
                        int count = mPingFolderSyncQueue.Count;
                        KeyValuePair<int, List<workspace_datastore.models.Folder>>[] arValues = mPingFolderSyncQueue.ToArray();
                        KeyValuePair<int, List<workspace_datastore.models.Folder>> arValue = arValues[count - 1];

                        foreach (workspace_datastore.models.Folder folderToSync in arValue.Value)
                        {
                            LoggingWP8.WP8Logger.LogMessage("PING BACKGROUND WORKER Sync Folder {0}", folderToSync);
                            result = await mActiveSync.SyncMailBasedOnFolder(folderToSync.displayName, folderToSync.parentFolder_id, EASSyncFlags.SyncForPingResponse);
                        }
                        LoggingWP8.WP8Logger.LogMessage("PING BACKGROUND WORKER Remove Key {0}", arValue.Key);
                        mPingFolderSyncQueue.Remove(arValue.Key);
                    } while (mPingFolderSyncQueue.Count > 0);
                    eMailScreen.IsSyncing = false;
                }
                catch (Exception ec)
                {
                    eMailScreen.IsSyncing = false;
                    LoggingWP8.WP8Logger.LogMessage("Exception {0} Message: {1}", ec.GetType().ToString(), ec.Message);
                }


                // Now that we are done syncing, we need to check to see if there are changes and/or deletes to messages
                Message_UpdateManager mum = new Message_UpdateManager();
                List<Message_Updates> changes = await mum.GetUpdatedMsgInfoAsync(0);
                if (null != changes && changes.Count > 0)
                {
                    List<MessageManager> messagesToUpdate = new List<MessageManager>();
                    foreach (Message_Updates msgUpdate in changes)
                    {
                        MessageManager mm = new MessageManager();
                        mm.messageId = msgUpdate.id;
                        mm.flag = msgUpdate.flag;
                        mm.read = msgUpdate.read;
                        mm.folderId = msgUpdate.folder_id;
                    }
                    bool changeDone = await ActiveSync.GetInstance().SyncMailClientChanges(mCurrentVisibleMailFolder, ServerSyncCommand.ServerSyncCommandType.Change, messagesToUpdate);
                }

                Message_DeleteManager mdm = new Message_DeleteManager();
                List<Message_Deletes> deletes = await mdm.GetDeletedMsgInfoAsync(0);
                if (null != deletes && deletes.Count > 0)
                {
                    List<ASDeleteItemsRequest> deleteRequests = new List<ASDeleteItemsRequest>();
                    foreach (Message_Deletes itemsToDelete in deletes)
                    {
                        deleteRequests.Add(new ASDeleteItemsRequest { serverId = itemsToDelete.serverIdentifier });
                    }
                    await ActiveSync.GetInstance().DeleteEmaiItems(mCurrentVisibleMailFolder.displayName, mCurrentVisibleMailFolder.parentFolder_id, deleteRequests);
                }

                if (null != mPanoramScreenObject)
                {
                    // Set up the pinger again after processing changes.
                    //await mPanoramScreenObject.SetupPingRequest(false, true);
                    forcePingRequest = true;
                }

            }
            #endregion

            // Complated EAS Network communications
            mIsBusy = false;

            if (restartSync)
            {
                dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                {
                    mPanoramScreenObject.RestartSync();
                });
            }

            if (null != mPanoramScreenObject)
            {
                // Set up the pinger again after sync(s) have completed
                await mPanoramScreenObject.SetupPingRequest(false, forcePingRequest);
            }
        }

        #endif
        private void SetSystemTrayVisible(string message)
        {
            #if WP_80_SILVERLIGHT
            dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                SystemTray.SetIsVisible(mPanoramScreenObject, true);
                SystemTray.SetOpacity(mPanoramScreenObject, 1.0);
                mProgressIndicator = new ProgressIndicator();
                mProgressIndicator.IsVisible = true;
                mProgressIndicator.IsIndeterminate = true;
                mProgressIndicator.Text = message;

                SystemTray.SetProgressIndicator(mPanoramScreenObject, mProgressIndicator);
            });
            #endif
        }

        #if WP_80_SILVERLIGHT
        void bg_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //We are not relying upon this callback function here
            // First, handle the case where an exception was thrown. 
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            }
            else if (e.Cancelled)
            {

                MessageBox.Show("Canceled");
            }
            else
            {

            }
        }
        #endif

        async void activeSync_EASCollectionChangedEvent(object sender, EASCollectionChangedEventArgs e)
        {
            //Dhruv: Added to sync contacts and calendar, earlier it was synching only emails
            if (e.Folder.Name.ToLower() == "contacts")
            {
                ParsingUtils.ParseContactContent(e.Adds, e.Deletes, e.Changes, e.SoftDeletes, e.Folder);
            }
            else if (e.Folder.Name.ToLower() == "calendar")
            {
                Dictionary<string, List<object>> results = await ParsingUtils.ParseCalendarContent(e.Adds, e.Deletes, e.Changes, e.SoftDeletes, e.Folder);
#if WP_80_SILVERLIGHT
                if (eventsScreen != null)
                {
                    dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                    {
                        eventsScreen.UpdateEventsList(results);
                    });
                }
#endif
            }
            else
            {
#if NOTYET  // Need to change this method signature to add an 'moreItems' param so that we only show the toast when we ARE DONE!
                if (e.Adds.Count > 0)
                {
                    ShellToast st = new ShellToast();
                    st.Title = "Workspace Mail";
                    st.Content = string.Format("Received {0} new messages", e.Adds.Count);
                    st.Show();
                }
#endif
                LoggingWP8.WP8Logger.LogMessage("EAS Collection Changed event {0} Adds {1} Deletes {2} Changes  FOLDER CHANGES NAME {3} VISIBLE FOLDER {4}", e.Adds.Count, e.Deletes.Count, e.Changes.Count, e.Folder.Name, mCurrentVisibleMailFolder);
                Dictionary<string, List<object>> messages = await ParsingUtils.ParseMailContent(e.Adds, e.Deletes, e.Changes, e.SoftDeletes, e.Folder);

                if (null == messages)
                {
                    messages = new Dictionary<string, List<object>>();
                }
                //List<MessageManager> messagesFromNetwork = messages.Cast<MessageManager>().ToList();
                // Do an asynchronous update of the email items list view if this is the folder we are looking at.
                if (e.Folder.Name.ToLower() == mCurrentVisibleMailFolder.displayName.ToLower())
                {
                    //eMailScreen.GenerateFolderIdAndSetCurrentFolder(mCurrentVisibleMailFolder);

                    if (eMailScreen != null)
                    {
                        eMailScreen.GenerateFolderIdAndSetCurrentFolder(mCurrentVisibleMailFolder);
                        dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                        {
                            eMailScreen.UpdateMessageList(messages);
                        });
                    }

                    #region Old Code
                    //System.Windows.Deployment.Current.Dispatcher.BeginInvoke(async () =>
                    //{
                    //    // Need to await this to make sure it gets done in this cycle.
                    //    bool syncForPing = (e.Flags == EASSyncFlags.SyncForPingResponse);
                    //    if (e.Adds != null && e.Adds.Count > 0)
                    //    {
                    //        var result = messages["ADD"].ToList();
                    //        List<MessageManager> messagesFromNetwork = result.Cast<MessageManager>().ToList();
                    //        eMailScreen.UpdateMessageListForAdds("", messagesFromNetwork);
                    //    }
                    //    if ((e.Deletes != null && e.Deletes.Count > 0))
                    //    {
                    //        var result = messages["DELETE"].ToList();
                    //        List<MessageManager> messagesFromNetwork = result.Cast<MessageManager>().ToList();
                    //        eMailScreen.UpdateMessageListForDeletes("", messagesFromNetwork);
                    //    }
                    //    if ((e.SoftDeletes != null && e.SoftDeletes.Count > 0))
                    //    {
                    //        var result = messages["SOFTDELETE"].ToList();
                    //        List<MessageManager> messagesFromNetwork = result.Cast<MessageManager>().ToList();
                    //        eMailScreen.UpdateMessageListForDeletes("", messagesFromNetwork);
                    //    }
                    //    if (e.Changes != null && e.Changes.Count > 0)
                    //    {
                    //        var result = messages["CHANGE"].ToList();
                    //        List<MessageManager> messagesFromNetwork = result.Cast<MessageManager>().ToList();
                    //        eMailScreen.UpdateMessageListForChanges("", messagesFromNetwork);
                    //    }
                    //}); 
                    #endregion
                }
            }
#if NOTYET
            dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                await mPanoramScreenObject.UpdateSelectedPanoromaItem(e);
            });
#endif
        }

        void mActiveSync_EASCommunicationsErrorEvent(object sender, EASCommunicationsErrorEventArgs e)
        {
            #if TO_FIX_FOR_TABLET
            if (e.CommandResponse.HttpStatus == System.Net.HttpStatusCode.Unauthorized)
            {
                Common.ToastNotification tn = new Common.ToastNotification();
                tn.CreateToastNotificationForVertical("Exchange Communication Error", "Exchange server returned: UNAUTHORIZED");
            }
            else
            {
                Common.ToastNotification tn = new Common.ToastNotification();
                tn.CreateToastNotificationForVertical("Exchange Communication Error", "Exchange Server returned: UNKNOWN");
            }
#endif
        }
    }
}
