﻿// <copyright file="PrivacyNotice.xaml.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini                           1.0                 First Version </summary>

namespace workspace.About
{
    #region NAMESPACE
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Windows;
    using Windows.UI.Xaml.Controls;
    #endregion
    /// <summary>
    /// Page to display Terms Of Use
    /// </summary>
    public partial class TermsOfUse : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TermsOfUse"/> class.
        /// </summary>
        public TermsOfUse()
        {
            this.InitializeComponent();
        }
    }
}