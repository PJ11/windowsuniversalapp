﻿// <copyright file="Licensing.xaml.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini                           1.0                 First Version </summary>

namespace workspace.About
{
    #region NAMESPACE
    using Windows.UI.Xaml.Controls;
    #endregion
    /// <summary>
    /// Page to show Licensing statement
    /// </summary>
    public partial class Licensing : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Licensing"/> class.
        /// </summary>
        public Licensing()
        {
            this.InitializeComponent();
        }
    }
}