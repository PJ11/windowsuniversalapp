﻿// <copyright file="PrivacyNotice.xaml.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini                           1.0                 First Version </summary>

namespace workspace.About
{
    #region NAMESPACE
    using Windows.UI.Xaml.Controls;
    #endregion
    /// <summary>
    /// Page to display the privacy notice
    /// </summary>
    public partial class PrivacyNotice : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyNotice"/> class.
        /// </summary>
        public PrivacyNotice()
        {
            this.InitializeComponent();
        }
    }
}