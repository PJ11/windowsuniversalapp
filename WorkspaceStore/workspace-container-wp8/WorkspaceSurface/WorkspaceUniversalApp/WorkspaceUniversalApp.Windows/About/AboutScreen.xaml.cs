﻿// <copyright file="AboutScreen.xaml.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini                           1.0                 First Version </summary>

namespace workspace.About
{
    #region NAMESPACE
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Windows;
    using Common;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Navigation;
    #endregion

    /// <summary>
    /// About Screen of the application
    /// </summary>
    public partial class AboutScreen : Page
    {
        /// <summary>
        /// The time
        /// </summary>
        private int time = 2;

        /// <summary>
        /// The expiry time list
        /// </summary>
        private List<InactivityTimeOut> expiryTimeList;

        /// <summary>
        /// The MGR
        /// </summary>
        private DKCNAppManager mgr = DKCNAppManager.Instance();


        /// <summary>
        /// Initializes a new instance of the <see cref="AboutScreen"/> class.
        /// </summary>
        public AboutScreen()
        {
            this.InitializeComponent();
            this.LoadData();
        }

        /// <summary>
        /// Gets or sets the pin length.
        /// </summary>
        /// <value>
        /// The pin length.
        /// </value>
        private string pinlen { get; set; }

        /// <summary>
        /// Gets or sets the pin attempt.
        /// </summary>
        /// <value>
        /// The pin attempt.
        /// </value>
        private string pinAttempt { get; set; }

        /// <summary>
        /// Gets or sets the expiration.
        /// </summary>
        /// <value>
        /// The expiration.
        /// </value>
        private string expiration { get; set; }

        /// <summary>
        /// Gets or sets the timeout.
        /// </summary>
        /// <value>
        /// The timeout.
        /// </value>
        private int timeout { get; set; }


        /// <summary>
        /// Loads the data.
        /// </summary>
        private async void LoadData()
        {
            this.expiryTimeList = new List<InactivityTimeOut>();
            this.pinlen = Convert.ToString(App.mManager.settingsUserOptions.PinLength);
            this.pinAttempt = Convert.ToString(App.mManager.settingsUserOptions.LoginAttemptsAllowed);
            this.expiration = Convert.ToString(App.mManager.settingsUserOptions.ChecinExpiration);

            this.timeout = App.mManager.settingsUserOptions.CCMInactivityTimeout;

            pinLength.Text = pinlen + " Digits";
            pinAttempts.Text = pinAttempt + " Times";
            checkInExpiration.Text = expiration + " Days";
            if (timeout >= 60)
            {
                inactivityTimePicker.Items.Add("60 Minutes");
            }
            if (timeout >= 30)
            {
                inactivityTimePicker.Items.Add("30 Minutes");
            }
            if (timeout >= 10)
            {
                inactivityTimePicker.Items.Add("10 Minutes");
            }
            if (timeout >= 5)
            {
                inactivityTimePicker.Items.Add("5 Minutes");
            }
            if (timeout >= 2)
            {
                inactivityTimePicker.Items.Add("2 Minutes");
            }
            if (timeout >= 1)
            {
                inactivityTimePicker.Items.Add("1 Minute");
            }
        }
        //  }

        /// <summary>
        /// Handles the SelectionChanged event of the AddressPicker control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void AddressPicker_SelectionChanged(object sender, SelectionChangedEventArgs selectionChangedEventArgs)
        {
#if WP_80_SILVERLIGHT
            if (e.RemovedItems != null && e.RemovedItems.Count > 0)
            {
                var item = inactivityTimePicker.SelectedItem as string;
                switch (item)
                {
                    case "1 Minute": this.time = 1;
                        break;
                    case "2 Minutes": this.time = 2;
                        break;
                    case "5 Minutes": this.time = 5;
                        break;
                    case "10 Minutes": this.time = 10;
                        break;
                    case "30 Minutes": this.time = 30;
                        break;
                    case "60 Minutes": this.time = 60;
                        break;
                }

                App.mManager.settingsUserOptions.InactivityTimeout = this.time;
                App.mManager.storeUserSettings();
            }
#endif
        }


        /// <summary>
        /// Handles the Click event of the Applications control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Applications_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            //NavigationService.Navigate(new Uri("/Workspace;component/About/About_Applications.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage(typeof(AboutApplications));
        }

        /// <summary>
        /// Handles the Click event of the TermsOfUse control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void TermsOfUse_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/Workspace;component/WorkspaceWebBrowserWP8/BrowserView.xaml?IsEulaView=true", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("WebBrowserPage");

        }

        /// <summary>
        /// Handles the Click event of the PrivacyNotice control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void PrivacyNotice_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/Workspace;component/About/PrivacyNotice.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage(typeof(PrivacyNotice));
        }

        /// <summary>
        /// Handles the Click event of the License control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void License_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/Workspace;component/About/Licensing.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage(typeof(Licensing));
        }


        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            bool flag = App.mManager.settingsUserOptions.CopyPasteEnabled;
            if (!flag)
            {
                copyPasteText.Text = "Disabled";
                flag = false;
            }
            else
            {
                copyPasteText.Text = "Enabled";
                flag = true;
            }

            int i = 0;
            switch (timeout)
            {
                case 60: i = 6; break;
                case 30: i = 5; break;
                case 10: i = 4; break;
                case 5: i = 3; break;
                case 2: i = 2; break;
                case 1: i = 1; break;
            }

            var currentTime = App.mManager.settingsUserOptions.InactivityTimeout;

            switch (currentTime)
            {
                case 60: inactivityTimePicker.SelectedIndex = i - 6; break;
                case 30: inactivityTimePicker.SelectedIndex = i - 5; break;
                case 10: inactivityTimePicker.SelectedIndex = i - 4; break;
                case 5: inactivityTimePicker.SelectedIndex = i - 3; break;
                case 2: inactivityTimePicker.SelectedIndex = i - 2; break;
                case 1: inactivityTimePicker.SelectedIndex = i - 1; break;

            }
        }
    }

    /// <summary>
    /// Class to hold the time for inactivty.
    /// </summary>
    public class InactivityTimeOut
    {
        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        /// <value>
        /// The time.
        /// </value>
        public string Time { get; set; }
    }
}