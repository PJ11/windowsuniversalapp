﻿// <copyright file="About_Applications.xaml.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary> Sajini                           1.0                 First Version </summary>

namespace workspace.About
{
    #region NAMESPACE
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;
    using workspace;
    #endregion

    /// <summary>
    /// Screen to enable/disable the applications
    /// </summary>
    public partial class AboutApplications : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AboutApplications"/> class.
        /// </summary>
        public AboutApplications()
        {
            InitializeComponent();        
        }

        private void emailTB_Loaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if(App.mManager.settingsUserOptions.EmailAppGranted!=null)
            {
                if(App.mManager.settingsUserOptions.EmailAppGranted==true)
                {
                    emailTB.Text = "Enabled";
                }
                else
                {
                    emailTB.Text = "Disabled";
                }
            }
        }

        private void calendarTB_Loaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (App.mManager.settingsUserOptions.CalendarAppGranted != null)
            {
                if (App.mManager.settingsUserOptions.CalendarAppGranted == true)
                {
                    calendarTB.Text = "Enabled";
                }
                else
                {
                    calendarTB.Text = "Disabled";
                }
            }
        }

        private void contactsTB_Loaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (App.mManager.settingsUserOptions.ContactsAppGranted != null)
            {
                if (App.mManager.settingsUserOptions.ContactsAppGranted == true)
                {
                    contactsTB.Text = "Enabled";
                }
                else
                {
                    contactsTB.Text = "Disabled";
                }
            }
        }

        private void fileTB_Loaded(object sender, RoutedEventArgs routedEventArgs)
        {
           
                if (App.mManager.settingsUserOptions.FileAppGranted != null)
                {
                    if (App.mManager.settingsUserOptions.FileAppGranted == true)
                    {
                        fileTB.Text = "Enabled";
                    }
                    else
                    {
                        fileTB.Text = "Disabled";
                    }
                }
          
        }

        private void webTB_Loaded(object sender, RoutedEventArgs routedEventArgs)
        {     
            if (App.mManager.settingsUserOptions.WebAppGranted != null)
            {
                if (App.mManager.settingsUserOptions.WebAppGranted == true)
                {
                    webTB.Text = "Enabled";
                }
                else
                {
                    webTB.Text = "Disabled";
                }
            }            
        }
    
    }
}