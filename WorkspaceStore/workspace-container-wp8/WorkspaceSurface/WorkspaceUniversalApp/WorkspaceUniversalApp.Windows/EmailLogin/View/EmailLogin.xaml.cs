﻿// <copyright file="EmailLogin.xaml.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Loren</author>
// <email>lorenr@dmpdev1.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary>Loren                          1.0                 First Version </summary>

using System;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml.Navigation;

namespace workspace.EmailLogin
{
    #region NAMESPACE
    using Windows.UI.Xaml.Controls;
    #endregion
    /// <summary>
    /// Page for Email Login.
    /// </summary>
    public partial class EmailLogin : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailLogin"/> class.
        /// </summary>
        public EmailLogin()
        {
            InitializeComponent();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            StatusBar statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();

            // Hide the status bar
            await statusBar.HideAsync();
        
            base.OnNavigatedTo(e);
        }
    }
}