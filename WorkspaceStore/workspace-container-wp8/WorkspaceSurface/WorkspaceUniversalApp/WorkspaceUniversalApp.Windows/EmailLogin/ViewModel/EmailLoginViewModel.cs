﻿// <copyright file="EmailLoginViewModel.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Loren</author>
// <email>lorenr@dmpdev1.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary>Loren                          1.0                 First Version </summary>
#define USE_ASYNC
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using workspace.Login.View;

namespace workspace.EmailLogin.ViewModel
{
    #region NAMESPACE
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;
    using ActiveSyncPCL;
    using Helpers;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using WorkspaceWP81;
    using WorkspaceWP81.Helpers;
    using workspace_datastore.Managers;
    using workspace_datastore.models;
    using Microsoft.Phone.Controls;
    using System.Windows;
    #endregion

    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class EmailLoginViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the EmailLoginViewModel class.
        /// </summary>              
        #region LOCAL VARIABLE

        /// <summary>
        /// The manual options visibility property name
        /// </summary>
        private const string ManualOptionsVisibilityPropertyName = "ManualOptionsVisibility";

        /// <summary>
        /// The domain property name
        /// </summary>
        private const string DomainPropertyName = "Domain";

        /// <summary>
        /// The SSL property name
        /// </summary>
        private const string SslPropertyName = "UseSSL";

        /// <summary>
        /// The account property name
        /// </summary>
        private const string AccountPropertyName = "Account";

        /// <summary>
        /// The pass property name
        /// </summary>
        private const string PassPropertyName = "PassEnabled";

        /// <summary>
        /// The _navigation service object
        /// </summary>
        private readonly INavigationService navigationService;

        /// <summary>
        /// The data service
        /// </summary>
        private readonly IDataService dataService;

        /// <summary>
        /// The _visibility property name
        /// </summary>
        private Visibility visibility = Visibility.Collapsed;

        /// <summary>
        /// The _domain
        /// </summary>
        private string domain = string.Empty;

        /// <summary>
        /// The _account
        /// </summary>
        private Account account = null;

        /// <summary>
        /// The _use SSL
        /// </summary>
        private bool useSSL = true;

        /// <summary>
        /// The _pass
        /// </summary>
        private bool pass = true;

        /// <summary>
        /// The count
        /// </summary>
        private int count = 0;

        /// <summary>
        /// The  account setup helper variable
        /// </summary>
        private EASAccountSetupHelper accountSetupHelper = null;
        #endregion

        #region CONSTRUCTOR

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailLoginViewModel"/> class.
        /// </summary>
        /// <param name="dataServiceParam">The data service parameter.</param>
        /// <param name="navigationServiceParam">The navigation service parameter.</param>
        public EmailLoginViewModel(IDataService dataServiceParam, INavigationService navigationServiceParam)
        {
            this.PassEnabled = true;
            this.navigationService = navigationServiceParam;
            this.dataService = dataServiceParam;
            //// WP8Logger.LogMessage("ManualSignInViewModel Constructor");
            this.NavigateToHomePage = new RelayCommand(this.ShowHomeView);
            this.NavigateBack = new RelayCommand(this.GoBack);
            this.LoginAuthenticate = new RelayCommand(this.AuthenticateUser);
            Account = new Account();

            LoadCCMSettings();
            this.accountSetupHelper = new EASAccountSetupHelper(this.Account, this.ErrorCallbackMethod);
            ////Sybscribe the funcction GetAccountInfo
            //// EASAccount.GetAccountInfo += new EASAccount.GetAccInfoHandler(GetAccountInfo);          
        }

        async void LoadCCMSettings()
        {
            if (null == App.mManager.settingsUserOptions)
            {
                await App.mManager.loadUserSettings();
            }
            if (App.mManager.settingsUserOptions != null)
            {
                Account.domainName = App.mManager.settingsUserOptions.EasDomain;
                Account.authUser = App.mManager.settingsUserOptions.EasUserName;
                Account.emailAddress = App.mManager.settingsUserOptions.EasEmailAddress;
                Account.serverUrl = App.mManager.settingsUserOptions.EasHost;
            }
            else
            {
                this.ManualOptionsVisibility = Visibility.Visible;
                Account.emailAddress = string.Empty;
                Account.serverUrl = string.Empty;
            }
        }
        #endregion

        #region PROPERTY

        /// <summary>
        /// Gets or sets the manual options visibility.
        /// </summary>
        /// <value>
        /// The manual options visibility.
        /// </value>
        public Visibility ManualOptionsVisibility
        {
            get
            {
                return this.visibility;
            }

            set
            {
                if (this.visibility == value)
                {
                    return;
                }

                this.visibility = value;
                this.RaisePropertyChanged(ManualOptionsVisibilityPropertyName);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [pass enabled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [pass enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool PassEnabled
        {
            get
            {
                return this.pass;
            }

            set
            {
                if (this.pass == value)
                {
                    return;
                }

                this.pass = value;
                this.RaisePropertyChanged(PassPropertyName);
            }
        }

        /// <summary>
        /// Gets or sets the account.
        /// </summary>
        /// <value>
        /// The account.
        /// </value>
        public Account Account
        {
            get
            {
                return this.account;
            }

            set
            {
                if (this.account == value)
                {
                    return;
                }

                this.account = value;
                this.RaisePropertyChanged(AccountPropertyName);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [use SSL].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [use SSL]; otherwise, <c>false</c>.
        /// </value>
        public bool UseSSL
        {
            get
            {
                return this.useSSL;
            }

            set
            {
                if (this.useSSL == value)
                {
                    return;
                }

                this.useSSL = value;
                this.RaisePropertyChanged(SslPropertyName);
            }
        }

        /// <summary>
        /// Gets the navigate to home page.
        /// </summary>
        /// <value>
        /// The navigate to home page.
        /// </value>
        public ICommand NavigateToHomePage { get; private set; }

        /// <summary>
        /// Gets the navigate back.
        /// </summary>
        /// <value>
        /// The navigate back.
        /// </value>
        public ICommand NavigateBack { get; private set; }

        /// <summary>
        /// Gets the login authenticate.
        /// </summary>
        /// <value>
        /// The login authenticate.
        /// </value>
        public ICommand LoginAuthenticate { get; private set; }
        #endregion

        /// <summary>
        /// Errors the callback method.
        /// </summary>
        /// <param name="ar">The ar.</param>
        private void ErrorCallbackMethod(EASAccountSetupHelper.ResultActions ar)
        {
            if (ar == EASAccountSetupHelper.ResultActions.UsePasswordOnlyLoginScreen)
            {
                this.PassEnabled = true;
            }
            else
            {
                this.navigationService.NavigateTo(typeof(ManualSignInView));
            }
        }

        /// <summary>
        /// Authenticates the user.
        /// </summary>
        private async void AuthenticateUser()
        {
            this.PassEnabled = false;
            var result = await this.accountSetupHelper.authneticateUser();
            if (result == 0)
            {
                // Move to next page
                this.navigationService.NavigateTo(typeof(UserWelcomeScreen));
            }
            else
            {
                string EASMessageString = "";
                if (result == 177)
                {
                    EASMessageString = "Too many devices provisioned.  Please delete one or more devices and try again.";
                }
                else
                {
                    EASMessageString = string.Format("Status returned from EAS Authenticate (Autodiscover and Provision) is {0}", result);
                }

#if WP_80_SILVERLIGHT
                CustomMessageBox messageBox = new CustomMessageBox()
                {
                    Caption = "Exchange Autodiscover/Provision Error",
                    Message = EASMessageString,
                    LeftButtonContent = "OK",

                };
                messageBox.Dismissed += (s1, e1) =>
                {
                    switch (e1.Result)
                    {
                        case CustomMessageBoxResult.LeftButton:
                            var frame = Window.Current.Content as Frame;
                            if (frame != null)
                                frame.Navigate(typeof(ManualSignInView));
                            break;
                        case CustomMessageBoxResult.RightButton:
                            break;
                        case CustomMessageBoxResult.None:
                            break;
                        default:
                            break;
                    }
                };
                messageBox.Show();
#else
                MessageDialog messageDialog = new MessageDialog(EASMessageString, "Exchange Autodiscover/Provision Error");
                messageDialog.Commands.Add(new UICommand("OK"));
                await messageDialog.ShowAsync();
#endif
            }
        }

        /// <summary>
        /// Gets the account information.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns></returns>
        private EASAccount GetAccountInfo(string email)
        {
            try
            {
                ////Create object of account manager to get account details from database
                var objAcc = new AccountManager();
                ////Create EASAccount object to set account value
                EASAccount objEAS = null;
                ////call method to get account info
#if USE_ASYNC
                List<Account> list = objAcc.GetAccountInfoAsync(email).Result;
#else
                var list = objAcc.GetAccountInfo(email).ToList();
#endif
                if (list != null && list.Count > 0)
                {
                    objEAS = new EASAccount();
                    ////get single result
                    var acc = list.Single();
                    if (acc != null)
                    {
                        ////set EASAccount object
                        objEAS.DomainName = acc.domainName;
                        objEAS.AuthUser = acc.authUser;
                        objEAS.AuthPassword = acc.authPass;
                        objEAS.EmailAddress = acc.emailAddress;
                        objEAS.ServerUrl = acc.serverUrl;
                        objEAS.PolicyKey = Convert.ToUInt32(acc.policyKey);
                        objEAS.ProtocolVersion = acc.protocolVersion;
                    }
                }
                ////return object
                return objEAS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Goes the back.
        /// </summary>
        private void GoBack()
        {
            // They shouldn't be able to go back
            this.navigationService.GoBack();
        }

        /// <summary>
        /// Shows the home view.
        /// </summary>
        private void ShowHomeView()
        {
            Common.Navigation.NavigationService.NavigateToPage(ViewManager.uriHomeView.GetType());
        }
    }
}