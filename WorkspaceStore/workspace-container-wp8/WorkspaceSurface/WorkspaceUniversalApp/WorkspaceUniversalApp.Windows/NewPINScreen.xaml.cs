﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using System.Windows.Input;
using KeyGenerationForPhone;
using WorkspaceWP81;
using Common;
using System.Threading;
using AbstractsPCL;
using ActiveSyncPCL;
using workspace_datastore.Managers;
using workspace_datastore.models;
using System.Collections.Generic;
using WorkspaceWP81.PIN_View;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;

namespace workspace
{
    public partial class NewPINScreen : Page
    {      

        #region Properties
        public int NewPINLength { get; protected set; }
        #endregion


        public NewPINScreen()
        {
            InitializeComponent();
            var vm = (PinViewModel)this.DataContext;
            vm.newPin = true;
            Loaded += NewPINScreen_Loaded;
        }

        void NewPINScreen_Loaded(object sender, RoutedEventArgs e)
        {
            SetPIN.Focus();
            var vm = (PinViewModel)this.DataContext;
            vm.newPin = true;         

            if (App.mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE) || App.mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE))
            {
                vm.mMandatoryPINChange = true;
            }
               vm.FirstText = "Set New Pin";
               vm.GetPINDigitFromSettings();
           vm.DigitCountText = "Enter a new " + vm.NewPINLength + "-digit PIN Number";

             //  FirstInstruction.Text = "Set New PIN";
             //  InstructionsText.Text = "Enter a new " + vm.NewPINLength + "-digit PIN Number";
        }

        private void PasswordTextBox_KeyUp(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            var viewModelObject = (PinViewModel)this.DataContext;
            viewModelObject.PasswordTextBox_KeyUp(sender, e);                 
        }

        private void PasswordTextBox_KeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            var vm = (PinViewModel)this.DataContext;
            vm.PasswordTextBox_KeyDown(sender, e);         
        }       

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            var vm = (PinViewModel)this.DataContext;
            vm.SetPINText = "";
            vm.passCodeDisplay = "";
            vm.passCodeValue = "";
            vm.passCodeValueConfirm = "";
            vm.passCodeValueInitial = "";
            vm.newPin= false;
            vm.DigitCountText = "";
            vm.FirstText = "";

            if (vm.mSuccessfullyChangedPIN)
            {
                ToastNotification toast = new ToastNotification();
                toast.CreateToastNotificationForVertical("", "Changed PIN Successfully");
            }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            // If the change PIN is required by a # change from CCM, then they can't go back
            if (NewPINLength > 0)
            {
                e.Cancel = true;
            }
#if WP_80_SILVERLIGHT
            else
                base.OnBackKeyPress(e);
#endif
        }
    }
}