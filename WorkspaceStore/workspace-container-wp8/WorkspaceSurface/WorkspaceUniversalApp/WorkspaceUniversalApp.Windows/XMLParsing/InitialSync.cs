﻿#define USE_ASYNC
using ActiveSyncPCL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using workspace_datastore.Managers;

namespace workspace.XMLParsing
{
    class InitialSync
    {
        workspace_datastore.models.Folder folderObject = new workspace_datastore.models.Folder();

        FolderManager folderMngr = new FolderManager();
        internal async Task Parse(ASFolderSyncResponse response)
        {
            try
            {
                XDocument document = new XDocument();
                TextReader tr = new StringReader(response.XmlString);
                document = XDocument.Load(tr);
                string ns = "{FolderHierarchy}";
                var syncKeyQ = document.Descendants().SingleOrDefault(p => p.Name == (ns + "SyncKey"));
                if (syncKeyQ != null)
                {
                    folderObject.syncKey = syncKeyQ.Value;
                    IEnumerable<XElement> addNodeQ = from query in document.Descendants().Where(p => p.Name == (ns + "Add")).ToList() select query;
                    if (null != addNodeQ)
                    {
                        foreach (XElement addNode in addNodeQ)
                        {
                            //folderObject = new workspace_datastore.models.Folder();
                            XElement j = addNode as XElement;
                            if (null != j)
                            {
                                XElement nameQ = j.Descendants().First(p => p.Name == (ns + "DisplayName"));
                                XElement serverIdQ = j.Descendants().First(p => p.Name == (ns + "ServerId"));
                                XElement parentIdQ = j.Descendants().First(p => p.Name == (ns + "ParentId"));
                                XElement typeQ = j.Descendants().First(p => p.Name == (ns + "Type"));

                                folderObject.serverIdentifier = serverIdQ.Value.ToString();
                                folderObject.parentFolder_id = XmlConvert.ToInt32(parentIdQ.Value);
                                folderObject.displayName = nameQ.Value.ToString();
                                folderObject.folderType = XmlConvert.ToInt32(typeQ.Value);


#if USE_ASYNC
                                folderMngr.AddFolderInfoAsync(folderObject);
#else
                                folderMngr.AddFolderInfo(ds.Db, folderObject);
#endif

                            }
                        }
                    }


                    IEnumerable<XElement> updateNodeQ = from query in document.Descendants().Where(p => p.Name == (ns + "Update")).ToList() select query;
                    if (null != updateNodeQ)
                    {
                        foreach (XElement updateNode in updateNodeQ)
                        {
                            //folderObject = new workspace_datastore.models.Folder();
                            XElement j = updateNode as XElement;
                            if (null != j)
                            {
                                XElement nameQ = j.Descendants().First(p => p.Name == (ns + "DisplayName"));
                                XElement serverIdQ = j.Descendants().First(p => p.Name == (ns + "ServerId"));
                                XElement parentIdQ = j.Descendants().First(p => p.Name == (ns + "ParentId"));
                                XElement typeQ = j.Descendants().First(p => p.Name == (ns + "Type"));

                                folderObject.serverIdentifier = serverIdQ.Value.ToString();
                                folderObject.parentFolder_id = XmlConvert.ToInt32(parentIdQ.Value);
                                folderObject.displayName = nameQ.Value.ToString();
                                folderObject.folderType = XmlConvert.ToInt32(typeQ.Value);
#if USE_ASYNC
                                folderMngr.UpdateFolderInfoAsync(folderObject);
#else
								folderMngr.UpdateFolderInfo(ds.Db, folderObject);
#endif
                            }
                        }
                    }


                    IEnumerable<XElement> deleteNodeQ = from query in document.Descendants().Where(p => p.Name == (ns + "Delete")).ToList() select query;
                    if (null != deleteNodeQ)
                    {
                        foreach (XElement deleteNode in deleteNodeQ)
                        {
                            //folderObject = new workspace_datastore.models.Folder();
                            XElement j = deleteNode as XElement;
                            if (null != j)
                            {

                                XElement serverIdQ = j.Descendants().First(p => p.Name == (ns + "ServerId"));
                                string serverIdentifier = serverIdQ.Value.ToString();
#if USE_ASYNC
                                folderMngr.DeleteFolderInfoAsync(serverIdentifier);
#else
								folderMngr.DeleteFolderInfo(ds.Db, folderObject);
#endif
                            }
                        }
                    }

                    //Saving root information for getting folder information next time
                    workspace_datastore.models.Folder rootFolderDB = await folderMngr.GetBaseRootFolder();

                    if (rootFolderDB == null)
                    {
                        workspace_datastore.models.Folder rootFolder = new workspace_datastore.models.Folder();
                        rootFolder.displayName = "RootFolder";
                        rootFolder.syncKey = syncKeyQ.Value.ToString();
                        rootFolder.serverIdentifier = "0";
                        folderMngr.AddFolderInfoAsync(rootFolder);
                    }
                    else
                    {
                        rootFolderDB.syncKey = syncKeyQ.Value.ToString();
                        folderMngr.UpdateFolderInfoAsync(rootFolderDB);
                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //ds.Dispose();
            }
        }

    }
}