﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace workspace.Helpers
{
    /// <summary>
    /// [DEPRECATED] Take from Windows.Phone.Globalization
    /// </summary>
    public sealed class SortedLocaleGrouping
    {
        public IReadOnlyCollection<string> GroupDisplayNames { get; private set; }

        public bool SupportsPhonetics { get; private set; }
        public SortedLocaleGrouping(CultureInfo ci)
        {
            SupportsPhonetics = false;
        }

        public int GetGroupIndex(string name)
        {
            return -1;
        }
    }

    public class KeyedList<T> : List<T>
    {
       /// <summary>
        /// The delegate that is used to get the key information.
        /// </summary>
        /// <param name="item">An object of type T</param>
        /// <returns>The key value to use for this object</returns>
        public delegate string GetKeyDelegate(T item);

        /// <summary>
        /// The Key of this group.
        /// </summary>
        public string Key { get; private set; }

        /// <summary>
        /// Public constructor.
        /// </summary>
        /// <param name="key">The key for this group.</param>
        public KeyedList(string key)
        {
            Key = key;
        }

        /// <summary>
        /// Create a list of AlphaGroup<T> with keys set by a SortedLocaleGrouping.
        /// </summary>
        /// <param name="slg">The </param>
        /// <returns>Theitems source for a LongListSelector</returns>
        private static List<KeyedList<T>> CreateGroups(SortedLocaleGrouping slg)
        {
            List<KeyedList<T>> list = new List<KeyedList<T>>();

            foreach (string key in slg.GroupDisplayNames)
            {
                list.Add(new KeyedList<T>(key));
            }

            return list;
        }

        /// <summary>
        /// Create a list of AlphaGroup<T> with keys set by a SortedLocaleGrouping.
        /// </summary>
        /// <param name="items">The items to place in the groups.</param>
        /// <param name="ci">The CultureInfo to group and sort by.</param>
        /// <param name="getKey">A delegate to get the key from an item.</param>
        /// <param name="sort">Will sort the data if true.</param>
        /// <returns>An items source for a LongListSelector</returns>
        public static List<KeyedList<T>> CreateGroups(IEnumerable<T> items, CultureInfo ci, GetKeyDelegate getKey, bool sort)
        {
            SortedLocaleGrouping slg = new SortedLocaleGrouping(ci);
            List<KeyedList<T>> list = CreateGroups(slg);

            foreach (T item in items)
            {
                int index = 0;
                if (slg.SupportsPhonetics)
                {
                    //check if your database has yomi string for item
                    //if it does not, then do you want to generate Yomi or ask the user for this item.
                    //index = slg.GetGroupIndex(getKey(Yomiof(item)));
                }
                else
                {
                    
                    index = slg.GetGroupIndex(getKey(item));
                }
                if (index >= 0 && index < list.Count)
                {
                    list[index].Add(item);
                }
                else
                {
                    int lastIndex = list.Count-1;
                    list[lastIndex].Add(item);
                }
                    
            }

            if (sort)
            {
                foreach (KeyedList<T> group in list)
                {
                    group.Sort((c0, c1) => { return ci.CompareInfo.Compare(getKey(c0), getKey(c1)); });
                }
            }

            return list;
        }
    }
}
