﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace WorkspaceUniversalApp.Helpers
{
    /// <summary>
    /// This class is used to display the progress ring if page is taking much time to load
    /// </summary>
    public class WorkspaceProgressRing
    {
        //create local variables here
        static public ProgressRing progressRing = new ProgressRing();
        static public TextBlock loadingText = new TextBlock();
      
        /// <summary>
        /// This method is used to show progress ring on the page
        /// </summary>
        /// <param name="LoadingText">Text to show on the page</param>
        /// <param name="PageObject">Current page objetc</param>
        static public void StartProgressRing(string LoadingText, Page PageObject,Grid grid)
        {
            try
            {
          //      ProgressRing progressRing = new ProgressRing();
          //TextBlock loadingText = new TextBlock();
                if (PageObject == null || PageObject.IsEnabled == false)
                    return;
                grid.Children.Remove(progressRing);
                grid.Children.Remove(loadingText);
                grid.Children.Add(progressRing);
                grid.Children.Add(loadingText);
                progressRing.Margin = new Windows.UI.Xaml.Thickness(0, 0, 0, 0);
                progressRing.Height = 200;
                progressRing.Width = 200;
                progressRing.FontSize = 10;
               
                progressRing.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Colors.White);
                progressRing.IsActive = true;

                //loadingText.Margin = new Windows.UI.Xaml.Thickness(0, 100, 0, 0);
                loadingText.Text = LoadingText + "..";
                loadingText.FontSize = 20;
                loadingText.HorizontalAlignment = HorizontalAlignment.Center;
                loadingText.VerticalAlignment = VerticalAlignment.Center;
                loadingText.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(Colors.Black);
                PageObject.Opacity = 0.5;
                PageObject.IsEnabled = false;
            }
            catch (Exception ex)
            {
                //write exception in text file                         
            }
            
        }

        /// <summary>
        /// This method is used to stop the progress ring
        /// </summary>
        /// <param name="PageObject">Current page object</param>
        static public void StopProgressRing(Page PageObject,Grid grid)
        {
            try
            {

                grid.Children.Remove(progressRing);
                grid.Children.Remove(loadingText);
                //.Remove(progressRing);
                //grid.Children.Remove(loadingText);
                progressRing.IsActive = false;
                loadingText.Text = "";
                PageObject.Opacity = 1.0;
                PageObject.IsEnabled = true;
            }
            catch (Exception ex)
            {
                //write exception in text file
               
            }

        }
    }
}
 