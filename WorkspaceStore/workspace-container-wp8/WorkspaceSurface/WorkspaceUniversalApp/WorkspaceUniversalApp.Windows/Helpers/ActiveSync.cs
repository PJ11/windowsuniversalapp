﻿#define USEPCL
#define USE_ASYNC
using ActiveSyncPCL;
using workspace.Helpers;
using workspace;
//using workspace.WorkspaceEmailWP8.ViewModel;
using Common;
using CommonPCL;
using PCLStorage;
using SQLite;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using workspace_datastore;
using workspace_datastore.Managers;
using workspace_datastore.models;
using Windows.Storage;
using workspace_datastore.Helpers;
using Windows.UI.Core;
using Windows.ApplicationModel.Core;
using WorkspaceUniversalApp;



namespace workspace.Helpers
{
    public enum EventICSEncodingAction
    {
        ActionForwardMeetingToAttendees,
        ActionDeleteCalendarItems,
        ActionSendMailToOrganizer,
        ActionSendMailToAttendees
    }

    public class ActiveSync : ActiveSyncBase
    {
        protected static string MAIL_CONTENT_MIME_SPECIFIER = "X-MimeOLE: Produced By Microsoft MimeOLE V6.00.2900.3350";
        public event EASCollectionChangedEventHandler EASCollectionChangedEvent;
        public event EASCommunicationsErrorHandler EASCommunicationsErrorEvent;
        public event EASTriggerSendPingEventHandler EASTriggerSendPingEvent;
        public event EASHandlePingResultEventHandler EASHandlePingResultEvent;
        public event EASResyncFolder EASHandleResyncEvent;
        public event ConstructOutgoingMessageHandler ConstructOutgoingMessage;
        public event DeleteOutgoingMessageHandler DeleteOutgoingMessage;

        #region Properties
        public string ProtocolVersion { get; set; }

        public string UserAgent { get; set; }

        public bool EventHandlersAttached { get; set; }

        public bool EASPingRequested { get; set; }
        #endregion

        // const string activeSyncServer = "oa.kace.com";
        // Set to false to disable SSL
        const bool useSSL = true;
        NetworkCredential userCredentials;
        // Set to true to use a base64-encoded request line
        const bool useBase64RequestLine = false;
        // Replace this value with a local directory
        // to store mailbox contents
        const string mailboxCacheLocation = @"C:\Users\lrogers\Documents\Kace\Mailbox";

        private static ActiveSync _instance;
        //public static bool mEventHandlersAttached = false;
        List<workspace_datastore.Managers.MessageManager> mMessageManagerListForClientSideChanges = null;
        List<workspace_datastore.models.Contacts> mContactListForClientChanges = null;

        ApplicationDataContainer mAppStorageSetting = ApplicationData.Current.LocalSettings;


        byte[] mMapOfIndexToCharacters = new byte[255];

        List<string> foldersCurrentlyBeingSyncd = new List<string>();

        private void generateMapofIndexToCharacters()
        {
            byte[] possibleValues = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 
                                        65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 
                                        97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};
            // The only valid characters EAS accepts for DeviceID is 'A' - 'Z'(65 - 90), 'a' - 'z'(97 - 122), '0' - '9'(48 - 57)
            for (int i = 0; i < 255; i++)
            {
                mMapOfIndexToCharacters[i] = possibleValues[i % 62];
            }
        }

        public static ActiveSyncPCL.Folder getFolderObject(string folderName, List<workspace_datastore.models.Folder> folderListDB)
        {
            workspace_datastore.models.Folder dbMailFolder = folderListDB.Find(x => x.displayName == folderName);// rootFolder.SubFolders.SingleOrDefault(x => x.Name == folderName);
            if (dbMailFolder != null)
            {
                ActiveSyncPCL.Folder.FolderType ft = (ActiveSyncPCL.Folder.FolderType)dbMailFolder.folderType;
                ActiveSyncPCL.Folder asFolder = new ActiveSyncPCL.Folder(dbMailFolder.displayName, dbMailFolder.serverIdentifier.ToString(), ft, null);
                if (dbMailFolder.syncKey != "1")
                {
                    asFolder.SyncKey = dbMailFolder.syncKey;
                }
                double val = System.Convert.ToDouble(dbMailFolder.syncTime);
				//PK:Porting
                //DateTime dt = new DateTime(1, 1, 1).FromOADate(val);
	            DateTime dt = DateTimeExtensions.FromOADate(val);
                asFolder.LastSyncTime = dt;
                return asFolder;
            }
            return null;
        }

		//PK:porting
		private byte[] GetDeviceId()
		{
			var token = Windows.System.Profile.HardwareIdentification.GetPackageSpecificToken(null);
			var hardwareId = token.Id;
			var dataReader = Windows.Storage.Streams.DataReader.FromBuffer(hardwareId);

			byte[] bytes = new byte[hardwareId.Length];
			return bytes;
			//dataReader.ReadBytes(bytes);


			//return BitConverter.ToString(bytes).Replace("-", "");
		}//Note: This function may throw an exception. 

        private ActiveSync()
        {
            Instance = this;
            deviceToProvision = new Device();
            // Need to attach context at the end of device ID
            generateMapofIndexToCharacters();

            //byte[] myDeviceID = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");
	        byte[] myDeviceID = GetDeviceId();
            byte[] deviceid = new byte[myDeviceID.Length];
            for (int i = 0; i < myDeviceID.Length; i++)
            {
                deviceid[i] += mMapOfIndexToCharacters[myDeviceID[i]];
            }

            string udid = Encoding.UTF8.GetString(deviceid, 0, 20);
			//PK: Porting---No function available for OSVersion
            //string OSVersion = Environment.OSVersion.Version.ToString();
	        string OSVersion = "8.1"; //might be wrong but link suggested same.
            deviceToProvision.DeviceID = "workspace" + udid;
            deviceToProvision.DeviceType = "WP8";
            deviceToProvision.Model = "Microsoft Virtual";
            deviceToProvision.FriendlyName = "Dell WorkspaceWP81 Application";
            deviceToProvision.OperatingSystem = "Windows Phone " + OSVersion;
            deviceToProvision.MobileOperator = "Unknown";
            deviceToProvision.UserAgent = "MSFT-WP/" + OSVersion;
            //deviceToProvision.PhoneNumber = "0";
            UserAgent = deviceToProvision.UserAgent;

            if (null != App.mActiveSyncManager && null != App.mActiveSyncManager.Account)
            {
                ProtocolVersion = App.mActiveSyncManager.Account.ProtocolVersion;
#if WP8
                userCredentials = new NetworkCredential(App.mActiveSyncManager.Account.AuthUser, App.mActiveSyncManager.Account.AuthPassword);
#else
                userCredentials = new NetworkCredential(App.mActiveSyncManager.Account.AuthUser, App.mActiveSyncManager.Account.AuthPassword, App.mActiveSyncManager.Account.DomainName);
#endif
            }
            else
            {
                ProtocolVersion = "12.0"; // Default to lowest
            }
        }

        static public ActiveSync GetInstance()
        {
            if (_instance == null)
            {
                _instance = new ActiveSync();
                _instance.EventHandlersAttached = false;
                _instance.EASPingRequested = false;
            }
            return _instance;
        }
        public string getUserNameOnly(EASAccount acct)
        {
            string[] userNameParts = acct.AuthUser.Split('\\');
            string userName;
            if (userNameParts.Count() >= 2)
            {
                userName = userNameParts[1];
            }
            else
            {
                userName = acct.AuthUser;
            }
            return userName;
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        static string GetString(byte[] bytes)
        {
            try
            {
                char[] chars = new char[bytes.Length * 2];
                System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
                return new string(chars);
            }
            catch (Exception)
            {
                return "";
            }
        }

        public override async Task<ASOptionsResponse> getOptions(EASAccount acct)
        {

            System.Diagnostics.Debug.WriteLine("Sending OPTIONS request to server...");

            ASOptionsRequest optionsRequest = new ASOptionsRequest();
            optionsRequest.Server = acct.ServerUrl;
            optionsRequest.UseSSL = useSSL;
            optionsRequest.Credentials = userCredentials;

            // Send the OPTIONS request
            ASOptionsResponse optionsResponse = await optionsRequest.GetOptions();
            if (optionsResponse == null)
            {
                //ReportError("Unable to connect to server.");
                return null;
            }

            System.Diagnostics.Debug.WriteLine("Supported ActiveSync versions: {0}", optionsResponse.SupportedVersions);
            System.Diagnostics.Debug.WriteLine("Supported ActiveSync commands: {0}\n", optionsResponse.SupportedCommands);

            // The EAS protocol version comes in via Options
            ProtocolVersion = optionsResponse.HighestSupportedVersion;
            
            return optionsResponse;
        }

        public override Task provisionUser(string userName, string password, string fullServerUrl)
        {
            return null;
        }

        public override async Task<ASProvisionResponse> provision(EASAccount acct)
        {
            ASProvisionResponse resp = null;
            // Initialize the policy key
            acct.PolicyKey = 0;

            string userName = getUserNameOnly(acct);

            // Phase 1: Initial provision request
            // During this phase, the client requests the policy
            // and the server sends the policy details, along with a temporary
            // policy key.
            ASProvisionRequest initialProvisionRequest = new ASProvisionRequest();

            // Initialize the request with information
            // that applies to all requests.
            initialProvisionRequest.Credentials = userCredentials;
            initialProvisionRequest.Server = acct.ServerUrl; 
            initialProvisionRequest.User = userName;
            initialProvisionRequest.DeviceID = deviceToProvision.DeviceID;
            initialProvisionRequest.DeviceType = deviceToProvision.DeviceType;
            initialProvisionRequest.ProtocolVersion = ProtocolVersion;
            initialProvisionRequest.PolicyKey = acct.PolicyKey;
            initialProvisionRequest.UseEncodedRequestLine = useBase64RequestLine;
            initialProvisionRequest.UserAgent = UserAgent;

            // Initialize the Provision command-specific
            // information.
            initialProvisionRequest.ProvisionDevice = deviceToProvision;

            System.Diagnostics.Debug.WriteLine("Sending initial provision request...");
            // Send the request
            ASProvisionResponse initialProvisionResponse = null;
            var c = await initialProvisionRequest.GetResponse();

            if (null != c)
            {
                initialProvisionResponse = c as ASProvisionResponse;
            }
            System.Diagnostics.Debug.WriteLine("Initial Provision Request:");
            System.Diagnostics.Debug.WriteLine(initialProvisionRequest.XmlString);
            System.Diagnostics.Debug.WriteLine("+++ HIT ENTER TO CONTINUE +++");
            //Console.ReadLine();

            System.Diagnostics.Debug.WriteLine("Initial Provision Response:");
            System.Diagnostics.Debug.WriteLine(initialProvisionResponse.XmlString);
            System.Diagnostics.Debug.WriteLine("+++ HIT ENTER TO CONTINUE +++");
            //Console.ReadLine();

            // Check the result of the initial request. 
            // The server may have requested a remote wipe, or
            // may have returned an error.
            System.Diagnostics.Debug.WriteLine("Response Status: {0}", initialProvisionResponse.Status);
            if (initialProvisionResponse.Status == (Int32)ASProvisionResponse.ProvisionStatus.Success)
            {
                if (initialProvisionResponse.IsPolicyLoaded)
                {
                    if (initialProvisionResponse.Policy.Status ==
                        (Int32)ASProvisionResponse.ProvisionStatus.Success)
                    {
                        if (initialProvisionResponse.Policy.RemoteWipeRequested)
                        {
                            // The server requested a remote wipe.
                            // The client must acknowledge it.
                            System.Diagnostics.Debug.WriteLine("+++ The server has requested a remote wipe. +++\n");

                            ASProvisionRequest remoteWipeAcknowledgment = new ASProvisionRequest();

                            // Initialize the request with information
                            // that applies to all requests.
                            remoteWipeAcknowledgment.Credentials = userCredentials;
                            remoteWipeAcknowledgment.Server = acct.ServerUrl; // activeSyncServer;
                            remoteWipeAcknowledgment.User = userName;
                            remoteWipeAcknowledgment.DeviceID = deviceToProvision.DeviceID;
                            remoteWipeAcknowledgment.DeviceType = deviceToProvision.DeviceType;
                            remoteWipeAcknowledgment.ProtocolVersion = ProtocolVersion;
                            remoteWipeAcknowledgment.PolicyKey = acct.PolicyKey;
                            remoteWipeAcknowledgment.UseEncodedRequestLine = useBase64RequestLine;
                            remoteWipeAcknowledgment.UserAgent = UserAgent;

                            // Initialize the Provision command-specific
                            // information.
                            remoteWipeAcknowledgment.IsRemoteWipe = true;
                            // Indicate successful wipe
                            remoteWipeAcknowledgment.Status =
                                (int)ASProvisionResponse.ProvisionStatus.Success;

                            System.Diagnostics.Debug.WriteLine("Sending remote wipe acknowledgment...");
                            // Send the acknowledgment
                            // TODO: Implement WIPE 
                            ASProvisionResponse remoteWipeAckResponse = await remoteWipeAcknowledgment.GetResponse() as ASProvisionResponse;

                            System.Diagnostics.Debug.WriteLine("Remote Wipe Acknowledgment Request:");
                            System.Diagnostics.Debug.WriteLine(remoteWipeAcknowledgment.XmlString);
                            System.Diagnostics.Debug.WriteLine("+++ HIT ENTER TO CONTINUE +++");

                            System.Diagnostics.Debug.WriteLine("Remote Wipe Acknowledgment Response:");
                            System.Diagnostics.Debug.WriteLine(remoteWipeAckResponse.XmlString);
                            System.Diagnostics.Debug.WriteLine("+++ HIT ENTER TO CONTINUE +++");

                            System.Diagnostics.Debug.WriteLine("Remote wipe acknowledgment response status: {0}",
                                remoteWipeAckResponse.Status);
                        }
                        else
                        {
                            // The server has provided a policy
                            // and a temporary policy key.
                            // The client must acknowledge this policy
                            // in order to get a permanent policy
                            // key.
                            System.Diagnostics.Debug.WriteLine("Policy retrieved from the server.");
                            System.Diagnostics.Debug.WriteLine("Temporary policy key: {0}\n",
                                initialProvisionResponse.Policy.PolicyKey);

                            ASProvisionRequest policyAcknowledgment =
                                new ASProvisionRequest();

                            // Initialize the request with information
                            // that applies to all requests.
                            policyAcknowledgment.Credentials = userCredentials;
                            policyAcknowledgment.Server = acct.ServerUrl; // activeSyncServer;
                            policyAcknowledgment.User = userName;
                            policyAcknowledgment.DeviceID = deviceToProvision.DeviceID;
                            policyAcknowledgment.DeviceType = deviceToProvision.DeviceType;
                            policyAcknowledgment.ProtocolVersion = ProtocolVersion;
                            policyAcknowledgment.UserAgent = UserAgent;
                            // Set the policy key to the temporary policy key from
                            // the previous response.
                            policyAcknowledgment.PolicyKey = initialProvisionResponse.Policy.PolicyKey;
                            policyAcknowledgment.UseEncodedRequestLine = useBase64RequestLine;

                            // Initialize the Provision command-specific
                            // information.
                            policyAcknowledgment.IsAcknowledgement = true;
                            // Indicate successful application of the policy.
                            policyAcknowledgment.Status = (int)ASProvisionRequest.PolicyAcknowledgement.Success;

                            System.Diagnostics.Debug.WriteLine("Sending policy acknowledgment...");
                            // Send the request
                            ASProvisionResponse policyAckResponse = await policyAcknowledgment.GetResponse() as ASProvisionResponse;

                            System.Diagnostics.Debug.WriteLine("Policy Acknowledgment Request:");
                            System.Diagnostics.Debug.WriteLine(policyAcknowledgment.XmlString);
                            System.Diagnostics.Debug.WriteLine("+++ HIT ENTER TO CONTINUE +++");
                            //Console.ReadLine();

                            System.Diagnostics.Debug.WriteLine("Policy Acknowledgment Response:");
                            System.Diagnostics.Debug.WriteLine(policyAckResponse.XmlString);
                            System.Diagnostics.Debug.WriteLine("+++ HIT ENTER TO CONTINUE +++");
                            //Console.ReadLine();

                            System.Diagnostics.Debug.WriteLine("Response status: {0}", policyAckResponse.Status);

                            if (policyAckResponse.Status == (int)ASProvisionResponse.ProvisionStatus.Success
                                && policyAckResponse.IsPolicyLoaded)
                            {
                                System.Diagnostics.Debug.WriteLine("Policy acknowledgment successful.");
                                System.Diagnostics.Debug.WriteLine("Permanent Policy Key: {0}", policyAckResponse.Policy.PolicyKey);

                                // Save the permanent policy key for use
                                // in subsequent command requests.
                                acct.PolicyKey = policyAckResponse.Policy.PolicyKey;
                                resp = policyAckResponse;
                            }
                            else
                            {
                                System.Diagnostics.Debug.WriteLine("Error returned from policy acknowledgment request: {0}",
                                    policyAckResponse.Status);
                            }
                        }
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Policy Error returned from initial provision request: {0}",
                            initialProvisionResponse.Policy.Status);
                    }
                }
            }
            else if (initialProvisionResponse.Status == (Int32)ASProvisionResponse.ProvisionStatus.TooManyDevicesProvisioned)
            {
                resp = initialProvisionResponse;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Error returned from initial provision request: {0}",
                    initialProvisionResponse.Status);
            }
            // If we did not end up with a permanent policy
            // key, then exit.
            if (acct.PolicyKey == 0)
            {
                System.Diagnostics.Debug.WriteLine("Provisioning failed, cannot continue.");
                System.Diagnostics.Debug.WriteLine("+++ HIT ENTER TO EXIT +++");
                //Console.ReadLine();
                return resp;
            }
            return resp;
        }

        public override async Task<EASAccount> discoverUser(string emailAddressFromCCM, string serverUrlFromCCM, string domainFromCCM, string usernameFromCCM, string password)
        {
            App.mActiveSyncManager = ActiveSyncManager.sharedManagerForEmailAddress(emailAddressFromCCM);
            ExchangeServerFinder finder = new ExchangeServerFinder(App.mActiveSyncManager, password);
            bool result = await finder.tumbleExchangeHost(serverUrlFromCCM, emailAddressFromCCM, domainFromCCM, usernameFromCCM);
            if (result)
            {
                App.mActiveSyncManager.Account.DomainName = domainFromCCM;
                //Pankaj: We will save acct information and keep it in base class
#if WP8
                userCredentials = new NetworkCredential(App.mActiveSyncManager.Account.AuthUser, App.mActiveSyncManager.Account.AuthPassword);
#else
                userCredentials = new NetworkCredential(App.mActiveSyncManager.Account.AuthUser, App.mActiveSyncManager.Account.AuthPassword/*, App.mActiveSyncManager.Account.DomainName*/);
#endif
                return App.mActiveSyncManager.Account;
            }
            return null;
        }


        public override async Task<Byte[]> DownloadAttachment(String fileReference)
        {

            //Pankaj : We need to get the file Reference from UI/AppInfrastructure. Currently this is passed from test atmosphere.
            ASAttachmentRequest attachmentRequest = new ASAttachmentRequest(fileReference);

            ASAttachmentResponse attachmentResponse = null;

            if (App.mActiveSyncManager.Account == null)
                throw new InvalidDataException("Account not yet created");

            string userName = getUserNameOnly(App.mActiveSyncManager.Account);

            // Initialize the request with information
            // that applies to all requests.
            attachmentRequest.Credentials = userCredentials;
            attachmentRequest.Server = App.mActiveSyncManager.Account.ServerUrl;
            attachmentRequest.User = userName;
            attachmentRequest.DeviceID = deviceToProvision.DeviceID;
            attachmentRequest.DeviceType = deviceToProvision.DeviceType;
            attachmentRequest.ProtocolVersion = ProtocolVersion;
            attachmentRequest.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
            attachmentRequest.UseEncodedRequestLine = useBase64RequestLine;
            attachmentRequest.UserAgent = UserAgent;

            // Send the request
            attachmentResponse = await attachmentRequest.GetResponse() as ASAttachmentResponse;

            if (attachmentResponse == null)
            {
                System.Diagnostics.Debug.WriteLine("Unable to connect to server.");
                return null;
            }

            System.Diagnostics.Debug.WriteLine("Attachment Request:");
            System.Diagnostics.Debug.WriteLine(attachmentRequest.XmlString);
            System.Diagnostics.Debug.WriteLine("Attachment Response:");
            System.Diagnostics.Debug.WriteLine(attachmentResponse.XmlString);


            if (attachmentResponse.Status ==
                (int)ASAttachmentResponse.DownloadStatus.Success)
            {
                Byte[] attachmentByteArray = attachmentResponse.GetAttachmentData();
                string s = Encoding.UTF8.GetString(attachmentByteArray, 0, attachmentByteArray.Length);
                //FileManager mgr = new FileManager();
                //bool created = await mgr.TestCreateFile("attachment.cs", string.Empty, attachmentByteArray);

                ////checking and reading content again
                //Byte[] readArray = await mgr.TestReadFile("attachment.cs", string.Empty);
                //s = Encoding.UTF8.GetString(readArray, 0, readArray.Length);
                return attachmentByteArray;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Error returned from Attachment request: {0}", attachmentResponse.Status);
                return null;
            }
        }

        public override async Task<bool> SyncCalendar()
        {
            FolderManager folderManager = new FolderManager();
//#if USE_ASYNC
//            List<workspace_datastore.models.Folder> folderListDB = await folderManager.GetFolderInfoAsync(0);
//#else
//             List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfo(0);
//#endif

//            var folderDBCalendar = folderListDB.SingleOrDefault(x => x.displayName == "Calendar" && x.parentFolder_id == 0);

            var folderDBCalendar = await folderManager.GetFolderInfoAsync("Calendar",0); //0 for default folders
            ActiveSyncPCL.Folder.FolderType folderType = (ActiveSyncPCL.Folder.FolderType)folderDBCalendar.folderType;
            ActiveSyncPCL.Folder asCalendarFolder = new ActiveSyncPCL.Folder(folderDBCalendar.displayName, folderDBCalendar.serverIdentifier, folderType, null);

            if (asCalendarFolder != null)
            {
                if (folderDBCalendar.syncKey != "1")
                    asCalendarFolder.SyncKey = folderDBCalendar.syncKey;
            }
            return await GetFolderChanges(asCalendarFolder, App.mActiveSyncManager.Account, deviceToProvision);

        }

        public override async Task<bool> SyncContact()
        {
            FolderManager folderManager = new FolderManager();
//#if USE_ASYNC
//            List<workspace_datastore.models.Folder> folderListDB = await folderManager.GetFolderInfoAsync(0);
//#else
//             List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfo(0);
//#endif

//            var folderDBContacts = folderListDB.SingleOrDefault(x => x.displayName == "Contacts" && x.parentFolder_id == 0);

            var folderDBContacts = await folderManager.GetFolderInfoAsync("Contacts", 0); //0 for default folders
            ActiveSyncPCL.Folder.FolderType folderType = (ActiveSyncPCL.Folder.FolderType)folderDBContacts.folderType;
            ActiveSyncPCL.Folder asContactsFolder = new ActiveSyncPCL.Folder(folderDBContacts.displayName, folderDBContacts.serverIdentifier, folderType, null);

            if (asContactsFolder != null)
            {
                if (folderDBContacts.syncKey != "1")
                    asContactsFolder.SyncKey = folderDBContacts.syncKey;
            }
            return await GetFolderChanges(asContactsFolder, App.mActiveSyncManager.Account, deviceToProvision);

        }

        public override async Task<bool> SyncMailBasedOnFolder(ActiveSyncPCL.Folder folder)
        {
            bool result = false;
            if (!foldersCurrentlyBeingSyncd.Contains(folder.Name.ToLower()))
            {
                foldersCurrentlyBeingSyncd.Add(folder.Name.ToLower());
                result = await GetFolderChanges(folder, App.mActiveSyncManager.Account, deviceToProvision);
                foldersCurrentlyBeingSyncd.Remove(folder.Name.ToLower());
            }
            return result;
        }

        public override async Task<bool> SyncMailBasedOnFolder(string folderName, int folderParentID, object sender = null, EASSyncFlags flags = EASSyncFlags.Unknown)
        {
            bool result = false;
            //We will be getting only mail related(inbox, sent, junk, deleted) data here, contact and calender have different function calls

			FolderManager folderManager = new FolderManager();
			workspace_datastore.models.Folder folderDB = await folderManager.GetFolderInfoAsync(folderName, folderParentID);

//#if USE_ASYNC
//			List<workspace_datastore.models.Folder> folderListDB = await folderManager.GetFolderInfoAsync(0);
//#else
//			 List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfo(0);
//#endif

            //Inbox Folder
            //workspace_datastore.models.Folder dbMailFolder = folderListDB.SingleOrDefault(x => x.displayName == folderName);// rootFolder.SubFolders.SingleOrDefault(x => x.Name == folderName);
			if (folderDB != null)
            {
				if (!foldersCurrentlyBeingSyncd.Contains(folderDB.displayName.ToLower()))
                {
					foldersCurrentlyBeingSyncd.Add(folderDB.displayName.ToLower());
					ActiveSyncPCL.Folder.FolderType ft = (ActiveSyncPCL.Folder.FolderType)folderDB.folderType;
					ActiveSyncPCL.Folder asFolder = new ActiveSyncPCL.Folder(folderDB.displayName, folderDB.serverIdentifier.ToString(), ft, null);
					if (folderDB.syncKey != "1")
                    {
						asFolder.SyncKey = folderDB.syncKey;
                    }
					double val = System.Convert.ToDouble(folderDB.syncTime);
                    DateTime dt = DateTimeExtensions.FromOADate(val);
					//PK: Porting
	                //DateTime dt = DateTimeExtension.FromOADate(val);
                    asFolder.LastSyncTime = dt;
                    result = await GetFolderChanges(asFolder, App.mActiveSyncManager.Account, deviceToProvision);
                    foldersCurrentlyBeingSyncd.Remove(folderDB.displayName.ToLower());
                }
            }
            return result;
        }

        public override async Task<bool> SyncMail(object sender)
        {

            //We will be getting only mail related(inbox, sent, junk, deleted) data here, contact and calender have different function calls

            FolderManager folderManager = new FolderManager();
//#if USE_ASYNC
//            List<workspace_datastore.models.Folder> folderListDB = await folderManager.GetFolderInfoAsync(0);//.Result;
//#else
//             List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfo(0);
//#endif

//            //Creating a ASPCL Inboxfolder, for second time Sync
//            var folderDBInbox = folderListDB.SingleOrDefault(x => x.displayName == "Inbox");

            var folderDBInbox = await folderManager.GetFolderInfoAsync("Inbox", 0);//0 for default folders
            ActiveSyncPCL.Folder.FolderType folderType = (ActiveSyncPCL.Folder.FolderType)folderDBInbox.folderType;
            ActiveSyncPCL.Folder asInboxFolder = new ActiveSyncPCL.Folder(folderDBInbox.displayName, folderDBInbox.serverIdentifier, folderType, null);

            if (asInboxFolder != null)
            {
                //Pankaj: ToDo..Why bydefault its 1? it should be 0
                if (folderDBInbox.syncKey != "1")
                    asInboxFolder.SyncKey = folderDBInbox.syncKey;
            }
            return await GetFolderChanges(asInboxFolder, App.mActiveSyncManager.Account, deviceToProvision, sender);


            // //Sent Items Folder
            // var folderDBSent = folderListDB.SingleOrDefault(x => x.displayName == "Sent Items");
            // folderType = (Folder.FolderType)folderDBSent.folderType;
            // ActiveSyncPCL.Folder asSentItemsFolder = new Folder(folderDBSent.displayName, folderDBSent.serverIdentifier, folderType, null);
            // if (asSentItemsFolder != null)
            // {
            //     if (folderDBSent.syncKey != "1")
            //         asSentItemsFolder.SyncKey = folderDBSent.syncKey;
            // }
            // await GetFolderChanges(asSentItemsFolder, App.mActiveSyncManager.Account, deviceToProvision);


            //// Deleted Folder 
            // var folderDBDeleted = folderListDB.SingleOrDefault(x => x.displayName == "Deleted Items");
            // folderType = (Folder.FolderType)folderDBDeleted.folderType;
            // ActiveSyncPCL.Folder asDeletedItemsFolder = new Folder(folderDBDeleted.displayName, folderDBDeleted.serverIdentifier, folderType, null);
            // if (asDeletedItemsFolder != null)
            // {
            //     if (folderDBDeleted.syncKey != "1")
            //         asDeletedItemsFolder.SyncKey = folderDBDeleted.syncKey;
            // }
            // await GetFolderChanges(asDeletedItemsFolder, App.mActiveSyncManager.Account, deviceToProvision);


            // //Junk Folder
            //var folderDBJunk = folderListDB.SingleOrDefault(x => x.displayName == "Junk E-Mail");
            //folderType = (Folder.FolderType)folderDBJunk.folderType;
            //ActiveSyncPCL.Folder asJunkMailFolder = new Folder(folderDBJunk.displayName, folderDBJunk.serverIdentifier, folderType, null);
            //if (asJunkMailFolder != null)
            //{
            //    if (folderDBJunk.syncKey != "1")
            //        asJunkMailFolder.SyncKey = folderDBJunk.syncKey;
            //}
            //await GetFolderChanges(asJunkMailFolder, App.mActiveSyncManager.Account, deviceToProvision);


            // //Drafts Folder
            //var folderDBDrafts = folderListDB.SingleOrDefault(x => x.displayName == "Drafts");
            //folderType = (Folder.FolderType)folderDBDrafts.folderType;
            //ActiveSyncPCL.Folder asDraftsMailFolder = new Folder(folderDBDrafts.displayName, folderDBDrafts.serverIdentifier, folderType, null);
            //if (asDraftsMailFolder != null)
            //{
            //    if (folderDBDrafts.syncKey != "1")
            //        asDraftsMailFolder.SyncKey = folderDBDrafts.syncKey;
            //}
            //await GetFolderChanges(asDraftsMailFolder, App.mActiveSyncManager.Account, deviceToProvision);

        }

        public override async Task<ASFolderSyncResponse> SyncFolderStructure()
        {

            //if(acct==null)
            //{
            //    acct = new EASAccount();
            //    acct.ServerUrl = "https://" + App.mManager.settingsUserOptions.EasHost;//= GetAccountInfo(App.mManager.settingsUserOptions.EasEmailAddress);
            //    acct.DomainName = App.mManager.settingsUserOptions.EasDomain;
            //    acct.EmailAddress = App.mManager.settingsUserOptions.EasEmailAddress;
            //    acct.AuthUser = App.mManager.settingsUserOptions.EasUserName;
            //    acct.AuthPassword = "Dell123";// App.mManager.settingsUserOptions.EasPassword;
            //        #if WP8
            //                            userCredentials = new NetworkCredential(acct.AuthUser, acct.AuthPassword/*, acct.DomainName*/);
            //        #elif WIN8
            //                                     userCredentials = new NetworkCredential(acct.AuthUser, acct.AuthPassword, acct.DomainName);
            //        #endif

            //}
            string userName = getUserNameOnly(App.mActiveSyncManager.Account);

            // Replace this value with a local directory
            // to store mailbox contents

            // Initialize local storage of mailbox

            string mailboxCacheLocation = FileSystem.Current.LocalStorage.Path + "DMPMailbox";
            //Folder rootFolder = new Folder(mailboxCacheLocation);

			//To check whether we have base root folder information
			FolderManager folderManager = new FolderManager();
			workspace_datastore.models.Folder rootFolderDB = await folderManager.GetBaseRootFolder();

			if (rootFolderDB == null)
				rootFolder = new ActiveSyncPCL.Folder(mailboxCacheLocation);
			else
			{
				rootFolder = new ActiveSyncPCL.Folder(mailboxCacheLocation);
				rootFolder.SyncKey = rootFolderDB.syncKey;
 
			}


            ASFolderSyncRequest folderSyncRequest = new ASFolderSyncRequest();

            // Initialize the request with information that applies to all requests.

            folderSyncRequest.Credentials = userCredentials;
            folderSyncRequest.Server = App.mActiveSyncManager.Account.ServerUrl;
            folderSyncRequest.User = userName;
            folderSyncRequest.DeviceID = deviceToProvision.DeviceID;
            folderSyncRequest.DeviceType = deviceToProvision.DeviceType;
            folderSyncRequest.ProtocolVersion = ProtocolVersion;
            folderSyncRequest.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
            folderSyncRequest.UseEncodedRequestLine = useBase64RequestLine;
            folderSyncRequest.UserAgent = UserAgent;

            // Initialize the FolderSync command-specific information.

            folderSyncRequest.SyncKey = rootFolder.SyncKey;
            // Send the request
            //Console.WriteLine("Sending FolderSync request...");
            ASCommandResponse rest = await folderSyncRequest.GetResponse();
            ASFolderSyncResponse folderSyncResponse = (ASFolderSyncResponse)rest;
            if (folderSyncResponse.Status == (int)ASFolderSyncResponse.FolderSyncStatus.Success)
            {
                // Populate the folder hierarchy from the response
                folderSyncResponse.UpdateFolderTree(rootFolder);



                return folderSyncResponse;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Error returned from FolderSync request: {0}", folderSyncResponse.Status);
                return null;
            }
        }

        public override async Task<bool> GetFolderChanges(ActiveSyncPCL.Folder folder, EASAccount acct, Device device, object sender = null, EASSyncFlags flags = EASSyncFlags.Unknown)
        {
            ASSyncResponse folderSyncResponse = null;
            string userName = getUserNameOnly(acct);
            // If this is the first time syncing Inbox
            // (Sync Key = 0), then you must
            // send an initial sync request to "prime"
            // the sync state.
            if (folder.SyncKey == "0")
            {
                ASSyncRequest initialSyncRequest = new ASSyncRequest();

                // Initialize the request with information
                // that applies to all requests.
                initialSyncRequest.Credentials = userCredentials;
                initialSyncRequest.Server = acct.ServerUrl;
                initialSyncRequest.User = userName;
                initialSyncRequest.DeviceID = device.DeviceID;
                initialSyncRequest.DeviceType = device.DeviceType;
                initialSyncRequest.ProtocolVersion = ProtocolVersion;
                initialSyncRequest.PolicyKey = acct.PolicyKey;
                initialSyncRequest.UseEncodedRequestLine = false;
                initialSyncRequest.UserAgent = UserAgent;

                folder.AreChangesIgnored = true;
                // Initialize the Sync command-specific
                // information.
                // Add the Inbox to the folders to be synced
                initialSyncRequest.Folders.Add(folder);


                // Send the request
                ASSyncResponse initialSyncResponse = await initialSyncRequest.GetResponse() as ASSyncResponse;
                if (initialSyncResponse == null)
                {
                    CoreDispatcher dispatcher = CoreApplication.MainView.CoreWindow.Dispatcher;
                    dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                    {
                        //ToastNotification toast = new ToastNotification();
                        //string message = "Please try again later";
                        //toast.CreateToastNotificationForVertical("Couldn't open connection to the server", message);
                    });

                    return false;
                }

                if (initialSyncResponse.Status == (int)ASSyncResponse.SyncStatus.Success)
                {
                    folder.SyncKey = initialSyncResponse.GetSyncKeyForFolder(folder.Id);
                }
                else
                {
                    return false;
                }
            }
            bool breakLoop = false;

            List<ServerSyncCommand> addCommands = new List<ServerSyncCommand>();
            List<ServerSyncCommand> changeCommands = new List<ServerSyncCommand>();
            List<ServerSyncCommand> deleteCommands = new List<ServerSyncCommand>();
            List<ServerSyncCommand> softDeleteCommands = new List<ServerSyncCommand>();
            bool changes = true;
            bool hasError = false;

            do
            {
                ASSyncRequest inboxSyncRequest = new ASSyncRequest();

                // Initialize the request with information
                // that applies to all requests.
                inboxSyncRequest.Credentials = userCredentials;
                inboxSyncRequest.Server = acct.ServerUrl;
                inboxSyncRequest.User = userName;
                inboxSyncRequest.DeviceID = device.DeviceID;
                inboxSyncRequest.DeviceType = device.DeviceType;
                inboxSyncRequest.ProtocolVersion = ProtocolVersion;
                inboxSyncRequest.PolicyKey = acct.PolicyKey;
                inboxSyncRequest.UseEncodedRequestLine = false;
                inboxSyncRequest.UserAgent = UserAgent;

                // Initialize the Sync command-specific
                // information.
                // Add the Inbox to the folders to be synced
                inboxSyncRequest.Folders.Add(folder);

                // You can specify a Wait or HeartBeatInterval
                // to direct the server to delay a response
                // until something changes.
                // NOTE: Per MS-ASCMD, don't include both
                // a Wait and a HeartBeatInterval.

                // The value of HeartBeatInterval is in seconds
                //inboxSyncRequest.HeartBeatInterval = 480;

                // The value of Wait is in minutes
                //inboxSyncRequest.Wait = 8;

                // You can specify a maximum number of changes
                // that the server should send in the response.
                // Uncomment the line below to specify a maximum.
                //inboxSyncRequest.WindowSize = 512;

                // You can also specify sync options on a specific
                // folder. This is useful if you are syncing
                // multiple folders in a single Sync command request
                // and need to specify different options for each.

                //Useconversation mode need to be set only for mail related sync, not for contacts/calendar 
                if (folder.Name != "Contacts" && folder.Name != "Calendar")
                    folder.UseConversationMode = true;

                // TEST TEST TEST
                folder.UseConversationMode = false;
                // END TEST TEST TEST

                // if folder.syncKey is non-zero (not being primed) then the default assumption would be GetChanges = TRUE
                folder.AreDeletesPermanent = true;
                folder.AreChangesIgnored = false;
                folder.WindowSize = 100;
                // Further options are available in the <Options>
                // element as a child of the <Colletion> element.
                // (See MS-ASCMD section 2.2.3.115.5)


                //PK: We need below condition for all folder where we expect a "Body" and "Data" in xml response
                //We will need user mail folder as well here for body of mail
                if (folder.Name == "Inbox" || folder.Name == "Sent Items" || folder.Name == "Drafts"
                    || folder.Name == "Junk E-Mail" || folder.Name == "Deleted Items"
                    || folder.Type == ActiveSyncPCL.Folder.FolderType.UserMail) // LWR Aug 04, Why just "Inbox" here?
                {
                    // Further options are available in the <Options>
                    // element as a child of the <Colletion> element.
                    // (See MS-ASCMD section 2.2.3.115.5)
                    // From MS-ASAIRS http://msdn.microsoft.com/en-us/library/ee177968(v=exchg.80).aspx
                    folder.Options = new FolderSyncOptions();
                    folder.Options.BodyPreference = new BodyPreferences[1];
                    folder.Options.BodyPreference[0] = new BodyPreferences();

                    folder.Options.BodyPreference[0].Type = BodyType.HTML;
                    folder.Options.BodyPreference[0].TruncationSize = 32768;
                    folder.Options.BodyPreference[0].AllOrNone = false;
                    string[] majorAndMinorProtocolVersions = ProtocolVersion.Split('.');

                    int protocolVersionMajor = Convert.ToInt32(majorAndMinorProtocolVersions[0]);
                    int protocolVersionMinor = 0;
                    if (majorAndMinorProtocolVersions.Length > 1)
                    {
                        protocolVersionMinor = Convert.ToInt32(majorAndMinorProtocolVersions[1]);
                    }
                    if(protocolVersionMajor > 12)
                        folder.Options.BodyPreference[0].Preview = 100;

                    SyncFilterType mailSyncDays = SyncFilterType.TwoWeeksBack;
                    if (Windows.Storage.ApplicationData.Current.LocalSettings.Values.ContainsKey(AppConstString.KEY_MAIL_SYNC_DAYS))
                    {
                        mailSyncDays = Windows.Storage.ApplicationData.Current.LocalSettings.Values[AppConstString.KEY_MAIL_SYNC_DAYS] is SyncFilterType 
                            ? (SyncFilterType) Windows.Storage.ApplicationData.Current.LocalSettings.Values[AppConstString.KEY_MAIL_SYNC_DAYS] : SyncFilterType.NoFilter;
                    }
                    folder.Options.FilterType = mailSyncDays;
                }
                else if (folder.Name == "Calendar")
                {
                    // From: http://msdn.microsoft.com/en-us/library/gg709713(v=exchg.80).aspx
                    //Calendar items that are in the future or that have recurrence but no end date are sent to the client 
                    // regardless of the FilterType element value. Calendar items that fall between the FilterType element 
                    // value and the present time are returned to the client. For example, an appointment that occurred two 
                    // weeks ago is returned when the FilterType element value is set to 5 (one month back). The result of 
                    // including a FilterType element value of 8 for a Calendar item is undefined. The server can return a 
                    // protocol status error in response to such a command request.
                    // Further options are available in the <Options>
                    // element as a child of the <Colletion> element.
                    // (See MS-ASCMD section 2.2.3.115.5)

                    folder.Options = new FolderSyncOptions();
                    folder.Options.BodyPreference = new BodyPreferences[1];
                    folder.Options.BodyPreference[0] = new BodyPreferences();

                    folder.Options.BodyPreference[0].Type = BodyType.HTML;   //Phone number problem 
                    folder.Options.BodyPreference[0].TruncationSize = 32768;
                    SyncFilterType syncWindow = SyncFilterType.NoFilter;
                    if (Windows.Storage.ApplicationData.Current.LocalSettings.Values.ContainsKey(AppConstString.CALENDAR_SYNC_WINDOW))
                    {
                        CommonPCL.DKCNCalendarDays syncCalWindow = DKCNCalendarDays.CalendarDaysTwoWeeksBack;

                        syncCalWindow = Windows.Storage.ApplicationData.Current.LocalSettings.Values[AppConstString.CALENDAR_SYNC_WINDOW] is DKCNCalendarDays 
                            ? (DKCNCalendarDays) Windows.Storage.ApplicationData.Current.LocalSettings.Values[AppConstString.CALENDAR_SYNC_WINDOW] : DKCNCalendarDays.CalendarDaysNotSet;
                        
                        if (syncCalWindow == DKCNCalendarDays.CalendarDaysNotSet)
                            syncWindow = SyncFilterType.NoFilter;
                        else if (syncCalWindow == DKCNCalendarDays.CalendarDaysTwoWeeksBack)
                            syncWindow = SyncFilterType.TwoWeeksBack;
                        else if (syncCalWindow == DKCNCalendarDays.CalendarDaysOneMonthBack)
                            syncWindow = SyncFilterType.OneMonthBack;
                        else if (syncCalWindow == DKCNCalendarDays.CalendarDaysThreeMonthsBack)
                            syncWindow = SyncFilterType.ThreeMonthsBack;
                        else if (syncCalWindow == DKCNCalendarDays.CalendarDaysSixMonthsBack)
                            syncWindow = SyncFilterType.SixMonthsBack;
                    }
                    folder.Options.FilterType = syncWindow;
                }
                // Send the request

                
                if (null != EASCommunicationsErrorEvent && hasError)
                {
                    EASCommunicationsErrorEvent(this, new EASCommunicationsErrorEventArgs(folderSyncResponse));
                }

                //if (folder.Name.ToLower() == "inbox")
                //    System.Diagnostics.Debug.WriteLine("FOLDER SYNC KEY WRITE NETWORK: {0}", folder.SyncKey);
                folderSyncResponse = await inboxSyncRequest.GetResponse() as ASSyncResponse;

                if (folderSyncResponse == null)
                {
                    return false;
                }
                if (folderSyncResponse.Status == (int)ASSyncResponse.SyncStatus.Retry)
                {
                    folderSyncResponse.Dispose();
                    continue;
                }
                else if (folderSyncResponse.Status == (int)ASSyncResponse.SyncStatus.Success)
                {
                    List<ServerSyncCommand> iterationAdds = folderSyncResponse.GetServerSyncCommandsForFolder(folder.Id, null, ServerSyncCommand.ServerSyncCommandType.Add);
                    List<ServerSyncCommand> iterationDeletes = folderSyncResponse.GetServerSyncCommandsForFolder(folder.Id, null, ServerSyncCommand.ServerSyncCommandType.Delete);
                    List<ServerSyncCommand> iterationChanges = folderSyncResponse.GetServerSyncCommandsForFolder(folder.Id, null, ServerSyncCommand.ServerSyncCommandType.Change);
                    List<ServerSyncCommand> iterationSoftDeletes = folderSyncResponse.GetServerSyncCommandsForFolder(folder.Id, null, ServerSyncCommand.ServerSyncCommandType.SoftDelete);

                    //Checking Moreavailable tag in response
                    bool moreitems = folderSyncResponse.IsMoreAvailable(folder.Id);

                    if (!moreitems)
                    {
                        breakLoop = true;
                    }
                    folder.SyncKey = folderSyncResponse.GetSyncKeyForFolder(folder.Id);
                    folder.LastSyncTime = DateTime.Now;
                    //if(folder.Name.ToLower() == "inbox")
                    //    System.Diagnostics.Debug.WriteLine("FOLDER SYNC KEY <POST ARRAY BUILDS> NETWORK: {0}", folder.SyncKey);
                    if (null != EASCollectionChangedEvent)
                    {
                        EASCollectionChangedEvent(this, new EASCollectionChangedEventArgs(iterationAdds, iterationChanges, iterationDeletes, iterationSoftDeletes, folder, flags, sender));
                        changes = false;
                    }

                    // Dispose of response
                    folderSyncResponse.Dispose();
                }
                else if (folderSyncResponse.Status == (int)ASSyncResponse.SyncStatus.InvalidSyncKey)
                {
                    // Looks like we have to clear the Message and Attachment tables for this folder and retry full sync
                    if (null != EASHandleResyncEvent)
                    {
                        EASHandleResyncEvent(this, new EASResyncFolderEventArgs(folder));
                    }
                    changes = false;
                    breakLoop = true;

                    // Dispose of response
                    folderSyncResponse.Dispose();
                }
                else if (folderSyncResponse.HttpStatus == HttpStatusCode.OK)
                {

                    // The server had no changes
                    // to send. At this point you could
                    // repeat the last sync request by sending
                    // an empty sync request (MS-ASCMD section 2.2.2.19.1.1)
                    // This sample does not implement repeating the request,
                    // so just notify the user.
                    folder.LastSyncTime = DateTime.Now;

                    // Call SaveFolderInfo to update the last sync time
                    breakLoop = true;

                    changes = true;
                    folderSyncResponse.Dispose();
                }
                else if (folderSyncResponse.Status == 0)
                {
                    hasError = true;
                    changes = false;
                    breakLoop = true;
                    folderSyncResponse.Dispose();
                }
                else
                {
                    changes = false;
                    breakLoop = true;
                    folderSyncResponse.Dispose();
                }


            } while (!breakLoop);

            if (null != EASCollectionChangedEvent && changes)
            {

                EASCollectionChangedEvent(this, new EASCollectionChangedEventArgs(addCommands, changeCommands, deleteCommands, softDeleteCommands, folder, flags, sender));
            }

            if (null != EASCommunicationsErrorEvent && hasError)
            {
                EASCommunicationsErrorEvent(this, new EASCommunicationsErrorEventArgs(folderSyncResponse));
            }

            return true;
        }

        public override async Task<ASDeleteItemsResponse> DeleteEmaiItems(string folderName,int folderParentID, List<ASDeleteItemsRequest> msgServerId)
        {

            string userName = getUserNameOnly(App.mActiveSyncManager.Account);
            ASDeleteItemsRequest deleteItemsRequest = new ASDeleteItemsRequest();


			FolderManager folderManager = new FolderManager();
			workspace_datastore.models.Folder folderDB = await folderManager.GetFolderInfoAsync(folderName, folderParentID);
//#if USE_ASYNC
//			List<workspace_datastore.models.Folder> folderListDB = await folderManager.GetFolderInfoAsync(0);
//#else
//			 List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfo(0);
//#endif

            //Creating a ASPCL Inboxfolder, for second time Sync
            //var folderDB = folderListDB.SingleOrDefault(x => x.displayName == folderName);
            ActiveSyncPCL.Folder ASFolder = null;
			if (foldersCurrentlyBeingSyncd.Contains(folderDB.displayName.ToLower()))
            {
                Message_DeleteManager mdm = new Message_DeleteManager();
                foreach (ASDeleteItemsRequest delRequest in msgServerId)
                {
                    Message_Deletes deleteMessage = new Message_Deletes();
                    deleteMessage.serverIdentifier = delRequest.serverId;
                    deleteMessage.folder_id = folderDB.id;
                    mdm.AddUpdateDeletedMsgInfoAsync(deleteMessage);
                }
            }
            else
            {
                if (folderDB != null)
                {
                    ActiveSyncPCL.Folder.FolderType folderType = (ActiveSyncPCL.Folder.FolderType)folderDB.folderType;
                    ASFolder = new ActiveSyncPCL.Folder(folderDB.displayName, folderDB.serverIdentifier, folderType, null);

                    if (ASFolder != null)
                    {
                        if (folderDB.syncKey != "1")
                            ASFolder.SyncKey = folderDB.syncKey;
                    }
                }
                if (ASFolder.SyncKey != "0")
                {
                    deleteItemsRequest.CollectionId = ASFolder.Id;
                    deleteItemsRequest.SyncKey = ASFolder.SyncKey;
                    deleteItemsRequest.Items = msgServerId;
                    deleteItemsRequest.CommandType = EnumContactCommandType.Delete;
                    // Initialize the request with information
                    // that applies to all requests.
                    deleteItemsRequest.Credentials = userCredentials;
                    deleteItemsRequest.Server = App.mActiveSyncManager.Account.ServerUrl;
                    deleteItemsRequest.User = userName;
                    deleteItemsRequest.DeviceID = deviceToProvision.DeviceID;
                    deleteItemsRequest.DeviceType = deviceToProvision.DeviceType;
                    deleteItemsRequest.ProtocolVersion = ProtocolVersion;
                    deleteItemsRequest.UserAgent = UserAgent;
                    deleteItemsRequest.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
                    deleteItemsRequest.UseEncodedRequestLine = useBase64RequestLine;

                    ASDeleteItemsResponse deletedItemResponse = await deleteItemsRequest.GetResponse() as ASDeleteItemsResponse;
                    if (deletedItemResponse != null && deletedItemResponse.status == 1)
                    {
                        ASFolder.LastSyncTime = DateTime.Now;
                        ASFolder.SyncKey = deletedItemResponse.GetSyncKeyForFolder(ASFolder.Id);
                        //update sync key in database if folser is not contact or caledar
                        if (ASFolder.Name.ToLower() == "contacs")
                        { }
                        else if (ASFolder.Name.ToLower() == "calendar")
                        { }
                        else
                        {
                            await workspace_datastore.Helpers.ParsingUtils.ParseMailContent(null, null, null, null, ASFolder);
                        }
                        return deletedItemResponse;
                    }
                }
            }
            return null;
        }



        #region Attende response to server and to Organizer

        /// <summary>
        /// Forward meeting requests to someone
        /// </summary>
        /// <param name="organizerName"></param>
        /// <param name="organizerEmail"></param>
        /// <param name="attendees"></param>
        /// <param name="location"></param>
        /// <param name="meetingStatus"></param>
        /// <param name="recurrence"></param>
        /// <param name="dtstamp"></param>
        /// <param name="UID"></param>
        /// <param name="subject"></param>
        /// <param name="content"></param>
        /// <param name="startime"></param>
        /// <param name="endtime"></param>
        /// <returns></returns>
        public async Task<Guid> ForwardMeetingtoAttendessMail(string organizerName, string organizerEmail,
          List<EASAttendee> newAttendees, List<EASAttendee> allAttendees, string location, MeetingStatusType meetingStatus,
          CMRecurrence recurrence, string dtstamp, string UID, string subject, string content, string startime, string endtime, string mailMessage, int singleEventofReoccuring, double singleEventofReoccuringstartDayAndTime)
        {
            ASSendMailRequest sendMailRequest = new ASSendMailRequest();

            ASSendMailResponse sendMailResponse = null;

            if (App.mActiveSyncManager.Account == null)
                throw new InvalidDataException("Account not yet created");

            string userName = getUserNameOnly(App.mActiveSyncManager.Account);

            // Initialize the request with information
            // that applies to all requests.
            sendMailRequest.Credentials = userCredentials;
            sendMailRequest.Server = App.mActiveSyncManager.Account.ServerUrl;
            sendMailRequest.User = userName;
            sendMailRequest.DeviceID = deviceToProvision.DeviceID;
            sendMailRequest.DeviceType = deviceToProvision.DeviceType;
            sendMailRequest.ProtocolVersion = ProtocolVersion;
            sendMailRequest.UserAgent = UserAgent;
            sendMailRequest.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
            sendMailRequest.UseEncodedRequestLine = useBase64RequestLine;
#if OLDWAY
            StringBuilder meetinmsg = new StringBuilder();
            meetinmsg.AppendLine("BEGIN:VCALENDAR");
            meetinmsg.AppendLine("PRODID:-//Dell/Dell Calendar V1.0/EN");
            meetinmsg.AppendLine("VERSION:2.0");
            meetinmsg.AppendLine("CALSCALE:GREGORIAN");
            meetinmsg.AppendLine("METHOD:REQUEST");

            meetinmsg.AppendLine("BEGIN:VEVENT");
            meetinmsg.AppendLine("CREATED:" + dtstamp);
            meetinmsg.AppendLine("DTSTAMP:" + DateTimeOffset.Now.LocalDateTime.ToString("yyyyMMddThhmmssZ"));
            meetinmsg.AppendLine("LAST-MODIFIED:" + DateTimeOffset.Now.LocalDateTime.ToString("yyyyMMddThhmmssZ"));
            meetinmsg.AppendLine("UID:" + UID);
            meetinmsg.AppendLine("SUMMARY:" + subject);
            meetinmsg.AppendLine("LOCATION:" + location);
            meetinmsg.AppendLine("DESCRIPTION:" + content);
            meetinmsg.AppendLine("DTSTART:" + startime);
            meetinmsg.AppendLine("DTEND:" + endtime);
            string organizerLine = string.Format("ORGANIZER;CN={0};RSVP=TRUE;PARTSTAT=ACCEPTED;ROLE=CHAIR:mailto:{1}", organizerName, organizerEmail);
            meetinmsg.AppendLine(organizerLine);
            foreach (EASAttendee attendee in allAttendees)
            {
                string action = "NEEDS-ACTION";
                if (attendee.AttendeeStatus == EventStatus.Accepted)
                {
                    action = "ACCEPTED";
                }
                else if (attendee.AttendeeStatus == EventStatus.Declined)
                {
                    action = "DECLINED";
                }
                else if (attendee.AttendeeStatus == EventStatus.Tentative)
                {
                    action = "TENTATIVE";
                }
                string line = string.Format("ATTENDEE;CN={0};RSVP=TRUE;PARTSTAT={1};ROLE=REQ-PARTICIPANT:mailto:{2}", attendee.Name, action, attendee.Email);
                meetinmsg.AppendLine(line);

            }
            string status = "CONFIRMED";
            if (meetingStatus == MeetingStatusType.OrganizerCanceled || meetingStatus == MeetingStatusType.GuestCanceled)
            {
                status = "CANCELED";
            }

            if (null != recurrence)
            {
                // Build the recurrence String
                string type = "DAILY";
                string rule = "";
                switch (recurrence.RecurrenceType)
                {
                    case CMRecurrenceType.CMRecurrenceTypeWeekly:
                        type = "WEEKLY";
                        break;
                    case CMRecurrenceType.CMRecurrenceTypeMonthly:
                    case CMRecurrenceType.CMRecurrenceTypeMonthlyNDay:
                        type = "MONTHLY";
                        break;
                    case CMRecurrenceType.CMRecurrenceTypeYearly:
                    case CMRecurrenceType.CMRecurrenceTypeYearlyNDay:
                        type = "YEARLY";
                        break;
                    default:
                        break;
                }
                rule += "FREQ=" + type;

                if (recurrence.Interval > 0)
                {
                    rule += ";INTERVAL=" + recurrence.Interval.ToString();
                }

                if (null != recurrence.Until)
                {
                    rule += string.Format(";UNTIL={0}", recurrence.Until.ToString("yyyyMMddThhmmssZ"));
                }
                else if (recurrence.Occurences > 0)
                {
                    rule += ";COUNT=" + recurrence.Occurences;
                }
                if (recurrence.DayOfWeek > 0)
                {
                    List<string> days = new List<string>();
                    int d = recurrence.DayOfWeek;
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeSunday) > 0)
                    {
                        days.Add("SU");
                    }
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeMonday) > 0)
                    {
                        days.Add("MO");
                    }
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeTuesday) > 0)
                    {
                        days.Add("TU");
                    }
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeWednesday) > 0)
                    {
                        days.Add("WE");
                    }
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeThursday) > 0)
                    {
                        days.Add("TH");
                    }
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeFriday) > 0)
                    {
                        days.Add("FR");
                    }
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeSaturday) > 0)
                    {
                        days.Add("SA");
                    }
                    if (days.Count > 0)
                    {
                        if (recurrence.WeekOfMonth > 0)
                        {
                            rule += ";BYDAY=" + recurrence.WeekOfMonth.ToString() + days[0];
                        }
                        else
                        {
                            rule += ";BYDAY=";
                            foreach (string day in days)
                            {
                                rule += day;
                                if (day != days[days.Count])
                                {
                                    rule += ",";
                                }
                            }
                        }
                    }
                    if (recurrence.DayOfMonth > 0)
                    {
                        rule += ";BYMONTHDAY=" + recurrence.DayOfMonth.ToString();
                    }
                    if (recurrence.MonthOfYear > 0)
                    {
                        rule += ";BYMONTH=" + recurrence.MonthOfYear.ToString();
                    }
                }
                meetinmsg.AppendLine(rule);
            }
            meetinmsg.AppendLine("STATUS:" + status);  // Need to update this
            meetinmsg.AppendLine("TRANSP:OPAQUE");
            meetinmsg.AppendLine("SEQUENCE:0");
            meetinmsg.AppendLine("END:VEVENT");
            meetinmsg.AppendLine("END:VCALEDNAR");
#else
            StringBuilder meetingMsg = EncodeCalendarICSFormat(EventICSEncodingAction.ActionForwardMeetingToAttendees,
                organizerName, organizerEmail,
                allAttendees, location, meetingStatus, recurrence,
                dtstamp, UID, subject, content, startime, endtime, false, "", EventStatus.None, singleEventofReoccuring, singleEventofReoccuringstartDayAndTime);
#endif
            byte[] meetingMessageAsBytes = Encoding.UTF8.GetBytes(meetingMsg.ToString());
            string base64EncodedICS = Convert.ToBase64String(meetingMessageAsBytes);
#if OLDWAY
            StringBuilder emailBody = new StringBuilder();
            emailBody.AppendLine("From: " + App.mActiveSyncManager.Account.EmailAddress);
            string emailString = "";
            foreach (EASAttendee attendee in newAttendees)
            {
                if (attendee.Name.Length > 0)
                {
                    emailString += string.Format("\"{0}\"", attendee.Name);
                }
                emailString += attendee.Email;
                if (attendee != newAttendees[newAttendees.Count - 1])
                {
                    emailString += ",";
                }
            }
            emailBody.AppendLine("To: " + emailString);
            emailBody.AppendLine("Subject: " + "FW:" + subject);
            emailBody.AppendLine("Mime-Version: 1.0");
            emailBody.AppendLine("Content-Type: multipart/mixed; boundary=NextPart");
            emailBody.AppendLine();

            emailBody.AppendLine("--NextPart");
            emailBody.AppendLine("Content-Type: text/plain; charset=\"iso-8859-1\"");
            emailBody.AppendLine();
            emailBody.AppendLine(mailMessage);
            emailBody.AppendLine();
            emailBody.AppendLine(content);
            emailBody.AppendLine();

            emailBody.AppendLine("--NextPart");
            emailBody.AppendLine("Content-Type: text/calendar; method=REQUEST; name=\"invite.ics\"");
            emailBody.AppendLine("Content-Transfer-Encoding: base64");
            emailBody.AppendLine();
            emailBody.AppendLine(base64EncodedICS);
            emailBody.AppendLine();
            emailBody.AppendLine("--NextPart--");
#else
            StringBuilder emailStringExcludingAttachmentContent = new StringBuilder();
            StringBuilder emailBody = EncodeEventMail(subject, content, mailMessage, newAttendees, base64EncodedICS, ref emailStringExcludingAttachmentContent);
#endif
            sendMailRequest.MimeContent = emailBody;
            Guid clientId = Guid.NewGuid();
            sendMailRequest.ClientID = clientId.ToString();

            if (null != ConstructOutgoingMessage)
            {
                // Until attachments are supported in a Calendar event, we can continue to use the emailBody string.  After
                // that we'll have to use emailBodyWithoutAttachments instead of emailBody
                ConstructOutgoingMessage(this, new ConstructOutgoingMessageEventArgs(clientId.ToString(), -1, emailBody, (int)MailActionType.ReferenceForward, null));
            }

            // Send the request
            sendMailResponse = await sendMailRequest.GetResponse() as ASSendMailResponse;

            if (sendMailResponse == null)
            {
                System.Diagnostics.Debug.WriteLine("Unable to connect to server.");
                return Guid.Empty;
            }

            System.Diagnostics.Debug.WriteLine("SendMail Request:");
            System.Diagnostics.Debug.WriteLine(sendMailRequest.XmlString);
            System.Diagnostics.Debug.WriteLine("SendMail Response:");
            System.Diagnostics.Debug.WriteLine(sendMailResponse.XmlString);


            if (sendMailResponse.Status ==
                (int)ASSendMailResponse.DownloadStatus.Success)
            {
                return clientId;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Error returned from Send Email: {0}", sendMailResponse.Status);
                return Guid.Empty;
            }
        }

        //Deletes mail first from server then send message to attendee            
        /// <summary>
        /// Deletes the calendar items.
        /// </summary>
        /// <param name="folderName">Name of the folder.</param>
        /// <param name="calServerIds">The cal server ids.</param>
        /// <param name="organizerName">Name of the organizer.</param>
        /// <param name="organizerEmail">The organizer email.</param>
        /// <param name="attendees">The attendees.</param>
        /// <param name="location">The location.</param>
        /// <param name="meetingStatus">The meeting status.</param>
        /// <param name="recurrence">The recurrence.</param>
        /// <param name="dtstamp">The dtstamp.</param>
        /// <param name="UID">The uid.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="content">The content.</param>
        /// <param name="startime">The startime.</param>
        /// <param name="endtime">The endtime.</param>
        /// <param name="replyContent">Content of the reply.</param>
        /// <param name="typeofDelete">The typeof delete. If we are deleting single instance of recurrent, value = 2 else it can be 0 or 1</param>
        /// <param name="startDateofdeletSingle">The start dateofdelet single.</param>
        /// <returns></returns>
        /// <exception cref="InvalidDataException">Account not yet created</exception>
        public async Task<ASDeleteItemsResponse> DeleteCalendarItems(string folderName, List<ASDeleteItemsRequest> calServerIds,
            string organizerName, string organizerEmail, List<EASAttendee> attendees, string location, MeetingStatusType meetingStatus,
            CMRecurrence recurrence, string dtstamp, string UID, string subject, string content, string startime, string endtime, string replyContent, int typeofDelete, double startDateofdeletSingle)
        {

            string userName = getUserNameOnly(App.mActiveSyncManager.Account);
            ASDeleteItemsRequest deleteItemsRequest = new ASDeleteItemsRequest();
            FolderManager folderManager = new FolderManager();
#if USE_ASYNC
            List<workspace_datastore.models.Folder> folderListDB = await folderManager.GetFolderInfoAsync(0);
#else
             List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfo(0);
#endif

            //Creating a ASPCL Inboxfolder, for second time Sync
            var folderDB = folderListDB.SingleOrDefault(x => x.displayName == "Calendar");

            ActiveSyncPCL.Folder ASFolder = null;
            if (folderDB != null)
            {
                ActiveSyncPCL.Folder.FolderType folderType = (ActiveSyncPCL.Folder.FolderType)folderDB.folderType;
                ASFolder = new ActiveSyncPCL.Folder(folderDB.displayName, folderDB.serverIdentifier, folderType, null);

                if (ASFolder != null)
                {
                    if (folderDB.syncKey != "1")
                        ASFolder.SyncKey = folderDB.syncKey;
                }
            }
            if (ASFolder.SyncKey != "0")
            {
                deleteItemsRequest.CollectionId = ASFolder.Id;
                deleteItemsRequest.SyncKey = ASFolder.SyncKey;
                deleteItemsRequest.Items = calServerIds;
                deleteItemsRequest.CommandType = EnumContactCommandType.Delete;
                // Initialize the request with information
                // that applies to all requests.
                deleteItemsRequest.Credentials = userCredentials;
                deleteItemsRequest.Server = App.mActiveSyncManager.Account.ServerUrl;
                deleteItemsRequest.User = userName;
                deleteItemsRequest.DeviceID = deviceToProvision.DeviceID;
                deleteItemsRequest.DeviceType = deviceToProvision.DeviceType;
                deleteItemsRequest.ProtocolVersion = ProtocolVersion;
                deleteItemsRequest.UserAgent = UserAgent;
                deleteItemsRequest.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
                deleteItemsRequest.UseEncodedRequestLine = useBase64RequestLine;
                ASDeleteItemsResponse deletedItemResponse = null;
                if (typeofDelete != 2)
                {
                    deletedItemResponse = await deleteItemsRequest.GetResponse() as ASDeleteItemsResponse;
                }
                if ((deletedItemResponse != null && deletedItemResponse.status == 1) || typeofDelete == 2)
                {
                    //  ASFolder.LastSyncTime = DateTime.Now;
                    //   ASFolder.SyncKey = deletedItemResponse.GetSyncKeyForFolder(ASFolder.Id);
                    //update sync key in database if folser is not contact or caledar
                    if (organizerEmail == App.mActiveSyncManager.Account.EmailAddress)
                    {
                        #region SendMail to attendees as event has been deleted

                        ASSendMailRequest sendMailRequest = new ASSendMailRequest();

                        ASSendMailResponse sendMailResponse = null;

                        if (App.mActiveSyncManager.Account == null)
                            throw new InvalidDataException("Account not yet created");


                        string replySubject = "";
                        string replySubjectCalendar = "";

                        // Initialize the request with information
                        // that applies to all requests.
                        sendMailRequest.Credentials = userCredentials;
                        sendMailRequest.Server = App.mActiveSyncManager.Account.ServerUrl;
                        sendMailRequest.User = userName;
                        sendMailRequest.DeviceID = deviceToProvision.DeviceID;
                        sendMailRequest.DeviceType = deviceToProvision.DeviceType;
                        sendMailRequest.ProtocolVersion = ProtocolVersion;
                        sendMailRequest.UserAgent = UserAgent;
                        sendMailRequest.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
                        sendMailRequest.UseEncodedRequestLine = useBase64RequestLine;

                        #region stringBuilder
#if OLDWAY
                        StringBuilder meetinmsg = new StringBuilder();
                        meetinmsg.AppendLine("BEGIN:VCALENDAR");
                        meetinmsg.AppendLine("PRODID:-//Dell/Dell Calendar V1.0/EN");
                        meetinmsg.AppendLine("VERSION:2.0");
                        meetinmsg.AppendLine("X-MS-OLK-FORCEINSPECTOROPEN:TRUE");
                        meetinmsg.AppendLine("METHOD:CANCEL");
                        meetinmsg.AppendLine("BEGIN:VEVENT");


                        foreach (EASAttendee attendee in attendees)
                        {
                            string line = string.Format("ATTENDEE;CN={0};RSVP=TRUE:mailto:{1}", attendee.Name, attendee.Email);
                            meetinmsg.AppendLine(line);
                        }

                        // meetinmsg.AppendLine("ATTENDEE;CN=Saurabh;RSVP=TRUE:mailto:saurabh_chandel@dell.com");                 
                        meetinmsg.AppendLine("CLASS:PUBLIC");
                        meetinmsg.AppendLine("CREATED:" + dtstamp);
                        meetinmsg.AppendLine("DTSTAMP:" + DateTimeOffset.Now.LocalDateTime.ToString("yyyyMMddThhmmssZ"));
                        meetinmsg.AppendLine("LAST-MODIFIED:" + DateTimeOffset.Now.LocalDateTime.ToString("yyyyMMddThhmmssZ"));
                        meetinmsg.AppendLine("LOCATION:" + location);
                        meetinmsg.AppendLine("ORGANIZER;CN=" + userName + ":mailto:" + App.mActiveSyncManager.Account.EmailAddress);
                        meetinmsg.AppendLine("PRIORITY:1");
                        meetinmsg.AppendLine("SEQUENCE:1");
                        meetinmsg.AppendLine("SUMMARY;LANGUAGE=en-us:Canceled:" + subject);
                        meetinmsg.AppendLine("TRANSP:TRANSPARENT");
                        meetinmsg.AppendLine("UID:" + UID);
                        meetinmsg.AppendLine("DESCRIPTION:" + content);
                        meetinmsg.AppendLine("DTSTART:" + startime);
                        meetinmsg.AppendLine("DTEND:" + endtime);
                        meetinmsg.AppendLine("X-MICROSOFT-CDO-BUSYSTATUS:FREE");
                        meetinmsg.AppendLine("X-MICROSOFT-CDO-IMPORTANCE:2");
                        meetinmsg.AppendLine("X-MICROSOFT-DISALLOW-COUNTER:FALSE");
                        meetinmsg.AppendLine("X-MS-OLK-ALLOWEXTERNCHECK:TRUE");
                        //meetinmsg.AppendLine("X-MS-OLK-APPTSEQTIME:20080208T174833Z");
                        meetinmsg.AppendLine("X-MS-OLK-AUTOSTARTCHECK:FALSE");
                        meetinmsg.AppendLine("X-MS-OLK-CONFTYPE:0");
                        meetinmsg.AppendLine("X-MS-OLK-SENDER;CN=" + userName + ":mailto:" + App.mActiveSyncManager.Account.EmailAddress);
                        meetinmsg.AppendLine("END:VEVENT");
                        meetinmsg.AppendLine("END:VCALEDNAR");
#else
                        StringBuilder meetingMsg = EncodeCalendarICSFormat(EventICSEncodingAction.ActionDeleteCalendarItems, organizerName, organizerEmail,
                            attendees, location, meetingStatus, recurrence, dtstamp, UID, subject, content, startime, endtime, false, "", EventStatus.None, typeofDelete, startDateofdeletSingle);
#endif
                        #endregion


                        byte[] meetingMessageAsBytes = Encoding.UTF8.GetBytes(meetingMsg.ToString());
                        string base64EncodedICS = Convert.ToBase64String(meetingMessageAsBytes);

                        StringBuilder emailBody = new StringBuilder();
                        StringBuilder emailBodyWithoutAttachments = new StringBuilder();
#if OLDWAY
                        emailBody.AppendLine("From: " + App.mActiveSyncManager.Account.EmailAddress);
                        string emailString = "";
                        foreach (EASAttendee attendee in attendees)
                        {
                            if (attendee.Name.Length > 0)
                            {
                                emailString += string.Format("\"{0}\"", attendee.Name);
                            }
                            emailString += attendee.Email;
                            if (attendee != attendees[attendees.Count - 1])
                            {
                                emailString += ",";
                            }
                        }
                        emailBody.AppendLine("To: " + emailString);
                        emailBody.AppendLine("Subject:" + subject);
                        emailBody.AppendLine("Mime-Version: 1.0");
                        emailBody.AppendLine("Content-Type: multipart/mixed; boundary=NextPart");
                        emailBody.AppendLine();

                        emailBody.AppendLine("--NextPart");
                        emailBody.AppendLine("Content-Type: text/plain; charset=\"iso-8859-1\"");
                        emailBody.AppendLine();
                        emailBody.AppendLine(replyContent);
                        emailBody.AppendLine();

                        emailBody.AppendLine("--NextPart");
                        emailBody.AppendLine("Content-Type: text/calendar;charset=utf-8; method=CANCEL; name=\"invite.ics\"");
                        emailBody.AppendLine("Content-Transfer-Encoding: base64");
                        emailBody.AppendLine();
                        emailBody.AppendLine(base64EncodedICS);
                        emailBody.AppendLine();
                        emailBody.AppendLine("--NextPart--");
#else
                        emailBody = EncodeEventMail(subject, content, "", attendees, base64EncodedICS, ref emailBodyWithoutAttachments);
#endif
                        sendMailRequest.MimeContent = emailBody;
                        sendMailRequest.ClientID = Guid.NewGuid().ToString();

                        if (null != ConstructOutgoingMessage)
                        {
                            // Until attachments are supported in a Calendar event, we can continue to use the emailBody string.  After
                            // that we'll have to use emailBodyWithoutAttachments instead of emailBody
                            ConstructOutgoingMessage(this, new ConstructOutgoingMessageEventArgs(sendMailRequest.ClientID, -1, emailBody, (int)MailActionType.ReferenceNone, null));
                        }

                        // Send the request
                        sendMailResponse = await sendMailRequest.GetResponse() as ASSendMailResponse;


                        #endregion
                    }
                    //if (ASFolder.Name.ToLower() == "contacs")
                    //{ 
                    //}
                    //else if (ASFolder.Name.ToLower() == "calendar")
                    //{

                    //}
                    //else
                    //{
                    //    await workspace.XMLParsing.ParsingUtils.ParseMailContent(null, null, null, null, ASFolder);
                    //}
                    // return deletedItemResponse;
                }
            }
            return null;
        }



        /// <summary>
        /// Attendee accept decline or tentative status updated to server
        /// sync to get it locally
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="responseRequest"></param>
        /// <returns></returns>
        public async Task<string> SendMeetingResponse(ActiveSyncPCL.Folder folder, ASMeetingResponseCommandRequest responseRequest)
        {
            string userName = getUserNameOnly(App.mActiveSyncManager.Account);
            string Status = "";
            // If this is the first time syncing Inbox
            // (Sync Key = 0), then you must send an initial sync request to "prime" the sync state.
            try
            {
                if (folder.SyncKey != "0")
                {
                    // Initialize the request with information
                    // that applies to all requests.
                    responseRequest.Credentials = userCredentials;
                    responseRequest.Server = App.mActiveSyncManager.Account.ServerUrl;
                    responseRequest.User = userName;
                    responseRequest.DeviceID = deviceToProvision.DeviceID;
                    responseRequest.DeviceType = deviceToProvision.DeviceType;
                    responseRequest.ProtocolVersion = ProtocolVersion;
                    responseRequest.UserAgent = UserAgent;
                    responseRequest.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
                    responseRequest.UseEncodedRequestLine = useBase64RequestLine;
                    ASMeetingResponseCommandResponse meetingResponse = await responseRequest.GetResponse() as ASMeetingResponseCommandResponse;

                    if (meetingResponse != null)
                    {
                        StringBuilder output = new StringBuilder();
                        string xmlString =
                         meetingResponse.XmlString;

                        XDocument doc = XDocument.Parse(xmlString);
                        string ns = "{MeetingResponse}";
                        IEnumerable<XElement> collection = doc.Descendants().Where(p => p.Name == (ns + "Result")).ToList();
                        if (collection != null)
                        {
                            foreach (XElement coll in collection)
                            {
                                XElement j = coll as XElement;
                                if (j != null)
                                {
                                    XElement collNode = j.Descendants().First(p => p.Name == (ns + "Status"));
                                    Status = collNode.Value;
                                }
                            }
                        }
                    }

                    if (Status != "1")
                    {
                        LoggingWP8.WP8Logger.LogMessage("Error in communication with server" + Status);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingWP8.WP8Logger.LogMessage("Exception in SendMeetingResponse" + ex.ToString());
            }
            return Status;
        }

        public async Task<Guid> SendMailtoOrganizerMail(string organizerName, string organizerEmail,
            List<EASAttendee> allAttendees, string location, MeetingStatusType meetingStatus,
            CMRecurrence recurrence, string dtstamp, string UID, string subject, string content,
            string startime, string endtime, EventStatus evntStatus, string replyContent, int singleEventofReoccuring, double singleEventofReoccuringstartDayAndTime)
        {
            ASSendMailRequest sendMailRequest = new ASSendMailRequest();

            ASSendMailResponse sendMailResponse = null;

            if (App.mActiveSyncManager.Account == null)
                throw new InvalidDataException("Account not yet created");

            string userName = getUserNameOnly(App.mActiveSyncManager.Account);
            string replySubject = "";
            string replySubjectCalendar = "";
            if ((int)evntStatus == 3)
            {
                replySubject = "Accepted";
                replySubjectCalendar = "ACCEPTED";
            }
            if ((int)evntStatus == 2)
            {
                replySubject = "Tentative";
                replySubjectCalendar = "TENTATIVE";
            }
            if ((int)evntStatus == 4)
            {
                replySubject = "Declined";
                replySubjectCalendar = "DECLINED";
            }
            // Initialize the request with information
            // that applies to all requests.
            sendMailRequest.Credentials = userCredentials;
            sendMailRequest.Server = App.mActiveSyncManager.Account.ServerUrl;
            sendMailRequest.User = userName;
            sendMailRequest.DeviceID = deviceToProvision.DeviceID;
            sendMailRequest.DeviceType = deviceToProvision.DeviceType;
            sendMailRequest.ProtocolVersion = ProtocolVersion;
            sendMailRequest.UserAgent = UserAgent;
            sendMailRequest.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
            sendMailRequest.UseEncodedRequestLine = useBase64RequestLine;

            #region stringBuilder
#if OLDWAY
            StringBuilder meetinmsg = new StringBuilder();
            meetinmsg.AppendLine("BEGIN:VCALENDAR");
            meetinmsg.AppendLine("PRODID:-//Dell/Dell Calendar V1.0/EN");
            meetinmsg.AppendLine("VERSION:2.0");
            meetinmsg.AppendLine("X-MS-OLK-FORCEINSPECTOROPEN:TRUE");
            meetinmsg.AppendLine("METHOD:REPLY");

            meetinmsg.AppendLine("BEGIN:VEVENT");
            meetinmsg.AppendLine("ATTENDEE;PARTSTAT=" + replySubjectCalendar + ":MAILTO:" + App.mActiveSyncManager.Account.EmailAddress);//need to send the attendies email ID rather than organizerEmail.
            meetinmsg.AppendLine("CLASS:PUBLIC");
            meetinmsg.AppendLine("CREATED:" + dtstamp);
            meetinmsg.AppendLine("DTSTAMP:" + DateTimeOffset.Now.LocalDateTime.ToString("yyyyMMddThhmmssZ"));
            meetinmsg.AppendLine("LAST-MODIFIED:" + DateTimeOffset.Now.LocalDateTime.ToString("yyyyMMddThhmmssZ"));
            meetinmsg.AppendLine("UID:" + UID);
            meetinmsg.AppendLine("SUMMARY:" + "Accepted: Lunch?");
            meetinmsg.AppendLine("LOCATION:" + location);
            //meetinmsg.AppendLine("DESCRIPTION:" + content);
            meetinmsg.AppendLine("DTSTART:" + startime);
            meetinmsg.AppendLine("DTEND:" + endtime);
            meetinmsg.AppendLine("X-MICROSOFT-CDO-BUSYSTATUS:BUSY");
            meetinmsg.AppendLine("X-MICROSOFT-CDO-IMPORTANCE:1");
            meetinmsg.AppendLine("X-MS-OLK-AUTOFILLLOCATION:FALSE");
            meetinmsg.AppendLine("X-MS-OLK-CONFTYPE:0");
            meetinmsg.AppendLine("PRIORITY:5");  // Need to update this
            meetinmsg.AppendLine("TRANSP:OPAQUE");
            meetinmsg.AppendLine("SEQUENCE:0");
            meetinmsg.AppendLine("END:VEVENT");
            meetinmsg.AppendLine("END:VCALEDNAR");
            //meetinmsg.AppendLine("BEGIN:VCALENDAR");
            //meetinmsg.AppendLine("PRODID:-//Dell/Dell Calendar V1.0/EN");
            //meetinmsg.AppendLine("VERSION:2.0");
            //meetinmsg.AppendLine("X-MS-OLK-FORCEINSPECTOROPEN:TRUE");
            //meetinmsg.AppendLine("METHOD:REPLY");

            //meetinmsg.AppendLine("BEGIN:VEVENT");
            //meetinmsg.AppendLine("ATTENDEE;PARTSTAT=ACCEPTED:mailto:saurabh_chandel@dell.com");
            //meetinmsg.AppendLine("CLASS:PUBLIC");
            //meetinmsg.AppendLine("CREATED:20150208T174434Z");
            //meetinmsg.AppendLine("DTSTAMP:20150208T174434Z");
            //meetinmsg.AppendLine("LAST-MODIFIED:20150208T174439Z");
            //meetinmsg.AppendLine("UID:" + UID);
            //meetinmsg.AppendLine("SUMMARY:" + "Accepted: Lunch?");
            //meetinmsg.AppendLine("LOCATION:Fourth");
            ////meetinmsg.AppendLine("DESChttp://msdn.microsoft.com/en-us/library/ee159544(v=exchg.80).aspxRIPTION:" + content);
            //meetinmsg.AppendLine("DTSTART:20150208T200000Z");
            //meetinmsg.AppendLine("DTEND:20150208T203000Z");
            //meetinmsg.AppendLine("X-MICROSOFT-CDO-BUSYSTATUS:BUSY");
            //meetinmsg.AppendLine("X-MICROSOFT-CDO-IMPORTANCE:1");
            //meetinmsg.AppendLine("X-MS-OLK-AUTOFILLLOCATION:FALSE");
            //meetinmsg.AppendLine("X-MS-OLK-CONFTYPE:0");
            //meetinmsg.AppendLine("PRIORITY:5");  // Need to update this
            //meetinmsg.AppendLine("TRANSP:OPAQUE");
            //meetinmsg.AppendLine("SEQUENCE:0");
            //meetinmsg.AppendLine("END:VEVENT");
            //meetinmsg.AppendLine("END:VCALEDNAR");
#else
            StringBuilder meetingMsg = EncodeCalendarICSFormat(EventICSEncodingAction.ActionSendMailToOrganizer, organizerName, organizerEmail, allAttendees,
                location, meetingStatus, recurrence, dtstamp, UID, subject, content,
                startime, endtime, false, "", evntStatus, singleEventofReoccuring, singleEventofReoccuringstartDayAndTime);
#endif
            #endregion
            byte[] meetingMessageAsBytes = Encoding.UTF8.GetBytes(meetingMsg.ToString());
            string base64EncodedICS = Convert.ToBase64String(meetingMessageAsBytes);

            StringBuilder emailBody = new StringBuilder();
#if OLDWAY
            emailBody.AppendLine("From: " + App.mActiveSyncManager.Account.EmailAddress);
            string emailString = "";
            foreach (EASAttendee attendee in allAttendees)
            {
                if (attendee.Name.Length > 0)
                {
                    emailString += string.Format("\"{0}\"", attendee.Name);
                }
                emailString += attendee.Email;
                if (attendee != allAttendees[allAttendees.Count - 1])
                {
                    emailString += ",";
                }
            }
            emailBody.AppendLine("To: " + organizerEmail);
            emailBody.AppendLine("Subject: " + replySubject + ":" + subject);
            emailBody.AppendLine("Mime-Version: 1.0");
            emailBody.AppendLine("Content-Type: multipart/mixed; boundary=NextPart");
            emailBody.AppendLine();

            emailBody.AppendLine("--NextPart");
            emailBody.AppendLine("Content-Type: text/plain; charset=\"iso-8859-1\"");
            emailBody.AppendLine();
            emailBody.AppendLine(replyContent);
            emailBody.AppendLine();

            emailBody.AppendLine("--NextPart");
            emailBody.AppendLine("Content-Type: text/calendar;charset=utf-8; method=REPLY; name=\"invite.ics\"");
            emailBody.AppendLine("Content-Transfer-Encoding: base64");
            emailBody.AppendLine();
            emailBody.AppendLine(base64EncodedICS);
            emailBody.AppendLine();
            emailBody.AppendLine("--NextPart--");
#else
            StringBuilder emailStringWithoutAttachments = new StringBuilder();
            emailBody = EncodeEventMail(replySubject + ":" + subject, content, "", allAttendees, base64EncodedICS, ref emailStringWithoutAttachments);
#endif
            sendMailRequest.MimeContent = emailBody;
            Guid clientId = Guid.NewGuid();
            sendMailRequest.ClientID = clientId.ToString();

            if (null != ConstructOutgoingMessage)
            {
                // Until attachments are supported in a Calendar event, we can continue to use the emailBody string.  After
                // that we'll have to use emailBodyWithoutAttachments instead of emailBody
                ConstructOutgoingMessage(this, new ConstructOutgoingMessageEventArgs(clientId.ToString(), -1, emailBody, (int)MailActionType.ReferenceNone, null));
            }

            // Send the request
            sendMailResponse = await sendMailRequest.GetResponse() as ASSendMailResponse;

            if (sendMailResponse == null)
            {
                System.Diagnostics.Debug.WriteLine("Unable to connect to server.");
                return Guid.Empty;
            }

            System.Diagnostics.Debug.WriteLine("SendMail Request:");
            System.Diagnostics.Debug.WriteLine(sendMailRequest.XmlString);
            System.Diagnostics.Debug.WriteLine("SendMail Response:");
            System.Diagnostics.Debug.WriteLine(sendMailResponse.XmlString);


            if (sendMailResponse.Status ==
                (int)ASSendMailResponse.DownloadStatus.Success)
            {
                return clientId;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Error returned from Send Email: {0}", sendMailResponse.Status);
                return Guid.Empty;
            }
        }

        public string GetCalnedarString(string organizerName, string organizerEmail,
        List<EASAttendee> attendees, string location, MeetingStatusType meetingStatus,
        CMRecurrence recurrence, string dtstamp, string UID, string subject, string content, string startime, string endtime, EventStatus evntStatus, string replyContent)
        {
            ASSendMailRequest sendMailRequest = new ASSendMailRequest();
            if (App.mActiveSyncManager.Account == null)
                throw new InvalidDataException("Account not yet created");

            string userName = getUserNameOnly(App.mActiveSyncManager.Account);
            string replySubject = "";
            string replySubjectCalendar = "";
            if ((int)evntStatus == 3)
            {
                replySubject = "Accepted";
                replySubjectCalendar = "ACCEPTED";
            }
            if ((int)evntStatus == 2)
            {
                replySubject = "Tentative";
                replySubjectCalendar = "TENTATIVE";
            }
            if ((int)evntStatus == 4)
            {
                replySubject = "Declined";
                replySubjectCalendar = "DECLINED";
            }
            // Initialize the request with information
            // that applies to all requests.
            sendMailRequest.Credentials = userCredentials;
            sendMailRequest.Server = App.mActiveSyncManager.Account.ServerUrl;
            sendMailRequest.User = userName;
            sendMailRequest.DeviceID = deviceToProvision.DeviceID;
            sendMailRequest.DeviceType = deviceToProvision.DeviceType;
            sendMailRequest.ProtocolVersion = ProtocolVersion;
            sendMailRequest.UserAgent = UserAgent;

            sendMailRequest.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
            sendMailRequest.UseEncodedRequestLine = useBase64RequestLine;
            #region stringBuilder

            StringBuilder meetinmsg = new StringBuilder();
            meetinmsg.AppendLine("BEGIN:VCALENDAR");
            meetinmsg.AppendLine("PRODID:-//Dell/Dell Calendar V1.0/EN");
            meetinmsg.AppendLine("VERSION:2.0");
            meetinmsg.AppendLine("X-MS-OLK-FORCEINSPECTOROPEN:TRUE");
            meetinmsg.AppendLine("METHOD:REPLY");

            meetinmsg.AppendLine("BEGIN:VEVENT");
            meetinmsg.AppendLine("ATTENDEE;PARTSTAT=" + replySubjectCalendar + ":MAILTO:" + organizerEmail);
            meetinmsg.AppendLine("CLASS:PUBLIC");
            meetinmsg.AppendLine("CREATED:" + dtstamp);
            meetinmsg.AppendLine("DTSTAMP:" + DateTimeOffset.Now.LocalDateTime.ToString("yyyyMMddThhmmssZ"));
            meetinmsg.AppendLine("LAST-MODIFIED:" + DateTimeOffset.Now.LocalDateTime.ToString("yyyyMMddThhmmssZ"));
            meetinmsg.AppendLine("UID:" + UID);
            meetinmsg.AppendLine("SUMMARY:" + "Accepted: Lunch?");
            meetinmsg.AppendLine("LOCATION:" + location);
            //meetinmsg.AppendLine("DESCRIPTION:" + content);

            meetinmsg.AppendLine("DTSTART:" + startime);
            meetinmsg.AppendLine("DTEND:" + endtime);
            meetinmsg.AppendLine("X-MICROSOFT-CDO-BUSYSTATUS:BUSY");
            meetinmsg.AppendLine("X-MICROSOFT-CDO-IMPORTANCE:1");
            meetinmsg.AppendLine("X-MS-OLK-AUTOFILLLOCATION:FALSE");
            meetinmsg.AppendLine("X-MS-OLK-CONFTYPE:0");
            meetinmsg.AppendLine("PRIORITY:5");  // Need to update this
            meetinmsg.AppendLine("TRANSP:OPAQUE");
            meetinmsg.AppendLine("SEQUENCE:0");
            meetinmsg.AppendLine("END:VEVENT");
            meetinmsg.AppendLine("END:VCALEDNAR");

            #endregion
            byte[] meetingMessageAsBytes = Encoding.UTF8.GetBytes(meetinmsg.ToString());
            string base64EncodedICS = Convert.ToBase64String(meetingMessageAsBytes);

            return base64EncodedICS;

        }

//        public async Task<Guid> SendMailWithResponse(workspace_datastore.models.Message message, List<workspace.WorkspaceEmailWP8.ViewModel.AttachmentDetail> attachmentDetailList, string calendarString)
//        {
//            //Pankaj : We need to get the file Reference from UI/AppInfrastructure. Currently this is passed from test atmosphere.
//            ASSendMailRequest sendMailRequest = new ASSendMailRequest();

//            ASSendMailResponse sendMailResponse = null;

//            if (App.mActiveSyncManager.Account == null)
//                throw new InvalidDataException("Account not yet created");

//            StringBuilder emailString = new StringBuilder();
//#if OLDWAY
//            emailString.Append("From: " + message.fromEmail);
//            emailString.AppendLine();
//            emailString.AppendLine("To: " + message.toRecipientText);
//            emailString.AppendLine("Cc:" + message.ccRecipientText);
//            emailString.AppendLine("Bcc: " + message.bccRecipientText);
//            emailString.AppendLine("Subject: " + message.subject);
//            emailString.AppendLine("MIME-Version: 1.0");
//            emailString.AppendLine("Content-Type: multipart/mixed; boundary=boundarytext");
//            emailString.AppendLine();
//            bool isAttachment = false;


//            emailString.AppendLine("--boundarytext");
//            emailString.AppendLine("Content-Type: text/plain; charset=\"iso-8859-1\"");
//            emailString.AppendLine();
//            emailString.AppendLine(message.bodyText);
//            emailString.AppendLine();

//            emailString.AppendLine("--boundarytext");
//            emailString.AppendLine("Content-Type: text/calendar;charset=utf-8; method=REPLY; name=\"invite.ics\"");
//            emailString.AppendLine("Content-Transfer-Encoding: base64");
//            emailString.AppendLine();
//            emailString.AppendLine(calendarString);
//            emailString.AppendLine();

//            if (isAttachment)
//                emailString.AppendLine("--" + "boundarytext");
//            emailString.AppendLine("Content-Type: text/plain");
//            emailString.AppendLine("Content-Transfer-Encoding: 7bit");
//            emailString.AppendLine(MAIL_CONTENT_MIME_SPECIFIER);
//            emailString.AppendLine("");
//            emailString.AppendLine(message.bodyText);


//            //if (!isAttachment)
//            {
//                String mailSignature;
//                mAppStorageSetting.TryGetValue<string>(AppConstString.KEY_DEFAULT_SIGNATURE, out mailSignature);
//                emailString.AppendLine("");
//                emailString.AppendLine(mailSignature);
//            }

//            //adding attachment if any
//            //this data will come from mail screen. Please dont remove below commented code for time being

//            //emailString.AppendLine("Content-Type: text/plain; charset=\"iso-8859-1\"");
//            //emailString.AppendLine("Content-Transfer-Encoding: base64");
//            //emailString.AppendLine("Content-Disposition: attachment; filename=\"abc.txt\"");

//            if (attachmentDetailList != null && attachmentDetailList.Count > 0)
//            {
//                foreach (AttachmentDetail attachmentDetail in attachmentDetailList)
//                {
//                    emailString.AppendLine("--" + "boundarytext");
//                    emailString.AppendLine("Content-Type:" + attachmentDetail.contentType + ";" + "charset=" + attachmentDetail.charset + "");
//                    emailString.AppendLine("Content-Transfer-Encoding: " + attachmentDetail.contentEncoding);
//                    emailString.AppendLine("Content-Disposition: attachment; filename=" + attachmentDetail.name);
//                    emailString.AppendLine("MIME-Version: 1.0");
//                    emailString.AppendLine("");

//                    //This information from attachment object. currently doing from here as filepicker not implemented
//                    //byte[] byteArrayData = GetBytes("this is test Attachment");
//                    var attachmentData = System.Convert.ToBase64String(attachmentDetail.data);
//                    emailString.AppendLine(attachmentData);

//                    //End attachment
//                }
//            }


//            if (isAttachment)
//                emailString.AppendLine("--" + "boundarytext" + "--");
//#else
//            StringBuilder emailStringExcludingAttachmentContent = new StringBuilder();
//            emailString = EncodeMail(message, attachmentDetailList, ref emailStringExcludingAttachmentContent);
//#endif
//            Guid clientId = Guid.NewGuid();
//            sendMailRequest.ClientID = clientId.ToString();
//            sendMailRequest.MimeContent = emailString;

//            string userName = getUserNameOnly(App.mActiveSyncManager.Account);

//            // Initialize the request with information
//            // that applies to all requests.
//            sendMailRequest.Credentials = userCredentials;
//            sendMailRequest.Server = App.mActiveSyncManager.Account.ServerUrl;
//            sendMailRequest.User = userName;
//            sendMailRequest.DeviceID = deviceToProvision.DeviceID;
//            sendMailRequest.DeviceType = deviceToProvision.DeviceType;
//            sendMailRequest.ProtocolVersion = ProtocolVersion;
//            sendMailRequest.UserAgent = UserAgent;
//            sendMailRequest.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
//            sendMailRequest.UseEncodedRequestLine = useBase64RequestLine;

//            if (null != ConstructOutgoingMessage)
//            {
//                List<object> attachments = attachmentDetailList.Cast<object>().ToList();
//                ConstructOutgoingMessage(this, new ConstructOutgoingMessageEventArgs(clientId.ToString(), -1, emailString, (int)MailActionType.ReferenceReply, attachments));
//            }

//            // Send the request
//            sendMailResponse = await sendMailRequest.GetResponse() as ASSendMailResponse;

//            if (sendMailResponse == null)
//            {
//                System.Diagnostics.Debug.WriteLine("Unable to connect to server.");
//                return Guid.Empty;
//            }
//            else if (null != EASCommunicationsErrorEvent && sendMailResponse.HttpStatus != HttpStatusCode.OK)
//            {
//                EASCommunicationsErrorEvent(this, new EASCommunicationsErrorEventArgs(sendMailResponse));
//                return Guid.Empty;
//            }
//            else if (sendMailResponse.HttpStatus == HttpStatusCode.OK && sendMailResponse.Status == 0)
//            {
//                //SaveClientIDForSendMail(id);
//                return clientId;
//            }
//            else
//                return Guid.Empty;
//        }

        #endregion


        public override async Task<string> SendMeetingRequest(ActiveSyncPCL.Folder folder, ASMeetingRequest meetingrequest)
        {
            string userName = getUserNameOnly(App.mActiveSyncManager.Account);
            string serverId = string.Empty;
            string Status = string.Empty;
            try
            {
                // If this is the first time syncing Inbox
                // (Sync Key = 0), then you must send an initial sync request to "prime" the sync state.
                if (folder.SyncKey != "0")
                {
                    bool breakLoop = false;
                    bool changes = true;
                    bool hasError = false;
                    int iteration = 1;

                    ASMeetingResponse meetingResponse = null;
                    ASSyncRequest subsequentRequests = new ASSyncRequest();
                    do
                    {
                        meetingrequest.Credentials = subsequentRequests.Credentials = userCredentials;
                        meetingrequest.Server = subsequentRequests.Server = App.mActiveSyncManager.Account.ServerUrl;
                        meetingrequest.User = subsequentRequests.User = userName;
                        meetingrequest.DeviceID = subsequentRequests.DeviceID = deviceToProvision.DeviceID;
                        meetingrequest.DeviceType = subsequentRequests.DeviceType = deviceToProvision.DeviceType;
                        meetingrequest.ProtocolVersion = subsequentRequests.ProtocolVersion = ProtocolVersion;
                        meetingrequest.UserAgent = subsequentRequests.UserAgent = UserAgent;
                        meetingrequest.PolicyKey = subsequentRequests.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
                        meetingrequest.UseEncodedRequestLine = subsequentRequests.UseEncodedRequestLine = useBase64RequestLine;
                        
                        meetingrequest.WindowSize = 100;
                        folder.Options = new FolderSyncOptions();
                        folder.Options.BodyPreference = new BodyPreferences[1];
                        folder.Options.BodyPreference[0] = new BodyPreferences();

                        meetingrequest.SyncOptions = new FolderSyncOptions();
                        meetingrequest.SyncOptions.BodyPreference = new BodyPreferences[1];
                        meetingrequest.SyncOptions.BodyPreference[0] = new BodyPreferences();

                        meetingrequest.SyncOptions.BodyPreference[0].Type = folder.Options.BodyPreference[0].Type = BodyType.HTML;
                        meetingrequest.SyncOptions.BodyPreference[0].TruncationSize = folder.Options.BodyPreference[0].TruncationSize = 32768;
                        SyncFilterType syncWindow = SyncFilterType.TwoWeeksBack;
                        if (mAppStorageSetting.Values.ContainsKey(AppConstString.CALENDAR_SYNC_WINDOW))
                        {
                            object getOut = null;
                            CommonPCL.DKCNCalendarDays syncCalWindow = DKCNCalendarDays.CalendarDaysTwoWeeksBack;
                            if(mAppStorageSetting.Values.TryGetValue(AppConstString.CALENDAR_SYNC_WINDOW, out getOut))
                            {
                                // LWR: 05/01/2015: As of WP8.1 SDK Port this could cause system to crash
                                syncCalWindow = (DKCNCalendarDays)getOut;
                            }

                            if (syncCalWindow == DKCNCalendarDays.CalendarDaysNotSet)
                                syncWindow = SyncFilterType.NoFilter;
                            else if (syncCalWindow == DKCNCalendarDays.CalendarDaysTwoWeeksBack)
                                syncWindow = SyncFilterType.TwoWeeksBack;
                            else if (syncCalWindow == DKCNCalendarDays.CalendarDaysOneMonthBack)
                                syncWindow = SyncFilterType.OneMonthBack;
                            else if (syncCalWindow == DKCNCalendarDays.CalendarDaysThreeMonthsBack)
                                syncWindow = SyncFilterType.ThreeMonthsBack;
                            else if (syncCalWindow == DKCNCalendarDays.CalendarDaysSixMonthsBack)
                                syncWindow = SyncFilterType.SixMonthsBack;
                        }
                        meetingrequest.SyncOptions.FilterType = folder.Options.FilterType = syncWindow;

                        if (null != EASCommunicationsErrorEvent && hasError)
                        {
                            EASCommunicationsErrorEvent(this, new EASCommunicationsErrorEventArgs(meetingResponse));
                        }

                        folder.UseConversationMode = false;
                        folder.AreDeletesPermanent = true;
                        folder.AreChangesIgnored = false;
                        folder.WindowSize = 100;


                        subsequentRequests.Folders.Clear();
                        subsequentRequests.Folders.Add(folder);

                        // send the request to server
                        if (iteration == 1)
                        {
                            meetingResponse = await meetingrequest.GetResponse() as ASMeetingResponse;
                        }
                        else
                        {
                            ASSyncResponse resp = await subsequentRequests.GetResponse() as ASSyncResponse;
                            
                            meetingResponse = new ASMeetingResponse(resp, meetingrequest.CreateException);
                        }

                        if (meetingResponse == null)
                        {
                            return string.Empty;
                        }
                        else if(meetingResponse.Status == (int)ASSyncResponse.SyncStatus.Success)
                        {
                            Status = meetingResponse.Status.ToString();

                            List<ServerSyncCommand> iterationAdds = meetingResponse.GetServerSyncCommandsForFolder(folder.Id, meetingrequest.ApplicationDataNode, ServerSyncCommand.ServerSyncCommandType.Add);
                            List<ServerSyncCommand> iterationDeletes = meetingResponse.GetServerSyncCommandsForFolder(folder.Id, meetingrequest.ApplicationDataNode, ServerSyncCommand.ServerSyncCommandType.Delete);
                            List<ServerSyncCommand> iterationChanges = meetingResponse.GetServerSyncCommandsForFolder(folder.Id, meetingrequest.ApplicationDataNode, ServerSyncCommand.ServerSyncCommandType.Change);
                            List<ServerSyncCommand> iterationSoftDeletes = meetingResponse.GetServerSyncCommandsForFolder(folder.Id, meetingrequest.ApplicationDataNode, ServerSyncCommand.ServerSyncCommandType.SoftDelete);

                            //Checking Moreavailable tag in response
                            bool moreitems = meetingResponse.IsMoreAvailable(folder.Id);

                            if (!moreitems)
                            {
                                breakLoop = true;
                            }

                            if (iteration == 1)
                            {
                                if (iterationAdds != null && iterationAdds.Count > 0)
                                {
                                    serverId = iterationAdds.FirstOrDefault().ServerId;
                                }
                            }

                            folder.SyncKey = meetingResponse.GetSyncKeyForFolder(folder.Id);
                            folder.LastSyncTime = DateTime.Now;

                            if (null != EASCollectionChangedEvent)
                            {
                                // When adding a calendar event, we get a sync response
                                EASCollectionChangedEvent(this, new EASCollectionChangedEventArgs(iterationAdds, iterationChanges, iterationDeletes, iterationSoftDeletes, folder, EASSyncFlags.SyncForEventCreation, null));
                            }
                            meetingResponse.Dispose();
                            iteration += 1;
                            if(moreitems)
                            {
                                subsequentRequests = new ASSyncRequest();
                            }
                        }
                        else if (meetingResponse.HttpStatus == HttpStatusCode.OK)
                        {

                            // The server had no changes
                            // to send. At this point you could
                            // repeat the last sync request by sending
                            // an empty sync request (MS-ASCMD section 2.2.2.19.1.1)
                            // This sample does not implement repeating the request,
                            // so just notify the user.
                            folder.LastSyncTime = DateTime.Now;

                            // Call SaveFolderInfo to update the last sync time
                            breakLoop = true;

                            changes = true;
                            meetingResponse.Dispose();
                            iteration += 1;
                        }
                        else if (meetingResponse.Status == 0)
                        {
                            hasError = true;
                            changes = false;
                            breakLoop = true;
                            meetingResponse.Dispose();
                            iteration += 1;
                        }
                        else
                        {
                            changes = false;
                            breakLoop = true;
                            meetingResponse.Dispose();
                            iteration += 1;
                        }

                        if (Status != "1")
                        {
                            LoggingWP8.WP8Logger.LogMessage("Error in communication with server" + Status);
                        }
                    } while (!breakLoop);
                }

                return serverId + ";" + Status;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Sends the mailto attendess mail.
        /// </summary>
        /// <param name="organizerName">Name of the organizer.</param>
        /// <param name="organizerEmail">The organizer email.</param>
        /// <param name="attendees">The attendees.</param>
        /// <param name="location">The location.</param>
        /// <param name="meetingStatus">The meeting status.</param>
        /// <param name="recurrence">The recurrence.</param>
        /// <param name="dtstamp">The dtstamp.</param>
        /// <param name="UID">The uid.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="content">The content.</param>
        /// <param name="startime">The startime.</param>
        /// <param name="endtime">The endtime.</param>
        /// <param name="reminderMinute">The reminder minute.</param>
        /// <param name="allDay">if set to <c>true</c> [all day].</param>
        /// <param name="singleEventofReoccuring">The single eventof reoccuring.value=2 signifies single instance of recurring event</param>
        /// <param name="singleEventofReoccuringstartDayAndTime">The single eventof reoccuringstart day and time.</param>
        /// <returns></returns>
        /// <exception cref="InvalidDataException">Account not yet created</exception>
        public async Task<Guid> SendMailtoAttendessMail(string organizerName, string organizerEmail,
            List<EASAttendee> attendees, string location, MeetingStatusType meetingStatus,
            CMRecurrence recurrence, string dtstamp, string UID, string subject, string content, string startime, string endtime, string reminderMinute, bool allDay, int singleEventofReoccuring, double singleEventofReoccuringstartDayAndTime)
        {
            ASSendMailRequest sendMailRequest = new ASSendMailRequest();

            ASSendMailResponse sendMailResponse = null;

            if (App.mActiveSyncManager.Account == null)
                throw new InvalidDataException("Account not yet created");

            string userName = getUserNameOnly(App.mActiveSyncManager.Account);

            // Initialize the request with information
            // that applies to all requests.
            sendMailRequest.Credentials = userCredentials;
            sendMailRequest.Server = App.mActiveSyncManager.Account.ServerUrl;
            sendMailRequest.User = userName;
            sendMailRequest.DeviceID = deviceToProvision.DeviceID;
            sendMailRequest.DeviceType = deviceToProvision.DeviceType;
            sendMailRequest.ProtocolVersion = ProtocolVersion;
            sendMailRequest.UserAgent = UserAgent;
            sendMailRequest.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
            sendMailRequest.UseEncodedRequestLine = useBase64RequestLine;

            StringBuilder meetingMsg = new StringBuilder();
#if OLDWAY
            meetingMsg.AppendLine("BEGIN:VCALENDAR");
            meetingMsg.AppendLine("PRODID:-//Dell/Dell Calendar V1.0/EN");
            meetingMsg.AppendLine("VERSION:2.0");
            meetingMsg.AppendLine("CALSCALE:GREGORIAN");
            meetingMsg.AppendLine("METHOD:REQUEST");

            meetingMsg.AppendLine("BEGIN:VEVENT");
            meetingMsg.AppendLine("CREATED:" + dtstamp);
            meetingMsg.AppendLine("DTSTAMP:" + DateTimeOffset.Now.LocalDateTime.ToUniversalTime().ToString("yyyyMMddThhmmssZ"));
            meetingMsg.AppendLine("LAST-MODIFIED:" + DateTimeOffset.Now.LocalDateTime.ToUniversalTime().ToString("yyyyMMddThhmmssZ"));
            meetingMsg.AppendLine("UID:" + UID);
            meetingMsg.AppendLine("SUMMARY:" + subject);
            meetingMsg.AppendLine("LOCATION:" + location);
            meetingMsg.AppendLine("DESCRIPTION:" + content);
            if (allDay == false)
            {
                meetingMsg.AppendLine("DTSTART:" + startime);
                meetingMsg.AppendLine("DTEND:" + endtime);
            }
            else
            {
              //  for full day meet set from 12:00AM to !2:00 AM 
                meetingMsg.AppendLine("DTSTART:" + startime.Substring(0, 9) + "000000Z");
                Console.Write("DTSTART:" + startime.Substring(0, 9) + "000000Z");           
                DateTime endDate = DateTime.ParseExact(endtime, "yyyyMMddTHHmmssZ", CultureInfo.InvariantCulture).AddDays(1);
                string parsedEnddate = endDate.ToString("yyyyMMddThhmmssZ");
                meetingMsg.AppendLine("DTEND:" + parsedEnddate.Substring(0, 9) + "000000Z");            
            }
            string organizerLine = string.Format("ORGANIZER;CN={0};RSVP=TRUE;PARTSTAT=ACCEPTED;ROLE=CHAIR:mailto:{1}", organizerName, organizerEmail);
            meetingMsg.AppendLine(organizerLine);
            foreach (EASAttendee attendee in attendees)
            {
                string action = "NEEDS-ACTION";
                if (attendee.AttendeeStatus == EventStatus.Accepted)
                {
                    action = "ACCEPTED";
                }
                else if (attendee.AttendeeStatus == EventStatus.Declined)
                {
                    action = "DECLINED";
                }
                else if (attendee.AttendeeStatus == EventStatus.Tentative)
                {
                    action = "TENTATIVE";
                }
                string line = string.Format("ATTENDEE;CN={0};RSVP=TRUE;PARTSTAT={1};ROLE=REQ-PARTICIPANT:mailto:{2}", attendee.Name, action, attendee.Email);
                meetingMsg.AppendLine(line);

            }
            string status = "CONFIRMED";
            if (meetingStatus == MeetingStatusType.OrganizerCanceled || meetingStatus == MeetingStatusType.GuestCanceled)
            {
                status = "CANCELED";
            }

            if (null != recurrence)
            {
                // Build the recurrence String
                string type = "DAILY";
                string rule = "";
                switch (recurrence.RecurrenceType)
                {
                    case CMRecurrenceType.CMRecurrenceTypeWeekly:
                        type = "WEEKLY";
                        break;
                    case CMRecurrenceType.CMRecurrenceTypeMonthly:
                    case CMRecurrenceType.CMRecurrenceTypeMonthlyNDay:
                        type = "MONTHLY";
                        break;
                    case CMRecurrenceType.CMRecurrenceTypeYearly:
                    case CMRecurrenceType.CMRecurrenceTypeYearlyNDay:
                        type = "YEARLY";
                        break;
                    default:
                        break;
                }
                rule += "FREQ=" + type;

                if (recurrence.Interval > 0)
                {
                    rule += ";INTERVAL=" + recurrence.Interval.ToString();
                }

                if (null != recurrence.Until)
                {
                    rule += string.Format(";UNTIL={0}", recurrence.Until.ToString("yyyyMMddThhmmssZ"));
                }
                else if (recurrence.Occurences > 0)
                {
                    rule += ";COUNT=" + recurrence.Occurences;
                }
                if (recurrence.DayOfWeek > 0)
                {
                    List<string> days = new List<string>();
                    int d = recurrence.DayOfWeek;
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeSunday) > 0)
                    {
                        days.Add("SU");
                    }
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeMonday) > 0)
                    {
                        days.Add("MO");
                    }
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeTuesday) > 0)
                    {
                        days.Add("TU");
                    }
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeWednesday) > 0)
                    {
                        days.Add("WE");
                    }
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeThursday) > 0)
                    {
                        days.Add("TH");
                    }
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeFriday) > 0)
                    {
                        days.Add("FR");
                    }
                    if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeSaturday) > 0)
                    {
                        days.Add("SA");
                    }
                    if (days.Count > 0)
                    {
                        if (recurrence.WeekOfMonth > 0)
                        {
                            rule += ";BYDAY=" + recurrence.WeekOfMonth.ToString() + days[0];
                        }
                        else
                        {
                            rule += ";BYDAY=";
                            int countDay = 0;
                            foreach (string day in days)
                            {
                                rule += day;
                                if (countDay<days.Count-1)
                                {
                                    rule += ",";
                                    countDay += 1;
                                }
                            }
                        }
                    }
                    if (recurrence.DayOfMonth > 0)
                    {
                        rule += ";BYMONTHDAY=" + recurrence.DayOfMonth.ToString();
                    }
                    if (recurrence.MonthOfYear > 0)
                    {
                        rule += ";BYMONTH=" + recurrence.MonthOfYear.ToString();
                    }
                }
                meetingMsg.AppendLine("RRULE:" + rule);
            }
            meetingMsg.AppendLine("STATUS:" + status);  // Need to update this
            meetingMsg.AppendLine("TRANSP:OPAQUE");
            meetingMsg.AppendLine("SEQUENCE:0");
           //add reminder
            meetingMsg.AppendLine("BEGIN:VALARM");
            meetingMsg.AppendLine("TRIGGER:-PT" + reminderMinute + "M");
            meetingMsg.AppendLine("ACTION:DISPLAY");
            meetingMsg.AppendLine("DESCRIPTION:Reminder");
            meetingMsg.AppendLine("END:VALARM");
            meetingMsg.AppendLine("END:VEVENT");
            meetingMsg.AppendLine("END:VCALEDNAR");
#else
            meetingMsg = EncodeCalendarICSFormat(EventICSEncodingAction.ActionSendMailToAttendees, organizerName, organizerEmail, attendees,
                location, meetingStatus, recurrence, dtstamp, UID, subject, content,
                startime, endtime, allDay, reminderMinute, EventStatus.None, singleEventofReoccuring, singleEventofReoccuringstartDayAndTime);
#endif
            byte[] meetingMessageAsBytes = Encoding.UTF8.GetBytes(meetingMsg.ToString());
            string base64EncodedICS = Convert.ToBase64String(meetingMessageAsBytes);

            StringBuilder emailBody = new StringBuilder();
            StringBuilder emailBodyWithoutAttachments = new StringBuilder();
#if OLDWAY
            emailBody.AppendLine("From: " + organizerEmail);
            string emailString = "";
            foreach (EASAttendee attendee in attendees)
            {
                if (null != attendee.Name && attendee.Name.Length > 0)
                {
                    emailString += string.Format("\"{0}\"", attendee.Name);
                }
                emailString += attendee.Email;
                if (attendee != attendees[attendees.Count - 1])
                {
                    emailString += ",";
                }
            }
            emailBody.AppendLine("To: " + emailString);
            emailBody.AppendLine("Subject: " + subject);
            emailBody.AppendLine("Mime-Version: 1.0");
            emailBody.AppendLine("Content-Type: multipart/mixed; boundary=NextPart");
            emailBody.AppendLine();

            emailBody.AppendLine("--NextPart");
            emailBody.AppendLine("Content-Type: text/plain; charset=\"iso-8859-1\"");
            emailBody.AppendLine();
            emailBody.AppendLine(content);
            emailBody.AppendLine();

            emailBody.AppendLine("--NextPart");
            emailBody.AppendLine("Content-Type: text/calendar; method=REQUEST; name=\"invite.ics\"");
            emailBody.AppendLine("Content-Transfer-Encoding: base64");
            emailBody.AppendLine();
            emailBody.AppendLine(base64EncodedICS);
            emailBody.AppendLine();
            emailBody.AppendLine("--NextPart--");
#else
            emailBody = EncodeEventMail(subject, content, "", attendees, base64EncodedICS, ref emailBodyWithoutAttachments);
#endif
            sendMailRequest.MimeContent = emailBody;
            Guid clientId = Guid.NewGuid();
            sendMailRequest.ClientID = clientId.ToString();

            if (null != ConstructOutgoingMessage)
            {
                // Until attachments are supported in a Calendar event, we can continue to use the emailBody string.  After
                // that we'll have to use emailBodyWithoutAttachments instead of emailBody
                ConstructOutgoingMessage(this, new ConstructOutgoingMessageEventArgs(clientId.ToString(), -1, emailBody, (int)MailActionType.ReferenceNone, null));
            }

            // Send the request
            sendMailResponse = await sendMailRequest.GetResponse() as ASSendMailResponse;

            if (sendMailResponse == null)
            {
                System.Diagnostics.Debug.WriteLine("Unable to connect to server.");
                return Guid.Empty;
            }

            System.Diagnostics.Debug.WriteLine("SendMail Request:");
            System.Diagnostics.Debug.WriteLine(sendMailRequest.XmlString);
            System.Diagnostics.Debug.WriteLine("SendMail Response:");
            System.Diagnostics.Debug.WriteLine(sendMailResponse.XmlString);


            if (sendMailResponse.Status ==
                (int)ASSendMailResponse.DownloadStatus.Success)
            {
                return clientId;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Error returned from Send Email: {0}", sendMailResponse.Status);
                return Guid.Empty;
            }
        }
        public override async Task<ASMeetingResponseCommandResponse> SendMeetingResponseRequest(string userResponse, string collectionId, string requestId)
        {
            ASMeetingResponseCommandRequest request = new ASMeetingResponseCommandRequest();
            request.UserResponse = userResponse;
            request.CollectionId = collectionId;
            request.RequestId = requestId;
            if (App.mActiveSyncManager.Account == null)
                throw new InvalidDataException("Account not yet created");
            string userName = getUserNameOnly(App.mActiveSyncManager.Account);

            request.Credentials = userCredentials;
            request.Server = App.mActiveSyncManager.Account.ServerUrl;
            request.User = userName;
            request.DeviceID = deviceToProvision.DeviceID;
            request.DeviceType = deviceToProvision.DeviceType;
            request.ProtocolVersion = ProtocolVersion;
            request.UserAgent = UserAgent;
            request.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
            request.UseEncodedRequestLine = useBase64RequestLine;

            // Send the request
            ASMeetingResponseCommandResponse response = await request.GetResponse() as ASMeetingResponseCommandResponse;

            if (response != null && response.status == 1)
            {

            }
            return response;
        }

        //public async Task<Guid> ForwardMail(workspace_datastore.models.Message message, List<workspace.WorkspaceEmailWP8.ViewModel.AttachmentDetail> attachmentDetailList,
        //                                    string folderID, string itemID)
        //{
        //    //Pankaj : We need to get the file Reference from UI/AppInfrastructure. Currently this is passed from test atmosphere.
        //    ASSmartForwardRequest sendMailRequest = new ASSmartForwardRequest();

        //    ASSmartForwardResponse sendMailResponse = null;

        //    if (App.mActiveSyncManager.Account == null)
        //        throw new InvalidDataException("Account not yet created");

        //    StringBuilder emailString = new StringBuilder();
        //    emailString.Append("From: " + message.fromEmail);
        //    emailString.AppendLine();
        //    emailString.AppendLine("To: " + message.toRecipientText);
        //    emailString.AppendLine("Cc:" + message.ccRecipientText);
        //    emailString.AppendLine("Bcc: " + message.bccRecipientText);
        //    emailString.AppendLine("Subject: " + message.subject);
        //    emailString.AppendLine("MIME-Version: 1.0");

        //    bool isAttachment = false;
        //    if (attachmentDetailList != null && attachmentDetailList.Count > 0)
        //    {
        //        isAttachment = true;
        //        emailString.AppendLine("Content-Type: multipart/mixed; boundary=\"boundarytext\"");
        //    }

        //    if (isAttachment)
        //        emailString.AppendLine("--" + "boundarytext");
        //    emailString.AppendLine("Content-Type: text/plain");
        //    emailString.AppendLine("Content-Transfer-Encoding: 7bit");
        //    emailString.AppendLine(MAIL_CONTENT_MIME_SPECIFIER);
        //    emailString.AppendLine("");
        //    emailString.AppendLine(message.bodyText);

        //    //if (!isAttachment)
        //    //we need to add signature in UI instead of sending in email content
        //    //{
        //    //    String mailSignature;
        //    //    mAppStorageSetting.TryGetValue<string>(AppConstString.KEY_DEFAULT_SIGNATURE, out mailSignature);
        //    //    emailString.AppendLine("");
        //    //    emailString.AppendLine(mailSignature);
        //    //}

        //    //adding attachment if any
        //    //this data will come from mail screen. Please dont remove below commented code for time being

        //    //emailString.AppendLine("Content-Type: text/plain; charset=\"iso-8859-1\"");
        //    //emailString.AppendLine("Content-Transfer-Encoding: base64");
        //    //emailString.AppendLine("Content-Disposition: attachment; filename=\"abc.txt\"");

        //    if (attachmentDetailList != null && attachmentDetailList.Count > 0)
        //    {
        //        foreach (AttachmentDetail attachmentDetail in attachmentDetailList)
        //        {
        //            emailString.AppendLine("--" + "boundarytext");
        //            emailString.AppendLine("Content-Type:" + attachmentDetail.contentType + ";" + "charset=" + attachmentDetail.charset + "");
        //            emailString.AppendLine("Content-Transfer-Encoding: " + attachmentDetail.contentEncoding);
        //            emailString.AppendLine("Content-Disposition: attachment; filename=" + attachmentDetail.name);
        //            emailString.AppendLine("MIME-Version: 1.0");
        //            emailString.AppendLine("");

        //            //This information from attachment object. currently doing from here as filepicker not implemented
        //            //byte[] byteArrayData = GetBytes("this is test Attachment");
        //            var attachmentData = System.Convert.ToBase64String(attachmentDetail.data);
        //            emailString.AppendLine(attachmentData);

        //            //End attachment
        //        }
        //    }


        //    if (isAttachment)
        //        emailString.AppendLine("--" + "boundarytext" + "--");

        //    Guid clientId = Guid.NewGuid();
        //    sendMailRequest.ClientID = clientId.ToString();
        //    sendMailRequest.MimeContent = emailString;

        //    sendMailRequest.folderID = folderID;
        //    sendMailRequest.itemID = itemID;

        //    string userName = getUserNameOnly(App.mActiveSyncManager.Account);

        //    // Initialize the request with information
        //    // that applies to all requests.
        //    sendMailRequest.Credentials = userCredentials;
        //    sendMailRequest.Server = App.mActiveSyncManager.Account.ServerUrl;
        //    sendMailRequest.User = userName;
        //    sendMailRequest.DeviceID = deviceToProvision.DeviceID;
        //    sendMailRequest.DeviceType = deviceToProvision.DeviceType;
        //    sendMailRequest.ProtocolVersion = ProtocolVersion;
        //    sendMailRequest.UserAgent = UserAgent;
        //    sendMailRequest.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
        //    sendMailRequest.UseEncodedRequestLine = useBase64RequestLine;

        //    // Send the request
        //    sendMailResponse = await sendMailRequest.GetResponse() as ASSmartForwardResponse;

        //    if (sendMailResponse == null)
        //    {
        //        System.Diagnostics.Debug.WriteLine("Unable to connect to server.");
        //        return Guid.Empty;
        //    }
        //    else if (null != EASCommunicationsErrorEvent && sendMailResponse.HttpStatus != HttpStatusCode.OK)
        //    {
        //        EASCommunicationsErrorEvent(this, new EASCommunicationsErrorEventArgs(sendMailResponse));
        //        return Guid.Empty;
        //    }
        //    else if (sendMailResponse.HttpStatus == HttpStatusCode.OK && sendMailResponse.Status == 0)
        //    {
        //        //SaveClientIDForSendMail(id);
        //        return clientId;
        //    }
        //    else
        //        return Guid.Empty;

        //}

        /// <summary>
        /// Method that encodes calendar-event data into an "ICS" format
        /// TODO Need to go through each action and pull out common encoding to make this method simpler
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="organizerName">Name of the organizer.</param>
        /// <param name="organizerEmail">The organizer email.</param>
        /// <param name="allAttendees">All attendees.</param>
        /// <param name="location">The location.</param>
        /// <param name="meetingStatus">The meeting status.</param>
        /// <param name="recurrence">The recurrence.</param>
        /// <param name="dtstamp">The dtstamp.</param>
        /// <param name="UID">The uid.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="content">The content.</param>
        /// <param name="startime">The startime.</param>
        /// <param name="endtime">The endtime.</param>
        /// <param name="allDay">if set to <c>true</c> [all day].</param>
        /// <param name="reminderMinute">The reminder minute.</param>
        /// <param name="evntStatus">The evnt status.</param>
        /// <param name="singleEventofReoccuring">The single eventof reoccuring. value==2 for single instance modificaition of recurrent event</param>
        /// <param name="singleEventofReoccuringstartDayAndTime">The single eventof reoccuringstart day and time.</param>
        /// <returns></returns>
        public StringBuilder EncodeCalendarICSFormat(EventICSEncodingAction action, string organizerName, string organizerEmail,
          List<EASAttendee> allAttendees, string location, MeetingStatusType meetingStatus,
          CMRecurrence recurrence, string dtstamp, string UID, string subject, string content, string startime, string endtime,
            // action-specific params
            bool allDay,  // action = EventICSEncoding.ActionSendMailToAttendees
            string reminderMinute, // action = EventICSEncoding.ActionSendMailToAttendees
            EventStatus evntStatus // action = EventICSEncodingAction.ActionSendMailToOrganizer
            , int singleEventofReoccuring, double singleEventofReoccuringstartDayAndTime)
        {
            // Methods that use ICS encoding:
            //   ForwardMeetingtoAttendessMail
            //   DeleteCalendarItems
            //   SendMailtoOrganizerMail
            //   (GetCalnedarString) - method to retrieve Calendar ICS
            //   SendMailtoAttendessMail
            StringBuilder meetingMsg = new StringBuilder();

            string organizerLine = "";
            string status = "";

            switch (action)
            {
                case EventICSEncodingAction.ActionSendMailToAttendees:
                    #region Original or editing event
                    meetingMsg.AppendLine("BEGIN:VCALENDAR");
                    meetingMsg.AppendLine("PRODID:-//Dell/Dell Calendar V1.0/EN");
                    meetingMsg.AppendLine("VERSION:2.0");
                    meetingMsg.AppendLine("CALSCALE:GREGORIAN");
                    meetingMsg.AppendLine("METHOD:REQUEST");

                    meetingMsg.AppendLine("BEGIN:VEVENT");
                    meetingMsg.AppendLine("CREATED:" + dtstamp);
                    if (singleEventofReoccuring == 2)
                    {
                        meetingMsg.AppendLine("RECURRENCE-ID:" + DateTimeExtensions.FromOADate(singleEventofReoccuringstartDayAndTime).ToUniversalTime().ToString("yyyyMMddTHHmmssZ"));
                    }
                    meetingMsg.AppendLine("DTSTAMP:" + DateTimeOffset.Now.LocalDateTime.ToUniversalTime().ToString("yyyyMMddThhmmssZ"));
                    meetingMsg.AppendLine("LAST-MODIFIED:" + DateTimeOffset.Now.LocalDateTime.ToUniversalTime().ToString("yyyyMMddThhmmssZ"));
                    meetingMsg.AppendLine("UID:" + UID);
                    meetingMsg.AppendLine("SUMMARY:" + subject);
                    meetingMsg.AppendLine("LOCATION:" + location);
                    meetingMsg.AppendLine("DESCRIPTION:" + content);
                    if (allDay == false)
                    {
                        meetingMsg.AppendLine("DTSTART:" + startime);
                        meetingMsg.AppendLine("DTEND:" + endtime);
                    }
                    else
                    {
                        //  for full day meet set from 12:00AM to !2:00 AM 
                        meetingMsg.AppendLine("DTSTART:" + startime.Substring(0, 9) + "000000Z");
                        //Console.Write("DTSTART:" + startime.Substring(0, 9) + "000000Z");
                        DateTime endDate = DateTime.ParseExact(endtime, "yyyyMMddTHHmmssZ", CultureInfo.InvariantCulture).AddDays(1);
                        string parsedEnddate = endDate.ToString("yyyyMMddThhmmssZ");
                        meetingMsg.AppendLine("DTEND:" + parsedEnddate.Substring(0, 9) + "000000Z");
                    }
                    organizerLine = string.Format("ORGANIZER;CN={0};RSVP=TRUE;PARTSTAT=ACCEPTED;ROLE=CHAIR:mailto:{1}", organizerName, organizerEmail);
                    meetingMsg.AppendLine(organizerLine);
                    foreach (EASAttendee attendee in allAttendees)
                    {
                        string actionString = "NEEDS-ACTION";
                        if (attendee.AttendeeStatus == EventStatus.Accepted)
                        {
                            actionString = "ACCEPTED";
                        }
                        else if (attendee.AttendeeStatus == EventStatus.Declined)
                        {
                            actionString = "DECLINED";
                        }
                        else if (attendee.AttendeeStatus == EventStatus.Tentative)
                        {
                            actionString = "TENTATIVE";
                        }
                        string line = string.Format("ATTENDEE;CN={0};RSVP=TRUE;PARTSTAT={1};ROLE=REQ-PARTICIPANT:mailto:{2}", attendee.Name, actionString, attendee.Email);
                        meetingMsg.AppendLine(line);

                    }
                    status = "CONFIRMED";
                    if (meetingStatus == MeetingStatusType.OrganizerCanceled || meetingStatus == MeetingStatusType.GuestCanceled)
                    {
                        status = "CANCELED";
                    }

                    if (null != recurrence)
                    {
                        // Build the recurrence String
                        string type = "DAILY";
                        string rule = "";
                        switch (recurrence.RecurrenceType)
                        {
                            case CMRecurrenceType.CMRecurrenceTypeWeekly:
                                type = "WEEKLY";
                                break;
                            case CMRecurrenceType.CMRecurrenceTypeMonthly:
                            case CMRecurrenceType.CMRecurrenceTypeMonthlyNDay:
                                type = "MONTHLY";
                                break;
                            case CMRecurrenceType.CMRecurrenceTypeYearly:
                            case CMRecurrenceType.CMRecurrenceTypeYearlyNDay:
                                type = "YEARLY";
                                break;
                            default:
                                break;
                        }
                        rule += "FREQ=" + type;

                        if (recurrence.Interval > 0)
                        {
                            rule += ";INTERVAL=" + recurrence.Interval.ToString();
                        }

                        if (null != recurrence.Until)
                        {
                            rule += string.Format(";UNTIL={0}", recurrence.Until.ToString("yyyyMMddThhmmssZ"));
                        }
                        else if (recurrence.Occurences > 0)
                        {
                            rule += ";COUNT=" + recurrence.Occurences;
                        }
                        if (recurrence.DayOfWeek > 0)
                        {
                            List<string> days = new List<string>();
                            int d = recurrence.DayOfWeek;
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeSunday) > 0)
                            {
                                days.Add("SU");
                            }
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeMonday) > 0)
                            {
                                days.Add("MO");
                            }
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeTuesday) > 0)
                            {
                                days.Add("TU");
                            }
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeWednesday) > 0)
                            {
                                days.Add("WE");
                            }
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeThursday) > 0)
                            {
                                days.Add("TH");
                            }
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeFriday) > 0)
                            {
                                days.Add("FR");
                            }
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeSaturday) > 0)
                            {
                                days.Add("SA");
                            }
                            if (days.Count > 0)
                            {
                                if (recurrence.WeekOfMonth > 0)
                                {
                                    rule += ";BYDAY=" + recurrence.WeekOfMonth.ToString() + days[0];
                                }
                                else
                                {
                                    rule += ";BYDAY=";
                                    int countDay = 0;
                                    foreach (string day in days)
                                    {
                                        rule += day;
                                        if (countDay < days.Count - 1)
                                        {
                                            rule += ",";
                                            countDay += 1;
                                        }
                                    }
                                }
                            }
                            if (recurrence.DayOfMonth > 0)
                            {
                                rule += ";BYMONTHDAY=" + recurrence.DayOfMonth.ToString();
                            }
                            if (recurrence.MonthOfYear > 0)
                            {
                                rule += ";BYMONTH=" + recurrence.MonthOfYear.ToString();
                            }
                        }
                        if (singleEventofReoccuring != 2)
                        {
                            meetingMsg.AppendLine("RRULE:" + rule);
                        }
                    }
                    meetingMsg.AppendLine("STATUS:" + status);  // Need to update this
                    meetingMsg.AppendLine("TRANSP:OPAQUE");
                    if (singleEventofReoccuring == 2)
                    {
                        meetingMsg.AppendLine("SEQUENCE:1");
                    }
                    else
                    {
                        meetingMsg.AppendLine("SEQUENCE:0");
                    }
                    //add reminder
                    meetingMsg.AppendLine("BEGIN:VALARM");
                    meetingMsg.AppendLine("TRIGGER:-PT" + reminderMinute + "M");
                    meetingMsg.AppendLine("ACTION:DISPLAY");
                    meetingMsg.AppendLine("DESCRIPTION:Reminder");
                    meetingMsg.AppendLine("END:VALARM");
                    meetingMsg.AppendLine("END:VEVENT");
                    meetingMsg.AppendLine("END:VCALEDNAR");
                    #endregion
                    break;

                case EventICSEncodingAction.ActionForwardMeetingToAttendees:
                    #region reduce scroll
                    meetingMsg.AppendLine("BEGIN:VCALENDAR");
                    meetingMsg.AppendLine("PRODID:-//Dell/Dell Calendar V1.0/EN");
                    meetingMsg.AppendLine("VERSION:2.0");
                    meetingMsg.AppendLine("CALSCALE:GREGORIAN");
                    meetingMsg.AppendLine("METHOD:REQUEST");    // Different

                    meetingMsg.AppendLine("BEGIN:VEVENT");
                    meetingMsg.AppendLine("CREATED:" + dtstamp);
                    meetingMsg.AppendLine("DTSTAMP:" + DateTimeOffset.Now.LocalDateTime.ToString("yyyyMMddThhmmssZ"));
                    meetingMsg.AppendLine("LAST-MODIFIED:" + DateTimeOffset.Now.LocalDateTime.ToString("yyyyMMddThhmmssZ"));
                    meetingMsg.AppendLine("UID:" + UID);
                    meetingMsg.AppendLine("SUMMARY:" + subject);
                    meetingMsg.AppendLine("LOCATION:" + location);
                    meetingMsg.AppendLine("DESCRIPTION:" + content);
                    meetingMsg.AppendLine("DTSTART:" + startime);
                    meetingMsg.AppendLine("DTEND:" + endtime);
                    organizerLine = string.Format("ORGANIZER;CN={0};RSVP=TRUE;PARTSTAT=ACCEPTED;ROLE=CHAIR:mailto:{1}", organizerName, organizerEmail);
                    meetingMsg.AppendLine(organizerLine);
                    foreach (EASAttendee attendee in allAttendees)
                    {
                        string actionString = "NEEDS-ACTION";
                        if (attendee.AttendeeStatus == EventStatus.Accepted)
                        {
                            actionString = "ACCEPTED";
                        }
                        else if (attendee.AttendeeStatus == EventStatus.Declined)
                        {
                            actionString = "DECLINED";
                        }
                        else if (attendee.AttendeeStatus == EventStatus.Tentative)
                        {
                            actionString = "TENTATIVE";
                        }
                        string line = string.Format("ATTENDEE;CN={0};RSVP=TRUE;PARTSTAT={1};ROLE=REQ-PARTICIPANT:mailto:{2}", attendee.Name, actionString, attendee.Email);
                        meetingMsg.AppendLine(line);

                    }
                    status = "CONFIRMED";
                    if (meetingStatus == MeetingStatusType.OrganizerCanceled || meetingStatus == MeetingStatusType.GuestCanceled)
                    {
                        status = "CANCELED";
                    }

                    if (null != recurrence)
                    {
                        // Build the recurrence String
                        string type = "DAILY";
                        string rule = "";
                        switch (recurrence.RecurrenceType)
                        {
                            case CMRecurrenceType.CMRecurrenceTypeWeekly:
                                type = "WEEKLY";
                                break;
                            case CMRecurrenceType.CMRecurrenceTypeMonthly:
                            case CMRecurrenceType.CMRecurrenceTypeMonthlyNDay:
                                type = "MONTHLY";
                                break;
                            case CMRecurrenceType.CMRecurrenceTypeYearly:
                            case CMRecurrenceType.CMRecurrenceTypeYearlyNDay:
                                type = "YEARLY";
                                break;
                            default:
                                break;
                        }
                        rule += "FREQ=" + type;

                        if (recurrence.Interval > 0)
                        {
                            rule += ";INTERVAL=" + recurrence.Interval.ToString();
                        }

                        if (null != recurrence.Until)
                        {
                            rule += string.Format(";UNTIL={0}", recurrence.Until.ToString("yyyyMMddThhmmssZ"));
                        }
                        else if (recurrence.Occurences > 0)
                        {
                            rule += ";COUNT=" + recurrence.Occurences;
                        }
                        if (recurrence.DayOfWeek > 0)
                        {
                            List<string> days = new List<string>();
                            int d = recurrence.DayOfWeek;
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeSunday) > 0)
                            {
                                days.Add("SU");
                            }
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeMonday) > 0)
                            {
                                days.Add("MO");
                            }
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeTuesday) > 0)
                            {
                                days.Add("TU");
                            }
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeWednesday) > 0)
                            {
                                days.Add("WE");
                            }
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeThursday) > 0)
                            {
                                days.Add("TH");
                            }
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeFriday) > 0)
                            {
                                days.Add("FR");
                            }
                            if ((d & (int)CMDayOfWeekType.CMDayOfWeekTypeSaturday) > 0)
                            {
                                days.Add("SA");
                            }
                            if (days.Count > 0)
                            {
                                if (recurrence.WeekOfMonth > 0)
                                {
                                    rule += ";BYDAY=" + recurrence.WeekOfMonth.ToString() + days[0];
                                }
                                else
                                {
                                    rule += ";BYDAY=";
                                    foreach (string day in days)
                                    {
                                        rule += day;
                                        if (day != days[days.Count])
                                        {
                                            rule += ",";
                                        }
                                    }
                                }
                            }
                            if (recurrence.DayOfMonth > 0)
                            {
                                rule += ";BYMONTHDAY=" + recurrence.DayOfMonth.ToString();
                            }
                            if (recurrence.MonthOfYear > 0)
                            {
                                rule += ";BYMONTH=" + recurrence.MonthOfYear.ToString();
                            }
                        }
                        meetingMsg.AppendLine(rule);
                    }
                    meetingMsg.AppendLine("STATUS:" + status);  // Need to update this
                    meetingMsg.AppendLine("TRANSP:OPAQUE");
                    meetingMsg.AppendLine("SEQUENCE:0");
                    meetingMsg.AppendLine("END:VEVENT");
                    meetingMsg.AppendLine("END:VCALEDNAR");
                    #endregion
                    break;

                case EventICSEncodingAction.ActionDeleteCalendarItems:
                    string userName = getUserNameOnly(App.mActiveSyncManager.Account);
                    meetingMsg.AppendLine("BEGIN:VCALENDAR");
                    meetingMsg.AppendLine("PRODID:-//Dell/Dell Calendar V1.0/EN");
                    meetingMsg.AppendLine("VERSION:2.0");
                    meetingMsg.AppendLine("X-MS-OLK-FORCEINSPECTOROPEN:TRUE");
                    meetingMsg.AppendLine("METHOD:CANCEL");
                    meetingMsg.AppendLine("BEGIN:VEVENT");

                    foreach (EASAttendee attendee in allAttendees)
                    {
                        string line = string.Format("ATTENDEE;CN={0};RSVP=TRUE:mailto:{1}", attendee.Name, attendee.Email);
                        meetingMsg.AppendLine(line);
                    }

                    // meetinmsg.AppendLine("ATTENDEE;CN=Saurabh;RSVP=TRUE:mailto:saurabh_chandel@dell.com");                 
                    meetingMsg.AppendLine("CLASS:PUBLIC");
                    meetingMsg.AppendLine("CREATED:" + dtstamp);
                    meetingMsg.AppendLine("DTSTAMP:" + DateTimeOffset.Now.LocalDateTime.ToUniversalTime().ToString("yyyyMMddThhmmssZ"));
                    meetingMsg.AppendLine("LAST-MODIFIED:" + DateTimeOffset.Now.LocalDateTime.ToUniversalTime().ToString("yyyyMMddThhmmssZ"));
                    meetingMsg.AppendLine("LOCATION:" + location);
                    meetingMsg.AppendLine("ORGANIZER;CN=" + userName + ":mailto:" + App.mActiveSyncManager.Account.EmailAddress);
                    meetingMsg.AppendLine("PRIORITY:1");
                    meetingMsg.AppendLine("SEQUENCE:1");
                    meetingMsg.AppendLine("SUMMARY;LANGUAGE=en-us:Canceled:" + subject);
                    meetingMsg.AppendLine("TRANSP:TRANSPARENT");
                    meetingMsg.AppendLine("UID:" + UID);
                    meetingMsg.AppendLine("DESCRIPTION:" + content);
                    meetingMsg.AppendLine("DTSTART:" + startime);
                    meetingMsg.AppendLine("DTEND:" + endtime);
                    if (singleEventofReoccuring == 2)
                    {
                        meetingMsg.AppendLine("RECURRENCE-ID:" + DateTimeExtensions.FromOADate(singleEventofReoccuringstartDayAndTime).ToUniversalTime().ToString("yyyyMMddThhmmssZ"));
                    }
                    meetingMsg.AppendLine("X-MICROSOFT-CDO-BUSYSTATUS:FREE");
                    meetingMsg.AppendLine("X-MICROSOFT-CDO-IMPORTANCE:2");
                    meetingMsg.AppendLine("X-MICROSOFT-DISALLOW-COUNTER:FALSE");
                    meetingMsg.AppendLine("X-MS-OLK-ALLOWEXTERNCHECK:TRUE");
                    //meetinmsg.AppendLine("X-MS-OLK-APPTSEQTIME:20080208T174833Z");
                    meetingMsg.AppendLine("X-MS-OLK-AUTOSTARTCHECK:FALSE");
                    meetingMsg.AppendLine("X-MS-OLK-CONFTYPE:0");
                    meetingMsg.AppendLine("X-MS-OLK-SENDER;CN=" + userName + ":mailto:" + App.mActiveSyncManager.Account.EmailAddress);
                    meetingMsg.AppendLine("END:VEVENT");
                    meetingMsg.AppendLine("END:VCALEDNAR");
                    break;

                case EventICSEncodingAction.ActionSendMailToOrganizer:
                    meetingMsg.AppendLine("BEGIN:VCALENDAR");
                    meetingMsg.AppendLine("PRODID:-//Dell/Dell Calendar V1.0/EN");
                    meetingMsg.AppendLine("VERSION:2.0");
                    meetingMsg.AppendLine("X-MS-OLK-FORCEINSPECTOROPEN:TRUE");
                    meetingMsg.AppendLine("METHOD:REPLY");

                    meetingMsg.AppendLine("BEGIN:VEVENT");
                    string replySubjectCalendar = "";
                    if ((int)evntStatus == 3)
                    {
                        replySubjectCalendar = "ACCEPTED";
                    }
                    if ((int)evntStatus == 2)
                    {
                        replySubjectCalendar = "TENTATIVE";
                    }
                    if ((int)evntStatus == 4)
                    {
                        replySubjectCalendar = "DECLINED";
                    }
                    meetingMsg.AppendLine("ATTENDEE;PARTSTAT=" + replySubjectCalendar + ":MAILTO:" + App.mActiveSyncManager.Account.EmailAddress);//need to send the attendies email ID rather than organizerEmail.
                    meetingMsg.AppendLine("CLASS:PUBLIC");
                    meetingMsg.AppendLine("CREATED:" + dtstamp);
                    meetingMsg.AppendLine("DTSTAMP:" + DateTimeOffset.Now.LocalDateTime.ToString("yyyyMMddThhmmssZ"));
                    meetingMsg.AppendLine("LAST-MODIFIED:" + DateTimeOffset.Now.LocalDateTime.ToString("yyyyMMddThhmmssZ"));
                    meetingMsg.AppendLine("UID:" + UID);
                    meetingMsg.AppendLine("SUMMARY:" + "Accepted: Lunch?");
                    meetingMsg.AppendLine("LOCATION:" + location);
                    //meetingMsg.AppendLine("DESCRIPTION:" + content);
                    meetingMsg.AppendLine("DTSTART:" + startime);
                    meetingMsg.AppendLine("DTEND:" + endtime);
                    meetingMsg.AppendLine("X-MICROSOFT-CDO-BUSYSTATUS:BUSY");
                    meetingMsg.AppendLine("X-MICROSOFT-CDO-IMPORTANCE:1");
                    meetingMsg.AppendLine("X-MS-OLK-AUTOFILLLOCATION:FALSE");
                    meetingMsg.AppendLine("X-MS-OLK-CONFTYPE:0");
                    meetingMsg.AppendLine("PRIORITY:5");  // Need to update this
                    meetingMsg.AppendLine("TRANSP:OPAQUE");
                    meetingMsg.AppendLine("SEQUENCE:0");
                    meetingMsg.AppendLine("END:VEVENT");
                    meetingMsg.AppendLine("END:VCALEDNAR");
                    //meetingMsg.AppendLine("BEGIN:VCALENDAR");
                    //meetingMsg.AppendLine("PRODID:-//Dell/Dell Calendar V1.0/EN");
                    //meetingMsg.AppendLine("VERSION:2.0");
                    //meetingMsg.AppendLine("X-MS-OLK-FORCEINSPECTOROPEN:TRUE");
                    //meetingMsg.AppendLine("METHOD:REPLY");

                    //meetingMsg.AppendLine("BEGIN:VEVENT");
                    //meetingMsg.AppendLine("ATTENDEE;PARTSTAT=ACCEPTED:mailto:saurabh_chandel@dell.com");
                    //meetingMsg.AppendLine("CLASS:PUBLIC");
                    //meetingMsg.AppendLine("CREATED:20150208T174434Z");
                    //meetingMsg.AppendLine("DTSTAMP:20150208T174434Z");
                    //meetingMsg.AppendLine("LAST-MODIFIED:20150208T174439Z");
                    //meetingMsg.AppendLine("UID:" + UID);
                    //meetingMsg.AppendLine("SUMMARY:" + "Accepted: Lunch?");
                    //meetingMsg.AppendLine("LOCATION:Fourth");
                    //meetingMsg.AppendLine("DESChttp://msdn.microsoft.com/en-us/library/ee159544(v=exchg.80).aspxRIPTION:" + content);
                    //meetingMsg.AppendLine("DTSTART:20150208T200000Z");
                    //meetingMsg.AppendLine("DTEND:20150208T203000Z");
                    //meetingMsg.AppendLine("X-MICROSOFT-CDO-BUSYSTATUS:BUSY");
                    //meetingMsg.AppendLine("X-MICROSOFT-CDO-IMPORTANCE:1");
                    //meetingMsg.AppendLine("X-MS-OLK-AUTOFILLLOCATION:FALSE");
                    //meetingMsg.AppendLine("X-MS-OLK-CONFTYPE:0");
                    //meetingMsg.AppendLine("PRIORITY:5");  // Need to update this
                    //meetingMsg.AppendLine("TRANSP:OPAQUE");
                    //meetingMsg.AppendLine("SEQUENCE:0");
                    //meetingMsg.AppendLine("END:VEVENT");
                    //meetingMsg.AppendLine("END:VCALEDNAR");
                    break;
                default:
                    break;
            }

            return meetingMsg;
        }

        /// <summary>
        /// Encodes an event email (No attachments).
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="content"></param>
        /// <param name="mailMessage"></param>
        /// <param name="newAttendees"></param>
        /// <param name="base64EncodedICS"></param>
        /// <param name="emailStringExcludingAttachmentContent">Unused until app supports Event attachments</param>
        /// <returns></returns>
        public StringBuilder EncodeEventMail(string subject, string content, string mailMessage, List<EASAttendee> newAttendees, string base64EncodedICS, ref StringBuilder emailStringExcludingAttachmentContent)
        {
            StringBuilder emailBody = new StringBuilder();
            emailBody.AppendLine("From: " + App.mActiveSyncManager.Account.EmailAddress);
            string emailString = "";
            foreach (EASAttendee attendee in newAttendees)
            {
                if (attendee.Name != null && attendee.Name.Length > 0)
                {
                    emailString += string.Format("\"{0}\"", attendee.Name);
                }
                emailString += attendee.Email;
                if (attendee != newAttendees[newAttendees.Count - 1])
                {
                    emailString += ",";
                }
            }
            emailBody.AppendLine("To: " + emailString);
            //Removing FW: text from subject, we will pass FW:,RE: with subject message
            emailBody.AppendLine("Subject: " + subject);
            emailBody.AppendLine("Mime-Version: 1.0");
            emailBody.AppendLine("Content-Type: multipart/mixed; boundary=NextPart");
            emailBody.AppendLine();

            emailBody.AppendLine("--NextPart");
            emailBody.AppendLine("Content-Type: text/plain; charset=\"iso-8859-1\"");
            emailBody.AppendLine();
            emailBody.AppendLine(mailMessage);
            emailBody.AppendLine();
            emailBody.AppendLine(content);
            emailBody.AppendLine();

            emailBody.AppendLine("--NextPart");
            emailBody.AppendLine("Content-Type: text/calendar; method=REQUEST; name=\"invite.ics\"");
            emailBody.AppendLine("Content-Transfer-Encoding: base64");
            emailBody.AppendLine();
            emailBody.AppendLine(base64EncodedICS);
            emailBody.AppendLine();
            emailBody.AppendLine("--NextPart--");

            return emailBody;
        }

        /// <summary>
        /// Encodes a regular Non-Event email.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="attachmentDetailList"></param>
        /// <param name="emailStringExcludingAttachmentContent">Encoded only message without attachment data</param>
        /// <returns></returns>
        //public StringBuilder EncodeMail(workspace_datastore.models.Message message,
        //    List<workspace.WorkspaceEmailWP8.ViewModel.AttachmentDetail> attachmentDetailList,
        //    ref StringBuilder emailStringExcludingAttachmentContent)
        //{
        //    StringBuilder emailString = new StringBuilder();

        //    if (message.fromEmail == null || message.fromEmail.Length <= 0)
        //    {
        //        emailString.AppendLine("From: " + App.mActiveSyncManager.Account.EmailAddress);

        //    }
        //    else
        //    {
        //        emailString.AppendLine("From: " + message.fromEmail);
        //    }
        //    emailString.AppendLine("To: " + message.toRecipientText);
        //    if (message.ccRecipientText != null && message.ccRecipientText.Length > 0 && message.ccRecipientText.ToLower() != "cc")
        //        emailString.AppendLine("Cc: " + message.ccRecipientText);
        //    if (message.bccRecipientText != null && message.bccRecipientText.Length > 0 && message.bccRecipientText.ToLower() != "bcc")
        //        emailString.AppendLine("Bcc: " + message.bccRecipientText);
        //    if (message.subject != null && message.subject.Length > 0 && message.subject.ToLower() != "subject:")
        //        emailString.AppendLine("Subject: " + message.subject);
        //    string priority = "normal";
        //    if (message.importance == 2)
        //    {
        //        priority = "high";
        //    }
        //    else if (message.importance == 0)
        //    {
        //        priority = "low";
        //    }
        //    emailString.AppendLine("Importance: " + priority);
        //    emailString.AppendLine("MIME-Version: 1.0");

        //    bool isAttachment = false;
        //    if (attachmentDetailList != null && attachmentDetailList.Count > 0)
        //    {
        //        isAttachment = true;
        //        emailString.AppendLine("Content-Type: multipart/mixed; boundary=\"boundarytext\"");
        //    }

        //    if (isAttachment)
        //        emailString.AppendLine("--" + "boundarytext");
        //    emailString.AppendLine("Content-Type: text/plain");
        //    emailString.AppendLine("Content-Transfer-Encoding: 7bit");
        //    emailString.AppendLine(MAIL_CONTENT_MIME_SPECIFIER);
        //    emailString.AppendLine("");
        //    emailString.AppendLine(message.bodyText);

        //    //if (!isAttachment)
        //    //we need to add signature in UI instead of sending in email content
        //    //{
        //    //    String mailSignature;
        //    //    mAppStorageSetting.TryGetValue<string>(AppConstString.KEY_DEFAULT_SIGNATURE, out mailSignature);
        //    //    emailString.AppendLine("");
        //    //    emailString.AppendLine(mailSignature);
        //    //}

        //    //adding attachment if any
        //    //this data will come from mail screen. Please dont remove below commented code for time being

        //    //emailString.AppendLine("Content-Type: text/plain; charset=\"iso-8859-1\"");
        //    //emailString.AppendLine("Content-Transfer-Encoding: base64");
        //    //emailString.AppendLine("Content-Disposition: attachment; filename=\"abc.txt\"");
        //    emailStringExcludingAttachmentContent.AppendLine(emailString.ToString());

        //    if (attachmentDetailList != null && attachmentDetailList.Count > 0)
        //    {
        //        foreach (AttachmentDetail attachmentDetail in attachmentDetailList)
        //        {
        //            // If the user sent an email with an attachment while network is down (Outbox)
        //            //  and before network comes back the attached file is deleted, don't send it
        //            if (null != attachmentDetail.data)
        //            {
        //                emailString.AppendLine("--" + "boundarytext");
        //                emailString.AppendLine("Content-Type:" + attachmentDetail.contentType + ";" + "charset=" + attachmentDetail.charset + "");
        //                emailString.AppendLine("Content-Transfer-Encoding: " + attachmentDetail.contentEncoding);
        //                emailString.AppendLine("Content-Disposition: attachment; filename=" + attachmentDetail.name);
        //                emailString.AppendLine("MIME-Version: 1.0");
        //                emailString.AppendLine("");

        //                var attachmentData = System.Convert.ToBase64String(attachmentDetail.data);
        //                emailString.AppendLine(attachmentData);
        //            }
        //        }
        //    }

        //    if (isAttachment)
        //        emailString.AppendLine("--" + "boundarytext" + "--");

        //    return emailString;
        //}

        public static workspace_datastore.models.Message DecodeMail(StringBuilder emailString)
        {
            workspace_datastore.models.Message mm = new workspace_datastore.models.Message();

            string[] items = emailString.ToString().Split('\n', '\r');
            int bodyTextMarkerIndex = emailString.ToString().IndexOf(MAIL_CONTENT_MIME_SPECIFIER);
            string boundaryText = "";
            foreach (string item in items)
            {
                if (item.Length > 0)
                {
                    if (item.ToLower().IndexOf("boundary=") != -1)
                    {
                        boundaryText = item.Substring(item.ToLower().IndexOf("boundary=") + "boundary=".Length);
                        boundaryText = boundaryText.Replace("\"", "");
                    }
                    if (item.IndexOf("From:") != -1)
                    {
                        mm.fromEmail = item.Substring("From:".Length);
                    }
                    else if (item.IndexOf("To:") != -1)
                    {
                        mm.toRecipientText = item.Substring("To:".Length);
                    }
                    else if (item.IndexOf("Cc:") != -1)
                    {
                        mm.ccRecipientText = item.Substring("Cc:".Length);
                    }
                    else if (item.IndexOf("Bcc:") != -1)
                    {
                        mm.bccRecipientText = item.Substring("Bcc:".Length);
                    }
                    else if (item.IndexOf("Subject:") != -1)
                    {
                        mm.subject = item.Substring("Subject:".Length);
                    }
                    else if (item.IndexOf("Importance:") != -1)
                    {
                        int importance = 1;
                        string importanceText = item.Substring("Importance:".Length);
                        if (importanceText.ToLower() == "high" || importanceText.ToLower() == "low")
                        {
                            importance = importanceText.ToLower() == "high" ? 2 : 0;
                        }
                        mm.importance = importance;
                    }
                    else if (item.IndexOf("Bcc:") != -1)
                    {
                        mm.bccRecipientText = item.Substring("Bcc:".Length);
                    }
                }
            }
            if (bodyTextMarkerIndex > 0)
            {
                // The + 4 is for "\n\r\n\r"
                // The - 6 is for "\n\r\n\r\n\r" at the end.
                int startIndex = bodyTextMarkerIndex + MAIL_CONTENT_MIME_SPECIFIER.Length + 4;
                if (boundaryText.Length > 0)
                {
                    // This email has encoded attachments with boundary specifiers
                    // So find the FIRST --boundaryspecifier after the email content, this is where the main body text ends and the attachment encoding begins
                    string bodyAndAttachments = emailString.ToString().Substring(startIndex);
                    int firstBoundaryTextIndexAfterBody = bodyAndAttachments.ToLower().IndexOf(boundaryText.ToLower());
                    if (firstBoundaryTextIndexAfterBody > 0)
                    {
                        // The -2 is for the '--'
                        mm.bodyText = bodyAndAttachments.Substring(0, firstBoundaryTextIndexAfterBody - (2 + 6));
                    }
                    else
                    {
                        mm.bodyText = bodyAndAttachments.Substring(0, bodyAndAttachments.Length - 6);
                    }
                }
                else
                {
                    // This has no attachments, so text to the end will be all body.
                    int lengthOfBody = emailString.ToString().Length - startIndex;
                    mm.bodyText = emailString.ToString().Substring(startIndex, lengthOfBody - 6);
                }
            }
            return mm;
        }

        //public async Task<Guid> SendMail(workspace_datastore.models.Message message, List<workspace.WorkspaceEmailWP8.ViewModel.AttachmentDetail> attachmentDetailList)
        //{
        //    //Pankaj : We need to get the file Reference from UI/AppInfrastructure. Currently this is passed from test atmosphere.
        //    ASSendMailRequest sendMailRequest = new ASSendMailRequest();

        //    ASSendMailResponse sendMailResponse = null;

        //    if (App.mActiveSyncManager.Account == null)
        //        throw new InvalidDataException("Account not yet created");

        //    StringBuilder emailString = null;
        //    StringBuilder emailStringExcludingAttachmentContent = new StringBuilder();
        //    emailString = EncodeMail(message, attachmentDetailList, ref emailStringExcludingAttachmentContent);

        //    Guid clientId = Guid.NewGuid();
        //    sendMailRequest.ClientID = clientId.ToString();
        //    sendMailRequest.MimeContent = emailString;

        //    string userName = getUserNameOnly(App.mActiveSyncManager.Account);

        //    // Initialize the request with information
        //    // that applies to all requests.
        //    sendMailRequest.Credentials = userCredentials;
        //    sendMailRequest.Server = App.mActiveSyncManager.Account.ServerUrl;
        //    sendMailRequest.User = userName;
        //    sendMailRequest.DeviceID = deviceToProvision.DeviceID;
        //    sendMailRequest.DeviceType = deviceToProvision.DeviceType;
        //    sendMailRequest.ProtocolVersion = ProtocolVersion;
        //    sendMailRequest.UserAgent = UserAgent;
        //    sendMailRequest.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
        //    sendMailRequest.UseEncodedRequestLine = useBase64RequestLine;

        //    if (null != ConstructOutgoingMessage)
        //    {
        //        MailActionType type = MailActionType.ReferenceNone;
        //        if (message.id > 0)
        //        {
        //            type = MailActionType.ReferenceReply;
        //        }
        //        List<object> attachments = attachmentDetailList.Cast<object>().ToList();
        //        ConstructOutgoingMessage(this, new ConstructOutgoingMessageEventArgs(clientId.ToString(), message.folder_id, emailStringExcludingAttachmentContent, (int)type, attachments));
        //    }

        //    // Send the request
        //    sendMailResponse = await sendMailRequest.GetResponse() as ASSendMailResponse;

        //    if (sendMailResponse == null)
        //    {
        //        System.Diagnostics.Debug.WriteLine("Unable to connect to server.");
        //        return Guid.Empty;
        //    }
        //    else if (null != EASCommunicationsErrorEvent && sendMailResponse.HttpStatus != HttpStatusCode.OK)
        //    {
        //        EASCommunicationsErrorEvent(this, new EASCommunicationsErrorEventArgs(sendMailResponse));
        //        return Guid.Empty;
        //    }
        //    else if (sendMailResponse.HttpStatus == HttpStatusCode.OK && sendMailResponse.Status == 0)
        //    {
        //        //SaveClientIDForSendMail(id);
        //        return clientId;
        //    }
        //    else
        //        return Guid.Empty;
        //}

        public override async Task<ASPingResponse> SendPing(int heartBeatInSeconds, List<ActiveSyncPCL.Folder> folders)
        {
            if(!EASPingRequested)
            {
                EASPingRequested = true;
                ASPingRequest request = new ASPingRequest(folders);
                request.HeartBeatInterval = heartBeatInSeconds;
                if (App.mActiveSyncManager.Account == null)
                    throw new InvalidDataException("Account not yet created");
                string userName = getUserNameOnly(App.mActiveSyncManager.Account);

                LoggingWP8.WP8Logger.LogMessage("Sending Ping event");

                request.Credentials = userCredentials;
                request.Server = App.mActiveSyncManager.Account.ServerUrl;
                request.User = userName;
                request.DeviceID = deviceToProvision.DeviceID;
                request.DeviceType = deviceToProvision.DeviceType;
                request.ProtocolVersion = ProtocolVersion;
                request.UserAgent = UserAgent;
                request.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
                request.UseEncodedRequestLine = useBase64RequestLine;

                // Send the request
                ASPingResponse response = await request.GetResponse() as ASPingResponse;

                LoggingWP8.WP8Logger.LogMessage("Recv'd Ping event response is {0}", (null == response) ? -1 : (int)response.Status);

                bool sendPingEvent = true;
                if (response != null)
                {
                    if (response.Status == (int)ASPingResponse.PingStatus.Changes)
                    {
                        LoggingWP8.WP8Logger.LogMessage("THERE ARE PING CHANGES! About to sync Folder");
                        if (null != EASHandlePingResultEvent)
                        {
                            EASHandlePingResultEvent(this, new EASHandlePingResultEventArgs(response, folders));
                            sendPingEvent = false;
                        }
                    }
                    else if (response.Status == (int)ASPingResponse.PingStatus.MissingParams)
                    {
                        // I have seen this.
                        LoggingWP8.WP8Logger.LogMessage("Received 'Missing Params' from the EAS Server");
                        sendPingEvent = false;
                    }
                }
                if (sendPingEvent && null != EASTriggerSendPingEvent)
                    EASTriggerSendPingEvent(this, new EASTriggerSendPingEventArgs(folders));

                return response;
            }
            else
            {
                LoggingWP8.WP8Logger.LogMessage("Ping Requested but is busy");
            }
            return null;
        }

        /// <summary>
        /// Method to move email items from one folder to another folder
        /// </summary>
        /// <param name="moveItems"></param>
        /// <returns></returns>
        public override async Task<ASMoveItemsResponse> MoveEmailItems(List<ASMoveItemsRequest> moveItems)
        {
            ASMoveItemsRequest request = new ASMoveItemsRequest();
            try
            {
                if (App.mActiveSyncManager.Account == null)
                    throw new InvalidDataException("Account not yet created");
                string userName = getUserNameOnly(App.mActiveSyncManager.Account);

                request.Credentials = userCredentials;
                request.Server = App.mActiveSyncManager.Account.ServerUrl;
                request.User = userName;
                request.DeviceID = deviceToProvision.DeviceID;
                request.DeviceType = deviceToProvision.DeviceType;
                request.ProtocolVersion = ProtocolVersion;
                request.UserAgent = UserAgent;
                request.PolicyKey = App.mActiveSyncManager.Account.PolicyKey;
                request.UseEncodedRequestLine = useBase64RequestLine;
                request.Items = moveItems;
                // Send the request
                ASMoveItemsResponse response = await request.GetResponse() as ASMoveItemsResponse;

                if (response != null && response.status == 3)
                {

                    // Something changed.  Set up a Sync request.
                }
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<bool> SyncMailClientChanges(workspace_datastore.models.Folder folderDB, ActiveSyncPCL.ServerSyncCommand.ServerSyncCommandType command, List<workspace_datastore.Managers.MessageManager> msgManagerList)
        {
//			FolderManager folderManager = new FolderManager();
//#if USE_ASYNC
//			List<workspace_datastore.models.Folder> folderListDB = await folderManager.GetFolderInfoAsync(0);
//#else
//			 List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfo(0);
//#endif
		//	if (foldersCurrentlyBeingSyncd.Contains(folderDB.displayName.ToLower()))
            
                // Put this in the "queue" for later processing once this folder is sync'd.
                Message_UpdateManager mum = new Message_UpdateManager();
                foreach (MessageManager msg in msgManagerList)
                {
                    Message_Updates msgUpdate = new Message_Updates();
                    msgUpdate.folder_id = msg.folderId;
                    msgUpdate.id = msg.messageId;
                    msgUpdate.read = msg.read;
                    msgUpdate.flag = msg.flag;
                    msgUpdate.serverIdentifier = msg.serverID;
                    msgUpdate.moveToFolderServerIdentifier = ""; // TODO For moving whilst syncing
                    msgUpdate.flagStatus = msg.flagStatus;
                    msgUpdate.flagStartDate = msg.flagStartDate;
                    msgUpdate.flagDueDate = msg.flagDueDate;
                    msgUpdate.flagUTCStartDate = msg.flagUTCStartDate;
                    msgUpdate.flagUTCDueDate = msg.flagUTCDueDate;
                    msgUpdate.flagReminderSet = msg.flagReminderSet;

                    // Will these ever be needed?
#if FLAG_INFO_NEEDED
                    msgUpdate.flagType = msg.flagType;
                    msgUpdate.flagCompletedDate = msg.flagCompletedDate;
                    msgUpdate.flagCompletedTime = msg.flagCompletedTime;
                    msgUpdate.flagReminderTime = msg.flagReminderTime;
                    msgUpdate.flagOrdinalDate = msg.flagOrdinalDate;
                    msgUpdate.flagSubOrdinalDate = msg.flagSubOrdinalDate;
#endif
                    mum.AddUpdateUpdatedMsgInfoAsync(msgUpdate);
                }
           
           	if (!foldersCurrentlyBeingSyncd.Contains(folderDB.displayName.ToLower()))
            {
                //copying the change list 
                mMessageManagerListForClientSideChanges = msgManagerList;
                //Creating a ASPCL Inboxfolder, for second time Sync
                //var folderDB = folderListDB.SingleOrDefault(x => x.displayName == folderName);
                if (folderDB != null)
                {
                    ActiveSyncPCL.Folder.FolderType folderType = (ActiveSyncPCL.Folder.FolderType)folderDB.folderType;
                    ActiveSyncPCL.Folder ASFolder = new ActiveSyncPCL.Folder(folderDB.displayName, folderDB.serverIdentifier, folderType, null);

                    if (ASFolder != null)
                    {
                        if (folderDB.syncKey != "1")
                            ASFolder.SyncKey = folderDB.syncKey;
                    }
                    bool mChangedStatus=false;
                    //It was throwing out of index exception if msgManagerList count is 0 because we are getting server id by using msgManagerList.ElementAt(0).serverID
                    //So i am checking the count before calling this method
                    if (msgManagerList.Count > 0)
                    {
                        mChangedStatus = await SendClientMailChangesRequest(ASFolder, command, msgManagerList);
                    }
                    if (mChangedStatus == true)
                    {
                        mum.DeleteAllChangedMessagesAsync();  
                    }
                    return mChangedStatus;
                }
            }
            return false;
        }

        private async Task<bool> SendClientMailChangesRequest(ActiveSyncPCL.Folder folder, ActiveSyncPCL.ServerSyncCommand.ServerSyncCommandType command,
            List<workspace_datastore.Managers.MessageManager> msgManagerList)
        {
            try
            {
                // If this is the first time syncing Inbox
                // (Sync Key = 0), then you must send an initial sync request to "prime" the sync state.
                EASAccount acct = App.mActiveSyncManager.Account;

                string userName = getUserNameOnly(acct);
                if (folder.SyncKey != "0")
                {
                    ASMailClientChangesSyncRequest clientChangeRequest = new ASMailClientChangesSyncRequest();

                    clientChangeRequest.Credentials = userCredentials;
                    clientChangeRequest.Server = acct.ServerUrl;
                    clientChangeRequest.User = userName;
                    clientChangeRequest.DeviceID = deviceToProvision.DeviceID;
                    clientChangeRequest.DeviceType = deviceToProvision.DeviceType;
                    clientChangeRequest.ProtocolVersion = ProtocolVersion;
                    clientChangeRequest.UserAgent = UserAgent;

                    clientChangeRequest.PolicyKey = acct.PolicyKey;
                    clientChangeRequest.UseEncodedRequestLine = useBase64RequestLine;

                    //meeting request input paarameter
                    ASMailClientChangesSyncRequest.AppendClientChangesInRequestXML += new ASMailClientChangesSyncRequest.SetClientChangesInRequestXMLDelegate(SetXMLDataInRequest);

                    clientChangeRequest.mLastSyncKey = folder.SyncKey;
                    clientChangeRequest.mCollectionId = folder.Id;
                    clientChangeRequest.CommandType = command;

                    //clientChangeRequest.CommandType = EnumContactCommandType.Change;
                    clientChangeRequest.mServerID = msgManagerList.ElementAt(0).serverID;
                    clientChangeRequest.mRead = Convert.ToString(msgManagerList.ElementAt(0).read);

                    // Send the request
                    ASMailClientChangesSyncResponse clientChangeResponse = await clientChangeRequest.GetResponse() as ASMailClientChangesSyncResponse;

                    if (null == clientChangeResponse)
                    {
                        // No network I presume
                        return false;
                    }
                    if (clientChangeResponse.status == (int)ASSyncResponse.SyncStatus.Success)
                    {
                        folder.SyncKey = clientChangeResponse.GetSyncKeyForFolder(folder.Id);
                        folder.LastSyncTime = DateTime.Now;


                        //save the sync key
                        FolderManager folderManager = new FolderManager();
#if USE_ASYNC
                        List<workspace_datastore.models.Folder> folderList = await folderManager.GetFolderInfoAsync(0);
#else
                        List<workspace_datastore.models.Folder> folderList = folderManager.GetFolderInfo(0);
#endif
                        workspace_datastore.models.Folder folderDB = folderList.FirstOrDefault(x => x.displayName == folder.Name);
                        if (folderDB != null)
                        {
                            folderDB.syncKey = folder.SyncKey;
                            folderDB.syncTime = folder.LastSyncTime.ToBinary();
#if USE_ASYNC
                            await folderManager.UpdateFolderInfoAsync(folderDB);
#else
                            folderManager.UpdateFolderInfo(folderDB);
#endif
                        }

                        //save the messages
                        List<workspace_datastore.models.Message> msgsToUpdate = new List<workspace_datastore.models.Message>();
                        foreach (MessageManager msgManager in mMessageManagerListForClientSideChanges)
                        {
#if USE_ASYNC
                            workspace_datastore.models.Message msg = msgManager.GetMessageInfoAsync(msgManager.serverID).Result.ElementAt(0);
#else
                            workspace_datastore.models.Message msg = msgManager.GetMessageInfo(msgManager.serverID).ElementAt(0);
#endif
                            msg.read = msgManager.read;
                            msg.flag = msgManager.flag;
                            msg.flagStatus = msgManager.flagStatus;
                            msg.flagStartDate = msgManager.flagStartDate;
                            msg.flagUTCStartDate = msgManager.flagUTCStartDate;
                            msg.flagDueDate = msgManager.flagDueDate;
                            msg.flagUTCDueDate = msgManager.flagUTCDueDate;
                            msgsToUpdate.Add(msg);
                        }

                        MessageManager mm = new MessageManager();
#if USE_ASYNC
                        mm.UpdateAllMessagesInfoAsync(msgsToUpdate);
#else
                        mm.UpdateAllMessagesInfo(msgsToUpdate);
#endif
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private XDocument SetXMLDataInRequest(ASMailClientChangesSyncRequest reqObject)
        {
            XDocument syncXML = new XDocument(new XDeclaration("1.0", "utf-8", null));
            XNamespace airSyncBaseNameSpace = Namespaces.airSyncBaseNamespace;
            XNamespace airSyncNameSpace = Namespaces.airSyncNamespace;
            XNamespace mailNameSpace = Namespaces.mailNamespace;
            XNamespace taskNameSpace = Namespaces.taskNamespace;



            XElement syncNode = new XElement(airSyncNameSpace + "Sync",
                new XAttribute(XNamespace.Xmlns + Xmlns.mailXmlns, Namespaces.mailNamespace),
                new XAttribute(XNamespace.Xmlns + Xmlns.taskXmlns, Namespaces.taskNamespace));

            syncXML.Add(syncNode);

            // Only add a collections node if there are folders in the request.
            // If omitting, there should be a Partial element.

            XElement collectionsNode = new XElement(airSyncNameSpace + "Collections");
            syncNode.Add(collectionsNode);

            XElement collectionNode = new XElement(airSyncNameSpace + "Collection");
            collectionsNode.Add(collectionNode);

            XElement syncKeyNode = new XElement(airSyncNameSpace + "SyncKey", new XText(reqObject.mLastSyncKey));
            collectionNode.Add(syncKeyNode);

            XElement collectionIdNode = new XElement(airSyncNameSpace + "CollectionId", new XText(reqObject.mCollectionId));
            collectionNode.Add(collectionIdNode);

            if (reqObject.CommandType == ServerSyncCommand.ServerSyncCommandType.Change)
            {
                XElement commandsNode = new XElement(airSyncNameSpace + "Commands");
                collectionNode.Add(commandsNode);

                foreach (MessageManager msgManager in mMessageManagerListForClientSideChanges)
                {
                    XElement AddNode = new XElement(airSyncNameSpace + "Change");
                    commandsNode.Add(AddNode);

                    XElement serverIDNode = new XElement(airSyncNameSpace + "ServerId", msgManager.serverID);
                    AddNode.Add(serverIDNode);

                    XElement ApplicationDataNode = new XElement(airSyncNameSpace + "ApplicationData");
                    AddNode.Add(ApplicationDataNode);

                    //Read Data
                    XElement readNode = new XElement(mailNameSpace + "Read", msgManager.read);
                    ApplicationDataNode.Add(readNode);

                    //Flag data

                    XElement flagNode = new XElement(mailNameSpace + "Flag");
                    ApplicationDataNode.Add(flagNode);


                    //0 for clear. 1 for done. 2 for active
                    string flagStatus = string.Empty;
                    if (msgManager.flag == 0)
                        flagStatus = "0";
                    else
                        flagStatus = "2";

                    XElement statusNode = new XElement(mailNameSpace + "Status", flagStatus);
                    flagNode.Add(statusNode);

                    /*<email:Flag>
                            <tasks:DueDate>2014-08-18T00:00:00.000Z</tasks:DueDate>
                            <tasks:UtcDueDate>2014-08-18T04:00:00.000Z</tasks:UtcDueDate>
                            <tasks:UtcStartDate>2014-08-18T04:00:00.000Z</tasks:UtcStartDate>
                            <tasks:Subject>From Loren at kace</tasks:Subject>
                            <email:Status>2</email:Status>
                            <email:FlagType>Flag for follow up</email:FlagType>
                            <tasks:StartDate>2014-08-18T00:00:00.000Z</tasks:StartDate>
                        </email:Flag>
                        */
                    //DateTime currentTime = DateTime.Now;
                    //DateTime currentUTCTime = DateTime.UtcNow;

                    //String currentTimeString = currentTime.Year.ToString() + "-" + currentTime.Month.ToString() + "-" +
                    //    currentTime.Day.ToString() +"T" + currentTime.Hour.ToString() + ":00:00.000Z";

                    //String utcTimeString = currentUTCTime.Year.ToString() + "-" + currentUTCTime.Month.ToString() + "-" +
                    //    currentUTCTime.Day.ToString() + "T" + currentUTCTime.Hour.ToString() + ":00:00.000Z";

                    XElement flagTypeNode = new XElement(mailNameSpace + "FlagType", "for Follow Up");
                    flagNode.Add(flagTypeNode);

                    //Flag can be send without date and we dont have any way to set date in app

                    //XElement startDateNode = new XElement(taskNameSpace + "StartDate", currentTimeString);
                    //flagNode.Add(startDateNode);

                    //XElement utcStartDateNode = new XElement(taskNameSpace + "UtcStartDate", utcTimeString);
                    //flagNode.Add(utcStartDateNode);

                    //XElement dueDateNode = new XElement(taskNameSpace + "DueDate", currentTimeString);
                    //flagNode.Add(dueDateNode);

                    //XElement utcDueDateNode = new XElement(taskNameSpace + "UtcDueDate", utcTimeString);
                    //flagNode.Add(utcDueDateNode); 

                    //PK:ToDo - Not sure about reminder currently
                    XElement reminderNode = new XElement(taskNameSpace + "ReminderSet", "0");
                    flagNode.Add(reminderNode);


                }

            }


            //if (reqObject.CommandType == ServerSyncCommand.ServerSyncCommandType.Delete)
            //{
            //    //XElement deleteAsMovesNode = new XElement(airSyncNameSpace + "DeletesAsMoves", "0");
            //    //collectionNode.Add(deleteAsMovesNode);

            //    //XElement getChangesNode = new XElement(airSyncNameSpace + "GetChanges", "1");
            //    //collectionNode.Add(getChangesNode);


            //    XElement commandsNode = new XElement(airSyncNameSpace + "Commands");
            //    collectionNode.Add(commandsNode);

            //    XElement AddNode = new XElement(airSyncNameSpace + "Delete");
            //    commandsNode.Add(AddNode);

            //    XElement serverIDNode = new XElement(airSyncNameSpace + "ServerId", "2:3");
            //    AddNode.Add(serverIDNode);

            //}

            return syncXML;

        }

        public async Task<List<workspace_datastore.models.Contacts>> SearchGAL(string searchItem)
        {
            //List<workspace_datastore.models.Contacts> gal = new List<workspace_datastore.models.Contacts>();
            List<workspace_datastore.models.Contacts> gal = null;
            try
            {
                // If this is the first time syncing Inbox
                // (Sync Key = 0), then you must send an initial sync request to "prime" the sync state.
                EASAccount acct = App.mActiveSyncManager.Account;

                string userName = getUserNameOnly(acct);

                ASGALRequest globalAddressListRequest = new ASGALRequest();

                globalAddressListRequest.Credentials = userCredentials;
                globalAddressListRequest.Server = acct.ServerUrl;
                globalAddressListRequest.User = userName;
                globalAddressListRequest.DeviceID = deviceToProvision.DeviceID;
                globalAddressListRequest.DeviceType = deviceToProvision.DeviceType;
                globalAddressListRequest.ProtocolVersion = ProtocolVersion;
                globalAddressListRequest.UserAgent = UserAgent;

                globalAddressListRequest.PolicyKey = acct.PolicyKey;
                globalAddressListRequest.UseEncodedRequestLine = useBase64RequestLine;
                globalAddressListRequest.SetSearchString(searchItem);

                // Send the request
                ASGALResponse globalAddressListResponse = await globalAddressListRequest.GetResponse() as ASGALResponse;

                if (globalAddressListResponse != null)
                {
                    if (globalAddressListResponse.status == (int)ASSyncResponse.SyncStatus.Success)
                    {

                        //Do the parsing
                        XDocument responseXml = new XDocument();
                        TextReader tr = new StringReader(globalAddressListResponse.XmlString);
                        responseXml = XDocument.Load(tr, LoadOptions.None);

                        gal = workspace_datastore.Helpers.ParsingUtils.ParseAndGetGALSearchResultList(responseXml);

                        //string galNS = "{GAL}";
                        //string searchNS = "{Search}";
                        //var addElement = responseXml.Element(searchNS + "Search").
                        //                                        Element(searchNS + "Response").
                        //                                        Element(searchNS + "Store").
                        //                                        Elements(searchNS + "Result").ToList();

                        //foreach (XElement element in addElement)
                        //{
                        //    workspace_datastore.models.Contacts galObj = new workspace_datastore.models.Contacts();
                        //    try
                        //    {
                        //        var dispName = element.Element(searchNS + "Properties").Element(galNS + "DisplayName").Value;
                        //        var aliasName = element.Element(searchNS + "Properties").Element(galNS + "Alias").Value;

                        //        XElement displayName = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "DisplayName");
                        //        XElement firstName = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "FirstName");
                        //        XElement lastName = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "LastName");
                        //        XElement email1Address = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "EmailAddress");
                        //        XElement office = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "Office");
                        //        XElement title = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "Title");
                        //        XElement company = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "Company");
                        //        XElement alias = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "Alias");


                        //        if (null != firstName)
                        //            galObj.firstName = firstName.Value;
                        //        if (null != lastName)
                        //            galObj.lastName = lastName.Value;
                        //        if (null != email1Address)
                        //            galObj.email1Address = email1Address.Value;
                        //    }
                        //    catch
                        //    {

                        //    }
                        //    gal.Add(galObj);
                        //}
                    }
                    else
                    {
                        return null;
                    }
                }

                return gal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> DoContactChangesFromClient(ActiveSyncPCL.Folder folder, EnumContactCommandType command, List<workspace_datastore.models.Contacts> contactListToChangeOrAdd)
        {
            mContactListForClientChanges = contactListToChangeOrAdd;
            try
            {
                EASAccount acct = App.mActiveSyncManager.Account;
                string userName = getUserNameOnly(acct);

                // (Sync Key = 0), then you must send an initial sync request to "prime" the sync state.
                if (folder.SyncKey != "0")
                {
                    ASContactClientChangesSyncRequest addContactRequest = new ASContactClientChangesSyncRequest();

                    addContactRequest.Credentials = userCredentials;
                    addContactRequest.Server = acct.ServerUrl;
                    addContactRequest.User = userName;
                    addContactRequest.DeviceID = deviceToProvision.DeviceID;
                    addContactRequest.DeviceType = deviceToProvision.DeviceType;
                    addContactRequest.ProtocolVersion = ProtocolVersion;
                    addContactRequest.UserAgent = UserAgent;

                    addContactRequest.PolicyKey = acct.PolicyKey;
                    addContactRequest.UseEncodedRequestLine = useBase64RequestLine;
                    addContactRequest.mLastSyncKey = folder.SyncKey;

                    //addContactRequest.ClientId = Guid.NewGuid().ToString();
                    addContactRequest.CollectionId = folder.Id;
                    addContactRequest.CommandType = command;


                    ASContactClientChangesSyncRequest.AppendClientChangesInRequestXML += new ASContactClientChangesSyncRequest.SetClientChangesInRequestXMLDelegate(SetXMLDataInContactRequest);


                    // Send the request
                    ASContactClientChangesSyncResponse addContactResponse = await addContactRequest.GetResponse() as ASContactClientChangesSyncResponse;
                    ASContactClientChangesSyncRequest.AppendClientChangesInRequestXML -= new ASContactClientChangesSyncRequest.SetClientChangesInRequestXMLDelegate(SetXMLDataInContactRequest);
                    if (addContactResponse == null)
                        return false;
                    else
                    {
                        if (addContactResponse.SyncStatus == (int)ASSyncResponse.SyncStatus.Success)
                        {
                            folder.SyncKey = addContactResponse.GetSyncKeyForFolder(folder.Id);
                            folder.LastSyncTime = DateTime.Now;


                            FolderManager folderManager = new FolderManager();
                            List<workspace_datastore.models.Folder> folderList = await folderManager.GetFolderInfoAsync(0);
                            workspace_datastore.models.Folder folderDB = folderList.FirstOrDefault(x => x.displayName == folder.Name);

                            
                            if (folderDB != null)
                            {
                                folderDB.syncKey = folder.SyncKey;
                                folderDB.syncTime = folder.LastSyncTime.ToBinary();
                                await folderManager.UpdateFolderInfoAsync(folderDB);
                            }

                            //adding/updating contact in db
                            ContactManager contactsManager = new ContactManager();
                            DataSourceAsync ds = new DataSourceAsync();

                            if (command == EnumContactCommandType.Add)
                            {
                                foreach (workspace_datastore.models.Contacts contact in mContactListForClientChanges)
                                {
                                    if (addContactResponse.GetStatusOfAddedContact(contact.clientID) == 1)
                                    {
                                        string serverId = addContactResponse.GetServerIdOfAddedContact(contact.clientID);
                                        contact.serverId = serverId;
                                        //Passing folder id and id value for any future use. Right now no use of it.(Dhruv 12-03-2014)                                       
                                        contact.folder_id = Convert.ToInt32(contact.serverId.Split(':')[0]);
                                        contact.id = Convert.ToInt32(contact.serverId.Split(':')[1]);
                                        contactsManager.AddContactInfoAsync(contact);
                                    }
                                }
                            }
                            else if (command == EnumContactCommandType.Change)
                            {
                                foreach (workspace_datastore.models.Contacts contact in mContactListForClientChanges)
                                {
                                    contactsManager.UpdateContactInfoAsync(contact);
                                }
                            }
                            else
                            {
                                foreach (workspace_datastore.models.Contacts contact in mContactListForClientChanges)
                                {
                                    int folderid = System.Convert.ToInt32(folder.Id);
                                    await contactsManager.DeleteContactInfoAsync(folderid, contact.id);
                                }
                            }
                            return true;
                        }
                        else
                            return false;
                    }

                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;

            }
        }

        private XDocument SetXMLDataInContactRequest(ASContactClientChangesSyncRequest reqObject)
        {
            XDocument syncXML = new XDocument(new XDeclaration("1.0", "utf-8", null));
            XNamespace airsyncbasenamespace = Namespaces.airSyncBaseNamespace;
            XNamespace asyncnamespace = Namespaces.airSyncNamespace;
            //XNamespace syncnamespace = Namespaces.contactNamespace;
            XNamespace contactNameSpace = Namespaces.contactNamespace;
            XElement syncNode = new XElement(asyncnamespace + "Sync",
                new XAttribute(XNamespace.Xmlns + Xmlns.contactXmlns, Namespaces.contactNamespace));

            syncXML.Add(syncNode);

            // Only add a collections node if there are folders in the request.
            // If omitting, there should be a Partial element.

            XElement collectionsNode = new XElement(asyncnamespace + "Collections");
            syncNode.Add(collectionsNode);

            XElement collectionNode = new XElement(asyncnamespace + "Collection");
            collectionsNode.Add(collectionNode);

            XElement syncKeyNode = new XElement(asyncnamespace + "SyncKey", new XText(reqObject.mLastSyncKey));
            collectionNode.Add(syncKeyNode);

            XElement collectionIdNode = new XElement(asyncnamespace + "CollectionId", new XText(reqObject.CollectionId));
            collectionNode.Add(collectionIdNode);

            //We will add the application node as per command below 
            if (reqObject.CommandType == EnumContactCommandType.Add)
            {
                XElement commandsNode = new XElement(asyncnamespace + "Commands");
                collectionNode.Add(commandsNode);

                foreach (workspace_datastore.models.Contacts contact in mContactListForClientChanges)
                {
                    XElement AddNode = new XElement(asyncnamespace + "Add");
                    commandsNode.Add(AddNode);

                    XElement ClientIdNode = new XElement(asyncnamespace + "ClientId", new XText(contact.clientID));
                    AddNode.Add(ClientIdNode);

                    XElement ApplicationDataNode = GetApplicationDataNodeForContact(contact);
                    AddNode.Add(ApplicationDataNode);
                }
                return syncXML;

            }
            else if (reqObject.CommandType == EnumContactCommandType.Change)
            {
                XElement commandsNode = new XElement(asyncnamespace + "Commands");
                collectionNode.Add(commandsNode);

                foreach (workspace_datastore.models.Contacts contact in mContactListForClientChanges)
                {
                    XElement AddNode = new XElement(asyncnamespace + "Change");
                    commandsNode.Add(AddNode);
                    XElement serverIDNode = new XElement(asyncnamespace + "ServerId", contact.serverId);
                    AddNode.Add(serverIDNode);

                    XElement ApplicationDataNode = GetApplicationDataNodeForContact(contact);
                    AddNode.Add(ApplicationDataNode);
                }
                return syncXML;
            }
            else if (reqObject.CommandType == EnumContactCommandType.Delete)
            {
                XElement deleteAsMovesNode = new XElement(asyncnamespace + "DeletesAsMoves", "0");
                collectionNode.Add(deleteAsMovesNode);

                XElement getChangesNode = new XElement(asyncnamespace + "GetChanges", "1");
                collectionNode.Add(getChangesNode);

                XElement windowSizeNode = new XElement(asyncnamespace + "WindowSize", "512");
                collectionNode.Add(windowSizeNode);

                XElement commandsNode = new XElement(asyncnamespace + "Commands");
                collectionNode.Add(commandsNode);

                foreach (workspace_datastore.models.Contacts contact in mContactListForClientChanges)
                {
                    XElement AddNode = new XElement(asyncnamespace + "Delete");
                    commandsNode.Add(AddNode);

                    XElement serverIDNode = new XElement(asyncnamespace + "ServerId", contact.serverId);
                    AddNode.Add(serverIDNode);
                }
                return syncXML;
            }
            else
                return null;

        }

        private XElement GetApplicationDataNodeForContact(workspace_datastore.models.Contacts contactForClientChanges)
        {
            XNamespace contactNameSpace = Namespaces.contactNamespace;
            XNamespace asyncnamespace = Namespaces.airSyncNamespace;

            XElement ApplicationDataNode = new XElement(asyncnamespace + "ApplicationData");

            if (contactForClientChanges.email1Address != null && contactForClientChanges.email1Address != string.Empty)
            {
                XElement email1Address = new XElement(contactNameSpace + "Email1Address", new XText(contactForClientChanges.email1Address));
                ApplicationDataNode.Add(email1Address);
            }
            if (contactForClientChanges.email2Address != null && contactForClientChanges.email2Address != string.Empty)
            {
                XElement email2Address = new XElement(contactNameSpace + "Email2Address", new XText(contactForClientChanges.email2Address));
                ApplicationDataNode.Add(email2Address);
            }
            if (contactForClientChanges.email3Address != null && contactForClientChanges.email3Address != string.Empty)
            {
                XElement email3Address = new XElement(contactNameSpace + "Email3Address", new XText(contactForClientChanges.email3Address));
                ApplicationDataNode.Add(email3Address);
            }
            if (contactForClientChanges.firstName != null && contactForClientChanges.firstName != string.Empty)
            {
                XElement firstNameNode = new XElement(contactNameSpace + "FirstName", new XText(contactForClientChanges.firstName));
                ApplicationDataNode.Add(firstNameNode);
            }
            //XElement middleNameNode = new XElement(contactNameSpace + "MiddleName", new XText(mNewContactToAdd.mi));
            //ApplicationDataNode.Add(middleNameNode);

            if (contactForClientChanges.lastName != null && contactForClientChanges.lastName != string.Empty)
            {
                XElement lastNameNode = new XElement(contactNameSpace + "LastName", new XText(contactForClientChanges.lastName));
                ApplicationDataNode.Add(lastNameNode);
            }
            //XElement titleNode = new XElement(contactNameSpace + "Title", new XText(mNewContactToAdd.t));
            //ApplicationDataNode.Add(titleNode);

            if (contactForClientChanges.companyName != null && contactForClientChanges.companyName != string.Empty)
            {
                XElement companyName = new XElement(contactNameSpace + "CompanyName", new XText(contactForClientChanges.companyName));
                ApplicationDataNode.Add(companyName);
            }
            if (contactForClientChanges.jobTitle != null && contactForClientChanges.jobTitle != string.Empty)
            {
                XElement jobTitle = new XElement(contactNameSpace + "JobTitle", new XText(contactForClientChanges.jobTitle));
                ApplicationDataNode.Add(jobTitle);
            }


            //Business Address
            if (contactForClientChanges.businessAddressCountry != null && contactForClientChanges.businessAddressCountry != string.Empty)
            {
                XElement businessAdderessCountry = new XElement(contactNameSpace + "BusinessAddressCountry", new XText(contactForClientChanges.businessAddressCountry));
                ApplicationDataNode.Add(businessAdderessCountry);
            }
            if (contactForClientChanges.businessAddressPostalCode != null && contactForClientChanges.businessAddressPostalCode != string.Empty)
            {
                XElement businessAdderessPostalCode = new XElement(contactNameSpace + "BusinessAddressPostalCode", new XText(contactForClientChanges.businessAddressPostalCode));
                ApplicationDataNode.Add(businessAdderessPostalCode);
            }
            if (contactForClientChanges.businessAddressState != null && contactForClientChanges.businessAddressState != string.Empty)
            {
                XElement businessAddressState = new XElement(contactNameSpace + "BusinessAddressState", new XText(contactForClientChanges.businessAddressState));
                ApplicationDataNode.Add(businessAddressState);
            }
            if (contactForClientChanges.businessAddressStreet != null && contactForClientChanges.businessAddressStreet != string.Empty)
            {
                XElement businessAddressStreet = new XElement(contactNameSpace + "BusinessAddressStreet", new XText(contactForClientChanges.businessAddressStreet));
                ApplicationDataNode.Add(businessAddressStreet);
            }
            if (contactForClientChanges.businessFaxNumber != null && contactForClientChanges.businessFaxNumber != string.Empty)
            {
                XElement businessFaxNumber = new XElement(contactNameSpace + "BusinessFaxNumber", new XText(contactForClientChanges.businessFaxNumber));
                ApplicationDataNode.Add(businessFaxNumber);
            }
            if (contactForClientChanges.businessPhoneNumber != null && contactForClientChanges.businessPhoneNumber != string.Empty)
            {
                XElement businessPhoneNumber = new XElement(contactNameSpace + "BusinessPhoneNumber", new XText(contactForClientChanges.businessPhoneNumber));
                ApplicationDataNode.Add(businessPhoneNumber);
            }

            //Home Address
            if (contactForClientChanges.home2PhoneNumber != null && contactForClientChanges.home2PhoneNumber != string.Empty)
            {
                XElement home2PhoneNumber = new XElement(contactNameSpace + "Home2PhoneNumber", new XText(contactForClientChanges.home2PhoneNumber));
                ApplicationDataNode.Add(home2PhoneNumber);
            }
            if (contactForClientChanges.homeAddressCity != null && contactForClientChanges.homeAddressCity != string.Empty)
            {
                XElement homeAddressCity = new XElement(contactNameSpace + "HomeAddressCity", new XText(contactForClientChanges.homeAddressCity));
                ApplicationDataNode.Add(homeAddressCity);
            }
            if (contactForClientChanges.homeAddressCountry != null && contactForClientChanges.homeAddressCountry != string.Empty)
            {
                XElement homeAddressCountry = new XElement(contactNameSpace + "HomeAddressCountry", new XText(contactForClientChanges.homeAddressCountry));
                ApplicationDataNode.Add(homeAddressCountry);
            }
            if (contactForClientChanges.homeAddressPostalCode != null && contactForClientChanges.homeAddressPostalCode != string.Empty)
            {
                XElement homeAddressPostalCode = new XElement(contactNameSpace + "HomeAddressPostalCode", new XText(contactForClientChanges.homeAddressPostalCode));
                ApplicationDataNode.Add(homeAddressPostalCode);
            }
            if (contactForClientChanges.homeAddressState != null && contactForClientChanges.homeAddressState != string.Empty)
            {
                XElement homeAddressState = new XElement(contactNameSpace + "HomeAddressState", new XText(contactForClientChanges.homeAddressState));
                ApplicationDataNode.Add(homeAddressState);
            }
            if (contactForClientChanges.homeAddressStreet != null && contactForClientChanges.homeAddressStreet != string.Empty)
            {
                XElement homeAddressStreet = new XElement(contactNameSpace + "HomeAddressStreet", new XText(contactForClientChanges.homeAddressStreet));
                ApplicationDataNode.Add(homeAddressStreet);
            }
            if (contactForClientChanges.homeFaxNumber != null && contactForClientChanges.homeFaxNumber != string.Empty)
            {
                XElement homeFaxNumber = new XElement(contactNameSpace + "HomeFaxNumber", new XText(contactForClientChanges.homeFaxNumber));
                ApplicationDataNode.Add(homeFaxNumber);
            }
            if (contactForClientChanges.homePhoneNumber != null && contactForClientChanges.homePhoneNumber != string.Empty)
            {
                XElement homePhoneNumber = new XElement(contactNameSpace + "HomePhoneNumber", new XText(contactForClientChanges.homePhoneNumber));
                ApplicationDataNode.Add(homePhoneNumber);
            }

            //Other Address
            if (contactForClientChanges.otherAddressCity != null && contactForClientChanges.otherAddressCity != string.Empty)
            {
                XElement otherAddressCity = new XElement(contactNameSpace + "OtherAddressCity", new XText(contactForClientChanges.otherAddressCity));
                ApplicationDataNode.Add(otherAddressCity);
            }
            if (contactForClientChanges.otherAddressCountry != null && contactForClientChanges.otherAddressCountry != string.Empty)
            {
                XElement otherAddressCountry = new XElement(contactNameSpace + "OtherAddressCountry", new XText(contactForClientChanges.otherAddressCountry));
                ApplicationDataNode.Add(otherAddressCountry);
            }
            if (contactForClientChanges.otherAddressPostalCode != null && contactForClientChanges.otherAddressPostalCode != string.Empty)
            {
                XElement otherAddressPostalCode = new XElement(contactNameSpace + "OtherAddressPostalCode", new XText(contactForClientChanges.otherAddressPostalCode));
                ApplicationDataNode.Add(otherAddressPostalCode);
            }
            if (contactForClientChanges.otherAddressState != null && contactForClientChanges.otherAddressState != string.Empty)
            {
                XElement otherAddressState = new XElement(contactNameSpace + "OtherAddressState", new XText(contactForClientChanges.otherAddressState));
                ApplicationDataNode.Add(otherAddressState);
            }
            if (contactForClientChanges.otherAddressStreet != null && contactForClientChanges.otherAddressStreet != string.Empty)
            {
                XElement otherAddressStreet = new XElement(contactNameSpace + "OtherAddressStreet", new XText(contactForClientChanges.otherAddressStreet));
                ApplicationDataNode.Add(otherAddressStreet);
            }

            return ApplicationDataNode;
        }
    }
}
