﻿#define USE_ASYNC
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using workspace_datastore.Managers;

namespace com.dell.workspace.Helpers
{
    public class LiveList<T> : ViewModelBase, IDisposable
    {
        ObservableCollection<T> _items = null;
        DispatcherTimer _dt = null;
        string mFolderName = "";

        public LiveList()
        {
            _items = new ObservableCollection<T>();
        }

        public void StartPolling(string folderName)
        {
            mFolderName = folderName;
            if (this._dt == null)
            {
                this._dt = new DispatcherTimer();
                this._dt.Interval = TimeSpan.FromMilliseconds(10000);
                this._dt.Tick += new EventHandler(dispatcherTimer_Tick);
                this._dt.Start();
            }
        }

        public void StopPolling()
        {
            if (null != _dt)
            {
                _dt.Stop();
                _dt = null;
            }
            _items = null;
        }

        public ObservableCollection<T> Items 
        { 
            get
            {
                return _items;
            }

            set
            {
                if(_items != value)
                {
                    _items = value;

                    RaisePropertyChanged("Items");
                }
            }
        }

        private async void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            FolderManager mgr = new FolderManager();
#if USE_ASYNC
            int folderId = await mgr.GetFolderIdentifierBasedOnFolderNameAsync(mFolderName);
#else
            int folderId = mgr.GetFolderIdentifierBasedOnFolderName(folderName);
#endif
            MessageManager msgManager = new MessageManager();
            // CRICKET based on what T is we need get the right info.  e.g. if T is MessageManager, then we use the following
#if USE_ASYNC
            List<MessageManager> res = null;
            if (mFolderName.ToLower().Trim() == "drafts")
            {
                res = msgManager.GetDraftPartInfoAsync(folderId, 0).Result;
            }
            else
            {
                res = msgManager.GetMessagewithAttachInfoAsync(folderId, 0).Result;
            }
#else
            var res = msgManager.GetMessagewithAttachInfo(folderId, 0);
#endif
            int count = Items.Count;
            if(res.Count != count)
            {
                System.Diagnostics.Debug.WriteLine("Messages To Render: {0}", count);
                Items.Clear();
                Items = new ObservableCollection<T>(res as List<T>);
            }
        }

        public void Dispose()
        {
            if(null != _dt)
            {
                _dt.Stop();
                _dt = null;
            }
        }
    }
}
