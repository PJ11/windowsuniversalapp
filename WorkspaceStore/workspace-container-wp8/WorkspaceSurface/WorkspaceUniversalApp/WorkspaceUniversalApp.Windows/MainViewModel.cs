using workspace.Helpers;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Windows.Input;

namespace workspace
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {


        private const string stringHomeView = "/HomePage.xaml";
        private readonly IDataService _dataService;
        private readonly INavigationService _navigationService;

        /// <summary>
        /// The <see cref="WelcomeTitle" /> property's name.
        /// </summary>
        public const string WelcomeTitlePropertyName = "WelcomeTitle";
        

        private string _welcomeTitle = string.Empty;
         public ICommand NavigateToMailView1 { get; private set; }
       

        public ICommand NavigateToBrowserView1 { get; private set; }
       
        public ICommand NavigateToCalendarView1 { get; private set; }
       
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService, INavigationService navigationService)
        {
            _navigationService = navigationService;
            _dataService = dataService;
            _dataService.GetData(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    WelcomeTitle = item.Title;
                });

            NavigateToMailView1 = new RelayCommand(ShowMailView1);
            

            NavigateToBrowserView1 = new RelayCommand(ShowFileBrowserView1);
            
            NavigateToCalendarView1 = new RelayCommand(ShowCalendarView1);
                        
        }

        /// <summary>
        /// Gets the WelcomeTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string WelcomeTitle
        {
            get
            {
                return _welcomeTitle;
            }

            set
            {
                if (_welcomeTitle == value)
                {
                    return;
                }

                _welcomeTitle = value;
                RaisePropertyChanged(WelcomeTitlePropertyName);
            }
        }
        //private void ShowHomeView()
        //{

        //    _navigationService.NavigateTo(new Uri(string.Format(stringHomeView), UriKind.Relative));
        //}

        private void ShowMailView1()
        {
            if (ViewManager._mailViewHistory.Count == 0)
            {
                _navigationService.NavigateTo(ViewManager.uriMailViewOne.GetType());
                ViewManager._mailViewHistory.Add(ViewManager.uriMailViewOne);
            }
            else
            {
                Uri lastPage = ViewManager._mailViewHistory[ViewManager._mailViewHistory.Count - 1];
                _navigationService.NavigateTo(lastPage.GetType());

            }

        }

        private void ShowFileBrowserView1()
        {
            if (ViewManager._filebrowserViewHistory.Count == 0)
            {
                _navigationService.NavigateTo(ViewManager.uriFileBrowserViewOne.GetType());
                ViewManager._filebrowserViewHistory.Add(ViewManager.uriFileBrowserViewOne);
            }
            else
            {
                Uri lastPage = ViewManager._filebrowserViewHistory[ViewManager._filebrowserViewHistory.Count - 1];
                _navigationService.NavigateTo(lastPage.GetType());

            }

        }

        private void ShowCalendarView1()
        {


            if (ViewManager._calenderViewHistory.Count == 0)
            {
                _navigationService.NavigateTo(ViewManager.uriCalendarViewOne.GetType());
                ViewManager._calenderViewHistory.Add(ViewManager.uriCalendarViewOne);
            }
            else
            {
                Uri lastPage = ViewManager._calenderViewHistory[ViewManager._calenderViewHistory.Count - 1];
                _navigationService.NavigateTo(lastPage.GetType());

            }
        }
             
    }
}