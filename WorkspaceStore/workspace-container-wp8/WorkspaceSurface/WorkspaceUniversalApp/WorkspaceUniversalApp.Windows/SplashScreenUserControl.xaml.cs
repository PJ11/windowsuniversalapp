﻿// Author : Sajini

using Windows.UI.Xaml.Controls;

namespace workspace
{
    #region N A M E S P A C E

    

    #endregion
    public partial class SplashScreenUserControl : UserControl
    {
        public SplashScreenUserControl()
        {
            InitializeComponent();
#if WP_80_SILVERLIGHT
            HockeyApp.CrashHandler.Instance.HandleCrashes();

            spStackPanel.Height = System.Windows.Application.Current.Host.Content.ActualHeight;
#endif
        }
    }
}
