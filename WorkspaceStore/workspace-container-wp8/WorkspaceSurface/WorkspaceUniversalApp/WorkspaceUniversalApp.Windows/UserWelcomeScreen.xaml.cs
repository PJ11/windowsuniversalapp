﻿namespace workspace
{
    #region N A M E S P A C E
    using System.Windows;    
    using System;
    using Common;
    using ActiveSyncPCL;
    using System.Collections.Generic;
    using System.Xml.Linq;
    using Common.ToastWithIndeterminateProgressBar;
    using workspace_datastore.Helpers;
    using Windows.UI.Xaml.Controls;
    using Windows.UI.Xaml.Navigation;
    using Windows.UI.Xaml;
    using workspace.Helpers;
    using WorkspaceUniversalApp;
    #endregion

    public partial class UserWelcomeScreen : Page
    {

        public UserWelcomeScreen()
        {
            InitializeComponent();
            //DataContext = App.ViewModel;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //ToastNotification toast = new ToastNotification();
            //toast.CreateToastNotificationForVertical("", "PIN Set Successfully");
            //if (!App.ViewModel.IsDataLoaded)
            {
                //App.ViewModel.LoadData();
            }
      
           // HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }


        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            //HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        //void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        //{
        //    e.Handled = true;
        //}

        private void Text_Loaded(object sender, RoutedEventArgs e)
        {
            //welcomeOne.Header = "Hello" + " " + App.mManager.settingsUserOptions.UserFirstName;
            //username.Text = "Hello" + " " + App.mManager.settingsUserOptions.UserFirstName;
        }

        private async void GetStarted_Click(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            /*ParsingUtils parse = new ParsingUtils();
            ActiveSync activeSync = ActiveSync.GetInstance();
            ToastWithIndeterminateProgressBar toast = new ToastWithIndeterminateProgressBar();
            toast.Title = "Please wait";
            toast.SubTitle = "Folder and Inbox Sync in progress";
            toast.MillisecondsUntilHidden = 50000;
            toast.Show();
            UserPage.IsLocked = true;
            GetStarted_Home.IsEnabled = false;
            GetStarted_Mail.IsEnabled = false;
            GetStarted_Contacts.IsEnabled = false;
            GetStarted_Calendar.IsEnabled = false;
            GetStarted_Browser.IsEnabled = false;
            GetStarted_Files.IsEnabled = false;
            //await activeSync.SyncFolderStructure();
            // set up the collection add/update/delete listener
            activeSync.EASCollectionChangedEvent += activeSync_EASCollectionChangedEvent;
            LoggingWP8.WP8Logger.LogMessage("About to pull Email inbox items.");
            await activeSync.SyncMail();
            activeSync.EASCollectionChangedEvent -= activeSync_EASCollectionChangedEvent;

            //LoggingWP8.WP8Logger.LogMessage("Finished EAS Email sync. Now parse response");
            //if (responseXML != null)
            //    await ParsingUtils.ParseMailContent(responseXML);
            LoggingWP8.WP8Logger.LogMessage("Finished Parsing Email Inbox data Items.");
            activeSync.EASCollectionChangedEvent += activeSync_EASCollectionChangedEventCalendar;
            await activeSync.SyncCalendar();
            activeSync.EASCollectionChangedEvent -= activeSync_EASCollectionChangedEventCalendar;

            activeSync.EASCollectionChangedEvent += activeSync_EASCollectionChangedEventContact;
            await activeSync.SyncContact();
            activeSync.EASCollectionChangedEvent -= activeSync_EASCollectionChangedEventContact;
            //call method to create default folders
            FileHandlingPCL.FileManager objFileMngr=new FileHandlingPCL.FileManager();
            await objFileMngr.CreateRootFolders();
            */
            SuspensionManager.SessionState["PanoramaPageOpeningFromWelcomePage"] = true;
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/PanoramaPage.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage(typeof(PanoramaPage));
            //toast.Hide();
        }

        private void activeSync_EASCollectionChangedEventContact(object sender, EASCollectionChangedEventArgs e)
        {
            ParsingUtils.ParseContactContent(e.Adds, e.Deletes, e.Changes, e.SoftDeletes, e.Folder);
        }
        private void activeSync_EASCollectionChangedEventCalendar(object sender, EASCollectionChangedEventArgs e)
        {
            ParsingUtils.ParseCalendarContent(e.Adds, e.Deletes, e.Changes, e.SoftDeletes, e.Folder);
        }

        /// <summary>
        /// Called when the client has RECEIVED the add/update/delete items list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void  activeSync_EASCollectionChangedEvent(object sender, EASCollectionChangedEventArgs e)
        {
            ParsingUtils.ParseMailContent(e.Adds, e.Deletes, e.Changes, e.SoftDeletes, e.Folder);
        }
    }
}