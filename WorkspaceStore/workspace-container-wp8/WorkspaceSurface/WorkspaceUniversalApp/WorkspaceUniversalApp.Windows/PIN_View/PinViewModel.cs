#define USE_ASYNC
using ActiveSyncPCL;
//using com.dell.workspace.Helpers;
using Common;
using CommonPCL;
using FileHandlingPCL;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

using System;
using System.ComponentModel;
//using System.Data.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

using System.Linq;
//using Workspace.Helpers;
using workspace_datastore.models;
using System.Collections.Generic;
using workspace_datastore.Managers;

using AbstractsPCL;
using KeyGenerationForPhone;
using GalaSoft.MvvmLight;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using workspace;
using WorkspaceUniversalApp;
using workspace.Helpers;

namespace WorkspaceUniversalApp.PIN_View
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class PinViewModel : ViewModelBase, INotifyPropertyChanged
    {
        #region VARIABLES
        private readonly IDataService _dataService;
        private readonly INavigationService _navigationService;
        public const string DigitCountTextPropertyName = "DigitCountText";
        private string _DigitCountText = string.Empty;
        public const string FailedAttemptPropertyName = "FailedAttempt";
        private string _FailedAttempt = string.Empty;
        public const string UpdateTextPropertyName = "UpdateText";
        private string _updateText = string.Empty;
        public const string InstructionTextPropertyName = "InstructionText";
        private string _instructionText = string.Empty;

        public const string FirstTextPropertyName = "FirstText";
        private string _firstText = string.Empty;
        public const string SetPINPropertyName = "SetPINText";
        private string _setPINText = string.Empty;

        public const string InstructionTextVisibilityPropertyName = "InstructionTextVisibility";
        private Windows.UI.Xaml.Visibility _instructionVisibility = Windows.UI.Xaml.Visibility.Visible;

        public const string SetPinVisibilityPropertyName = "SetPinVisibility";
        private Windows.UI.Xaml.Visibility _setPinVisibility = Windows.UI.Xaml.Visibility.Visible;

        public const string MaxAttemptsVisibilityPropertyName = "MaxAttemptsVisibility";
        private Windows.UI.Xaml.Visibility _maxAttemptsVisibility = Windows.UI.Xaml.Visibility.Collapsed;

        public const string AttemptCountVisibilityPropertyName = "AttemptCountVisibility";
        private Windows.UI.Xaml.Visibility _attemptCountVisibility = Windows.UI.Xaml.Visibility.Collapsed;

        public const string SetPINSelectionStartPropertyName = "SetPINSelectionStart";
        private int _setPINSelectionStart;

        private DispatcherTimer _dt;

        public string passCodeValue = "";
        public string passCodeValueInitial = "";
        public string passCodeValueConfirm = "";
        public string passCodeDisplay = "";
        public string _passwordChar = "\u25CF";
        //string pinLength = "4";
        static bool isConfirmPinMode = false;
        bool firstLogin = true;
        byte[] mDecryptedEncryptionKey = null;
        public bool mSuccessfullyChangedPIN;

        public bool mMandatoryPINChange = false;
        PinChangeSupport pinChange = new PinChangeSupport();
        #endregion

        #region PROPERTIES
        public bool AppStartOrWakeup { get; set; }
        public string Password { get; protected set; }
        public int DigitCount { get; protected set; }
        public int FailedAttempts { get; protected set; }
        public int FailedAttemptsBase { get; protected set; } // Load value from setting on page load
        public int TimeLeft { get; protected set; }
        public string EnterprisePassword { get; set; }
        public bool ForcedPINChange { get; set; }
        // ----------------------------------------------------------------  end
        public ICommand StartTimer { get; private set; }

        public bool isChangePin { get; set; }
        public int NewPINLength { get; set; }
        private static DateTime EndTime { get; set; }

        public string oldPinToBeChanged { get; set; }


        public bool newPin { get; set; }
        public string UpdateText
        {
            get
            {
                return _updateText;
            }

            set
            {
                if (_updateText == value)
                {
                    return;
                }

                _updateText = "Exceeded Login Attempts" + System.Environment.NewLine + "Try Again in " + value;
                RaisePropertyChanged(UpdateTextPropertyName);

            }
        }

        public string InstructionText
        {
            get
            {
                return _instructionText;
            }

            set
            {
                if (_instructionText == value)
                {
                    return;
                }

                RaisePropertyChanged(InstructionTextPropertyName);
            }
        }

        public string SetPINText
        {
            get
            {
                return _setPINText;
            }

            set
            {
                if (_setPINText == value)
                {
                    return;
                }
                _setPINText = value;
                RaisePropertyChanged(SetPINPropertyName);
            }
        }

        public int SetPINSelectionStart
        {
            get
            {
                return _setPINSelectionStart;
            }

            set
            {
                if (_setPINSelectionStart == value)
                {
                    return;
                }
                _setPINSelectionStart = value;
                RaisePropertyChanged(SetPINSelectionStartPropertyName);
            }
        }
        public string FirstText
        {
            get
            {
                return _firstText;
            }

            set
            {
                if (_firstText == value)
                {
                    return;
                }
                _firstText = value;
                RaisePropertyChanged(FirstTextPropertyName);
            }
        }
        public string DigitCountText
        {
            get
            {
                return _DigitCountText;
            }

            set
            {
                if (_DigitCountText == value)
                {
                    return;
                }
                _DigitCountText = value;
                RaisePropertyChanged(DigitCountTextPropertyName);

            }
        }

        public string FailedAttempt
        {
            get
            {
                return _FailedAttempt;
            }

            set
            {
                if (_FailedAttempt == value)
                {
                    return;
                }

                _FailedAttempt = "Wrong Pin. Try again. " + "Will Timeout after " + value + " more attempts.";
                RaisePropertyChanged(FailedAttemptPropertyName);

            }
        }

        public Visibility InstructionTextVisibility
        {
            get
            {
                return _instructionVisibility;
            }

            set
            {
                if (_instructionVisibility == value)
                {
                    return;
                }

                _instructionVisibility = value;
                RaisePropertyChanged(InstructionTextVisibilityPropertyName);
            }
        }

        public Visibility SetPinVisibility
        {
            get
            {
                return _setPinVisibility;
            }

            set
            {
                if (_setPinVisibility == value)
                {
                    return;
                }

                _setPinVisibility = value;
                RaisePropertyChanged(SetPinVisibilityPropertyName);
            }
        }

        public Visibility MaxAttemptsVisibility
        {
            get
            {
                return _maxAttemptsVisibility;
            }

            set
            {
                if (_maxAttemptsVisibility == value)
                {
                    return;
                }

                _maxAttemptsVisibility = value;
                RaisePropertyChanged(MaxAttemptsVisibilityPropertyName);
            }
        }

        public Visibility AttemptCountVisibility
        {
            get
            {
                return _attemptCountVisibility;
            }

            set
            {
                if (_attemptCountVisibility == value)
                {
                    return;
                }

                _attemptCountVisibility = value;
                RaisePropertyChanged(AttemptCountVisibilityPropertyName);
            }
        }
        public event EventHandler<TextArgs> Feedback;

        public class TextArgs : EventArgs
        {
            private string szMessage;
            public TextArgs(string TextMessage)
            {
                szMessage = TextMessage;
            }
            public string Message
            {
                get { return szMessage; }
                set { szMessage = value; }
            }
        }

        public ICommand ClearPinTextCommand { get; set; }

        #endregion


        #region CONSTRUCTOR
        /// <summary>
        /// Initializes a new instance of the PinViewModel class.
        /// </summary>
        public PinViewModel(IDataService dataService, INavigationService navigationService)
        {
            _navigationService = navigationService;
            _dataService = dataService;
            _dataService.GetData(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    UpdateText = "Exceeded Login attempts";
                });

            ClearPinTextCommand = new RelayCommand(ClearPinText);
        }

        private void ClearPinText()
        {
            if (passCodeValue.Length > 0)
            {
                passCodeValue = passCodeValue.Remove(passCodeValue.Length - 1, 1);
                passCodeDisplay = passCodeDisplay.Remove(passCodeDisplay.Length - 1, 1);
                SetPINText = passCodeDisplay;
            }
        }

        #endregion

        #region LOCAL AND GLOBAL METHODS
        public void PageLoad()
        {
            isConfirmPinMode = false;
            //  var MyPinScreenViewModel = (PinViewModel)DataContext;
            Feedback += new EventHandler<WorkspaceUniversalApp.PIN_View.PinViewModel.TextArgs>(Feedback_Received);

            if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_UNINITIALIZED))
                if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED))
                    if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE))
                    { firstLogin = false; }

            if (firstLogin)
            {
                FirstText = "Welcome To Workspace";
                GetPINDigitFromSettings();
                DigitCountText = "Enter Your Desired " + DigitCount + "-digit PIN Number";
            }

            //else if(newPin)
            //{
            //    FirstText = "Set New Pin";
            //    GetPINDigitFromSettings();
            //    DigitCountText = "Enter a new " + DigitCount + "-digit PIN Number";
            //}

            else
            {
                FirstText = "Welcome To Workspace";
                //    SetPIN.Height = 110;
                //InstructionsText.Padding = new Thickness(18, 25, 0, 0);
                //  InstructionsText.Margin = new Thickness(46, 108, 20, 120);
                InstructionTextVisibility = Visibility.Visible;
                DigitCountText = "Enter Your Pin Number";
                AttemptCountVisibility = Visibility.Collapsed;
                UpdateSettingsFromCCM();
            }

            //If the application was exited in locked state need not execute the block.
            if (!App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE))
            {
                //on back event from other screen control should be reset and test box will be focused.
                passCodeDisplay = "";
                passCodeValue = "";
                SetPINText = "";
                // SetPIN.Focus();
            }

        }

        public void StartTimerMethod(int TimeLeft)
        {
            if (this._dt == null)
            {
                this._dt = new DispatcherTimer();
                this._dt.Interval = TimeSpan.FromMilliseconds(250);
                //this._dt.Tick += new EventHandler(dispatcherTimer_Tick);
            }

            //if (EndTime == DateTime.MinValue)
            {
                EndTime = DateTime.Now + (TimeSpan)TimeSpan.FromSeconds(TimeLeft);
            }

            UpdateText = string.Format("Seconds: {0}", TimeLeft);
            this._dt.Start();
            //return TimeLeft;
        }

        void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            var remaining = EndTime - DateTime.Now;
            int remainingSeconds = (int)remaining.TotalSeconds;

            UpdateText = string.Format("Seconds: {0}", remainingSeconds);

            if (remaining.TotalSeconds <= 0)
            {
                this._dt.Stop();
                RaiseFeedback("DONE");
            }

        }

        public void getDigitCount(int count)
        {
            DigitCountText = string.Format("{0}", count);
        }

        public void getFailedLoginCount(int count)
        {
            FailedAttempt = string.Format("{0}", count);
        }

        private void RaiseFeedback(string p)
        {
            EventHandler<TextArgs> handler = Feedback;
            if (handler != null)
            {
                handler(null, new TextArgs(p));
            }
        }

        public void GetPINDigitFromSettings()
        {
            //it waas throwing exception due to null value
            if (App.mManager.settingsUserOptions == null)
            {
                App.mAppState.resetAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED);
                App.mAppState.resetAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS);
                App.mAppState.resetAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_UNINITIALIZED);
                string pageToLoad = "/Workspace;component/CCMLoginScreen/LoginScreen.xaml";
                // _navigationService.NavigateTo(new Uri(pageToLoad, UriKind.Relative));
            }
            else
            {
                DigitCount = App.mManager.settingsUserOptions.PinLength;
                if (-1 == DigitCount)
                {
                    LoggingWP8.WP8Logger.LogMessage("App.mManager.loadUserSettings() returned false");
                    //CustomMessageBox msg = new CustomMessageBox()
                    //{
                    //    Message = "CCM credentials Error.You will be redirected to CCM login Page again",
                    //    LeftButtonContent = "OK",
                    //};
                    //msg.Show();
                    //Common.Message message = new Common.Message();
                    //message.PhoneMessage("CCM credentials Error.You will be redirected to CCM login Page again");
                    //AppStartOrWakeup = true;
                    App.mAppState.resetAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED);
                    App.mAppState.resetAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS);
                    App.mAppState.resetAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_UNINITIALIZED);
                    string pageToLoad = "/Workspace;component/CCMLoginScreen/LoginScreen.xaml";
                    //_navigationService.NavigateTo(new Uri(pageToLoad, UriKind.Relative));
                }
                FailedAttempts = App.mManager.settingsUserOptions.LoginAttemptsAllowed;
                if (-1 == FailedAttempts)
                {
                    LoggingWP8.WP8Logger.LogMessage("App.mManager.loadUserSettings() returned false");

                }
                GetTimeOutValueFromSettings(App.mManager.settingsUserOptions.LoginFailureAction);
            }
        }

        public async Task<bool> UpdateSettingsFromCCM()
        {
            bool success = false;
            int digitCount = 0;
            int timeLeft = 0;
            int failedAttempts = 0;
            success = App.mManager.loadUnencryptedUserSettings(ref digitCount, ref timeLeft, ref failedAttempts);  // await App.mManager.loadUserSettings().ConfigureAwait(false);
            if (success)
            {
                // If we get to this point, we have: authenticated with CCM, recv'd config, set up PIN and created master key
                FailedAttemptsBase = failedAttempts;
                FailedAttempts = failedAttempts; // App.mManager.settingsUserOptions.LoginAttemptsAllowed;
                GetTimeOutValueFromSettings((LoginFailureAction)timeLeft);
                DigitCount = digitCount; // App.mManager.settingsUserOptions.PinLength;
                //If application was exited in locked state need to display the locked screen after getting settings from CCM.
                if (App.mAppState.checkAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE))
                {
                    DisplayLockScreen();
                }
            }
            else
            {
                LoggingWP8.WP8Logger.LogMessage("App.mManager.loadUserSettings() returned false");
                //CustomMessageBox msg = new CustomMessageBox()
                //{
                //    Message = "CCM file storage Error. You will be redirected to CCM login Page again",
                //    LeftButtonContent = "OK",
                //};
                //msg.Show();
                //Common.Message message = new Common.Message();
                //message.PhoneMessage("CCM file storage Error. You will be redirected to CCM login Page again");
                App.mAppState.resetAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_AUTHENTICATED);
                App.mAppState.resetAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS);
                App.mAppState.resetAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_UNINITIALIZED);
                string pageToLoad = "/Workspace;component/CCMLoginScreen/LoginScreen.xaml";
                //_navigationService.NavigateTo(new Uri(pageToLoad, UriKind.Relative));
            }
            return success;
        }

        private void GetTimeOutValueFromSettings(LoginFailureAction loginFailureAction)
        {
            switch (loginFailureAction)
            {
                case LoginFailureAction.LOGIN_FAILURE_TO1:
                    TimeLeft = 60;
                    break;
                case LoginFailureAction.LOGIN_FAILURE_TO2:
                    TimeLeft = 120;
                    break;
                case LoginFailureAction.LOGIN_FAILURE_TO3:
                    TimeLeft = 180;
                    break;
                case LoginFailureAction.LOGIN_FAILURE_TO4:
                    TimeLeft = 240;
                    break;
                case LoginFailureAction.LOGIN_FAILURE_TO5:
                    TimeLeft = 300;
                    break;
                default:
                    break;
                //TBD : Wipe out case
            }
        }


        private void DisplayLockScreen()
        {
            FirstText = "Workspace Locked";
            MaxAttemptsVisibility = Windows.UI.Xaml.Visibility.Visible;
            LoginAttemptFailure(TimeLeft);
            SetPINText = string.Empty;
            SetPinVisibility = Windows.UI.Xaml.Visibility.Collapsed;
            //  InstructionText.Margin = new Thickness(46, 108, 20, 120);
            DigitCountText = "Enter Your Pin Number ";
            InstructionTextVisibility = Windows.UI.Xaml.Visibility.Collapsed;
            AttemptCountVisibility = Windows.UI.Xaml.Visibility.Collapsed;
            passCodeDisplay = "";
            passCodeValue = "";
        }


        public async void PasswordTextBox_KeyUp(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if (e.Key != Windows.System.VirtualKey.NonConvert)
            {
                if (SetPINText.Length <= DigitCount - 1)
                {
                    //modify new passcode according to entered key

                    passCodeValue = GetNewPasscode(passCodeValue, e);

                    if (!isConfirmPinMode)
                        passCodeValueInitial = passCodeValue;
                    else
                        passCodeValueConfirm = passCodeValue;


                    SetPINText = passCodeDisplay; // _enteredPasscode;
                    if (isChangePin)
                    {
                        AppStartOrWakeup = true;
                        passCodeValueInitial = passCodeValue;
                        isConfirmPinMode = false;
                    }



                    //replace text by *

                    //SetPIN.Text = Regex.Replace(_enteredPasscode, @".", _passwordChar);
                    //take cursor to end of string
                    SetPINSelectionStart = SetPINText.Length;

                    if (passCodeValue.Length == DigitCount)
                    {
                        bool needToChangePage = true;
                        string pageToLoad = "/Workspace;component/PanoramaPage.xaml";

                        if (!AppStartOrWakeup)
                        {
                            // Setting up PIN and confirm PIN
                            if (!isConfirmPinMode)
                            {
                                FirstText = "Confirm Pin";
                                DoConfirmPin();
                                needToChangePage = false;
                            }
                            else
                            {
                                if (passCodeValueInitial.Equals(passCodeValueConfirm))
                                {
                                    isConfirmPinMode = true;
                                    // Once the PIN set and Confirm PIN set are complete, set the state to app initialized.
                                    App.mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_APP_INITIALIZED);

                                    if (App.mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE)
                                        & App.mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP))
                                    {
                                        pageToLoad = "/Workspace;component/PanoramaPage.xaml";

                                    }
                                    else
                                    {
                                        App.mAppState.CreateMasterKey(passCodeValueConfirm);
                                        mDecryptedEncryptionKey = App.mAppState.ValidatePin(passCodeValueConfirm);
                                        await SetDBEncryptionKeyAndBGTaskAgentFile();
                                        App.mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE);

                                        // Store user settings in CCM
                                        App.mManager.storeUserSettings();

                                        if (null != EnterprisePassword && EnterprisePassword.Length > 0)
                                        {
                                            // They are using SSO (AD Credentials) so, bypass password and set up account
                                            Account acct = new Account();
                                            if (App.mManager.settingsUserOptions != null)
                                            {
                                                acct.domainName = App.mManager.settingsUserOptions.EasDomain;
                                                acct.authUser = App.mManager.settingsUserOptions.EasUserName;
                                                acct.emailAddress = App.mManager.settingsUserOptions.EasEmailAddress;
                                                acct.serverUrl = App.mManager.settingsUserOptions.EasHost;
                                                acct.authPass = EnterprisePassword;
                                            }
                                            else
                                            {
                                                // What do we doo if this is null?
                                            }

                                            EASAccountSetupHelper helper = new EASAccountSetupHelper(acct, null);
                                            int result = await helper.authenticateUserSSO();
                                            if (result == 0)
                                            {
                                                pageToLoad = "/Workspace;component/UserWelcomeScreen.xaml";
                                            }
                                            else
                                            {
                                                needToChangePage = false;
                                                string EASMessageString = "";
                                                if (result == 177)
                                                {
                                                    EASMessageString = "Too many devices provisioned.  Please delete one or more devices and try again.";
                                                }
                                                else
                                                {
                                                    EASMessageString = string.Format("Status returned from EAS Authenticate (Autodiscover and Provision) is {0}", result);
                                                }

                                                //CustomMessageBox messageBox = new CustomMessageBox()
                                                //{
                                                //    Caption = "Exchange Autodiscover/Provision Error",
                                                //    Message = EASMessageString,
                                                //    LeftButtonContent = "OK",

                                                //};
                                                //messageBox.Dismissed += (s1, e1) =>
                                                //{
                                                //    switch (e1.Result)
                                                //    {
                                                //        case CustomMessageBoxResult.LeftButton:
                                                //            pageToLoad = "/Workspace;component/Login/View/ManualSignInView.xaml";
                                                //            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(pageToLoad, UriKind.Relative));
                                                //            break;
                                                //        case CustomMessageBoxResult.RightButton:
                                                //            break;
                                                //        case CustomMessageBoxResult.None:
                                                //            break;
                                                //        default:
                                                //            break;
                                                //    }
                                                //};
                                                //messageBox.Show();
                                            }
                                        }
                                        else
                                        {
                                            if (!App.mManager.settingsUserOptions.DynamicUserInfo && App.mManager.settingsUserOptions.EasPassword.Length > 0)
                                            {
                                                Account acct = new Account();
                                                if (App.mManager.settingsUserOptions != null)
                                                {
                                                    acct.domainName = App.mManager.settingsUserOptions.EasDomain;
                                                    acct.authUser = App.mManager.settingsUserOptions.EasUserName;
                                                    acct.emailAddress = App.mManager.settingsUserOptions.EasEmailAddress;
                                                    acct.serverUrl = App.mManager.settingsUserOptions.EasHost;
                                                    acct.authPass = App.mManager.settingsUserOptions.EasPassword;
                                                }
                                                else
                                                {
                                                    // What do we doo if this is null?
                                                }

                                                EASAccountSetupHelper helper = new EASAccountSetupHelper(acct, null);
                                                int result = await helper.authenticateUserSSO();
                                                if (result == 0)
                                                {
                                                    pageToLoad = "/Workspace;component/UserWelcomeScreen.xaml";
                                                }
                                                else
                                                {
                                                    needToChangePage = false;
                                                    string EASMessageString = "";
                                                    if (result == 177)
                                                    {
                                                        EASMessageString = "Too many devices provisioned.  Please delete one or more devices and try again.";
                                                    }
                                                    else
                                                    {
                                                        EASMessageString = string.Format("Status returned from EAS Authenticate (Autodiscover and Provision) is {0}", result);
                                                    }

                                                    //CustomMessageBox messageBox = new CustomMessageBox()
                                                    //{
                                                    //    Caption = "Exchange Autodiscover/Provision Error",
                                                    //    Message = EASMessageString,
                                                    //    LeftButtonContent = "OK",

                                                    //};
                                                    //messageBox.Dismissed += (s1, e1) =>
                                                    //{
                                                    //    switch (e1.Result)
                                                    //    {
                                                    //        case CustomMessageBoxResult.LeftButton:
                                                    //            pageToLoad = "/Workspace;component/Login/View/ManualSignInView.xaml";
                                                    //            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(pageToLoad, UriKind.Relative));
                                                    //            break;
                                                    //        case CustomMessageBoxResult.RightButton:
                                                    //            break;
                                                    //        case CustomMessageBoxResult.None:
                                                    //            break;
                                                    //        default:
                                                    //            break;
                                                    //    }
                                                    //};
                                                    //messageBox.Show();
                                                }
                                            }
                                            else
                                            {
                                                pageToLoad = "/Workspace;component/EmailLogin/View/EmailLogin.xaml";
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    DoConfirmPin(true);
                                    needToChangePage = false;
                                }
                            }
                        }

                        else if (newPin)
                        {
                            if (!isConfirmPinMode)
                            {
                                FirstText = "Confirm Pin";
                                DoConfirmPin();
                                needToChangePage = false;
                            }
                            else
                            {
                                if (passCodeValueInitial.Equals(passCodeValueConfirm))
                                {
                                    isConfirmPinMode = true;
                                    // if both th pin are equal, change pin 
                                    mSuccessfullyChangedPIN = pinChange.UserPinChange(oldPinToBeChanged, passCodeValueConfirm).Result;
                                    // App.mAppState.CreateMasterKey(passCodeValueConfirm);

                                    EASAccount.GetAccountInfo += new EASAccount.GetAccInfoHandler(GetAccountInfo);
                                    App.mActiveSyncManager = ActiveSyncManager.sharedManagerForEmailAddress(App.mManager.settingsUserOptions.EasEmailAddress);

                                    if (mSuccessfullyChangedPIN)
                                    {
                                        //   If the PIN length has changed, then we must store the new PIN length to our settings
                                        App.mManager.settingsUserOptions.Settings[HaloPolicySettingsAbstract.KEY_PIN_LENGTH] = DigitCount.ToString();
                                        App.mManager.storeUserSettings();

                                        if (mMandatoryPINChange)
                                        {
                                            mMandatoryPINChange = false;
                                            App.mAppState.resetAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE);
                                            App.mAppState.resetAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE);
                                            App.mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP);
                                            App.mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE);
                                            //_navigationService.NavigateTo(new Uri("/Workspace;component/PanoramaPage.xaml?PINCHANGED=1", UriKind.Relative));

                                        }
                                        else
                                            if ((Windows.UI.Xaml.Window.Current.Content as Frame).CanGoBack)
                                                (Windows.UI.Xaml.Window.Current.Content as Frame).GoBack();

                                    }
                                }
                                else
                                    DoConfirmPin(true);
                                needToChangePage = false;
                            }

                        }

                        else
                        {

                            // PIN is already set up, master key set, now user is typing in his PIN to get access to Application
                            if (App.mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE))
                            {
                                mDecryptedEncryptionKey = App.mAppState.ValidatePin(passCodeValueInitial);

                                if (null != mDecryptedEncryptionKey)
                                {
                                    // Set the Active Sync Manager based on the email address of CCM.
                                    await SetDBEncryptionKeyAndBGTaskAgentFile();

                                    // Load CCM storage settings
                                    bool loadedUserSettings = await App.mManager.loadUserSettings();
                                    if (loadedUserSettings)
                                    {
                                        if (isChangePin) // If user wants to change existing pin
                                        {
                                            pageToLoad = string.Format("/Workspace;component/NewPINScreen.xaml?pin={0}", passCodeValue);
                                            oldPinToBeChanged = passCodeValue;
                                            NewPINLength = App.mManager.settingsUserOptions.PinLength;
                                            if (NewPINLength > 0)
                                            {
                                                pageToLoad += "&PINLength=" + NewPINLength;
                                                // clear the app states
                                                App.mAppState.resetAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE);
                                                App.mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE);

                                            }
                                            // _navigationService.NavigateTo(new Uri(uri, UriKind.Relative));
                                        }

                                        // Has the user gone through the Welcome -> Get Started yet?
                                        else if (App.mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP))
                                        {
                                            //PJ. Why is this registered here and not on the Panorama screen?
                                            EASAccount.GetAccountInfo += new EASAccount.GetAccInfoHandler(GetAccountInfo);
                                            App.mActiveSyncManager = ActiveSyncManager.sharedManagerForEmailAddress(App.mManager.settingsUserOptions.EasEmailAddress);
                                            pageToLoad = "/Workspace;component/PanoramaPage.xaml";
                                        }
                                        else
                                        {
                                            if (null != EnterprisePassword && EnterprisePassword.Length > 0)
                                            {
                                                // They are using SSO (AD Credentials) so, bypass password and set up account
                                                Account acct = new Account();
                                                if (App.mManager.settingsUserOptions != null)
                                                {
                                                    acct.domainName = App.mManager.settingsUserOptions.EasDomain;
                                                    acct.authUser = App.mManager.settingsUserOptions.EasUserName;
                                                    acct.emailAddress = App.mManager.settingsUserOptions.EasEmailAddress;
                                                    acct.serverUrl = App.mManager.settingsUserOptions.EasHost;
                                                }
                                                else
                                                {
                                                    // What do we doo if this is null?
                                                }

                                                EASAccountSetupHelper helper = new EASAccountSetupHelper(acct, null);
                                                int result = await helper.authenticateUserSSO();
                                                if (result == 0)
                                                {
                                                    pageToLoad = "/Workspace;component/UserWelcomeScreen.xaml";
                                                }
                                                else
                                                {
                                                    needToChangePage = false;
                                                    string EASMessageString = "";
                                                    if (result == 177)
                                                    {
                                                        EASMessageString = "Too many devices provisioned.  Please delete one or more devices and try again.";
                                                    }
                                                    else
                                                    {
                                                        EASMessageString = string.Format("Status returned from EAS Authenticate (Autodiscover and Provision) is {0}", result);
                                                    }

                                                    //CustomMessageBox messageBox = new CustomMessageBox()
                                                    //{
                                                    //    Caption = "Exchange Autodiscover/Provision Error",
                                                    //    Message = EASMessageString,
                                                    //    LeftButtonContent = "OK",

                                                    //};
                                                    //messageBox.Dismissed += (s1, e1) =>
                                                    //{
                                                    //    switch (e1.Result)
                                                    //    {
                                                    //        case CustomMessageBoxResult.LeftButton:
                                                    //            pageToLoad = "/Workspace;component/Login/View/ManualSignInView.xaml";
                                                    //            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(pageToLoad, UriKind.Relative));
                                                    //            break;
                                                    //        case CustomMessageBoxResult.RightButton:
                                                    //            break;
                                                    //        case CustomMessageBoxResult.None:
                                                    //            break;
                                                    //        default:
                                                    //            break;
                                                    //    }
                                                    //};
                                                    //messageBox.Show();
                                                }
                                            }
                                            else
                                            {
                                                if (!App.mManager.settingsUserOptions.DynamicUserInfo && App.mManager.settingsUserOptions.EasPassword.Length > 0)
                                                {
                                                    Account acct = new Account();
                                                    if (App.mManager.settingsUserOptions != null)
                                                    {
                                                        acct.domainName = App.mManager.settingsUserOptions.EasDomain;
                                                        acct.authUser = App.mManager.settingsUserOptions.EasUserName;
                                                        acct.emailAddress = App.mManager.settingsUserOptions.EasEmailAddress;
                                                        acct.serverUrl = App.mManager.settingsUserOptions.EasHost;
                                                        acct.authPass = App.mManager.settingsUserOptions.EasPassword;
                                                    }
                                                    else
                                                    {
                                                        // What do we doo if this is null?
                                                    }

                                                    EASAccountSetupHelper helper = new EASAccountSetupHelper(acct, null);
                                                    int result = await helper.authenticateUserSSO();
                                                    if (result == 0)
                                                    {
                                                        pageToLoad = "/Workspace;component/UserWelcomeScreen.xaml";
                                                    }
                                                    else
                                                    {
                                                        needToChangePage = false;
                                                        string EASMessageString = "";
                                                        if (result == 177)
                                                        {
                                                            EASMessageString = "Too many devices provisioned.  Please delete one or more devices and try again.";
                                                        }
                                                        else
                                                        {
                                                            EASMessageString = string.Format("Status returned from EAS Authenticate (Autodiscover and Provision) is {0}", result);
                                                        }

                                                        //CustomMessageBox messageBox = new CustomMessageBox()
                                                        //{
                                                        //    Caption = "Exchange Autodiscover/Provision Error",
                                                        //    Message = EASMessageString,
                                                        //    LeftButtonContent = "OK",

                                                        //};
                                                        //messageBox.Dismissed += (s1, e1) =>
                                                        //{
                                                        //    switch (e1.Result)
                                                        //    {
                                                        //        case CustomMessageBoxResult.LeftButton:
                                                        //            pageToLoad = "/Workspace;component/Login/View/ManualSignInView.xaml";
                                                        //            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(pageToLoad, UriKind.Relative));
                                                        //            break;
                                                        //        case CustomMessageBoxResult.RightButton:
                                                        //            break;
                                                        //        case CustomMessageBoxResult.None:
                                                        //            break;
                                                        //        default:
                                                        //            break;
                                                        //    }
                                                        //};
                                                        //messageBox.Show();
                                                    }
                                                }
                                                else
                                                {
                                                    pageToLoad = "/Workspace;component/EmailLogin/View/EmailLogin.xaml";
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // Something catastrophic
                                        // Need to wipe container and start from scratch
                                        //CustomMessageBox messageBox = new CustomMessageBox()
                                        //{
                                        //    Caption = "Catastrophic failure!",
                                        //    Message = "The CCM settings file is corrupt. Tap OK to wipe container and restart",
                                        //    LeftButtonContent = "OK",

                                        //};
                                        //messageBox.Dismissed += (s1, e1) =>
                                        //{
                                        //    switch (e1.Result)
                                        //    {
                                        //        case CustomMessageBoxResult.LeftButton:
                                        //            // TODO We must wipe container and bring user back to CCM login screen to start again.
                                        //            break;
                                        //        case CustomMessageBoxResult.RightButton:
                                        //            break;
                                        //        case CustomMessageBoxResult.None:
                                        //            break;
                                        //        default:
                                        //            break;
                                        //    }
                                        //};
                                        //messageBox.Show();
                                    }
                                }
                                else
                                {
                                    //wrong Pin
                                    //Decrement the number of failed attempts
                                    FirstText = "Workspace Timed Out";
                                    FailedAttempts -= 1;
                                    //If equal to 0
                                    if (FailedAttempts == 0)
                                    {
                                        App.mAppState.setAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE);
                                        MaxAttemptsVisibility = Visibility.Visible;
                                        LoginAttemptFailure(TimeLeft);
                                        SetPINText = string.Empty;
                                        SetPinVisibility = Visibility.Collapsed;
                                        //  InstructionText.Margin = new Thickness(46, 108, 20, 120);
                                        DigitCountText = "Enter Your Pin Number ";
                                        InstructionTextVisibility = Visibility.Collapsed;
                                        AttemptCountVisibility = Visibility.Collapsed;
                                        //FailedAttempts = App.mManager.settingsUserOptions.LoginAttemptsAllowed;
                                        int failAttempts = 0;
                                        if (App.mManager.loadLoginAttemptsAllowedSettings(ref failAttempts))
                                        {
                                            FailedAttempts = failAttempts;
                                            FailedAttemptsBase = failAttempts;
                                        }
                                        else
                                        {
                                            // Load value from FailedAttemptsBase property
                                            FailedAttempts = FailedAttemptsBase;
                                        }
                                        needToChangePage = false;
                                        passCodeDisplay = "";
                                        passCodeValue = "";


                                    }
                                    else
                                    {

                                        getFailedLoginCount(FailedAttempts);
                                        AttemptCountVisibility = Visibility.Visible;
                                        InstructionTextVisibility = Visibility.Collapsed;
                                        SetPINText = string.Empty;
                                        needToChangePage = false;
                                        passCodeDisplay = "";
                                        passCodeValue = "";
                                    }

                                }

                            }
                        }
                        if (needToChangePage)
                        {
                            //_navigationService.NavigateTo(new Uri(pageToLoad, UriKind.Relative));
                            //if ((Application.Current.RootVisual as PhoneApplicationFrame).CanGoBack && (Application.Current.RootVisual as PhoneApplicationFrame).BackStack.Count() != 0 && (!isChangePin))//true if its locked due to inactive timeout. 
                            //    (Application.Current.RootVisual as PhoneApplicationFrame).GoBack();
                            //else
                            //{
                            //    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(pageToLoad, UriKind.Relative));
                            //}

                        }
                    }

                    e.Handled = true;
                }
            }
            else
            {
                e.Handled = true;
                TextBox myTextBox = sender as TextBox;
                myTextBox.Text = myTextBox.Text.Replace(".", "");
                myTextBox.SelectionStart = myTextBox.Text.Length;

            }
        }

        private EASAccount GetAccountInfo(string email)
        {
            try
            {
                //Create object of account manager to get account details from database
                AccountManager objAcc = new AccountManager();
                //Create EASAccount object to set account value
                EASAccount objEAS = null;
                //call method to get account info
#if USE_ASYNC
                List<Account> list = objAcc.GetAccountInfoAsync(email).Result;
#else
                var list = objAcc.GetAccountInfo(email).ToList();
#endif
                if (list != null && list.Count > 0)
                {
                    objEAS = new EASAccount();
                    //get single result
                    var acc = list.Single();
                    if (acc != null)
                    {
                        //set EASAccount object
                        objEAS.AccountId = acc.id;
                        objEAS.DomainName = acc.domainName;
                        objEAS.AuthUser = acc.authUser;
                        objEAS.AuthPassword = acc.authPass;
                        objEAS.EmailAddress = acc.emailAddress;
                        objEAS.ServerUrl = acc.serverUrl;
                        objEAS.PolicyKey = Convert.ToUInt32(acc.policyKey);
                        objEAS.ProtocolVersion = acc.protocolVersion;
                    }
                }
                //return object
                return objEAS;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string GetNewPasscode(string oldPasscode, Windows.UI.Xaml.Input.KeyRoutedEventArgs keyEventArgs)
        {
            string newPasscode = string.Empty;
            switch (keyEventArgs.Key)
            {
                //case Key.D0:
                //case Key.D1:
                //case Key.D2:
                //case Key.D3:
                //case Key.D4:
                //case Key.D5:
                //case Key.D6:
                //case Key.D7:
                //case Key.D8:
                //case Key.D9:
                //    newPasscode = oldPasscode + (keyEventArgs.PlatformKeyCode - 48);
                //    passCodeDisplay += _passwordChar;
                //    break;
                //case Key.Back:
                //    if (oldPasscode.Length > 0)
                //        newPasscode = oldPasscode.Substring(0, oldPasscode.Length - 1);
                //    if (passCodeDisplay.Length > 0)
                //        passCodeDisplay = passCodeDisplay.Remove(passCodeDisplay.Length - 1, 1);
                //    break;
                default:
                    //others
                    newPasscode = oldPasscode;
                    break;
            }
            return newPasscode;
        }
        private void DoConfirmPin(bool noMatch = false)
        {
            isConfirmPinMode = true;
            passCodeDisplay = "";
            passCodeValue = "";
            SetPINText = "";
            //   InstructionsText.Margin = new Thickness(46, 108, 20, 120);
            //if no match is true, we want to retain the screen
            if (noMatch)
                DigitCountText = "No match, please try again";
            else
            {
                DigitCountText = "Please Confirm Your Pin Number";
            }

        }
        public void PasswordTextBox_KeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {

            if (SetPINText.Length >= DigitCount - 1)
            {
                e.Handled = true;
            }
        }

        private void LoginAttemptFailure(int TimeLeft)
        {
            StartTimerMethod(TimeLeft);
        }

        public void Feedback_Received(object sender, WorkspaceUniversalApp.PIN_View.PinViewModel.TextArgs e)
        {
            //MessageBox.Show("DONE");
            App.mAppState.resetAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE);
            MaxAttemptsVisibility = Visibility.Collapsed;
            SetPINText = string.Empty;
            SetPinVisibility = Visibility.Visible;
            // InstructionsText.Margin = new Thickness(46, 108, 20, 120);
            DigitCountText = "Enter Your Pin Number ";
            InstructionTextVisibility = Visibility.Visible;

        }

        public async Task SetDBEncryptionKeyAndBGTaskAgentFile()
        {
            if (mDecryptedEncryptionKey != null)
            {
                workspace_datastore.ConstantData.DBKey = mDecryptedEncryptionKey;
                workspace_datastore.ConstantData.DBHexKey = BitConverter.ToString(workspace_datastore.ConstantData.DBKey).Replace("-", "").Substring(0, 64);
                // byte[] myDeviceID = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");

                using (Mutex mutex = new Mutex(true, "XApplicationData"))
                {
                    mutex.WaitOne();
                    try
                    {
                        byte[] validBytes = new byte[24];
                        // myDeviceID.CopyTo(validBytes, 0); // First 20 bytes
                        // validBytes.Concat(myDeviceID.Take(4));
                        string keystring = Convert.ToBase64String(validBytes);
                        FileManager fileMan = new FileManager();
                        await fileMan.CreateFile("x_app_info", "", mDecryptedEncryptionKey, keystring);
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                    }
                    finally
                    {
                        mutex.ReleaseMutex();
                    }
                }

#if NEEDED
                string key = Convert.ToBase64String(EEK);
                string my_String = Regex.Replace(key, @"[^a-zA-Z]+", "");

                workspace_datastore.ConstantData.DBPwd = my_String;
#endif
                //i think in this variable value will be first time
                if (!string.IsNullOrEmpty(passCodeValueConfirm))
                {
                    await workspace_datastore.SqlcipherManager.CreateDBTables();

                }
            }

            // FOR NOW
            if (!string.IsNullOrEmpty(passCodeValueConfirm))
            {
                await workspace_datastore.SqlcipherManager.CreateDBTables();

            }
        }
        private async Task DeleteDB()
        {
            if (mDecryptedEncryptionKey != null)
            {
                workspace_datastore.ConstantData.DBKey = mDecryptedEncryptionKey;
                workspace_datastore.ConstantData.DBHexKey = BitConverter.ToString(workspace_datastore.ConstantData.DBKey).Replace("-", "").Substring(0, 64);
                // byte[] myDeviceID = (byte[])Microsoft.Phone.Info.DeviceExtendedProperties.GetValue("DeviceUniqueId");
                await workspace_datastore.SqlcipherManager.DeleteDBTables();
                using (Mutex mutex = new Mutex(true, "XApplicationData"))
                {
                    mutex.WaitOne();
                    try
                    {
                        byte[] validBytes = new byte[24];
                        //myDeviceID.CopyTo(validBytes, 0); // First 20 bytes
                        //validBytes.Concat(myDeviceID.Take(4));
                        string keystring = Convert.ToBase64String(validBytes);
                        FileManager fileMan = new FileManager();
                        await fileMan.DeleteFile("x_app_info", "");//, mDecryptedEncryptionKey, keystring);
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                    }
                    finally
                    {
                        mutex.ReleaseMutex();
                    }
                }

            }
        }
        #endregion

        public async Task PinScreenChanged(int value)
        {
            if (SetPINText.Length <= DigitCount - 1)
            {
                passCodeValue += value;
                passCodeDisplay += _passwordChar;

                if (!isConfirmPinMode)
                    passCodeValueInitial = passCodeValue;
                else
                    passCodeValueConfirm = passCodeValue;

                SetPINText = passCodeDisplay;

                if (isChangePin)
                {
                    AppStartOrWakeup = true;
                    passCodeValueInitial = passCodeValue;
                    isConfirmPinMode = false;
                }



                //replace text by *

                //SetPIN.Text = Regex.Replace(_enteredPasscode, @".", _passwordChar);
                //take cursor to end of string
                SetPINSelectionStart = SetPINText.Length;

                if (passCodeValue.Length == DigitCount)
                {
                    bool needToChangePage = true;
                    //string pageToLoad = "/Workspace;component/PanoramaPage.xaml";
                    string pageToLoad = "PanoramaPage";

                    if (!AppStartOrWakeup)
                    {
                        // Setting up PIN and confirm PIN
                        if (!isConfirmPinMode)
                        {
                            FirstText = "Confirm Pin";
                            DoConfirmPin();
                            needToChangePage = false;
                        }
                        else
                        {
                            if (passCodeValueInitial.Equals(passCodeValueConfirm))
                            {
                                isConfirmPinMode = true;
                                // Once the PIN set and Confirm PIN set are complete, set the state to app initialized.
                                App.mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_APP_INITIALIZED);

                                if (App.mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE)
                                    & App.mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP))
                                {
                                    pageToLoad = "PanoramaPage";

                                }
                                else
                                {
                                    App.mAppState.CreateMasterKey(passCodeValueConfirm);
                                    mDecryptedEncryptionKey = App.mAppState.ValidatePin(passCodeValueConfirm);
                                    await SetDBEncryptionKeyAndBGTaskAgentFile();
                                    App.mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE);

                                    // Store user settings in CCM
                                    App.mManager.storeUserSettings();

                                    if (null != EnterprisePassword && EnterprisePassword.Length > 0)
                                    {
                                        // They are using SSO (AD Credentials) so, bypass password and set up account
                                        Account acct = new Account();
                                        if (App.mManager.settingsUserOptions != null)
                                        {
                                            acct.domainName = App.mManager.settingsUserOptions.EasDomain;
                                            acct.authUser = App.mManager.settingsUserOptions.EasUserName;
                                            acct.emailAddress = App.mManager.settingsUserOptions.EasEmailAddress;
                                            acct.serverUrl = App.mManager.settingsUserOptions.EasHost;
                                            acct.authPass = EnterprisePassword;
                                        }
                                        else
                                        {
                                            // What do we doo if this is null?
                                        }

                                        EASAccountSetupHelper helper = new EASAccountSetupHelper(acct, null);
                                        int result = await helper.authenticateUserSSO();
                                        if (result == 0)
                                        {
                                            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                                            {
                                                Common.Navigation.NavigationService.NavigateToPage(typeof(UserWelcomeScreen));
                                            });
                                            //pageToLoad = "/Workspace;component/UserWelcomeScreen.xaml";
                                        }
                                        else
                                        {
                                            needToChangePage = false;
                                            string EASMessageString = "";
                                            if (result == 177)
                                            {
                                                EASMessageString = "Too many devices provisioned.  Please delete one or more devices and try again.";
                                            }
                                            else
                                            {
                                                EASMessageString = string.Format("Status returned from EAS Authenticate (Autodiscover and Provision) is {0}", result);
                                            }

                                            //CustomMessageBox messageBox = new CustomMessageBox()
                                            //{
                                            //    Caption = "Exchange Autodiscover/Provision Error",
                                            //    Message = EASMessageString,
                                            //    LeftButtonContent = "OK",

                                            //};
                                            //messageBox.Dismissed += (s1, e1) =>
                                            //{
                                            //    switch (e1.Result)
                                            //    {
                                            //        case CustomMessageBoxResult.LeftButton:
                                            //            pageToLoad = "/Workspace;component/Login/View/ManualSignInView.xaml";
                                            //            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(pageToLoad, UriKind.Relative));
                                            //            break;
                                            //        case CustomMessageBoxResult.RightButton:
                                            //            break;
                                            //        case CustomMessageBoxResult.None:
                                            //            break;
                                            //        default:
                                            //            break;
                                            //    }
                                            //};
                                            //messageBox.Show();
                                        }
                                    }
                                    else
                                    {
                                        if (!App.mManager.settingsUserOptions.DynamicUserInfo && App.mManager.settingsUserOptions.EasPassword.Length > 0)
                                        {
                                            Account acct = new Account();
                                            if (App.mManager.settingsUserOptions != null)
                                            {
                                                acct.domainName = App.mManager.settingsUserOptions.EasDomain;
                                                acct.authUser = App.mManager.settingsUserOptions.EasUserName;
                                                acct.emailAddress = App.mManager.settingsUserOptions.EasEmailAddress;
                                                acct.serverUrl = App.mManager.settingsUserOptions.EasHost;
                                                acct.authPass = App.mManager.settingsUserOptions.EasPassword;
                                            }
                                            else
                                            {
                                                // What do we doo if this is null?
                                            }

                                            EASAccountSetupHelper helper = new EASAccountSetupHelper(acct, null);
                                            int result = await helper.authenticateUserSSO();
                                            if (result == 0)
                                            {
                                                pageToLoad = "UserWelcomeScreen";
                                            }
                                            else
                                            {
                                                needToChangePage = false;
                                                string EASMessageString = "";
                                                if (result == 177)
                                                {
                                                    EASMessageString = "Too many devices provisioned.  Please delete one or more devices and try again.";
                                                }
                                                else
                                                {
                                                    EASMessageString = string.Format("Status returned from EAS Authenticate (Autodiscover and Provision) is {0}", result);
                                                }

                                                //CustomMessageBox messageBox = new CustomMessageBox()
                                                //{
                                                //    Caption = "Exchange Autodiscover/Provision Error",
                                                //    Message = EASMessageString,
                                                //    LeftButtonContent = "OK",

                                                //};
                                                //messageBox.Dismissed += (s1, e1) =>
                                                //{
                                                //    switch (e1.Result)
                                                //    {
                                                //        case CustomMessageBoxResult.LeftButton:
                                                //            pageToLoad = "/Workspace;component/Login/View/ManualSignInView.xaml";
                                                //            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(pageToLoad, UriKind.Relative));
                                                //            break;
                                                //        case CustomMessageBoxResult.RightButton:
                                                //            break;
                                                //        case CustomMessageBoxResult.None:
                                                //            break;
                                                //        default:
                                                //            break;
                                                //    }
                                                //};
                                                //messageBox.Show();
                                            }
                                        }
                                        else
                                        {
                                            pageToLoad = "EmailLogin";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                DoConfirmPin(true);
                                needToChangePage = false;
                            }
                        }
                    }

                    else if (newPin)
                    {
                        if (!isConfirmPinMode)
                        {
                            FirstText = "Confirm Pin";
                            DoConfirmPin();
                            needToChangePage = false;
                        }
                        else
                        {
                            if (passCodeValueInitial.Equals(passCodeValueConfirm))
                            {
                                isConfirmPinMode = true;
                                // if both th pin are equal, change pin 
                                mSuccessfullyChangedPIN = pinChange.UserPinChange(oldPinToBeChanged, passCodeValueConfirm).Result;
                                // App.mAppState.CreateMasterKey(passCodeValueConfirm);

                                EASAccount.GetAccountInfo += new EASAccount.GetAccInfoHandler(GetAccountInfo);
                                App.mActiveSyncManager = ActiveSyncManager.sharedManagerForEmailAddress(App.mManager.settingsUserOptions.EasEmailAddress);

                                if (mSuccessfullyChangedPIN)
                                {
                                    //   If the PIN length has changed, then we must store the new PIN length to our settings
                                    App.mManager.settingsUserOptions.Settings[HaloPolicySettingsAbstract.KEY_PIN_LENGTH] = DigitCount.ToString();
                                    App.mManager.storeUserSettings();

                                    if (mMandatoryPINChange)
                                    {
                                        mMandatoryPINChange = false;
                                        App.mAppState.resetAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE);
                                        App.mAppState.resetAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_PIN_NEEDS_CHANGE);
                                        App.mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP);
                                        App.mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE);
                                        //_navigationService.NavigateTo(new Uri("/Workspace;component/PanoramaPage.xaml?PINCHANGED=1", UriKind.Relative));

                                    }
                                    else
                                        if ((Windows.UI.Xaml.Window.Current.Content as Frame).CanGoBack)
                                            (Windows.UI.Xaml.Window.Current.Content as Frame).GoBack();

                                }
                            }
                            else
                                DoConfirmPin(true);
                            needToChangePage = false;
                        }

                    }

                    else
                    {

                        // PIN is already set up, master key set, now user is typing in his PIN to get access to Application
                        if (App.mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE))
                        {
                            mDecryptedEncryptionKey = App.mAppState.ValidatePin(passCodeValueInitial);

                            if (null != mDecryptedEncryptionKey)
                            {
                                // Set the Active Sync Manager based on the email address of CCM.
                                await SetDBEncryptionKeyAndBGTaskAgentFile();

                                // Load CCM storage settings
                                bool loadedUserSettings = await App.mManager.loadUserSettings();
                                if (loadedUserSettings)
                                {
                                    if (isChangePin) // If user wants to change existing pin
                                    {
                                        pageToLoad = string.Format("/Workspace;component/NewPINScreen.xaml?pin={0}", passCodeValue);
                                        oldPinToBeChanged = passCodeValue;
                                        NewPINLength = App.mManager.settingsUserOptions.PinLength;
                                        if (NewPINLength > 0)
                                        {
                                            pageToLoad += "&PINLength=" + NewPINLength;
                                            // clear the app states
                                            App.mAppState.resetAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE);
                                            App.mAppState.setAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_MANDATORY_PIN_CHANGE);

                                        }
                                        // _navigationService.NavigateTo(new Uri(uri, UriKind.Relative));
                                    }

                                    // Has the user gone through the Welcome -> Get Started yet?
                                    else if (App.mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP))
                                    {
                                        //PJ. Why is this registered here and not on the Panorama screen?
                                        EASAccount.GetAccountInfo += new EASAccount.GetAccInfoHandler(GetAccountInfo);
                                        App.mActiveSyncManager = ActiveSyncManager.sharedManagerForEmailAddress(App.mManager.settingsUserOptions.EasEmailAddress);
                                        pageToLoad = "PanoramaPage";
                                    }
                                    else
                                    {
                                        if (null != EnterprisePassword && EnterprisePassword.Length > 0)
                                        {
                                            // They are using SSO (AD Credentials) so, bypass password and set up account
                                            Account acct = new Account();
                                            if (App.mManager.settingsUserOptions != null)
                                            {
                                                acct.domainName = App.mManager.settingsUserOptions.EasDomain;
                                                acct.authUser = App.mManager.settingsUserOptions.EasUserName;
                                                acct.emailAddress = App.mManager.settingsUserOptions.EasEmailAddress;
                                                acct.serverUrl = App.mManager.settingsUserOptions.EasHost;
                                            }
                                            else
                                            {
                                                // What do we doo if this is null?
                                            }

                                            EASAccountSetupHelper helper = new EASAccountSetupHelper(acct, null);
                                            int result = await helper.authenticateUserSSO();
                                            if (result == 0)
                                            {
                                                pageToLoad = "UserWelcomeScreen";
                                            }
                                            else
                                            {
                                                needToChangePage = false;
                                                string EASMessageString = "";
                                                if (result == 177)
                                                {
                                                    EASMessageString = "Too many devices provisioned.  Please delete one or more devices and try again.";
                                                }
                                                else
                                                {
                                                    EASMessageString = string.Format("Status returned from EAS Authenticate (Autodiscover and Provision) is {0}", result);
                                                }

                                                //CustomMessageBox messageBox = new CustomMessageBox()
                                                //{
                                                //    Caption = "Exchange Autodiscover/Provision Error",
                                                //    Message = EASMessageString,
                                                //    LeftButtonContent = "OK",

                                                //};
                                                //messageBox.Dismissed += (s1, e1) =>
                                                //{
                                                //    switch (e1.Result)
                                                //    {
                                                //        case CustomMessageBoxResult.LeftButton:
                                                //            pageToLoad = "/Workspace;component/Login/View/ManualSignInView.xaml";
                                                //            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(pageToLoad, UriKind.Relative));
                                                //            break;
                                                //        case CustomMessageBoxResult.RightButton:
                                                //            break;
                                                //        case CustomMessageBoxResult.None:
                                                //            break;
                                                //        default:
                                                //            break;
                                                //    }
                                                //};
                                                //messageBox.Show();
                                            }
                                        }
                                        else
                                        {
                                            if (!App.mManager.settingsUserOptions.DynamicUserInfo && App.mManager.settingsUserOptions.EasPassword.Length > 0)
                                            {
                                                Account acct = new Account();
                                                if (App.mManager.settingsUserOptions != null)
                                                {
                                                    acct.domainName = App.mManager.settingsUserOptions.EasDomain;
                                                    acct.authUser = App.mManager.settingsUserOptions.EasUserName;
                                                    acct.emailAddress = App.mManager.settingsUserOptions.EasEmailAddress;
                                                    acct.serverUrl = App.mManager.settingsUserOptions.EasHost;
                                                    acct.authPass = App.mManager.settingsUserOptions.EasPassword;
                                                }
                                                else
                                                {
                                                    // What do we doo if this is null?
                                                }

                                                EASAccountSetupHelper helper = new EASAccountSetupHelper(acct, null);
                                                int result = await helper.authenticateUserSSO();
                                                if (result == 0)
                                                {
                                                    pageToLoad = "UserWelcomeScreen";
                                                }
                                                else
                                                {
                                                    needToChangePage = false;
                                                    string EASMessageString = "";
                                                    if (result == 177)
                                                    {
                                                        EASMessageString = "Too many devices provisioned.  Please delete one or more devices and try again.";
                                                    }
                                                    else
                                                    {
                                                        EASMessageString = string.Format("Status returned from EAS Authenticate (Autodiscover and Provision) is {0}", result);
                                                    }

                                                    //CustomMessageBox messageBox = new CustomMessageBox()
                                                    //{
                                                    //    Caption = "Exchange Autodiscover/Provision Error",
                                                    //    Message = EASMessageString,
                                                    //    LeftButtonContent = "OK",

                                                    //};
                                                    //messageBox.Dismissed += (s1, e1) =>
                                                    //{
                                                    //    switch (e1.Result)
                                                    //    {
                                                    //        case CustomMessageBoxResult.LeftButton:
                                                    //            pageToLoad = "/Workspace;component/Login/View/ManualSignInView.xaml";
                                                    //            (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(pageToLoad, UriKind.Relative));
                                                    //            break;
                                                    //        case CustomMessageBoxResult.RightButton:
                                                    //            break;
                                                    //        case CustomMessageBoxResult.None:
                                                    //            break;
                                                    //        default:
                                                    //            break;
                                                    //    }
                                                    //};
                                                    //messageBox.Show();
                                                }
                                            }
                                            else
                                            {
                                                //pageToLoad = "/Workspace;component/EmailLogin/View/EmailLogin.xaml";
                                                pageToLoad = "EmailLogin";
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    // Something catastrophic
                                    // Need to wipe container and start from scratch
                                    //CustomMessageBox messageBox = new CustomMessageBox()
                                    //{
                                    //    Caption = "Catastrophic failure!",
                                    //    Message = "The CCM settings file is corrupt. Tap OK to wipe container and restart",
                                    //    LeftButtonContent = "OK",

                                    //};
                                    //messageBox.Dismissed += (s1, e1) =>
                                    //{
                                    //    switch (e1.Result)
                                    //    {
                                    //        case CustomMessageBoxResult.LeftButton:
                                    //            // TODO We must wipe container and bring user back to CCM login screen to start again.
                                    //            break;
                                    //        case CustomMessageBoxResult.RightButton:
                                    //            break;
                                    //        case CustomMessageBoxResult.None:
                                    //            break;
                                    //        default:
                                    //            break;
                                    //    }
                                    //};
                                    //messageBox.Show();
                                }
                            }
                            else
                            {
                                //wrong Pin
                                //Decrement the number of failed attempts
                                FirstText = "Workspace Timed Out";
                                FailedAttempts -= 1;
                                //If equal to 0
                                if (FailedAttempts == 0)
                                {
                                    App.mAppState.setAppStatus(Common.DKAppState.DKAppStatus.DK_APP_STATE_USER_LOCKED_PAUSE);
                                    MaxAttemptsVisibility = Visibility.Visible;
                                    LoginAttemptFailure(TimeLeft);
                                    SetPINText = string.Empty;
                                    SetPinVisibility = Visibility.Collapsed;
                                    //  InstructionText.Margin = new Thickness(46, 108, 20, 120);
                                    DigitCountText = "Enter Your Pin Number ";
                                    InstructionTextVisibility = Visibility.Collapsed;
                                    AttemptCountVisibility = Visibility.Collapsed;
                                    //FailedAttempts = App.mManager.settingsUserOptions.LoginAttemptsAllowed;
                                    int failAttempts = 0;
                                    if (App.mManager.loadLoginAttemptsAllowedSettings(ref failAttempts))
                                    {
                                        FailedAttempts = failAttempts;
                                        FailedAttemptsBase = failAttempts;
                                    }
                                    else
                                    {
                                        // Load value from FailedAttemptsBase property
                                        FailedAttempts = FailedAttemptsBase;
                                    }
                                    needToChangePage = false;
                                    passCodeDisplay = "";
                                    passCodeValue = "";


                                }
                                else
                                {

                                    getFailedLoginCount(FailedAttempts);
                                    AttemptCountVisibility = Visibility.Visible;
                                    InstructionTextVisibility = Visibility.Collapsed;
                                    SetPINText = string.Empty;
                                    needToChangePage = false;
                                    passCodeDisplay = "";
                                    passCodeValue = "";
                                }

                            }

                        }
                    }
                    if (needToChangePage)
                    {
                        //_navigationService.NavigateTo(new Uri(pageToLoad, UriKind.Relative));
                        Common.Navigation.NavigationService.NavigateToPage(pageToLoad);
                        //if ((Application.Current.RootVisual as PhoneApplicationFrame).CanGoBack && (Application.Current.RootVisual as PhoneApplicationFrame).BackStack.Count() != 0 && (!isChangePin))//true if its locked due to inactive timeout. 
                        //    (Application.Current.RootVisual as PhoneApplicationFrame).GoBack();
                        //else
                        //{
                        //    (Application.Current.RootVisual as PhoneApplicationFrame).Navigate(new Uri(pageToLoad, UriKind.Relative));
                        //}

                    }
                }
            }
        }
    }
}