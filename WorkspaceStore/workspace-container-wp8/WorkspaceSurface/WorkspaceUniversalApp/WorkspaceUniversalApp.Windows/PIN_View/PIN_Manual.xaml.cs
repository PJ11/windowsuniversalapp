﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HockeyApp;

namespace WorkspaceWP81.PIN_View
{
    public partial class PinView : PhoneApplicationPage
    {
        public PinView()
        {
            InitializeComponent();
            HockeyApp.CrashHandler.Instance.HandleCrashes();
            string appid = App.mAppIdQA;
#if DEBUG
            appid = App.mAppIdDevelopment;
#endif
            UpdateManager.RunUpdateCheck(appid);
        }
    }
}