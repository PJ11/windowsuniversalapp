﻿#define USE_ASYNC
using System;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using workspace.Helpers;
using WorkspaceUniversalApp.Helpers;

namespace WorkspaceUniversalApp.PIN_View
{
    public partial class PINScreen : Page
    {
       
      
        static bool isConfirmPinMode = false;
        bool firstLogin = true;
        byte[] mDecryptedEncryptionKey = null;
       
        #region Properties
        public bool AppStartOrWakeup { get; protected set; }
        public string Password { get; protected set; }
        public int DigitCount { get; protected set; }
        public int FailedAttempts { get; protected set; }
        public int TimeLeft { get; protected set; }
        public string EnterprisePassword { get; set; }
        public bool ForcedPINChange { get; set; }
        #endregion

        public PINScreen()
        {
            isConfirmPinMode = false;
            DigitCount = -1;
            FailedAttempts = -1;
            TimeLeft = 0;
           
            InitializeComponent();
            AppStartOrWakeup = false;           

        }
    



        private void PasswordTextBox_KeyUp(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            var viewModelObject = (PinViewModel)this.DataContext;
            viewModelObject.PasswordTextBox_KeyUp(sender, e); 
        }



        private void PasswordTextBox_KeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            var vm = (PinViewModel)this.DataContext;
            vm.PasswordTextBox_KeyDown(sender, e);
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            var entry = this.Frame.BackStack.FirstOrDefault();
           
            //i dont think we are using it
            //if (entry != null && entry.SourcePageType == typeof(LaunchPage))
            //{
            //    this.Frame.BackStack.RemoveAt(this.Frame.BackStack.Count-1);
            //}
            base.OnNavigatedTo(e);
        }
        //protected async override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        //{
        //    //PJ: we dont want to stop user from navigating in any case. Will conform from user before exiting.
        //    //when pin not set. User is not navigated out of the application. 
        //    //if (!App.mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE))
        //    //{
        //    //    e.Cancel = true;
        //    //    //DO nothing and stay on the same pin set or confirm page.
        //    //} ContentDialog noWifiDialog = new ContentDialog()

        //    CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
        //    await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
        //        {
        //            var dialog = new Windows.UI.Popups.MessageDialog("Are you sure you want to exit the application?",
        //       "Exit Workspace");
        //            dialog.Commands.Add(new Windows.UI.Popups.UICommand("Ok") { Id = 0 });
        //            dialog.Commands.Add(new Windows.UI.Popups.UICommand("Cancel") { Id = 1 });
        //            var op =  dialog.ShowAsync();
        //            if ((int)op.Id == 0)
        //            {
        //                Application.Current.Exit();
        //            }
                  
        //        });
        //    e.Cancel = true;
        //}

        //void messageBox_Dismissed(object sender, DismissedEventArgs e)
        //{
        //    switch (e.Result)
        //                {
        //                    case CustomMessageBoxResult.LeftButton:
        //                        App.Current.Terminate();
        //                        break;
        //                    case CustomMessageBoxResult.RightButton:
        //                        break;
        //                    default:
        //                        break;
        //                }
        //}

        private async void CheckAdminLock()
        {
            //added a check for value equals 1 as the state might exist with value 0 in some cases. Dont want to get locked out.
            int settingsVal= -1;
           // settingsVal = (int)DKSharedPreferences.mySettings.Values[Common.DKAppState.DKAppStatus.DK_APP_STATE_ADMIN_LOCKED.ToString()];

            if ( settingsVal == 1) 
            {
                var dialog = new Windows.UI.Popups.MessageDialog("Please contact the administrator",
               "Workspace Locked");
                dialog.Commands.Add(new Windows.UI.Popups.UICommand("Ok") { Id = 0 });
                var op = await dialog.ShowAsync();

                if((int)op.Id==0)
                {
                    Application.Current.Exit();
                }
                //messageBox.Dismissing += (s1, e1) =>
                //{
                //    switch (e1.Result)
                //    {
                //        case CustomMessageBoxResult.LeftButton:// exit app
                //            Application.Current.Terminate();
                //            break;

                //        case CustomMessageBoxResult.None:
                //            break;
                //        default:
                //            break;
                //    }
                //};
                //messageBox.Show();
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            this.Focus(Windows.UI.Xaml.FocusState.Programmatic);
            var vm = (PinViewModel)this.DataContext;
            vm.passCodeDisplay = "";
            vm.passCodeValue = "";
            vm.passCodeValueConfirm = "";
            vm.passCodeValueInitial = "";
            vm.MaxAttemptsVisibility = Windows.UI.Xaml.Visibility.Collapsed;
            vm.AttemptCountVisibility = Windows.UI.Xaml.Visibility.Collapsed;
            base.OnNavigatingFrom(e);
        }

        private async void Page_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            CheckAdminLock();

            var viewModelObj = (PinViewModel)this.DataContext;
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            //isConfirmPinMode = false;
            //var MyPinScreenViewModel = (PinViewModel)DataContext;
            //MyPinScreenViewModel.Feedback += new EventHandler<Workspace.PIN_View.PinViewModel.TextArgs>(viewModelObj.Feedback_Received);
            //EnterprisePassword = null;
            if (SuspensionManager.SessionState.ContainsKey("AppStartOrWakeup"))
            {
                viewModelObj.AppStartOrWakeup = System.Convert.ToBoolean(SuspensionManager.SessionState["AppStartOrWakeup"]);
                SuspensionManager.SessionState.Remove("AppStartOrWakeup");
            }
            if (localSettings.Values.ContainsKey("EnterprisePassword"))
            {
                viewModelObj.EnterprisePassword = (string)localSettings.Values["EnterprisePassword"];
            }
            if (localSettings.Values.ContainsKey("ForcedPINChange"))
            {
                viewModelObj.ForcedPINChange = System.Convert.ToBoolean(localSettings.Values["ForcedPINChange"]);
            }

            if (localSettings.Values.ContainsKey("Timeout"))
            {
                var mtimeout = System.Convert.ToBoolean(localSettings.Values["Timeout"]);
                FirstInstruction.Text = "Workspace Timed Out";
                InstructionsText.Text = "Enter PIN to Continue";
            }

            viewModelObj.PageLoad();
            await Task.Delay(TimeSpan.FromMilliseconds(2000));
            SetPIN.Focus(Windows.UI.Xaml.FocusState.Programmatic);
        }
        private async Task PinChanged(int number)
        {
            //call method to show wait progress baar
            WorkspaceProgressRing.StartProgressRing(AppConstString.PROGRESS_RING_LOADING_TEXT, pinscreen, gridForProgressRing);
            var vm = (PinViewModel)this.DataContext;
            await vm.PinScreenChanged(number);
            //call method to show wait progress baar
            WorkspaceProgressRing.StopProgressRing(pinscreen, gridForProgressRing);

        }
        private async void BtnNumericOne_Clicked(object sender, RoutedEventArgs e)
        {
            //call method to check pin
            await PinChanged(1);  
        }
      
        private async void BtnNumericTwo_OnClick(object sender, RoutedEventArgs e)
        {
            //call method to check pin
           await PinChanged(2);
        }

        private async void BtnNumericThree_OnClick(object sender, RoutedEventArgs e)
        {
            //call method to check pin
            await PinChanged(3);  
        }

        private async void BtnNumericFour_OnClick(object sender, RoutedEventArgs e)
        {
            //call method to check pin
            await PinChanged(4); 
        }

        private async void BtnNumericFive_OnClick(object sender, RoutedEventArgs e)
        {
            //call method to check pin
            await PinChanged(5);
        }

        private async void BtnNumericSix_OnClick(object sender, RoutedEventArgs e)
        {
            //call method to check pin
            await PinChanged(6);
        }

        private async void BtnNumericSeven_OnClick(object sender, RoutedEventArgs e)
        {
            //call method to check pin
            await PinChanged(7);
        }

        private async void BtnNumericEight_OnClick(object sender, RoutedEventArgs e)
        {
            //call method to check pin
            await PinChanged(8);
        }

        private async void BtnNumericNine_OnClick(object sender, RoutedEventArgs e)
        {
            //call method to check pin
            await PinChanged(9);
        }

        private async void BtnNumericZero_OnClick(object sender, RoutedEventArgs e)
        {
            //call method to check pin
            await PinChanged(0);
        }
    }
}