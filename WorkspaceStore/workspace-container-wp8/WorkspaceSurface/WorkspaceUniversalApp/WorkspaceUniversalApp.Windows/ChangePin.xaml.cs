﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using WorkspaceWP81;
using Common;
using WorkspaceWP81.PIN_View;

namespace workspace
{
    public partial class ChangePin : Page
    {     
       
        #region Properties
        public string NewPINLength { get; protected set; }
        #endregion

        public ChangePin()
        {         
            InitializeComponent();
            this.Loaded += ChangePin_Loaded;           
        }

        void ChangePin_Loaded(object sender, RoutedEventArgs e)
        {
            SetPIN.Focus();
            var viewModelObject = (PinViewModel)this.DataContext;
            //NewPINLength = "";
            //if (this.NavigationContext.QueryString.ContainsKey("PINLength") && this.NavigationContext.QueryString["PINLength"] !="-1")
            //{
            //       viewModelObject.NewPINLength = this.NavigationContext.QueryString["PINLength"];
            //}
            viewModelObject.isChangePin = true;
            //viewModelObject.FirstText = "Change PIN";
            //viewModelObject.DigitCountText = "Enter your Current PIN";

            FirstInstruction.Text = "Change PIN";
            InstructionsText.Text = "Enter your Current PIN";
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            var vm = (PinViewModel)this.DataContext;
            // If the change PIN is required by a # change from CCM, then they can't go back
            //if (vm.NewPINLength!=0 && vm.NewPINLength > 0)
            //{
            //    e.Cancel = true;
            //}
            //else
                base.OnBackKeyPress(e);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var vm = (PinViewModel)this.DataContext;
            vm.SetPINText = "";
            vm.DigitCountText = "";
            vm.FirstText = "";

            Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            base.OnNavigatedTo(e);
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            var entry = NavigationService.BackStack.FirstOrDefault();
            if (entry != null && entry.Source.OriginalString.Contains("ChangePin.xaml"))
            {
                NavigationService.RemoveBackEntry();
            }
            var vm = (PinViewModel)this.DataContext;
            vm.SetPINText = "";
            vm.passCodeDisplay = "";
            vm.passCodeValue = "";
            vm.passCodeValueConfirm = "";
            vm.passCodeValueInitial = "";
            vm.DigitCountText = "";
            vm.FirstText = "";
            vm.MaxAttemptsVisibility = Visibility.Collapsed;
            vm.AttemptCountVisibility = Visibility.Collapsed;
            vm.isChangePin = false;

            Windows.Phone.UI.Input.HardwareButtons.BackPressed -= HardwareButtons_BackPressed;

            base.OnNavigatedFrom(e);
        }

        void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
            e.Handled = true;
        }

        private void PasswordTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var viewModelObject = (PinViewModel)this.DataContext;
            viewModelObject.PasswordTextBox_KeyUp(sender, e);           
        }

        private void PasswordTextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var vm = (PinViewModel)this.DataContext;
            vm.PasswordTextBox_KeyDown(sender, e);
        }      

    }
}