﻿// <copyright file="LongListUserControl.xaml.cs" company="Dell Inc">
// Copyright (c) 2014 All Right Reserved
// </copyright>
// <author>Sajini PM</author>
// <email>Sajini_PM@Dell.com</email>
// <date></date>
// <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
// <summary>Sajini                           1.0                 First Version </summary>

using Windows.UI.Xaml.Controls;

namespace WorkspaceUniversalApp.CCMLoginScreen
{
    #region NAMESPACE

    

    #endregion
    /// <summary>
    /// User Control for displaying CCM servers.
    /// </summary>
    public partial class LongListUserControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LongListUserControl"/> class.
        /// </summary>
        public LongListUserControl()
        {
            this.InitializeComponent();
        }
    }
}
