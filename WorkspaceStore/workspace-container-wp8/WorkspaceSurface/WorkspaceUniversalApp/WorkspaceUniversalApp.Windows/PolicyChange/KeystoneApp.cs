﻿using Common;
using CommonPCL;
using FileHandlingPCL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkspaceWP81.PolicyChange
{
    public class KeystoneApp
    {


        private static KeystoneApp mInstance = null;
        private static DKAppState mAppState = null;


        private static HaloService mHaloService = null;



        protected KeystoneApp()
        {
            mAppState = DKAppState.getInstance();
            mHaloService = App.mHaloService;
            mHaloService.ccmWipeContainerHandler += mHaloService_ccmWipeContainerHandler;
            // mAppState.registerAppStateChangeListener(this);
        }

        void mHaloService_ccmWipeContainerHandler(object sender, CCMWipeContainerEventArgs e)
        {
            onAppEvent(e.Event, e.MSG);
        }


        public static KeystoneApp getInstance()
        {
            if (mInstance == null)
            {
                mInstance = new KeystoneApp();
            }
            return mInstance;
        }

        public void onUserlocked(Message msg)
        {

            //LocktheApp();
        }

        //    @Override
        public void onUserUnlocked(Message msg)
        {
            //if (checkAppVisibility())
            //    restartSplash(); // Will do the same thing, launch Splash
        }

        //    @Override
        public void onAdminlocked(Message msg)
        {

            //LocktheApp();
        }

        //    @Override
        public void onAdminUnlocked(Message msg)
        {
            //if (checkAppVisibility())
            //    restartSplash(); // Will do the same thing, launch Splash
        }

        //    @Override
        public void onStatusChanged(DKAppState.DKAppStatus status, Message msg)
        {

        }

        //    @Override
        public void onMemoryLow(Message msg)
        {

        }

        //    @Override
        public void onAppEvent(DKAppState.DKAppEvent e, Message msg)
        {
            int newInterval = DKConstants.DEFAULT_INACTIVITY_TIMEOUT;
#if OtherEvents
                //if (e == DKAppState.DKAppEvent.DK_APP_EVENT_KEYS_AVAILABLE && mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_ACCOUNTS_SETUP)) {
                //    if (mSecurityMgr.getMasterKeyString() != null)
                //        Log.i(TAG, "Keys are valid.");
                //    startApplicationServices();
                //    if (mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS)){
                //        newInterval = DKUserSettings.getInstance().getUserInactivityTimeout();
                //        CheckInService.scheduleCheckInAlarm(mContext);
                //    }
                //    if (newInterval > 0) {
                //        PASSCODE_TIMEOUT_INTERVAL = newInterval*MilliSecInAMin;
                //    }
                //    // Start the timer to check for passcode popped up.
                //    startPasscodeTimer();
                //    // fake the call to make sure DKSecurityMgrInternal is not Gc'ed
                //    if (mSecurityMgr.getMasterKeyString() != null)
                //        Log.i(TAG, "Keys are valid.");
                //    else
                //        Log.i(TAG, "Keys are null");
                //    if (mSettingsMgr.getCopyPasteEnabled()) {
                //        clearClipBoard = false;
                //    } else {
                //        clearClipBoard = true;
                //    }
                //}
                //if (event == DKAppState.DKAppEvent.DK_APP_EVENT_ACCOUNTS_SETUP && mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_KEYS_AVAILABLE)) {
                //    if (mSecurityMgr.getMasterKeyString() != null)
                //        Log.i(TAG, "Keys are valid.");
                //    startApplicationServices();
                //    if (mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS))
                //        newInterval = DKUserSettings.getInstance().getUserInactivityTimeout();
                //    if (newInterval > 0)
                //        PASSCODE_TIMEOUT_INTERVAL = newInterval*MilliSecInAMin;
                //    // Start the timer to check for passcode popped up.
                //    startPasscodeTimer();

                //    // fake the call to make sure DKSecurityMgrInternal is not Gc'ed
                //    if (mSecurityMgr.getMasterKeyString() != null)
                //        Log.i(TAG, "Keys are valid.");
                //    else
                //        Log.i(TAG, "Keys are null");
                //    if (mSettingsMgr.getCopyPasteEnabled()) {
                //        clearClipBoard = false;
                //    } else {
                //        clearClipBoard = true;
                //    }
                //}
                if (e == DKAppState.DKAppEvent.DK_APP_EVENT_POLICY_CHANGE) 
                {
                    //if (mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS)){
                    //    newInterval = DKUserSettings.getInstance().getUserInactivityTimeout();
                    //    CheckInService.scheduleCheckInAlarm(mContext);
                    //}
                    //if (newInterval > 0)
                    //    PASSCODE_TIMEOUT_INTERVAL = newInterval*MilliSecInAMin;
                    //// Setup to clear clipboard if necessary.
                    //if (mSettingsMgr.getCopyPasteEnabled()) {
                    //    clearClipBoard = false;
                    //} else {
                    //    clearClipBoard = true;
                    //}

                }
#endif
            if (e == DKAppState.DKAppEvent.DK_APP_EVENT_WIPE_CONTAINER)
            {
                keystoneWipeContainer();

            }
            //if (event == DKAppState.DKAppEvent.DK_APP_EVENT_USER_INACTIVITY_TIMEOUT_CHANGE) {
            //    newInterval = DKUserSettings.getInstance().getUserInactivityTimeout();
            //    if (newInterval > 0){
            //        PASSCODE_TIMEOUT_INTERVAL = newInterval*MilliSecInAMin;
            //        startPasscodeTimer();
            //    }

            //}

            //if(event == DKAppState.DKAppEvent.DK_APP_EVENT_DEVICE_UNLOCK && mAppState.checkAppStatus(DKAppState.DKAppStatus.DK_APP_STATE_CCM_HAS_SETTINGS_PERSISTED))
            //{
            //    DKAppState.getInstance().detectJailbreak();
            //}
        }
#if Android
        //    /*
        //     * Keystone OnPolicyChanged Listener Callbacks
        //     */

        //    @Override
        //    public void onKeystoneSettingsChange() {

        //    }

        //    @Override
        //    public void onEmailSettingsChange() {

        //    }

        //    @Override
        //    public void onActiveAppListChange() {

        //    }

        //    @Override
        //    public void onSettingsUpdated() {

        //    }

        //    @Override
        //    public void onWebBrowserSettingsChange() {

        //    }

        //    @Override
        //    public void onEasSettingsChange() {

        //    }

        //    public void startHaloService() {
        //        startService(new Intent(this, HaloService.class));
        //     }

        //    private void restartSplash  () {
        //        String intentName = getPackageName() + ".action.APP_LOCK";
        //        Intent intent = new Intent(intentName);
        //        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
        //        startActivity(intent);

        //    }

        //  /*  private static Timer delayTimer = null;
        //    public static void startPasscodeTimer() {
        //       // if (DKConstants.DKDebug) {
        //       //     if (android.os.Debug.isDebuggerConnected())
        //       //         return;
        //       // }
        //        if (delayTimer != null)
        //            delayTimer.cancel();

        //        delayTimer = new Timer();
        //        long currentTime = System.currentTimeMillis();
        //        userIdleTime = currentTime - timeOfLastUserInteraction;
        //        long scheduleAmount = PASSCODE_TIMEOUT_INTERVAL-userIdleTime+10; // The 10 is it make sure we
        //        if (scheduleAmount <= 0) {
        //            scheduleAmount = PASSCODE_TIMEOUT_INTERVAL;
        //            mAppState.sendAppEvent(DKAppState.DKAppEvent.DK_APP_EVENT_DEVICE_LOCK);
        //            Log.d(TAG, String.format("Device Lock Sent"));
        //        }
        //        Log.d(TAG, String.format("Starting timer delay = %d", scheduleAmount));
        //        delayTimer.schedule(new TimerTask() {
        //            @Override
        //            public void run() {
        //                long currentTime = System.currentTimeMillis();

        //                userIdleTime = currentTime - timeOfLastUserInteraction;
        //                if (userIdleTime > PASSCODE_TIMEOUT_INTERVAL) {
        //                    mAppState.sendAppEvent(DKAppState.DKAppEvent.DK_APP_EVENT_DEVICE_LOCK);
        //                    Log.d(TAG, String.format("Device Lock Sent"));
        //                } else {
        //                    startPasscodeTimer();
        //                }
        //            }
        //        }, scheduleAmount);

        //    }*/

        //    public static void startPasscodeTimer()
        //    {

        //            // create the object
        //            AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        //            if(pendingIntent != null)
        //            {
        //                alarmManager.cancel(pendingIntent);
        //                pendingIntent = null;
        //            }
        //            long currentTime = System.currentTimeMillis();
        //            long timeOfLastUserInteraction = Long.valueOf(System.getProperty(SYSTEM_TIME_OF_LAST_USER_INTERACTION,Long.toString(currentTime)));
        //            long userIdleTime = currentTime - timeOfLastUserInteraction;
        //            System.setProperty(SYSTEM_USER_IDLE_TIME,Long.toString(userIdleTime));
        //            // always get a latest value from settings so that after upgrade as well this works
        //            int newInterval = DKUserSettings.getInstance().getUserInactivityTimeout();
        //            if (newInterval > 0) {
        //                PASSCODE_TIMEOUT_INTERVAL = newInterval*MilliSecInAMin;
        //            }
        //            long scheduleAmount = PASSCODE_TIMEOUT_INTERVAL-userIdleTime+10; // The 10 is it make sure we
        //            if (scheduleAmount <= 0) {
        //                scheduleAmount = PASSCODE_TIMEOUT_INTERVAL;
        //                mAppState.sendAppEvent(DKAppState.DKAppEvent.DK_APP_EVENT_DEVICE_LOCK);

        //                return; // As device lock event is set, no need to run Alarm again till unlocked.
        //            }



        //            // create an Intent and set the class which will execute when Alarm triggers, here we have
        //            // given AlarmReciever in the Intent, the onRecieve() method of this class will execute when
        //            // alarm triggers and
        //            //we will write the code to send SMS inside onRecieve() method pf Alarmreciever class
        //            Intent intentAlarm = new Intent(mContext, PasscodeTimerAlarmReciever.class);


        //            pendingIntent = PendingIntent.getBroadcast(mContext, 1, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT);
        //            //set the alarm for particular time
        //            alarmManager.set(AlarmManager.RTC_WAKEUP,System.currentTimeMillis() + scheduleAmount, pendingIntent);
        //    }

            private void stopServices()
            {
                //mHaloService.reset();
                //stopApplicationServices();

            }
            private void clearSharedPreferences()
            {
                //File dir = new File(ctx.getFilesDir().getParent() + "/shared_prefs/");
                //String[] children = dir.list();
                //for (int i = 0; i < children.length; i++) {
                //    // clear each of the prefrances
                //    ctx.getSharedPreferences(children[i].replace(".xml", ""), Context.MODE_PRIVATE).edit().clear().commit();
                //}
                //// Make sure it has enough time to save all the commited changes
                //try { Thread.sleep(1000); } catch (InterruptedException e) {}
                //for (int i = 0; i < children.length; i++) {
                //    // delete the files
                //    new File(dir, children[i]).delete();
                //}
            }
            private void startWipeTimer() 
            {
                //Timer wipeTimer = new Timer();

                ///*schedule timer for 5 seconds */
                //long scheduleAmount = 100;

                //wipeTimer.schedule(new TimerTask() {
                //    @Override
                //    public void run() {
                //        WipeContainer();
                //        stopServices();
                //        android.os.Process.killProcess(android.os.Process.myPid());
                //    }
                //}, scheduleAmount);

            }
#endif

        public void keystoneWipeContainer()
        {
            // cancel all the notifications first
            //NotificationManager mNtf = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            //mNtf.cancelAll();
            WipeContainer();
            // kill all the activities

            //startWipeTimer();

        }
        private async Task WipeContainer()
        {
            FileManager fileMan = new FileManager();
            bool bFileDeleted = await fileMan.DeleteFile("x_app_info", "");
            if (await workspace_datastore.SqlcipherManager.DeleteDBTables())
            {
                //stopServices();
                mAppState.resetAppStatus();
                // clearSharedPreferences();
                App.Current.Terminate();
            }
            else
            { //issue deleting app db}
            }
#if Android           
                //killAllActivties();
                //DKSecurityMgrInternal security = DKSecurityMgrInternal.getInstance();
                //security.resetKeys();
                //File dir = getCacheDir();

                //if(dir!= null && dir.isDirectory()){
                //    try{
                //        File[] children = dir.listFiles();
                //        for (File child : children) {
                //            deleteDir(child);
                //        }
                //    }catch(Exception e)
                //    {
                //        Log.e("Cache", "failed cache clean");
                //    }
                //}

                //// delete all storage as well
                //dir = new File(DKFileMgr.getInstance().getRootPath());

                //if(dir!= null && dir.isDirectory()){
                //    try{
                //        File[] children = dir.listFiles();
                //        for (File child : children) {
                //            deleteDir(child);
                //        }
                //    }catch(Exception e)
                //    {
                //        Log.e("Cache", "failed cache clean");
                //    }
                //}
                //DKSettingsMgr.reset();
                //DKSettingsMgrInternal.reset();
                ////SettingsProvider.reset();
                //startWipeTimer();

            }

        //    public  boolean deleteDir(File dir) {
        //        if (dir != null && dir.isDirectory()) {
        //            String[] children = dir.list();
        //            for (int i = 0; i < children.length; i++) {
        //                boolean success = deleteDir(new File(dir, children[i]));
        //                if (!success) {
        //                    return false;
        //                }
        //            }
        //        }
        //        return dir.delete();
        //    }

        //    // Code for Halo Services

        //    public static HaloService getHaloService() {
        //        return mHaloService;
        //    }

        //    ServiceConnection mHaloConnection = new ServiceConnection() {
        //        @Override
        //        public void onServiceConnected(ComponentName name, IBinder service) {
        //            HaloService.HaloBinder binder = (HaloService.HaloBinder) service;
        //            mHaloService = binder.getService();
        //        }

        //        @Override
        //        public void onServiceDisconnected(ComponentName name) {
        //            mHaloService = null;
        //        }
        //    };

        //    @Override
        //    public void onTerminate() {
        //        stopServices();
        //        if (mSecurityMgr != null)
        //            mSecurityMgr.resetKeys();
        //        mSecurityMgr = null;
        //        super.onTerminate();
        //   }
        //}
        //}
#endif


        }
    }
}
