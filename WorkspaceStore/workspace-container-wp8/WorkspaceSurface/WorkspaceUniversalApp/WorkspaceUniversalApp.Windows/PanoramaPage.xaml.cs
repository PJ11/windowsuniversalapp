#define DEBUG_AGENT
#define USE_ASYNC
using AbstractsPCL;
using ActiveSyncPCL;
using workspace;
using workspace.Helpers;
//using workspace.WorkspaceFileBrowserWP8.ViewModel;
//using workspace.WorkspaceEmailWP8.ViewModel;
//using workspace.WorkspaceWebBrowserWP8.Settings;
#if WP_80_SILVERLIGHT
using workspace.WorkspaceCalendarWP8.View;
using workspace.WorkspaceEmailWP8.Helpers;
using workspace.WorkspaceEmailWP8.View;
using workspace.WorkspaceWebBrowserWP8.Settings;
#endif
using Common;
using Common.ToastWithIndeterminateProgressBar;
using FileHandlingPCL;
using GalaSoft.MvvmLight;
//using IpcWrapper;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Foundation.Metadata;
using Windows.UI.Xaml;
using workspace.Helpers;
using workspace_datastore;
using workspace_datastore.Helpers;
using workspace_datastore.Managers;
using workspace_datastore.models;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Core;
using Windows.UI.Popups;

using Windows.Storage;
using System.Net.NetworkInformation;
using Windows.Networking.Connectivity;
using WorkspaceUniversalApp.WorkspaceEmailWP8.View;


//using workspace.WorkspaceEmailWP8.Helpers;
//using workspace.WorkspaceEmailWP8.View;

namespace WorkspaceUniversalApp
{
    class PanoramaPageCallbacks
    {
        public Action Init { get; set; }
        public Action OnPanoramaItemSelected { get; set; }
        public Action<NavigationEventArgs> OnNavigatedToSelected { get; set; }
        public Action<CancelEventArgs> OnBackKeyPress { get; set; }
        public AppBar CurrentApplicationBar { get; set; }
    }

    public sealed partial class PanoramaPage : Page
    {
        
        Dictionary<object, PanoramaPageCallbacks> _callbacks = null;
        ActiveSync mActiveSync = null;
        //string mCurrentVisibleMailFolder = "Inbox";

        workspace_datastore.models.Folder mCurrentVisibleMailFolder = null;
#if WP_80_SILVERLIGHT
        SettingsManager SettMgr = new SettingsManager();
        List<WebSettings> list = new List<WebSettings>();
#else
        TextBlock mMeetingSubject = null;
        TextBlock mTimeStartDuration = null; // timeStartDuration
        TextBlock mTimeStartDuration1 = null; // timeStartDuration1
        TextBlock mTimeEndDuration = null; // timeEndDuration
        TextBlock mTimeEndDuration1 = null;
        TextBlock mMoreMeetingToday = null; // moreMeetingToday
        TextBlock mMoreMeetingToday1 = null; // moreMeetingToday1
        TextBlock mDashTime = null; // dashTime
        TextBlock mDashText = null; // dashText
        TextBlock mDashDuratoin = null; // dashDuration

        StackPanel mTodaysSchedule = null;  //todaySch
        TextBlock mAllDayEvent = null;    // allDayEvent

        TextBlock mFirstAgendaTodayTitle = null; //firstAgendaTodayTitle
        TextBlock mFirstAgendaTodaySubtitle = null; // firstAgendaTodaySubTitle
        TextBlock mNoMeetingsToday = null; // noTodayMeet
        TextBlock mMoreMeetinsToday = null; // moreMeetingToday
        Border mMoreToday = null; // moreToday
        TextBlock mTodayDate = null; //todayDate

#endif
        List<MessageManager> msgManagerListForReadUnread = null;
        List<MessageManager> msgManagerListForFlagging = null;
        Message_UpdateManager msgManagerListWaitingForFlag = null;
        
        bool mPanoramaPageActive = false;
        Dictionary<string, string> mPendingSettings = null;

        //PJ : verified list of variables
        int folderIdInbox;
        FolderManager mgr = new FolderManager();
        MessageManager msgManager = new MessageManager();
        //int inboxMails = -1;

#if WP_80_SILVERLIGHT
        IEnumerable<ScheduledNotification> mReminderNotifications = null;
#endif
        
        
        List<Events> mTodaysEvents = new List<Events>();

        //BackgroundWorker mEmailSyncBackgroundWorker = new BackgroundWorker();

        BackgroundWorkerForSync mSyncBGWorker = null;
        bool mSetupPinger = false;
        System.Diagnostics.Stopwatch sw = null;
        double currentTimeZoneMinuteFromUTC = TimeZoneInfo.Local.GetUtcOffset(DateTime.Now).TotalMinutes;
        //All command bar name
        private CommandBar menuPageBar = null;
        private CommandBar mailPageAppBar = null;
        private CommandBar deletedItemsAppBar = null;
        private CommandBar mailPageAppBarSelect= null;
        private CommandBar mail_no_contentpageAppBar = null;
        private CommandBar calendarPageAppBar = null;
        private CommandBar contactsPageAppBar = null;
        private CommandBar contacts_no_contentpageAppBar = null;
        private CommandBar fileManagerPageAppBar = null;


        
        public PanoramaPage()
        {
           
            InitializeComponent();

            sw = System.Diagnostics.Stopwatch.StartNew();
            this.Loaded += PanoramaPage_Loaded;

            //HardwareButtons.BackPressed += HardwareButton_BackKeyPress;
        }

        private void PanoramaPage_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Loaded--- finish");
            System.Diagnostics.Debug.WriteLine(" timetaken {0}", sw.ElapsedMilliseconds);
        }

        private void OnInializePanorama()
        {
            
            App.NetworkStatusChanged += ChangeDetected;

            if (null == _callbacks)
            {
                _callbacks = new Dictionary<object, PanoramaPageCallbacks>();
                _callbacks[Menu] = new PanoramaPageCallbacks
                {
                    Init = OnInitializeMenu,
                    OnNavigatedToSelected = OnNavigationToMenu,
                    OnPanoramaItemSelected = OnMenuItemSelected,
                    OnBackKeyPress = null,
#if WP_80_SILVERLIGHT

                    CurrentApplicationBar = (ApplicationBar)this.Resources["menuPageBar"]
#endif
                };
                _callbacks[Mail] = new PanoramaPageCallbacks
                {
                    //Init = CreateEmailApplicationBarItems,//nothing is done on this.
                    OnNavigatedToSelected = OnNavigationToEmail,
                    OnPanoramaItemSelected = OnEmailItemSelected,//the selection mode is turned off.
                    OnBackKeyPress = null, //OnEmailBackKeyPressed,//selection mode is on then turned of and menu items are refreshed.
                    #if WP_80_SILVERLIGHT
                    CurrentApplicationBar = (ApplicationBar)this.Resources["mailPageAppBar"]
#endif

                };
                _callbacks[calendar] = new PanoramaPageCallbacks
                {
                    //Init = null,
                    OnNavigatedToSelected = OnNavigationToCalendar,
                    OnPanoramaItemSelected = OnCalendarItemSelected,
                    OnBackKeyPress = null
                };
                _callbacks[contacts] = new PanoramaPageCallbacks
                {
                    //Init = null,
                    OnNavigatedToSelected = null,
                    OnPanoramaItemSelected = OnContactItemSelected,
                    OnBackKeyPress = null
                };
                _callbacks[browser] = new PanoramaPageCallbacks
                {
                    Init = InitialzeBrowserItem,
                    OnNavigatedToSelected = OnNavigateToBrowser,
                    OnPanoramaItemSelected = OnBrowserItemSelected,
                    OnBackKeyPress = null
                };
                _callbacks[files] = new PanoramaPageCallbacks
                {
                    // Init = null,
                    OnNavigatedToSelected = null,
                    OnPanoramaItemSelected = OnFileItemSelected,
                    OnBackKeyPress = null
                };
            }

            //By defaullt we have 
            #if WP_80_SILVERLIGHT
            ApplicationBar = (ApplicationBar)this.Resources["mailPageAppBar"];
#endif
            //foreach (var callbacks in _callbacks.Values)
            //{
            //    if (callbacks.Init != null)
            //    {
            //        callbacks.Init();
            //    }
            //}

            // Register with Halo Service events
           
            App.mHaloService.profileUpdateHandler += halo_profileUpdateHandler;
            if (mActiveSync == null)
            {
                mActiveSync = ActiveSync.GetInstance();
                mActiveSync.EASTriggerSendPingEvent += mActiveSync_EASTriggerSendPingEvent;
                mActiveSync.EASHandlePingResultEvent += mActiveSync_EASHandlePingResultEvent;
                mActiveSync.ConstructOutgoingMessage += mActiveSync_ConstructOutgoingMessage;
                mActiveSync.DeleteOutgoingMessage += mActiveSync_DeleteOutgoingMessage;

                App.NetworkStatusChanged += App_NetworkStatusChanged;
            }

            //PJ. When is this invoked?

            // emailScreen.LayoutUpdated += emailScreen_LayoutUpdated;

            FolderManager folderManager = new FolderManager();
            mCurrentVisibleMailFolder = folderManager.GetFolderInfoAsync("Inbox", 0).Result;

            //OnEmailItemSelected();


        }

        private void InitializeCommandBars()
        {
        

            /////////////////////////////////////////////
            //MenuPageBar
            menuPageBar = new CommandBar();
            menuPageBar.IsOpen = true;
            AppBarButton temp = new AppBarButton() { Label = "Settings" };
            temp.Click += About_Click;
            menuPageBar.SecondaryCommands.Add(temp);
            temp = new AppBarButton() { Label = "Change Pin" };
            temp.Click += ChangePIN_Click;
            menuPageBar.SecondaryCommands.Add(temp);


            /////////////////////////////////////////////
            //MailPageAppBar
            mailPageAppBar = new CommandBar();
            mailPageAppBar.IsOpen = true;
            temp = new AppBarButton() { Name = "CreateNew", Label = "Create New", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_plus.png") } };
            temp.Click += CreateNew_Click;
            mailPageAppBar.PrimaryCommands.Add(temp);

            temp = new AppBarButton() { Name = "Select", Label = "Select", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_list.png") } };
            temp.Click += Select_Click;
            mailPageAppBar.PrimaryCommands.Add(temp);

            temp = new AppBarButton() { Name = "Search", Label = "Search", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_search.png") } };
            temp.Click += Select_Click;
            mailPageAppBar.PrimaryCommands.Add(temp);

            temp = new AppBarButton() { Label = "Folders" };
            temp.Click += Folders_Click;
            mailPageAppBar.SecondaryCommands.Add(temp);

            /////////////////////////////////////////////
            //deletedItemsAppBar

            deletedItemsAppBar = new CommandBar();
            deletedItemsAppBar.IsOpen = true;
            temp = new AppBarButton() { Name = "Delete", Label = "Delete", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_deleted.png") } };
            temp.Click += deletemail_Click;
            deletedItemsAppBar.PrimaryCommands.Add(temp);

            temp = new AppBarButton() { Name = "Folder", Label = "Folder", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_folder.png") } };
            temp.Click += changefolder_Click;
            deletedItemsAppBar.PrimaryCommands.Add(temp);

            temp = new AppBarButton() { Name = "" , Label = "Empty Trash" };
            temp.Click += EmptyTrash_Click;
            deletedItemsAppBar.SecondaryCommands.Add(temp);

            /////////////////////////////////////////////
            //mailPageAppBarSelect

            mailPageAppBarSelect = new CommandBar();
            mailPageAppBarSelect.IsOpen = true;

            temp = new AppBarButton() { Name = "Delete", Label = "Delete", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_deleted.png") } };
            temp.Click += deletemail_Click;
            deletedItemsAppBar.PrimaryCommands.Add(temp);

            temp = new AppBarButton() { Name = "Unread", Label = "Unread", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_mail_read.png") } };
            temp.Click += markunread_Click;
            deletedItemsAppBar.PrimaryCommands.Add(temp);

            temp = new AppBarButton() { Name = "flagmail", Label = "flag", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_flag.png") } };
            temp.Click += flagmail_Click;
            deletedItemsAppBar.PrimaryCommands.Add(temp);

            temp = new AppBarButton() { Name = "changefolder", Label = "folder", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_folder.png") } };
            temp.Click += changefolder_Click;
            deletedItemsAppBar.PrimaryCommands.Add(temp);

            /////////////////////////////////////////////
            //mail_no_contentpageAppBar

            mail_no_contentpageAppBar = new CommandBar();
            mail_no_contentpageAppBar.IsOpen = true;

            temp = new AppBarButton() { Name = "CreateNew", Label = "Create New", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_plus.png") } };
            temp.Click += CreateNew_Click;
            mail_no_contentpageAppBar.PrimaryCommands.Add(temp);

            temp = new AppBarButton() { Name = "Folders", Label = "Folders"};
            temp.Click += Folders_Click;
            mail_no_contentpageAppBar.SecondaryCommands.Add(temp);

            temp = new AppBarButton() { Name = "Settings", Label = "Settings"};
            temp.Click += Settings_Click;
            mail_no_contentpageAppBar.SecondaryCommands.Add(temp);

            temp = new AppBarButton() { Name = "Refresh", Label = "Refresh"};
            temp.Click += Refresh_Click;
            mail_no_contentpageAppBar.SecondaryCommands.Add(temp);


            /////////////////////////////////////////////
            //calendarPageAppBar

            calendarPageAppBar = new CommandBar();
            calendarPageAppBar.IsOpen = true;

            temp = new AppBarButton() {Label = "Create", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_plus.png") } };
            temp.Click += CreateNewEventItem_Click;
            calendarPageAppBar.PrimaryCommands.Add(temp);

            temp = new AppBarButton() {Label = "Search", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_search.png") } };
            temp.Click += SearchIconButton_Click;
            calendarPageAppBar.PrimaryCommands.Add(temp);

            temp = new AppBarButton() {Label = "Settings", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_search.png") } };
            temp.Click += ApplicationBarMenuItem_Click;
            calendarPageAppBar.SecondaryCommands.Add(temp);


            /////////////////////////////////////////////
            //contactsPageAppBar
            contactsPageAppBar = new CommandBar();
            contactsPageAppBar.IsOpen = true;
          

            temp = new AppBarButton() {Label = "Create New", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_plus.png") } };
            temp.Click += ContactsPageMenuItemAdd_Click;
            contactsPageAppBar.PrimaryCommands.Add(temp);
            

            temp = new AppBarButton() {Label = "Search", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_search.png") } };
            temp.Click += ContactsPageMenuItemSearch_Click;
            contactsPageAppBar.PrimaryCommands.Add(temp);

            /////////////////////////////////////////////
            //contacts_no_contentpageAppBar

            contacts_no_contentpageAppBar = new CommandBar();
            contacts_no_contentpageAppBar.IsOpen = true;

            temp = new AppBarButton() {Label = "Create New", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_plus.png") } };
            temp.Click += CreateNewContacts_Click;
            contacts_no_contentpageAppBar.PrimaryCommands.Add(temp);

            /////////////////////////////////////////////
            //fileManagerPageAppBar

            fileManagerPageAppBar = new CommandBar();
            fileManagerPageAppBar.IsOpen = true;

            temp = new AppBarButton() { Label = "Create New", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_plus.png") } };
            temp.Click += FileManagerPageMenuItem_Click;
            fileManagerPageAppBar.PrimaryCommands.Add(temp);

            temp = new AppBarButton() { Label = "Search", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_search.png") } };
            temp.Click += FileManagerPageMenuItem_Click;
            fileManagerPageAppBar.PrimaryCommands.Add(temp);

            temp = new AppBarButton() { Label = "Edit", Icon = new BitmapIcon() { UriSource = new Uri("ms-appx:///Assets/CM_Assets/Phone/ic_nav_edit.png") } };
            temp.Click += FileManagerPageMenuItem_Click;
            fileManagerPageAppBar.PrimaryCommands.Add(temp);

        }

        //private void GetAppRequiredControls()
        //{

        //}

        private DependencyObject FindChildControl<T>(DependencyObject control, string ctrlName)
        {
            int childNumber = VisualTreeHelper.GetChildrenCount(control);
            for (int i = 0; i < childNumber; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(control, i);
                FrameworkElement fe = child as FrameworkElement;
                // Not a framework element or is null
                if (fe == null) return null;

                if (child is T && fe.Name == ctrlName)
                {
                    // Found the control so return
                    return child;
                }
                else
                {
                    // Not found it - search children
                    DependencyObject nextLevel = FindChildControl<T>(child, ctrlName);
                    if (nextLevel != null)
                        return nextLevel;
                }
            }
            return null;
        }
        //private async void DoInitialSync()
        //{   
        //    //We will be getting call back in same function for all, so no need to declare individual callback
        //    //Mail Sync
        //    await mActiveSync.SyncMail();
        //    //calendar Sync
        //    await mActiveSync.SyncCalendar();
        //    //Contact Sync
        //    await mActiveSync.SyncContact();

        //    //call method to create default folders
        //    FileHandlingPCL.FileManager objFileMngr = new FileHandlingPCL.FileManager();
        //    await objFileMngr.CreateRootFolders();
        //}

        private void OnInitializeMenu()
        {
            //PanoramaWorkspace.DefaultItem = Menu;
            PanoramaWorkspace.DefaultSectionIndex = 0; // "Menu";
            BottomAppBar = (AppBar)this.Resources["mailPageAppBar"];
            #region Get the Count of unread email and populate the count.
            //PJ : Do we need to await for this?
            UpdateNewEmailCountOnMenuItem();
            #endregion

            UpdateMeetingCount();
        }

        public void OnMenuItemSelected()
        {
            PanoramaWorkspace.DefaultSectionIndex = 0; // Menu;//For refreshing the Title to align center
            PanoramaWorkspace.Header = "Menu";
            
            
#if WP_80_SILVERLIGHT
            PanoramaWorkspace.Title = "Menu";
#endif
            //BottomAppBar = (AppBar)this.Resources["menuPageBar"];
            BottomAppBar = menuPageBar;

            //we need to refresh the count in case the background is not doing it.
            UpdateNewEmailCountOnMenuItem();
            UpdateMeetingCount();
        }

        private void OnNavigationToMenu(NavigationEventArgs obj)
        {
            UpdateNewEmailCountOnMenuItem();
            UpdateMeetingCount();
        }


        private void CreateEmailApplicationBarItems()
        {

        }

        string mailTitle = "Mail";

        /// <summary>
        /// Called when Email panorma item is activated : makes sure that selection is disabled and updates the application bar
        /// </summary>
        private void OnEmailItemSelected()
        {
            #region Mail
            PanoramaWorkspace.DefaultSectionIndex = 1; // Mail; //For refreshing the Title to align center
#if WP_80_SILVERLIGHT
            if (mailTitle.Equals("Inbox"))
                PanoramaWorkspace.Title = "Mail";
            else
                PanoramaWorkspace.Title = mailTitle;
#endif
            if (mailTitle.Equals("Inbox"))
                PanoramaWorkspace.Header = "Mail";
            else
                PanoramaWorkspace.Header = mailTitle;

            UpdateEmailItemUI();
            #endregion
        }

        private async void OnNavigationToEmail(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back && SuspensionManager.SessionState.ContainsKey("SelectedFolderToShowMail"))
            {
                #region Hardware Background from Select Folder emailScreen.

                workspace_datastore.models.Folder folderToShowMail = SuspensionManager.SessionState["SelectedFolderToShowMail"] as workspace_datastore.models.Folder;

                //mailTitle = SuspensionManager.SessionState["SelectedFolderToShowMail"] as string;
                mailTitle = folderToShowMail.displayName;
                SuspensionManager.SessionState.Remove("SelectedFolderToShowMail");
                ToastWithIndeterminateProgressBar toast = new ToastWithIndeterminateProgressBar();

                mCurrentVisibleMailFolder = folderToShowMail;
                bool isSyncDone = false;
                if (mCurrentVisibleMailFolder.displayName.ToLower() != "outbox" && mCurrentVisibleMailFolder.displayName.ToLower() != "flagged")
                {
                    toast.Title = "Please wait";
                    toast.SubTitle = "Sync in progress";
                    toast.Show();

                    Stopwatch stopWatch = new Stopwatch();
                    stopWatch.Start();

                    //move email items from one folder to another folder
#if WP_80_SILVERLIGHT
                    var selectedMails = emailScreen.mailList.SelectedItems;
                    List<ASMoveItemsRequest> moveItems = new List<ASMoveItemsRequest>();
                    if (selectedMails.Count > 0)
                    {
                        FolderManager folderManager = new FolderManager();
                        List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfoAsync(0).Result;
                        ActiveSyncPCL.Folder fldr = ActiveSync.getFolderObject(mailTitle, folderListDB);
                        foreach (MessageManager msg in selectedMails)
                        {
                            ASMoveItemsRequest objMoveItemsReq = new ASMoveItemsRequest();
                            objMoveItemsReq.sourceFolderId = msg.folderId.ToString();
                            objMoveItemsReq.destFolderId = fldr.Id;
                            objMoveItemsReq.msgId = msg.serverID;
                            moveItems.Add(objMoveItemsReq);
                        }
                        //call active sync method to move email tiems
                        await mActiveSync.MoveEmailItems(moveItems);
                    }
#endif
                    //end
                    //isSyncDone = await mActiveSync.SyncMailBasedOnFolder(mailTitle);
                    #if TO_FIX_FOR_TABLET
                    mSyncBGWorker.SetScreenObject(this);
                    bool isSyncStarted = mSyncBGWorker.InitializeAndDoSync(mCurrentVisibleMailFolder);
#endif
#if TO_FIX_FOR_TABLET
                    if (isSyncStarted)
                    {
                        stopWatch.Stop();

                        ToastNotification toastDelay = new ToastNotification();
                        string message = "Last Updated " + Decimal.ToInt32(stopWatch.ElapsedMilliseconds / 1000) + " Seconds Ago";
                        toastDelay.CreateToastNotificationForVertical(message, "");

                        //in BG worker sync we are not relying on this
                        if (isSyncDone)
                        {
#if WP_80_SILVERLIGHT
                            if (mailTitle.Equals("Inbox"))
                                //    Mail.Header = "Mail";
                                PanoramaWorkspace.Title = "Mail";
                            else
                                PanoramaWorkspace.Title = mailTitle;
#endif
                            //BottomAppBar = (AppBar)this.Resources["mailPageAppBar"];
                            BottomAppBar = mailPageAppBar;
                        }
                    }
                    else
                    {

                        #if TO_FIX_FOR_TABLET
                        ToastNotification toastDelay = new ToastNotification();
                        string message = "Please wait";
                        toastDelay.CreateToastNotificationForVertical(message, "Another Sync Session is going on. Please try after some time");
#endif
                    }
                    #endif
                    //else
                    //{
                    //    ToastNotification toast1 = new ToastNotification();
                    //    toast1.CreateToastNotificationForVertical("Couldn't open connection to the server", "Please try to sync again the device.");
                    //}
                }
                else
                {
                    // Local Database only
                    if (mCurrentVisibleMailFolder.displayName.ToLower() == "outbox")
                    {
                        PanoramaWorkspace.Header = mailTitle;

                        //BottomAppBar = (AppBar)this.Resources["mailPageAppBar"];
                        BottomAppBar = mailPageAppBar;

#if NOT_NEEDED
                        FolderManager fm = new FolderManager();
                        int folderId = await fm.GetFolderIdentifierBasedOnFolderNameAsync("Outbox");
                        List<workspace_datastore.models.Folder> outboxFolders = await fm.GetFolderInfoAsync(folderId);
                        OutboxManager om = new OutboxManager();
                        List<Outbox> msgs = await om.GetOutboxDataAsync("");
                        if(msgs != null && msgs.Count > 0)
                        {
                            List<workspace_datastore.models.Message> msgList = new List<workspace_datastore.models.Message>();
                            //List<MessageManager> mmList = new List<MessageManager>();
                            foreach (Outbox outMsg in msgs)
                            {
                                workspace_datastore.models.Message msg = mActiveSync.DecodeMail(new StringBuilder(outMsg.bodyText));
                                msg.folder_id = folderId;
                                msg.receivedDate = outMsg.creationTime;
                                msg.id = outMsg.message_id;
                                if (outMsg.attachmentGuidsList.Length > 0)
                                {
                                }
                                msgList.Add(msg);
#if NOT_NEEDED
                                MessageManager mm = new MessageManager();
                                mm.folderId = folderId;
                                mm.receivedDate = outMsg.creationTime;
                                mm.messgaeId = outMsg.message_id;

                                mm.importance = msg.importance;
                                mm.toRecipientText = msg.toRecipientText;
                                mm.ccRecipientText = msg.ccRecipientText;
                                mm.bccRecipientText = msg.bccRecipientText;
                                mm.bodyText = msg.bodyText;
                                mm.subject = msg.subject;
                                mmList.Add(mm);
#endif // NOT_NEEDED
                            }

                            if(msgList.Count > 0)
                            {
                                MessageManager mmView = new MessageManager();
                                DataSourceAsync ds = new DataSourceAsync();
                                mmView.AddAllMessageInfoAsync(ds.Db, msgList);
                            }
                        }
#endif // NOT_NEEDED
#if WP_80_SILVERLIGHT
                        var dt = (EmailItemsViewModel)emailScreen.DataContext;
                        dt.LoadData("Outbox");
#endif
                    }
                    else if (mCurrentVisibleMailFolder.displayName.ToLower() == "flagged")
                    {
#if WP_80_SILVERLIGHT
                        //PK Date: 23 jan
                        //We dont need to get data from AS here
                        var dt = (EmailItemsViewModel)emailScreen.DataContext;
                        await dt.LoadData(mCurrentVisibleMailFolder.displayName);  // Probably inefficient
                        PanoramaWorkspace.Title = mailTitle;
                        //emailScreen.UpdateSource(DateTime.Now.ToOADate(), EmailItemsView.Order.Descending);
#endif
                    }
                }

                #endregion
            }
#if WP_80_SILVERLIGHT
            else if (e.NavigationMode == NavigationMode.Back && SuspensionManager.SessionState.ContainsKey("ApplicationNavigatedFromDraftMessage") && mCurrentVisibleMailFolder.displayName.ToLower() == "drafts")
            {
                if (SuspensionManager.SessionState["ApplicationNavigatedFromDraftMessage"].ToString().Trim().ToLower() == "drafts")
                {
                    SuspensionManager.SessionState.Remove("ApplicationNavigatedFromDraftMessage");
                    //if current folder is drafts folder then it will refresh data(in opther case when user will go in this folder it would be refreshed)
                    if (mCurrentVisibleMailFolder.displayName.ToLower() == "drafts")
                    {
                        var dt = (EmailItemsViewModel)emailScreen.DataContext;
                        await dt.LoadData(mCurrentVisibleMailFolder.displayName);
                        UpdateEmailItemUI();
                    }
                }
            }
            else if (e.NavigationMode == NavigationMode.Back && SuspensionManager.SessionState.ContainsKey("ApplicationNavigatedFromDetailPage"))
            {
                if (SuspensionManager.SessionState["ApplicationNavigatedFromDetailPage"].ToString() == "maildeleted")
                {
                    SuspensionManager.SessionState.Remove("ApplicationNavigatedFromDetailPage");
                    emailScreen.GenerateFolderIdAndSetCurrentFolder(mCurrentVisibleMailFolder);
                    await emailScreen.UpdateSource(DateTime.Now.ToOADate(), "", EmailItemsView.Order.Descending, false, true);
                }
            }
            else if ((e.NavigationMode == NavigationMode.Back && SuspensionManager.SessionState.ContainsKey("MailSettingsSaved")))
            {
                SuspensionManager.SessionState.Remove("MailSettingsSaved");

                ToastWithIndeterminateProgressBar toast = new ToastWithIndeterminateProgressBar();
                toast.Title = "Please wait";
                toast.SubTitle = "Sync in progress for selected folders";
                toast.Show();

                //Code to embed

                mSyncBGWorker.SetScreenObject(this);
                mSyncBGWorker.InitializeAndDoSync(mCurrentVisibleMailFolder);



                //if (!mEmailSyncBackgroundWorker.IsBusy)
                //{
                //    mEmailSyncBackgroundWorker.WorkerReportsProgress = true;

                //    mEmailSyncBackgroundWorker.DoWork -= bg_DoWork;
                //    mEmailSyncBackgroundWorker.RunWorkerCompleted -= bg_RunWorkerCompleted;
                //    //mEmailSyncBackgroundWorker.ProgressChanged -= bg_ProgressChanged;

                //    mEmailSyncBackgroundWorker.DoWork += bg_DoWork;
                //    mEmailSyncBackgroundWorker.RunWorkerCompleted += bg_RunWorkerCompleted;
                //    //mEmailSyncBackgroundWorker.ProgressChanged += bg_ProgressChanged;
                //    mEmailSyncBackgroundWorker.RunWorkerAsync();
                //}
            }
            else if (SuspensionManager.SessionState.ContainsKey("EventResponseFromEmailScreenRSVPToast"))
            {
                string msgforToast = (string)SuspensionManager.SessionState["EventResponseFromEmailScreenRSVPToast"];
                SuspensionManager.SessionState.Remove("EventResponseFromEmailScreenRSVPToast");
                ToastNotification toastRSVP = new ToastNotification();
                string messageRSVP = "";
                toastRSVP.CreateToastNotificationForVertical(messageRSVP, "");
                if (msgforToast.Length <= 20)
                    messageRSVP = msgforToast;
                else
                    messageRSVP = msgforToast.Substring(0, 15) + " " + "...";
                // toast.SubTitle = "Syncing in progress for all selected folders";
                toastRSVP.CreateToastNotificationForVertical(messageRSVP, "");
            }
            else if (e.NavigationMode == NavigationMode.Back)
            {
                // Coming back from the search page will get us in here
                emailScreen.UpdateSource(DateTime.Now.ToOADate(), "", EmailItemsView.Order.Descending, false, true);
            }

            //var dt = (EmailItemsViewModel)emailScreen.DataContext;
            //await dt.LoadData(mCurrentVisibleMailFolder);
            //UpdateEmailItemUI();
#endif
        }

        void bg_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("PCT: {0}", e.ProgressPercentage);
        }

        private void UpdateEmailItemUI()
        {
#if WP_80_SILVERLIGHT
            if (!emailScreen.mailList.IsSelectionEnabled)
            {
                UpdateEmailItemUIBasedOnEmailCount();
            }
            else
            {
                UpdateEmailItemUIBasedOnEmailCount();
                emailScreen.mailList.IsSelectionEnabled = false; // Will update the application bar too
                //PJ:Does not update the app bar automatically.
                BottomAppBar = (AppBar)this.Resources["mailPageAppBar"];
                foreach (ApplicationBarIconButton btn in BottomAppBar.Buttons)
                {
                    btn.IsEnabled = true;
                }

            }
#endif
        }

        private async void UpdateEmailItemUIBasedOnEmailCount()
        {
            int mailCount = await GetTotalEmailCountForInbox();
            BottomAppBar = (mailCount == 0) ? (AppBar)this.Resources["mail_no_contentpageAppBar"] : BottomAppBar = (AppBar)this.Resources["mailPageAppBar"];
            if (mCurrentVisibleMailFolder.displayName.ToLower() == "outbox")
            {
                //BottomAppBar = (AppBar)this.Resources["mailPageAppBar"];
                BottomAppBar = mailPageAppBar;
            }
#if WP_80_SILVERLIGHT
            // Only show the 'new email' message for Inbox-type folders.  This excludes 'drafts', 'flagged', 'outbox',
            //   'sent items', 'deleted items'.
            if (mCurrentVisibleMailFolder.displayName.ToLower().CompareTo("drafts") != 0 &&
                mCurrentVisibleMailFolder.displayName.ToLower().CompareTo("flagged") != 0 &&
                mCurrentVisibleMailFolder.displayName.ToLower().CompareTo("outbox") != 0 &&
                mCurrentVisibleMailFolder.displayName.ToLower().CompareTo("sent items") != 0 &&
                mCurrentVisibleMailFolder.displayName.ToLower().CompareTo("deleted items") != 0)
            {
                if (mailCount == 0)
                {
                    emailScreen.Visibility = Visibility.Collapsed;
                    MailText.Visibility = Visibility.Visible;
                    InstructionText.Visibility = Visibility.Visible;
                }
                else
                {
                    emailScreen.Visibility = Visibility.Visible;
                    MailText.Visibility = Visibility.Collapsed;
                    InstructionText.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                emailScreen.Visibility = Visibility.Visible;
                MailText.Visibility = Visibility.Collapsed;
                InstructionText.Visibility = Visibility.Collapsed;
            }
#endif
        }

        private async Task<int> GetTotalEmailCountForInbox()
        {
            // The 'Flagged' folder will have an id of -1.
            int totalEmails = 0;
#if USE_ASYNC

            int folderId = await mgr.GetFolderIdentifierBasedOnFolderNameAsync(mCurrentVisibleMailFolder.displayName, mCurrentVisibleMailFolder.parentFolder_id);
#else
             int folderId = mgr.GetFolderIdentifierBasedOnFolderName("Inbox");
#endif
            if (-1 != folderId)
            {
#if USE_ASYNC
                totalEmails = msgManager.GetTotalMailCountForFolderIdAsync(folderId).Result;
#else
                int totalEmails = msgManager.GetTotalMailCountForFolderId(folderIdInbox);
#endif
            }
            else
            {
                if (mCurrentVisibleMailFolder.displayName.ToLower().CompareTo("flagged") == 0)
                {
                    MessageManager mm = new MessageManager();
                    List<MessageManager> flaggedItems = await mm.GetMessagewithAttachInfoAsyncForFlagged(-1, 0);
                    totalEmails = flaggedItems.Count;
                }
            }
            return totalEmails;
        }

        protected void SetEmailMultiSelectionCaps(bool selectionEnabled)
        {
            if (!selectionEnabled)
            {
#if WP_80_SILVERLIGHT
                emailScreen.mailList.IsSelectionEnabled = false;
                BottomAppBar = (AppBar)this.Resources["mailPageAppBar"];
                foreach (ApplicationBarIconButton btn in ApplicationBar.Buttons)
                {
                    btn.IsEnabled = true;
                }
#endif
            }
        }

        /// <summary>
        /// Back Key Pressed = leaves the selection mode
        /// </summary>
        /// <param name="e"></param>
        //protected void OnEmailBackKeyPressed(CancelEventArgs e)
        //{
        //    if (emailScreen.mailList.IsSelectionEnabled)
        //    {
        //        SetEmailMultiSelectionCaps(false);
        //        e.Cancel = true;
        //    }

        //}

        //private DependencyObject FindChildControl<T>(DependencyObject control, string ctrlName)
        //{
        //    int childNumber = VisualTreeHelper.GetChildrenCount(control);
        //    for (int i = 0; i < childNumber; i++)
        //    {
        //        DependencyObject child = VisualTreeHelper.GetChild(control, i);
        //        FrameworkElement fe = child as FrameworkElement;
        //        // Not a framework element or is null
        //        if (fe == null) return null;

        //        if (child is T && fe.Name == ctrlName)
        //        {
        //            // Found the control so return
        //            return child;
        //        }
        //        else
        //        {
        //            // Not found it - search children
        //            DependencyObject nextLevel = FindChildControl<T>(child, ctrlName);
        //            if (nextLevel != null)
        //                return nextLevel;
        //        }
        //    }
        //    return null;
        //}

        private async void UpdateNewEmailCountOnMenuItem()
        {
#if USE_ASYNC
            if (folderIdInbox == 0)
                folderIdInbox = await mgr.GetFolderIdentifierBasedOnFolderNameAsync("Inbox");
#else
            int folderId = mgr.GetFolderIdentifierBasedOnFolderName("Inbox");
#endif

#if USE_ASYNC
            int inboxUnreadEMails = msgManager.GetNoofUnreadMsgBasedOnFolderIDAsync(folderIdInbox).Result;
#else
            int inboxUnreadEMails = msgManager.GetNoofUnreadMsgBasedOnFolderID(folderIdInbox);
#endif
            SetNewMail(inboxUnreadEMails.ToString());
        }

        private async void UpdateMeetingCount()
        {
            #region renderCalender

            EventManager evMan1 = new EventManager();
            FolderManager folderManager1 = new FolderManager();

#if USE_ASYNC
            List<workspace_datastore.models.Folder> folderListDB1 = await folderManager1.GetFolderInfoAsync(0);
#else
            List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfo(0);
#endif
            var folderDB1 = folderListDB1.Find(x => x.displayName == "Calendar");
            if (null != folderDB1)
            {
#if USE_ASYNC
                List<Events> allEvent1 = evMan1.GetEventInfoAsync(folderDB1.id, 0).Result;
#else
                List<Events> allEvent = evMan.GetEventInfo(folderDB.id, 0);
#endif
                List<Events> recurrentEvents = new List<Events>();
                //recurrentEvents = RecurringEventsBuilder.BuildRecurringEvents(allEvent1);
                List<Events> eventToday = new List<Events>(); // WP_80_SILVERLIGHT RecurringEventsBuilder.GenerateWindowForFutureDays(1, DateTime.Today.FromBinary(), allEvent1, recurrentEvents);

#if WP_80_SILVERLIGHT
                mTodaysEvents = eventToday.FindAll(c => DateTime.FromOADate(c.startDayAndTime).Date == DateTime.Today.Date);
                var nonRecurrentEvent = allEvent1.FindAll(c => DateTime.FromOADate(c.startDayAndTime).Date == DateTime.Today.Date);

                foreach (var item in nonRecurrentEvent)
                {
                    mTodaysEvents.Add(item);
                }

                /*Reminder code */
                mReminderNotifications = ScheduledActionService.GetActions<ScheduledNotification>();
                HtmlToText htmlTotext = new HtmlToText();

                List<Events> eventsToAddToReminders = new List<Events>();              //add these events sans duplicate
                List<Events> duplicateEvents = new List<Events>();              //duplicate events

                foreach (Events copyEvent in mTodaysEvents)
                {
                    eventsToAddToReminders.Add(copyEvent);
                }

                foreach (Events thisEvent in mTodaysEvents)
                {
                    if (thisEvent.description == null)
                    {
                        thisEvent.description = "";
                    }
                    foreach (ScheduledNotification notification in mReminderNotifications)
                    {
                        if (thisEvent.uid == notification.Name)
                        {
                            duplicateEvents.Add(thisEvent);
                        }
                    }
                }

                foreach (Events evnts in duplicateEvents)
                {
                    eventsToAddToReminders.Remove(evnts);
                }

                foreach (var item in eventsToAddToReminders)
                {
                    String name = item.uid;

                    RecurrenceInterval recurrence = RecurrenceInterval.None;

                    Reminder reminder = new Reminder(name);
                    
                    try
                    {
                        reminder.Title = item.subject;
                        int remin = item.originalReminder;
                        //The content string cannot be zero length. The maximum length of the content string is 256 characters. 
                        string content = htmlTotext.ConvertHtml(item.description);
                        //check content is available or not                    
                        if (string.IsNullOrWhiteSpace(content))
                        {
                            reminder.Content = "Content is not available.";
                        }
                        else
                        {
                            //Check if content length is more that 256 character
                            if (content.Length > 256)
                            {
                                //take string from 0 to 255 character
                                reminder.Content = content.Substring(0, 255);
                            }
                            else
                            {
                                reminder.Content = content;
                            }
                        }
                        reminder.BeginTime = DateTime.FromOADate(item.startDayAndTime).AddMinutes((-1) * item.originalReminder);   //show reminder before actual beginning                                      
                        reminder.RecurrenceType = recurrence;

                        if (DateTime.Now.AddMinutes(1) < DateTime.FromOADate(item.endDayAndTime))    //only those events add who has not expiredd else exception will be thrown
                            reminder.ExpirationTime = DateTime.FromOADate(item.endDayAndTime);

                        if (DateTime.Now.AddMinutes(1) < DateTime.FromOADate(item.startDayAndTime).AddMinutes((-1) * item.originalReminder))  //add only those events who has not started else exception is thrown
                        {
                            LoggingWP8.WP8Logger.LogMessage("Adding EVENT REMINDER {0} to with start time of {1} ScheduledActionsService", item.subject, DateTime.FromOADate(item.startDayAndTime).ToShortDateString() + " " + DateTime.FromOADate(item.startDayAndTime).ToShortTimeString());
                            ScheduledActionService.Add(reminder);
                        }
                    }
                    catch (System.ArgumentOutOfRangeException)
                    {
                        //Why is this exception happening?
                        //The content string cannot be zero length. The maximum length of the content string is 256 characters.(Dhruv)
                    }
                    catch (System.InvalidOperationException e)
                    {
                        if(e.Message.Contains("BNS Error: The maximum number of ScheduledActions of this type have already been added"))
                        {
                            mReminderNotifications = ScheduledActionService.GetActions<ScheduledNotification>();
                            CustomMessageBox messageBox = new CustomMessageBox()
                            {
                                Caption = "Too many reminders",
                                Message = string.Format("There are {0} Event reminders which is too many", mReminderNotifications.Count()),
                                LeftButtonContent = "OK",

                            };
                            messageBox.Dismissed += (s1, e1) =>
                            {
                                switch (e1.Result)
                                {
                                    case CustomMessageBoxResult.LeftButton:
                                        break;
                                    case CustomMessageBoxResult.RightButton:
                                        break;
                                    case CustomMessageBoxResult.None:
                                        break;
                                    default:
                                        break;
                                }
                            };
                            messageBox.Show();
                        }
                    }
                }
                /*Reminder code */
#endif
                TextBlock meetingSubject = FindChildControl<TextBlock>(Menu, "meetingSubject") as TextBlock;
                if (mTodaysEvents.Count == 0)
                {
#if WP_80_SILVERLIGHT
                    meetingSubject.Text = "No meetings";
                    timeStartDuration1.Text = "";
                    timeEndDuration1.Text = "";
                    moreMeetingToday1.Text = "0";
                    dashTime.Visibility = Visibility.Collapsed;
#else
                    
                    meetingSubject.Text = "No meetings";

#endif
                }
                else
                {
                    meetingSubject.Text = mTodaysEvents[0].subject;
                    if (mTodaysEvents[0].allDay == 0)
                    {
                        TextBlock timeStartDuration1 = FindChildControl<TextBlock>(Menu, "timeStartDuration1") as TextBlock;
                        timeStartDuration1.Text = DateTimeExtensions.FromOADate(mTodaysEvents[0].startDayAndTime).ToShortTimeString();

                        TextBlock timeEndDuration1 = FindChildControl<TextBlock>(Menu, "timeEndDuration1") as TextBlock;
                        timeEndDuration1.Text = DateTimeExtensions.FromOADate(mTodaysEvents[0].endDayAndTime).ToShortTimeString();

                        TextBlock dashTime = FindChildControl<TextBlock>(Menu, "dashTime") as TextBlock;
                        dashTime.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        StackPanel todaySch = FindChildControl<StackPanel>(calendar, "todaySch") as StackPanel;
                        todaySch.Visibility = Visibility.Collapsed;

                        TextBlock allDayEvent = FindChildControl<TextBlock>(calendar ,"allDayEvent") as TextBlock;
                        allDayEvent.Visibility = Visibility.Visible;
                    }
                    TextBlock moreMeetingToday1 = FindChildControl<TextBlock>(Menu, "moreMeetingToday1") as TextBlock;
                    moreMeetingToday1.Text = (mTodaysEvents.Count - 1).ToString();

                    TextBlock todayDate = FindChildControl<TextBlock>(calendar, "todayDate") as TextBlock;
                    todayDate.Text = DateTimeExtensions.FromOADate(mTodaysEvents[0].startDayAndTime).Day.ToString();
                    
                }
            }
            #endregion
        }

        public void SetNewMail(string numberOfnewMails)
        {
            TextBlock NumberOfNewMail = FindChildControl<TextBlock>(Menu, "NumberOfNewMail") as TextBlock;

            if (!NumberOfNewMail.Text.Equals(numberOfnewMails))
            {
                NumberOfNewMail.Text = numberOfnewMails;
            }
        }

        public void SetMeetingText(string numberOfMoreMeetings)
        {
            TextBlock moreMeetingToday = FindChildControl<TextBlock>(calendar, "moreMeetingToday") as TextBlock;
            moreMeetingToday.Text = numberOfMoreMeetings + " " + "more";
        }

        private void OnNavigateToBrowser(NavigationEventArgs obj)
        {
            #region ShowBrowser
            if (SuspensionManager.SessionState.ContainsKey("ApplicationNavigatedFrom") && SuspensionManager.SessionState["ApplicationNavigatedFrom"].ToString() == "WebBrowserApp")
            {
                SuspensionManager.SessionState.Remove("ApplicationNavigatedFrom");
                BrowserItemDisplay();
            }
            #endregion
        }

        private void InitialzeBrowserItem()
        {
            #region Browser
            BrowserItemDisplay();
            #endregion
        }

        private async void BrowserItemDisplay()
        {
#if WP_80_SILVERLIGHT
            try
            {
                List<WebSettings> listTemp = await SettMgr.ReadRecentUrls();
                //redraw only of there is a change in the recent URL
                //PJ: to test the condition
                if ((listTemp != null && list == null) || (listTemp != null && !list.Equals(listTemp)))
                {
                    list = listTemp;
                    NoRecentPanel.Visibility = Visibility.Collapsed;
                    recentUrlList.Visibility = Visibility.Visible;
                    List<Favicons> favList = new List<Favicons>();
                    foreach (WebSettings web in list)
                    {
                        Uri url = new Uri("http://www.google.com/s2/favicons?domain=" + web.url, UriKind.Absolute);
                        favList.Add(new Favicons() { faviconPath = url, siteUrl = web.url, title = web.title });
                    }
                    favList.Reverse();
                    recentFaviconList.ItemsSource = favList.Take(10).ToList();
                }
                else
                {
                    NoRecentPanel.Visibility = Visibility.Visible;
                    recentUrlList.Visibility = Visibility.Collapsed;
                }

            }
            catch
            {

            }
#endif
        }


        private void OnBrowserItemSelected()
        {
            PanoramaWorkspace.DefaultSectionIndex = 5; // browser;//For refreshing the Title to align center
#if WP_80_SILVERLIGHT
            PanoramaWorkspace.Title = "Browser";
            BottomAppBar = null;
            pop.Visibility = Visibility.Visible;
#endif

        }

        private async void OnNavigationToCalendar(NavigationEventArgs e)
        {
            UpdateMeetingCount();

            if (e.NavigationMode == NavigationMode.Back)
            {
                //pop.Visibility = Visibility.Collapsed;
                EventManager evMan = new EventManager();
                FolderManager folderManager = new FolderManager();
#if USE_ASYNC
                List<workspace_datastore.models.Folder> folderListDB = await folderManager.GetFolderInfoAsync(0);
#else
                    List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfo(0);
#endif
                var folderDB = folderListDB.Find(x => x.displayName == "Calendar" && x.parentFolder_id == 0);
#if USE_ASYNC
                List<Events> allEvent = evMan.GetEventInfoForDaysAsync(DateTime.Today.ToUniversalTime().ToOADate(), 3, 3).Result;
                List<Events> pastRecoccurentallEvents = evMan.GetRecurrentEventInfoForDaysAsync(DateTime.Today.ToUniversalTime().ToOADate(), 2, 5).Result;
                List<Events> pastMultipleDayEvents = evMan.GetMultipleDayEventInfoAsync(DateTime.Today.ToUniversalTime().ToOADate(), 0, 5).Result;
                allEvent.AddRange(pastRecoccurentallEvents);

#else
                    List<Events> allEvent = evMan.GetEventInfo(folderDB.id, 0);
#endif
                //  DateTime test = startDateandTime(allEvent[0].startTime);
                //get today event
                DateTime today = DateTime.Now; //DateTime.Today.Subtract(new TimeSpan(3, 0, 0, 0));
                List<Events> recurringEvents = RecurringEventsBuilder.FilterRecurringEventDefinitionsFromSource(allEvent);
                List<Events> futureEvents = RecurringEventsBuilder.GenerateWindowForFutureDays(3, today.ToOADate(), allEvent, recurringEvents);
                allEvent = allEvent.Concat(futureEvents).ToList();
                foreach (Events eventO in allEvent)
                {
                    //double timefromUTCminutes = CalculateDisatnceFromUTC(eventO.timeZone);
                    //double biasForEventDate = currentTimeZoneMinuteFromUTC - timefromUTCminutes;
                    eventO.startDayAndTime = DateTimeExtensions.FromOADate(eventO.startDayAndTime).AddMinutes(currentTimeZoneMinuteFromUTC).ToOADate();
                    eventO.endDayAndTime = DateTimeExtensions.FromOADate(eventO.endDayAndTime).AddMinutes(currentTimeZoneMinuteFromUTC).ToOADate();
                }
                foreach (Events eventO in pastMultipleDayEvents)
                {
                    //double timefromUTCminutes = CalculateDisatnceFromUTC(eventO.timeZone);
                    //double biasForEventDate = currentTimeZoneMinuteFromUTC - timefromUTCminutes;
                    eventO.startDayAndTime = DateTimeExtensions.FromOADate(eventO.startDayAndTime).AddMinutes(currentTimeZoneMinuteFromUTC).ToOADate();
                    eventO.endDayAndTime = DateTimeExtensions.FromOADate(eventO.endDayAndTime).AddMinutes(currentTimeZoneMinuteFromUTC).ToOADate();
                }

                List<Events> multiDayEventsPast = CreateMultiDayEvent(pastMultipleDayEvents);
                allEvent.AddRange(multiDayEventsPast);

                var multiDayEventfromTodayOnward = allEvent.FindAll(x => (x.startDayAndTime >= DateTime.Today.ToOADate()) && (DateTimeExtensions.FromOADate(x.endDayAndTime).Date.ToOADate() > DateTimeExtensions.FromOADate(x.startDayAndTime).Date.ToOADate()) && (x.reoccurs == 0));
                List<Events> multiDayEventsNowAndFuture = CreateMultiDayEventFuture(multiDayEventfromTodayOnward);

                foreach (var item in multiDayEventfromTodayOnward)
                {
                    allEvent.Remove(item);
                }

                allEvent.AddRange(multiDayEventsNowAndFuture);
                //allEvent.AddRange(multiDayEventsPast);

                var todayevnt = allEvent.FindAll(c => DateTimeExtensions.FromOADate(c.startDayAndTime).Date == today.Date && c.selfAttendeeStatus != 5 && c.meetingStatus != 7).OrderBy(o => o.startDayAndTime).ToList();

                var tomevnt = allEvent.FindAll(c => DateTimeExtensions.FromOADate(c.startDayAndTime).Date == today.Date.AddDays(1).Date && c.selfAttendeeStatus != 5 && c.meetingStatus != 7).OrderBy(o => o.startDayAndTime).ToList();
                var datomevnt = allEvent.FindAll(c => DateTimeExtensions.FromOADate(c.startDayAndTime).Date == today.Date.AddDays(2).Date && c.selfAttendeeStatus != 5 && c.meetingStatus != 7).OrderBy(o => o.startDayAndTime).ToList();
                //  todayevnt.Sort(x=>startDateandTime(x.startTime).Date.Date. )

                TextBlock dashText = FindChildControl<TextBlock>(calendar, "dashText") as TextBlock;
                TextBlock dashDuration = FindChildControl<TextBlock>(calendar, "dashDuration") as TextBlock;
                TextBlock moreToday = FindChildControl<TextBlock>(calendar, "moreToday") as TextBlock;
                TextBlock noTodayMeet = FindChildControl<TextBlock>(calendar, "noTodayMeet") as TextBlock;
                TextBlock moreMeetingToday = FindChildControl<TextBlock>(calendar, "moreMeetingToday") as TextBlock;
                TextBlock todayDate = FindChildControl<TextBlock>(calendar, "todayDate") as TextBlock;
                
               
                #region Events of Today
                if (todayevnt.Count == 0)
                {
                    TextBlock firstAgendaTodayTitle = FindChildControl<TextBlock>(calendar, "firstAgendaTodayTitle") as TextBlock;
                    firstAgendaTodayTitle.Text = "No meetings";

                    TextBlock firstAgendaTodaySubTitle = FindChildControl<TextBlock>(calendar, "firstAgendaTodaySubTitle") as TextBlock;
                    firstAgendaTodaySubTitle.Text = "";

                    TextBlock timeStartDuration = FindChildControl<TextBlock>(calendar, "timeStartDuration") as TextBlock;
                    timeStartDuration.Text = "";

                    TextBlock timeEndDuration = FindChildControl<TextBlock>(calendar, "timeEndDuration") as TextBlock;
                    timeEndDuration.Text = "";

                    moreMeetingToday.Text = "0";
                    dashText.Visibility = Visibility.Collapsed;
                    dashDuration.Visibility = Visibility.Collapsed;
                    moreToday.Visibility = Visibility.Collapsed;
                    noTodayMeet.Visibility = Visibility.Collapsed;

                    
                    todayDate.Text = DateTime.Today.Day.ToString();
                }
                else
                {
                    TextBlock firstAgendaTodayTitle = FindChildControl<TextBlock>(calendar, "firstAgendaTodayTitle") as TextBlock;
                    firstAgendaTodayTitle.Text = todayevnt[0].subject;

                    TextBlock firstAgendaTodaySubTitle = FindChildControl<TextBlock>(calendar, "firstAgendaTodaySubTitle") as TextBlock;
                    firstAgendaTodaySubTitle.Text = todayevnt[0].location;

                    if (todayevnt[0].allDay == 0)
                    {
                        TextBlock timeStartDuration = FindChildControl<TextBlock>(calendar, "timeStartDuration") as TextBlock;
                        timeStartDuration.Text = DateTimeExtensions.FromOADate(todayevnt[0].startDayAndTime).ToShortTimeString();

                        TextBlock timeEndDuration = FindChildControl<TextBlock>(calendar, "timeEndDuration") as TextBlock;
                        timeEndDuration.Text = DateTimeExtensions.FromOADate(todayevnt[0].endDayAndTime).ToShortTimeString();
                    }
                    else
                    {
                        StackPanel todaySch = FindChildControl<TextBlock>(calendar, "todaySch") as StackPanel;
                        todaySch.Visibility = Visibility.Collapsed;

                        TextBlock allDayEvent = FindChildControl<TextBlock>(calendar, "allDayEvent") as TextBlock;
                        allDayEvent.Visibility = Visibility.Visible;
                    }
                    if (todayevnt.Count == 1)
                    {
                        dashText.Visibility = Visibility.Visible;
                        dashDuration.Visibility = Visibility.Visible;
                        moreToday.Visibility = Visibility.Collapsed;
                        noTodayMeet.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        dashText.Visibility = Visibility.Visible;
                        dashDuration.Visibility = Visibility.Visible;
                        moreToday.Visibility = Visibility.Visible;
                        noTodayMeet.Visibility = Visibility.Visible;
                    }

                    moreMeetingToday.Text = (todayevnt.Count - 1).ToString();
                    todayDate.Text = DateTimeExtensions.FromOADate(todayevnt[0].startDayAndTime).Day.ToString();
                }
                #endregion

                #region Events of Tommorow
                TextBlock firstAgendaTommorowTitle = FindChildControl<TextBlock>(calendar, "firstAgendaTommorowTitle") as TextBlock;
                TextBlock moreMeetingTom = FindChildControl<TextBlock>(calendar, "moreMeetingTom") as TextBlock;
                TextBlock timeStartDurationTom = FindChildControl<TextBlock>(calendar, "timeStartDurationTom") as TextBlock;
                TextBlock tomDate = FindChildControl<TextBlock>(calendar, "tomDate") as TextBlock;
                Border tomMore = FindChildControl<Border>(calendar, "tomMore") as Border;
                Border tomNBorder = FindChildControl<Border>(calendar, "tomNBorder") as Border;
                TextBlock alldayEventtom = FindChildControl<TextBlock>(calendar, "alldayEventtom") as TextBlock;
                TextBlock firstAgendaTommorowSubTitle = FindChildControl<TextBlock>(calendar, "firstAgendaTommorowSubTitle") as TextBlock;
                TextBlock tomDash = FindChildControl<TextBlock>(calendar, "tomDash") as TextBlock;

                if (tomevnt.Count == 0)
                {
                    firstAgendaTommorowTitle.Text = "No meetings";
                    firstAgendaTommorowSubTitle.Text = "";

                    timeStartDurationTom.Text = "";

                    moreMeetingTom.Text = "0";

                    tomDash.Visibility = Visibility.Visible;
                    tomNBorder.Visibility = Visibility.Collapsed;
                    tomMore.Visibility = Visibility.Collapsed;

                    tomDate.Text = DateTime.Today.AddDays(1).Day.ToString();
                }
                else
                {
                    firstAgendaTommorowTitle.Text = tomevnt[0].subject;
                    firstAgendaTommorowSubTitle.Text = tomevnt[0].location;

                    if (tomevnt[0].allDay == 0)
                    {
                        timeStartDurationTom.Text = DateTimeExtensions.FromOADate(tomevnt[0].startDayAndTime).ToShortTimeString();
                    }
                    else
                    {
                        timeStartDurationTom.Visibility = Visibility.Collapsed;
                        alldayEventtom.Visibility = Visibility.Visible;
                    }

                    if (tomevnt.Count == 1)
                    {
                        tomDash.Visibility = Visibility.Visible;
                        tomNBorder.Visibility = Visibility.Collapsed;
                        tomMore.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        tomDash.Visibility = Visibility.Visible;
                        tomNBorder.Visibility = Visibility.Visible;
                        tomMore.Visibility = Visibility.Visible;
                    }

                    moreMeetingTom.Text = (tomevnt.Count - 1).ToString();
                    
                    tomDate.Text = DateTimeExtensions.FromOADate(tomevnt[0].startDayAndTime).Day.ToString();
                }
                #endregion
                #region Events of DayAfter Tommorrow
                TextBlock firstAgendaDayAfTommorowTitle = FindChildControl<TextBlock>(calendar, "firstAgendaDayAfTommorowTitle") as TextBlock;
                TextBlock alldayEventtom1 = FindChildControl<TextBlock>(calendar, "alldayEventtom1") as TextBlock;
                TextBlock timeStartDurationTom1 = FindChildControl<TextBlock>(calendar, "timeStartDurationTom1") as TextBlock;
                TextBlock dayAftomDate = FindChildControl<TextBlock>(calendar, "dayAftomDate") as TextBlock;
                Border dayAfTomNo = FindChildControl<Border>(calendar, "dayAfTomNo") as Border;
                Border dayAfTomMore = FindChildControl<Border>(calendar, "dayAfTomMore") as Border;
                TextBlock moreMeetingDayAfTom = FindChildControl<TextBlock>(calendar, "moreMeetingDayAfTom") as TextBlock;
                TextBlock firstAgendaDayAfTommorowSubTitle = FindChildControl<TextBlock>(calendar, "firstAgendaDayAfTommorowSubTitle") as TextBlock;
                TextBlock dayAfTomDash = FindChildControl<TextBlock>(calendar, "dayAfTomDash") as TextBlock;

                if (datomevnt.Count == 0)
                {
                    firstAgendaDayAfTommorowTitle.Text = "No meetings";
                    firstAgendaDayAfTommorowSubTitle.Text = "";

                    timeStartDurationTom1.Text = "";
                    // timeEndDuration.Text = startDateandTime(tomevnt[0].endTime).ToString("hh:mm tt");
                    moreMeetingDayAfTom.Text = "";

                    dayAfTomDash.Visibility = Visibility.Collapsed;
                    dayAfTomNo.Visibility = Visibility.Collapsed;
                    dayAfTomMore.Visibility = Visibility.Collapsed;

                    dayAftomDate.Text = DateTime.Today.AddDays(2).Day.ToString();
                }
                else
                {
                    firstAgendaDayAfTommorowTitle.Text = datomevnt[0].subject;
                    firstAgendaDayAfTommorowSubTitle.Text = datomevnt[0].location;
                    if (datomevnt[0].allDay == 0)
                    {
                        timeStartDurationTom1.Text = DateTimeExtensions.FromOADate(datomevnt[0].startDayAndTime).ToShortTimeString();
                        // timeEndDuration.Text = startDateandTime(tomevnt[0].endTime).ToString("hh:mm tt");
                    }
                    else
                    {
                        timeStartDurationTom1.Visibility = Visibility.Collapsed;
                        alldayEventtom1.Visibility = Visibility.Visible;
                    }

                    if (datomevnt.Count == 1)
                    {
                        dayAfTomDash.Visibility = Visibility.Visible;
                        dayAfTomNo.Visibility = Visibility.Collapsed;
                        dayAfTomMore.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        dayAfTomDash.Visibility = Visibility.Visible;
                        dayAfTomNo.Visibility = Visibility.Visible;
                        dayAfTomMore.Visibility = Visibility.Visible;
                    }

                    moreMeetingDayAfTom.Text = (datomevnt.Count - 1).ToString();
                    dayAftomDate.Text = DateTimeExtensions.FromOADate(datomevnt[0].startDayAndTime).Day.ToString();
                }

                #endregion

                BottomAppBar = calendarPageAppBar;
                //BottomAppBar = (AppBar)this.Resources["calendarPageAppBar"];
            }

        }


        private List<Events> CreateMultiDayEventFuture(List<Events> multiDayEventfromTodayOnward)
        {
            List<Events> multiDayEvents = new List<Events>();

            try
            {
                foreach (var eventM in multiDayEventfromTodayOnward)
                {
                    DateTime startDateTime = DateTimeExtensions.FromOADate(eventM.startDayAndTime);
                    DateTime endDateTime = DateTimeExtensions.FromOADate(eventM.endDayAndTime);

                    #region Add First Instace
                    Events multiDaySingleEvent = new Events();
                    multiDaySingleEvent.CopyToThis(eventM);
                    multiDaySingleEvent.startDayAndTime = startDateTime.ToOADate();
                    multiDaySingleEvent.endDayAndTime = startDateTime.Date.AddHours(24).ToOADate();
                    multiDayEvents.Add(multiDaySingleEvent);

                    startDateTime = startDateTime.Date.AddHours(24);
                    #endregion

                    while (startDateTime.Date.ToOADate() < endDateTime.Date.ToOADate())
                    {
                        Events multiDaySingleEventFull = new Events();
                        multiDaySingleEventFull.CopyToThis(eventM);
                        multiDaySingleEventFull.startDayAndTime = startDateTime.ToOADate();
                        multiDaySingleEventFull.endDayAndTime = startDateTime.Date.AddHours(24).ToOADate();
                        multiDayEvents.Add(multiDaySingleEventFull);
                        startDateTime = startDateTime.Date.AddHours(24);
                    }

                    #region Add last Instace
                    double originalStartDate = DateTimeExtensions.FromOADate(eventM.startDayAndTime).Date.ToOADate();
                    double originalEndDate = DateTimeExtensions.FromOADate(eventM.endDayAndTime).Date.ToOADate();
                    if ((originalStartDate != originalEndDate) && (DateTimeExtensions.FromOADate(eventM.endDayAndTime).ToShortTimeString() != "12:00 AM"))
                    {
                        Events multiDaySingleEventLast = new Events();
                        multiDaySingleEventLast.CopyToThis(eventM);
                        multiDaySingleEventLast.startDayAndTime = startDateTime.ToOADate();
                        multiDaySingleEventLast.endDayAndTime = endDateTime.ToOADate();
                        multiDayEvents.Add(multiDaySingleEventLast);

                        startDateTime = startDateTime.Date.AddHours(24);
                    #endregion
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return multiDayEvents;
        }

        private void OnFileItemSelected()
        {
            PanoramaWorkspace.DefaultSectionIndex = 5; // files;//For refreshing the Title to align center
#if WP_80_SILVERLIGHT
            PanoramaWorkspace.Title = "Files";
            pop.Visibility = Visibility.Collapsed;
            BottomAppBar = null;
            //create default folders
            //PJ: Do we need to await for this method to complete?
#endif
            ShowFiles();
        }

        private KeyValuePair<int, int> NearestTodayEvent(List<Events> todayEvents)
        {
            List<double> differences = new List<double>();

            foreach (Events currEvnt in todayEvents)
            {
                if (currEvnt.endDayAndTime < currEvnt.startDayAndTime)
                {
                    var endtime = DateTimeExtensions.FromOADate(currEvnt.endDayAndTime).TimeOfDay;
                    differences.Add(DateTime.Today.Add(endtime).ToOADate() - DateTime.Now.ToOADate());
                }
                else
                {
                    differences.Add(currEvnt.endDayAndTime - DateTime.Now.ToOADate());
                }

            }

            int index = 0;

            index = differences.Select((v, i) => new { v, i }).Where(x => x.v > 0).Select(x => x.i).DefaultIfEmpty(-1).First();

            int moreLeft = 0;
            moreLeft = differences.Count(x => (x > 0));
            if (index != -1)
                return new KeyValuePair<int, int>(index, moreLeft - 1);
            else
                return new KeyValuePair<int, int>(index, 0);
        }

        private async void OnContactItemSelected()
        {
#if WP_80_SILVERLIGHT
            #region RenderContacts
            PanoramaWorkspace.DefaultItem = contacts;//For refreshing the Title to align center
            PanoramaWorkspace.Title = "Contacts";
            pop.Visibility = Visibility.Collapsed;
            FolderManager mgr = new FolderManager();
#if USE_ASYNC
            int fdId = await mgr.GetFolderIdentifierBasedOnFolderNameAsync("Contacts");
#else
           int fdId = fdmgr.GetFolderIdentifierBasedOnFolderName("Contacts");
#endif

            if (fdId != -1)
            {
                ContactManager contManager = new ContactManager();
                List<Contacts> contlist = new List<Contacts>();
#if USE_ASYNC
                //  contlist = contManager.GetContactInfoAsync(fdId, 0).Result;
                contlist = contManager.GetRecentContactInfoAsync().Result;
#else              
                    contlist = contManager.GetContactInfo(fdId, 0);
#endif
                contactList.ItemsSource = contlist.Take(7).ToList();
#if USE_ASYNC
                int contactcount = contManager.GetTotalContactCountForFolderIdAsync(fdId).Result;
#else
                    int contactcount = contManager.GetTotalContactCountForFolderId(fdId);
#endif

                if (contactcount == 0)
                {
                    RecentText.Visibility = Visibility.Collapsed;
                    ViewAll.Visibility = Visibility.Collapsed;
                    ContactsText.Visibility = Visibility.Visible;
                    ContactsInstructionText.Visibility = Visibility.Visible;
                    BottomAppBar = (AppBar)this.Resources["contacts_no_contentpageAppBar"];
                }
                else
                {
                    RecentText.Visibility = Visibility.Visible;
                    ViewAll.Visibility = Visibility.Visible;
                    ContactsText.Visibility = Visibility.Collapsed;
                    ContactsInstructionText.Visibility = Visibility.Collapsed;
                    BottomAppBar = (AppBar)this.Resources["contactsPageAppBar"];
                }
            }
            #endregion
#endif // WP_80_SILVERLIGHT
        }

        private async void OnCalendarItemSelected()
        {
//            #region renderCalender
//            PanoramaWorkspace.DefaultSectionIndex = 2; // calendar; //For refreshing the Title to align center
//#if WP_80_SILVERLIGHT
//            PanoramaWorkspace.Title = "Calendar";
//#endif
//            EventManager evMan = new EventManager();
//            FolderManager mgr = new FolderManager();
//#if USE_ASYNC
//            List<workspace_datastore.models.Folder> folderListDB = await mgr.GetFolderInfoAsync(0);
//#else
//                    List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfo(0);
//#endif
//            var folderDB = folderListDB.Find(x => x.displayName == "Calendar");
//#if USE_ASYNC
//            List<Events> allEvent = evMan.GetEventInfoAsync(folderDB.id, 0).Result;
//#else
//                    List<Events> allEvent = evMan.GetEventInfo(folderDB.id, 0);
//#endif
//            //  DateTime test = startDateandTime(allEvent[0].startTime);
//            //get today event
//            DateTime today = DateTime.Now; //DateTime.Today.Subtract(new TimeSpan(3, 0, 0, 0));
//            List<Events> recurringEvents = RecurringEventsBuilder.FilterRecurringEventDefinitionsFromSource(allEvent);
//            List<Events> futureEvents = RecurringEventsBuilder.GenerateWindowForFutureDays(3, today.ToBinary(), allEvent, recurringEvents);
//            allEvent = allEvent.Concat(futureEvents).ToList();
//            var todayevnt = allEvent.FindAll(c => DateTime.FromBinary(c.startDayAndTime).Date == today.Date);

//            var tomevnt = allEvent.FindAll(c => DateTime.FromBinary(c.startDayAndTime).Date == today.Date.AddDays(1).Date);
//            var datomevnt = allEvent.FindAll(c => DateTime.FromBinary(c.startDayAndTime).Date == today.Date.AddDays(2).Date);
//            //  todayevnt.Sort(x=>startDateandTime(x.startTime).Date.Date. )

//            #region Events of Today
//            if (todayevnt.Count == 0)
//            {
//                firstAgendaTodayTitle.Text = "No meetings";
//                firstAgendaTodaySubTitle.Text = "";

//                timeStartDuration.Text = "";
//                timeEndDuration.Text = "";
//                moreMeetingToday.Text = "0";

//                dashText.Visibility = Visibility.Collapsed;
//                dashDuration.Visibility = Visibility.Collapsed;
//                moreToday.Visibility = Visibility.Collapsed;
//                noTodayMeet.Visibility = Visibility.Collapsed;
//                todayDate.Text = DateTime.Today.Day.ToString();
//            }
//            else
//            {
//                firstAgendaTodayTitle.Text = todayevnt[0].subject;
//                firstAgendaTodaySubTitle.Text = todayevnt[0].location;

//                if (todayevnt[0].allDay == 0)
//                {
//                    timeStartDuration.Text = DateTimeExtensions.FromOADate(todayevnt[0].startDayAndTime).ToShortTimeString();
//                    timeEndDuration.Text = DateTimeExtensions.FromOADate(todayevnt[0].endDayAndTime).ToShortTimeString();
//                }
//                else
//                {
//                    todaySch.Visibility = Visibility.Collapsed;
//                    allDayEvent.Visibility = Visibility.Visible;
//                }

//                if (todayevnt.Count == 1)
//                {
//                    dashText.Visibility = Visibility.Visible;
//                    dashDuration.Visibility = Visibility.Visible;
//                    moreToday.Visibility = Visibility.Collapsed;
//                    noTodayMeet.Visibility = Visibility.Collapsed;
//                }
//                else
//                {
//                    dashText.Visibility = Visibility.Visible;
//                    dashDuration.Visibility = Visibility.Visible;
//                    moreToday.Visibility = Visibility.Visible;
//                    noTodayMeet.Visibility = Visibility.Visible;
//                }

//                moreMeetingToday.Text = (todayevnt.Count - 1).ToString();
//                todayDate.Text = DateTimeExtensions.FromOADate(todayevnt[0].startDayAndTime).Day.ToString();
//            }
//            #endregion
//            #region Events of Tommorow
//            if (tomevnt.Count == 0)
//            {
//                firstAgendaTommorowTitle.Text = "No meetings";
//                firstAgendaTommorowSubTitle.Text = "";

//                timeStartDurationTom.Text = "";
//                moreMeetingTom.Text = "0";

//                tomDash.Visibility = Visibility.Collapsed;
//                tomNBorder.Visibility = Visibility.Collapsed;
//                tomMore.Visibility = Visibility.Collapsed;

//                tomDate.Text = DateTime.Today.AddDays(1).Day.ToString();
//            }
//            else
//            {
//                firstAgendaTommorowTitle.Text = tomevnt[0].subject;
//                firstAgendaTommorowSubTitle.Text = tomevnt[0].location;

//                if (tomevnt[0].allDay == 0)
//                {


//                    timeStartDurationTom.Text = DateTimeExtensions.FromOADate(tomevnt[0].startDayAndTime).ToShortTimeString();
//                }
//                else
//                {
//                    timeStartDurationTom.Visibility = Visibility.Collapsed;
//                    alldayEventtom.Visibility = Visibility.Visible;
//                }
//                if (tomevnt.Count == 1)
//                {
//                    tomDash.Visibility = Visibility.Visible;
//                    tomNBorder.Visibility = Visibility.Collapsed;
//                    tomMore.Visibility = Visibility.Collapsed;
//                }
//                else
//                {
//                    tomDash.Visibility = Visibility.Visible;
//                    tomNBorder.Visibility = Visibility.Visible;
//                    tomMore.Visibility = Visibility.Visible;
//                }


//                moreMeetingTom.Text = (tomevnt.Count - 1).ToString();
//                tomDate.Text = DateTimeExtensions.FromOADate(tomevnt[0].startDayAndTime).Day.ToString();
//            }

//            #endregion
//            #region Events of Day After Tommorow
//            if (datomevnt.Count == 0)
//            {
//                firstAgendaDayAfTommorowTitle.Text = "No meetings";
//                firstAgendaDayAfTommorowSubTitle.Text = "";

//                timeStartDurationTom1.Text = "";
//                // timeEndDuration.Text = startDateandTime(tomevnt[0].endTime).ToString("hh:mm tt");

//                dayAfTomDash.Visibility = Visibility.Collapsed;
//                dayAfTomNo.Visibility = Visibility.Collapsed;
//                dayAfTomMore.Visibility = Visibility.Collapsed;

//                moreMeetingDayAfTom.Text = "0";
//                dayAftomDate.Text = DateTime.Today.AddDays(2).Day.ToString();
//            }
//            else
//            {
//                firstAgendaDayAfTommorowTitle.Text = datomevnt[0].subject;
//                firstAgendaDayAfTommorowSubTitle.Text = datomevnt[0].location;
//                if (datomevnt[0].allDay == 0)
//                {
//                    timeStartDurationTom1.Text = DateTimeExtensions.FromOADate(datomevnt[0].startDayAndTime).ToShortTimeString();
//                    // timeEndDuration.Text = startDateandTime(tomevnt[0].endTime).ToString("hh:mm tt");
//                }
//                else
//                {
//                    timeStartDurationTom1.Visibility = Visibility.Collapsed;
//                    alldayEventtom1.Visibility = Visibility.Visible;
//                }

//                if (datomevnt.Count == 1)
//                {
//                    dayAfTomDash.Visibility = Visibility.Visible;
//                    dayAfTomNo.Visibility = Visibility.Collapsed;
//                    dayAfTomMore.Visibility = Visibility.Collapsed;
//                }
//                else
//                {
//                    dayAfTomDash.Visibility = Visibility.Visible;
//                    dayAfTomNo.Visibility = Visibility.Visible;
//                    dayAfTomMore.Visibility = Visibility.Visible;
//                }

//                moreMeetingDayAfTom.Text = (datomevnt.Count - 1).ToString();
//                dayAftomDate.Text = DateTimeExtensions.FromOADate(datomevnt[0].startDayAndTime).Day.ToString();
//            }
//            BottomAppBar = (AppBar)this.Resources["calendarPageAppBar"];
//            #endregion

//            #endregion







            #region renderCalender
            PanoramaWorkspace.DefaultSectionIndex = 2; //For refreshing the Title to align center
            PanoramaWorkspace.Header = "Calendar";
            EventManager evMan = new EventManager();
            FolderManager mgr = new FolderManager();
#if USE_ASYNC
            List<workspace_datastore.models.Folder> folderListDB = await mgr.GetFolderInfoAsync(0);
#else
                    List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfo(0);
#endif
            var folderDB = folderListDB.Find(x => x.displayName == "Calendar" && x.parentFolder_id == 0);
#if USE_ASYNC
            List<Events> allEvent = evMan.GetEventInfoForDaysAsync(DateTime.Today.ToUniversalTime().ToOADate(), 3, 3).Result;
            List<Events> pastRecoccurentallEvents = evMan.GetRecurrentEventInfoForDaysAsync(DateTime.Today.ToUniversalTime().ToOADate(), 2, 5).Result;
            List<Events> pastMultipleDayEvents = evMan.GetMultipleDayEventInfoAsync(DateTime.Today.ToUniversalTime().ToOADate(), 0, 5).Result;
            allEvent.AddRange(pastRecoccurentallEvents);
#else
                    List<Events> allEvent = evMan.GetEventInfo(folderDB.id, 0);
#endif
            //  DateTime test = startDateandTime(allEvent[0].startTime);
            //get today event
            DateTime today = DateTime.Now; //DateTime.Today.Subtract(new TimeSpan(3, 0, 0, 0));
            List<Events> recurringEvents = RecurringEventsBuilder.FilterRecurringEventDefinitionsFromSource(allEvent);
            List<Events> futureEvents = RecurringEventsBuilder.GenerateWindowForFutureDays(3, today.ToOADate(), allEvent, recurringEvents);
            allEvent = allEvent.Concat(futureEvents).ToList();
            foreach (Events eventO in allEvent)
            {
                //    double timefromUTCminutes = CalculateDisatnceFromUTC(eventO.timeZone);
                //    double biasForEventDate = currentTimeZoneMinuteFromUTC - timefromUTCminutes;
                eventO.startDayAndTime = DateTimeExtensions.FromOADate(eventO.startDayAndTime).AddMinutes(currentTimeZoneMinuteFromUTC).ToOADate();
                eventO.endDayAndTime = DateTimeExtensions.FromOADate(eventO.endDayAndTime).AddMinutes(currentTimeZoneMinuteFromUTC).ToOADate();
            }

            foreach (Events eventO in pastMultipleDayEvents)
            {
                //double timefromUTCminutes = CalculateDisatnceFromUTC(eventO.timeZone);
                //double biasForEventDate = currentTimeZoneMinuteFromUTC - timefromUTCminutes;
                eventO.startDayAndTime = DateTimeExtensions.FromOADate(eventO.startDayAndTime).AddMinutes(currentTimeZoneMinuteFromUTC).ToOADate();
                eventO.endDayAndTime = DateTimeExtensions.FromOADate(eventO.endDayAndTime).AddMinutes(currentTimeZoneMinuteFromUTC).ToOADate();
            }

            List<Events> multiDayEvents = CreateMultiDayEvent(pastMultipleDayEvents);
            allEvent.AddRange(multiDayEvents);

            var multiDayEventfromTodayOnward = allEvent.FindAll(x => (x.startDayAndTime >= DateTime.Today.ToOADate()) && (DateTimeExtensions.FromOADate(x.endDayAndTime).Date.ToOADate() > DateTimeExtensions.FromOADate(x.startDayAndTime).Date.ToOADate()) && (x.reoccurs == 0));
            List<Events> multiDayEventsNowAndFuture = CreateMultiDayEventFuture(multiDayEventfromTodayOnward);

            foreach (var item in multiDayEventfromTodayOnward)
            {
                allEvent.Remove(item);
            }

            allEvent.AddRange(multiDayEventsNowAndFuture);

            var todayevnt = allEvent.FindAll(c => DateTimeExtensions.FromOADate(c.startDayAndTime).Date == today.Date && c.selfAttendeeStatus != 5 && c.meetingStatus != 7).OrderBy(o => o.startDayAndTime).ToList();

            var tomevnt = allEvent.FindAll(c => DateTimeExtensions.FromOADate(c.startDayAndTime).Date == today.Date.AddDays(1).Date && c.selfAttendeeStatus != 5 && c.meetingStatus != 7).OrderBy(o => o.startDayAndTime).ToList();
            var datomevnt = allEvent.FindAll(c => DateTimeExtensions.FromOADate(c.startDayAndTime).Date == today.Date.AddDays(2).Date && c.selfAttendeeStatus != 5 && c.meetingStatus != 7).OrderBy(o => o.startDayAndTime).ToList();
            //  todayevnt.Sort(x=>startDateandTime(x.startTime).Date.Date. )


            //Getting all control od inside hub

            TextBlock firstAgendaTodayTitle = FindChildControl<TextBlock>(calendar, "firstAgendaTodayTitle") as TextBlock;
            TextBlock dashText = FindChildControl<TextBlock>(calendar, "dashText") as TextBlock;
            TextBlock moreToday = FindChildControl<TextBlock>(calendar, "moreToday") as TextBlock;
            TextBlock noTodayMeet = FindChildControl<TextBlock>(calendar, "noTodayMeet") as TextBlock;
            TextBlock moreMeetingToday = FindChildControl<TextBlock>(calendar, "moreMeetingToday") as TextBlock;
            TextBlock timeStartDuration = FindChildControl<TextBlock>(calendar, "timeStartDuration") as TextBlock;
            TextBlock timeEndDuration = FindChildControl<TextBlock>(calendar, "timeEndDuration") as TextBlock;
            TextBlock todayDate = FindChildControl<TextBlock>(calendar, "todayDate") as TextBlock;

            #region Events of Today
            if (todayevnt.Count == 0)
            {
                
                firstAgendaTodayTitle.Text = "No meetings";
                //firstAgendaTodaySubTitle.Text = "";
                TextBlock firstAgendaTodaySubTitle = FindChildControl<TextBlock>(calendar, "firstAgendaTodaySubTitle") as TextBlock;
                timeStartDuration.Text = "";
                timeEndDuration.Text = "";
                moreMeetingToday.Text = "0";
                // dashDuration.Visibility = System.Windows.Visibility.Collapsed;
                moreToday.Visibility = Visibility.Collapsed;
                noTodayMeet.Visibility = Visibility.Collapsed;
            }
            else
            {
                KeyValuePair<int, int> eventAndRemainingEvent = NearestTodayEvent(todayevnt);

                if (eventAndRemainingEvent.Key != -1)
                {
                    
                    firstAgendaTodayTitle.Text = todayevnt[eventAndRemainingEvent.Key].subject;

                    // firstAgendaTodaySubTitle.Text = todayevnt[eventAndRemainingEvent.Key].location;

                    if (todayevnt[eventAndRemainingEvent.Key].selfAttendeeStatus == 2)
                    {
                        firstAgendaTodayTitle.Text = firstAgendaTodayTitle.Text + " (tentative)";
                    }
                    if (todayevnt[eventAndRemainingEvent.Key].allDay == 0)
                    {
                        timeStartDuration.Text = DateTimeExtensions.FromOADate(todayevnt[eventAndRemainingEvent.Key].startDayAndTime).ToShortTimeString();
                        timeEndDuration.Text = DateTimeExtensions.FromOADate(todayevnt[eventAndRemainingEvent.Key].endDayAndTime).ToShortTimeString();
                    }
                    else
                    {
                        StackPanel todaySch = FindChildControl<StackPanel>(calendar, "todaySch") as StackPanel;
                        todaySch.Visibility = Visibility.Collapsed;
                        TextBlock allDayEvent = FindChildControl<TextBlock>(calendar, "allDayEvent") as TextBlock;
                        allDayEvent.Visibility = Visibility.Visible;
                    }

                   
                    if (todayevnt.Count == 1)
                    {
                        
                        dashText.Visibility = Visibility.Visible;
                        //  dashDuration.Visibility = System.Windows.Visibility.Visible;
                        moreToday.Visibility = Visibility.Collapsed;
                        noTodayMeet.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        dashText.Visibility = Visibility.Visible;
                        // dashDuration.Visibility = System.Windows.Visibility.Visible;
                        moreToday.Visibility = Visibility.Visible;
                        noTodayMeet.Visibility = Visibility.Visible;
                    }

                    moreMeetingToday.Text = (eventAndRemainingEvent.Value).ToString();
                    todayDate.Text = DateTimeExtensions.FromOADate(todayevnt[eventAndRemainingEvent.Key].startDayAndTime).Day.ToString();
                    if (eventAndRemainingEvent.Value == 0)
                    {
                        moreToday.Visibility = Visibility.Collapsed;
                        noTodayMeet.Visibility = Visibility.Collapsed;
                    }
                }
                else
                {
                    firstAgendaTodayTitle.Text = "No meetings";
                    //firstAgendaTodaySubTitle.Text = "";

                    timeStartDuration.Text = "";
                    timeEndDuration.Text = "";
                    moreMeetingToday.Text = "0";

                    dashText.Visibility = Visibility.Collapsed;
                    // dashDuration.Visibility = System.Windows.Visibility.Collapsed;
                    moreToday.Visibility = Visibility.Collapsed;
                    noTodayMeet.Visibility = Visibility.Collapsed;
                    todayDate.Text = DateTime.Today.Day.ToString();
                }
            }
            #endregion
            #region Events of Tommorow

            TextBlock firstAgendaTommorowTitle = FindChildControl<TextBlock>(calendar, "firstAgendaTommorowTitle") as TextBlock;
            TextBlock moreMeetingTom = FindChildControl<TextBlock>(calendar, "moreMeetingTom") as TextBlock;
            TextBlock timeStartDurationTom = FindChildControl<TextBlock>(calendar, "timeStartDurationTom") as TextBlock;
            TextBlock tomDate = FindChildControl<TextBlock>(calendar, "tomDate") as TextBlock;
            Border tomMore = FindChildControl<Border>(calendar, "tomMore") as Border;
            Border tomNBorder = FindChildControl<Border>(calendar, "tomNBorder") as Border;
            TextBlock alldayEventtom = FindChildControl<TextBlock>(calendar, "alldayEventtom") as TextBlock;

            if (tomevnt.Count == 0)
            {
                firstAgendaTommorowTitle.Text = "No meetings";
                // firstAgendaTommorowSubTitle.Text = "";

                timeStartDurationTom.Text = "";
                moreMeetingTom.Text = "0";

                // tomDash.Visibility = System.Windows.Visibility.Collapsed;
                tomNBorder.Visibility = Visibility.Collapsed;
                tomMore.Visibility = Visibility.Collapsed;

                tomDate.Text = DateTime.Today.AddDays(1).Day.ToString();
            }
            else
            {
                firstAgendaTommorowTitle.Text = tomevnt[0].subject;
                //firstAgendaTommorowSubTitle.Text = tomevnt[0].location;

                if (tomevnt[0].allDay == 0)
                {
                    timeStartDurationTom.Text = DateTimeExtensions.FromOADate(tomevnt[0].startDayAndTime).ToShortTimeString();
                }
                else
                {
                    timeStartDurationTom.Visibility = Visibility.Collapsed;
                    alldayEventtom.Visibility = Visibility.Visible;
                }
                if (tomevnt.Count == 1)
                {
                    // tomDash.Visibility = System.Windows.Visibility.Visible;
                    tomNBorder.Visibility = Visibility.Collapsed;
                    tomMore.Visibility = Visibility.Collapsed;
                }
                else
                {
                    //  tomDash.Visibility = System.Windows.Visibility.Visible;
                    tomNBorder.Visibility = Visibility.Visible;
                    tomMore.Visibility = Visibility.Visible;
                }


                moreMeetingTom.Text = (tomevnt.Count - 1).ToString();
                tomDate.Text = DateTimeExtensions.FromOADate(tomevnt[0].startDayAndTime).Day.ToString();
            }

            #endregion
            #region Events of Day After Tommorow

            TextBlock firstAgendaDayAfTommorowTitle = FindChildControl<TextBlock>(calendar, "firstAgendaDayAfTommorowTitle") as TextBlock;
            TextBlock alldayEventtom1 = FindChildControl<TextBlock>(calendar, "alldayEventtom1") as TextBlock;
            TextBlock timeStartDurationTom1 = FindChildControl<TextBlock>(calendar, "timeStartDurationTom1") as TextBlock;
            TextBlock dayAftomDate = FindChildControl<TextBlock>(calendar, "dayAftomDate") as TextBlock;
            Border dayAfTomNo = FindChildControl<Border>(calendar, "dayAfTomNo") as Border;
            Border dayAfTomMore = FindChildControl<Border>(calendar, "dayAfTomMore") as Border;
            TextBlock moreMeetingDayAfTom = FindChildControl<TextBlock>(calendar, "moreMeetingDayAfTom") as TextBlock;

            if (datomevnt.Count == 0)
            {
                firstAgendaDayAfTommorowTitle.Text = "No meetings";
                //firstAgendaDayAfTommorowSubTitle.Text = "";

                timeStartDurationTom1.Text = "";
                // timeEndDuration.Text = startDateandTime(tomevnt[0].endTime).ToString("hh:mm tt");

                // dayAfTomDash.Visibility = System.Windows.Visibility.Collapsed;
                dayAfTomNo.Visibility = Visibility.Collapsed;
                dayAfTomMore.Visibility = Visibility.Collapsed;

                moreMeetingDayAfTom.Text = "0";
                dayAftomDate.Text = DateTime.Today.AddDays(2).Day.ToString();
            }
            else
            {
                firstAgendaDayAfTommorowTitle.Text = datomevnt[0].subject;
                // firstAgendaDayAfTommorowSubTitle.Text = datomevnt[0].location;
                if (datomevnt[0].allDay == 0)
                {
                    timeStartDurationTom1.Text = DateTimeExtensions.FromOADate(datomevnt[0].startDayAndTime).ToShortTimeString();
                    // timeEndDuration.Text = startDateandTime(tomevnt[0].endTime).ToString("hh:mm tt");
                }
                else
                {
                    timeStartDurationTom1.Visibility = Visibility.Collapsed;
                    alldayEventtom1.Visibility = Visibility.Visible;
                }

                if (datomevnt.Count == 1)
                {
                    //dayAfTomDash.Visibility = System.Windows.Visibility.Visible;
                    dayAfTomNo.Visibility = Visibility.Collapsed;
                    dayAfTomMore.Visibility = Visibility.Collapsed;
                }
                else
                {
                    // dayAfTomDash.Visibility = System.Windows.Visibility.Visible;
                    dayAfTomNo.Visibility = Visibility.Visible;
                    dayAfTomMore.Visibility = Visibility.Visible;
                }

                moreMeetingDayAfTom.Text = (datomevnt.Count - 1).ToString();
                dayAftomDate.Text = DateTimeExtensions.FromOADate(datomevnt[0].startDayAndTime).Day.ToString();
            }

            //ApplicationBar = (ApplicationBar)this.Resources["calendarPageAppBar"];
            BottomAppBar = calendarPageAppBar;

            #endregion

            #endregion
        }

        private List<Events> CreateMultiDayEvent(List<Events> pastMultipleDayEvents)
        {
            List<Events> multiDayEvents = new List<Events>();

            try
            {
                foreach (var eventM in pastMultipleDayEvents)
                {


                    if (eventM.startDayAndTime < DateTime.Today.ToOADate())
                    {
                        if (eventM.endDayAndTime > DateTime.Today.AddDays(1).ToOADate())
                        {
                            Events multiDaySingleEvent = new Events();
                            multiDaySingleEvent.CopyToThis(eventM);
                            multiDaySingleEvent.startDayAndTime = DateTime.Today.ToOADate();
                            multiDaySingleEvent.endDayAndTime = DateTime.Today.AddHours(24).ToOADate();
                            multiDayEvents.Add(multiDaySingleEvent);
                        }
                        if (eventM.endDayAndTime > DateTime.Today.AddDays(2).ToOADate())
                        {
                            Events multiDaySingleEvent = new Events();
                            multiDaySingleEvent.CopyToThis(eventM);
                            multiDaySingleEvent.startDayAndTime = DateTime.Today.AddDays(1).ToOADate();
                            multiDaySingleEvent.endDayAndTime = DateTime.Today.AddHours(24).ToOADate();
                            multiDayEvents.Add(multiDaySingleEvent);
                        }
                        if (eventM.endDayAndTime > DateTime.Today.AddDays(3).ToOADate())
                        {
                            Events multiDaySingleEvent = new Events();
                            multiDaySingleEvent.CopyToThis(eventM);
                            multiDaySingleEvent.startDayAndTime = DateTime.Today.AddDays(2).ToOADate();
                            multiDaySingleEvent.endDayAndTime = DateTime.Today.AddHours(24).ToOADate();
                            multiDayEvents.Add(multiDaySingleEvent);
                        }
                        if (DateTimeExtensions.FromOADate(eventM.endDayAndTime).Date == DateTime.Today.Date)
                        {
                            Events multiDaySingleEvent = new Events();
                            multiDaySingleEvent.CopyToThis(eventM);
                            multiDaySingleEvent.startDayAndTime = DateTime.Today.ToOADate();
                            multiDaySingleEvent.endDayAndTime = eventM.endDayAndTime;
                            multiDayEvents.Add(multiDaySingleEvent);
                        }
                        if (DateTimeExtensions.FromOADate(eventM.endDayAndTime).Date == DateTime.Today.AddDays(1).Date)
                        {
                            Events multiDaySingleEvent = new Events();
                            multiDaySingleEvent.CopyToThis(eventM);
                            multiDaySingleEvent.startDayAndTime = DateTime.Today.AddDays(1).ToOADate();
                            multiDaySingleEvent.endDayAndTime = eventM.endDayAndTime;
                            multiDayEvents.Add(multiDaySingleEvent);
                        }
                        if (DateTimeExtensions.FromOADate(eventM.endDayAndTime).Date == DateTime.Today.AddDays(2).Date)
                        {
                            Events multiDaySingleEvent = new Events();
                            multiDaySingleEvent.CopyToThis(eventM);
                            multiDaySingleEvent.startDayAndTime = DateTime.Today.AddDays(2).ToOADate();
                            multiDaySingleEvent.endDayAndTime = eventM.endDayAndTime;
                            multiDayEvents.Add(multiDaySingleEvent);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return multiDayEvents;
        }

        /// <summary>
        /// Handles the Tap event of the more evnts of today events tile.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.GestureEventArgs"/> instance containing the event data.</param>
		private void todayAgenda_Tap(object sender, TappedRoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceCalendarWP8/View/DayView.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("DayView");
            //IsolatedStorageSettings.ApplicationSettings["dayTile"] = 0;           //set status to navigate to appropriate day
            ApplicationDataContainer applicationSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            applicationSettings.Values["dayTile"] = 0;
        }

        /// <summary>
        ///Handles the Tap event of the more evnts of tomorrow events tile.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.GestureEventArgs"/> instance containing the event data.</param>
		private void tommorowAgenda_Tap(object sender, TappedRoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceCalendarWP8/View/DayView.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("ContactsDetailsView");
            //IsolatedStorageSettings.ApplicationSettings["DayView"] = 1;
            ApplicationDataContainer applicationSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            applicationSettings.Values["DayView"] = 1;
        }

        /// <summary>
        /// Handles the Tap event of the more evnts of day after tommorow events tile.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.GestureEventArgs"/> instance containing the event data.</param>
		private void dayAftommorowAgenda_Tap(object sender, TappedRoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceCalendarWP8/View/DayView.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("DayView");
            //IsolatedStorageSettings.ApplicationSettings["dayTile"] = 2;
            ApplicationDataContainer applicationSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            applicationSettings.Values["dayTile"] = 2;
        }

		

        void ProcessHaloUpdates()
        {
            string itemsChanged = "";
            string pinDigits = "";

            Dictionary<string, string> savedSettings = App.mManager.settingsUserOptions.Settings;

            // find out what's changed
            foreach (string key in mPendingSettings.Keys)
            {
                var pendingValue = mPendingSettings[key];
                var savedValue = savedSettings[key];

                if (pendingValue != savedValue)
                {
                    itemsChanged += key + " ";
                    //savedSettings[key] = pendingValue; Saved settings should not be updated.
                    if (key == HaloPolicySettingsAbstract.KEY_PIN_LENGTH)
                    {
                        pinDigits = pendingValue;
                    }
                    else
                    {
                        savedSettings[key] = pendingValue;
                    
                    }
                }
            }

            if (itemsChanged.Length > 0)
            {
                string message = string.Format("CHANGES: {0}", itemsChanged);
                //System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    #if WP_80_SILVERLIGHT
                    CustomMessageBox messageBox = new CustomMessageBox()
                    {
                        Caption = "CCM Server Change",
                        Message = message,
                        LeftButtonContent = "OK",

                    };
                    messageBox.Dismissed += (s1, e1) =>
                    {
                        switch (e1.Result)
                        {
                            case CustomMessageBoxResult.LeftButton:
                                if (pinDigits.Length > 0)
                                {
                                    //NavigationService.Navigate(new Uri(string.Format("/WorkspaceWP81;component/ChangePin.xaml?PINLength={0}", pinDigits), UriKind.Relative));
                                    Common.Navigation.NavigationService.NavigateToPage("ChangePin",pinDigits);
                                }
                                //else//in any caase we want to save the setting so no else required.
                                //{
                                    // Save the items
                                    App.mManager.storeUserSettings();
                                    // TODO We need to do process other changes as well.  But the PIN length change is the big one!
                                //}
                                break;
                            case CustomMessageBoxResult.RightButton:
                                break;
                            case CustomMessageBoxResult.None:
                                break;
                            default:
                                break;
                        }
                    };
                    messageBox.Show();
#endif
                });
            }
        }

        void halo_profileUpdateHandler(object sender, ProfileUpdateEventArgs e)
        {
            if (e.FullConfiguration)
            {
                // Just save the whole thing
                e.DKCNAppManager.storeUserSettings();

                // CRICKET: Will we ever get an update with full configuration after?
            }
            else
            {
                mPendingSettings = e.DKCNAppManager.settingsPendingOptions.Settings;

                if (mPanoramaPageActive)
                {
                    ProcessHaloUpdates();
                    mPendingSettings.Clear();
                    mPendingSettings = null;
                }
            }
        }


        void mActiveSync_EASTriggerSendPingEvent(object sender, EASTriggerSendPingEventArgs e)
        {
            // We don't want to send another Ping event if we are currently syncing
            #if TO_FIX_FOR_TABLET
            if (!mSyncBGWorker.SyncingThreadBusy)
            {
                (sender as ActiveSync).EASPingRequested = false;
                (sender as ActiveSync).SendPing(ActiveSyncManager.ASPingHeartBeatInterval, e.Folders);
            }
            else
            {
                // Clear the flag for when we are done syncing we can set up the pinger again
                mSetupPinger = false;
            }
            #endif
        }

        async void mActiveSync_EASHandlePingResultEvent(object sender, EASHandlePingResultEventArgs e)
        {
            #if TO_FIX_FOR_TABLET
            (sender as ActiveSync).EASPingRequested = false;
            if (e.CommandResponse != null && !mSyncBGWorker.SyncingThreadBusy)
            {
                LoggingWP8.WP8Logger.LogMessage("Setting up folders in response to PING");
                //get all folders from response xml
                List<workspace_datastore.models.Folder> dbFolders = ParsingUtils.GetFoldersForPingResult(e.CommandResponse.XmlString);
                if(dbFolders.Count > 0)
                {
                    mSyncBGWorker.InitializeAndDoSync(dbFolders);
                }
            }
            else
            {
                LoggingWP8.WP8Logger.LogMessage("Ping event: No processing because syncing");
                // Clear the flag for when we are done syncing we can set up the pinger again
                mSetupPinger = false;
            }
#endif
        }

        void mActiveSync_EASHandleResyncEvent(object sender, EASResyncFolderEventArgs e)
        {
            #if TO_FIX_FOR_TABLET
            // Here we need to show toast, delete Message items with the same folder id, and delete attachments, then do a FULL resync
            ToastNotification toast = new ToastNotification();
            string message = string.Format("Folder {0} is out of sync.  Commencing resync", e.Folder.Name);
            toast.CreateToastNotificationForVertical("Error", message);
#endif

            MessageManager mm = new MessageManager();
#if USE_ASYNC
            mm.DeleteAllMessagesForFolderAsync(Convert.ToInt32(e.Folder.Id));
#else
            mm.DeleteMessageInfo(Convert.ToInt32(folder.Id));
#endif
            // CRICKET: What if we get a Ping when all this is happening?
            e.Folder.SyncKey = "0";
            e.Folder.LastSyncTime = DateTime.MinValue;
            mActiveSync.SyncMailBasedOnFolder(e.Folder);
        }

        /// <summary>
        /// Method gets called right before sending an email so that we can populate the Outbox table in case of network failure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="clientId"></param>
        /// <param name="content"></param>
        /// <param name="actionType"></param>
        void mActiveSync_ConstructOutgoingMessage(object sender, ConstructOutgoingMessageEventArgs e)
        {
            #if TO_FIX_FOR_TABLET
            OutboxManager om = new OutboxManager();
            Outbox msg = new Outbox();
            FolderManager fm = new FolderManager();
            int folderIdOfOutbox = fm.GetFolderIdentifierBasedOnFolderName("Outbox");

            // Make sure we don't already have a pending message in the outbox (No duplicates!)
            if (e.FolderId != folderIdOfOutbox)
            {
                msg.folderId = folderIdOfOutbox;
                msg.account_id = App.mActiveSyncManager.Account.AccountId;
                msg.actionType = e.ActionType;
                msg.clientIdentifier = e.ClientId;
                msg.message_id = 0; // This is the Auto-Increment, and Primary Key
                msg.bodyText = e.Content.ToString();

                if (null != e.Attachments && e.ActionType == (int)MailActionType.ReferenceNone)
                {
                    List<AttachmentDetail> attachments = e.Attachments.Cast<AttachmentDetail>().ToList();
                    if (null != attachments)
                    {
                        StringBuilder attachmentsList = new StringBuilder();
                        List<Attachment> dbAttachments = new List<Attachment>();
                        foreach (AttachmentDetail attachment in attachments)
                        {
                            attachmentsList.AppendLine(attachment.filePath);
                        }
                        // Now set the outbox message string of attachment Path(s)
                        msg.attachmentFilePathsList = attachmentsList.ToString();
                    }
                }

                msg.creationTime = DateTime.Now.ToOADate();
                om.AddOutboxDataAsync(msg);
            }
#endif
        }

        /// <summary>
        /// Method gets called when email successfully delivered
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="clientId"></param>
        async void mActiveSync_DeleteOutgoingMessage(object sender, DeleteOutgoingMessageEventArgs e)
        {
            // Delete email from outbox
            OutboxManager om = new OutboxManager();
            om.DeleteOutboxDataAsync(e.ClientId);
        }

        async void App_NetworkStatusChanged(object sender, NetworkChangedEventArgs e)
        {
#if TO_FIX_FOR_TABLET
            if (e.IsOnline)
            {
                // Send the outbox items if any
                OutboxManager om = new OutboxManager();
                List<Outbox> msgs = await om.GetOutboxDataAsync("");
                if (msgs != null && msgs.Count > 0)
                {
                    FolderManager fm = new FolderManager();
                    int folderId = fm.GetFolderIdentifierBasedOnFolderName("Outbox");
                    List<LocalItem> mmList = new List<LocalItem>();
                    foreach (Outbox outMsg in msgs)
                    {
                        workspace_datastore.models.Message msg = ActiveSync.DecodeMail(new StringBuilder(outMsg.bodyText));
                        msg.folder_id = folderId;
                        msg.receivedDate = outMsg.creationTime;
                        msg.id = outMsg.message_id;

                        List<AttachmentDetail> attachmentsToSend = new List<AttachmentDetail>();
                        if (outMsg.attachmentFilePathsList != null && outMsg.attachmentFilePathsList.Length > 0)
                        {
                            string[] attachmentPaths = outMsg.attachmentFilePathsList.Split('\n', '\r');
                            foreach (string attPath in attachmentPaths)
                            {
                                if (attPath.Length > 0)
                                {
                                    FileManager fileMngr = new FileManager();
                                    FileConstantData.CurrentPath = attPath; // Needed?
                                    string filename = Path.GetFileName(attPath);
                                    byte[] byteArrayAttachmentData = await fileMngr.ReadFileFullPath(attPath, Encoding.UTF8.GetString(ConstantData.DBKey, 0, 32));
                                    string contentType = MimeType.GetMimeType(filename);
                                    Uri imageUri = MimeType.GetImageUri(filename);
                                    AttachmentDetail attchmentDetail = new AttachmentDetail() { filePath = attPath, data = byteArrayAttachmentData, name = filename, contentEncoding = "base64", contentType = contentType, imageUri = imageUri, charset = "iso-8859-1" };
                                    attachmentsToSend.Add(attchmentDetail);
                                }
                            }
                        }

                        Guid clientid = await ActiveSync.GetInstance().SendMail(msg, attachmentsToSend);
                        if (clientid != Guid.Empty)
                        {
                            // Delete from the outbox
                            Outbox outMsgToDelete = (from omsg in msgs where omsg.message_id == outMsg.message_id select omsg).FirstOrDefault();
                            if (null != msg)
                            {
                                om.DeleteOutboxDataAsync(outMsgToDelete.clientIdentifier);
                            }
                        }
                    }
                }
            }
#endif
        }

        //first page tap function
		private void mailGrid_Tap(object sender, TappedRoutedEventArgs e)
        {
            
            //PanoramaWorkspace.DefaultItem = Mail;
            //PanoramaWorkspace.Title = "Mail";
            PanoramaWorkspace.ScrollToSection(Mail);
		    PanoramaWorkspace.Header = "Mail";

        }

        private void calendarGrid_Tap(object sender, TappedRoutedEventArgs e)
        {
            //PanoramaWorkspace.DefaultItem = calendar;
            //PanoramaWorkspace.Title = "Calendar";

            PanoramaWorkspace.ScrollToSection(calendar);
            PanoramaWorkspace.Header = "Calendar";
        }

        private void filesGrid_Tap(object sender, TappedRoutedEventArgs e)
        {
            //PanoramaWorkspace.DefaultItem = files;
            //PanoramaWorkspace.Title = "Files";

            PanoramaWorkspace.ScrollToSection(files);
            PanoramaWorkspace.Header = "Files";
        }

        private void browserGrid_Tap(object sender, TappedRoutedEventArgs e)
        {
            //PanoramaWorkspace.DefaultItem = browser;
            //PanoramaWorkspace.Title = "Browser";

            PanoramaWorkspace.ScrollToSection(browser);
            PanoramaWorkspace.Header = "Browser";
        }

        private void contactsGrid_Tap(object sender, TappedRoutedEventArgs e)
        {
            //PanoramaWorkspace.DefaultItem = contacts;
            //PanoramaWorkspace.Title = "Contacts";

            PanoramaWorkspace.ScrollToSection(contacts);
            PanoramaWorkspace.Header = "Contacts";
        }

        //PK: Never used function
        //AppBar UpdateApplicationBarForPanoramaItem(HubSection newItem, HubSection oldItem)
        //{
        //    IApplicationBar currentAppBar = null;

        //    PanoramaPageCallbacks callbacks;
        //    if (oldItem != null)
        //    {
        //        if (_callbacks.TryGetValue(oldItem, out callbacks))
        //        {
        //            callbacks.CurrentApplicationBar = BottomAppBar;
        //        }
        //    }
        //    if (newItem != null)
        //    {
        //        if (_callbacks.TryGetValue(newItem, out callbacks))
        //        {
        //            currentAppBar = callbacks.CurrentApplicationBar;
        //        }
        //    }
        //    return currentAppBar;
        //}

        private void Panorama_SelectionChanged(object sender, Windows.UI.Xaml.Controls.SectionsInViewChangedEventArgs e)
        {
            #if WP_80_SILVERLIGHT
            _callbacks[((Panorama)sender).SelectedItem].OnPanoramaItemSelected();
            #endif
            
            var section = ((Hub)sender).SectionsInView[0];
            var tag = section.Tag.ToString();
            var selectedIndex = System.Convert.ToInt32(tag);
            _callbacks[selectedIndex].OnPanoramaItemSelected();

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceFileBrowserWP8/ViewFolders.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("ViewFolders");

        }
        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        private async Task ShowFiles()
        {
            FileManager objFlmngr = new FileManager();
            RecentFileManager objRecentFileManager = new RecentFileManager();

#if WP_80_SILVERLIGHT //for class FileBrowserFileItem
            List<FileBrowserFileItem> fileitemlist = new List<FileBrowserFileItem>();
            long fileSize = 0;
            int mfileCount = 0;

            var recentFiles = await objRecentFileManager.GetRecentFiles();

            foreach (var recentFile in recentFiles)
            {
                var arr = await objFlmngr.ReadFileFullPath(recentFile.path + "\\" + recentFile.name, Encoding.UTF8.GetString(ConstantData.DBKey, 0, 32));
                if (arr == null)
                {
                    // DELETE RECORD FROM RECENT FILE TABLE
                    objRecentFileManager.DeleteRecentFile(recentFile.name, recentFile.path);
                    continue;
                }

                using (var ms = new System.IO.MemoryStream(arr))
                {
                    fileSize = ms.Length;
                }
                fileitemlist.Add(new FileBrowserFileItem(null, recentFile.name, recentFile.path, FormatedSize(fileSize), false));

                mfileCount++;
            }


            if (mfileCount > 0)
            {
                //Pj.Why constraint to 6?
                ListView fileList = FindChildControl<TextBlock>(files, "fileList") as ListView;
                fileList.ItemsSource = fileitemlist.Take(6).ToList();

                StackPanel NoRecentFile = FindChildControl<TextBlock>(files, "NoRecentFile") as StackPanel;
                NoRecentFile.Visibility = Visibility.Collapsed;

                StackPanel recentFileList = FindChildControl<TextBlock>(files, "recentFileList") as StackPanel;
                recentFileList.Visibility = Visibility.Visible;


                Button btnViewFolders = FindChildControl<TextBlock>(files, "btnViewFolders") as Button;
                btnViewFolders.Margin = new Thickness(16, 310, 0, 0);
            }
            else
            {
                StackPanel NoRecentFile = FindChildControl<TextBlock>(files, "NoRecentFile") as StackPanel;
                NoRecentFile.Visibility = Visibility.Visible;

                StackPanel recentFileList = FindChildControl<TextBlock>(files, "recentFileList") as StackPanel;
                recentFileList.Visibility = Visibility.Collapsed;
            }
#endif
#if EXAMPLE
            FileManager fileMngr = new FileManager();

            List<string> files = new List<string>();
            List<Files> fl = new List<Files>();

            btnViewFolders.Visibility = Visibility.Visible;
            byte[] byteArrayData = GetBytes("this is test");
            var fles = await fileMngr.CreateFile("FileName.pdf", "", byteArrayData, "DellTempKey12345");
            fl.Add(new Files() { filename = "FileName.pdf", filepath = "", filesize = 2 });
            fl.Add(new Files() { filename = "FileName.pdf", filepath = "", filesize = 2 });
            fl.Add(new Files() { filename = "FileName.pdf", filepath = "", filesize = 2 });
            fl.Add(new Files() { filename = "FileName.pdf", filepath = "", filesize = 2 });
            fl.Add(new Files() { filename = "FileName.pdf", filepath = "", filesize = 2 });
            fl.Add(new Files() { filename = "FileName.pdf", filepath = "", filesize = 2 });

            var fldrs = await fileMngr.GetAllFiles(FileConstantData.RootPath);

            if (fldrs != null)
            {
                foreach (IFile fld in fldrs)
                {
                    files.Add(fld.Name);
                }
            }


            if (fl != null && fl.Count > 0)
            {
                fileList.ItemsSource = fl;
                NoRecentFile.Visibility = Visibility.Collapsed;
                recentFileList.Visibility = Visibility.Visible;
                btnViewFolders.Margin = new Thickness(16, 310, 0, 0);
            }
            else
            {
                NoRecentFile.Visibility = Visibility.Visible;
                recentFileList.Visibility = Visibility.Collapsed;
            }
            // var folders =  Windows.Storage.KnownFolders.DocumentsLibrary;
#endif
        }



        /// <summary>
        /// Click event for menu items of Menu Page, sender will contain the information for clicked item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void CreateNewEventItem_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceCalendarWP8/View/NewEvent.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("NewEvent");
        }

        private void ContactsPageMenuItemAdd_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceContactsWP8/View/AddContactOrEditView.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("AddContactOrEditView");
        }

        private void ContactsPageMenuItemSearch_Click(object sender, RoutedEventArgs e)
        {
            SuspensionManager.SessionState["IsSearchOpenFromContactPivot"] = "true";
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceContactsWP8/ContactLists.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("ContactLists");
        }

        private void FileManagerPageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceFileBrowserWP8/AddFolder.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("AddFolder");

        }

        private void CreateNew_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceEmailWP8/View/CreateNewView.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("CreateNewView");

        }

        //PK: We dont have Back Button in tablet
        //private void HardwareButton_BackKeyPress(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        //{
        //    //PanoramaPageCallbacks callbacks;
        //    //Boolean bSelectionEnabled = false;
        //    //if (_callbacks.TryGetValue(PanoramaWorkspace.SelectedItem, out callbacks) && (callbacks.OnBackKeyPress != null))//not required as there are no senarios for different callbacks with the current requirements.
        //    //{
        //    //callbacks.OnBackKeyPress(e);
        //    #if WP_80_SILVERLIGHT
        //    if (PanoramaWorkspace.SelectedItem == Mail && emailScreen.mailList.IsSelectionEnabled)
        //    {
        //        SetEmailMultiSelectionCaps(false);
        //        //bSelectionEnabled = true;
        //        e.Cancel = true;
        //    }
        //    //}
        //    else //if (!bSelectionEnabled)
        //    {
        //        CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
        //        {
        //            //MessageDialog exitMessage = new MessageDialog();
        //            //m.
        //            CustomMessageBox messageBox = new CustomMessageBox()
        //            {
        //                Caption = "Exit WorkspaceWP81",
        //                Message = "Are you sure you want to exit the application?",
        //                LeftButtonContent = "Cancel",
        //                RightButtonContent = "OK"

        //            };
        //            messageBox.Dismissing += (s1, e1) =>
        //            {
        //                switch (e1.Result)
        //                {
        //                    case CustomMessageBoxResult.LeftButton://dont exit app
        //                        //so do nothing
        //                        break;
        //                    case CustomMessageBoxResult.RightButton://
        //                        Application.Current.Terminate();
        //                        break;
        //                    case CustomMessageBoxResult.None:
        //                        break;
        //                    default:
        //                        break;
        //                }
        //            };
        //            messageBox.Show();
        //        });
        //        e.Cancel = true;
        //    }
        //#endif
            
        //}

        //protected override void OnRemovedFromJournal(JournalEntryRemovedEventArgs e)
        //{
        //    base.OnRemovedFromJournal(e);
        //}

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            
            mPanoramaPageActive = false;
            base.OnNavigatedFrom(e);
        }

        // ReSharper disable once RedundantOverridenMember
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            OnInializePanorama();

        }

//        protected override async void OnNavigatedTo(NavigationEventArgs e)
//        {
            
//            mPanoramaPageActive = true;

//            base.OnNavigatedTo(e);
//            //Panorama always needs to be the home screen, hence removing all other screens on the stack.
            
//            #if WP_80_SILVERLIGHT
//            while (NavigationService.BackStack.FirstOrDefault() != null)
//            {
//                NavigationService.RemoveBackEntry();
//            }
//            #endif

//            var section = PanoramaWorkspace.SectionsInView[0];
//            var tag = section.Tag.ToString();
//            var selectedIndex = System.Convert.ToInt32(tag);
//            #if WP_80_SILVERLIGHT
//            if (PanoramaWorkspace.SelectedItem != null && _callbacks[PanoramaWorkspace.SelectedItem].OnNavigatedToSelected != null)
//                _callbacks[PanoramaWorkspace.SelectedItem].OnNavigatedToSelected(e);
//            #endif

//            if (_callbacks[selectedIndex].OnNavigatedToSelected != null)
//                _callbacks[selectedIndex].OnNavigatedToSelected(e);
//            else
//            {
//                if (e.NavigationMode == NavigationMode.New)
//                {
//                    if (SuspensionManager.SessionState.ContainsKey("PanoramaPageOpeningFromWelcomePage"))
//                    {
//                        SuspensionManager.SessionState.Remove("PanoramaPageOpeningFromWelcomePage");
//                        //PK: initial Sync for one time only. It will be done only once when this screen is 
//                        //is opened from Welcome page
//                        PanoramaWorkspace.Header = "Menu";
//                        BottomAppBar = (AppBar)this.Resources["menuPageBar"];
                        
//                        #if WP_80_SILVERLIGHT
//                        PanoramaWorkspace.DefaultItem = PanoramaWorkspace.Items[0];
//                        #endif

//                        PanoramaWorkspace.DefaultSectionIndex = 0;
//                        //PK:18Feb-Single Thread change
//                        #if TO_FIX_FOR_TABLET
//                        mSyncBGWorker.SetScreenObject(this);
//                        mSyncBGWorker.InitializeAndDoSync("INITIALSYNC");
//                        #endif
//                    }
//                    else
//                    {
//                        FirstTimeNavigateToPanoramaTask(false);
//                    }
//                }
//            }



//            //pj. Not sure of this senario?
//            //This will do sync, when app is opened second time and having folders to sync from settings
//            #region done at start of application first time.Moving this code to the OnNavigateToEmail of email item.
//            // if (e.NavigationMode == NavigationMode.New)
//            //{ 


//            //                Stopwatch stopWatch = new Stopwatch();
//            //                stopWatch.Start();

//            //                //PJ. Not sure of this condition and the reson of this being here.....????????
//            //                if (SuspensionManager.SessionState.ContainsKey("EventResponseFromEmailScreenRSVPToast"))
//            //                {
//            //                    string msgforToast = (string)SuspensionManager.SessionState["EventResponseFromEmailScreenRSVPToast"];
//            //                    SuspensionManager.SessionState.Remove("EventResponseFromEmailScreenRSVPToast");
//            //                    ToastNotification toastRSVP = new ToastNotification();
//            //                    string messageRSVP = "";
//            //                    //toastRSVP.CreateToastNotificationForVertical(messageRSVP, "");//pj: empty toast???
//            //                    if (msgforToast.Length <= 20)
//            //                        messageRSVP = msgforToast;
//            //                    else
//            //                        messageRSVP = msgforToast.Substring(0, 15) + " " + "...";
//            //                    // toast.SubTitle = "Syncing in progress for all selected folders";
//            //                    toastRSVP.CreateToastNotificationForVertical(messageRSVP, "");
//            //                }
//            //                else
//            //                {
//            //                    ToastWithIndeterminateProgressBar toast = new ToastWithIndeterminateProgressBar();
//            //                    toast.Title = "Please wait";
//            //                    toast.SubTitle = "Syncing in progress for all selected folders";
//            //                    toast.Show();
//            //                }

//            //                List<ActiveSyncPCL.Folder> easFolders = new List<ActiveSyncPCL.Folder>();

//            //                PanoramaWorkspace.DefaultItem = Mail;
//            //                ApplicationBar = (ApplicationBar)this.Resources["mailPageAppBar"];

//            //                string selectedFolders;
//            //                IsolatedStorageSettings mailFolderSyncSettings = IsolatedStorageSettings.ApplicationSettings;
//            //                mailFolderSyncSettings.TryGetValue<string>("MAIL_FOLDER_SYNC_SETTING", out selectedFolders);

//            //                if (selectedFolders != null && selectedFolders != string.Empty)
//            //                {
//            //                   // FolderManager folderManager = new FolderManager();
//            //#if USE_ASYNC
//            //                   // var result = await folderManager.GetFolderInfoAsync(0);
//            //#else
//            //                    var result = fldrManager.GetFolderInfo(0);
//            //#endif
//            //                    string[] spilitfolderIDs = selectedFolders.Split(';');

//            //                    foreach (string strfldr in spilitfolderIDs)
//            //                    {
//            //                        if (!String.IsNullOrEmpty(strfldr))
//            //                        {
//            //                            #region PJ: Need to check why is this check required. If needed need to do in the activesync.cs
//            //                            //workspace_datastore.models.Folder dbFolder = result.Where(c => c.displayName.ToLower() == strfldr.ToLower()).First();
//            //                            //if (dbFolder != null)
//            //                            //{
//            //                            //    ActiveSyncPCL.Folder.FolderType ft = (ActiveSyncPCL.Folder.FolderType)dbFolder.folderType;
//            //                            //    ActiveSyncPCL.Folder asFolder = new ActiveSyncPCL.Folder(dbFolder.displayName, dbFolder.serverIdentifier.ToString(), ft, null);
//            //                            //    if (dbFolder.syncKey != "1")
//            //                            //    {
//            //                            //        asFolder.SyncKey = dbFolder.syncKey;
//            //                            //    }
//            //                            //    double val = System.Convert.ToDouble(dbFolder.syncTime);
//            //                            //    DateTime dt = DateTime.FromOADate(val);
//            //                            //    asFolder.LastSyncTime = dt;
//            //                            //    easFolders.Add(asFolder);

//            //                            //    await mActiveSync.SyncMailBasedOnFolder(asFolder);
//            //                            //}
//            //                            //else
//            //                            //{
//            //                            //    await mActiveSync.SyncMailBasedOnFolder(strfldr);
//            //                            //} 
//            //                            #endregion

//            //                            await mActiveSync.SyncMailBasedOnFolder(strfldr);
//            //                        }
//            //                    }
//            //                }
//            //                else
//            //                {
//            //                    // Set up folders for prepopulated with 'Inbox', 'Sent Items', 'Deleted Items'
//            //                    await mActiveSync.SyncMailBasedOnFolder("Inbox");
//            //                    await mActiveSync.SyncMailBasedOnFolder("Sent Items");
//            //                    await mActiveSync.SyncMailBasedOnFolder("Deleted Items");

//            //                    #region PJ: Not required code. Need to confirm.
//            //                    //                    FolderManager fldrManager = new FolderManager();
//            //                    //#if USE_ASYNC
//            //                    //                    var result = await fldrManager.GetFolderInfoAsync(0);
//            //                    //#else
//            //                    //                    var result = fldrManager.GetFolderInfo(0);
//            //                    //#endif
//            //                    //                    workspace_datastore.models.Folder getRootFolders = result.Where(c => c.displayName.ToLower() == "inbox").First();
//            //                    //                    if (getRootFolders != null)
//            //                    //                    {
//            //                    //                        ActiveSyncPCL.Folder.FolderType ft = (ActiveSyncPCL.Folder.FolderType)getRootFolders.folderType;
//            //                    //                        ActiveSyncPCL.Folder asFolder = new ActiveSyncPCL.Folder(getRootFolders.displayName, getRootFolders.serverIdentifier.ToString(), ft, null);
//            //                    //                        if (getRootFolders.syncKey != "1")
//            //                    //                        {
//            //                    //                            asFolder.SyncKey = getRootFolders.syncKey;
//            //                    //                        }
//            //                    //                        double val = System.Convert.ToDouble(getRootFolders.syncTime);
//            //                    //                        DateTime dt = DateTime.FromOADate(val);
//            //                    //                        asFolder.LastSyncTime = dt;
//            //                    //                        easFolders.Add(asFolder);
//            //                    //                    }
//            //                    //                    getRootFolders = result.Where(c => c.displayName.ToLower() == "sent items").First();
//            //                    //                    if (getRootFolders != null)
//            //                    //                    {
//            //                    //                        ActiveSyncPCL.Folder.FolderType ft = (ActiveSyncPCL.Folder.FolderType)getRootFolders.folderType;
//            //                    //                        ActiveSyncPCL.Folder asFolder = new ActiveSyncPCL.Folder(getRootFolders.displayName, getRootFolders.serverIdentifier.ToString(), ft, null);
//            //                    //                        if (getRootFolders.syncKey != "1")
//            //                    //                        {
//            //                    //                            asFolder.SyncKey = getRootFolders.syncKey;
//            //                    //                        }
//            //                    //                        double val = System.Convert.ToDouble(getRootFolders.syncTime);
//            //                    //                        DateTime dt = DateTime.FromOADate(val);
//            //                    //                        asFolder.LastSyncTime = dt;
//            //                    //                        easFolders.Add(asFolder);
//            //                    //                    }
//            //                    //                    getRootFolders = result.Where(c => c.displayName.ToLower() == "deleted items").First();
//            //                    //                    if (getRootFolders != null)
//            //                    //                    {
//            //                    //                        ActiveSyncPCL.Folder.FolderType ft = (ActiveSyncPCL.Folder.FolderType)getRootFolders.folderType;
//            //                    //                        ActiveSyncPCL.Folder asFolder = new ActiveSyncPCL.Folder(getRootFolders.displayName, getRootFolders.serverIdentifier.ToString(), ft, null);
//            //                    //                        if (getRootFolders.syncKey != "1")
//            //                    //                        {
//            //                    //                            asFolder.SyncKey = getRootFolders.syncKey;
//            //                    //                        }
//            //                    //                        double val = System.Convert.ToDouble(getRootFolders.syncTime);
//            //                    //                        DateTime dt = DateTime.FromOADate(val);
//            //                    //                        asFolder.LastSyncTime = dt;
//            //                    //                        easFolders.Add(asFolder);
//            //                    //                    } 
//            //                    #endregion
//            //                    workspace.WorkspaceEmailWP8.View.MailFolderSync.StoreAutoSyncFolderIds("Inbox;Sent Items;Deleted Items");

//            //                }
//            //                // DO NOT AWAIT THIS!
//            //                mActiveSync.SendPing(ActiveSyncManager.ASPingHeartBeatInterval, easFolders);
//            //                stopWatch.Stop();

//            //                ToastNotification toastDelay = new ToastNotification();
//            //                string message = "Last Updated " + Decimal.ToInt32(stopWatch.ElapsedMilliseconds / 1000) + " Seconds Ago";
//            //                toastDelay.CreateToastNotificationForVertical(message, "");
//            //                //toast.Title = "Please wait"; 

//            //            }
//            #endregion

//            #region Hardware Background from Select Folder emailScreen.
//            /*else if (e.NavigationMode == NavigationMode.Back && SuspensionManager.SessionState.ContainsKey("SelectedFolderToShowMail"))
//            {
//                Stopwatch stopWatch = new Stopwatch();
//                stopWatch.Start();

//                string folderTapped = SuspensionManager.SessionState["SelectedFolderToShowMail"] as string;
//                SuspensionManager.SessionState.Remove("SelectedFolderToShowMail");

//                PanoramaWorkspace.DefaultItem = Mail;

//                ToastWithIndeterminateProgressBar toast = new ToastWithIndeterminateProgressBar();
//                toast.Title = "Please wait";
//                toast.SubTitle = "Sync in progress";
//                toast.Show();

//                mCurrentVisibleMailFolder = folderTapped;

//                //bool isSyncDone = true; //await mActiveSync.SyncMailBasedOnFolder(folderTapped);  
//                //code for testing meeting response
//                //await mActiveSync.SendMeetingResponseRequest("1", "5", "5:15");
//                //Inbox Folder
//                //                FolderManager folderManager = new FolderManager();
//                //#if USE_ASYNC
//                //                List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfoAsync(0).Result;
//                //#else
//                //                List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfo(0);
//                //#endif          
//                //                List<ActiveSyncPCL.Folder> fld = new List<ActiveSyncPCL.Folder>();
//                //                ActiveSyncPCL.Folder fldr = null;
//                //                fldr= getFolderObject("Inbox",folderListDB);
//                //                fld.Add(fldr);
//                //                fldr = getFolderObject("Sent Items", folderListDB);
//                //                fld.Add(fldr);
//                //                fldr = getFolderObject("Deleted Items", folderListDB);
//                //                fld.Add(fldr);
//                //                fldr = getFolderObject("Calendar", folderListDB);
//                //                fld.Add(fldr);
//                //                fldr = getFolderObject("Contacts", folderListDB);
//                //                fld.Add(fldr);
//                //                mActiveSync.SendPing(ActiveSyncManager.ASPingHeartBeatInterval, fld);


//                //#if NOTNOW
//                //move email items from one folder to another folder
//                var selectedMails = emailScreen.mailList.SelectedItems;
//                List<ASMoveItemsRequest> moveItems = new List<ASMoveItemsRequest>();
//                if (selectedMails.Count > 0)
//                {
//                    FolderManager folderManager = new FolderManager();
//                    List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfoAsync(0).Result;
//                    ActiveSyncPCL.Folder fldr = ActiveSync.getFolderObject(folderTapped, folderListDB);
//                    foreach (MessageManager msg in selectedMails)
//                    {
//                        ASMoveItemsRequest objMoveItemsReq = new ASMoveItemsRequest();
//                        objMoveItemsReq.sourceFolderId = msg.folderId.ToString();
//                        objMoveItemsReq.destFolderId = fldr.Id;
//                        objMoveItemsReq.msgId = msg.serverID;
//                        moveItems.Add(objMoveItemsReq);
//                    }
//                    //call active sync method to move email tiems
//                    await mActiveSync.MoveEmailItems(moveItems);
//                }
//                //end
//                bool isSyncDone = await mActiveSync.SyncMailBasedOnFolder(folderTapped);
//                stopWatch.Stop();

//                ToastNotification toastDelay = new ToastNotification();
//                string message = "Last Updated " + Decimal.ToInt32(stopWatch.ElapsedMilliseconds / 1000) + " Seconds Ago";
//                toastDelay.CreateToastNotificationForVertical(message, "");
//                //toast.Title = "Please wait";

//                if (isSyncDone)
//                {
//                    var dt = (EmailItemsViewModel)emailScreen.DataContext;
//                    // dt.LoadData(folderTapped);
//                    if (folderTapped.Equals("Inbox"))
//                        Mail.Header = "Mail";
//                    else
//                        Mail.Header = folderTapped;



//                    ApplicationBar = (ApplicationBar)this.Resources["mailPageAppBar"];
//                }
//                else
//                {
//                    ToastNotification toast1 = new ToastNotification();
//                    toast1.CreateToastNotificationForVertical("Error", "Please try to sync again the device.");
//                }
//                //#endif
//            } */
//            #endregion
//            //string strItemIndex;
//            #region Moved to OnNavigatetoBrowser
//            //if (e.NavigationMode == NavigationMode.Back && SuspensionManager.SessionState.ContainsKey("ApplicationNavigatedFrom"))
//            //    {
//            //        OnNavigateToBrowser(e);
//            //    } 
//            #endregion

//            #region Back from draft Moved to OnNavigatetoEmail
//            /*if (e.NavigationMode == NavigationMode.Back && SuspensionManager.SessionState.ContainsKey("ApplicationNavigatedFromDraftMessage"))
//            {
                
//                if (SuspensionManager.SessionState["ApplicationNavigatedFromDraftMessage"].ToString().Trim().ToLower() == "drafts")
//                {
//                    SuspensionManager.SessionState.Remove("ApplicationNavigatedFromDraftMessage");
//                    var dt = (EmailItemsViewModel)emailScreen.DataContext;
//                    dt.LoadData(mCurrentVisibleMailFolder);
//                } 
                

//            }*/
//            #endregion

//            // We've come back to Panorama page, so let's update any halo updates that came from ccm
//            if (mPendingSettings != null && mPendingSettings.Count > 0)
//            {
//                ProcessHaloUpdates();
//                mPendingSettings.Clear();
//                mPendingSettings = null;
//            }
//            #region Refresh contact data(Dhruv 04 dec 2014)
//            if (SuspensionManager.SessionState.ContainsKey("NewContactAdded"))
//            {
//                SuspensionManager.SessionState.Remove("NewContactAdded");
//                OnContactItemSelected();
//            }
//            //if user come back from contact list page after deleting some contact
//            else if (SuspensionManager.SessionState.ContainsKey("BackToPanorma"))
//            {
//                SuspensionManager.SessionState.Remove("BackToPanorma");
//                OnContactItemSelected();
//            }
//            #endregion

            
//            //If pin is changed show this messgae
//            //if (e.Parameter.ToString().Contains("PINCHANGED"))
//           #if WP_80_SILVERLIGHT
//            if (NavigationContext.QueryString.ContainsKey("PINCHANGED"))
//            {
//                PanoramaWorkspace.DefaultItem = PanoramaWorkspace.Items[0];
//                PanoramaWorkspace.Header = "Menu";
//                BottomAppBar = (AppBar)this.Resources["menuPageBar"];

//                string val = NavigationContext.QueryString["PINCHANGED"];
//                if (val == "1")
//                {
//                    ToastNotification myToast = new ToastNotification();
//                    myToast.CreateToastNotificationForVertical("Changed Pin Succesfully", "");
//                }

//                NavigationContext.QueryString.Remove("PINCHANGED");        // remove pinchanges key
//            }
//            #else

//            if(e.Parameter.ToString().Contains("PINCHANGED"))
//            {
//                PanoramaWorkspace.ScrollToSection(Menu);
//                //PanoramaWorkspace.DefaultItem = PanoramaWorkspace.Items[0];
//                PanoramaWorkspace.Header = "Menu";
//                BottomAppBar = (AppBar)this.Resources["menuPageBar"];

//                //string val = NavigationContext.QueryString["PINCHANGED"];
//                if (e.Parameter.ToString() == "1")
//                {
//                    #if TO_FIX_FOR_TABLET
//                    ToastNotification myToast = new ToastNotification();
//                    myToast.CreateToastNotificationForVertical("Changed Pin Succesfully", "");
//#endif
//                }

//                //NavigationContext.QueryString.Remove("PINCHANGED");        // remove pinchanges key
//            }

//#endif
//        }

        public async Task SetupPingRequest(bool syncListenerFolders, bool overridePingSetup = false)
        {
            if (!mSetupPinger || overridePingSetup)
            {
                mSetupPinger = true;

                List<ActiveSyncPCL.Folder> easFolders = new List<ActiveSyncPCL.Folder>();
                string selectedFolders;
#if WP_80_SILVERLIGHT


                IsolatedStorageSettings mailFolderSyncSettings = IsolatedStorageSettings.ApplicationSettings;
                mailFolderSyncSettings.TryGetValue<string>("MAIL_FOLDER_SYNC_SETTING", out selectedFolders);
#else
                ApplicationDataContainer mailFolderSyncSettings = ApplicationData.Current.LocalSettings;
                selectedFolders   = mailFolderSyncSettings.Values.ContainsKey("MAIL_FOLDER_SYNC_SETTING")? mailFolderSyncSettings.Values["MAIL_FOLDER_SYNC_SETTING"] as String : String.Empty;

#endif


                if (syncListenerFolders && selectedFolders != null && selectedFolders != string.Empty)
                {
                    FolderManager folderManager = new FolderManager();
#if USE_ASYNC
                    var result = await folderManager.GetFolderInfoAsync(0);
#else
                    var result = fldrManager.GetFolderInfo(0);
#endif
                    string[] spilitfolderIDs = selectedFolders.Split(';');

                    foreach (string strfldr in spilitfolderIDs)
                    {
                        if (!String.IsNullOrEmpty(strfldr))
                        {
                            workspace_datastore.models.Folder dbFolder = result.Where(c => c.displayName.ToLower() == strfldr.ToLower()).First();
                            if (dbFolder != null)
                            {
                                ActiveSyncPCL.Folder.FolderType ft = (ActiveSyncPCL.Folder.FolderType)dbFolder.folderType;
                                ActiveSyncPCL.Folder asFolder = new ActiveSyncPCL.Folder(dbFolder.displayName, dbFolder.serverIdentifier.ToString(), ft, null);
                                if (dbFolder.syncKey != "1")
                                {
                                    asFolder.SyncKey = dbFolder.syncKey;
                                }
                                double val = System.Convert.ToDouble(dbFolder.syncTime);
                                DateTime dt = DateTimeExtensions.FromOADate(val);
                                asFolder.LastSyncTime = dt;
                                easFolders.Add(asFolder);

                                #if TO_FIX_FOR_TABLET
                                mSyncBGWorker.InitializeAndDoSync(asFolder.Name);
#endif
                            }
                        }
                    }
                }
                else
                {
                    // Set up folders for prepopulated with 'Inbox', 'Sent Items', 'Deleted Items'
                    if (syncListenerFolders)
                    {
                        #if TO_FIX_FOR_TABLET
                        mSyncBGWorker.SetScreenObject(this);
                        mSyncBGWorker.InitializeAndDoSync("Inbox");
#endif
                        //mSyncBGWorker.InitializeAndDoSync("Sent Items");
                        //mSyncBGWorker.InitializeAndDoSync("Deleted Items");
                    }

                    FolderManager fldrManager = new FolderManager();
#if USE_ASYNC
                    var result = await fldrManager.GetFolderInfoAsync(0);
#else
                    var result = fldrManager.GetFolderInfo(0);
#endif
                    workspace_datastore.models.Folder getRootFolders = result.Where(c => c.displayName.ToLower() == "inbox" && c.parentFolder_id == 0).First();
                    if (getRootFolders != null)
                    {
                        ActiveSyncPCL.Folder.FolderType ft = (ActiveSyncPCL.Folder.FolderType)getRootFolders.folderType;
                        ActiveSyncPCL.Folder asFolder = new ActiveSyncPCL.Folder(getRootFolders.displayName, getRootFolders.serverIdentifier.ToString(), ft, null);
                        if (getRootFolders.syncKey != "1")
                        {
                            asFolder.SyncKey = getRootFolders.syncKey;
                        }
                        double val = System.Convert.ToDouble(getRootFolders.syncTime);
                        DateTime dt = DateTimeExtensions.FromOADate(val);
                        asFolder.LastSyncTime = dt;
                        easFolders.Add(asFolder);
                    }
                    getRootFolders = result.Where(c => c.displayName.ToLower() == "calendar" && c.parentFolder_id == 0).First();
                    if (getRootFolders != null)
                    {
                        // For calendar
                        ActiveSyncPCL.Folder.FolderType ft = (ActiveSyncPCL.Folder.FolderType)getRootFolders.folderType;
                        ActiveSyncPCL.Folder asFolder = new ActiveSyncPCL.Folder(getRootFolders.displayName, getRootFolders.serverIdentifier.ToString(), ft, null);
                        if (getRootFolders.syncKey != "1")
                        {
                            asFolder.SyncKey = getRootFolders.syncKey;
                        }
                        double val = System.Convert.ToDouble(getRootFolders.syncTime);
                        DateTime dt = DateTimeExtensions.FromOADate(val);
                        asFolder.LastSyncTime = dt;
                        easFolders.Add(asFolder);
                    }
                    getRootFolders = result.Where(c => c.displayName.ToLower() == "contacts" && c.parentFolder_id == 0).First();
                    if (getRootFolders != null)
                    {
                        // For contacts
                        ActiveSyncPCL.Folder.FolderType ft = (ActiveSyncPCL.Folder.FolderType)getRootFolders.folderType;
                        ActiveSyncPCL.Folder asFolder = new ActiveSyncPCL.Folder(getRootFolders.displayName, getRootFolders.serverIdentifier.ToString(), ft, null);
                        if (getRootFolders.syncKey != "1")
                        {
                            asFolder.SyncKey = getRootFolders.syncKey;
                        }
                        double val = System.Convert.ToDouble(getRootFolders.syncTime);
                        DateTime dt = DateTimeExtensions.FromOADate(val);
                        asFolder.LastSyncTime = dt;
                        easFolders.Add(asFolder);
                    }
#if SYNC_THESE_FOLDERS_AT_INSTALL // We don't want to sync these folders at install time.
                    getRootFolders = result.Where(c => c.displayName.ToLower() == "sent items").First();
                    if (getRootFolders != null)
                    {
                        ActiveSyncPCL.Folder.FolderType ft = (ActiveSyncPCL.Folder.FolderType)getRootFolders.folderType;
                        ActiveSyncPCL.Folder asFolder = new ActiveSyncPCL.Folder(getRootFolders.displayName, getRootFolders.serverIdentifier.ToString(), ft, null);
                        if (getRootFolders.syncKey != "1")
                        {
                            asFolder.SyncKey = getRootFolders.syncKey;
                        }
                        double val = System.Convert.ToDouble(getRootFolders.syncTime);
                        DateTime dt = DateTime.FromOADate(val);
                        asFolder.LastSyncTime = dt;
                        easFolders.Add(asFolder);
                    }
                    getRootFolders = result.Where(c => c.displayName.ToLower() == "deleted items").First();
                    if (getRootFolders != null)
                    {
                        ActiveSyncPCL.Folder.FolderType ft = (ActiveSyncPCL.Folder.FolderType)getRootFolders.folderType;
                        ActiveSyncPCL.Folder asFolder = new ActiveSyncPCL.Folder(getRootFolders.displayName, getRootFolders.serverIdentifier.ToString(), ft, null);
                        if (getRootFolders.syncKey != "1")
                        {
                            asFolder.SyncKey = getRootFolders.syncKey;
                        }
                        double val = System.Convert.ToDouble(getRootFolders.syncTime);
                        DateTime dt = DateTime.FromOADate(val);
                        asFolder.LastSyncTime = dt;
                        easFolders.Add(asFolder);
                    }
#endif
                  //  workspace.WorkspaceEmailWP8.View.MailFolderSync.StoreAutoSyncFolderIds("Inbox");
                }
                //if(!syncListenerFolders)
                {
                    // DO NOT AWAIT THIS!
                    mActiveSync.EASPingRequested = false;
                    mActiveSync.SendPing(ActiveSyncManager.ASPingHeartBeatInterval, easFolders);
                }
            }
        }

        /// <summary>
        /// Method that gets called only after the sync after application start is COMPLETED
        /// </summary>
        public async void FirstTimeNavigateToPanoramaTask(bool bFromInitialSync)
        {
            #region First time panorama page code
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            if (SuspensionManager.SessionState.ContainsKey("EventResponseFromEmailScreenRSVPToast"))
            {
                #if TO_FIX_FOR_TABLET
                string msgforToast = (string)SuspensionManager.SessionState["EventResponseFromEmailScreenRSVPToast"];
                SuspensionManager.SessionState.Remove("EventResponseFromEmailScreenRSVPToast");
                ToastNotification toastRSVP = new ToastNotification();
                string messageRSVP = "";
    
                toastRSVP.CreateToastNotificationForVertical(messageRSVP, "");
                if (msgforToast.Length <= 20)
                    messageRSVP = msgforToast;
                else
                    messageRSVP = msgforToast.Substring(0, 15) + " " + "...";
                // toast.SubTitle = "Syncing in progress for all selected folders";
                toastRSVP.CreateToastNotificationForVertical(messageRSVP, "");
                #endif
            }
            else if (!bFromInitialSync)
            {
                ToastWithIndeterminateProgressBar toast = new ToastWithIndeterminateProgressBar();
                toast.Title = "Please wait";
                toast.SubTitle = "Syncing in progress for all selected folders";
                toast.Show();
            }

            // Set up the Pinger
            SetupPingRequest(!bFromInitialSync);

            stopWatch.Stop();
            #if TO_FIX_FOR_TABLET

            ToastNotification toastDelay = new ToastNotification();
            string message = "Last Updated " + Decimal.ToInt32(stopWatch.ElapsedMilliseconds / 1000) + " Seconds Ago";
            toastDelay.CreateToastNotificationForVertical(message, "");
#endif

            /*
                            Dispatcher.BeginInvoke(() =>
                            {
                                this.LayoutRoot.Opacity = 1.0;
                                this.wait_tosync_popup.Visibility =  Visibility.Collapsed;
                                this.PanoramaWorkspace.IsEnabled = true;
                                ApplicationBar.IsVisible = true;
                            });
            */
            #endregion
        }

        /// <summary>
        /// Called when the client has RECEIVED the add/update/delete items list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        //PK:Never gets called
//        async void activeSync_EASCollectionChangedEvent(object sender, EASCollectionChangedEventArgs e)
//        {
//            //Dhruv: Added to sync contacts and calendar, earlier it was synching only emails
//            if (e.Folder.Name.ToLower() == "contacts")
//            {
//                ParsingUtils.ParseContactContent(e.Adds, e.Deletes, e.Changes, e.SoftDeletes, e.Folder);
//            }
//            else if (e.Folder.Name.ToLower() == "calendar")
//            {
//                ParsingUtils.ParseCalendarContent(e.Adds, e.Deletes, e.Changes, e.SoftDeletes, e.Folder);
//            }
//            else
//            {
//                if (e.Adds.Count > 0)
//                {
//#if WP_80_SILVERLIGHT
//                    ToastNotificationManager st = new ShellToast();
//                    st.Title = "WorkspaceWP81 Mail";
//                    st.Content = string.Format("Received {0} new messages", e.Adds.Count);
//                    st.Show();
//#endif
//                }

//                await ParsingUtils.ParseMailContent(e.Adds, e.Deletes, e.Changes, e.SoftDeletes, e.Folder);

//                // Do an asynchronous update of the email items list view
//                if (e.Folder.Name.ToLower() == mCurrentVisibleMailFolder.displayName.ToLower())
//                {
//                    EmailItemsView emailScreen = FindChildControl<EmailItemsView>(calendar, "emailScreen") as EmailItemsView;
//                    await emailScreen.GenerateFolderIdAndSetCurrentFolder(mCurrentVisibleMailFolder);

//                    //System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
//                    CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
//                    {
//                        emailScreen.UpdateSource(DateTime.Now.ToOADate(), "", EmailItemsView.Order.Descending);
//                    });
//                }
//            }
//            //System.Windows.Deployment.Current.Dispatcher.BeginInvoke(async () =>
//            CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
//            {
//                await UpdateSelectedPanoromaItem(e);
//            });


//        }



        public async Task UpdateSelectedPanoromaItem(EASCollectionChangedEventArgs e)
        {

            var section = PanoramaWorkspace.SectionsInView[0];
            var tag = section.Tag.ToString();
            var selectedIndex = System.Convert.ToInt32(tag);

            switch (selectedIndex)
            {
                case 0: //Menu
                    if (e.Folder.Name.ToLower() == "inbox")
                    {
                        UpdateNewEmailCountOnMenuItem();
                    }
                    else if (e.Folder.Name.ToLower() == "calendar")
                    {
                        UpdateMeetingCount();
                    }
                    break;
                case 1://Email
                    //PK: We will be loading current visible folder data, when we will get call back of same folder.
                    //In rest cases, we will be updating only database of related folder, not the UI stuff
                    //if (mCurrentVisibleMailFolder.Equals(e.Folder.Name))
                    //{
                    //    var dt = (EmailItemsViewModel)emailScreen.DataContext;
                    //    await dt.LoadData(mCurrentVisibleMailFolder);  // Probably inefficient
                    //    UpdateEmailItemUI();
                    //}
                    if (mCurrentVisibleMailFolder.Equals(e.Folder.Name))
                    {
#if WP_80_SILVERLIGHT
                        await emailScreen.GenerateFolderIdAndSetCurrentFolder(mCurrentVisibleMailFolder);
                        emailScreen.UpdateSource(emailScreen.DateAndTimeOfFirstMessageInListViewBlock, "", EmailItemsView.Order.Descending);
#endif

                        #if TO_FIX_FOR_TABLET
                        EmailItemsView emailScreen = FindChildControl<EmailItemsView>(calendar, "emailScreen") as EmailItemsView;

                        emailScreen.GenerateFolderIdAndSetCurrentFolder(mCurrentVisibleMailFolder);
                        await emailScreen.UpdateSourceFromUI("");
#endif

                    }
                    break;
            }
        }

        void mActiveSync_EASCommunicationsErrorEvent(object sender, EASCommunicationsErrorEventArgs e)
        {
            #if TO_FIX_FOR_TABLET
            if (e.CommandResponse.HttpStatus == System.Net.HttpStatusCode.Unauthorized)
            {
                Common.ToastNotification tn = new Common.ToastNotification();
                tn.CreateToastNotificationForVertical("Exchange Communication Error", "Exchange server returned: UNAUTHORIZED");
            }
            else
            {
                Common.ToastNotification tn = new Common.ToastNotification();
                tn.CreateToastNotificationForVertical("Exchange Communication Error", "Exchange Server returned: UNKNOWN");
            }
           #endif
        }

        //public ObservableCollection<MessageManager> Items { get; private set; }

		private void About_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/About/AboutScreen.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("AboutScreen");
        }

        private void ChangePIN_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/ChangePin.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("ChangePin");
        }

        private void Select_Click(object sender, RoutedEventArgs e)
        {
            #if TO_FIX_FOR_TABLET
            EmailItemsView emailScreen = FindChildControl<EmailItemsView>(calendar, "emailScreen") as EmailItemsView;
            var dt = (EmailItemsViewModel)emailScreen.DataContext;
#if WP_80_SILVERLIGHT
            emailScreen.mailList.IsSelectionEnabled = true;
#endif
            dt.IsVisibleSel = "Visible";
            dt.IsVisible = "Collapsed";
            BottomAppBar = mailPageAppBarSelect;
            BottomAppBar.Visibility = Windows.UI.Xaml.Visibility.Visible;
#endif
        }

        private void MailSearch_Click(object sender, RoutedEventArgs e)
        {
            SuspensionManager.SessionState["CurrentFolder"] = mCurrentVisibleMailFolder;
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceEmailWP8/View/MailSearchView.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("MailSearchView");
        }

        private void Folders_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceEmailWP8/View/MailFolderSync.xaml", UriKind.Relative));
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceEmailWP8/View/MailFolders.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("MailFolders");
        }

        private void Settings_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceEmailWP8/View/MailSettings.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("MailSettings");
        }

        private async void Refresh_Click(object sender, RoutedEventArgs e)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceEmailWP8/View/MailFolderSync.xaml", UriKind.Relative));
            if (mCurrentVisibleMailFolder.displayName != string.Empty)
            {
                ToastWithIndeterminateProgressBar toast = new ToastWithIndeterminateProgressBar();
                toast.Title = "Please wait";
                toast.SubTitle = "Syncing " + mCurrentVisibleMailFolder.displayName;
                toast.Show();

                string visibleFolder = mCurrentVisibleMailFolder.displayName;
                if (visibleFolder.ToLower().CompareTo("flagged") == 0)
                {
                    visibleFolder = "Inbox";
                }
                bool isSynced = await mActiveSync.SyncMailBasedOnFolder(mCurrentVisibleMailFolder.displayName, mCurrentVisibleMailFolder.parentFolder_id);
                stopwatch.Stop();

                #if TO_FIX_FOR_TABLET
                ToastNotification toastDelay = new ToastNotification();
                string message = "Last Updated " + Decimal.ToInt32(stopwatch.ElapsedMilliseconds / 1000) + " Seconds Ago";
                toastDelay.CreateToastNotificationForVertical(message, "");
                //toast.Title = "Please wait";
#endif

                if (!isSynced)
                {
                    #if TO_FIX_FOR_TABLET
                    ToastNotification toast1 = new ToastNotification();
                    toast1.CreateToastNotificationForVertical("Error", "Couldn't open connection to the server.");
#endif
                }
            }
        }

        private async void EmptyTrash_Click(object sender, RoutedEventArgs e)
        {
            if (App.CheckInternetConnectivity())
            {
                string leftButtonContent = "Empty Trash";
                string rightButtonContent = "Cancel";

                string caption = "Empty Trash?";
                string message = "All mails in the Deleted Items folder will be permanently removed.";

#if WP_80_SILVERLIGHT
                CustomMessageBox messageBox = new CustomMessageBox()
                {
                    Caption = caption,
                    Message = message,
                    LeftButtonContent = leftButtonContent,
                    RightButtonContent = rightButtonContent

                };
                messageBox.Dismissed += new EventHandler<DismissedEventArgs>(EmptyTrashToastClicked);
                messageBox.Show();
                #endif
            }
            else
            {
#if TO_FIX_FOR_TABLET
                ToastNotification toast = new ToastNotification();
                toast.CreateToastNotificationForVertical("Connection Error.", "Network is not available.");
                #endif
            }
        }


#if WP_80_SILVERLIGHT
        private async void EmptyTrashToastClicked(Object obj, DismissedEventArgs e)
        {
            
            switch (e.Result)
            {
                case CustomMessageBoxResult.LeftButton:

                    string folderId = mCurrentVisibleMailFolder.serverIdentifier;
                    if (!string.IsNullOrEmpty(folderId))
                    {
                        var isEmptyTrashDone = await mActiveSync.EmptyFolderAsync(folderId);

                        if (isEmptyTrashDone)
                        {
                            //PK: We are getting delete command after Emptyfoder operation from server...
                            //So better rely on server for deleting the messages. So Sync again the current folder.
                            //It will contain many delete command...doing in BGWorker
                            mSyncBGWorker.SetScreenObject(this);
                            bool isSyncStarted = mSyncBGWorker.InitializeAndDoSync(mCurrentVisibleMailFolder);


                            //Create toast notfication object
                            ToastNotification toast = new ToastNotification();

                            SetEmailMultiSelectionCaps(false);
                            emailScreen.UpdateSourceFromUI("");
                            toast.CreateToastNotificationForVertical("Success", "Empty Trash Done");

                            UpdateEmailItemUI();
                            UpdateEmailItemUIBasedOnEmailCount();

                        }
                    }
                    break;
                case CustomMessageBoxResult.RightButton:
                    break;
                case CustomMessageBoxResult.None:
                    break;
                default:
                    break;
            }
           
        }
#endif
        private async void deletemail_Click(object sender, RoutedEventArgs e)
        {
            #if TO_FIX_FOR_TABLET
            EmailItemsView emailScreen = FindChildControl<EmailItemsView>(calendar, "emailScreen") as EmailItemsView;
            var selectedMails = emailScreen.mailList.SelectedItems;

            if (selectedMails.Count <= 0)
                return;
            string caption = string.Empty;
            string message = string.Empty;
            string leftButtonContent = "Delete";
            string rightButtonContent = "Cancel";
            if (selectedMails.Count == 1)
            {
                caption = "Delete Message?";
                message = "Deleting will send message to deleted items.";

                if (mCurrentVisibleMailFolder.displayName.ToLower() == "deleted items")
                {
                    message = "Deleting will permanently remove this message.";
                }
            }
            else
            {
                caption = "Delete Messages?";
                message = "Deleting will send messages to deleted items.";
                if (mCurrentVisibleMailFolder.displayName.ToLower() == "deleted items")
                {
                    message = "Deleting will permanently remove these messages.";
                }
            }
#endif
            #if WP_80_SILVERLIGHT
            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Caption = caption,
                Message = message,
                LeftButtonContent = leftButtonContent,
                RightButtonContent = rightButtonContent

            };
            messageBox.Dismissed += new EventHandler<DismissedEventArgs>(DeleteItemToastClicked);
            messageBox.Show();
#endif
        }

         #if WP_80_SILVERLIGHT
        private async void DeleteItemToastClicked(Object obj, DismissedEventArgs e)
        {
           
            switch (e.Result)
            {
                case CustomMessageBoxResult.LeftButton:

                    var selectedMails = emailScreen.mailList.SelectedItems;
                    List<ASDeleteItemsRequest> deleteItems = new List<ASDeleteItemsRequest>();
                    //create bool varibale to check emails deleted or not
                    bool isDeleted = false;
                    foreach (MessageManager msgMngr in selectedMails)
                    {
                        if (mCurrentVisibleMailFolder.displayName.Trim().ToLower() == "drafts")
                        {
                            await new Draft_PartsManager().DeleteDraftPartsInfo(msgMngr.messgaeId);
                            await new MessageManager().DeleteDraftMessageInfoAsync(msgMngr.messgaeId,Convert.ToInt32(mCurrentVisibleMailFolder.serverIdentifier));
                            //if local draft message deleted set bool variable true
                            isDeleted = true;
                        }
                        if (msgMngr.serverID != null)
                        {
                            ASDeleteItemsRequest objDeleteItemsRequest = new ASDeleteItemsRequest();
                            objDeleteItemsRequest.serverId = msgMngr.serverID;
                            deleteItems.Add(objDeleteItemsRequest);
                        }
                    }
                    if (deleteItems != null && deleteItems.Count > 0)
                    {
                        ASDeleteItemsResponse deleteResponse = await mActiveSync.DeleteEmaiItems(mCurrentVisibleMailFolder.displayName, mCurrentVisibleMailFolder.parentFolder_id, deleteItems);
                        if (deleteResponse != null && deleteResponse.status == 1)
                        {
                            //if message delete from server set bool variable true
                            isDeleted = true;
                            foreach (MessageManager msgMngr in selectedMails)
                            {
                                MessageManager objmssgMngr = new MessageManager();
                                objmssgMngr.DeleteMessageInfoAsync(msgMngr.folderId, msgMngr.messgaeId);
                            }
                            selectedMails = null;
                        }
                    }
                    //Create toast notfication object
                    ToastNotification toast = new ToastNotification();
                    //if message deleted then refresh UI by getting updated data from local database
                    if (isDeleted)
                    {
                        //set multi selction false
                        SetEmailMultiSelectionCaps(false);
                        //call method to load updated data from DB 
                        if (mCurrentVisibleMailFolder.displayName.ToLower() == "outbox" || mCurrentVisibleMailFolder.displayName.ToLower() == "flagged")
                        {
                            var dt = (EmailItemsViewModel)emailScreen.DataContext;
                            await dt.LoadData(mCurrentVisibleMailFolder.displayName);
                        }
                        else
                        {
                            //call method to update data in list
                            await emailScreen.UpdateSource(DateTime.Now.ToOADate(), "", EmailItemsView.Order.Descending, false, true);
                        }
                        //update email count
                        UpdateEmailItemUI();
                        toast.CreateToastNotificationForVertical("Success", "Deleted successfully");
                    }
                    else
                    {
                        toast.CreateToastNotificationForVertical("Failed to delete message ", "Couldn't open connection to the server");
                    }
                    break;
                case CustomMessageBoxResult.RightButton:
                    break;
                case CustomMessageBoxResult.None:
                    break;
                default:
                    break;
            }
        
        }
#endif

        private void markunread_Click(object sender, RoutedEventArgs e)
        {
        
            #if TO_FIX_FOR_TABLET
            EmailItemsView emailScreen = FindChildControl<EmailItemsView>(calendar, "emailScreen") as EmailItemsView;
            var selectedMails = emailScreen.mailList.SelectedItems;

            if (selectedMails.Count <= 0)
                return;
            string caption = string.Empty;
            string message = string.Empty;
            string leftButtonContent = string.Empty;
            string rightButtonContent = "Cancel";


            msgManagerListForReadUnread = new List<MessageManager>();
            foreach (MessageManager msg in selectedMails)
                msgManagerListForReadUnread.Add(msg);

            bool isAnyReadMailInSelected = msgManagerListForReadUnread.Exists(p => p.read == 1);
            bool isAnyUnreadMailInSelected = msgManagerListForReadUnread.Exists(p => p.read == 0);            
            //All read selected
            if (isAnyReadMailInSelected == true && isAnyUnreadMailInSelected == false)
            {
                caption = "Mark Unread?";
                message = "Marking unread will display it as unread";
                leftButtonContent = "Mark as Unread";
            }
            //All unread selected
            else if (isAnyReadMailInSelected == false && isAnyUnreadMailInSelected == true)
            {
                caption = "Mark Read?";
                message = "Marking read will display it as read";
                leftButtonContent = "Mark as Read";
            }
            //read unread mix
            else
            {
                caption = "Mark All Selected as Unread?";
                message = "this will display all selected as Unread";
                leftButtonContent = "Mark as Unread";
            }
#endif
            #if WP_80_SILVERLIGHT
            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Caption = caption,
                Message = message,
                LeftButtonContent = leftButtonContent,
                RightButtonContent = rightButtonContent

            };
            //it was updating the list before clicking on read\unread button
            //foreach (MessageManager msg in msgManagerListForReadUnread)
            //{
            //    msg.read = readChange;
            //}

            messageBox.Dismissed += new EventHandler<DismissedEventArgs>(UnReadToastClicked);
            messageBox.Show();
#endif
            
        }
            #if WP_80_SILVERLIGHT
        private async void UnReadToastClicked(Object obj, DismissedEventArgs e)
        {

            //get custom message box object and store in local variable
            CustomMessageBox customMsgObj =(CustomMessageBox)obj;
            switch (e.Result)
            {
                case CustomMessageBoxResult.LeftButton:
                    {
                        int readChange = 0;
                        //user click on mark as read button
                        if (customMsgObj.LeftButtonContent.ToString().ToLower() == "mark as read")
                        {
                            readChange = 1;
                        }
                        //set read value of messgae in msgManagerListForReadUnread
                        foreach (MessageManager msg in msgManagerListForReadUnread)
                        {
                            msg.read = readChange;
                        }
                        FolderManager folderManager = new FolderManager();
                        mCurrentVisibleMailFolder = folderManager.GetFolderInfoAsync("Inbox", 0).Result;
                        ToastNotification toast = new ToastNotification(); 
                         bool changeDone = await ActiveSync.GetInstance().SyncMailClientChanges(mCurrentVisibleMailFolder, ServerSyncCommand.ServerSyncCommandType.Change, msgManagerListForReadUnread);
                         if (App.CheckInternetConnectivity())
                         {

                             if (changeDone)
                             {
                                 toast.CreateToastNotificationForVertical("Success", "Read/Unread Change Done");
                                 SetEmailMultiSelectionCaps(false);
                                 //System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                                 CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                                 {
                                     emailScreen.UpdateSource(DateTime.Now.ToOADate(), "", EmailItemsView.Order.Descending, false, true);
                                 });
                             }
                             else
                                 toast.CreateToastNotificationForVertical("Read/Unread Change Failed ", "Couldn't open connection to the server");
                         }
                         else
                         {
                             //How it will update on server(EAS)???
                             #region Write to messageTable
                             List<workspace_datastore.models.Message> msgsToUpdate = new List<workspace_datastore.models.Message>();
                             foreach (MessageManager msgManager in msgManagerListForReadUnread)
                             {
                                 workspace_datastore.models.Message msg = msgManager.GetMessageInfoAsync(msgManager.serverID).Result.ElementAt(0);
                                 msg.read = msgManager.read;
                                 msg.flag = msgManager.flag;
                                 msg.flagStatus = msgManager.flagStatus;
                                 msg.flagStartDate = msgManager.flagStartDate;
                                 msg.flagUTCStartDate = msgManager.flagUTCStartDate;
                                 msg.flagDueDate = msgManager.flagDueDate;
                                 msg.flagUTCDueDate = msgManager.flagUTCDueDate;
                                 msgsToUpdate.Add(msg);
                             }

                             MessageManager meesageMangerWritetoLocal = new MessageManager();
                             meesageMangerWritetoLocal.UpdateAllMessagesInfoAsync(msgsToUpdate);
                             #endregion
                             toast.CreateToastNotificationForVertical("Success", "Read/Unread Change Done");
                             SetEmailMultiSelectionCaps(false);
                             //System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                             CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                             {
                                 emailScreen.UpdateSource(DateTime.Now.ToOADate(), "", EmailItemsView.Order.Descending, false, true);
                             });
                         }
                    }
                    break;
                case CustomMessageBoxResult.RightButton:
                case CustomMessageBoxResult.None:
                default:
                    break;
            }

       
        }
#endif
        int mFlagChange = 0;

        private void flagmail_Click(object sender, RoutedEventArgs e)
        {
            #if TO_FIX_FOR_TABLET
            EmailItemsView emailScreen = FindChildControl<EmailItemsView>(calendar, "emailScreen") as EmailItemsView;
            var selectedMails = emailScreen.mailList.SelectedItems;

            if (selectedMails.Count <= 0)
                return;

            int selectedMailCount = selectedMails.Count;
            string caption = string.Empty;
            string message = string.Empty;
            string leftButtonContent = string.Empty;
            string rightButtonContent = "Cancel";


            msgManagerListForFlagging = new List<MessageManager>();
            foreach (MessageManager msg in selectedMails)
                msgManagerListForFlagging.Add(msg);

            bool isAnyFlaggedMailInSelected = msgManagerListForFlagging.Exists(p => p.flag == 1);
            bool isAnyUnFlaggedMailInSelected = msgManagerListForFlagging.Exists(p => p.flag == 0);
         
            //All read selected
            if (isAnyFlaggedMailInSelected == false && isAnyUnFlaggedMailInSelected == true)
            {
                caption = "Flag Message?";
                if (selectedMailCount == 1)
                {
                    message = "Flagging this message will add it to your flagged mail folder.";
                    leftButtonContent = "Flag";
                }
                else
                {
                    message = "flagging these messages will add them to your flagged mail folder. ";
                    leftButtonContent = "Flag Messages";
                }
               // flagChange = 1;
            }
            //All unread selected

            else if (isAnyFlaggedMailInSelected == true && isAnyUnFlaggedMailInSelected == false)
            {
                caption = "Mark Unflagged";

                if (selectedMailCount == 1)
                {
                    message = "Unflagging this message will remove it from your flagged mail folder.";
                    leftButtonContent = "Unflag Message";
                }
                else
                {
                    message = "Unflagging these messages will remove them from your flagged mail folder. ";
                    leftButtonContent = "Unflag";
                }
            }
            //read unread mix
            else
            {
                caption = "Mark All Selected as Flagged";
                message = "this will add all selected to your flagged folder";
                if (selectedMailCount == 1)
                    leftButtonContent = "Flag Message";
                else
                    leftButtonContent = "Flag Messages";

                //2 is used for flag
               // flagChange = 1;
            }

#endif

#if WP_80_SILVERLIGHT
            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Caption = caption,
                Message = message,
                LeftButtonContent = leftButtonContent,
                RightButtonContent = rightButtonContent

            };

         

            messageBox.Dismissed += new EventHandler<DismissedEventArgs>(DoFlagToastClicked);
            messageBox.Show();
#endif

        }
        #if WP_80_SILVERLIGHT
        private async void DoFlagToastClicked(Object obj, DismissedEventArgs e)
        {
            
            switch (e.Result)
            {
                case CustomMessageBoxResult.LeftButton:
                    {
                        foreach (MessageManager msg in msgManagerListForFlagging)
                        {
                            if (mFlagChange == 0)
                            {
                                if (msg.flag == 0)
                                {
                                    msg.flag = 1;
                                }
                                else
                                    msg.flag = 0;
                            }
                            else
                            {
                                msg.flag = 1;
                            }

                        }
                        mFlagChange = 0;

                        //PK: We have correct folder here but not the sync key as we have assigned the folder before sync happens
                        //So we need to update the sync key of folder. Just get the updated folder info from DB.
                        FolderManager folderManager = new FolderManager();
                        //Flagged folder is an opaque folder, so it doesn't exists in Folder table. Here i am getting the folder id from flagged message and will sync that folder
                        if (mCurrentVisibleMailFolder.displayName.ToLower() == "flagged")
                        {
                            int folderID = msgManagerListForFlagging[0].folderId;
                            mCurrentVisibleMailFolder = folderManager.GetFolderInfoAsync(folderID).Result.FirstOrDefault();
                        }
                        else
                        {
                            mCurrentVisibleMailFolder = await folderManager.GetFolderInfoAsync(mCurrentVisibleMailFolder.displayName, mCurrentVisibleMailFolder.parentFolder_id);
                        }
                        bool changeDone = await ActiveSync.GetInstance().SyncMailClientChanges(mCurrentVisibleMailFolder, ServerSyncCommand.ServerSyncCommandType.Change, msgManagerListForFlagging);

                        if (App.CheckInternetConnectivity())
                        {
                            ToastNotification toast = new ToastNotification();
                            if (changeDone)
                            {
                                toast.CreateToastNotificationForVertical("Success", "Flagging/Unflagging Done");
                                // We are in multi-select, so get out of multi-select mode.
                                SetEmailMultiSelectionCaps(false);
                            }
                            else
                            {
                                SetEmailMultiSelectionCaps(false);
                                // toast.CreateToastNotificationForVertical("Failure", "Flagging/Unflagging Error");
                            }
                        }
                        else
                        {
                            #region Write to messageTable
                            List<workspace_datastore.models.Message> msgsToUpdate = new List<workspace_datastore.models.Message>();
                            foreach (MessageManager msgManager in msgManagerListForFlagging)
                            {
                                workspace_datastore.models.Message msg = msgManager.GetMessageInfoAsync(msgManager.serverID).Result.ElementAt(0);
                                msg.read = msgManager.read;
                                msg.flag = msgManager.flag;
                                msg.flagStatus = msgManager.flagStatus;
                                msg.flagStartDate = msgManager.flagStartDate;
                                msg.flagUTCStartDate = msgManager.flagUTCStartDate;
                                msg.flagDueDate = msgManager.flagDueDate;
                                msg.flagUTCDueDate = msgManager.flagUTCDueDate;
                                msgsToUpdate.Add(msg);
                            }

                            MessageManager meesageMangerWritetoLocal = new MessageManager();
                            meesageMangerWritetoLocal.UpdateAllMessagesInfoAsync(msgsToUpdate);
                            #endregion
                            Message_UpdateManager messageUpdateManager = new Message_UpdateManager();
                            List<Message_Updates> mj = messageUpdateManager.GetUpdatedMsgInfoAsync(0).Result;

                            ToastNotification toast = new ToastNotification();
                            toast.CreateToastNotificationForVertical("Success", "Flagging/Unflagging Done");
                            //  We are in multi-select, so get out of multi-select mode.
                            SetEmailMultiSelectionCaps(false);
                        }
                    }
                    break;
                case CustomMessageBoxResult.RightButton:
                    mFlagChange = 0; break;
                case CustomMessageBoxResult.None:
                default:
                    break;
            }
           
        }
#endif
#if meetingSubject
        /// <summary>
        /// Detects change in network availability.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="NetworkNotificationEventArgs"/> instance containing the event data.</param>
        private void ChangeDetected(object sender, NetworkNotificationEventArgs e)
        {
            string change = string.Empty;
            switch (e.NotificationType)
            {
                case NetworkNotificationType.InterfaceConnected:
                    change = "Connected to ";
                    break;
                case NetworkNotificationType.InterfaceDisconnected:
                    change = "Disconnected from ";
                    break;
                default:
                    // change = "Unknown change with ";
                    break;
            }

            string changeInformation = String.Format(" {0} {1} {2} ({3})",
                        DateTime.Now.ToString(), change, e.NetworkInterface.InterfaceName,
                        e.NetworkInterface.InterfaceType.ToString());
            LoggingWP8.WP8Logger.LogMessage("Connection Log:" + changeInformation);
            // We are making UI updates, so make sure these happen on the UI thread.
            Message_UpdateManager messageUpdateManager = new Message_UpdateManager();
            List<Message_Updates> messageUpdates = messageUpdateManager.GetUpdatedMsgInfoAsync(0).Result;

        #region Create List of message Manager
            List<MessageManager> itemsToUpdate = new List<MessageManager>();
            foreach (Message_Updates msgUp in messageUpdates)
            {
                MessageManager myMessManager = new MessageManager();
                myMessManager.folderId = msgUp.folder_id;
                myMessManager.messgaeId = msgUp.id;
                myMessManager.read = msgUp.read;
                myMessManager.flag = msgUp.flag;
                myMessManager.serverID = msgUp.serverIdentifier;
                myMessManager.flagStatus = msgUp.flagStatus;
                myMessManager.flagStartDate = (float)msgUp.flagStartDate;
                myMessManager.flagDueDate = (float)msgUp.flagDueDate;
                myMessManager.flagUTCStartDate = (float)msgUp.flagUTCStartDate;
                myMessManager.flagDueDate = (float)msgUp.flagUTCDueDate;
                myMessManager.flagReminderSet = msgUp.flagReminderSet;
                itemsToUpdate.Add(myMessManager);
            }

            #endregion

            if (checkConnection())
            {
                FolderManager folderManager = new FolderManager();
                mCurrentVisibleMailFolder = folderManager.GetFolderInfoAsync("Inbox", 0).Result;

                bool changeDone = ActiveSync.GetInstance().SyncMailClientChanges(mCurrentVisibleMailFolder, ServerSyncCommand.ServerSyncCommandType.Change, itemsToUpdate).Result;
                if (changeDone)
                {
                    Message_UpdateManager myu = new Message_UpdateManager();
                    List<Message_Updates> mj = myu.GetUpdatedMsgInfoAsync(0).Result;
                    myu.DeleteAllChangedMessagesAsync();
                }
                else
                {
                    // TODO: what if changedone returns false, do i try again or some message 
                }
            }

        }
#endif

        async void ChangeDetected(object sender, NetworkChangedEventArgs e)
        {

            if (e.IsOnline)
            {
                //LoggingWP8.WP8Logger.LogMessage("Connection Log:" + changeInformation);
                // We are making UI updates, so make sure these happen on the UI thread.
                //changes of message
                Message_UpdateManager messageUpdateManager = new Message_UpdateManager();
                List<Message_Updates> messageUpdates = messageUpdateManager.GetUpdatedMsgInfoAsync(0).Result;

                #region Create List of message Manager
                List<MessageManager> itemsToUpdate = new List<MessageManager>();
                foreach (Message_Updates msgUp in messageUpdates)
                {
                    MessageManager myMessManager = new MessageManager();
                    myMessManager.folderId = msgUp.folder_id;
                    myMessManager.messageId = msgUp.id;
                    myMessManager.read = msgUp.read;
                    myMessManager.flag = msgUp.flag;
                    myMessManager.serverID = msgUp.serverIdentifier;
                    myMessManager.flagStatus = msgUp.flagStatus;
                    myMessManager.flagStartDate = msgUp.flagStartDate;
                    myMessManager.flagDueDate = msgUp.flagDueDate;
                    myMessManager.flagUTCStartDate = msgUp.flagUTCStartDate;
                    myMessManager.flagDueDate = msgUp.flagUTCDueDate;
                    myMessManager.flagReminderSet = msgUp.flagReminderSet;
                    itemsToUpdate.Add(myMessManager);
                }

                #endregion

                if (App.CheckInternetConnectivity())
                {
                    //Update
                    FolderManager folderManager = new FolderManager();
                    mCurrentVisibleMailFolder = await folderManager.GetFolderInfoAsync(mCurrentVisibleMailFolder.displayName, mCurrentVisibleMailFolder.parentFolder_id);

                    if (itemsToUpdate.Count > 0)
                    {
                        bool changeDone = await ActiveSync.GetInstance().SyncMailClientChanges(mCurrentVisibleMailFolder, ServerSyncCommand.ServerSyncCommandType.Change, itemsToUpdate);
                        if (changeDone)
                        {
                            Message_UpdateManager myu = new Message_UpdateManager();
                            List<Message_Updates> mj = myu.GetUpdatedMsgInfoAsync(0).Result;
                            myu.DeleteAllChangedMessagesAsync();
                        }
                        else
                        {
                            // TODO: what if changedone returns false, do i try again or some message 
                        }
                    }

                    //Delete

                    Message_DeleteManager messageDeleteManager = new Message_DeleteManager();
                    List<Message_Deletes> messageDeletes = messageDeleteManager.GetDeletedMsgInfoAsync(0).Result;

                    List<MessageManager> itemsToDelete = new List<MessageManager>();
                    List<ASDeleteItemsRequest> deleteItems = new List<ASDeleteItemsRequest>();

                    foreach (Message_Deletes msgDelete in messageDeletes)
                    {
                        ASDeleteItemsRequest objDeleteItemsRequest = new ASDeleteItemsRequest();
                        objDeleteItemsRequest.serverId = msgDelete.serverIdentifier;
                        deleteItems.Add(objDeleteItemsRequest);
                    }

                    if (deleteItems.Count > 0)
                    {
                        ASDeleteItemsResponse deleteResponse = ActiveSync.GetInstance().DeleteEmaiItems(mCurrentVisibleMailFolder.displayName, mCurrentVisibleMailFolder.parentFolder_id, deleteItems).Result;
                        if (deleteResponse != null)
                        {
                            List<Message_Deletes> message = messageDeleteManager.GetDeletedMsgInfoAsync(0).Result;
                            messageDeleteManager.DeleteAllToDeleteMessagesAsync();
                        }
                    }

                    //Here those contacts will be synced which are changed/deleted in offline mode
                    //Get dirty contacts first

                    ContactManager contactsManager = new ContactManager();
                    List<Contacts> dirtyContactsList = contactsManager.GetDirtyContactsListAsync().Result;
                    if (dirtyContactsList.Count > 0)
                    {

                        List<workspace_datastore.models.Folder> folderListDB = folderManager.GetFolderInfoAsync(0).Result;
                        //Creating a ASPCL Inboxfolder, for second time Sync
                        var folderDBInbox = folderListDB.Find(x => x.displayName == "Contacts" && x.parentFolder_id == 0);
                        ActiveSyncPCL.Folder.FolderType folderType = (ActiveSyncPCL.Folder.FolderType)folderDBInbox.folderType;
                        ActiveSyncPCL.Folder asContactFolder = new ActiveSyncPCL.Folder(folderDBInbox.displayName, folderDBInbox.serverIdentifier, folderType, null);
                        if (folderDBInbox.syncKey != "1")
                        {
                            asContactFolder.SyncKey = folderDBInbox.syncKey;
                        }

                        //Added contacts
                        List<Contacts> addedContacts = dirtyContactsList.FindAll(x => x.serverId == null) as List<Contacts>;
                        if (addedContacts.Count > 0)
                            await ActiveSync.GetInstance().DoContactChangesFromClient(asContactFolder, ActiveSyncPCL.EnumContactCommandType.Add, addedContacts);

                        //edited contacts
                        List<Contacts> editedContacts = dirtyContactsList.FindAll(x => x.serverId != string.Empty && x.deleted == 0 && x.dirty == 1) as List<Contacts>;
                        if (editedContacts.Count > 0)
                            await ActiveSync.GetInstance().DoContactChangesFromClient(asContactFolder, ActiveSyncPCL.EnumContactCommandType.Change, editedContacts);

                        //deleted contacts
                        List<Contacts> deletedContacts = dirtyContactsList.FindAll(x => x.serverId != string.Empty && x.deleted == 1 && x.dirty == 1) as List<Contacts>;
                        if (deletedContacts.Count > 0)
                            await ActiveSync.GetInstance().DoContactChangesFromClient(asContactFolder, ActiveSyncPCL.EnumContactCommandType.Delete, deletedContacts);
                    }

                }

            }
        }
        private void changefolder_Click(object sender, RoutedEventArgs e)
        {
			//NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceEmailWP8/View/MailFolders.xaml?movefolder=true", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("MailFolders");
        }

        private void ApplicationBarMenuItem_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceCalendarWP8/View/CalenderSettings.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("CalenderSettings");
        }

        private void TextBlock_Tap(object sender, TappedRoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceCalendarWP8/View/DayView.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("DayView");
        }

        private void CreateNewContacts_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceContactsWP8/View/AddContactOrEditView.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("AddContactOrEditView");
        }

        private void DayView_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceCalendarWP8/View/DayView.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("DayView");
            ApplicationDataContainer ApplicationSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            ApplicationSettings.Values["MyName"] = 0;


            //IsolatedStorageSettings.ApplicationSettings["MyName"] = 0;
        }

        private void ViewAll_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceContactsWP8/ContactLists.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("ContactLists");
        }

        //Navigation form Appbar UserControl

        //void Navigation(object sender, EventArgs e)
        //{
        //    NavigationService.Navigate(((NavigationEventArgs)e).Uri);
        //    pop.Visibility = Visibility.Collapsed;
        //}
        //Not in Use
        //private void Navigate_WEB(object sender, GestureEventArgs e)
        //{
        //    string URI = "/WorkspaceWP81;component/WorkspaceWebBrowserWP8/BrowserView.xaml?msg=" + list[list.Count - 1];
        //    //NavigationService.Navigate(new Uri(URI, UriKind.Relative));

        //    Common.Navigation.NavigationService.NavigateToPage("BrowserView", list[list.Count - 1]);
        //}

        //private void MonthModeIconButton_Click(object sender, EventArgs e)
        //{
        //    string URI = "/WorkspaceWP81;component/WorkspaceCalendarWP8/View/MonthView.xaml";
        //    NavigationService.Navigate(new Uri(URI, UriKind.Relative));
        //}

        //search button on calendar page

        private void SearchIconButton_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceCalendarWP8/View/SearchPage.xaml", UriKind.Relative));
            Common.Navigation.NavigationService.NavigateToPage("SearchPage");
        }
        private void recentFaviconList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListView recentFaviconList = FindChildControl<ListView>(calendar, "recentFaviconList") as ListView;
            var item = recentFaviconList.SelectedItem as Favicons;
            //NavigationService.Navigate(new Uri("/WorkspaceWP81;component/WorkspaceWebBrowserWP8/BrowserView.xaml?msg=" + item.siteUrl, UriKind.Relative));

            Common.Navigation.NavigationService.NavigateToPage("BrowserView", item.siteUrl);
        }

        private void RecentContactList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListView longListSelector = sender as ListView;
            if (longListSelector != null && longListSelector.SelectedItem != null)
            {
                Contacts mySelectedItem = (Contacts)longListSelector.SelectedItem;
                SuspensionManager.SessionState["SelectedContactForDetailScreen"] = mySelectedItem;
                Common.Navigation.NavigationService.NavigateToPage("ContactsDetailsView");
                //NavigationService.na(new Uri("/WorkspaceWP81;component/WorkspaceContactsWP8/View/ContactsDetailsView.xaml", UriKind.Relative));
                longListSelector.SelectedItem = null; // Reset
            }
        }

        //Commenting as it will be done from App.xaml.cs
        //public bool checkConnection()
        //{
        //    var networkInterface = NetworkInterface.GetIsNetworkAvailable();
           

        //    bool isConnected = false;
        //    if ((networkInterface == NetworkInterfaceType.Wireless80211) || (networkInterface == NetworkInterfaceType.MobileBroadbandGsm) || (networkInterface == NetworkInterfaceType.MobileBroadbandCdma))
        //        isConnected = true;
        //    else if (networkInterface == NetworkInterfaceType.None)
        //        isConnected = false;
        //    return isConnected;
        //}
     
        //private void emailScreen_LayoutUpdated(object sender, EventArgs e)
        //{
        //    var vm = (EmailItemsViewModel)emailScreen.DataContext;
        //    var emailCount = vm.LiveListItems;
        //    if (emailCount.Items.Count > 0)
        //    {
        //        MailText.Visibility = Visibility.Collapsed;
        //        InstructionText.Visibility = Visibility.Collapsed;
        //    }
        //}

        /// <summary>
        /// Formats the size of file in bytes to kb mb etc.
        /// </summary>
        /// <param name="fileSize">Size of the file.</param>
        /// <returns></returns>
        public String FormatedSize(long fileSize)
        {
            string[] sizes = { "b", "kb", "mb", "gb" };
            int order = 0;
            while (fileSize >= 1024 && order + 1 < sizes.Length)
            {
                order++;
                fileSize = fileSize / 1024;
            }

            string result = String.Format("{0:0.##}{1}", fileSize, sizes[order]);
            return result;
        }

	    private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
	    {
		    throw new NotImplementedException();
	    }

        private void Mail_Loaded(object sender, RoutedEventArgs e)
        {
            EmailItemsView emailScreen = FindChildControl<EmailItemsView>(Mail, "emailScreen") as EmailItemsView;
            mSyncBGWorker = BackgroundWorkerForSync.GetInstance(emailScreen);
            //Setting panorama object in BGWorker for proper calls and DB insertion
            mSyncBGWorker.SetScreenObject(this);
            mSyncBGWorker.InitializeAndDoSync("INITIALSYNC");
        }
    }
    //public class Files
    //{

    //    public string filepath { get; set; }
    //    public string filename { get; set; }
    //    public int filesize { get; set; }
    //}

    public class Favicons
    {
        public Uri faviconPath { get; set; }
        public string siteUrl { get; set; }
        public string title { get; set; }
    }
}
