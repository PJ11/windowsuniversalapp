﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Common;

namespace WorkspaceWP81.AppHub_View
{
    public partial class AppHubView : Page
    {
        protected DKCNAppManager appmanager = new DKCNAppManager();
        private Button lastButton = null;
        public AppHubView()
        {
            InitializeComponent();
            EnableComponentButton();
        }
        private void EnableComponentButton()
        {
            bool IsMailEnabled = appmanager.MailAppGranted;
            bool IsCalendarEnabled = appmanager.CalendarAppGranted;
            bool IsContactsEnabled = appmanager.ContactsAppGranted;
            bool IsFilesEnabled = appmanager.FilesAppGranted;
            bool IsBrowserEnabled =appmanager.BrowerAppGranted;
            if (IsMailEnabled)
            {
                MailButton.IsEnabled = true;
                MailButton.Visibility = Visibility.Visible;
            }
            if (IsCalendarEnabled)
            {
                CalendarButton.IsEnabled = true;
                CalendarButton.Visibility = Visibility.Visible;
            }
            if (IsContactsEnabled)
            {
                ContactsButton.IsEnabled = true;
                ContactsButton.Visibility = Visibility.Visible;
            }
            if (IsFilesEnabled)
            {
                FilesButton.IsEnabled = true;
                FilesButton.Visibility = Visibility.Visible;
            }
            if (IsBrowserEnabled)
            {
                BrowserButton.IsEnabled = true;
                BrowserButton.Visibility = Visibility.Visible;
            }
            
        }
          
        private void mailbutton_click(object sender, RoutedEventArgs e)
        {
            if (lastButton != null)
            {
                lastButton.BorderBrush = new SolidColorBrush(Colors.Gray); ;// Brushes.LightGray;
            }
            Button clickedButton = (Button)sender;
            MailButton.BorderBrush = new SolidColorBrush(Colors.Blue);
            /*---TBD---------Live tile to display number of unread Mails-----------------------*/

            MailButton.BorderBrush = new SolidColorBrush(Colors.Blue);
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_menu_mail_active.png"));
            MailImage.Source = brush.ImageSource;
            lastButton = clickedButton;
        }
        private void MailButton_MouseEnter(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_menu_mail_active.png"));
            MailImage.Source = brush.ImageSource;
        }

        private void MailButton_MouseLeave(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_menu_mail_normal.png"));
            MailImage.Source = brush.ImageSource;
        }
        private void contactbutton_click(object sender, RoutedEventArgs e)
        {
            if (lastButton != null)
            {
                lastButton.BorderBrush = new SolidColorBrush(Colors.Gray); ;// Brushes.LightGray;
            }
            Button clickedButton = (Button)sender;
            ContactsButton.BorderBrush = new SolidColorBrush(Colors.Blue);
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_menu_contact_active.png"));
            ContactImage.Source = brush.ImageSource;
            lastButton = clickedButton;

        }
        private void contactbutton_MouseEnter(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_contact_mail_active.png"));
            ContactImage.Source = brush.ImageSource;
        }

        private void contactbutton_MouseLeave(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_contact_mail_normal.png"));
            ContactImage.Source = brush.ImageSource;
        }
        private void calendarbutton_click(object sender, RoutedEventArgs e)
        {
            if (lastButton != null)
            {
                lastButton.BorderBrush = new SolidColorBrush(Colors.Gray); ;// Brushes.LightGray;
            }
            Button clickedButton = (Button)sender;
            CalendarButton.BorderBrush = new SolidColorBrush(Colors.Blue);
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_menu_calendar_active.png"));
            CalendarImage.Source = brush.ImageSource;
            lastButton = clickedButton;
        }
        private void calendarbutton_MouseEnter(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_calendar_mail_active.png"));
            ContactImage.Source = brush.ImageSource;
        }

        private void calendarbutton_MouseLeave(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_calendar_mail_normal.png"));
            CalendarImage.Source = brush.ImageSource;
        }
        private void filesbutton_click(object sender, RoutedEventArgs e)
        {
            if (lastButton != null)
            {
                lastButton.BorderBrush = new SolidColorBrush(Colors.Gray); 
            }
            Button clickedButton = (Button)sender;
            FilesButton.BorderBrush = new SolidColorBrush(Colors.Blue);
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_menu_file_active.png"));
            FilesImage.Source = brush.ImageSource;
            lastButton = clickedButton;
        }
        private void filesbutton_MouseEnter(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_file_mail_active.png"));
            FilesImage.Source = brush.ImageSource;
        }

        private void filesbutton_MouseLeave(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_file_mail_normal.png"));
            FilesImage.Source = brush.ImageSource;
        }
        private void browserbutton_click(object sender, RoutedEventArgs e)
        {
            if (lastButton != null)
            {
                lastButton.BorderBrush = new SolidColorBrush(Colors.Gray); ;// Brushes.LightGray;
            }
            Button clickedButton = (Button)sender;
            BrowserButton.BorderBrush = new SolidColorBrush(Colors.Blue);
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_menu_browser_active.png"));
            BrowserImage.Source = brush.ImageSource;
            lastButton = clickedButton;
        }
        private void browserbutton_MouseEnter(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_browser_mail_active.png"));
            BrowserImage.Source = brush.ImageSource;
        }

        private void browserbutton_MouseLeave(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            var brush = new ImageBrush();
            brush.ImageSource = new BitmapImage(new Uri("Assets/CM_Assets/Phone/ic_browser_mail_normal.png"));
            BrowserImage.Source = brush.ImageSource;
        }

    }

}