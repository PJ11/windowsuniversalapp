﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using PCLStorage;
using System.Runtime.InteropServices.WindowsRuntime;

namespace LoggingWP8
{
    public class WP8Logger
    {
        private static WP8Logger _inst = null;
        protected Queue<string> messageQueue = new Queue<string>();
        private readonly static object syncLock = new object();

        private bool bExit = false;
        private IFile mFile = null;
        public static bool IsLogEnabled = true;

        protected WP8Logger()
        {
            Task.Run(() =>
                {
                    string item = null;
                    bool result = initLoggerFile();
                    if (result)
                    {
                        using (Stream stream = mFile.OpenAsync(PCLStorage.FileAccess.ReadAndWrite).Result)
                        {
                            //  Read and write stream
                            using (StreamWriter writer = new StreamWriter(stream, Encoding.UTF8))
                            {
                                do
                                {
                                    try
                                    {
                                        lock (messageQueue)
                                        {
                                            if (messageQueue.Count > 0)
                                            {
                                                item = Dequeue();
                                            }
                                            else
                                            {
                                                item = null;
                                            }
                                        }
                                        if (null != item)
                                        {
                                            printMessage(item, writer);
                                            item = null;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        // Any exception, just keep going (I think :))

                                    }
                                    if (bExit)
                                    {
                                        break;
                                    }

                                } while (true);
                            }
                        }
                    }
                });
        }

        private bool initLoggerFile()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;

            if(null == mFile)
            {
                try
                {
                    IFolder folder = rootFolder.CreateFolderAsync("Logger", CreationCollisionOption.OpenIfExists).Result;
                    mFile = folder.CreateFileAsync("Workspace.log", CreationCollisionOption.ReplaceExisting).Result;
                    return true;

                }
                catch(Exception e)
                {
                    mFile = null;
                    return false;
                }
            }
            return true;
        }

        protected void printMessage(string message, StreamWriter writer)
        {
            // Output to debug log
            System.Diagnostics.Debug.WriteLine(message);
            try
            {
                writer.Write(message);
                writer.Flush();
            }
            catch(ObjectDisposedException e)
            {

            }
            catch(NotSupportedException e)
            {

            }
            catch(IOException e)
            {

            }
        }

        private void wait(int time)
        {
            try
            {
                //Monitor.Wait(mThread, time);
                Monitor.Wait(messageQueue, time);
            }
            catch (System.Threading.SynchronizationLockException e)
            {
                string message = e.Message;
            }
        }
        
        protected void EnqueueItem(string item)
        {
            // Request the lock, and block until it is obtained.
            Monitor.Enter(messageQueue);
            try
            {
                // When the lock is obtained, add an element.
                messageQueue.Enqueue(item);
            }
            finally
            {
                // Ensure that the lock is released.
                Monitor.Exit(messageQueue);
            }
        }

        protected bool TryEnqueue(string item)
        {
            // Request the lock.
            if (Monitor.TryEnter(messageQueue))
            {
                try
                {
                    messageQueue.Enqueue(item);
                }
                finally
                {
                    // Ensure that the lock is released.
                    Monitor.Exit(messageQueue);
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        protected bool TryEnqueue(string item, int waitTime)
        {
            // Request the lock.
            if (Monitor.TryEnter(messageQueue, waitTime))
            {
                try
                {
                    messageQueue.Enqueue(item);
                }
                finally
                {
                    // Ensure that the lock is released.
                    Monitor.Exit(messageQueue);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        // Lock the queue and dequeue an element.
        protected string Dequeue()
        {
            string retval;

            // Request the lock, and block until it is obtained.
            Monitor.Enter(messageQueue);
            try
            {
                // When the lock is obtained, dequeue an element.
                retval = messageQueue.Dequeue();
            }
            finally
            {
                // Ensure that the lock is released.
                Monitor.Exit(messageQueue);
            }

            return retval;
        }

        // Delete all elements that equal the given object.
        protected int Remove(string item)
        {
            int removedCt = 0;

            // Wait until the lock is available and lock the queue.
            Monitor.Enter(messageQueue);
            try
            {
                int counter = messageQueue.Count;
                while (counter > 0)
                //Check each element.
                {
                    string elem = messageQueue.Dequeue();
                    if (!elem.Equals(item))
                    {
                        messageQueue.Enqueue(elem);
                    }
                    else
                    {
                        // Keep a count of items removed.
                        removedCt += 1;
                    }
                    counter = counter - 1;
                }
            }
            finally
            {
                // Ensure that the lock is released.
                Monitor.Exit(messageQueue);
            }

            return removedCt;
        }

        public static void LogMessage(string message, params object[] args)
        {
            if (IsLogEnabled)
            {
                if (null == _inst)
                {
                    _inst = new WP8Logger();
                }

                string newMessage = DateTime.Now.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern) + ":" + DateTime.Now.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern) + "->";
                try
                {
                    if (args.Length == 0)
                    {
                        newMessage += message + Environment.NewLine;
                    }
                    else
                    {
                        string newMsg = String.Format(message, args);
                        newMessage += newMsg + Environment.NewLine;
                    }

                    _inst.EnqueueItem(newMessage);
                }
                catch (Exception)
                {
                    // Not sure what exceptions can arise here

                }
                // Nothing was added
            }
        }
    }
}
