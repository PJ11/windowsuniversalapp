﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using workspace_datastore.Managers;
using workspace_datastore.models;
using workspace_datastore.Models;

namespace workspace_datastore.Helpers
{
   public class AddingRecentContactHelper
    {

        public async void AddContactDetail(string email)
        {
            var objEmailSearch = new EmailSearch();
            var objEmailSearchManager = new EmailSearchManager();          
            DataSourceAsync dsa = new DataSourceAsync();
            try
            {
                var existContactInfo = await dsa.Db.Table<Contacts>().Where(c => c.email1Address == email || c.email2Address == email || c.email3Address == email).ToListAsync().ConfigureAwait(false);
                if (existContactInfo!=null && existContactInfo.Count != 0)
                {
                    foreach (Contacts s in existContactInfo)
                    {
                        var contact = s;
                        objEmailSearch.first_name = s.firstName;
                        objEmailSearch.last_name = s.lastName;
                        objEmailSearch.recent_timestamp = DateTime.Now;
                        objEmailSearch.email = email;
                        objEmailSearch.linker_id = s.id;
                        await objEmailSearchManager.AddOrUpdateEmailSearchContactInfoAsync(dsa.Db, objEmailSearch);
                    }
                }
                else
                {
                    objEmailSearch.email = email;
                    await objEmailSearchManager.AddOrUpdateEmailSearchContactInfoAsync(dsa.Db, objEmailSearch);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsa.Dispose();
            }
        }


        public async void AddPhoneContactDetail(Contacts Con)
        {
            var objPhoneSearch = new PhoneSearch();
            var objPhoneSearchManager = new PhoneSearchManager();          
            DataSourceAsync dsa = new DataSourceAsync();
            try
            {
                var existContactInfo = await dsa.Db.Table<Contacts>().ToListAsync();   //.Where(c => Con).ToListAsync().ConfigureAwait(false);//c.assistantPhoneNumber == number || c.business2PhoneNumber == number || c.businessPhoneNumber == number || c.home2PhoneNumber == number || c.homePhoneNumber == number).ToListAsync().ConfigureAwait(false);
                if (existContactInfo!=null && existContactInfo.Count != 0)
                {
                    if(existContactInfo.Any(cus => cus.id == Con.id))
                    {
                        objPhoneSearch.first_name = Con.firstName;
                        objPhoneSearch.last_name = Con.lastName;
                        objPhoneSearch.recent_timestamp = DateTime.Now;
                       // objPhoneSearch.phone_number = number;
                        objPhoneSearch.linker_id = Con.id;
                        if (Con.assistantPhoneNumber != "")
                           objPhoneSearch.phone_number = Con.assistantPhoneNumber;
                        if (Con.business2PhoneNumber != "")
                            objPhoneSearch.phone_number = Con.business2PhoneNumber;
                        if (Con.businessPhoneNumber != "")
                            objPhoneSearch.phone_number = Con.businessPhoneNumber;
                        if (Con.home2PhoneNumber != "")
                            objPhoneSearch.phone_number = Con.home2PhoneNumber;
                        if (Con.homePhoneNumber != "")
                            objPhoneSearch.phone_number = Con.homePhoneNumber;
                        if (Con.mobilePhoneNumber != "")
                            objPhoneSearch.phone_number = Con.mobilePhoneNumber;  
                        await objPhoneSearchManager.AddOrUpdatePhoneSearchContactInfoAsync(dsa.Db, objPhoneSearch);
                    }
                }
                //else
                //{
                //    objPhoneSearch.phone_number = number;
                //    await objPhoneSearchManager.AddOrUpdatePhoneSearchContactInfoAsync(dsa.Db, objPhoneSearch);
                //}
            }
            catch
            {
                throw;
            }

            finally
            {
                dsa.Dispose();
            }
        }
    }
}
