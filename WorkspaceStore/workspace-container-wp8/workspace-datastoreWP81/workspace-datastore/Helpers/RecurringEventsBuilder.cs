﻿using ActiveSyncPCL;
using System;
using System.Collections.Generic;
using System.Linq;
using workspace_datastore.models;

namespace workspace_datastore.Helpers
{
    public class TimeZoneInfoForEvent
    {
        DateTime mFirstOccurrence = DateTime.MinValue;

        public TimeZoneInfoForEvent(DateTime firstOccurrence)
        {
            mFirstOccurrence = firstOccurrence;
            SupportsDST = false;
            InstanceOccursDuringDST = false;
            InitialBias = 0;
            StandardBias = 0;
            DaylightBias = 0;
        }

        /// <summary>
        /// This Time Zone supports DST
        /// </summary>
        public bool SupportsDST { get; set; }

        /// <summary>
        /// The startDateAndTime of the first instance (if recurring) or the only instance (non-recurring)
        /// </summary>
        public bool InstanceOccursDuringDST { get; set; }

        public double InitialBias { get; set; }

        /// <summary>
        /// Minutes added (or subtracted) to InitialBias for Standard Time
        /// </summary>
        public double StandardBias { get; set; }

        /// <summary>
        /// Minutes added (or subtracted) to InitialBias for DST
        /// </summary>
        public double DaylightBias { get; set; }
    }

    /// <summary>
    /// Because EAS does not give us recurring events in the future,
    /// we must build the UI ourselves.
    /// </summary>
    public static class RecurringEventsBuilder
    {
        public static List<Events> GenerateWindowForFutureDays(int numOfDays, double oldestDay, List<Events> eventsAlreadyInBlock, List<Events> recurringEventDefinitions, Dictionary<Events, TimeZoneInfoForEvent> recurringEventDefinitions2 = null)
        {
            // Now fabricate 30 more days from today worth of recurring events
            DateTime startDate = DateTimeExtensions.FromOADate(oldestDay).Date;
            List<Events> allFutureEventsForNDays = new List<Events>();

            if (recurringEventDefinitions == null)
            {
                return allFutureEventsForNDays;
            }
            for (int dayCounter = 0; dayCounter < numOfDays; dayCounter++)
            {
                DateTime thisDay = startDate.AddDays(dayCounter);

                foreach (Events futureEvent in (null != recurringEventDefinitions2) ? recurringEventDefinitions2.Keys.ToList() : recurringEventDefinitions)
                {
                    CMRecurrenceType recurrenceType = (CMRecurrenceType)futureEvent.rcType;
                    CMDayOfWeekType daysOfWeekForThisEvent = (CMDayOfWeekType)futureEvent.rcDayOfWeek;
                    DateTime startDayAndTime = DateTimeExtensions.FromOADate(futureEvent.startDayAndTime);
                    DateTime endDayAndTime = DateTimeExtensions.FromOADate(futureEvent.endDayAndTime);
                    
                    TimeSpan eventStartTimeOfDay = startDayAndTime.TimeOfDay;
                    TimeSpan eventEndTimeOfDay = endDayAndTime.TimeOfDay;
                    TimeSpan numberOfDaysForEvent  = TimeSpan.Zero;
                    
                    //if(eventStartTimeOfDay == eventEndTimeOfDay)
                    {
                        // This is a multi-day event, so let's get how many days
                        numberOfDaysForEvent = endDayAndTime - startDayAndTime;
                    }
                    if(null != recurringEventDefinitions2)
                    {
                        TimeZoneInfoForEvent tzInfo = recurringEventDefinitions2[futureEvent];
                        if(null != tzInfo)
                        {
                            bool timeZoneOfEventIsInDST = tzInfo.InstanceOccursDuringDST && tzInfo.SupportsDST;

                            // It appears that Exchange ActiveSync (EAS) takes into account IF THE FIRST OCCURRENCE OCCURS IN DST. 
                            // So if the first occurrence occurs in Austin Texas (Central) on May 01, then the UTC will be +5
                            //  HOWEVER, if the first occurrence in Austin Texas (Central) on February 01, then the UTC will be +6
                            // This is the "problem" that the adjustedTimezoneBiasBasedOnEventCreation variable is trying to solve.
                            double adjustedTimezoneBiasBasedOnEventCreation = (timeZoneOfEventIsInDST) ? tzInfo.InitialBias - tzInfo.DaylightBias : tzInfo.InitialBias;

                            // This will be automatically adjusted for DST or Standard Time.
                            double currentTimeZoneMinuteFromUTC = TimeZoneInfo.Local.GetUtcOffset(thisDay).TotalMinutes;

                            // Does this day fall within DST?
                            bool thisDayIsInDST = TimeZoneInfo.Local.IsDaylightSavingTime(thisDay);
                            
                            if(thisDayIsInDST && !timeZoneOfEventIsInDST)
                            {
                                currentTimeZoneMinuteFromUTC -= 60; // TODO How do we get the daylight savings time offset?  I believe it's NOT ALWAYS 60 minutes
                            }
                            double timeZoneDelta = currentTimeZoneMinuteFromUTC - adjustedTimezoneBiasBasedOnEventCreation;

                            eventStartTimeOfDay = startDayAndTime.AddMinutes(adjustedTimezoneBiasBasedOnEventCreation + timeZoneDelta).TimeOfDay;
                            eventEndTimeOfDay = endDayAndTime.AddMinutes(adjustedTimezoneBiasBasedOnEventCreation + timeZoneDelta).TimeOfDay;
                        }
                    }

                    DateTime until = DateTime.MaxValue;
                    #region commented code
                    // PERMUTATIONS AND THEIR RESPECTIVE OPTIONS:
                    // 1. Repeat Daily: (# day(s) || {Monday - Friday })
                    //    a. "Recurring test meeting". 63 Occurences Starting 8/29/2014. (Monday-Friday)
                    //        1. rcDayOfMonth:0, rcDayOfWeek:62, rcFirstDayOfWeek:0, rcInterval:1, rcMonthOfYear:0, rcOccurrences:63, rcType:1, rcWeekOfMonth:0
                    //    b. "Daily every 3 days" 17 Occurrences
                    //        1. rcDayOfMonth:0, rcDayOfWeek:0, rcFirstDayOfWeek:0, rcInterval:3, rcMonthOfYear:0, rcOccurrences:17, rcType:1, rcWeekOfMonth:0
                    //
                    // 2. Repeat Weekly: (# week(s) on {Sunday - Saturday})
                    //    a. "Recurring test meeting every Tu/Th for 26 Occurences" starting 9/16. 
                    //        1. rcDayOfMonth:0, rcDayOfWeek:20, rcFirstDayOfWeek:0, rcInterval:1, rcMonthOfYear:0, rcOccurrences:26, rcType:1, rcWeekOfMonth:0
                    //    b. "First Day of Week recurring event (Every 2 weeks on Sunday)" starting 9/14 with 11 occurences. 2 weeks on Sunday
                    //        1. rcDayOfMonth: 0, rcDayOfWeek:1, rcFirstDayOfWeek:0, rcInterval:2, rcMonthOfYear:0, rcOccurrences:11, rcType:1, rcWeekOfMonth:0
                    //    c. "Week Of Month: Every 1 week on Friday" starting 9/12/2014 with 16 occurrences. 1 week on Friday.
                    //        1. rcDayOfMonth:0, rcDayOfWeek:32, rcFirstDayOfWeek:0, rcInterval:1, rcMonthOfYear:0, rcOccurrences:16, rcType:1, rcWeekOfMonth:0
                    //
                    // 3. Repeat Yearly: (Month/Day || {first second, third, fourth, last}, {weekday, weekend, Sunday-Saturday} of {Jan-Dec})
                    //    a. "Yearly: The fourth Friday Of September 3 occurences" with 3 occurrences
                    //        1. rcDayOfMonth:0, rcDayOfWeek:32, rcFirstDayOfWeek:0, rcInterval:1, rcMonthOfYear:9, rcOccurrences:3, rcType:6, rcWeekOfMonth:4
                    //    b. "Yearly: October 23rd with 5 occurrences (Month/Day)
                    //        1. rcDayOfMonth:23, rcDayOfWeek:0, rcFirstDayOfWeek:0, rcInterval:1, rcMonthOfYear:10, rcOccurrences:5, rcType:5, rcWeekOfMonth:0
                    //
                    // 4. Repeat Monthly: (11th day of every # month(s) || {first, second, third, fourth, last} {weekday, weekend, Sunday-Saturday} of every # month(s))
                    //    a. "Month of Year: 3rd Monday of Every 3 Months".  33 Occurrences. Third Monday of every 3 Months
                    //        1. rcDayOfMonth:0, rcDayOfWeek:2, rcFirstDayOfWeek:0, rcInterval:3, rcMonthOfYear:0, rcOccurrences:33, rcType:3, rcWeekOfMonth:3
                    //
                    // 5. Repeat Monthly: (Day # of every # months || {first second, third, fourth, last}, {weekday, weekend, Sunday-Saturday} of every # months)
                    //    a. "Every 2 months on the 15th Day" with 7 occurrences. (Day 15 of every 2 months)
                    //        1. rcDayOfMonth:15, rcDayOfWeek:0, rcFirstDayOfWeek:0, rcInterval:2, rcMonthOfYear:0, rcOccurrences:7, rcType:2, rcWeekOfMonth:0
                    //
                    // 6. Repeat Monthly: (Day # of every # months || {first second, third, fourth, last}, {weekday, weekend, Sunday-Saturday} of every # months)
                    //    a. "Monthly: The third Thursday of every 1 month" with 10 occurrences. (Third Thursday of every 1 month)
                    //        1. rcDayOfMonth:0, rcDayOfWeek:16, rcFirstDayOfWeek:0, rcInterval:1, rcMonthOfYear:0, rcOccurrences:10, rcType:3, rcWeekOfMonth:3


                    // First thing to do is if Occurences is > 0, then calculate an "Until", because one is not supplied
                    // Ultimately we need to determine whether or not this particular event is still active during the
                    // date range that's visible on the screen.
                    #endregion

                    #region Set end date
                    if (futureEvent.rcOccurences > 0)
                    {
                        if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeYearly ||
                            recurrenceType == CMRecurrenceType.CMRecurrenceTypeYearlyNDay)
                        {
                            //   CAN ONLY HAVE AT MOST 1 EVENT PER YEAR
                            //   THE START DATE DAY NUMBER IS ALWAYS THE SAME
                            until = startDayAndTime.Date.AddYears(futureEvent.rcOccurences * futureEvent.rcInterval);
                        }
                        else if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeMonthly ||
                            recurrenceType == CMRecurrenceType.CMRecurrenceTypeMonthlyNDay)
                        {
                            // ASSUMPTIONS: 
                            //   CAN ONLY HAVE AT MOST 1 EVENT PER MONTH
                            //   THE START DATE DAY NUMBER IS ALWAYS THE SAME
                            //   rcMonthOfYear is ALWAYS 0. 
                            //   WHEN rcWeekOfMonth > 0: rcDayOfMonth is ALWAYS = 0, rcDayOfWeek is ALWAYS > 0. 
                            //   WHEN rcWeekOfMonth = 0: rcDayOfMonth is ALWAYS > 0, rcDayOfWeek is ALWAYS = 0.
                            int totalMeetingMonths = futureEvent.rcInterval * futureEvent.rcOccurences;
                            //int monthsleft = totalMeetingMonths / 12;
                            until = startDayAndTime.Date.AddMonths(totalMeetingMonths);
                        }
                        else if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeDaily ||
                            recurrenceType == CMRecurrenceType.CMRecurrenceTypeWeekly)
                        {
                            int totalDaysPerWeek = 0;
                            if (futureEvent.rcDayOfWeek == 0)
                            {
                                // All 7 days
                                totalDaysPerWeek = 7;
                            }
                            else
                            {
                                // ASSUMPTIONS: rcDayOfMonth is ALWAYS 0.  rcMonthOfYear is ALWAYS 0.  rcWeekOfMonth is ALWAYS 0
                                // Calculate how many days / week this event has
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeSunday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeMonday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeTuesday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeWednesday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeThursday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeFriday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeSaturday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                            }
                            // This is the date when this event is no longer valid.
                            //remove one as untill is days including starting day 
                            until = startDayAndTime.Date.AddDays(((((double)futureEvent.rcOccurences / totalDaysPerWeek)) * (futureEvent.rcInterval * 7)) - 1);
                        }
                    }
                    else if (futureEvent.rcUntil != 0)
                    {
                        until = DateTimeExtensions.FromOADate(futureEvent.rcUntil);
                    }
                    #endregion
                    
                    // If thisDay is before or the day of the final meeting
                    if (thisDay <= until)
                    {
                        Events copyOfFutureEvent = new Events();
                        copyOfFutureEvent.CopyToThis(futureEvent);                        
                        string newStartDate = Convert.ToSingle(thisDay.Date.Add(eventStartTimeOfDay).ToOADate()).ToString();
                        if (futureEvent.rcHiddenExceptions == null || !futureEvent.rcHiddenExceptions.Contains(newStartDate))
                        {
                            // TypeYearly = December 25 TypeYearlyNDay = 3rd Thursday of November 
                            if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeYearly ||
                                recurrenceType == CMRecurrenceType.CMRecurrenceTypeYearlyNDay)
                            {
                                // Determine if 'thisDay' is equal to the day in this 'futureEvent'
                                int dayNumber = futureEvent.rcDayOfMonth;
                                if (futureEvent.rcDayOfMonth == 0)
                                {
                                    var allWeeks = (from day in Enumerable.Range(1, DateTime.DaysInMonth(thisDay.Year, futureEvent.rcMonthOfYear))
                                                    where new DateTime(thisDay.Year, futureEvent.rcMonthOfYear, day).DayOfWeek == GetNETDayOfWeekType((CMDayOfWeekType)futureEvent.rcDayOfWeek)
                                                    select day).ToList();
                                    int weekOfMonth = futureEvent.rcWeekOfMonth;
                                    if (weekOfMonth > allWeeks.Count)
                                    {
                                        weekOfMonth = allWeeks.Count;
                                    }
                                    // Third Thursday of September for example.  Need to get a day number
                                    var eventDayVar = allWeeks.ElementAt(weekOfMonth - 1);
                                    dayNumber = eventDayVar;
                                }
                                DateTime tmp = new DateTime(thisDay.Year, futureEvent.rcMonthOfYear, dayNumber);
                                if (tmp.Year == thisDay.Year &&
                                    tmp.Month == thisDay.Month &&
                                    tmp.Day == thisDay.Day)
                                {
                                    if (startDayAndTime.Date <= thisDay.Date)
                                    {
                                        if(numberOfDaysForEvent.Days == 0)
                                        {
                                            DateTime newStartDateAndTime = thisDay.Add(eventStartTimeOfDay);
                                            DateTime newEndDateAndTime = thisDay.Add(eventEndTimeOfDay);

                                            copyOfFutureEvent.endDayAndTime = (newEndDateAndTime.ToOADate());
                                            copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                            if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                                allFutureEventsForNDays.Add(copyOfFutureEvent);
                                        }
                                        else
                                        {
                                            // Multi-Day event
                                            if (1 == copyOfFutureEvent.allDay)
                                            {
                                                for (int i = 0; i < numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);
                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.AddDays(i + 1).Add(eventEndTimeOfDay);

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                            else
                                            {
                                                for (int i = 0; i <= numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);

                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.Add(eventEndTimeOfDay);

                                                    if (i == 0)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i > 0)
                                                    {
                                                        newStartDateAndTime = startDayAndTime.Date.AddDays(i).Date;  // The subsequent days always start at midnight
                                                    }

                                                    if (i > 0 && i < numberOfDaysForEvent.Days)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i == numberOfDaysForEvent.Days)
                                                    {
                                                        // No need to touch the newEndDateAndTime variable
                                                    }

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // TypeMonthly = The 15th of every month, TypeMonthlyNDay = The 2nd Thursday of every month
                            else if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeMonthly ||
                                recurrenceType == CMRecurrenceType.CMRecurrenceTypeMonthlyNDay)
                            {
                                // Determine if 'thisDay' is equal to the day in this 'futureEvent'
                                int dayNumber = futureEvent.rcDayOfMonth;
                                if (futureEvent.rcDayOfMonth == 0)
                                {
                                    // Third Thursday for example.  Need to get a day number
                                    var eventDayVar = (from day in Enumerable.Range(1, DateTime.DaysInMonth(thisDay.Year, thisDay.Month))
                                                       where new DateTime(thisDay.Year, thisDay.Month, day).DayOfWeek == GetNETDayOfWeekType((CMDayOfWeekType)futureEvent.rcDayOfWeek)
                                                       select day);
                                    if (eventDayVar.Cast<int>().Count() == 4 && futureEvent.rcWeekOfMonth == 5)     //if fifth day do not exist in a month but recurrence is on last day of month, here day means "monday" tuesday etc
                                    {
                                        dayNumber = eventDayVar.ElementAt(3);                                     //assign last day availbale
                                    }
                                    else
                                    {
                                        dayNumber = eventDayVar.ElementAt(futureEvent.rcWeekOfMonth - 1);
                                    }
                                }
                                DateTime tmp = new DateTime(thisDay.Year, thisDay.Month, dayNumber);
                                if (tmp.Year == thisDay.Year &&
                                    tmp.Month == thisDay.Month &&
                                    tmp.Day == thisDay.Day)
                                {
                                    if (startDayAndTime.Date <= thisDay.Date)
                                    {
                                        if (numberOfDaysForEvent.Days == 0)
                                        {
                                            DateTime newStartDateAndTime = thisDay.Add(eventStartTimeOfDay);
                                            DateTime newEndDateAndTime = thisDay.Add(eventEndTimeOfDay);

                                            copyOfFutureEvent.endDayAndTime = (newEndDateAndTime.ToOADate());
                                            copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                            if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                                allFutureEventsForNDays.Add(copyOfFutureEvent);
                                        }
                                        else
                                        {
                                            // Multi-Day event
                                            if (1 == copyOfFutureEvent.allDay)
                                            {
                                                for (int i = 0; i < numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);
                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.AddDays(i + 1).Add(eventEndTimeOfDay);

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                            else
                                            {
                                                for (int i = 0; i <= numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);

                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.Add(eventEndTimeOfDay);

                                                    if (i == 0)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i > 0)
                                                    {
                                                        newStartDateAndTime = startDayAndTime.Date.AddDays(i).Date;  // The subsequent days always start at midnight
                                                    }

                                                    if (i > 0 && i < numberOfDaysForEvent.Days)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i == numberOfDaysForEvent.Days)
                                                    {
                                                        // No need to touch the newEndDateAndTime variable
                                                    }

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            // TypeDaily = everyday, or every Tue/Thur  TypeWeekly = every Thursday
                            else if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeDaily ||
                                recurrenceType == CMRecurrenceType.CMRecurrenceTypeWeekly)
                            {
                                // Daily meeting ALL 7 DAYS
                                if (copyOfFutureEvent.rcDayOfWeek == 0)
                                {
                                    if (startDayAndTime.Date <= thisDay.Date)
                                    {
                                        if (numberOfDaysForEvent.Days == 0)
                                        {
                                            DateTime newStartDateAndTime = thisDay.Add(eventStartTimeOfDay);
                                            DateTime newEndDateAndTime = thisDay.Add(eventEndTimeOfDay);

                                            copyOfFutureEvent.endDayAndTime = (newEndDateAndTime.ToOADate());
                                            copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                            if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                                allFutureEventsForNDays.Add(copyOfFutureEvent);
                                        }
                                        else
                                        {
                                            // Multi-Day event
                                            if (1 == copyOfFutureEvent.allDay)
                                            {
                                                for (int i = 0; i < numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);
                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.AddDays(i + 1).Add(eventEndTimeOfDay);

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                            else
                                            {
                                                for (int i = 0; i <= numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);

                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.Add(eventEndTimeOfDay);

                                                    if (i == 0)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i > 0)
                                                    {
                                                        newStartDateAndTime = startDayAndTime.Date.AddDays(i).Date;  // The subsequent days always start at midnight
                                                    }

                                                    if (i > 0 && i < numberOfDaysForEvent.Days)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i == numberOfDaysForEvent.Days)
                                                    {
                                                        // No need to touch the newEndDateAndTime variable
                                                    }

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                        }
                                    }
                                }
                                // Last day of the month for example
                                else if (thisDay.Day == DateTime.DaysInMonth(thisDay.Year, thisDay.Month) &&
                                    daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeLastDayofMonth))
                                {
                                    if (startDayAndTime.Date <= thisDay.Date)
                                    {
                                        if (numberOfDaysForEvent.Days == 0)
                                        {
                                            DateTime newStartDateAndTime = thisDay.Add(eventStartTimeOfDay);
                                            DateTime newEndDateAndTime = thisDay.Add(eventEndTimeOfDay);

                                            copyOfFutureEvent.endDayAndTime = (newEndDateAndTime.ToOADate());
                                            copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                            if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                                allFutureEventsForNDays.Add(copyOfFutureEvent);
                                        }
                                        else
                                        {
                                            // Multi-Day event
                                            if (1 == copyOfFutureEvent.allDay)
                                            {
                                                for (int i = 0; i < numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);
                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.AddDays(i + 1).Add(eventEndTimeOfDay);

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                            else
                                            {
                                                for (int i = 0; i <= numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);

                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.Add(eventEndTimeOfDay);

                                                    if (i == 0)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i > 0)
                                                    {
                                                        newStartDateAndTime = startDayAndTime.Date.AddDays(i).Date;  // The subsequent days always start at midnight
                                                    }

                                                    if (i > 0 && i < numberOfDaysForEvent.Days)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i == numberOfDaysForEvent.Days)
                                                    {
                                                        // No need to touch the newEndDateAndTime variable
                                                    }

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                        }
                                    }
                                }
                                // Every Wednesday for example
                                else if (FallsOnThisDay(thisDay, daysOfWeekForThisEvent, futureEvent))
                                {
                                    if (startDayAndTime.Date <= thisDay.Date)
                                    {
                                        if (numberOfDaysForEvent.Days == 0)
                                        {
                                            DateTime newStartDateAndTime = thisDay.Add(eventStartTimeOfDay);
                                            DateTime newEndDateAndTime = thisDay.Add(eventEndTimeOfDay);

                                            copyOfFutureEvent.endDayAndTime = (newEndDateAndTime.ToOADate());
                                            copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                            if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                                allFutureEventsForNDays.Add(copyOfFutureEvent);
                                        }
                                        else
                                        {
                                            // Multi-Day event
                                            if (1 == copyOfFutureEvent.allDay)
                                            {
                                                for (int i = 0; i < numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);
                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.AddDays(i + 1).Add(eventEndTimeOfDay);

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                            else
                                            {
                                                for (int i = 0; i <= numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);

                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.Add(eventEndTimeOfDay);

                                                    if (i == 0)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i > 0)
                                                    {
                                                        newStartDateAndTime = startDayAndTime.Date.AddDays(i).Date;  // The subsequent days always start at midnight
                                                    }

                                                    if (i > 0 && i < numberOfDaysForEvent.Days)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i == numberOfDaysForEvent.Days)
                                                    {
                                                        // No need to touch the newEndDateAndTime variable
                                                    }

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                        }
                                    }
                                }
                                // Every 18th of the Month for example
                                else if (thisDay.Day == futureEvent.rcDayOfMonth)
                                {
                                    if (startDayAndTime.Date <= thisDay.Date)
                                    {
                                        if (numberOfDaysForEvent.Days == 0)
                                        {
                                            DateTime newStartDateAndTime = thisDay.Add(eventStartTimeOfDay);
                                            DateTime newEndDateAndTime = thisDay.Add(eventEndTimeOfDay);

                                            copyOfFutureEvent.endDayAndTime = (newEndDateAndTime.ToOADate());
                                            copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                            if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                                allFutureEventsForNDays.Add(copyOfFutureEvent);
                                        }
                                        else
                                        {
                                            // Multi-Day event
                                            if (1 == copyOfFutureEvent.allDay)
                                            {
                                                for (int i = 0; i < numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);
                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.AddDays(i + 1).Add(eventEndTimeOfDay);

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                            else
                                            {
                                                for (int i = 0; i <= numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);

                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.Add(eventEndTimeOfDay);

                                                    if (i == 0)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i > 0)
                                                    {
                                                        newStartDateAndTime = startDayAndTime.Date.AddDays(i).Date;  // The subsequent days always start at midnight
                                                    }

                                                    if (i > 0 && i < numberOfDaysForEvent.Days)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i == numberOfDaysForEvent.Days)
                                                    {
                                                        // No need to touch the newEndDateAndTime variable
                                                    }

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                        }
                                    }
                                }
                                // If Sunday is the first day of the week, then every Sunday
                                else if (futureEvent.rcFirstDayOfWeek == 1 &&
                                    FallsOnThisDay(thisDay, CMDayOfWeekType.CMDayOfWeekTypeSunday, futureEvent))    // TODO: Change later because who know what the first day of week is
                                {
                                    if (startDayAndTime.Date <= thisDay.Date)
                                    {
                                        if (numberOfDaysForEvent.Days == 0)
                                        {
                                            DateTime newStartDateAndTime = thisDay.Add(eventStartTimeOfDay);
                                            DateTime newEndDateAndTime = thisDay.Add(eventEndTimeOfDay);

                                            copyOfFutureEvent.endDayAndTime = (newEndDateAndTime.ToOADate());
                                            copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                            if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                                allFutureEventsForNDays.Add(copyOfFutureEvent);
                                        }
                                        else
                                        {
                                            // Multi-Day event
                                            if (1 == copyOfFutureEvent.allDay)
                                            {
                                                for (int i = 0; i < numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);
                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.AddDays(i + 1).Add(eventEndTimeOfDay);

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                            else
                                            {
                                                for (int i = 0; i <= numberOfDaysForEvent.Days; i++)
                                                {
                                                    Events copyToAdd = new Events();
                                                    copyToAdd.CopyToThis(copyOfFutureEvent);

                                                    DateTime newStartDateAndTime = startDayAndTime.Date.AddDays(i).Add(eventStartTimeOfDay);
                                                    DateTime newEndDateAndTime = endDayAndTime.Date.Add(eventEndTimeOfDay);

                                                    if (i == 0)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i > 0)
                                                    {
                                                        newStartDateAndTime = startDayAndTime.Date.AddDays(i).Date;  // The subsequent days always start at midnight
                                                    }

                                                    if (i > 0 && i < numberOfDaysForEvent.Days)
                                                    {
                                                        newEndDateAndTime = startDayAndTime.Date.AddSeconds((23 * 3600) + (59 * 60) + 59);   // The first day ends at 23:59:59
                                                    }
                                                    else if (i == numberOfDaysForEvent.Days)
                                                    {
                                                        // No need to touch the newEndDateAndTime variable
                                                    }

                                                    copyToAdd.endDayAndTime = (newEndDateAndTime.ToOADate());
                                                    copyToAdd.startDayAndTime = (newStartDateAndTime.ToOADate());
                                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyToAdd))
                                                        allFutureEventsForNDays.Add(copyToAdd);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    // thisDay does not have any meetings attached to it.
                                }
                            }
                        }
                    }
                }
            }
            return allFutureEventsForNDays;
        }

        public static List<Events> GenerateSearchEventForDays(int numOfDays, double oldestDay, List<Events> eventsAlreadyInBlock, Events recurringEventDefinitions)
        {
            // Now fabricate 30 more days from today worth of recurring events
            DateTime startDate = DateTimeExtensions.FromOADate(oldestDay).Date;
            List<Events> allFutureEventsForNDays = new List<Events>();

            if (recurringEventDefinitions == null)
            {
                return allFutureEventsForNDays;
            }
            for (int dayCounter = 0; dayCounter < numOfDays; dayCounter++)
            {
                DateTime thisDay = startDate.AddDays(dayCounter);


                {
                    CMRecurrenceType recurrenceType = (CMRecurrenceType)recurringEventDefinitions.rcType;
                    CMDayOfWeekType daysOfWeekForThisEvent = (CMDayOfWeekType)recurringEventDefinitions.rcDayOfWeek;
                    DateTime startDayAndTime = DateTimeExtensions.FromOADate(recurringEventDefinitions.startDayAndTime);
                    TimeSpan startDayTime = startDayAndTime.TimeOfDay;

                    DateTime until = DateTime.MaxValue;
                    // PERMUTATIONS AND THEIR RESPECTIVE OPTIONS:
                    // 1. Repeat Daily: (# day(s) || {Monday - Friday })
                    //    a. "Recurring test meeting". 63 Occurences Starting 8/29/2014. (Monday-Friday)
                    //        1. rcDayOfMonth:0, rcDayOfWeek:62, rcFirstDayOfWeek:0, rcInterval:1, rcMonthOfYear:0, rcOccurrences:63, rcType:1, rcWeekOfMonth:0
                    //    b. "Daily every 3 days" 17 Occurrences
                    //        1. rcDayOfMonth:0, rcDayOfWeek:0, rcFirstDayOfWeek:0, rcInterval:3, rcMonthOfYear:0, rcOccurrences:17, rcType:1, rcWeekOfMonth:0
                    //
                    // 2. Repeat Weekly: (# week(s) on {Sunday - Saturday})
                    //    a. "Recurring test meeting every Tu/Th for 26 Occurences" starting 9/16. 
                    //        1. rcDayOfMonth:0, rcDayOfWeek:20, rcFirstDayOfWeek:0, rcInterval:1, rcMonthOfYear:0, rcOccurrences:26, rcType:1, rcWeekOfMonth:0
                    //    b. "First Day of Week recurring event (Every 2 weeks on Sunday)" starting 9/14 with 11 occurences. 2 weeks on Sunday
                    //        1. rcDayOfMonth: 0, rcDayOfWeek:1, rcFirstDayOfWeek:0, rcInterval:2, rcMonthOfYear:0, rcOccurrences:11, rcType:1, rcWeekOfMonth:0
                    //    c. "Week Of Month: Every 1 week on Friday" starting 9/12/2014 with 16 occurrences. 1 week on Friday.
                    //        1. rcDayOfMonth:0, rcDayOfWeek:32, rcFirstDayOfWeek:0, rcInterval:1, rcMonthOfYear:0, rcOccurrences:16, rcType:1, rcWeekOfMonth:0
                    //
                    // 3. Repeat Yearly: (Month/Day || {first second, third, fourth, last}, {weekday, weekend, Sunday-Saturday} of {Jan-Dec})
                    //    a. "Yearly: The fourth Friday Of September 3 occurences" with 3 occurrences
                    //        1. rcDayOfMonth:0, rcDayOfWeek:32, rcFirstDayOfWeek:0, rcInterval:1, rcMonthOfYear:9, rcOccurrences:3, rcType:6, rcWeekOfMonth:4
                    //    b. "Yearly: October 23rd with 5 occurrences (Month/Day)
                    //        1. rcDayOfMonth:23, rcDayOfWeek:0, rcFirstDayOfWeek:0, rcInterval:1, rcMonthOfYear:10, rcOccurrences:5, rcType:5, rcWeekOfMonth:0
                    //
                    // 4. Repeat Monthly: (11th day of every # month(s) || {first, second, third, fourth, last} {weekday, weekend, Sunday-Saturday} of every # month(s))
                    //    a. "Month of Year: 3rd Monday of Every 3 Months".  33 Occurrences. Third Monday of every 3 Months
                    //        1. rcDayOfMonth:0, rcDayOfWeek:2, rcFirstDayOfWeek:0, rcInterval:3, rcMonthOfYear:0, rcOccurrences:33, rcType:3, rcWeekOfMonth:3
                    //
                    // 5. Repeat Monthly: (Day # of every # months || {first second, third, fourth, last}, {weekday, weekend, Sunday-Saturday} of every # months)
                    //    a. "Every 2 months on the 15th Day" with 7 occurrences. (Day 15 of every 2 months)
                    //        1. rcDayOfMonth:15, rcDayOfWeek:0, rcFirstDayOfWeek:0, rcInterval:2, rcMonthOfYear:0, rcOccurrences:7, rcType:2, rcWeekOfMonth:0
                    //
                    // 6. Repeat Monthly: (Day # of every # months || {first second, third, fourth, last}, {weekday, weekend, Sunday-Saturday} of every # months)
                    //    a. "Monthly: The third Thursday of every 1 month" with 10 occurrences. (Third Thursday of every 1 month)
                    //        1. rcDayOfMonth:0, rcDayOfWeek:16, rcFirstDayOfWeek:0, rcInterval:1, rcMonthOfYear:0, rcOccurrences:10, rcType:3, rcWeekOfMonth:3


                    // First thing to do is if Occurences is > 0, then calculate an "Until", because one is not supplied
                    // Ultimately we need to determine whether or not this particular event is still active during the
                    // date range that's visible on the screen.
                    if (recurringEventDefinitions.rcOccurences > 0)
                    {
                        if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeYearly ||
                            recurrenceType == CMRecurrenceType.CMRecurrenceTypeYearlyNDay)
                        {
                            //   CAN ONLY HAVE AT MOST 1 EVENT PER YEAR
                            //   THE START DATE DAY NUMBER IS ALWAYS THE SAME
                            until = startDayAndTime.Date.AddYears(recurringEventDefinitions.rcOccurences * recurringEventDefinitions.rcInterval);
                        }
                        else if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeMonthly ||
                            recurrenceType == CMRecurrenceType.CMRecurrenceTypeMonthlyNDay)
                        {
                            // ASSUMPTIONS: 
                            //   CAN ONLY HAVE AT MOST 1 EVENT PER MONTH
                            //   THE START DATE DAY NUMBER IS ALWAYS THE SAME
                            //   rcMonthOfYear is ALWAYS 0. 
                            //   WHEN rcWeekOfMonth > 0: rcDayOfMonth is ALWAYS = 0, rcDayOfWeek is ALWAYS > 0. 
                            //   WHEN rcWeekOfMonth = 0: rcDayOfMonth is ALWAYS > 0, rcDayOfWeek is ALWAYS = 0.
                            int totalMeetingMonths = recurringEventDefinitions.rcInterval * recurringEventDefinitions.rcOccurences;
                            //int monthsleft = totalMeetingMonths / 12;
                            until = startDayAndTime.Date.AddMonths(totalMeetingMonths);
                        }
                        else if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeDaily ||
                            recurrenceType == CMRecurrenceType.CMRecurrenceTypeWeekly)
                        {
                            int totalDaysPerWeek = 0;
                            if (recurringEventDefinitions.rcDayOfWeek == 0)
                            {
                                // All 7 days
                                totalDaysPerWeek = 7;
                            }
                            else
                            {
                                // ASSUMPTIONS: rcDayOfMonth is ALWAYS 0.  rcMonthOfYear is ALWAYS 0.  rcWeekOfMonth is ALWAYS 0
                                // Calculate how many days / week this event has
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeSunday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeMonday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeTuesday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeWednesday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeThursday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeFriday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                                if (daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeSaturday))
                                {
                                    totalDaysPerWeek += 1;
                                }
                            }
                            // This is the date when this event is no longer valid.
                            until = startDayAndTime.Date.AddDays(((recurringEventDefinitions.rcOccurences / totalDaysPerWeek)) * (recurringEventDefinitions.rcInterval * 7));
                        }
                    }
                    else if (recurringEventDefinitions.rcUntil != 0)
                    {
                        until = DateTimeExtensions.FromOADate(recurringEventDefinitions.rcUntil);
                    }
                    if (thisDay <= until)
                    {
                        Events copyOfFutureEvent = new Events();
                        copyOfFutureEvent.CopyToThis(recurringEventDefinitions);

                        if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeYearly ||
                            recurrenceType == CMRecurrenceType.CMRecurrenceTypeYearlyNDay)
                        {
                            // Determine if 'thisDay' is equal to the day in this 'recurringEvents'
                            int dayNumber = recurringEventDefinitions.rcDayOfMonth;
                            if (recurringEventDefinitions.rcDayOfMonth == 0)
                            {
                                var allWeeks = (from day in Enumerable.Range(1, DateTime.DaysInMonth(thisDay.Year, recurringEventDefinitions.rcMonthOfYear))
                                                where new DateTime(thisDay.Year, recurringEventDefinitions.rcMonthOfYear, day).DayOfWeek == GetNETDayOfWeekType((CMDayOfWeekType)recurringEventDefinitions.rcDayOfWeek)
                                                select day).ToList();
                                int weekOfMonth = recurringEventDefinitions.rcWeekOfMonth;
                                if (weekOfMonth > allWeeks.Count)
                                {
                                    weekOfMonth = allWeeks.Count;
                                }
                                // Third Thursday of September for example.  Need to get a day number
                                var eventDayVar = allWeeks.ElementAt(weekOfMonth - 1);
                                dayNumber = eventDayVar;
                            }
                            DateTime tmp = new DateTime(thisDay.Year, recurringEventDefinitions.rcMonthOfYear, dayNumber);
                            if (tmp.Year == thisDay.Year &&
                                tmp.Month == thisDay.Month &&
                                tmp.Day == thisDay.Day)
                            {
                                if (startDayAndTime.Date <= thisDay.Date)
                                {
                                    DateTime newStartDateAndTime = thisDay.Date.Add(startDayTime);

                                    copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                        allFutureEventsForNDays.Add(copyOfFutureEvent);
                                }
                            }
                        }
                        else if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeMonthly ||
                            recurrenceType == CMRecurrenceType.CMRecurrenceTypeMonthlyNDay)
                        {
                            // Determine if 'thisDay' is equal to the day in this 'recurringEvents'
                            int dayNumber = recurringEventDefinitions.rcDayOfMonth;
                            if (recurringEventDefinitions.rcDayOfMonth == 0)
                            {
                                // Third Thursday for example.  Need to get a day number
                                var eventDayVar = (from day in Enumerable.Range(1, DateTime.DaysInMonth(thisDay.Year, thisDay.Month))
                                                   where new DateTime(thisDay.Year, thisDay.Month, day).DayOfWeek == GetNETDayOfWeekType((CMDayOfWeekType)recurringEventDefinitions.rcDayOfWeek)
                                                   select day);

                                if (eventDayVar.Cast<int>().Count() == 4 && recurringEventDefinitions.rcWeekOfMonth == 5)     //if fifth day do not exist in a month but recurrence is on last day of month, here day means "monday" tuesday etc
                                {
                                    dayNumber = eventDayVar.ElementAt(3);                                     //assign last day availbale
                                }
                                else
                                {
                                    dayNumber = eventDayVar.ElementAt(recurringEventDefinitions.rcWeekOfMonth - 1);
                                }

                            }
                            DateTime tmp = new DateTime(thisDay.Year, thisDay.Month, dayNumber);
                            if (tmp.Year == thisDay.Year &&
                                tmp.Month == thisDay.Month &&
                                tmp.Day == thisDay.Day)
                            {
                                if (startDayAndTime.Date <= thisDay.Date)
                                {
                                    DateTime newStartDateAndTime = thisDay.Date.Add(startDayTime);

                                    copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                        allFutureEventsForNDays.Add(copyOfFutureEvent);
                                }
                            }
                        }
                        else if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeDaily ||
                            recurrenceType == CMRecurrenceType.CMRecurrenceTypeWeekly)
                        {
                            if (copyOfFutureEvent.rcDayOfWeek == 0)
                            {
                                // Daily meeting all 7 days
                                if (startDayAndTime.Date <= thisDay.Date)
                                {
                                    DateTime newStartDateAndTime = thisDay.Date.Add(startDayTime);

                                    copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                        allFutureEventsForNDays.Add(copyOfFutureEvent);
                                }
                            }
                            // Last day of the month
                            else if (thisDay.Day == DateTime.DaysInMonth(thisDay.Year, thisDay.Month) &&
                                daysOfWeekForThisEvent.HasFlag(CMDayOfWeekType.CMDayOfWeekTypeLastDayofMonth))
                            {
                                if (startDayAndTime.Date <= thisDay.Date)
                                {
                                    DateTime newStartDateAndTime = thisDay.Date.Add(startDayTime);

                                    copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                        allFutureEventsForNDays.Add(copyOfFutureEvent);
                                }
                            }
                            // Every Wednesday for example
                            else if (FallsOnThisDay(thisDay, daysOfWeekForThisEvent, recurringEventDefinitions))
                            {
                                if (startDayAndTime.Date <= thisDay.Date)
                                {
                                    DateTime newStartDateAndTime = thisDay.Date.Add(startDayTime);

                                    copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                        allFutureEventsForNDays.Add(copyOfFutureEvent);
                                }
                            }
                            // Every 18th of the Month for example
                            else if (thisDay.Day == recurringEventDefinitions.rcDayOfMonth)
                            {
                                if (startDayAndTime.Date <= thisDay.Date)
                                {
                                    DateTime newStartDateAndTime = thisDay.Date.Add(startDayTime);

                                    copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                        allFutureEventsForNDays.Add(copyOfFutureEvent);
                                }
                            }
                            // First day of the week (SEE TODO!)
                            else if (recurringEventDefinitions.rcFirstDayOfWeek == 1 &&
                                FallsOnThisDay(thisDay, CMDayOfWeekType.CMDayOfWeekTypeSunday, recurringEventDefinitions))    // TODO: Change later because who know what the first day of week is
                            {
                                if (startDayAndTime.Date <= thisDay.Date)
                                {
                                    DateTime newStartDateAndTime = thisDay.Date.Add(startDayTime);

                                    copyOfFutureEvent.startDayAndTime = (newStartDateAndTime.ToOADate());
                                    if (!QueryForSameMeeting(eventsAlreadyInBlock, copyOfFutureEvent))
                                        allFutureEventsForNDays.Add(copyOfFutureEvent);
                                }
                            }
                            else
                            {
                                LoggingWP8.WP8Logger.LogMessage("Weird day!");
                            }
                        }
                    }
                }
            }
            return allFutureEventsForNDays;
        }


        public static CMDayOfWeekType GetCMDayOfWeekType(DayOfWeek dayOfWeek)
        {
            if (dayOfWeek == DayOfWeek.Sunday)
            {
                return CMDayOfWeekType.CMDayOfWeekTypeSunday;
            }
            if (dayOfWeek == DayOfWeek.Monday)
            {
                return CMDayOfWeekType.CMDayOfWeekTypeMonday;
            }
            if (dayOfWeek == DayOfWeek.Tuesday)
            {
                return CMDayOfWeekType.CMDayOfWeekTypeTuesday;
            }
            if (dayOfWeek == DayOfWeek.Wednesday)
            {
                return CMDayOfWeekType.CMDayOfWeekTypeWednesday;
            }
            if (dayOfWeek == DayOfWeek.Thursday)
            {
                return CMDayOfWeekType.CMDayOfWeekTypeThursday;
            }
            if (dayOfWeek == DayOfWeek.Friday)
            {
                return CMDayOfWeekType.CMDayOfWeekTypeFriday;
            }
            return CMDayOfWeekType.CMDayOfWeekTypeSaturday;
        }

        public static DayOfWeek GetNETDayOfWeekType(CMDayOfWeekType dayOfWeek)
        {
            if (dayOfWeek == CMDayOfWeekType.CMDayOfWeekTypeSunday)
            {
                return DayOfWeek.Sunday;
            }
            else if (dayOfWeek == CMDayOfWeekType.CMDayOfWeekTypeMonday)
            {
                return DayOfWeek.Monday;
            }
            else if (dayOfWeek == CMDayOfWeekType.CMDayOfWeekTypeTuesday)
            {
                return DayOfWeek.Tuesday;
            }
            else if (dayOfWeek == CMDayOfWeekType.CMDayOfWeekTypeWednesday)
            {
                return DayOfWeek.Wednesday;
            }
            else if (dayOfWeek == CMDayOfWeekType.CMDayOfWeekTypeThursday)
            {
                return DayOfWeek.Thursday;
            }
            else if (dayOfWeek == CMDayOfWeekType.CMDayOfWeekTypeFriday)
            {
                return DayOfWeek.Friday;
            }
            else
            {
                return DayOfWeek.Saturday;
            }
        }

        public static bool FallsOnThisDay(DateTime thisDay, CMDayOfWeekType daysOfWeekForThisEvent, Events futureEvent)
        {
            // Convert DayOfWeek to CMDayOfWeekType
            DayOfWeek thisDaysDayOfWeek = thisDay.DayOfWeek;
            CMDayOfWeekType thisDayOfWeekCustom = GetCMDayOfWeekType(thisDaysDayOfWeek);
            CMRecurrenceType recurrenceType = (CMRecurrenceType)futureEvent.rcType;

            if (daysOfWeekForThisEvent.HasFlag(thisDayOfWeekCustom))
            {
                if (futureEvent.rcInterval == 1)
                {
                    return true;
                }

                DateTime dtDayPart = DateTimeExtensions.FromOADate(futureEvent.startDayAndTime).Date;
                int interval = futureEvent.rcInterval;

                if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeDaily)
                {
                    // Do we skip this day or no?
                }
                else if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeWeekly)
                {
                    // Do we skip this week or no?
                    TimeSpan diff = thisDay - dtDayPart;
                    int result = (diff.Days / 7) % interval;
                    if (result == 0)
                    {
                        return true;
                    }
                    return false;
                }
                else if (recurrenceType == CMRecurrenceType.CMRecurrenceTypeMonthly)
                {
                    // Do we skip this month or no?
                    return true;
                }

            }

            return false;
        }

        public static bool QueryForSameMeeting(List<Events> allEvents, Events futureEvent)
        {
            try
            {
                bool thisEvent = (from calEvent in allEvents
                                  where
                                      calEvent.subject == futureEvent.subject &&
                                      calEvent.startDayAndTime == futureEvent.startDayAndTime &&
                                      calEvent.endDayAndTime == futureEvent.endDayAndTime
                                  select calEvent).Any();
                return thisEvent;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<Events> FilterRecurringEventDefinitionsFromSource(List<Events> theSource)
        {
            var events = (from calEvent in theSource where calEvent.reoccurs == 1 && calEvent.Original_id == 0 select calEvent).ToList();
            return events;
        }
    }
}
