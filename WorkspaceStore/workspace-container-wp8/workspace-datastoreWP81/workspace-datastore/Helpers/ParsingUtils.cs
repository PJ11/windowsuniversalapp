﻿#define USE_ASYNC
using ActiveSyncPCL;
using SQLite;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using workspace_datastore;
using workspace_datastore.Managers;
using workspace_datastore.models;
using HtmlAgilityPack;
using System.Threading;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Popups;

namespace workspace_datastore.Helpers
{
    /// <summary>
    /// Converts HTML content of description to text.
    /// Needed to see text in description text box.
    /// </summary>
    public class HtmlToText
    {
        public HtmlToText()
        {
        }

        public string Convert(string path)
        {
            HtmlDocument doc = new HtmlDocument();
            //doc.Load(path);

            StringWriter sw = new StringWriter();
            ConvertTo(doc.DocumentNode, sw);
            sw.Flush();
            return sw.ToString();
        }

        public string ConvertHtml(string html)
        {
            HtmlDocument doc = new HtmlDocument();
            if (html != null)
            {
                doc.LoadHtml(html);
            }
            else
            {
                LoggingWP8.WP8Logger.LogMessage("html is null @" + DateTime.Today.ToString());
            }
            StringWriter sw = new StringWriter();
            ConvertTo(doc.DocumentNode, sw);
            sw.Flush();
            return sw.ToString();
        }

        private void ConvertContentTo(HtmlNode node, TextWriter outText)
        {
            foreach (HtmlNode subnode in node.ChildNodes)
            {
                ConvertTo(subnode, outText);
            }
        }

        public void ConvertTo(HtmlNode node, TextWriter outText)
        {
            string html;
            switch (node.NodeType)
            {
                case HtmlNodeType.Comment:
                    // don't output comments
                    break;

                case HtmlNodeType.Document:
                    ConvertContentTo(node, outText);
                    break;

                case HtmlNodeType.Text:
                    // script and style must not be output
                    string parentName = node.ParentNode.Name;
                    if ((parentName == "script") || (parentName == "style"))
                        break;

                    // get text
                    html = ((HtmlTextNode)node).Text;

                    // is it in fact a special closing node output as text?
                    if (HtmlNode.IsOverlappedClosingElement(html))
                        break;

                    // check the text is meaningful and not a bunch of whitespaces
                    if (html.Trim().Length > 0)
                    {
                        outText.Write(HtmlEntity.DeEntitize(html));
                    }
                    break;

                case HtmlNodeType.Element:
                    switch (node.Name)
                    {
                        case "p":
                            // treat paragraphs as crlf
                            outText.Write("\r\n");
                            break;
                    }

                    if (node.HasChildNodes)
                    {
                        ConvertContentTo(node, outText);
                    }
                    break;
            }
        }
    }

    public class ParsingUtils
    {
        //This code will check whether a tag is present or not. if not present, it will return empty string
        static private string GetString(XElement xe, string tag)
        {
            if (xe != null && xe.HasElements && xe.Element(tag) != null)
                return xe.Element(tag).Value;
            else
                return string.Empty;
        }
        static private XElement GetXElement(XElement xe, string tag)
        {
            if (xe != null && xe.HasElements && xe.Element(tag) != null)
                return xe.Element(tag);
            else
                return null;
        }

        public static List <workspace_datastore.models.Folder> GetFoldersForPingResult(string response)
        {
            XDocument responseXml = null;
            List<workspace_datastore.models.Folder> foldersToReturn = new List<models.Folder>();

            if (response != null)
            {
                string ns = "{Ping}";
                responseXml = new XDocument();
                TextReader tr = new StringReader(response);
                responseXml = XDocument.Load(tr, LoadOptions.None);
                if (responseXml != null)
                {
                    IEnumerable<XElement> folders = responseXml.Descendants().Where(p => p.Name == (ns + "Folders")).ToList();
                    if (folders != null)
                    {
                        IEnumerable<XElement> foldersToSync = responseXml.Descendants().Where(p => p.Name == (ns + "Folder")).ToList();
                        if(null != foldersToSync && foldersToSync.Count() > 0)
                        {
                            FolderManager folderManager = new FolderManager();
                            foreach (XElement folder in foldersToSync)
                            {
                                List<workspace_datastore.models.Folder> dbFolders = folderManager.GetFolderInfoAsync(System.Convert.ToInt32(folder.Value)).Result;
                                if (dbFolders.Count != null && dbFolders.Count > 0)
                                    foldersToReturn.Add(dbFolders[0]); 
                            }
                        }
                    }
                }
            } 
            return foldersToReturn;
        }

        public static string cleanUpFrom(string from)
        {
            string newFrom = "";
            if (from.Contains('<'))
            {
                newFrom = from.Substring(0, from.IndexOf('<') - 1);
            }
            // Now get rid of the quotes
            newFrom = newFrom.Replace("\"", "");
            return newFrom;
        }

        public static string cleanUpFromToGetMailID(string from)
        {
            string newFrom = "";
            if (from.Contains('<'))
            {
                newFrom = from.Substring(from.IndexOf('<') + 1, from.IndexOf('>') - from.IndexOf('<')-1);
            }
            // Now get rid of the quotes
            //newFrom = newFrom.Replace("\"", "");
            return newFrom;
        }
        
        public static string cleanUpCC(string stringCC)
        {
            // This will cause an exception:
            //    "Steven_Colquitt@DELL.com> <Steven_Colquitt@DELL.com" <Steven_Colquitt@DELL.com>

            //stringCC = "\"Steven_Colquitt@DELL.com> <Steven_Colquitt@DELL.com\" <Steven_Colquitt@DELL.com> \"Steven_Colquitt@DELL.com> <Steven_Colquitt@DELL.com\" <Steven_Colquitt@DELL.com> \"Steven_Colquitt@DELL.com> <Steven_Colquitt@DELL.com\" <Steven_Colquitt@DELL.com>";
            string newCC = "";
            string tempCC = "";
            //Dictionary<string, string> userNameAndEmailDictionary = new Dictionary<string, string>();
            try
            {
                bool isInQuotes = false;
                bool isInEmail = false;
                for (int i = 0; i < stringCC.Length; i++ )
                {
                    if(stringCC[i] == '\"')
                    {
                        isInQuotes = (!isInQuotes) ? true : false;
                        // Get to the ending quotes
                        continue;
                    }
                    else if(isInQuotes)
                    {
                        continue;
                    }
                    if(!isInQuotes)
                    {
                        if(stringCC[i] == '<')
                        {
                            isInEmail = true;
                            continue;
                        }
                        else if(isInEmail && stringCC[i] == '>')
                        {
                            isInEmail = false;
                            if(i < stringCC.Length - 2)
                            {
                                // More to come!
                                newCC += "; ";
                            }
                            continue;
                        }
                        else if(isInEmail && stringCC[i] != ' ')
                        {
                            newCC += stringCC[i];
                        }
                    }
                }
            }
            catch(System.ArgumentOutOfRangeException)
            {
                newCC = "Unknown";
            }

            return newCC;
        }

        static public string GetSyncKeyForFolder(XDocument responseXMlDocument, string folderId)
        {
            string folderSyncKey = "0";

            string ns = "{AirSync}";
            IEnumerable<XElement> collection = responseXMlDocument.Descendants().Where(p => p.Name == (ns + "Collection")).ToList();
            if (collection != null)
            {
                foreach (XElement coll in collection)
                {
                    XElement j = coll as XElement;
                    if (j != null && (j.Descendants().First(p => p.Name == (ns + "CollectionId")).Value).ToString() == folderId.ToString())
                    {
                        XElement syncKey = j.Descendants().First(p => p.Name == (ns + "SyncKey"));
                        folderSyncKey = (string)syncKey.Value;
                        break;
                    }
                }
            }
            return folderSyncKey;
        }

        static private int GetSyncStatusForFolder(XDocument responseXMlDocument, string folderId)
        {
            int folderSyncStatus = 0;

            string ns = "{AirSync}";
            IEnumerable<XElement> collection = responseXMlDocument.Descendants().Where(p => p.Name == (ns + "Collection")).ToList();
            if (collection != null)
            {
                foreach (XElement coll in collection)
                {
                    XElement j = coll as XElement;
                    if (j != null && (j.Descendants().First(p => p.Name == (ns + "CollectionId")).Value).ToString() == folderId.ToString())
                    {
                        XElement statusKey = j.Descendants().First(p => p.Name == (ns + "Status"));
                        folderSyncStatus = int.Parse(statusKey.Value);
                        break;
                    }
                }
            }

            return folderSyncStatus;
        }

        static private bool IsMoreAvailable(XDocument responseXMlDocument, string folderId)
        {
            bool isMoreAvailable = false;

            string ns = "{AirSync}";
            IEnumerable<XElement> collection = responseXMlDocument.Descendants().Where(p => p.Name == (ns + "Collection")).ToList();
            if (collection != null)
            {
                foreach (XElement coll in collection)
                {
                    XElement j = coll as XElement;
                    if (j != null && (j.Descendants().First(p => p.Name == (ns + "CollectionId")).Value).ToString() == folderId.ToString())
                    {
                        //XElement moreAvailableTag = j.Descendants().First(p => p.Name == (ns + "MoreAvailable"));
                        XElement moreAvailableTag = GetXElement(coll, ns + "MoreAvailable");
                        if (moreAvailableTag != null)
                            isMoreAvailable = true;
                    }
                }
            }

            return isMoreAvailable;
        }

        static public async Task<Dictionary<string, List<object>>> ParseMailContent(List<ServerSyncCommand> addCommandList, List<ServerSyncCommand> deleteCommandList,
                                                    List<ServerSyncCommand> changeCommandList, List<ServerSyncCommand> softDeleteCommandList,
                                                    ActiveSyncPCL.Folder folder)
        {
            bool isException = false;
            string expMsg = string.Empty;

            Dictionary<string, List<object>> results = new Dictionary<string, List<object>>();
            List<MessageManager> messages = null;
            try
            {
                if (addCommandList != null && addCommandList.Count > 0)
                {
                    messages = await ParseMailAddCommandsAndUpdateDB(addCommandList, folder.Id);
                    results.Add("ADD", messages.Cast<Object>().ToList());
                }

                if (deleteCommandList != null && deleteCommandList.Count > 0)
                {
                     messages = await ParseMailDeleteCommandAndUpdateDB(deleteCommandList);
                     results.Add("DELETE", messages.Cast<Object>().ToList());
                }

                if (changeCommandList != null && changeCommandList.Count > 0)
                {
                    messages = await ParseMailChangeCommandsAndUpdateDB(changeCommandList, folder.Id);
                    results.Add("CHANGE", messages.Cast<Object>().ToList());
                }

                if (softDeleteCommandList != null && softDeleteCommandList.Count > 0)
                {
                    messages = await ParseMailDeleteCommandAndUpdateDB(softDeleteCommandList);
                    results.Add("SOFTDELETE", messages.Cast<Object>().ToList());
                }

                try
                {
                    await UpdateFolderSyncKey(folder);
                }
                catch (Exception e)
                {
                    expMsg = e.Message;
                    isException = true;
#if WP_80_SILVERLIGHT
                    System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        System.Windows.MessageBox.Show(Message, "", System.Windows.MessageBoxButton.OK);
                    });
#endif
                }

                if (isException)
                {
                    await ShowMessageBox("CATASTROPHE SyncKey not written to DB '" + expMsg + "'");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return results;
        }

        private async static Task UpdateFolderSyncKey(ActiveSyncPCL.Folder folder)
        {
            FolderManager folderManager = new FolderManager();
#if USE_ASYNC
            List<workspace_datastore.models.Folder> folderList = await folderManager.GetFolderInfoAsync(0);
#else
                List<workspace_datastore.models.Folder> folderList = folderManager.GetFolderInfo(0);
#endif
            workspace_datastore.models.Folder folderDB = folderList.FirstOrDefault(x => x.displayName == folder.Name);
            if (folderDB != null)
            {
                //if (folder.Name.ToLower() == "inbox")
                //    System.Diagnostics.Debug.WriteLine("FOLDER SYNC KEY WRITE DATABASE: {0}", folder.SyncKey);
                folderDB.syncKey = folder.SyncKey;
                folderDB.syncTime = Convert.ToSingle(folder.LastSyncTime.ToOADate());
#if USE_ASYNC
                await folderManager.UpdateFolderInfoAsync(folderDB);
#else
                    folderManager.UpdateFolderInfo(folderDB);
#endif
            }
        }

        private static async Task<List<workspace_datastore.models.Folder>> GetFolderInfo(FolderManager folderManager, int id)
        {
            List<workspace_datastore.models.Folder> folderList = await folderManager.GetFolderInfoAsync(id);
            return folderList;
        }

        static public async void ParseContactContent(List<ServerSyncCommand> addCommandList, List<ServerSyncCommand> deleteCommandList,
                                                   List<ServerSyncCommand> changeCommandList, List<ServerSyncCommand> softDeleteCommandList,
                                                   ActiveSyncPCL.Folder folder)
        {
            FolderManager folderManager = new FolderManager();
#if USE_ASYNC
            List<workspace_datastore.models.Folder> folderList = await ParsingUtils.GetFolderInfo(folderManager, 0); // folderManager.GetFolderInfo(0).Result;
#else
            List<workspace_datastore.models.Folder> folderList = folderManager.GetFolderInfo(0);
#endif

            if (addCommandList != null && addCommandList.Count > 0)
                ParseContactAddCommandAndUpdateDB(addCommandList, folder.Id);

            if (deleteCommandList != null && deleteCommandList.Count > 0)
                ParseContactDeleteCommandAndUpdateDB(deleteCommandList, folder.Id);

            //Pankaj: To Do
            if (changeCommandList != null && changeCommandList.Count > 0)
                ParseContactChangeCommandAndUpdateDB(changeCommandList, folder.Id);

            //Pankaj: To Do
            //if (softDeleteCommandList != null && softDeleteCommandList.Count > 0)
            //    ParseSoftDeleteCommandAndUpdateDBForMail(softDeleteCommandList);

            workspace_datastore.models.Folder folderDB = folderList.FirstOrDefault(x => x.displayName == folder.Name);
            if (folderDB != null)
            {
                folderDB.syncKey = folder.SyncKey;
                folderDB.syncTime = Convert.ToSingle(folder.LastSyncTime.ToOADate());
#if USE_ASYNC
                await folderManager.UpdateFolderInfoAsync(folderDB);
#else
                folderManager.UpdateFolderInfo(folderDB);
#endif
            }


        }


        //This is generalized fuction for getting ADD/DELETE/UPDATE command list from response xml
        static public List<ServerSyncCommand> GetServerSyncCommandsForFolder(XDocument responseXML, String folderId, ServerSyncCommand.ServerSyncCommandType commandType)
        {

            List<ServerSyncCommand> commands = new List<ServerSyncCommand>();
            string ns = "{AirSync}";
            XElement collectionsNodeQ = responseXML.Descendants(ns + "Collection").Where(p => p.Element(ns + "CollectionId").Value == folderId).ToList().ElementAt(0);

            IEnumerable<XElement> commandNodeQ = from query in collectionsNodeQ.Descendants().Where(p => p.Name == (ns + commandType)).ToList()
                                                 select query;
            if (null != commandNodeQ)
            {
                foreach (XElement commandNode in commandNodeQ)
                {
                    XElement j = commandNode as XElement;
                    if (null != j)
                    {
                        XElement serverIdNode = j.Descendants().First(p => p.Name == (ns + "ServerId"));
                        XElement applicationDataNode = GetXElement(j, ns + "ApplicationData");

                        // XElement applicationDataNode = j.Descendants().First(p => p.Name == (ns + "ApplicationData"));
                        if (serverIdNode != null && applicationDataNode != null && commandType == ServerSyncCommand.ServerSyncCommandType.Add)
                        {
                            ServerSyncCommand command = new ServerSyncCommand(
                                commandType,
                                serverIdNode.Value,
                                applicationDataNode,
                                null);

                            commands.Add(command);
                        }
                        else if (serverIdNode != null && commandType == ServerSyncCommand.ServerSyncCommandType.Delete)
                        {
                            ServerSyncCommand command = new ServerSyncCommand(
                                commandType,
                                serverIdNode.Value,
                                null,
                                null);

                            commands.Add(command);
                        }
                        else if ((serverIdNode != null && commandType == ServerSyncCommand.ServerSyncCommandType.Change))
                        {
                            ServerSyncCommand command = new ServerSyncCommand(
                                commandType,
                                serverIdNode.Value,
                                null,
                                null);

                            commands.Add(command);

                        }
                    }
                }
            }
            return commands;
        }

        static async public Task<List<Message>> ParseMailAddCommandsOnly(List<ServerSyncCommand> commandList, string folderID)
        {
            string ns = "{AirSync}";
            string email = "{Email}";
            string tasks = "{Tasks}";
            string airsyncbase = "{AirSyncBase}";
            string email2 = "Email2";
            List<Message> messages = new List<Message>();
            bool continueWithParsing = true;

            try
            {
                foreach (ServerSyncCommand addCommand in commandList)
                {
                    XElement appDataXElement = addCommand.AppDataXml;
                    Message msg = new Message();

                    //reading data   

                    msg.serverIdentifier = addCommand.ServerId;
                    int messageId = Convert.ToInt32(msg.serverIdentifier.Split(':')[1]);
                    msg.id = messageId;
                    msg.toRecipientText = GetString(appDataXElement, email + "DisplayTo");
                    // From will be in the form of "Loren Rogers" <loren_rogers@kace.com>
                    // We want just the name
                    msg.fromEmail = cleanUpFrom(GetString(appDataXElement, email + "From"));
                    //taking subject data instead of threadtopic
                    
                    string strSubject = GetString(appDataXElement, email + "Subject");
                    if (!string.IsNullOrWhiteSpace(strSubject))
                    {
                        msg.subject = ConvertISO8859EncodingToUtf8(strSubject);
                    }
                    else
                    {
                        msg.subject = strSubject;
                    }

                    string dr = GetString(appDataXElement, email + "DateReceived");
                    DateTime dt = System.Convert.ToDateTime(dr).ToUniversalTime();
                    msg.receivedDate =dt.ToOADate();

                    XElement bodyElement = GetXElement(appDataXElement, airsyncbase + "Body");
                    if (null != bodyElement)
                    {
                        msg.bodyText = GetString(bodyElement, airsyncbase + "Data");
                        
                        string previewText = GetString(bodyElement, airsyncbase + "Preview");
                        if(null == previewText || previewText.Length == 0)
                        {
                            // Convert from html to plain text
                            HtmlToText htmlToText = new HtmlToText();
                            previewText = htmlToText.ConvertHtml(msg.bodyText);
                        }

                        msg.previewText = previewText;
                    }
                    //appDataXElement.Element(email + "Categories").Value

                    string readString = GetString(appDataXElement, email + "Read");
                    if (readString != string.Empty)
                        msg.read = int.Parse(readString);

                    string importanceString = GetString(appDataXElement, email + "Importance");
                    if (importanceString != string.Empty)
                        msg.importance = int.Parse(importanceString);

                    XElement flagElement = GetXElement(appDataXElement, email + "Flag");
                    if (null != flagElement && flagElement.HasElements)
                    {
                        /*
                          <tasks:DueDate xmlns:tasks="Tasks">2014-08-11T14:13:53.000Z</tasks:DueDate> 
                          <tasks:UtcDueDate xmlns:tasks="Tasks">2014-08-11T14:13:53.000Z</tasks:UtcDueDate> 
                          <tasks:UtcStartDate xmlns:tasks="Tasks">2014-08-04T14:13:53.000Z</tasks:UtcStartDate> 
                          <email:Status>2</email:Status> 
                          <email:FlagType>FollowUp</email:FlagType> 
                          <tasks:StartDate xmlns:tasks="Tasks">2014-08-04T14:13:53.000Z</tasks:StartDate> 
                          <tasks:ReminderSet xmlns:tasks="Tasks">0</tasks:ReminderSet> 
                         */

                        XElement duedateElement = GetXElement(flagElement, tasks + "DueDate");
                        if (duedateElement != null)
                        {
                            string dd = GetString(flagElement, tasks + "DueDate");
                            DateTime dueDate = System.Convert.ToDateTime(dd).ToUniversalTime();
                            msg.flagDueDate = dueDate.ToOADate();
                        }


                        XElement startdateElement = GetXElement(flagElement, tasks + "StartDate");
                        if (startdateElement != null)
                        {
                            string sd = GetString(flagElement, tasks + "StartDate");
                            DateTime startDate = System.Convert.ToDateTime(sd).ToUniversalTime();
                            msg.flagStartDate = startDate.ToOADate();
                        }

                        msg.flagType = GetString(flagElement, email + "FlagType");
                        msg.flagStatus = int.Parse(GetString(flagElement, email + "Status"));
                        if (msg.flagStatus == 1)
                            msg.flag = 0;
                        else
                            msg.flag = 1;

                    }
                    else
                        msg.flag = 0;

                    //ccrecipent
                    msg.ccRecipientText = cleanUpCC(GetString(appDataXElement, email + "Cc"));
                    //bccrecipient
                    //PK: The tag "RecievedAsBcc" returns boolean value, not the velue of reciepents of BCC. 
                    //Earlier it was wrong
                    string ifBccRecipient = GetString(appDataXElement, email2 + "ReceivedAsBcc");
                    if (ifBccRecipient != string.Empty)
                        msg.isRecipientAsBcc = Convert.ToBoolean(ifBccRecipient);

					//for sake of variable existence, we will be assigning it. We need this for data binding for new mail
					msg.bccRecipientText = "";


                    //folderid: we are taking id from call. Not needed fro XML ?
                    msg.folder_id = Convert.ToInt32(msg.serverIdentifier.Split(':')[0]);

                    //mime type
                    //msg.mimeType = GetString(appDataXElement,email +  );
                    //conversation ID
                    msg.conversationId = GetString(appDataXElement, email2 + "ConversationId");
                    msg.conversationIndex = GetString(appDataXElement, email2 + "ConversationIndex");


                    String lastVerbExecutedString = GetString(appDataXElement, email2 + "LastVerbExecuted");
                    if (lastVerbExecutedString != String.Empty)
                        msg.lastVerbExecuted = int.Parse(lastVerbExecutedString);

                    // Need to set up if this is a calendar meeting request
                    XElement meetingRequestElement = GetXElement(appDataXElement, email + "MeetingRequest");
                    if (null != meetingRequestElement)
                    {
                        // Until we get calendar set up, for now just set event_id to 1
                        msg.event_id = 1;

                        string allDayString = GetString(meetingRequestElement, email + "AllDayEvent");
                        if (allDayString != String.Empty)
                            msg.allDay = int.Parse(allDayString);


                        string startTimeString = GetString(meetingRequestElement, email + "StartTime");
                        //string acceptableFormat = AcceptableDateTime(startTimeString);
                        dt = System.Convert.ToDateTime(startTimeString).ToUniversalTime();
                        msg.startDayAndTime = dt.ToOADate();


                        string endTimeString = GetString(meetingRequestElement, email + "EndTime");
                        //string acceptableFormat = AcceptableDateTime(endTimeString);
                        dt = System.Convert.ToDateTime(endTimeString).ToUniversalTime();
                        msg.endDayAndTime = dt.ToOADate();

                        string locationString = GetString(meetingRequestElement, email + "Location");
                        if (locationString != String.Empty)
                            msg.location = ConvertISO8859EncodingToUtf8(locationString);

                        string timezoneString = GetString(meetingRequestElement, email + "TimeZone");
                        if (timezoneString != String.Empty)
                            msg.timeZone = timezoneString;

                        string availibilityString = GetString(meetingRequestElement, email + "BusyStatus");
                        if (availibilityString != String.Empty)
                            msg.availability = int.Parse(availibilityString);

                        //PK: We r getting mail in <>, we need to store after removing it
                        string organizerEmailString = GetString(meetingRequestElement, email + "Organizer");
                        int startPos = organizerEmailString.IndexOf("<");
                        int lastPos = organizerEmailString.IndexOf(">");
                        if (startPos != -1 && lastPos != -1)
                            msg.organizerEmail = organizerEmailString.Substring(startPos + 1, lastPos - (startPos + 1));
                        else
                            msg.organizerEmail = organizerEmailString;

                        msg.organizerName = cleanUpFrom(organizerEmailString);


                        string globalObjectIDString = GetString(meetingRequestElement, email + "GlobalObjId");
                        string uidString = GetUIDFromGlobalObjectID(globalObjectIDString).ToUpper();
                        msg.uid = uidString;

                        string acceptableFormat = AcceptableDateTime(endTimeString);
                        dt = System.Convert.ToDateTime(endTimeString).ToUniversalTime();
                        msg.endDayAndTime = dt.ToOADate();
                        /*<email:MeetingRequest>
                            <email:AllDayEvent>0</email:AllDayEvent>
                            <email:StartTime>2009-03-17T20:00:00.000Z</email:StartTime>
                            <email:DtStamp>2009-02-19T08:47:19.527Z</email:DtStamp>
                            <email:EndTime>2009-03-17T21:00:00.000Z</email:EndTime>
                            <email:InstanceType>1</email:InstanceType>
                            <email:Location>My Office</email:Location>
                            <email:Organizer>"Device User2" &lt;someone2@example.com&gt;</email:Organizer>
                            <email:Reminder>900</email:Reminder>
                            <email:ResponseRequested>1</email:ResponseRequested>
                            <email:Recurrences>
                            <email:Recurrence>
                            <email:Type>3</email:Type>
                            <email:Interval>1</email:Interval>
                            <email:Until>20091229T210000Z</email:Until>
                            <email:WeekOfMonth>3</email:WeekOfMonth>
                            <email:DayOfWeek>4</email:DayOfWeek>
                            </email:Recurrence>
                            </email:Recurrences>
                            <email:Sensitivity>0</email:Sensitivity>
                            <email:BusyStatus>2</email:BusyStatus>
                            <email:TimeZone>aAEAACgARwBNAFQALQAwADYAOgAwADAAKQAgAEMAZQBuAHQAcgBhAGwAIABUAGkAbQBlACAAKABVAFMAIAAmACAAQwAAAAsAAAABAAIAAAAAAAAAAAAAACgARwBNAFQALQAwADYAOgAwADAAKQAgAEMAZQBuAHQAcgBhAGwAIABUAGkAbQBlACAAKABVAFMAIAAmACAAQwAAAAMAAAACAAIAAAAAAAAAxP///w==</email:TimeZone>
                            <email:GlobalObjId>BAAAAIIA4AB0xbcQGoLgCAAAAADok5WnbpLJAQAAAAAAAAAAEAAAAP4Ao5IYwQdKiFkDBeGTtgY=</email:GlobalObjId>
                            </email:MeetingRequest>
                         */



                    }
                    //int msgid = msgManager.AddMessageInfo(con, msg);
                    // Pull attachment info
                    XElement attachments = GetXElement(appDataXElement, airsyncbase + "Attachments");
                    if (null != attachments)
                    {
                        msg.hasAttachment = true;
                        // Iterate through attachments
                        IEnumerable<XElement> attachmentElements = from query in attachments.Descendants().Where(p => p.Name == (airsyncbase + "Attachment")).ToList() select query;
                        foreach (XElement attachment in attachmentElements)
                        {
                            Attachment att = new Attachment();
                            att.id = Guid.NewGuid();
                            att.displayName = GetString(attachment, airsyncbase + "DisplayName");
                            att.fileReference = GetString(attachment, airsyncbase + "FileReference");
                            att.method = Convert.ToInt32(GetString(attachment, airsyncbase + "Method"));
                            att.message_id = messageId;
                            att.folder_id = msg.folder_id;
                            att.contentIdentifier = msg.serverIdentifier;
#if DEBUG
                            //string DbgMsg = String.Format("Add attachment CI:{0}   FR:{1}   FID{2}   ID{3}   MSGID{4}", att.contentIdentifier, att.fileReference, att.folder_id.ToString(), att.id.ToString(), att.message_id.ToString());
                            //LoggingWP8.WP8Logger.LogMessage(DbgMsg);
#endif
                        }
                    }
                    messages.Add(msg);
                }
            }
            catch (Exception)
            {
                continueWithParsing = false;
            }
            finally
            {
            }
            return messages;
        }

        //This function parse the data under command xml. Mainly its responsible for parsing of ApplicationDataXML
        //This function will be used for inbox, Sent items, junk e-mail and deleted items
        //currently its doing for "ADD" only
        static int counter = 0;

        static async public Task<List<MessageManager>> ParseMailAddCommandsAndUpdateDB(List<ServerSyncCommand> commandList, string folderID)
        {
            bool isException = false;
            string expMsg = string.Empty;

            string ns = "{AirSync}";
            string email = "{Email}";
            string tasks = "{Tasks}";
            string airsyncbase = "{AirSyncBase}";
            string email2 = "{Email2}";
            MessageManager msgManager = new MessageManager();
            AttachmentManager objAtt = new AttachmentManager();
            
            List<Message> messagesToAdd = new List<Message>();
            List<Attachment> attachmentsToAdd = new List<Attachment>();
            List<MessageManager> returnMessages = new List<MessageManager>();


            bool continueWithParsing = true;
            FolderManager objfldr = new FolderManager();
#if USE_ASYNC
            int draft_folder_id = await objfldr.GetFolderIdentifierBasedOnFolderNameAsync("Drafts");
#else
            int draft_folder_id = objfldr.GetFolderIdentifierBasedOnFolderName("Drafts");
#endif
            counter += 1;
            foreach (ServerSyncCommand addCommand in commandList)
            {
                Message msg = new Message();
                try
                {
                    XElement appDataXElement = addCommand.AppDataXml;

                    //reading data   

                    msg.serverIdentifier = addCommand.ServerId;
                    int messageId = Convert.ToInt32(msg.serverIdentifier.Split(':')[1]);
                    msg.id = messageId;
                    msg.toRecipientText = GetString(appDataXElement, email + "DisplayTo");

                    msg.toRecipientEmail = cleanUpCC(GetString(appDataXElement, email + "To"));
                    // From will be in the form of "Loren Rogers" <loren_rogers@kace.com>
                    // We want just the name
                    msg.displayFromEmail = cleanUpFrom(GetString(appDataXElement, email + "From"));
                    msg.fromEmail = cleanUpFromToGetMailID(GetString(appDataXElement, email + "From"));
                    //taking subject data instead of threadtopic
                    string strSubject = GetString(appDataXElement, email + "Subject");

                    if (!string.IsNullOrWhiteSpace(strSubject))
                    {
                        msg.subject = ConvertISO8859EncodingToUtf8(strSubject);
                    }
                    else
                    {
                        msg.subject = strSubject;
                    }

                    string dr = GetString(appDataXElement, email + "DateReceived");
                    DateTime dt = System.Convert.ToDateTime(dr).ToUniversalTime(); ;
                    msg.receivedDate =dt.ToOADate();

                    XElement bodyElement = GetXElement(appDataXElement, airsyncbase + "Body");
                    if (null != bodyElement)
                    {
                        msg.bodyText = GetString(bodyElement, airsyncbase + "Data");

                        string previewText = GetString(bodyElement, airsyncbase + "Preview");
                        if (null == previewText || previewText.Length == 0)
                        {
                            // Convert from html to plain text
                            HtmlToText htmlToText = new HtmlToText();
                            previewText = htmlToText.ConvertHtml(msg.bodyText);
                        }

                        msg.previewText = previewText;
                    }
                    //appDataXElement.Element(email + "Categories").Value

                    string readString = GetString(appDataXElement, email + "Read");
                    if (readString != string.Empty)
                        msg.read = int.Parse(readString);

                    string importanceString = GetString(appDataXElement, email + "Importance");
                    if (importanceString != string.Empty)
                        msg.importance = int.Parse(importanceString);

                    if(counter >= 30)
                    {
                        counter = 0;
                        //throw new Exception("Test Exception");
                    }
                    XElement flagElement = GetXElement(appDataXElement, email + "Flag");
                    if (null != flagElement && flagElement.HasElements)
                    {
                        /*
                          <tasks:DueDate xmlns:tasks="Tasks">2014-08-11T14:13:53.000Z</tasks:DueDate> 
                          <tasks:UtcDueDate xmlns:tasks="Tasks">2014-08-11T14:13:53.000Z</tasks:UtcDueDate> 
                          <tasks:UtcStartDate xmlns:tasks="Tasks">2014-08-04T14:13:53.000Z</tasks:UtcStartDate> 
                          <email:Status>2</email:Status> 
                          <email:FlagType>FollowUp</email:FlagType> 
                          <tasks:StartDate xmlns:tasks="Tasks">2014-08-04T14:13:53.000Z</tasks:StartDate> 
                          <tasks:ReminderSet xmlns:tasks="Tasks">0</tasks:ReminderSet> 
                         */

                        XElement duedateElement = GetXElement(flagElement, tasks + "DueDate");
                        if (duedateElement != null)
                        {
                            string dd = GetString(flagElement, tasks + "DueDate");
                            DateTime dueDate = System.Convert.ToDateTime(dd).ToUniversalTime();
                            msg.flagDueDate = dueDate.ToOADate();
                        }


                        XElement startdateElement = GetXElement(flagElement, tasks + "StartDate");
                        if (startdateElement != null)
                        {
                            string sd = GetString(flagElement, tasks + "StartDate");
                            DateTime startDate = System.Convert.ToDateTime(sd).ToUniversalTime();
                            msg.flagStartDate = startDate.ToOADate();
                        }

                        msg.flagType = GetString(flagElement, email + "FlagType");
                        msg.flagStatus = int.Parse(GetString(flagElement, email + "Status"));
                        if (msg.flagStatus == 1)
                            msg.flag = 0;
                        else
                            msg.flag = 1;

                    }
                    else
                        msg.flag = 0;

                    //ccrecipent
                    string ccText = cleanUpCC(GetString(appDataXElement, email + "Cc"));
                    msg.ccRecipientText = ccText;
                    
                    string ifBccRecipient = GetString(appDataXElement, email2 + "ReceivedAsBcc");
                    if (ifBccRecipient != string.Empty)
                    {
                        if (ifBccRecipient == "1")
                            msg.isRecipientAsBcc = true;
                        else if (ifBccRecipient == "0")
                            msg.isRecipientAsBcc = false;
                        else if(ifBccRecipient.ToLower() == "true" || ifBccRecipient.ToLower() == "false")
                            msg.isRecipientAsBcc = Convert.ToBoolean(ifBccRecipient);
                    }
					
					//for sake of variable existence, we will be assigning it. We need this for data binding for new mail
					msg.bccRecipientText = "";

                    //folderid: we are taking id from call. Not needed fro XML ?
                    msg.folder_id = Convert.ToInt32(msg.serverIdentifier.Split(':')[0]);

                    //mime type
                    //msg.mimeType = GetString(appDataXElement,email +  );
                    //conversation ID
                    msg.conversationId = GetString(appDataXElement, email2 + "ConversationId");
                    msg.conversationIndex = GetString(appDataXElement, email2 + "ConversationIndex");


                    String lastVerbExecutedString = GetString(appDataXElement, email2 + "LastVerbExecuted");
                    if (lastVerbExecutedString != String.Empty)
                        msg.lastVerbExecuted = int.Parse(lastVerbExecutedString);
                    //stored message class to identify event is aceepted or decline or canceled
                    String messageClass = GetString(appDataXElement, email + "MessageClass");
                    if(!string.IsNullOrWhiteSpace(messageClass))
                    {
                        msg.messageClass = messageClass;
                    }
                    // Need to set up if this is a calendar meeting request
                    XElement meetingRequestElement = GetXElement(appDataXElement, email + "MeetingRequest");
                    if (null != meetingRequestElement)
                    {
                        // The body text becomes the note
                        //25th oct 2014,I am commenting below line becasue becasue it was storing blank value in message table, and user was not able to see notes in meeting request
                        // msg.bodyText = "";
                        // Until we get calendar set up, for now just set event_id to 1
                        msg.event_id = 1;

                        string allDayString = GetString(meetingRequestElement, email + "AllDayEvent");
                        if (allDayString != String.Empty)
                            msg.allDay = int.Parse(allDayString);


                        string startTimeString = GetString(meetingRequestElement, email + "StartTime");
                        dt = System.Convert.ToDateTime(startTimeString).ToUniversalTime();
                        msg.startDayAndTime = dt.ToOADate();


                        string endTimeString = GetString(meetingRequestElement, email + "EndTime");
                        dt = System.Convert.ToDateTime(endTimeString).ToUniversalTime();
                        msg.endDayAndTime = dt.ToOADate();


                        string locationString = GetString(meetingRequestElement, email + "Location");
                        if (locationString != String.Empty)
                            msg.location = ConvertISO8859EncodingToUtf8(locationString);

                        string timezoneString = GetString(meetingRequestElement, email + "TimeZone");
                        if (timezoneString != String.Empty)
                            msg.timeZone = timezoneString;

                        string availibilityString = GetString(meetingRequestElement, email + "BusyStatus");
                        if (availibilityString != String.Empty)
                            msg.availability = int.Parse(availibilityString);

                        //PK: We r getting mail in <>, we need to store after removing it
                        string organizerEmailString = GetString(meetingRequestElement, email + "Organizer");
                        int startPos = organizerEmailString.IndexOf("<");
                        int lastPos = organizerEmailString.IndexOf(">");
                        if (startPos != -1 && lastPos != -1)
                            msg.organizerEmail = organizerEmailString.Substring(startPos + 1, lastPos - (startPos + 1));
                        else
                            msg.organizerEmail = organizerEmailString;

                        msg.organizerName = cleanUpFrom(organizerEmailString);
                        //Added to check response is requested or not
                        string responseRequested = GetString(meetingRequestElement, email + "ResponseRequested");
                        if (responseRequested != String.Empty)
                            msg.responseRequested = int.Parse(responseRequested);

                        string globalObjectIDString = GetString(meetingRequestElement, email + "GlobalObjId");
                        string uidString = GetUIDFromGlobalObjectID(globalObjectIDString).ToUpper();
                        msg.uid = uidString;

                        string acceptableFormat = AcceptableDateTime(endTimeString);
                        dt = System.Convert.ToDateTime(endTimeString).ToUniversalTime();
                        msg.endDayAndTime = dt.ToOADate();
                        /*<email:MeetingRequest>
                            <email:AllDayEvent>0</email:AllDayEvent>
                            <email:StartTime>2009-03-17T20:00:00.000Z</email:StartTime>
                            <email:DtStamp>2009-02-19T08:47:19.527Z</email:DtStamp>
                            <email:EndTime>2009-03-17T21:00:00.000Z</email:EndTime>
                            <email:InstanceType>1</email:InstanceType>
                            <email:Location>My Office</email:Location>
                            <email:Organizer>"Device User2" &lt;someone2@example.com&gt;</email:Organizer>
                            <email:Reminder>900</email:Reminder>
                            <email:ResponseRequested>1</email:ResponseRequested>
                            <email:Recurrences>
                            <email:Recurrence>
                            <email:Type>3</email:Type>
                            <email:Interval>1</email:Interval>
                            <email:Until>20091229T210000Z</email:Until>
                            <email:WeekOfMonth>3</email:WeekOfMonth>
                            <email:DayOfWeek>4</email:DayOfWeek>
                            </email:Recurrence>
                            </email:Recurrences>
                            <email:Sensitivity>0</email:Sensitivity>
                            <email:BusyStatus>2</email:BusyStatus>
                            <email:TimeZone>aAEAACgARwBNAFQALQAwADYAOgAwADAAKQAgAEMAZQBuAHQAcgBhAGwAIABUAGkAbQBlACAAKABVAFMAIAAmACAAQwAAAAsAAAABAAIAAAAAAAAAAAAAACgARwBNAFQALQAwADYAOgAwADAAKQAgAEMAZQBuAHQAcgBhAGwAIABUAGkAbQBlACAAKABVAFMAIAAmACAAQwAAAAMAAAACAAIAAAAAAAAAxP///w==</email:TimeZone>
                            <email:GlobalObjId>BAAAAIIA4AB0xbcQGoLgCAAAAADok5WnbpLJAQAAAAAAAAAAEAAAAP4Ao5IYwQdKiFkDBeGTtgY=</email:GlobalObjId>
                            </email:MeetingRequest>
                         */



                    }
                    //code for storing draft message in draft_parts table
                    List<Draft_Parts> lstdraftattachment = new List<Draft_Parts>();
                    //end draft parts
                    //int msgid = msgManager.AddMessageInfo(con, msg);
                    // Pull attachment info
                    XElement attachments = GetXElement(appDataXElement, airsyncbase + "Attachments");
                    if (null != attachments)
                    {
                        msg.hasAttachment = true;
                        // Iterate through attachments
                        IEnumerable<XElement> attachmentElements = from query in attachments.Descendants().Where(p => p.Name == (airsyncbase + "Attachment")).ToList() select query;
                        foreach (XElement attachment in attachmentElements)
                        {
                            Attachment att = new Attachment();
                            att.id = Guid.NewGuid();
                            att.displayName = GetString(attachment, airsyncbase + "DisplayName");
                            att.fileReference = GetString(attachment, airsyncbase + "FileReference");
                            att.method = Convert.ToInt32(GetString(attachment, airsyncbase + "Method"));
                            att.message_id = messageId;
                            att.folder_id = msg.folder_id;
                            att.contentIdentifier = msg.serverIdentifier;
#if DEBUG
                            //string DbgMsg = String.Format("Add attachment CI:{0}   FR:{1}   FID{2}   ID{3}   MSGID{4}", att.contentIdentifier, att.fileReference, att.folder_id.ToString(), att.id.ToString(), att.message_id.ToString());
                            //LoggingWP8.WP8Logger.LogMessage(DbgMsg);
#endif
                            try
                            {
#if USE_ASYNC
                                if (msg.folder_id == draft_folder_id)
                                {
                                    Draft_Parts attachforDraft = new Draft_Parts();
                                    attachforDraft.partType = (int)DraftPartsType.PartAttachment;
                                    attachforDraft.contentType = att.contentType;
                                    attachforDraft.content = att.fileReference;
                                    attachforDraft.extData = att.displayName;
                                    lstdraftattachment.Add(attachforDraft);
                                }
                                else
                                {
                                    // LWR 12/18/2014 TEST await objAtt.AddAttachmentInfoAsync(ds.Db, att);
                                    attachmentsToAdd.Add(att);
                                }
#else
                                addAttachments.Add(att);
#endif
                            }
                            catch (SQLiteException ex)
                            {
                                if (ex.Message == "Constraint")
                                {
                                    // This seems to be happening on Deleted Items
                                    continueWithParsing = true;
                                }
                            }
                            catch (FormatException)
                            {
                                // Let's just move on if we get a FormatException
                                continueWithParsing = true;
                            }
                            catch (Exception)
                            {
                                continueWithParsing = false;
                                break;
                            }
                        }
                        
                    }
                    //Need to write calender related mail component
                    try
                    {
#if USE_ASYNC
                        if (msg.folder_id == draft_folder_id)
                        {
                            MessageManager objmsg = new MessageManager();
                            await objmsg.AddMessageInDraft(msg, lstdraftattachment);

                        }
                        else
                        {
                            //System.Diagnostics.Debug.WriteLine("M_ID: {0}  M_SUBJ: {1}   M_DATE: {2}  M_TIME: {3}", msg.id, msg.subject,  DateTime.FromOADate(msg.receivedDate).ToShortDateString(), DateTime.FromOADate(msg.receivedDate).ToShortTimeString());
                            messagesToAdd.Add(msg);
                            MessageManager returnMessage = new MessageManager();
                            
                            returnMessage.attach_MessageId = messageId;    // Is this correct?
                            returnMessage.attachment_serverId = msg.serverIdentifier;   // Is this correct?
                            returnMessage.displayFromEmail = msg.displayFromEmail;
                            returnMessage.event_id = msg.event_id;
                            returnMessage.flag = msg.flag;
                            returnMessage.flagDueDate = msg.flagDueDate;
                            returnMessage.flagReminderSet = msg.flagReminderSet;
                            returnMessage.flagStartDate = msg.flagStartDate;
                            returnMessage.flagStatus = msg.flagStatus;
                            returnMessage.flagUTCDueDate = msg.flagUTCDueDate;
                            returnMessage.flagUTCStartDate = msg.flagUTCStartDate;
                            returnMessage.folderId = msg.folder_id;
                            returnMessage.fromEmail = msg.fromEmail;
                            returnMessage.hasAttachment = msg.hasAttachment;
                            returnMessage.importance = msg.importance;
                            returnMessage.index = 0;    // Not used
                            returnMessage.lastVerbExecuted = msg.lastVerbExecuted;
                            returnMessage.messageId = msg.id;
                            returnMessage.previewText = msg.previewText;
                            returnMessage.read = msg.read;
                            returnMessage.receivedDate = msg.receivedDate;
                            returnMessage.serverID = msg.serverIdentifier;
                            returnMessage.subject = msg.subject;
                            
                            returnMessages.Add(returnMessage);

                        }
#else
                        msgList.Add(msg);
#endif

#if !USE_ASYNC
                        msgManager.AddAllMessageInfo(ds.Db, msgList);
                        objAtt.AddAllAttachfo(ds.Db, addAttachments);
#endif

                    }
                    catch (SQLiteException ex)
                    {
                        if (ex.Message == "Constraint")
                        {
                            // This seems to be happening on Deleted Items
                            continueWithParsing = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                catch (SQLiteException)
                {
                }
                catch (System.ArgumentOutOfRangeException)
                {
                    // Bob Keathley is getting this when syncing with 0.9.0.27
                    // Loren Rogers kace account 0.9.0.46
                    //string Message = "Problematic Email subject: '" + msg.subject + "'";
                    //System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                    //{
                    //    System.Windows.MessageBox.Show(Message, "", System.Windows.MessageBoxButton.OK);
                    //});
                    isException = true;
                    expMsg = "Problematic Email subject: '" + msg.subject + "'";
                    continueWithParsing = true;
                }
                catch (Exception)
                {
                    continueWithParsing = false;
                }

                if (!continueWithParsing)
                {
                   // ds.Dispose();
                   // ds = null;
                    break;
                }

                if (isException)
                {
                    await ShowMessageBox(expMsg);
                }
            }

            // LOREN ROGERS TEST 12/18/2014: Do block insert of attachments and Message objects
            await msgManager.AddAllMessageInfoAsync(messagesToAdd);
            await objAtt.AddAllAttachInfoAsync(attachmentsToAdd);          
            return returnMessages;
        }

        //static private string GetUIDFromGlobalObjectID(string globalObjectID)
        //{
        //    string uidString = string.Empty;
        //    byte[] data = Convert.FromBase64String(globalObjectID);
        //    return uidString;
        //}
        static private string GetUIDFromGlobalObjectID(string globalObjectID)
        {
            string uidString = string.Empty;
            bool isOutlookID = false;
            Int32 uidLength = 0;
            byte[] rawData = Convert.FromBase64String(globalObjectID);

            byte[] typeRefData = Encoding.UTF8.GetBytes("vCal-Uid");
            //byte[] typeRefData = GetBytes("vCal-Uid");

            if (rawData.Length < 53)
            {
                isOutlookID = true;
            }
            else
            {
                byte[] rawDataType = new byte[8];
                Array.Copy(rawData, 40, rawDataType, 0, 8);

                if (!rawDataType.SequenceEqual(typeRefData))
                {
                    isOutlookID = true;
                }
                else
                {
                    byte[] secondArray = new byte[4];
                    Array.Copy(rawData, 36, secondArray, 0, 4);
                    uidLength = BitConverter.ToInt32(rawData, 36);

                    isOutlookID = uidLength < 13 || uidLength - 13 > rawData.Length - 53;

                }

                if (isOutlookID)
                {
                    rawData[16] = 0;
                    rawData[17] = 0;
                    rawData[18] = 0;
                    rawData[19] = 0;

                    StringBuilder hex = new StringBuilder(rawData.Length * 2);
                    foreach (byte b in rawData)
                        hex.AppendFormat("{0:x2}", b);
                    return hex.ToString();


                }
                else
                {
                    uidLength -= 13;
                    byte[] thirdArray = new byte[uidLength];
                    Array.Copy(rawData, 52, thirdArray, 0, uidLength);
                    string thirdString = Encoding.UTF8.GetString(thirdArray, 0, thirdArray.Length);
                    return thirdString;

                }



            }
            return null;
        }

        static private async Task<List<MessageManager>> ParseMailChangeCommandsAndUpdateDB(List<ServerSyncCommand> commandList, string folderID)
        {
            string email = "{Email}";
            string tasks = "{Tasks}";

            MessageManager msgManager = new MessageManager();
            AttachmentManager objAtt = new AttachmentManager();
            List<MessageManager> returnMessages = new List<MessageManager>();

            XElement appDataXElement = null;

            try
            {
                foreach (ServerSyncCommand changeCommand in commandList)
                {
                    appDataXElement = changeCommand.AppDataXml;


                    String serverID = changeCommand.ServerId;
                    int folder_id = Convert.ToInt32(serverID.Split(':')[0]);
                    int messageID = Convert.ToInt32(serverID.Split(':')[1]);

                    // WPMW-878 and WPMW-879: For some reason, msgManager.GetMessageInfo() is returning a count of 0
                    // sometimes.  And this throws the ArgumentOutOfRangeException.  The question is WHY can't the message[serverID]
                    // be found?
#if USE_ASYNC
                    List<Message> messages = await msgManager.GetMessageInfoAsync(folder_id, messageID);
#else
                    List<Message> messages = msgManager.GetMessageInfo(folder_id, messageID);
#endif
                    if(messages != null && messages.Count > 0)
                    {
                        Message msg = messages[0];
                        if (msg != null)
                        {
                            //flag
                            XElement flagElement = GetXElement(appDataXElement, email + "Flag");
                            if (null != flagElement)
                            {
                                if (flagElement.HasElements)
                                {
                                    XElement duedateElement = GetXElement(flagElement, tasks + "DueDate");
                                    if (duedateElement != null)
                                    {
                                        string dd = GetString(flagElement, tasks + "DueDate");
                                        DateTime dueDate = System.Convert.ToDateTime(dd);
                                        msg.flagDueDate = dueDate.ToOADate();
                                    }


                                    XElement startdateElement = GetXElement(flagElement, tasks + "StartDate");
                                    if (startdateElement != null)
                                    {
                                        string sd = GetString(flagElement, tasks + "StartDate");
                                        DateTime startDate = System.Convert.ToDateTime(sd);
                                        msg.flagStartDate = startDate.ToOADate();
                                    }

                                    msg.flagType = GetString(flagElement, email + "FlagType");
                                    msg.flagStatus = int.Parse(GetString(flagElement, email + "Status"));

                                    if (msg.flagStatus == 1)
                                        msg.flag = 0;
                                    else
                                        msg.flag = 1;
                                }
                                else
                                {
                                    msg.flagType = "";
                                    msg.flagStatus = 0;
                                    msg.flag = 0;
                                }
                            }

                            //Read
                            XElement readElement = GetXElement(appDataXElement, email + "Read");
                            if (readElement != null)
                                msg.read = Convert.ToInt32(readElement.Value);
                            {
                                MessageManager returnMessage = new MessageManager();

                                returnMessage.attach_MessageId = msg.id;    // Is this correct?
                                returnMessage.attachment_serverId = msg.serverIdentifier;   // Is this correct?
                                returnMessage.displayFromEmail = msg.displayFromEmail;
                                returnMessage.event_id = msg.event_id;
                                returnMessage.flag = msg.flag;
                                returnMessage.flagDueDate = msg.flagDueDate;
                                returnMessage.flagReminderSet = msg.flagReminderSet;
                                returnMessage.flagStartDate = msg.flagStartDate;
                                returnMessage.flagStatus = msg.flagStatus;
                                returnMessage.flagUTCDueDate = msg.flagUTCDueDate;
                                returnMessage.flagUTCStartDate = msg.flagUTCStartDate;
                                returnMessage.folderId = msg.folder_id;
                                returnMessage.fromEmail = msg.fromEmail;
                                returnMessage.hasAttachment = msg.hasAttachment;
                                returnMessage.importance = msg.importance;
                                returnMessage.index = 0;    // Not used
                                returnMessage.lastVerbExecuted = msg.lastVerbExecuted;
                                returnMessage.messageId = msg.id;
                                returnMessage.previewText = msg.previewText;
                                returnMessage.read = msg.read;
                                returnMessage.receivedDate = msg.receivedDate;
                                returnMessage.serverID = msg.serverIdentifier;
                                returnMessage.subject = msg.subject;

                                returnMessages.Add(returnMessage);
                            }

#if USE_ASYNC
                            await msgManager.UpdateMessageInfoAsync(msg);
#else
                            msgManager.UpdateMessageInfo(msg);
#endif
                        }
                    }
                    else
                    {
                        // The message was not found. What do we do here?
                        string Message = "Message Not found: '" + serverID + "'";
#if WP_80_SILVERLIGHT
                        System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
                        {
                            System.Windows.MessageBox.Show(Message, "", System.Windows.MessageBoxButton.OK);
                        });
#else
                        await ShowMessageBox(Message);
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                StringWriter sw = new StringWriter();
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Async = false;
                settings.Indent = true;
                XmlWriter writer = XmlWriter.Create(sw, settings);
                appDataXElement.WriteTo(writer);
                writer.Flush();

                LoggingWP8.WP8Logger.LogMessage("ParseMailChangeCommandsAndUpdateDB() EXCEPTION {0}, XML   {1}", ex.Message, sw.ToString());
                
                throw ex;
            }
            finally
            {
                //ds.Dispose();
            }

            return returnMessages;
        }


        static public async Task<List<MessageManager>> ParseMailDeleteCommandAndUpdateDB(List<ServerSyncCommand> deleteCommandList)
        {

            MessageManager msgManager = new MessageManager();
            AttachmentManager attachmentMgr = new AttachmentManager();
            List<Message> messeges =new List<Message>();
            List<Attachment> attachments =new List<Attachment>();
            List<MessageManager> returnMessages = new List<MessageManager>();
            try
            {
                foreach (ServerSyncCommand deleteCommand in deleteCommandList)
                {
                    string serverIdentifier = deleteCommand.ServerId;
                    int msgeId = Convert.ToInt32(deleteCommand.ServerId.Split(':')[1]);
                    int folderId = Convert.ToInt32(deleteCommand.ServerId.Split(':')[0]);
                   //storen messsage info in list
                    Message msg = new Message();
                    msg.folder_id = folderId;
                    msg.id = msgeId;
                    msg.serverIdentifier = serverIdentifier;
                    MessageManager returnMessage = new MessageManager();

                    returnMessage.attach_MessageId = msgeId;    // Is this correct?
                    returnMessage.attachment_serverId = msg.serverIdentifier;   // Is this correct?
                    returnMessage.event_id = msg.event_id;
                    returnMessage.folderId = msg.folder_id;
                    returnMessage.index = 0;    // Not used
                    returnMessage.messageId = msg.id;
                    returnMessage.serverID = msg.serverIdentifier;

                    returnMessages.Add(returnMessage);

                    messeges.Add(msg);
                    //store attachment info in list
                    Attachment attach = new Attachment();
                    attach.folder_id = folderId;
                    attach.message_id = msgeId;                    
                    attachments.Add(attach);
#if USE_ASYNC
                    //int msgeId = msgManager.GetMessageIDFromServerIdAsync(serverIdentifier).Result;
#else
                    int msgeId = msgManager.GetMessageIDFromServerId(serverIdentifier);
#endif

                    if (msgeId != -1)
                    {
#if USE_ASYNC
                       //Commneted by Dhruv on 1 jan 2015 
                       // await attachmentMgr.DeleteAttachmentInfoWithMessageIdAsync(msgeId);
                       // await msgManager.DeleteMessageInfoAsync(ds.Db, folderId, msgeId);    
#else
                        attachmentMgr.DeleteAttachmentInfoWithMessageId(msgeId);
                        msgManager.DeleteMessageInfo(ds.Db, msgeId);
#endif
                    }

                }
                //Dhruv 01 Jan 2015, Added method to delete all message using transaction                
                await msgManager.DeletMultipleMsgwithAttachmentAsync(messeges,attachments);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
               // ds.Dispose();
            }
            return returnMessages;
        }

        static public List<MessageManager> ParseSearchMessages(XDocument responseXml)
        {
            if (responseXml == null)
                return null;
            List<MessageManager> messageManagerList = new List<MessageManager>();
            string airSyncNS = "{AirSync}";
            string airsyncbaseNS = "{AirSyncBase}";
            string mailNS = "{Email}";
            string mailNS2 = "{Email2}";
            string tasksNS = "{Tasks}";
            string searchNS = "{Search}";
            var searchElements = responseXml.Element(searchNS + "Search").
                                                    Element(searchNS + "Response").
                                                    Element(searchNS + "Store").
                                                    Elements(searchNS + "Result").ToList();

            foreach (XElement element in searchElements)
            {
                MessageManager msgMgr = new MessageManager();
                
                try
                {
                    if (element.Element(searchNS + "Properties") == null)
                        return messageManagerList;

                    msgMgr.event_id = 0;
                    XElement toFieldXElement = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == mailNS + "To");
                    if(null != toFieldXElement)
                        msgMgr.toRecipientText = cleanUpFromToGetMailID(toFieldXElement.Value);
                   
                    XElement fromXElement = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == mailNS + "From");
                    if (null != fromXElement)
                    {
                        msgMgr.fromEmail = fromXElement.Value;
                        msgMgr.displayFromEmail = cleanUpFromToGetMailID(fromXElement.Value);
                    }

                    XElement subjectXElement = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == mailNS + "Subject");
                    if (null != subjectXElement)
                        msgMgr.subject = subjectXElement.Value;

                    XElement dateReceivedElement = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == mailNS + "DateReceived");
                    if(null != dateReceivedElement)
                    {
                        string dateReceivedStr = dateReceivedElement.Value;
                        DateTime dt = System.Convert.ToDateTime(dateReceivedStr).ToUniversalTime(); ;
                        msgMgr.receivedDate = dt.ToOADate();
                    }

                    XElement importanceXElement = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == mailNS + "Importance");
                    if (null != importanceXElement)
                        msgMgr.importance = int.Parse(importanceXElement.Value);

                    XElement readXElement = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == mailNS + "Read");
                    if (null != readXElement)
                        msgMgr.read = int.Parse(readXElement.Value);


                    XElement bodyElement = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == airsyncbaseNS + "Body");
                    if (null != bodyElement)
                    {
                        msgMgr.bodyText = GetString(bodyElement, airsyncbaseNS + "Data");

                        string previewText = GetString(bodyElement, airsyncbaseNS + "Preview");
                        if(!string.IsNullOrEmpty(previewText))
                        {
                            // Convert from html to plain text
                            HtmlToText htmlToText = new HtmlToText();
                            previewText = htmlToText.ConvertHtml(msgMgr.bodyText);
                        }
                        else
                        {
                            previewText = msgMgr.bodyText.Substring(0, Math.Min(msgMgr.bodyText.Length, 100));
                        }

                        msgMgr.previewText = previewText;
                    }
                    
                    // If this is a meeting request
                    // Need to set up if this is a calendar meeting request
                    XElement meetingRequestElement = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == mailNS + "MeetingRequest");
                    if (null != meetingRequestElement)
                    {
                        DateTime dt = DateTime.MinValue;

                        // Until we get calendar set up, for now just set event_id to 1
                        msgMgr.event_id = 1;

                        string allDayString = GetString(meetingRequestElement, mailNS + "AllDayEvent");
                        if (allDayString != String.Empty)
                            msgMgr.allDay = int.Parse(allDayString);


                        string startTimeString = GetString(meetingRequestElement, mailNS + "StartTime");
                        //string acceptableFormat = AcceptableDateTime(startTimeString);
                        dt = System.Convert.ToDateTime(startTimeString).ToUniversalTime();
                        msgMgr.startDayAndTime = dt.ToOADate();


                        string endTimeString = GetString(meetingRequestElement, mailNS + "EndTime");
                        //string acceptableFormat = AcceptableDateTime(endTimeString);
                        dt = System.Convert.ToDateTime(endTimeString).ToUniversalTime();
                        msgMgr.endDayAndTime = dt.ToOADate();

                        string locationString = GetString(meetingRequestElement, mailNS + "Location");
                        if (allDayString != String.Empty)
                            msgMgr.location = locationString;

                        string timezoneString = GetString(meetingRequestElement, mailNS + "TimeZone");
                        if (allDayString != String.Empty)
                            msgMgr.timeZone = timezoneString;

                        string availibilityString = GetString(meetingRequestElement, mailNS + "BusyStatus");
                        if (allDayString != String.Empty)
                            msgMgr.availability = int.Parse(availibilityString);

                        //PK: We r getting mail in <>, we need to store after removing it
                        string organizerEmailString = GetString(meetingRequestElement, mailNS + "Organizer");
                        int startPos = organizerEmailString.IndexOf("<");
                        int lastPos = organizerEmailString.IndexOf(">");
                        if (startPos != -1 && lastPos != -1)
                            msgMgr.organizerEmail = organizerEmailString.Substring(startPos + 1, lastPos - (startPos + 1));
                        else
                            msgMgr.organizerEmail = organizerEmailString;

                        msgMgr.organizerName = cleanUpFrom(organizerEmailString);


                        string globalObjectIDString = GetString(meetingRequestElement, mailNS + "GlobalObjId");
                        string uidString = GetUIDFromGlobalObjectID(globalObjectIDString).ToUpper();
                        msgMgr.uid = uidString;

                        string acceptableFormat = AcceptableDateTime(endTimeString);
                        dt = System.Convert.ToDateTime(endTimeString).ToUniversalTime();
                        msgMgr.endDayAndTime = dt.ToOADate();
                    }

                    //XElement flagElement = GetXElement(element, mailNS + "Flag");
                    XElement flagElement = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == mailNS + "Flag");
                    if (null != flagElement && flagElement.HasElements)
                    {

                        XElement duedateElement = GetXElement(flagElement, mailNS + "DueDate");
                        if (duedateElement != null)
                        {
                            string dd = GetString(flagElement, tasksNS + "DueDate");
                            DateTime dueDate = System.Convert.ToDateTime(dd).ToUniversalTime();
                            msgMgr.flagDueDate = dueDate.ToOADate();
                        }


                        XElement startdateElement = GetXElement(flagElement, tasksNS + "StartDate");
                        if (startdateElement != null)
                        {
                            string sd = GetString(flagElement, tasksNS + "StartDate");
                            DateTime startDate = System.Convert.ToDateTime(sd).ToUniversalTime();
                            msgMgr.flagStartDate = startDate.ToOADate();
                        }

                        //msgMgr.flagType = GetString(flagElement, mailNS + "FlagType");
                        msgMgr.flagStatus = int.Parse(GetString(flagElement, mailNS + "Status"));
                        if (msgMgr.flagStatus == 1)
                            msgMgr.flag = 0;
                        else
                            msgMgr.flag = 1;

                    }
                    else
                        msgMgr.flag = 0;

                }
                catch
                {

                }
                messageManagerList.Add(msgMgr);
            }

            return messageManagerList;

        }

        #region Contact Commands Parsing

        static public void ParseContactAddCommandAndUpdateDB(List<ServerSyncCommand> commandList, string folderID)
        {

            string contactsNS = "{Contacts}";

            ContactManager contactsManager = new ContactManager();
            List<Contacts> contactList = new List<Contacts>();

            try
            {
                foreach (ServerSyncCommand addCommand in commandList)
                {
                    XElement appDataXElement = addCommand.AppDataXml;
                    Contacts contact = new Contacts();

                    contact.serverId = addCommand.ServerId;
                    contact.folder_id = Convert.ToInt32(contact.serverId.Split(':')[0]);
                    contact.id = Convert.ToInt32(contact.serverId.Split(':')[1]);

                    contact.firstName = GetString(appDataXElement, contactsNS + "FirstName");
                    contact.lastName = GetString(appDataXElement, contactsNS + "LastName");

                    contact.assistantPhoneNumber = GetString(appDataXElement, contactsNS + "AssistantPhoneNumber");
                    contact.business2PhoneNumber = GetString(appDataXElement, contactsNS + "Business2PhoneNumber");

                    contact.businessAddressCity = GetString(appDataXElement, contactsNS + "BusinessAddressCity");

                    contact.businessAddressCountry = GetString(appDataXElement, contactsNS + "BusinessAddressCountry");
                    contact.businessAddressPostalCode = GetString(appDataXElement, contactsNS + "BusinessAddressPostalCode");
                    contact.businessAddressState = GetString(appDataXElement, contactsNS + "BusinessAddressState");
                    contact.businessAddressStreet = GetString(appDataXElement, contactsNS + "BusinessAddressStreet");
                    contact.businessFaxNumber = GetString(appDataXElement, contactsNS + "BusinessFaxNumber");
                    contact.businessPhoneNumber = GetString(appDataXElement, contactsNS + "BusinessPhoneNumber");
                    contact.companyName = GetString(appDataXElement, contactsNS + "CompanyName");

                    //Pankaj: ToDo: need to know fields to map these?
                    //contact.deleted = Convert.ToInt32(GetString(appDataXElement, contactsNS + "AssistantPhoneNumber"));
                    //contact.dirty = Convert.ToInt32(GetString(appDataXElement, contactsNS + "AssistantPhoneNumber"));
                    //contact.fallbackDisplay = GetString(appDataXElement, contactsNS + "AssistantPhoneNumber");

                    //PK: We are getting email string like "lorenr@dmvdev1.com" <lorenr@dmvdev1.com> .
                    string emailString = GetString(appDataXElement, contactsNS + "Email1Address");
                    int startPos = emailString.IndexOf("<");
                    int lastPos = emailString.IndexOf(">");
                    if (startPos != -1 && lastPos != -1)
                        contact.email1Address = emailString.Substring(startPos + 1, lastPos - (startPos + 1));

                    string email2String = GetString(appDataXElement, contactsNS + "Email2Address");
                    startPos = email2String.IndexOf("<");
                    lastPos = email2String.IndexOf(">");
                    if (startPos != -1 && lastPos != -1)
                        contact.email2Address = email2String.Substring(startPos + 1, lastPos - (startPos + 1));

                    string email3String = GetString(appDataXElement, contactsNS + "Email3Address");
                    startPos = email3String.IndexOf("<");
                    lastPos = email3String.IndexOf(">");
                    if (startPos != -1 && lastPos != -1)
                        contact.email3Address = email3String.Substring(startPos + 1, lastPos - (startPos + 1));

                    //contact.email1Address = GetString(appDataXElement, contactsNS + "Email1Address");
                    //contact.email2Address = GetString(appDataXElement, contactsNS + "Email2Address");
                    //contact.email3Address = GetString(appDataXElement, contactsNS + "Email3Address");

                    contact.home2PhoneNumber = GetString(appDataXElement, contactsNS + "Home2PhoneNumber");
                    contact.homeAddressCity = GetString(appDataXElement, contactsNS + "HomeAddressCity");
                    contact.homeAddressCountry = GetString(appDataXElement, contactsNS + "HomeAddressCountry");
                    contact.homeAddressPostalCode = GetString(appDataXElement, contactsNS + "HomeAddressPostalCode");
                    contact.homeAddressState = GetString(appDataXElement, contactsNS + "HomeAddressState");
                    contact.homeAddressStreet = GetString(appDataXElement, contactsNS + "HomeAddressStreet");
                    contact.homeFaxNumber = GetString(appDataXElement, contactsNS + "HomeFaxNumber");
                    contact.homePhoneNumber = GetString(appDataXElement, contactsNS + "HomePhoneNumber");

                    contact.jobTitle = GetString(appDataXElement, contactsNS + "JobTitle");
                    contact.mobilePhoneNumber = GetString(appDataXElement, contactsNS + "MobilePhoneNumber");

                    contact.otherAddressCity = GetString(appDataXElement, contactsNS + "OtherAddressCity");
                    contact.otherAddressCountry = GetString(appDataXElement, contactsNS + "OtherAddressCountry");
                    contact.otherAddressPostalCode = GetString(appDataXElement, contactsNS + "OtherAddressPostalCode");
                    contact.otherAddressState = GetString(appDataXElement, contactsNS + "OtherAddressState");
                    contact.otherAddressStreet = GetString(appDataXElement, contactsNS + "OtherAddressStreet");

                    contactList.Add(contact);

                    //Pankaj: To Do: We need to remove below line of code 
                    //contactsManager.AddContactInfo(contact);
#if USE_ASYNC
                    contactsManager.AddContactInfoAsync(contact);
#else
                    contactsManager.AddContactInfo(ds.Db, contact);
#endif

                    //var a = contactsManager.GetContactInfo(contact.folder_id, 0);
                }

                //Pankaj: ToDo: We need a fuction in Contacts manager to add a contact list
                //contactsManager.AddContactList(contactList);
            }
            catch (Exception e)
            {
            }
            finally
            {
                //ds.Dispose();
            }
        }

        static public void ParseContactChangeCommandAndUpdateDB(List<ServerSyncCommand> commandList, string folderID)
        {

            string contactsNS = "{Contacts}";


            ContactManager contactsManager = new ContactManager();
            List<Contacts> contactList = new List<Contacts>();
            try
            {
                foreach (ServerSyncCommand changeCommand in commandList)
                {
                    XElement appDataXElement = changeCommand.AppDataXml;
                    //Contacts contact = new Contacts();
                    String serverID = changeCommand.ServerId;
                    int folder_id = Convert.ToInt32(serverID.Split(':')[0]);
                    int contactid = Convert.ToInt32(serverID.Split(':')[1]);

                    //We need to change only those values which we have got in response.
                    //So take contact from DB only first
#if USE_ASYNC
                    Contacts contact = contactsManager.GetContactInfoAsync(folder_id, contactid).Result.ElementAt(0);
#else
                    Contacts contact = contactsManager.GetContactInfo(folder_id, contactid).ElementAt(0);
#endif

                    XElement xElement = GetXElement(appDataXElement, contactsNS + "FirstName");
                    if (xElement != null)
                        contact.firstName = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "LastName");
                    if (xElement != null)
                        contact.lastName = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "AssistantPhoneNumber");
                    if (xElement != null)
                        contact.assistantPhoneNumber = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "Business2PhoneNumber");
                    if (xElement != null)
                        contact.business2PhoneNumber = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "BusinessAddressCity");
                    if (xElement != null)
                        contact.businessAddressCity = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "BusinessAddressCountry");
                    if (xElement != null)
                        contact.businessAddressCountry = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "BusinessAddressPostalCode");
                    if (xElement != null)
                        contact.businessAddressPostalCode = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "BusinessAddressState");
                    if (xElement != null)
                        contact.businessAddressState = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "BusinessAddressStreet");
                    if (xElement != null)
                        contact.businessAddressStreet = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "BusinessFaxNumber");
                    if (xElement != null)
                        contact.businessFaxNumber = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "BusinessPhoneNumber");
                    if (xElement != null)
                        contact.businessPhoneNumber = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "CompanyName");
                    if (xElement != null)
                        contact.companyName = xElement.Value;

                    //Pankaj: ToDo: need to know fields to map these?
                    //contact.deleted = Convert.ToInt32(GetString(appDataXElement, contactsNS + "AssistantPhoneNumber"));
                    //contact.dirty = Convert.ToInt32(GetString(appDataXElement, contactsNS + "AssistantPhoneNumber"));
                    //contact.fallbackDisplay = GetString(appDataXElement, contactsNS + "AssistantPhoneNumber");

                    xElement = GetXElement(appDataXElement, contactsNS + "Email1Address");
                    if (xElement != null)
                        contact.email1Address = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "Email2Address");
                    if (xElement != null)
                        contact.email2Address = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "Email3Address");
                    if (xElement != null)
                        contact.email3Address = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "Home2PhoneNumber");
                    if (xElement != null)
                        contact.home2PhoneNumber = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "HomeAddressCity");
                    if (xElement != null)
                        contact.homeAddressCity = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "HomeAddressCountry");
                    if (xElement != null)
                        contact.homeAddressCountry = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "HomeAddressPostalCode");
                    if (xElement != null)
                        contact.homeAddressPostalCode = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "HomeAddressState");
                    if (xElement != null)
                        contact.homeAddressState = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "HomeAddressStreet");
                    if (xElement != null)
                        contact.homeAddressStreet = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "HomeFaxNumber");
                    if (xElement != null)
                        contact.homeFaxNumber = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "HomePhoneNumber");
                    if (xElement != null)
                        contact.homePhoneNumber = xElement.Value;


                    xElement = GetXElement(appDataXElement, contactsNS + "JobTitle");
                    if (xElement != null)
                        contact.jobTitle = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "MobilePhoneNumber");
                    if (xElement != null)
                        contact.mobilePhoneNumber = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "OtherAddressCity");
                    if (xElement != null)
                        contact.otherAddressCity = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "OtherAddressCountry");
                    if (xElement != null)
                        contact.otherAddressCountry = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "OtherAddressPostalCode");
                    if (xElement != null)
                        contact.otherAddressPostalCode = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "OtherAddressState");
                    if (xElement != null)
                        contact.otherAddressState = xElement.Value;

                    xElement = GetXElement(appDataXElement, contactsNS + "OtherAddressStreet");
                    if (xElement != null)
                        contact.otherAddressStreet = xElement.Value;

                    contactList.Add(contact);

                    //Pankaj: To Do: if we have any function for contacts manager to update a contact list, it can be in one-go. 
#if USE_ASYNC
                    contactsManager.UpdateContactInfoAsync(contact);
#else
                        contactsManager.UpdateContactInfo(ds.Db, contact);
#endif

                    //var a = contactsManager.GetContactInfo(contact.folder_id, 0);
                }

            }
            catch (Exception e)
            {
            }
            finally
            {
                //ds.Dispose();
            }
        }

        static public void ParseContactDeleteCommandAndUpdateDB(List<ServerSyncCommand> deleteCommandList, string folderID)
        {

            ContactManager contactsManager = new ContactManager();

            try
            {
                foreach (ServerSyncCommand deleteCommand in deleteCommandList)
                {
                    string serverID = deleteCommand.ServerId;
                    int folder_Id = Convert.ToInt32(serverID.Split(':')[0]);
                    int contactId = Convert.ToInt32(serverID.Split(':')[1]);
#if USE_ASYNC
                    contactsManager.DeleteContactInfoAsync(folder_Id, contactId);
#else
                    contactsManager.DeleteContactInfo(ds.Db, folder_Id, contactId);
#endif

                    //var a = contactsManager.GetContactInfo(folder_Id, 0);
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
                //ds.Dispose();
            }

        }

        #endregion


        #region Calendar Parsing

        static public async Task<Dictionary<string, List<object>>> ParseCalendarContent(List<ServerSyncCommand> addCommandList, List<ServerSyncCommand> deleteCommandList,
                                                  List<ServerSyncCommand> changeCommandList, List<ServerSyncCommand> softDeleteCommandList,
                                                  ActiveSyncPCL.Folder folder)
        {
            Dictionary<string, List<object>> results = new Dictionary<string, List<object>>();

            FolderManager folderManager = new FolderManager();
#if USE_ASYNC
            List<workspace_datastore.models.Folder> folderList = await folderManager.GetFolderInfoAsync(0);
#else
            List<workspace_datastore.models.Folder> folderList = folderManager.GetFolderInfo(0);
#endif
            List<Events> events = null;
            if (addCommandList != null && addCommandList.Count > 0)
            {
                events = await ParseCalendarAddCommandsAndUpdateDB(addCommandList, folder.Id);
                results.Add("Add", events.Cast<object>().ToList());
            }

            if (deleteCommandList != null && deleteCommandList.Count > 0)
            {
                events = await ParseCalendarDeleteCommandAndUpdateDB(deleteCommandList, folder.Id);
                results.Add("Delete", events.Cast<object>().ToList());
            }

            if (changeCommandList != null && changeCommandList.Count > 0)
            {
                events = await ParseCalendarChangeCommandsAndUpdateDB(changeCommandList, folder.Id);
                results.Add("Change", events.Cast<object>().ToList());
            }

            //Pankaj: To Do
            if (softDeleteCommandList != null && softDeleteCommandList.Count > 0)
                ParseCalendarDeleteCommandAndUpdateDB(softDeleteCommandList,folder.Id);

            workspace_datastore.models.Folder folderDB = folderList.FirstOrDefault(x => x.displayName == folder.Name);
            if (folderDB != null)
            {
                folderDB.syncKey = folder.SyncKey;
                folderDB.syncTime = Convert.ToSingle(folder.LastSyncTime.ToOADate());
#if USE_ASYNC
                await folderManager.UpdateFolderInfoAsync(folderDB);
#else
                folderManager.UpdateFolderInfo(folderDB);
#endif
            }

            return results;
        }

        static private void AddFutureOccurencesUntil(XElement root, CMRecurrence reccurenceEvent)
        {

        }

        private static string AcceptableDateTime(string inputfromTable)
        {
            string dateString = "";
            if(!string.IsNullOrEmpty(inputfromTable))
            {
                string year;
                string month;
                string day;
                string hour;
                string min;
                string second;
                year = inputfromTable.Substring(0, 4);
                month = inputfromTable.Substring(4, 2);
                day = inputfromTable.Substring(6, 2);
                hour = inputfromTable.Substring(9, 2);
                min = inputfromTable.Substring(11, 2);
                second = inputfromTable.Substring(13, 2);
                dateString = year + "-" + month + "-" + day + "T" + hour + ":" + min + ":" + second + ".000Z";
            }
            // For testing purposes. throw new System.ArgumentOutOfRangeException();
            return dateString;
        }

        static async private Task<List<Events>> ParseCalendarAddCommandsAndUpdateDB(List<ServerSyncCommand> commandList, string folderID)
        {
            string calendarNS = "{Calendar}";
            string dataNS = "{AirSyncBase}";                     //needed to extract description and othere details in separate namespace
            EventManager eventManager = new EventManager();
            AttendeeManager attendeeManager = new AttendeeManager();
           //create list object of events and attendess to insert into DB
            List<Events> events = new List<Events>();
            List<Attendees> attendees = new List<Attendees>();
            List<Events> eventExceps = new List<Events>();
            //Create datasource object and get connection string

            try
            {
                foreach (ServerSyncCommand addCommand in commandList)
                {
                    XElement appDataXElement = addCommand.AppDataXml;

                    Events calendar = new Events();

                    calendar.serverId = addCommand.ServerId;

                    calendar.id = Convert.ToInt32(calendar.serverId.Split(':')[1]);
                    calendar.folderID = Convert.ToInt32(calendar.serverId.Split(':')[0]);
                    
                    string strSubject = GetString(appDataXElement, calendarNS + "Subject");
                    if (!string.IsNullOrWhiteSpace(strSubject))
                    {
                        calendar.subject = ConvertISO8859EncodingToUtf8(strSubject);
                    }
                    else
                    {
                        calendar.subject = strSubject;
                    }

                    // Set the start Day and Time
                    string timeStr = GetString(appDataXElement, calendarNS + "StartTime");
                    string acceptableFormat = AcceptableDateTime(timeStr);
                    DateTime dt = System.Convert.ToDateTime(acceptableFormat).ToUniversalTime();
                    double eventOriginalStartTime = dt.ToOADate();

                    // Set the end Day and Time
                    timeStr = GetString(appDataXElement, calendarNS + "EndTime");
                    acceptableFormat = AcceptableDateTime(timeStr);
                    dt = System.Convert.ToDateTime(acceptableFormat).ToUniversalTime();
                    double eventOriginalEndTime = dt.ToOADate();
                    
                    string strLocation = GetString(appDataXElement, calendarNS + "Location");
                    if (!string.IsNullOrWhiteSpace(strLocation))
                    {
                        calendar.location = ConvertISO8859EncodingToUtf8(strLocation);
                    }
                    else
                    {
                        calendar.location = strLocation;
                    }

                    calendar.organizerName = GetString(appDataXElement, calendarNS + "OrganizerName");
                    calendar.organizerEmail = GetString(appDataXElement, calendarNS + "OrganizerEmail");
                    calendar.timeZone = GetString(appDataXElement, calendarNS + "TimeZone");                
                    calendar.uid = GetString(appDataXElement, calendarNS + "UID");
                    calendar.timeCreated = GetString(appDataXElement, calendarNS + "DTStamp");

                    XElement allDayElement = GetXElement(appDataXElement, calendarNS + "AllDayEvent");
                    if (null != allDayElement)
                    {
                        calendar.allDay = Convert.ToInt32(allDayElement.Value);
                    }

                    XElement reminderElement = GetXElement(appDataXElement, calendarNS + "Reminder");
                    if (null != reminderElement && string.Empty != reminderElement.Value)
                    {
                        calendar.originalReminder = Convert.ToInt32(reminderElement.Value);
                    }

                    XElement selfAttendeeStatusElement = GetXElement(appDataXElement, calendarNS + "ResponseType");
                    if (null != selfAttendeeStatusElement && string.Empty != selfAttendeeStatusElement.Value)
                    {
                        calendar.selfAttendeeStatus = Convert.ToInt32(selfAttendeeStatusElement.Value);
                    }


                    XElement responseRequestedElement = GetXElement(appDataXElement, calendarNS + "ResponseRequested");
                    if (null != responseRequestedElement && string.Empty != responseRequestedElement.Value)
                    {
                        calendar.responseRequested = Convert.ToInt32(responseRequestedElement.Value);
                    }


                    XElement meetingStatusElement = GetXElement(appDataXElement, calendarNS + "MeetingStatus");
                    if (null != responseRequestedElement && string.Empty != meetingStatusElement.Value)
                    {
                        calendar.meetingStatus = Convert.ToInt32(meetingStatusElement.Value);
                    }


                    XElement bodyElement = GetXElement(appDataXElement, dataNS + "Body");
                    if (null != bodyElement)
                    {
                        calendar.description = GetString(bodyElement, dataNS + "Data");
                    }

                    //Parsing recurrence element
                    XElement recurrenceElement = GetXElement(appDataXElement, calendarNS + "Recurrence");
                    if (null != recurrenceElement)
                    {
                        // We DO NOT get future recurring events back from EAS. So we have to populate the database
                        // on our own.
                        calendar.reoccurs = 1;
                        CMRecurrence bilbo = new CMRecurrence();

                        XElement rcTypeElement = GetXElement(recurrenceElement, calendarNS + "Type");
                        if (null != rcTypeElement && string.Empty != rcTypeElement.Value)
                        {
                            calendar.rcType = Convert.ToInt32(rcTypeElement.Value);
                            bilbo.RecurrenceType = (CMRecurrenceType)calendar.rcType;
                        }


                        XElement rcOccurencesElement = GetXElement(recurrenceElement, calendarNS + "Occurrences");
                        if (null != rcOccurencesElement && string.Empty != rcOccurencesElement.Value)
                        {
                            calendar.rcOccurences = Convert.ToInt32(rcOccurencesElement.Value);
                            bilbo.Occurences = calendar.rcOccurences;
                        }


                        XElement rcIntervalElement = GetXElement(recurrenceElement, calendarNS + "Interval");
                        if (null != rcIntervalElement && string.Empty != rcIntervalElement.Value)
                        {
                            calendar.rcInterval = Convert.ToInt32(rcIntervalElement.Value);
                            bilbo.Interval = calendar.rcInterval;
                        }


                        XElement rcWeekOfMonthElement = GetXElement(recurrenceElement, calendarNS + "WeekOfMonth");
                        if (null != rcWeekOfMonthElement && string.Empty != rcWeekOfMonthElement.Value)
                        {
                            calendar.rcWeekOfMonth = Convert.ToInt32(rcWeekOfMonthElement.Value);
                            bilbo.WeekOfMonth = calendar.rcWeekOfMonth;
                        }

                        XElement rcDayOfWeekElement = GetXElement(recurrenceElement, calendarNS + "DayOfWeek");
                        if (null != rcDayOfWeekElement && string.Empty != rcDayOfWeekElement.Value)
                        {
                            calendar.rcDayOfWeek = Convert.ToInt32(rcDayOfWeekElement.Value);
                            bilbo.DayOfWeek = calendar.rcDayOfWeek;
                        }

                        XElement rcMonthOfYearElement = GetXElement(recurrenceElement, calendarNS + "MonthOfYear");
                        if (null != rcMonthOfYearElement && string.Empty != rcMonthOfYearElement.Value)
                        {
                            calendar.rcMonthOfYear = Convert.ToInt32(rcMonthOfYearElement.Value);
                            bilbo.MonthOfYear = calendar.rcMonthOfYear;
                        }

                        XElement rcUntilElement = GetXElement(recurrenceElement, calendarNS + "Until");
                        if (null != rcUntilElement && string.Empty != rcUntilElement.Value)
                        {
                            // Comes in as:
                            //   20141101T173000Z
                            // Needs to be:
                            //   2014-06-12T10:11:29.960Z
                            acceptableFormat = AcceptableDateTime(rcUntilElement.Value);
                            dt = System.Convert.ToDateTime(acceptableFormat);
                            calendar.rcUntil = dt.ToOADate();
                            bilbo.Until = dt;
                        }


                        XElement rcDayOfMonthElement = GetXElement(recurrenceElement, calendarNS + "DayOfMonth");
                        if (null != rcDayOfMonthElement && string.Empty != rcDayOfMonthElement.Value)
                        {
                            calendar.rcDayOfMonth = Convert.ToInt32(rcDayOfMonthElement.Value);
                            bilbo.DayOfMonth = calendar.rcDayOfMonth;
                        }

                     //   AddFutureOccurencesUntil(appDataXElement, bilbo);

                    }
                    // Parsing exception to check delete recurring event instance(Dhruv 29 Jan 2015)
                    XElement exceptionsElement = GetXElement(appDataXElement, calendarNS + "Exceptions");
                    if (null != exceptionsElement)
                    {                       
                       string deletedValue = string.Empty;                      
                        IEnumerable<XElement> exceptionList = from query in exceptionsElement.Descendants().Where(p => p.Name == (calendarNS + "Exception")).ToList() select query;
                        foreach (XElement exceptionElement in exceptionList)
                        {
                            Events eventExcep = new Events();
                           // eventExcep = evnt;
                            deletedValue = GetString(exceptionElement, calendarNS + "Deleted");
                            if (!String.IsNullOrWhiteSpace(deletedValue) && Convert.ToBoolean(Convert.ToInt32(deletedValue)))
                            {
                                string dateToHide = GetString(exceptionElement, calendarNS + "ExceptionStartTime");
                                acceptableFormat = AcceptableDateTime(dateToHide);
                                dt = System.Convert.ToDateTime(acceptableFormat).ToUniversalTime();
                                calendar.rcHiddenExceptions = calendar.rcHiddenExceptions + Convert.ToSingle(dt.ToOADate()) + ",";
                            }
                            else
                            {
                                eventExcep.allDay = calendar.allDay;
                                eventExcep.availability = calendar.availability;
                                eventExcep.description = calendar.description;
                                eventExcep.deleted = calendar.deleted;
                                eventExcep.dirty = calendar.dirty;
                                eventExcep.dtfinal = calendar.dtfinal;
                                eventExcep.folderID = calendar.folderID;
                                eventExcep.id = calendar.id;
                                eventExcep.meetingStatus = calendar.meetingStatus;
                                eventExcep.organizerEmail = calendar.organizerEmail;
                                eventExcep.organizerName = calendar.organizerName;
                                eventExcep.reoccurs = calendar.reoccurs;
                                eventExcep.responseRequested = calendar.responseRequested;
                                eventExcep.selfAttendeeStatus = calendar.selfAttendeeStatus;
                                eventExcep.serverId = calendar.serverId;
                                eventExcep.subject = calendar.subject;
                                eventExcep.timeZone = calendar.timeZone;
                                eventExcep.uid = calendar.uid;
                                string exceptionReminder = GetString(exceptionElement, calendarNS + "Reminder");
                                if (!String.IsNullOrWhiteSpace(exceptionReminder))
                                {
                                    eventExcep.originalReminder = Convert.ToInt32(exceptionReminder);
                                }
                                else
                                {
                                    eventExcep.originalReminder = calendar.originalReminder;
                                }
                                eventExcep.rcCalendarType = calendar.rcCalendarType;
                                eventExcep.rcDayOfMonth = calendar.rcDayOfMonth;
                                eventExcep.rcDayOfWeek = calendar.rcDayOfWeek;
                                eventExcep.rcFirstDayOfWeek = calendar.rcFirstDayOfWeek;
                                eventExcep.rcInterval = calendar.rcInterval;
                                eventExcep.rcIsLeapMonth = calendar.rcIsLeapMonth;
                                eventExcep.rcMonthOfYear = calendar.rcMonthOfYear;
                                eventExcep.rcOccurences = calendar.rcOccurences;
                                eventExcep.rcType = calendar.rcType;
                                eventExcep.rcUntil = calendar.rcUntil;
                                eventExcep.rcWeekOfMonth = calendar.rcWeekOfMonth;
                                eventExcep.Original_id = calendar.id;
                                eventExcep.timeCreated = GetString(exceptionElement, calendarNS + "DTStamp");
                                // Set the start Day and Time
                                timeStr = GetString(exceptionElement, calendarNS + "StartTime");
                                if(!string.IsNullOrEmpty(timeStr))
                                {
                                    acceptableFormat = AcceptableDateTime(timeStr);
                                    dt = System.Convert.ToDateTime(acceptableFormat).ToUniversalTime();
                                    eventExcep.startDayAndTime = dt.ToOADate();
                                }
                                else
                                {
                                    // TODO What do we do if the 'StartTime' element is missing?
                                }
                                // Set the end Day and Time
                                timeStr = GetString(exceptionElement, calendarNS + "EndTime");
                                if (!string.IsNullOrEmpty(timeStr))
                                {
                                    acceptableFormat = AcceptableDateTime(timeStr);
                                    dt = System.Convert.ToDateTime(acceptableFormat).ToUniversalTime();
                                    eventExcep.endDayAndTime = dt.ToOADate();
                                }
                                else
                                {
                                    // TODO What do we do if the 'EndTime' element is missing?
                                }
                                string dateToHide = GetString(exceptionElement, calendarNS + "ExceptionStartTime");
                                if (!string.IsNullOrEmpty(dateToHide))
                                {
                                    acceptableFormat = AcceptableDateTime(dateToHide);
                                    dt = System.Convert.ToDateTime(acceptableFormat).ToUniversalTime();
                                    calendar.rcHiddenExceptions = calendar.rcHiddenExceptions + Convert.ToSingle(dt.ToOADate()) + ",";
                                }
                                else
                                {
                                    // TODO What do we do if the 'ExceptionStartTime' element is missing?
                                }
                                
                                strLocation = GetString(exceptionElement, calendarNS + "Location");
                                if (!string.IsNullOrWhiteSpace(strLocation))
                                {
                                    eventExcep.location = ConvertISO8859EncodingToUtf8(strLocation);
                                }
                                else
                                {
                                    eventExcep.location = strLocation;
                                }

                                System.Diagnostics.Debug.WriteLine("EXCEPTS.ADD  M_SUBJ: {0}   RECURR: {1}   START: {2}  END: {3}", eventExcep.subject, eventExcep.reoccurs,
                                    DateTimeExtensions.FromOADate(eventExcep.startDayAndTime).ToShortDateString() + ":" + DateTimeExtensions.FromOADate(eventExcep.startDayAndTime).ToShortTimeString(),
                                    DateTimeExtensions.FromOADate(eventExcep.endDayAndTime).ToShortDateString() + ":" + DateTimeExtensions.FromOADate(eventExcep.endDayAndTime).ToShortTimeString());
                                eventExceps.Add(eventExcep);
                            }
                        }
                        //if (String.IsNullOrWhiteSpace(deletedValue) || !Convert.ToBoolean(Convert.ToInt32(deletedValue)))
                        //{
                        //    //Here we will add another event for exception(update recurring event instance)
                        //     eventManager.AddAllExceptionInfoAsync(ds.Db,eventExceps);
                        //}
                    }
                    //end
                    //set event original start and end date time
                    calendar.startDayAndTime = eventOriginalStartTime;
                    calendar.endDayAndTime = eventOriginalEndTime;
                    calendar.Original_id = 0;
                    //Parsing Attendees element

                    XElement attendeesElement = GetXElement(appDataXElement, calendarNS + "Attendees");
                    if (null != attendeesElement)
                    {                       
                        // Iterate through attachments
                        IEnumerable<XElement> attendeeList = from query in attendeesElement.Descendants().Where(p => p.Name == (calendarNS + "Attendee")).ToList() select query;
                        foreach (XElement attendeeElement in attendeeList)
                        {
                            Attendees attendee = new Attendees();

                            attendee.event_id = calendar.serverId;
                            attendee.attendeeName = GetString(attendeeElement, calendarNS + "Name");
                            attendee.attendeeEmail = GetString(attendeeElement, calendarNS + "Email");

                            XElement attendeeTypeElement = GetXElement(appDataXElement, calendarNS + "AttendeeType");
                            if (null != attendeeTypeElement && string.Empty != attendeeTypeElement.Value)
                            {
                                attendee.attendeeType = Convert.ToInt32(attendeeTypeElement.Value);
                            }

                            XElement attendeeStatusElement = GetXElement(appDataXElement, calendarNS + "AttendeeStatus");
                            if (null != attendeeStatusElement && string.Empty != attendeeStatusElement.Value)
                            {
                                attendee.attendeeStatus = Convert.ToInt32(attendeeStatusElement.Value);
                            }
//#if USE_ASYNC
//                            attendeeManager.AddAttendeeInfoAsync(ds.Db, attendee);
//#else
                            attendees.Add(attendee);
//#endif
                        }
//#if USE_ASYNC
//                        attendeeManager.AddAllAttendees( attendees);
//#endif
                    }

                    //Adding ca
//#if USE_ASYNC
//                    eventManager.AddEventInfoAsync(ds.Db, calendar);
//#else
                    System.Diagnostics.Debug.WriteLine("EVENTS.ADD  M_SUBJ: {0}   RECURR: {1}   START: {2}  END: {3}", calendar.subject, calendar.reoccurs,
                        DateTimeExtensions.FromOADate(calendar.startDayAndTime).ToShortDateString() + ":" + DateTimeExtensions.FromOADate(calendar.startDayAndTime).ToShortTimeString(),
                        DateTimeExtensions.FromOADate(calendar.endDayAndTime).ToShortDateString() + ":" + DateTimeExtensions.FromOADate(calendar.endDayAndTime).ToShortTimeString());
                    events.Add(calendar);
//#endif

                }
                //var a = eventManager.GetEventInfo(1, 0);
#if USE_ASYNC
                eventManager.AddAllEventsInfoAsync(events,attendees,eventExceps);
#endif
            }
            catch (Exception e)
            {
            }
            finally
            {
                //ds.Dispose();
            }
            return events;
        }

        static async private Task<List<Events>> ParseCalendarChangeCommandsAndUpdateDB(List<ServerSyncCommand> commandList, string folderID)
        {
            string dataNS = "{AirSyncBase}";
            string calendarNS = "{Calendar}";
            EventManager eventManager = new EventManager();
            AttendeeManager attendeeManager = new AttendeeManager();
            List<Events> events = new List<Events>();
            try
            {
                foreach (ServerSyncCommand changeCommand in commandList)
                {
                    XElement appDataXElement = changeCommand.AppDataXml;

                    //Events calendar = new Events();
                    String serverID = changeCommand.ServerId;
                    int folder_id = Convert.ToInt32(serverID.Split(':')[0]);
                    int eventID = Convert.ToInt32(serverID.Split(':')[1]);

                    //We need to change only those values which we have got in response.
                    //So take contact from DB only first
#if USE_ASYNC
                    Events calendar = eventManager.GetEventInfoAsync(folder_id, eventID).Result.ElementAt(0);
#else
                    Events calendar = eventManager.GetEventInfo(folder_id, contactid).ElementAt(0);
#endif
                    calendar.id = Convert.ToInt32(calendar.serverId.Split(':')[1]);
                    calendar.folderID = Convert.ToInt32(calendar.serverId.Split(':')[0]);

                    
                    XElement subjectElement = GetXElement(appDataXElement, calendarNS + "Subject");
                    if (subjectElement != null)
                    {
                        if (!string.IsNullOrWhiteSpace(subjectElement.Value))
                        {
                            calendar.subject = ConvertISO8859EncodingToUtf8(subjectElement.Value);
                        }
                        else
                        {
                            calendar.subject = subjectElement.Value;
                        }
                    }


                    // Set the start Day and Time
                    XElement startTimeElement = GetXElement(appDataXElement, calendarNS + "StartTime");
                    if (startTimeElement != null)
                    {
                        string timeStr = startTimeElement.Value;
                        string acceptableFormat = AcceptableDateTime(timeStr);
                        DateTime dt = System.Convert.ToDateTime(acceptableFormat).ToUniversalTime();
                        double eventOriginalStartTime = dt.ToOADate();
                        calendar.startDayAndTime = eventOriginalStartTime;
                    }

                    XElement endTimeElement = GetXElement(appDataXElement, calendarNS + "EndTime");
                    if (endTimeElement != null)
                    {
                        // Set the end Day and Time
                        string emdTimeStr = endTimeElement.Value;
                        string acceptableFormat = AcceptableDateTime(emdTimeStr);
                        DateTime dt = System.Convert.ToDateTime(acceptableFormat).ToUniversalTime();
                        double eventOriginalEndTime = dt.ToOADate();
                        calendar.endDayAndTime = eventOriginalEndTime;
                    }

                    XElement location = GetXElement(appDataXElement, calendarNS + "Location");
                    if (null != location)
                    {
                        if (!string.IsNullOrWhiteSpace(location.Value))
                        {
                            calendar.location = ConvertISO8859EncodingToUtf8(location.Value);
                        }
                        else
                        {
                            calendar.location = location.Value;
                        }
                    }

                    XElement organizerName = GetXElement(appDataXElement, calendarNS + "OrganizerName");
                    if (null != organizerName)
                    {
                        calendar.organizerName = organizerName.Value;
                    }

                    XElement organizerEmail = GetXElement(appDataXElement, calendarNS + "OrganizerEmail");
                    if (null != organizerEmail)
                    {
                        calendar.organizerEmail = organizerEmail.Value;
                    }

                    XElement timeZone = GetXElement(appDataXElement, calendarNS + "TimeZone");
                    if (null != timeZone)
                    {
                        calendar.timeZone = timeZone.Value;
                    }

                    XElement uid = GetXElement(appDataXElement, calendarNS + "UID");
                    if (null != uid)
                    {
                        calendar.uid = uid.Value;
                    }

                    XElement timeCreated = GetXElement(appDataXElement, calendarNS + "DTStamp");
                    if (null != timeCreated)
                    {
                        calendar.timeCreated = timeCreated.Value;
                    }

                    XElement allDayElement = GetXElement(appDataXElement, calendarNS + "AllDayEvent");
                    if (null != allDayElement)
                    {
                        calendar.allDay = Convert.ToInt32(allDayElement.Value);
                    }

                    XElement reminderElement = GetXElement(appDataXElement, calendarNS + "Reminder");
                    if (null != reminderElement && string.Empty != reminderElement.Value)
                    {
                        calendar.originalReminder = Convert.ToInt32(reminderElement.Value);
                    }

                    XElement selfAttendeeStatusElement = GetXElement(appDataXElement, calendarNS + "ResponseType");
                    if (null != selfAttendeeStatusElement && string.Empty != selfAttendeeStatusElement.Value)
                    {
                        calendar.selfAttendeeStatus = Convert.ToInt32(selfAttendeeStatusElement.Value);
                    }


                    XElement responseRequestedElement = GetXElement(appDataXElement, calendarNS + "ResponseRequested");
                    if (null != responseRequestedElement && string.Empty != responseRequestedElement.Value)
                    {
                        calendar.responseRequested = Convert.ToInt32(responseRequestedElement.Value);
                    }


                    XElement meetingStatusElement = GetXElement(appDataXElement, calendarNS + "MeetingStatus");
                    if (null != responseRequestedElement && string.Empty != meetingStatusElement.Value)
                    {
                        calendar.meetingStatus = Convert.ToInt32(meetingStatusElement.Value);
                    }


                    XElement bodyElement = GetXElement(appDataXElement, dataNS + "Body");
                    if (null != bodyElement)
                    {
                        calendar.description = GetString(bodyElement, dataNS + "Data");
                    }
                   
                    //Parsing recurrence element
                    XElement recurrenceElement = GetXElement(appDataXElement, calendarNS + "Recurrence");
                    if (null != recurrenceElement)
                    {
                        calendar.reoccurs = 1;


                        XElement rcTypeElement = GetXElement(recurrenceElement, calendarNS + "Type");
                        if (null != rcTypeElement && string.Empty != rcTypeElement.Value)
                        {
                            calendar.rcType = Convert.ToInt32(rcTypeElement.Value);
                        }


                        XElement rcOccurencesElement = GetXElement(recurrenceElement, calendarNS + "Occurrences");
                        if (null != rcOccurencesElement && string.Empty != rcOccurencesElement.Value)
                        {
                            calendar.rcOccurences = Convert.ToInt32(rcOccurencesElement.Value);
                        }


                        XElement rcIntervalElement = GetXElement(recurrenceElement, calendarNS + "Interval");
                        if (null != rcIntervalElement && string.Empty != rcIntervalElement.Value)
                        {
                            calendar.rcInterval = Convert.ToInt32(rcIntervalElement.Value);
                        }


                        XElement rcWeekOfMonthElement = GetXElement(recurrenceElement, calendarNS + "WeekOfMonth");
                        if (null != rcWeekOfMonthElement && string.Empty != rcWeekOfMonthElement.Value)
                        {
                            calendar.rcWeekOfMonth = Convert.ToInt32(rcWeekOfMonthElement.Value);
                        }

                        XElement rcDayOfWeekElement = GetXElement(recurrenceElement, calendarNS + "DayOfWeek");
                        if (null != rcDayOfWeekElement && string.Empty != rcDayOfWeekElement.Value)
                        {
                            calendar.rcDayOfWeek = Convert.ToInt32(rcDayOfWeekElement.Value);
                        }

                        XElement rcMonthOfYearElement = GetXElement(recurrenceElement, calendarNS + "MonthOfYear");
                        if (null != rcMonthOfYearElement && string.Empty != rcMonthOfYearElement.Value)
                        {
                            calendar.rcMonthOfYear = Convert.ToInt32(rcMonthOfYearElement.Value);
                        }

                        XElement rcUntilElement = GetXElement(recurrenceElement, calendarNS + "Until");
                        if (null != rcUntilElement && string.Empty != rcUntilElement.Value)
                        {
                            DateTime dt = System.Convert.ToDateTime(rcUntilElement.Value);
                            calendar.rcUntil = dt.ToOADate();
                        }


                        XElement rcDayOfMonthElement = GetXElement(recurrenceElement, calendarNS + "DayOfMonth");
                        if (null != rcDayOfMonthElement && string.Empty != rcDayOfMonthElement.Value)
                        {
                            calendar.rcDayOfMonth = Convert.ToInt32(rcDayOfMonthElement.Value);
                        }

                    }
                    // Parsing exception to check delete recurring event instance(Dhruv 29 Jan 2015)
                    XElement exceptionsElement = GetXElement(appDataXElement, calendarNS + "Exceptions");
                    if (null != exceptionsElement)
                    {
                        
                        IEnumerable<XElement> exceptionList = from query in exceptionsElement.Descendants().Where(p => p.Name == (calendarNS + "Exception")).ToList() select query;
                        foreach (XElement exceptionElement in exceptionList)
                        {
                            Events eventExcep = new Events();
                            string deletedValue = GetString(exceptionElement, calendarNS + "Deleted");
                            if (!String.IsNullOrWhiteSpace(deletedValue) && Convert.ToBoolean(Convert.ToInt32(deletedValue)))
                            {
                                string dateToHide = GetString(exceptionElement, calendarNS + "ExceptionStartTime");
                                string acceptableFormat = AcceptableDateTime(dateToHide);
                                DateTime dt = System.Convert.ToDateTime(acceptableFormat).ToUniversalTime();
                                calendar.rcHiddenExceptions = calendar.rcHiddenExceptions + Convert.ToSingle(dt.ToOADate()) + ",";
                            }
                            else
                            {
                                eventExcep.allDay = calendar.allDay;
                                eventExcep.availability = calendar.availability;
                                eventExcep.description = calendar.description;
                                eventExcep.deleted = calendar.deleted;
                                eventExcep.dirty = calendar.dirty;
                                eventExcep.dtfinal = calendar.dtfinal;
                                eventExcep.folderID = calendar.folderID;
                                eventExcep.id = calendar.id;
                                eventExcep.meetingStatus = calendar.meetingStatus;
                                eventExcep.organizerEmail = calendar.organizerEmail;
                                eventExcep.organizerName = calendar.organizerName;
                                eventExcep.reoccurs = calendar.reoccurs;
                                eventExcep.responseRequested = calendar.responseRequested;
                                eventExcep.selfAttendeeStatus = calendar.selfAttendeeStatus;
                                eventExcep.serverId = calendar.serverId;
                                eventExcep.subject = calendar.subject;
                                eventExcep.timeZone = calendar.timeZone;
                                eventExcep.uid = calendar.uid;
                                string exceptionReminder = GetString(exceptionElement, calendarNS + "Reminder");
                                if (!String.IsNullOrWhiteSpace(exceptionReminder))
                                {
                                    eventExcep.originalReminder = Convert.ToInt32(exceptionReminder);
                                }
                                else
                                {
                                    eventExcep.originalReminder = calendar.originalReminder;
                                }
                                eventExcep.rcCalendarType = calendar.rcCalendarType;
                                eventExcep.rcDayOfMonth = calendar.rcDayOfMonth;
                                eventExcep.rcDayOfWeek = calendar.rcDayOfWeek;
                                eventExcep.rcFirstDayOfWeek = calendar.rcFirstDayOfWeek;
                                eventExcep.rcInterval = calendar.rcInterval;
                                eventExcep.rcIsLeapMonth = calendar.rcIsLeapMonth;
                                eventExcep.rcMonthOfYear = calendar.rcMonthOfYear;
                                eventExcep.rcOccurences = calendar.rcOccurences;
                                eventExcep.rcType = calendar.rcType;
                                eventExcep.rcUntil = calendar.rcUntil;
                                eventExcep.rcWeekOfMonth = calendar.rcWeekOfMonth;
                                eventExcep.Original_id = calendar.id;
                                eventExcep.timeCreated = GetString(exceptionElement, calendarNS + "DTStamp");
                                // Set the start Day and Time
                                string starttimeString = GetString(exceptionElement, calendarNS + "StartTime");
                                if (!string.IsNullOrWhiteSpace(starttimeString))
                                {
                                    string acceptableFormat = AcceptableDateTime(starttimeString);
                                    DateTime dt = System.Convert.ToDateTime(acceptableFormat).ToUniversalTime();
                                    eventExcep.startDayAndTime = dt.ToOADate();
                                }

                                // Set the end Day and Time
                                string endtimeString = GetString(exceptionElement, calendarNS + "EndTime");
                                if (!string.IsNullOrWhiteSpace(endtimeString))
                                {
                                    string acceptableFormat = AcceptableDateTime(endtimeString);
                                    DateTime dt = System.Convert.ToDateTime(acceptableFormat).ToUniversalTime();
                                    eventExcep.endDayAndTime = dt.ToOADate();
                                }

                                string dateToHide = GetString(exceptionElement, calendarNS + "ExceptionStartTime");
                                if (!string.IsNullOrWhiteSpace(dateToHide))
                                {
                                    string acceptableFormat = AcceptableDateTime(dateToHide);
                                    DateTime dt = System.Convert.ToDateTime(acceptableFormat).ToUniversalTime();
                                    calendar.rcHiddenExceptions = calendar.rcHiddenExceptions + Convert.ToSingle(dt.ToOADate()) + ",";
                                }


                                string strLocation = GetString(exceptionElement, calendarNS + "Location");
                                if (!string.IsNullOrWhiteSpace(strLocation))
                                {
                                    eventExcep.location = ConvertISO8859EncodingToUtf8(strLocation);
                                }
                                else
                                {
                                    eventExcep.location = strLocation;
                                }

                                //Here we will add another event for exception(update recurring event instance)
                                eventManager.AddExceptionInfoAsync(eventExcep);
                            }
                        }
                        
                    }
                    //set event original start and end date time
                    //calendar.startDayAndTime = eventOriginalStartTime;
                    //calendar.endDayAndTime=eventOriginalEndTime;
                    calendar.Original_id = 0;
                    //end
                    //Parsing Attendees element

                    XElement attendeesElement = GetXElement(appDataXElement, calendarNS + "Attendees");
                    if (null != attendeesElement)
                    {
                        //need to delete first previous attendees 

                        // Iterate through attachments
#if USE_ASYNC
                        await attendeeManager.DeleteAttendeeListInfoAsync(calendar.serverId);
#else
                        attendeeManager.DeleteAttendeeListInfo(ds.Db, calendar.serverId);
#endif
                        IEnumerable<XElement> attendeeList = from query in attendeesElement.Descendants().Where(p => p.Name == (calendarNS + "Attendee")).ToList() select query;
                        foreach (XElement attendeeElement in attendeeList)
                        {
                            Attendees attendee = new Attendees();

                            attendee.event_id = calendar.serverId;
                            attendee.attendeeName = GetString(attendeeElement, calendarNS + "Name");
                            attendee.attendeeEmail = GetString(attendeeElement, calendarNS + "Email");

                            XElement attendeeTypeElement = GetXElement(attendeeElement, calendarNS + "AttendeeType");
                            if (null != attendeeTypeElement && string.Empty != attendeeTypeElement.Value)
                            {
                                attendee.attendeeType = Convert.ToInt32(attendeeTypeElement.Value);
                            }

                            XElement attendeeStatusElement = GetXElement(attendeeElement, calendarNS + "AttendeeStatus");
                            if (null != attendeeStatusElement && string.Empty != attendeeStatusElement.Value)
                            {
                                attendee.attendeeStatus = Convert.ToInt32(attendeeStatusElement.Value);
                            }
#if USE_ASYNC
                            attendeeManager.AddAttendeeInfoAsync(attendee);
#else
                            attendeeManager.AddAttendeeInfo(ds.Db, attendee);
#endif
                        }
                    }

                    //updating event
                    events.Add(calendar);
#if USE_ASYNC
                    eventManager.UpdateEventInfoAsync(calendar);
#else
                    eventManager.UpdateEventInfo(ds.Db, calendar);
#endif

                }
#if USE_ASYNC
                var a = eventManager.GetEventInfoAsync(1, 0);
#else
                var a = eventManager.GetEventInfo(1, 0);
#endif
            }
            catch (Exception e)
            {
            }
            finally
            {
               // ds.Dispose();
            }
            return events;
        }

        static async private Task<List<Events>> ParseCalendarDeleteCommandAndUpdateDB(List<ServerSyncCommand> deleteCommandList, string folderID)
        {

            EventManager eventsManager = new EventManager();
            AttendeeManager attendeeManager = new AttendeeManager();
            List<Events> returnEvents = new List<Events>();

            try
            {
                foreach (ServerSyncCommand deleteCommand in deleteCommandList)
                {
                    string serverID = deleteCommand.ServerId;
                    int folderId = Convert.ToInt32(serverID.Split(':')[0]);
                    int eventId = Convert.ToInt32(serverID.Split(':')[1]);
#if USE_ASYNC
                    Events calendar = eventsManager.GetEventInfoAsync(folderId, eventId).Result.ElementAt(0);
#else
                    Events calendar = eventsManager.GetEventInfo(folderId, eventId).ElementAt(0);
#endif  
                    returnEvents.Add(calendar);

                    await attendeeManager.DeleteAttendeeListInfoAsync(serverID);
                    await eventsManager.DeleteEventInfoAsync(folderId, eventId);
                }
            }
            catch (Exception e)
            {
            }
            finally
            {
               // ds.Dispose();
            }
            return returnEvents;
        }
        #endregion

        #region GAL Parsing

        static public List<workspace_datastore.models.Contacts> ParseAndGetGALSearchResultList(XDocument responseXml)
        {
            List<workspace_datastore.models.Contacts> gal = new List<workspace_datastore.models.Contacts>();

            string galNS = "{GAL}";
            string searchNS = "{Search}";
            var addElement = responseXml.Element(searchNS + "Search").
                                                    Element(searchNS + "Response").
                                                    Element(searchNS + "Store").
                                                    Elements(searchNS + "Result").ToList();

            foreach (XElement element in addElement)
            {
                workspace_datastore.models.Contacts galObj = new workspace_datastore.models.Contacts();
                try
                {
                    if (element.Element(searchNS + "Properties") == null)
                        return gal;
                    XElement displayName = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "DisplayName");
                    XElement phone = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "Phone");
                    XElement office = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "Office");
                    XElement title = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "Title");
                    XElement company = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "Company");
                    XElement alias = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "Alias");
                    XElement firstName = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "FirstName");
                    XElement lastName = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "LastName");
                    XElement homePhone = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "HomePhone");
                    XElement mobilePhone = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "MobilePhone");
                    XElement email1Address = element.Element(searchNS + "Properties").Descendants().SingleOrDefault(p => p.Name == galNS + "EmailAddress");





                    if (null != firstName)
                        galObj.firstName = firstName.Value;
                    if (null != lastName)
                        galObj.lastName = lastName.Value;

                    if (null != email1Address)
                        galObj.email1Address = email1Address.Value;

                    if (null != title)
                        galObj.jobTitle = title.Value;

                    if (null != company)
                        galObj.companyName = company.Value;

                    if (null != phone)
                        galObj.businessPhoneNumber = phone.Value;

                    if (null != homePhone)
                        galObj.homePhoneNumber = homePhone.Value;

                    if (null != mobilePhone)
                        galObj.mobilePhoneNumber = mobilePhone.Value;

                }
                catch
                {

                }
                gal.Add(galObj);
            }

            return gal;

        }
        #endregion

        /// <summary>
        /// Convert ISO-8859-1 encoding to UTF-8 string 
        /// </summary>
        /// <returns>UTF-8 string</returns>
        public static string ConvertISO8859EncodingToUtf8(string content)
        {
            var enc = Encoding.GetEncoding("ISO-8859-1");
            var bytes = enc.GetBytes(content);
            var result = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
            return result;
        }

        static async Task ShowMessageBox(string message)
        {
            CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            await dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                MessageDialog msgDialog = new MessageDialog(message);
                msgDialog.Commands.Add(new UICommand("Ok"));
                await msgDialog.ShowAsync();
            });
        }
    }



    /// <summary>
    /// Store Object in isolated storage
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class IsolatedStorageCacheManager<T>
    {
        public static async Task Store(string filename, T obj)
        {
            Windows.Storage.StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;

            Windows.Globalization.DateTimeFormatting.DateTimeFormatter formatter =
                new Windows.Globalization.DateTimeFormatting.DateTimeFormatter("longtime");

            StorageFile file = await localFolder.CreateFileAsync(filename,
                CreationCollisionOption.ReplaceExisting);
            await FileIO.WriteTextAsync(file, formatter.Format(DateTime.Now));

            //IsolatedStorageFile appStore = IsolatedStorageFile.GetUserStoreForApplication();
            //using (IsolatedStorageFileStream fileStream = appStore.OpenFile(filename, FileMode.Create))
            //{
            //    DataContractSerializer serializer = new DataContractSerializer(typeof(T));
            //    serializer.WriteObject(fileStream, obj);
            //}
        }
        public static T Retrieve(string filename)
        {
            T obj = default(T);
            //IsolatedStorageFile appStore = IsolatedStorageFile.GetUserStoreForApplication();
            //if (appStore.FileExists(filename))
            //{
            //    using (IsolatedStorageFileStream fileStream = appStore.OpenFile(filename, FileMode.Open))
            //    {
            //        DataContractSerializer serializer = new DataContractSerializer(typeof(T));
            //        obj = (T)serializer.ReadObject(fileStream);
            //    }
            //}
            return obj;
        }
    }

    public static class DateTimeExtensions
    {
        // Number of 100ns ticks per time unit
        private const long TicksPerMillisecond = 10000;
        private const long TicksPerSecond = TicksPerMillisecond * 1000;
        private const long TicksPerMinute = TicksPerSecond * 60;
        private const long TicksPerHour = TicksPerMinute * 60;
        private const long TicksPerDay = TicksPerHour * 24;

        // Number of milliseconds per time unit
        private const int MillisPerSecond = 1000;
        private const int MillisPerMinute = MillisPerSecond * 60;
        private const int MillisPerHour = MillisPerMinute * 60;
        private const int MillisPerDay = MillisPerHour * 24;

        // Number of days in a non-leap year
        private const int DaysPerYear = 365;
        // Number of days in 4 years
        private const int DaysPer4Years = DaysPerYear * 4 + 1;       // 1461
        // Number of days in 100 years
        private const int DaysPer100Years = DaysPer4Years * 25 - 1;  // 36524
        // Number of days in 400 years
        private const int DaysPer400Years = DaysPer100Years * 4 + 1; // 146097

        // Number of days from 1/1/0001 to 12/31/1600
        private const int DaysTo1601 = DaysPer400Years * 4;          // 584388
        // Number of days from 1/1/0001 to 12/30/1899
        private const int DaysTo1899 = DaysPer400Years * 4 + DaysPer100Years * 3 - 367;
        // Number of days from 1/1/0001 to 12/31/9999
        private const int DaysTo10000 = DaysPer400Years * 25 - 366;  // 3652059

        internal const long MinTicks = 0;
        internal const long MaxTicks = DaysTo10000 * TicksPerDay - 1;
        private const long MaxMillis = (long)DaysTo10000 * MillisPerDay;

        private const long FileTimeOffset = DaysTo1601 * TicksPerDay;
        private const long DoubleDateOffset = DaysTo1899 * TicksPerDay;
        // The minimum OA date is 0100/01/01 (Note it's year 100).
        // The maximum OA date is 9999/12/31
        private const long OADateMinAsTicks = (DaysPer100Years - DaysPerYear) * TicksPerDay;
        // All OA dates must be greater than (not >=) OADateMinAsDouble
        private const double OADateMinAsDouble = -657435.0;
        // All OA dates must be less than (not <=) OADateMaxAsDouble
        private const double OADateMaxAsDouble = 2958466.0;

        public static double ToOADate(this DateTime date)
        {
            long value = date.Ticks;

            if (value == 0)
                return 0.0;  // Returns OleAut's zero'ed date value.
            if (value < TicksPerDay) // This is a fix for VB. They want the default day to be 1/1/0001 rathar then 12/30/1899.
                value += DoubleDateOffset; // We could have moved this fix down but we would like to keep the bounds check.
            if (value < OADateMinAsTicks)
                throw new OverflowException("Arg_OleAutDateInvalid"); //Environment.GetResourceString("Arg_OleAutDateInvalid")

            // Currently, our max date == OA's max date (12/31/9999), so we don't 
            // need an overflow check in that direction.
            long millis = (value - DoubleDateOffset) / TicksPerMillisecond;
            if (millis < 0)
            {
                long frac = millis % MillisPerDay;
                if (frac != 0) millis -= (MillisPerDay + frac) * 2;
            }
            return (double)millis / MillisPerDay;
        }

        // Converts an OLE Date to a tick count.
        // This function is duplicated in COMDateTime.cpp
        internal static long DoubleDateToTicks(double value)
        {
            // The check done this way will take care of NaN
            if (!(value < OADateMaxAsDouble) || !(value > OADateMinAsDouble))
                throw new ArgumentException("Arg_OleAutDateInvalid"); //Environment.GetResourceString("Arg_OleAutDateInvalid")

            // Conversion to long will not cause an overflow here, as at this point the "value" is in between OADateMinAsDouble and OADateMaxAsDouble
            long millis = (long)(value * MillisPerDay + (value >= 0 ? 0.5 : -0.5));
            // The interesting thing here is when you have a value like 12.5 it all positive 12 days and 12 hours from 01/01/1899
            // However if you a value of -12.25 it is minus 12 days but still positive 6 hours, almost as though you meant -11.75 all negative
            // This line below fixes up the millis in the negative case
            if (millis < 0)
            {
                millis -= (millis % MillisPerDay) * 2;
            }

            millis += DoubleDateOffset / TicksPerMillisecond;

            if (millis < 0 || millis >= MaxMillis) throw new ArgumentException("Arg_OleAutDateScale"); //Environment.GetResourceString("Arg_OleAutDateScale")
            return millis * TicksPerMillisecond;
        }

        public static DateTime FromOADate(double d)
        {
            return new DateTime(DoubleDateToTicks(d), DateTimeKind.Unspecified);
        }

        public static String ToShortDateString(this DateTime date)
        {
            return date.ToString(DateTimeFormatInfo.CurrentInfo.ShortDatePattern);
        }

        public static String ToShortTimeString(this DateTime date)
        {
            return date.ToString(DateTimeFormatInfo.CurrentInfo.ShortTimePattern);
        }
    }
}
