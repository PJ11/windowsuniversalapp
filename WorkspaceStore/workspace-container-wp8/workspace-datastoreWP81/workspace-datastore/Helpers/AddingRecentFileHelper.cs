﻿using System;
using System.Threading.Tasks;
using workspace_datastore.models;

namespace workspace_datastore.Helpers
{
    public class AddingRecentFileHelper
    {
        private static AddingRecentFileHelper _instance;

        public static AddingRecentFileHelper GetInstance()
        {
            if (_instance != null) return _instance;
            _instance = new AddingRecentFileHelper();
            return _instance;
        }

        public async Task AddRecentFile(string fileName, string filePath)
        {
            await AddRecentFile(fileName, filePath, null, null);
        }

        public async Task AddRecentFile(string oldFileName, string oldFilePath,
            string newFileName, string newFilePath)
        {
            if (newFileName == null) newFileName = oldFileName;
            if (newFilePath == null) newFilePath = oldFilePath;

            DataSourceAsync dsa = new DataSourceAsync();
            try
            {
                var existRecentFile = await dsa.Db.Table<RecentFile>()
                    .Where(m => m.name == oldFileName && m.path== oldFilePath).FirstOrDefaultAsync().ConfigureAwait(false);
                if (existRecentFile == null)
                {
                    var recentFile = new RecentFile
                    {
                        name = newFileName,
                        path = newFilePath,
                        recent_timestamp = DateTime.Now
                    };
                    int result = await dsa.Db.InsertAsync(recentFile).ConfigureAwait(false);
                }
                else
                {
                    existRecentFile.name = newFileName;
                    existRecentFile.path = newFilePath;
                    existRecentFile.recent_timestamp = DateTime.Now;
                    await dsa.Db.UpdateAsync(existRecentFile).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsa.Dispose();
            }
        }
    }
}