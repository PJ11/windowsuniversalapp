﻿#define ASYNC_SUPPORTED
#define USE_SQLCIPHER_SQLITE
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace workspace_datastore
{
    public sealed class DataSourceAsync : IDisposable
    {
        
        public SQLiteAsyncConnection Db { get; set; }
        //store connection in local property
        private static SQLiteAsyncConnection CacheDBConn { get; set; }
        public DataSourceAsync(bool init = false)
        {
            //We cannot make async constructor, so i have created new async mehtod and calling method using Action inside constructor
            new Action(async () =>
            {
                //call method to create connection object with DB password,Sometimes DB password was setting after calling the sql query
                await CreateConnection(init);
            }).Invoke();
        }
        private async Task CreateConnection(bool init)
        {
#if ASYNC_SUPPORTED
            try
            {
#if USE_SQLCIPHER_SQLITE
                if (CacheDBConn == null)
                {
                    this.Db = new SQLiteAsyncConnection(ConstantData.DBPath);
                    //wait till password set
                    //await this.Db.ExecuteAsync("PRAGMA key = \"x'" + ConstantData.DBHexKey + "'\"").ConfigureAwait(false);
#else
                   this.Db = new SQLiteAsyncConnection(ConstantData.DBPath);
#endif
                    if (init)
                    {
                        //Db.ExecuteAsync("PRAGMA page_size = 2048").ConfigureAwait(false);
                        // By setting PRAGMA synchronous=OFF, all sync operations are omitted. 
                        //This makes SQLite seem to run faster, but it also allows the operating system to freely reorder writes,
                        //which could result in database corruption if a power failure or hard reset occurs prior to all content reaching persistent storage.
                        Db.ExecuteAsync("PRAGMA synchronous = FULL").ConfigureAwait(false);
                        Db.ExecuteAsync("PRAGMA temp_store = MEMORY").ConfigureAwait(false);
                        Db.ExecuteAsync("PRAGMA count_changes = OFF").ConfigureAwait(false);
                    }
                    CacheDBConn = Db;
                }
                else
                {
                    //set DB connection from cached DB
                    Db = CacheDBConn;
                }
               }
            catch (Exception ex)
            {
                throw ex;
            }
#else
            throw new Exception("sqlcipher does not support SQLiteAsyncConnection. Please use DataSource instead of DataSourceAsync");
#endif
        }
        public void Dispose()
        {
            this.Db = null;
            //GC.Collect();
            //LoggingWP8.WP8Logger.LogMessage("Destroyed Async Connection");
        }
    }

    public sealed class DataSource : IDisposable
    {

        public SQLiteConnection Db { get; set; }

        public DataSource(bool init = false)
        {
            try
            {
#if USE_SQLCIPHER_SQLITE
                this.Db = new SQLiteConnection(ConstantData.DBPath);
                this.Db.Execute("PRAGMA key = \"x'" + ConstantData.DBHexKey + "'\"");
#else
                this.Db = new SQLiteConnection(ConstantData.DBPath);
#endif
                //LoggingWP8.WP8Logger.LogMessage("Creating Sync Connection");
                if(init)
                {
                    Db.Execute("PRAGMA synchronous = FULL");
                    Db.Execute("PRAGMA temp_store = MEMORY");
                    Db.Execute("PRAGMA count_changes = OFF");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            this.Db.Dispose();
            this.Db = null;
            //LoggingWP8.WP8Logger.LogMessage("Destroyed Sync Connection");
        }
    }
}
