﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Loads the Message_Updates table schema for a SQLite database.  This requires an open connection to the SQLite database.  This class will not close
/// or dispose of that connection when done, it is strickly referencing an open one that is managed outside of this class.
/// </summary>
namespace workspace_datastore.models
{
    //*********************************************************************************************************************
    // <copyright file="Message_Updates.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>27-03-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           31-03-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
    public class Message_Updates
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int id { get; set; }
        public string serverIdentifier { get; set; }
        public string collectionId { get; set; }
        public int folder_id { get; set; }
        public int read { get; set; }
        public int account_id { get; set; }
        public string flagType { get; set; }
        public int flagStatus { get; set; }
        public string moveToFolderServerIdentifier { get; set; }
        public double flagCompletedDate { get; set; }
        public double flagCompletedTime { get; set; }
        public double flagStartDate { get; set; }
        public double flagDueDate { get; set; }
        public double flagUTCStartDate { get; set; }
        public double flagUTCDueDate { get; set; }
        public int flagReminderSet { get; set; }
        public double flagReminderTime { get; set; }
        public double flagOrdinalDate { get; set; }
        public double flagSubOrdinalDate { get; set; }
        public int flag { get; set; }
        public int lastVerbExecuted { get; set; }
    }
}
