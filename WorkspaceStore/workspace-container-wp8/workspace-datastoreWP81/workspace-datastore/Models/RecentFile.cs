﻿using System;
using SQLite;

/* This table is responsible to hold all recent files information*/
namespace workspace_datastore.models
{
    public class RecentFile
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int id { get; set; }
        public string name { get; set; }
        public string path { get; set; }
        public DateTime recent_timestamp { get; set; }
    }
}