﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* This table is responsible to hold all the linked contacts.
   for now this column will hold the record_id of linked native contact. */
namespace workspace_datastore.Models
{
  public  class Linked
    {
         [PrimaryKey, AutoIncrement]
        public int _id { get; set; }
         public string datasource_id { get; set; }
         public int datasource_type { get; set; }
         public string first_name { get; set; }
         public string last_name { get; set; }
         public string display_name { get; set; }
   
         public int primary_flag { get; set; }
         public int primary_link_id { get; set; }
         public string avatar_link { get; set; }
         public int recent_contact_flag { get; set; }
         public int recent_timestamp { get; set; }
    }
}
