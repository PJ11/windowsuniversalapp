﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Loads the Contacts table schema for a SQLite database.  This requires an open connection to the SQLite database.  This class will not close
/// or dispose of that connection when done, it is strickly referencing an open one that is managed outside of this class.
/// </summary>
namespace workspace_datastore.models
{
    //*********************************************************************************************************************
    // <copyright file="Contacts.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>27-03-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           28-03-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
    public class Contacts
    {
        [PrimaryKey,AutoIncrement,Column("_id")]
        [Indexed(Name = "ID", Order = 1)]
        public int id { get; set; }
        //[PrimaryKey]
        public string serverId { get; set; }         
        public int folder_id { get; set; }
        public string clientID { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string companyName { get; set; }
        public string jobTitle { get; set; }
        public string email1Address { get; set; }
        public string email2Address { get; set; }
        public string email3Address { get; set; }
        public string homePhoneNumber { get; set; }
        public string home2PhoneNumber { get; set; }
        public string homeFaxNumber { get; set; }
        public string businessPhoneNumber { get; set; }
        public string business2PhoneNumber { get; set; }
        public string businessFaxNumber { get; set; }
        public string mobilePhoneNumber { get; set; }
        public string assistantPhoneNumber { get; set; }
        public string homeAddressStreet { get; set; }
        public string homeAddressCity { get; set; }
        public string homeAddressState { get; set; }
        public string homeAddressPostalCode { get; set; }
        public string homeAddressCountry { get; set; }
        public string businessAddressStreet { get; set; }
        public string businessAddressCity { get; set; }
        public string businessAddressState { get; set; }
        public string businessAddressPostalCode { get; set; }
        public string businessAddressCountry { get; set; }
        public string otherAddressStreet { get; set; }
        public string otherAddressCity { get; set; }
        public string otherAddressState { get; set; }
        public string otherAddressPostalCode { get; set; }
        public string otherAddressCountry { get; set; }
        public string fallbackDisplay { get; set; }
        public int dirty { get; set; }
        public int deleted { get; set; }

        public int numberOfContactUsed { get; set; }
        public bool isASContact = true;
    }
}
