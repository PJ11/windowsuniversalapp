﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Loads the Events table schema for a SQLite database.  This requires an open connection to the SQLite database.  This class will not close
/// or dispose of that connection when done, it is strickly referencing an open one that is managed outside of this class.
/// </summary>
namespace workspace_datastore.models
{
    //*********************************************************************************************************************
    // <copyright file="Events.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>27-03-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           28-03-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
    public class Events
    {
        [PrimaryKey, AutoIncrement]
        public int _id { get; set; }
        public int id { get; set; }
        public int Original_id { get; set; }
        public string serverId { get; set; }
        public int folderID { get; set; }
        public string subject { get; set; }
        public string location { get; set; }
        public string description { get; set; }
        public string organizerName { get; set; }
        public string organizerEmail { get; set; }
        public int selfAttendeeStatus { get; set; }
        public int meetingStatus { get; set; }
        public string timeCreated { get; set; }
        public string timeZone { get; set; }
        public string uid { get; set; }
        public int availability { get; set; }
        public int allDay { get; set; }
        public double startDayAndTime { get; set; }
        public double endDayAndTime { get; set; }
        public int dtfinal { get; set; }
        public int originalReminder { get; set; }
        public int responseRequested { get; set; }
        public int dirty { get; set; }
        public int deleted { get; set; }
        public int reoccurs { get; set; }
        public int rcType { get; set; }
        public int rcInterval { get; set; }
        public int rcDayOfWeek { get; set; }
        public int rcDayOfMonth { get; set; }
        public int rcWeekOfMonth { get; set; }
        public int rcMonthOfYear { get; set; }
        public int rcFirstDayOfWeek { get; set; }
        public int rcIsLeapMonth { get; set; }
        public int rcCalendarType { get; set; }
        public double rcUntil { get; set; }
        public int rcOccurences { get; set; }
        public string rcVisibleExceptions { get; set; }
        public string rcHiddenExceptions { get; set; }
        public void CopyToThis(Events src)
        {
            this.serverId = src.serverId;
            this.folderID = src.folderID;
            this.subject = src.subject;
            this.location = src.location;
            this.description = src.description;
            this.organizerName = src.organizerName;
            this.organizerEmail = src.organizerEmail;
            this.selfAttendeeStatus = src.selfAttendeeStatus;
            this.meetingStatus = src.meetingStatus;
            this.timeCreated = src.timeCreated;
            this.timeZone = src.timeZone;
            this.uid = src.uid;
            this.availability = src.availability;
            this.allDay = src.allDay;
            this.startDayAndTime = src.startDayAndTime;
            this.endDayAndTime = src.endDayAndTime;
            this.dtfinal = src.dtfinal;
            this.originalReminder = src.originalReminder;
            this.responseRequested = src.responseRequested;
            this.dirty = src.dirty;
            this.deleted = src.deleted;
            this.reoccurs = src.reoccurs;
            this.rcType = src.rcType;
            this.rcInterval = src.rcInterval;
            this.rcDayOfWeek = src.rcDayOfWeek;
            this.rcDayOfMonth = src.rcDayOfMonth;
            this.rcWeekOfMonth = src.rcWeekOfMonth;
            this.rcMonthOfYear = src.rcMonthOfYear;
            this.rcFirstDayOfWeek = src.rcFirstDayOfWeek;
            this.rcIsLeapMonth = src.rcIsLeapMonth;
            this.rcCalendarType = src.rcCalendarType;
            this.rcUntil = src.rcUntil;
            this.rcOccurences = src.rcOccurences;
        }
       
    }
}
