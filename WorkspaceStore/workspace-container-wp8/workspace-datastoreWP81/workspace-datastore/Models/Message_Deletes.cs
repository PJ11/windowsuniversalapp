﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Loads the Message_Deletes table schema for a SQLite database.  This requires an open connection to the SQLite database.  This class will not close
/// or dispose of that connection when done, it is strickly referencing an open one that is managed outside of this class.
/// </summary>
namespace workspace_datastore.models
{
    //*********************************************************************************************************************
    // <copyright file="Message_Deletes.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>27-03-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           31-03-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
    public class Message_Deletes
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int id { get; set; }
        public string serverIdentifier { get; set; }
        public string collectionId { get; set; }
        public int folder_id { get; set; }
        public int account_id { get; set; }
    }
}
