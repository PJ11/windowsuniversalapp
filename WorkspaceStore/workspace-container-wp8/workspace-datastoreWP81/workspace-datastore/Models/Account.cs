﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Loads the Account table schema for a SQLite database.  This requires an open connection to the SQLite database.  This class will not close
/// or dispose of that connection when done, it is strickly referencing an open one that is managed outside of this class.
/// </summary>
namespace workspace_datastore.models
{
    //*********************************************************************************************************************
    // <copyright file="Account.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>27-03-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           27-03-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
    public class Account
    {
        /// <summary>
        /// auto incremented id of Account table
        /// </summary>
        [PrimaryKey]
        public int id { get; set; }
        /// <summary>
        /// port number
        /// </summary>
        public int port { get; set; }
        public int flags  { get; set; }
        public string emailAddress { get; set; }
        public string authUser { get; set; }
        public string domainName { get; set; }
        public string authPass { get; set; }
        public string serverUrl { get; set; }
        public string policyKey { get; set; }
        public string certAlias { get; set; }
        public string folderSyncKey { get; set; }
        public string protocolVersion { get; set; }
    }
}
