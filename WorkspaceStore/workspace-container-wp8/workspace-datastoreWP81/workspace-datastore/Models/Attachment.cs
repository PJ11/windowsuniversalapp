﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Loads the Attachment table schema for a SQLite database.  This requires an open connection to the SQLite database.  This class will not close
/// or dispose of that connection when done, it is strickly referencing an open one that is managed outside of this class.
/// </summary>
namespace workspace_datastore.models
{
    //*********************************************************************************************************************
    // <copyright file="Attachment.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>27-03-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           27-03-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
   public class Attachment
    {
        [PrimaryKey]
        public Guid id { get; set; }
        public string displayName { get; set; }
        public string fileReference { get; set; }
        public string filePath { get; set; }
        public int method { get; set; }
        public int estimatedSize { get; set; }
        public string contentIdentifier { get; set; }
        public int isInline { get; set; }

        public int folder_id { get; set; }
        public int message_id { get; set; }
        public string contentType { get; set; }
    }
}
