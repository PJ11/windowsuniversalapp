﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Loads the Message table schema for a SQLite database.  This requires an open connection to the SQLite database.  This class will not close
/// or dispose of that connection when done, it is strickly referencing an open one that is managed outside of this class.
/// </summary>
namespace workspace_datastore.models
{
    //*********************************************************************************************************************
    // <copyright file="Message.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>27-03-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           31-03-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
    public class Message
    {
        [PrimaryKey, AutoIncrement]
        public int _id { get; set; }
        public int id { get; set; }
        [Indexed(Name = "FolderId_Date", Order = 1)]
        public int folder_id { get; set; }
        [Indexed(Name = "ServerID", Order = 1)]
        public string serverIdentifier { get; set; }
        public string toRecipientText { get; set; }
        public string toRecipientEmail { get; set; }
        public string ccRecipientText { get; set; }
        public string bccRecipientText { get; set; }
        public bool isRecipientAsBcc { get; set; }
        public string fromEmail { get; set; }
        public string displayFromEmail { get; set; }
        [Indexed(Name = "FolderId_Date", Order = 2)]
        public double receivedDate { get; set; }
        public string subject { get; set; }
        public int read { get; set; }
        public int importance { get; set; }
        public string previewText { get; set; }
        public string bodyText { get; set; }     
        public string mimeType { get; set; }
        public string conversationId { get; set; }
        public string conversationIndex { get; set; }
        public int lastVerbExecuted { get; set; }
        public string flagType { get; set; }
        public int flagStatus { get; set; }
        public int draftActionType { get; set; }
        public int draftActionReferenceMessage_id { get; set; }
        public string draftActionReferenceMessage_longid { get; set; }
        public double flagCompletedDate { get; set; }
        public double flagCompletedTime { get; set; }
        public double flagStartDate { get; set; }
        public double flagDueDate { get; set; }
        public double flagUTCStartDate { get; set; }
        public double flagUTCDueDate { get; set; }
        public int flagReminderSet { get; set; }
        public double flagReminderTime { get; set; }
        public double flagOrdinalDate { get; set; }
        public double flagSubOrdinalDate { get; set; }
        public int flag { get; set; }
        public bool hasAttachment { get; set; }       
        //Calendar specific variables

        public string messageClass { get; set; }
        public int event_id { get; set; }
        public int allDay { get; set; }
        public string location { get; set; }
        public string timeZone { get; set; }
        public double startDayAndTime { get; set; }
        public double endDayAndTime { get; set; }
        public string uid { get; set; }
        public int availability { get; set; }
        public int originalReminder { get; set; }
        public int responseRequested { get; set; }
        public int meetingStatus { get; set; }
        public string organizerName { get; set; }
        public string organizerEmail { get; set; }
      
    }
}
