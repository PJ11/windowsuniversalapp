﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace workspace_datastore.models
{
   public class LiveTileStatus
    {
        [PrimaryKey,AutoIncrement]
        public int id { get; set; }
        /// <summary>
        /// port number
        /// </summary>
        public bool isCalendarLiveTile { get; set; }
    }
}
