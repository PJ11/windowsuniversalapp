﻿using SQLite;
using System;

/// <summary>
/// Loads the Outbox table schema for a SQLite database.  This requires an open connection to the SQLite database.  This class will not close
/// or dispose of that connection when done, it is strickly referencing an open one that is managed outside of this class.
/// </summary>
namespace workspace_datastore.models
{
    //*********************************************************************************************************************
    // <copyright file="Outbox.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>27-03-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           31-03-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
    public class Outbox
    {
        [PrimaryKey, AutoIncrement]
        public int message_id { get; set; }
        public int folderId { get; set; }
        public string clientIdentifier { get; set; }
        public int account_id { get; set; }
        public string bodyText { get; set; }
        public string attachmentFilePathsList { get; set; }
        public double creationTime { get; set; }
        public int actionType { get; set; }
        public string actionFolderServerId { get; set; }
        public string actionMessageServerId { get; set; }
        public string actionMessageServerLongId { get; set; }
    }
}
