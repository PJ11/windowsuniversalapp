﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Loads the Folder table schema for a SQLite database.  This requires an open connection to the SQLite database.  This class will not close
/// or dispose of that connection when done, it is strickly referencing an open one that is managed outside of this class.
/// </summary>
namespace workspace_datastore.models
{
    //*********************************************************************************************************************
    // <copyright file="Folder.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>27-03-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           31-03-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
    public class Folder
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int id { get; set; }
        public string displayName { get; set; }
        public string serverIdentifier { get; set; }
        public int account_id { get; set; }
        public string syncKey { get; set; }
        public double syncTime { get; set; }
        public int folderType { get; set; }
        public string folderClass { get; set; }
        public int parentFolder_id { get; set; }
        public int syncState { get; set; }
    }
}
