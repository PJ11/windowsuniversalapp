﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using workspace_datastore.Helpers;
using workspace_datastore.models;

/// <summary>
/// it incudes feature to add, edit ,get ,and delete event data into\from database.
/// </summary>
namespace workspace_datastore.Managers
{
    //*********************************************************************************************************************
    // <copyright file="EventManager.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>14-04-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           14-04-2014    1.0            First Version </summary>
    //*********************************************************************************************************************

    public class EventManager
    {
        #region Get event information
        /// <summary>
        /// method to get all events info or get event info based on event id(if id=0 then it will return all events info)
        /// </summary>
        /// <param name="EventID">Event id</param>
        /// <returns></returns>
        public List<Events> GetEventInfo(int folderID, int EventID)
        {
            //create local varibale to store event info into list 
            List<Events> existEventInfo = null;
            DataSource ds = new DataSource();
            try
            {
                //check if event id ==0 then retrun all events info
                if (EventID == 0)
                {
                    //get all event info
                    existEventInfo = ds.Db.Table<Events>().ToList();
                }
                else
                {
                    //get event info based on event id
                    existEventInfo = ds.Db.Table<Events>().Where(c => c.id == EventID && c.folderID == folderID).ToList();
                }
                return existEventInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        public async Task<List<Events>> GetEventInfoVasedonUIDAsync(string UID)
        {
            //create local varibale to store event info into list 
            List<Events> existEventInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get event info based on event id                
                existEventInfo = await ds.Db.Table<Events>().Where(c => c.uid.ToLower() == UID.ToLower()).ToListAsync().ConfigureAwait(false);
                return existEventInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        public async Task<List<Events>> GetEventInfoAsync(int folderID, int EventID)
        {
            //create local varibale to store event info into list 
            List<Events> existEventInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //check if event id ==0 then retrun all events info
                if (EventID == 0)
                {
                    //get all event info
                    existEventInfo = await ds.Db.Table<Events>().ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    //get event info based on event id
                    existEventInfo = await ds.Db.Table<Events>().Where(c => c.id == EventID && c.folderID == folderID && c.Original_id == 0).ToListAsync().ConfigureAwait(false);
                }
                //dispose object
                // db.Dispose();
                //return result
                return existEventInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<List<Events>> GetEventDurationInfoAsync(string serverId)
        {
            //create local varibale to store event info into list 
           List<Events> existEventInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                existEventInfo = await ds.Db.Table<Events>().Where(c => (c.serverId == serverId)).ToListAsync().ConfigureAwait(false);         
                return existEventInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        /// <summary>
        /// Gets days only required
        /// 0 first load of data
        /// 1 upscroll
        /// 2 downscroll
        /// 3 when on panaroma screen
        /// </summary>
        /// <param name="folderID"></param>
        /// <param name="baseDay"></param>
        /// <param name="daysToRetrieve"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public async Task<List<Events>> GetEventInfoForDaysAsync(double baseDay, int daysToRetrieve, int direction)
        {
            List<Events> existEventInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            DateTime dateToday = DateTimeExtensions.FromOADate(baseDay);
            double startDayofEventsTodisplay = baseDay; // LWR 06/24/2015 TEST  dateToday.AddDays(-1 * daysToRetrieve).ToOADate();
            double endDayofEventsTodisplay = dateToday.AddDays(1 * daysToRetrieve).ToOADate();
            try
            {
                existEventInfo =await ds.Db.Table<Events>().Where(c => ((c.reoccurs == 1) || (c.startDayAndTime >= startDayofEventsTodisplay) && (c.startDayAndTime <= endDayofEventsTodisplay))).ToListAsync().ConfigureAwait(false);//.Result;
#if NO
                switch (direction)
                {
                    case 0:
                        existEventInfo = ds.Db.Table<Events>().Where(c => ((c.reoccurs == 1) || (c.startDayAndTime > startDayofEventsTodisplay) && (c.startDayAndTime <= endDayofEventsTodisplay))).ToListAsync().Result;
                        break;
                    case 1:
                        existEventInfo = ds.Db.Table<Events>().Where(c => ((c.startDayAndTime > startDayofEventsTodisplay) && (c.startDayAndTime <= baseDay))).ToListAsync().Result;
                        break;
                    case 2:
                        existEventInfo = ds.Db.Table<Events>().Where(c => ((c.reoccurs == 1) || (c.startDayAndTime >= baseDay) && (c.startDayAndTime < endDayofEventsTodisplay))).ToListAsync().Result;
                        break;
                    case 3:
                        existEventInfo = ds.Db.Table<Events>().Where(c => ((c.startDayAndTime >= baseDay) && (c.startDayAndTime <= endDayofEventsTodisplay))).ToListAsync().Result;
                        break;               
                    default:
                        break;
                }
#endif
                return existEventInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        
        public async Task<List<Events>> GetEventInfoForMontViewAsync(double baseDay, int daysToRetrieve, int direction)
        {
            List<Events> existEventInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            DateTime dateToday = DateTimeExtensions.FromOADate(baseDay);
            double startDayofEventsTodisplay = baseDay; // LWR 06/24/2015 TEST  dateToday.AddDays(-1 * daysToRetrieve).ToOADate();
            double endDayofEventsTodisplay = dateToday.AddDays(1 * daysToRetrieve).ToOADate();
            try
            {
                string query = "SELECT id,Original_id, subject, startDayAndTime,endDayAndTime,location,reoccurs FROM Events " +
                     "Where ((startDayAndTime BETWEEN " + startDayofEventsTodisplay + " AND " + endDayofEventsTodisplay + ") or reoccurs=1)";
                //get event info based on date range
                existEventInfo = await ds.Db.QueryAsync<Events>(query).ConfigureAwait(false);
                return existEventInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        /// <summary>
        /// get past reoccuring event
        /// </summary>
        /// <param name="folderID"></param>
        /// <param name="baseDay"></param>
        /// <param name="daysToRetrieve"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public async Task<List<Events>> GetRecurrentEventInfoForDaysAsync(double baseDay, int daysToRetrieve, int direction)
        {
            //List<Events> recurrentEventInfinite = null;
            //List<Events> recurrentEventInfinite = null;
            List<Events> existEventInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            DateTime dateToday = DateTimeExtensions.FromOADate(baseDay);
            double startDayofEventsTodisplay = baseDay; // LWR 06/24/2015 TEST dateToday.AddDays(-1 * daysToRetrieve).ToOADate();
            double endDayofEventsTodisplay = dateToday.AddDays(1 * daysToRetrieve).ToOADate();
            try
            {
                //switch (direction)
                //{
                //    case 0:  
                if (direction != 5)
                {
                    existEventInfo = ds.Db.Table<Events>().Where(c => (
                                                                        ((c.startDayAndTime < startDayofEventsTodisplay) && (c.rcUntil == 0.0) && (c.reoccurs == 1))) || 
                                                                        ((c.startDayAndTime < startDayofEventsTodisplay) && (c.rcUntil > startDayofEventsTodisplay) && (c.reoccurs == 1)) || 
                                                                        ((c.startDayAndTime < startDayofEventsTodisplay) && (c.rcOccurences > 0) && (c.reoccurs == 1))).ToListAsync().Result;
                }
                else
                {
                    existEventInfo = ds.Db.Table<Events>().Where(c => (
                                                                        ((c.startDayAndTime < baseDay) && (c.rcUntil == 0.0) && (c.reoccurs == 1))) || 
                                                                        ((c.startDayAndTime < baseDay) && (c.rcUntil > baseDay) && (c.reoccurs == 1)) || 
                                                                        ((c.startDayAndTime < baseDay) && (c.rcOccurences > 0) && (c.reoccurs == 1))).ToListAsync().Result;
                }
                if (DateTime.Today.AddDays(1).ToOADate() == baseDay)
                {
                    existEventInfo = ds.Db.Table<Events>().Where(c => (((c.startDayAndTime < baseDay) && (c.rcUntil == 0.0) && (c.reoccurs == 1))) || ((c.startDayAndTime < baseDay) && (c.rcUntil > baseDay) && (c.reoccurs == 1)) || ((c.startDayAndTime < baseDay) && (c.rcOccurences > 0) && (c.reoccurs == 1))).ToListAsync().Result;
                }
                // existEventInfo = ds.Db.Table<Events>().Where(c => ((c.startDayAndTime < startDayofEventsTodisplay))).ToList();
                //        break;
                //    case 1:
                //        existEventInfo = ds.Db.Table<Events>().Where(c => ((c.startDayAndTime > startDayofEventsTodisplay) && (c.startDayAndTime <= baseDay))).ToList();
                //        break;
                //    case 2:
                //        existEventInfo = ds.Db.Table<Events>().Where(c => ((c.startDayAndTime >= baseDay) && (c.startDayAndTime < endDayofEventsTodisplay))).ToList();
                //        break;
                //    default:
                //        break;
                //}

                return existEventInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        /// <summary>
        /// Retrieves a list of multi-day SINGLE INSTANCE events that falls within this block of days STARTING with a baseDay
        /// </summary>
        public async Task<List<Events>> GetMultipleDayEventInfoAsync(double baseDay, int daysToRetrieve, int direction)
        {
            //List<Events> recurrentEventInfinite = null;
            //List<Events> recurrentEventInfinite = null;
            List<Events> existEventInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            DateTime dateToday = DateTimeExtensions.FromOADate(baseDay);
            double startDayofEventsTodisplay = baseDay; // LWR 06/24/2015 TEST dateToday.AddDays(-1 * daysToRetrieve).ToOADate();
            double endDayofEventsTodisplay = dateToday.AddDays(1 * daysToRetrieve).ToOADate();
            try
            {
                if (direction == 5)
                {
                    existEventInfo =await ds.Db.Table<Events>().Where(c => (c.startDayAndTime < startDayofEventsTodisplay) && (c.reoccurs == 0) && (c.endDayAndTime >= startDayofEventsTodisplay)).ToListAsync().ConfigureAwait(false);             
                }
                else
                {
                    existEventInfo =await ds.Db.Table<Events>().Where(c => (c.startDayAndTime < startDayofEventsTodisplay) && (c.reoccurs == 0) && (c.endDayAndTime >= startDayofEventsTodisplay)).ToListAsync().ConfigureAwait(false);                
                }              
                return existEventInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }


        /// <summary>
        /// Gets days only required
        /// </summary>
        /// <param name="folderID"></param>
        /// <param name="baseDay"></param>
        /// <param name="daysToRetrieve"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public async Task<List<Events>> GetEventInfoForDaysAsync(int folderID, double baseDay, int daysToRetrieve, int direction)
        {
            List<Events> existEventInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            DateTime dateToday = DateTimeExtensions.FromOADate(baseDay);
            double startDayofEventsTodisplay = dateToday.AddDays(-1 * daysToRetrieve).ToOADate();
            double endDayofEventsTodisplay = dateToday.AddDays(1 * daysToRetrieve).ToOADate();
            try
            {
                switch (direction)
                {
                    case 0:
                        existEventInfo =await ds.Db.Table<Events>().Where(c => ((c.startDayAndTime > startDayofEventsTodisplay) && (c.startDayAndTime <= endDayofEventsTodisplay))).ToListAsync().ConfigureAwait(false);
                        break;
                    case 1:
                        existEventInfo =await ds.Db.Table<Events>().Where(c => ((c.startDayAndTime > startDayofEventsTodisplay) && (c.startDayAndTime <= baseDay))).ToListAsync().ConfigureAwait(false);
                        break;
                    case 2:
                        existEventInfo =await ds.Db.Table<Events>().Where(c => ((c.startDayAndTime >= baseDay) && (c.startDayAndTime < endDayofEventsTodisplay))).ToListAsync().ConfigureAwait(false);
                        break;
                    default:
                        break;
                }

                return existEventInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        /// <summary>
        /// get past reoccuring event
        /// </summary>
        /// <param name="folderID"></param>
        /// <param name="baseDay"></param>
        /// <param name="daysToRetrieve"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public async Task<List<Events>> GetRecurrentEventInfoForDaysAsync(int folderID, double baseDay, int daysToRetrieve, int direction)
        {
            //List<Events> recurrentEventInfinite = null;
            //List<Events> recurrentEventInfinite = null;
            List<Events> existEventInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            DateTime dateToday = DateTimeExtensions.FromOADate(baseDay);
            double startDayofEventsTodisplay = dateToday.AddDays(-1 * daysToRetrieve).ToOADate();
            double endDayofEventsTodisplay = dateToday.AddDays(1 * daysToRetrieve).ToOADate();
            try
            {
                //switch (direction)
                //{
                //    case 0:
                existEventInfo =await ds.Db.Table<Events>().Where(c => (((c.startDayAndTime < startDayofEventsTodisplay) && (c.rcUntil == 0.0) && (c.reoccurs == 1))) || ((c.startDayAndTime < startDayofEventsTodisplay) && (c.rcUntil > startDayofEventsTodisplay) && (c.reoccurs == 1)) || ((c.startDayAndTime < startDayofEventsTodisplay) && (c.rcOccurences > 0) && (c.reoccurs == 1))).ToListAsync().ConfigureAwait(false);
                // existEventInfo = ds.Db.Table<Events>().Where(c => ((c.startDayAndTime < startDayofEventsTodisplay))).ToList();
                //        break;
                //    case 1:
                //        existEventInfo = ds.Db.Table<Events>().Where(c => ((c.startDayAndTime > startDayofEventsTodisplay) && (c.startDayAndTime <= baseDay))).ToList();
                //        break;
                //    case 2:
                //        existEventInfo = ds.Db.Table<Events>().Where(c => ((c.startDayAndTime >= baseDay) && (c.startDayAndTime < endDayofEventsTodisplay))).ToList();
                //        break;
                //    default:
                //        break;
                //}

                return existEventInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Add event information
        /// <summary>
        /// Add event information into database
        /// </summary>
        /// <param name="ObjEvent">Event object</param>
        public void AddEventInfo(SQLiteConnection db, Events ObjEvent)
        {
            try
            {
                //set all the event data here
                var newEvent = new Events()
                {
                    serverId = ObjEvent.serverId,
                    folderID = ObjEvent.folderID,
                    subject = ObjEvent.subject,
                    location = ObjEvent.location,
                    description = ObjEvent.description,
                    organizerName = ObjEvent.organizerName,
                    organizerEmail = ObjEvent.organizerEmail,
                    selfAttendeeStatus = ObjEvent.selfAttendeeStatus,
                    meetingStatus = ObjEvent.meetingStatus,
                    timeCreated = ObjEvent.timeCreated,
                    timeZone = ObjEvent.timeZone,
                    uid = ObjEvent.uid,
                    availability = ObjEvent.availability,
                    allDay = ObjEvent.allDay,
                    startDayAndTime = ObjEvent.startDayAndTime,
                    endDayAndTime = ObjEvent.endDayAndTime,
                    dtfinal = ObjEvent.dtfinal,
                    originalReminder = ObjEvent.originalReminder,
                    responseRequested = ObjEvent.responseRequested,
                    dirty = ObjEvent.dirty,
                    deleted = ObjEvent.deleted,
                    reoccurs = ObjEvent.reoccurs,
                    rcType = ObjEvent.rcType,
                    rcInterval = ObjEvent.rcInterval,
                    rcDayOfWeek = ObjEvent.rcDayOfWeek,
                    rcDayOfMonth = ObjEvent.rcDayOfMonth,
                    rcWeekOfMonth = ObjEvent.rcWeekOfMonth,
                    rcMonthOfYear = ObjEvent.rcMonthOfYear,
                    rcFirstDayOfWeek = ObjEvent.rcFirstDayOfWeek,
                    rcIsLeapMonth = ObjEvent.rcIsLeapMonth,
                    rcCalendarType = ObjEvent.rcCalendarType,
                    rcUntil = ObjEvent.rcUntil,
                    rcOccurences = ObjEvent.rcOccurences,
                    rcVisibleExceptions = ObjEvent.rcVisibleExceptions,
                    rcHiddenExceptions = ObjEvent.rcHiddenExceptions
                };
                //insert event information into database
                db.Insert(newEvent);
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }

        public async void AddAllEventsInfoAsync(List<Events> events,List<Attendees> attendees,List<Events> eventExceptions)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {               
                if (events.Count > 0)
                {
                    await ds.Db.RunInTransactionAsync(async (SQLiteAsyncConnection connection) =>
                    {  
                        //add all events
                        await connection.InsertAllAsync(events).ConfigureAwait(false);
                        //add all attendee
                        AttendeeManager objAttendeeMngr = new AttendeeManager();
                        objAttendeeMngr.AddAllAttendees(attendees);
                        //add all exceptions
                        AddAllExceptionInfoAsync(eventExceptions);
                    });
                }               
            }
            catch (Exception ex)
            { }
            finally
            {
                ds.Db = null;
            }
        }

        public async void AddEventInfoAsync(Events ObjEvent)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                   //await conn.RunInTransactionAsync(async (SQLiteAsyncConnection connection) =>
                   // { //get event info base on event id
                        var eventtoupdate = await ds.Db.Table<Events>().Where(c => c.serverId == ObjEvent.serverId && c.Original_id == 0).ToListAsync().ConfigureAwait(false);
                        if (eventtoupdate != null && eventtoupdate.Count > 0)
                        {
                            return;
                        }
                        //set all the event data here
                        var newEvent = new Events()
                        {
                            id = ObjEvent.id,
                            serverId = ObjEvent.serverId,
                            folderID = ObjEvent.folderID,
                            subject = ObjEvent.subject,
                            location = ObjEvent.location,
                            description = ObjEvent.description,
                            organizerName = ObjEvent.organizerName,
                            organizerEmail = ObjEvent.organizerEmail,
                            selfAttendeeStatus = ObjEvent.selfAttendeeStatus,
                            meetingStatus = ObjEvent.meetingStatus,
                            timeZone = ObjEvent.timeZone,
                            uid = ObjEvent.uid,
                            availability = ObjEvent.availability,
                            allDay = ObjEvent.allDay,
                            startDayAndTime = ObjEvent.startDayAndTime,
                            endDayAndTime = ObjEvent.endDayAndTime,
                            dtfinal = ObjEvent.dtfinal,
                            originalReminder = ObjEvent.originalReminder,
                            responseRequested = ObjEvent.responseRequested,
                            dirty = ObjEvent.dirty,
                            deleted = ObjEvent.deleted,
                            reoccurs = ObjEvent.reoccurs,
                            rcType = ObjEvent.rcType,
                            rcInterval = ObjEvent.rcInterval,
                            rcDayOfWeek = ObjEvent.rcDayOfWeek,
                            rcDayOfMonth = ObjEvent.rcDayOfMonth,
                            rcWeekOfMonth = ObjEvent.rcWeekOfMonth,
                            rcMonthOfYear = ObjEvent.rcMonthOfYear,
                            rcFirstDayOfWeek = ObjEvent.rcFirstDayOfWeek,
                            rcIsLeapMonth = ObjEvent.rcIsLeapMonth,
                            rcCalendarType = ObjEvent.rcCalendarType,
                            rcUntil = ObjEvent.rcUntil,
                            rcOccurences = ObjEvent.rcOccurences,
                            rcVisibleExceptions = ObjEvent.rcVisibleExceptions,
                            rcHiddenExceptions = ObjEvent.rcHiddenExceptions
                        };
                        //insert event information into database

                         await ds.Db.InsertAsync(newEvent).ConfigureAwait(false);
                    //});
            }
            catch (Exception ex)
            {
                //throw exception
                LoggingWP8.WP8Logger.LogMessage(ex.Message + ex.Source);
                throw ex;
            }
        }
        #endregion
        public async void AddAllExceptionInfoAsync(IList<Events> events)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                if (events.Count > 0)
                {
                    await ds.Db.RunInTransactionAsync(async (SQLiteAsyncConnection connection) =>
                    {
                        await connection.InsertAllAsync(events).ConfigureAwait(false);
                    });
                }
            }
            catch (Exception ex)
            { }
            finally
            {
                ds.Db = null;
            }
        }
        public async void AddExceptionInfoAsync(Events ObjEvent)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get event info base on event id
                var checkeventtoupdate = await ds.Db.Table<Events>().Where(c => c.serverId == ObjEvent.serverId).ToListAsync().ConfigureAwait(false);
                if (checkeventtoupdate != null && checkeventtoupdate.Count > 0)
                {
                    var getCurrentDateEvent = checkeventtoupdate.Where(c => DateTimeExtensions.FromOADate(c.startDayAndTime).Date == DateTimeExtensions.FromOADate(ObjEvent.startDayAndTime).Date &&
                        c.Original_id > 0).ToList();
                    // eventtoupdate = checkeventtoupdate.FirstOrDefault();
                    if (getCurrentDateEvent != null && getCurrentDateEvent.Count > 0)
                    {
                        var eventtoupdate = getCurrentDateEvent.FirstOrDefault();
                        eventtoupdate.subject = ObjEvent.subject;
                        eventtoupdate.location = ObjEvent.location;
                        eventtoupdate.description = ObjEvent.description;
                        eventtoupdate.startDayAndTime = ObjEvent.startDayAndTime;
                        eventtoupdate.endDayAndTime = ObjEvent.endDayAndTime;

                        eventtoupdate.rcVisibleExceptions = ObjEvent.rcVisibleExceptions;
                        eventtoupdate.rcHiddenExceptions = ObjEvent.rcHiddenExceptions;
                        //update event info
                        await ds.Db.UpdateAsync(eventtoupdate).ConfigureAwait(false);
                        return;
                    }
                }
                //set all the event data here
                var newEvent = new Events()
                {
                    id = ObjEvent.id,
                    serverId = ObjEvent.serverId,
                    folderID = ObjEvent.folderID,
                    subject = ObjEvent.subject,
                    location = ObjEvent.location,
                    description = ObjEvent.description,
                    organizerName = ObjEvent.organizerName,
                    organizerEmail = ObjEvent.organizerEmail,
                    selfAttendeeStatus = ObjEvent.selfAttendeeStatus,
                    meetingStatus = ObjEvent.meetingStatus,
                    timeZone = ObjEvent.timeZone,
                    uid = ObjEvent.uid,
                    availability = ObjEvent.availability,
                    allDay = ObjEvent.allDay,
                    startDayAndTime = ObjEvent.startDayAndTime,
                    endDayAndTime = ObjEvent.endDayAndTime,
                    dtfinal = ObjEvent.dtfinal,
                    originalReminder = ObjEvent.originalReminder,
                    responseRequested = ObjEvent.responseRequested,
                    dirty = ObjEvent.dirty,
                    deleted = ObjEvent.deleted,
                    reoccurs = ObjEvent.reoccurs,
                    rcType = ObjEvent.rcType,
                    rcInterval = ObjEvent.rcInterval,
                    rcDayOfWeek = ObjEvent.rcDayOfWeek,
                    rcDayOfMonth = ObjEvent.rcDayOfMonth,
                    rcWeekOfMonth = ObjEvent.rcWeekOfMonth,
                    rcMonthOfYear = ObjEvent.rcMonthOfYear,
                    rcFirstDayOfWeek = ObjEvent.rcFirstDayOfWeek,
                    rcIsLeapMonth = ObjEvent.rcIsLeapMonth,
                    rcCalendarType = ObjEvent.rcCalendarType,
                    rcUntil = ObjEvent.rcUntil,
                    rcOccurences = ObjEvent.rcOccurences,
                    rcVisibleExceptions = ObjEvent.rcVisibleExceptions,
                    //rcHiddenExceptions = ObjEvent.rcHiddenExceptions,
                    Original_id = ObjEvent.Original_id
                };
                //insert event information into database

                await ds.Db.InsertAsync(newEvent).ConfigureAwait(false);

            }
            catch (Exception ex)
            {
                //throw exception
                LoggingWP8.WP8Logger.LogMessage(ex.Message + ex.Source);
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        #region Update event information
        /// <summary>
        /// Update partucluar event information into database
        /// </summary>
        /// <param name="ObjEvent">Event object</param>
        public void UpdateEventInfo(Events ObjEvent)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                // DataSourceAsync ds = new DataSourceAsync();  
                //get event info base on event id
                var existingData = ds.Db.Table<Events>().Where(c => c._id == ObjEvent._id).ToListAsync().Result;
                if (existingData != null && existingData.Count > 0)
                {
                    var eventtoupdate = existingData.FirstOrDefault();
                    //if event exists
                    if (eventtoupdate != null)
                    {
                        eventtoupdate.id = ObjEvent.id;
                        eventtoupdate.serverId = ObjEvent.serverId;
                        eventtoupdate.folderID = ObjEvent.folderID;
                        eventtoupdate.subject = ObjEvent.subject;
                        eventtoupdate.location = ObjEvent.location;
                        eventtoupdate.description = ObjEvent.description;
                        eventtoupdate.organizerName = ObjEvent.organizerName;
                        eventtoupdate.organizerEmail = ObjEvent.organizerEmail;
                        eventtoupdate.selfAttendeeStatus = ObjEvent.selfAttendeeStatus;
                        eventtoupdate.meetingStatus = ObjEvent.meetingStatus;
                        eventtoupdate.timeCreated = ObjEvent.timeCreated;
                        eventtoupdate.timeZone = ObjEvent.timeZone;
                        eventtoupdate.uid = ObjEvent.uid;
                        eventtoupdate.availability = ObjEvent.availability;
                        eventtoupdate.allDay = ObjEvent.allDay;
                        eventtoupdate.startDayAndTime = ObjEvent.startDayAndTime;
                        eventtoupdate.endDayAndTime = ObjEvent.endDayAndTime;
                        eventtoupdate.dtfinal = ObjEvent.dtfinal;
                        eventtoupdate.originalReminder = ObjEvent.originalReminder;
                        eventtoupdate.responseRequested = ObjEvent.responseRequested;
                        eventtoupdate.dirty = ObjEvent.dirty;
                        eventtoupdate.deleted = ObjEvent.deleted;
                        eventtoupdate.reoccurs = ObjEvent.reoccurs;
                        eventtoupdate.rcType = ObjEvent.rcType;
                        eventtoupdate.rcInterval = ObjEvent.rcInterval;
                        eventtoupdate.rcDayOfWeek = ObjEvent.rcDayOfWeek;
                        eventtoupdate.rcDayOfMonth = ObjEvent.rcDayOfMonth;
                        eventtoupdate.rcWeekOfMonth = ObjEvent.rcWeekOfMonth;
                        eventtoupdate.rcMonthOfYear = ObjEvent.rcMonthOfYear;
                        eventtoupdate.rcFirstDayOfWeek = ObjEvent.rcFirstDayOfWeek;
                        eventtoupdate.rcIsLeapMonth = ObjEvent.rcIsLeapMonth;
                        eventtoupdate.rcCalendarType = ObjEvent.rcCalendarType;
                        eventtoupdate.rcUntil = ObjEvent.rcUntil;
                        eventtoupdate.rcOccurences = ObjEvent.rcOccurences;
                        eventtoupdate.rcVisibleExceptions = ObjEvent.rcVisibleExceptions;
                        eventtoupdate.rcHiddenExceptions = ObjEvent.rcHiddenExceptions;
                        //update event info
                        ds.Db.UpdateAsync(eventtoupdate);
                    }
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        public async void DeleteRecurringEvent(Events ObjEvent)
        {
            //get connection string and store in variable
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                 //get event info base on event id
                var checkeventtoupdate = await ds.Db.Table<Events>().Where(c => c.serverId == ObjEvent.serverId).ToListAsync().ConfigureAwait(false);
                if (checkeventtoupdate != null && checkeventtoupdate.Count > 0)
                {
                    var eventtodelete = checkeventtoupdate.Where(c => DateTimeExtensions.FromOADate(c.startDayAndTime).Date == DateTimeExtensions.FromOADate(ObjEvent.startDayAndTime).Date &&
                        c.Original_id > 0).ToList();
                   //if data exists
                    if (eventtodelete != null && eventtodelete.Count > 0)
                    {
                        foreach (Events evnt in eventtodelete)
                        {
                            //delete particular record from database
                            await ds.Db.DeleteAsync(evnt).ConfigureAwait(false);
                        }
                    }
                    else
                    {
                        //get event info base on event id
                        var existingData = checkeventtoupdate.Where(c => c.serverId == ObjEvent.serverId && c.Original_id == 0).ToList();//.ConfigureAwait(false);
                        if (existingData != null && existingData.Count > 0)
                        {
                            var eventtoupdate = existingData.FirstOrDefault();
                            if (eventtoupdate != null)
                            {
                                eventtoupdate.rcHiddenExceptions = eventtoupdate.rcHiddenExceptions + ObjEvent.rcHiddenExceptions;
                                await ds.Db.UpdateAsync(eventtoupdate).ConfigureAwait(false);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        public async void UpdateEventInfoAsync(Events ObjEvent)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get connection string and store in variable
                //var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //get event info base on event id
                var existingData = await ds.Db.Table<Events>().Where(c => c.id == ObjEvent.id && c.folderID == ObjEvent.folderID).ToListAsync().ConfigureAwait(false);
                if (existingData != null && existingData.Count > 0)
                {
                    var eventtoupdate = existingData.FirstOrDefault();
                    //if event exists
                    if (eventtoupdate != null)
                    {
                        eventtoupdate.serverId = ObjEvent.serverId;
                        eventtoupdate.folderID = ObjEvent.folderID;
                        eventtoupdate.subject = ObjEvent.subject;
                        eventtoupdate.location = ObjEvent.location;
                        eventtoupdate.description = ObjEvent.description;
                        eventtoupdate.organizerName = ObjEvent.organizerName;
                        eventtoupdate.organizerEmail = ObjEvent.organizerEmail;
                        eventtoupdate.selfAttendeeStatus = ObjEvent.selfAttendeeStatus;
                        eventtoupdate.meetingStatus = ObjEvent.meetingStatus;
                        eventtoupdate.timeCreated = ObjEvent.timeCreated;
                        eventtoupdate.timeZone = ObjEvent.timeZone;
                        eventtoupdate.uid = ObjEvent.uid;
                        eventtoupdate.availability = ObjEvent.availability;
                        eventtoupdate.allDay = ObjEvent.allDay;
                        eventtoupdate.startDayAndTime = ObjEvent.startDayAndTime;
                        eventtoupdate.endDayAndTime = ObjEvent.endDayAndTime;
                        eventtoupdate.dtfinal = ObjEvent.dtfinal;
                        eventtoupdate.originalReminder = ObjEvent.originalReminder;
                        eventtoupdate.responseRequested = ObjEvent.responseRequested;
                        eventtoupdate.dirty = ObjEvent.dirty;
                        eventtoupdate.deleted = ObjEvent.deleted;
                        eventtoupdate.reoccurs = ObjEvent.reoccurs;
                        eventtoupdate.rcType = ObjEvent.rcType;
                        eventtoupdate.rcInterval = ObjEvent.rcInterval;
                        eventtoupdate.rcDayOfWeek = ObjEvent.rcDayOfWeek;
                        eventtoupdate.rcDayOfMonth = ObjEvent.rcDayOfMonth;
                        eventtoupdate.rcWeekOfMonth = ObjEvent.rcWeekOfMonth;
                        eventtoupdate.rcMonthOfYear = ObjEvent.rcMonthOfYear;
                        eventtoupdate.rcFirstDayOfWeek = ObjEvent.rcFirstDayOfWeek;
                        eventtoupdate.rcIsLeapMonth = ObjEvent.rcIsLeapMonth;
                        eventtoupdate.rcCalendarType = ObjEvent.rcCalendarType;
                        eventtoupdate.rcUntil = ObjEvent.rcUntil;
                        eventtoupdate.rcOccurences = ObjEvent.rcOccurences;
                        eventtoupdate.rcVisibleExceptions = ObjEvent.rcVisibleExceptions;
                        eventtoupdate.rcHiddenExceptions = ObjEvent.rcHiddenExceptions;
                        await ds.Db.UpdateAsync(eventtoupdate).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Delete event information

        /// <summary>
        /// Delete particular exception event information from database
        /// </summary>
        /// <param name="EventId">Event id</param>
        public async void DeleteExceptionEventInfoAsync(string serverID)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {

                //get event based on event id
                var eventtodelete = await ds.Db.Table<Events>().Where(c => c.serverId == serverID && c.Original_id > 0).ToListAsync().ConfigureAwait(false);
                //if data exists
                if (eventtodelete != null && eventtodelete.Count > 0)
                {
                    foreach (Events evnt in eventtodelete)
                    {
                        //delete particular record from database
                        await ds.Db.DeleteAsync(evnt).ConfigureAwait(false);
                    }
                }
                //update exception field
                //get event based on event id
                eventtodelete = await ds.Db.Table<Events>().Where(c => c.serverId == serverID && c.Original_id == 0).ToListAsync().ConfigureAwait(false);
                if (eventtodelete != null && eventtodelete.Count > 0)
                {
                    var getCurrentEvent = eventtodelete.FirstOrDefault();
                    getCurrentEvent.rcHiddenExceptions = string.Empty;
                    await ds.Db.UpdateAsync(getCurrentEvent);
                }

            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        /// <summary>
        /// Delete particular event information from database
        /// </summary>
        /// <param name="EventId">Event id</param>
        public async void DeleteEventInfoAsync(string serverID)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {

                //get event based on event id
                var eventtodelete = await ds.Db.Table<Events>().Where(c => c.serverId == serverID).ToListAsync().ConfigureAwait(false);
                //if data exists
                if (eventtodelete != null && eventtodelete.Count > 0)
                {
                    foreach (Events evnt in eventtodelete)
                    {
                        //delete particular record from database
                        await ds.Db.DeleteAsync(evnt).ConfigureAwait(false);
                    }
                }

            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task DeleteEventInfoAsync(int folderId, int eventId)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get event based on event id
                var eventToDelete = await ds.Db.Table<Events>().Where(c => c.id == eventId && c.folderID == folderId).ToListAsync().ConfigureAwait(false);

                //if data exists
                if (eventToDelete != null && eventToDelete.Count > 0)
                {
                    foreach (Events evnt in eventToDelete)
                    {
                        //delete particular record from database
                        await ds.Db.DeleteAsync(evnt);
                    }
                }
                //dispose dataabse object

            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion
    }
}