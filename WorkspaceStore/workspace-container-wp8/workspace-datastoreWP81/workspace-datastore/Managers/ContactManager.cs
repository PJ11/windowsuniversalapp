﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using workspace_datastore.models;
using workspace_datastore.Models;

/// <summary>
/// It incudes feature to add, edit ,get ,and delete contact information into\from database.
/// </summary>
namespace workspace_datastore.Managers
{
    //*********************************************************************************************************************
    // <copyright file="ContactManager.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>14-04-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           14-04-2014    1.0            First Version </summary>
    //*********************************************************************************************************************

    public class ContactManager
    {
        #region Get contact information
        /// <summary>
        /// method to get all contact info or get contact info based on account id(if id=0 then it will return all contact info)
        /// </summary>
        /// <param name="ContactId">Contact id</param>
        /// <returns></returns>
        public List<Contacts> GetContactInfo(int folderID, int ContactId)
        {
            //create local varibale to store contact info into list 
            List<Contacts> existContactInfo = null;

            //get connection string and store in variable
            DataSource ds = new DataSource();
            try
            {
                //check if contact id ==0 then retrun all contact info
                if (ContactId == 0)
                {
                    //get all contact info
                    existContactInfo = ds.Db.Table<Contacts>().ToList();
                }
                else
                {
                    //get contact info based on contact id
                    existContactInfo = ds.Db.Table<Contacts>().Where(c => c.id == ContactId && c.folder_id == folderID).ToList();
                }
                //dispose object
                //db.Dispose();
                //return result
                return existContactInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<List<Contacts>> GetContactInfoBasedonEmailIdAsync(string conactEmailId)
        {
            //create local varibale to store contact info into list 
            List<Contacts> existContactInfo = null;
            //get connection string and store in variable
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get contact info based on contact id
                existContactInfo = await ds.Db.Table<Contacts>().Where(c => c.email1Address == conactEmailId || c.email2Address == conactEmailId
                    || c.email3Address == conactEmailId).ToListAsync().ConfigureAwait(false);
                //return result
                return existContactInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<List<Contacts>> GetContactInfoAsync(int folderID, int ContactId)
        {
            //create local varibale to store contact info into list 
            List<Contacts> existContactInfo = null;

            //get connection string and store in variable
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //check if contact id ==0 then retrun all contact info
                if (ContactId == 0)
                {
                    //get all contact info
                    existContactInfo = await ds.Db.Table<Contacts>().ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    //get contact info based on contact id
                    existContactInfo = await ds.Db.Table<Contacts>().Where(c => c.id == ContactId && c.folder_id == folderID).ToListAsync().ConfigureAwait(false);
                }
                return existContactInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<List<Contacts>> GetRecentContactInfoAsync()
        {
            //create local varibale to store contact info into list 
            List<EmailSearch> emailrecentList = null;
            List<PhoneSearch> phonerecentList = null;

            List<Contacts> recentContacts = new List<Contacts>();

            //get connection string and store in variable
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                List<Contacts> contacts = null;

                await ds.Db.RunInTransactionAsync(async (SQLiteConnection connection) =>
                {   
                    SQLiteCommand selectMaxCmd = new SQLiteCommand(connection);
                    string fullSQLStatement = "SELECT _id, firstName, lastName, companyName, jobTitle from Contacts;";

                    //string qualifiers = ";";
                    selectMaxCmd.CommandText = fullSQLStatement;
                    contacts = selectMaxCmd.ExecuteQuery<Contacts>().ToList();
                });

                if (contacts != null)
                {
                    emailrecentList = await ds.Db.Table<EmailSearch>().OrderByDescending(s => s.recent_timestamp).Take(500).ToListAsync();
                    //currently we are considering 500 items from recents emails
                    if (emailrecentList.Count != 0)
                    {
                        //  folderListDB.Find(x => x.displayName == folderName);//
#if WP_80_SILVERLIGHT
                        emailrecentList.ForEach(x =>
                        {
                            if (x.linker_id > 0)
                                recentContacts.Add(contacts.Find(s => s.id == x.linker_id));

                        });
#endif
                        foreach (var emailSearch in emailrecentList)
                        {
                            if (emailSearch.linker_id > 0)
                                recentContacts.Add(contacts.Find(s => s.id == emailSearch.linker_id));
                        }
                    }

                    phonerecentList = await ds.Db.Table<PhoneSearch>().OrderByDescending(s => s.recent_timestamp).Take(500).ToListAsync();

                    //currently we are considering 500 items from recents phone
                    if (phonerecentList.Count != 0)
                    {
#if WP_80_SILVERLIGHT
                        phonerecentList.ForEach(x =>
                        {
                            if (x.linker_id > 0)
                                recentContacts.Add(contacts.Find(s => s.id == x.linker_id));

                        });
#endif
                        foreach (var phoneSearch in phonerecentList)
                        {
                            if (phoneSearch.linker_id > 0)
                                recentContacts.Add(contacts.Find(s => s.id == phoneSearch.linker_id));
                        }
                    }

                    return recentContacts;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region To contact the no of contacts in the list
        public int GetTotalContactCountForFolderId(int folderID)
        {
            //get connection string and store in variable
            //var db = new SQLiteAsyncConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
            DataSource ds = new DataSource();
            try
            {
                //get number of unread message based on folder id
                int contactCount = ds.Db.Table<Contacts>().Where(c => c.folder_id == folderID).Count();
                return contactCount;
            }
            catch (Exception ex)
            {
                LoggingWP8.WP8Logger.LogMessage("GetTotalContactCountForFolderIdAsync Error: {0}", ex.Message);
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<int> GetTotalContactCountForFolderIdAsync()
        {
            //get connection string and store in variable
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get number of contact
                // I think there is no use of passing folder id in this query, so i am commenting it(dhruv 03-12-2014)
                //As contacts folder data will be for contact info only, so folder_id will be fixed. Also in GetRecentContactInfoAsync() method, we are not passing folder id 
                int messageCount = 0;
                await ds.Db.RunInTransactionAsync(async (SQLiteConnection connection) =>
                {
                    SQLiteCommand selectMaxCmd = new SQLiteCommand(connection);
                    string fullSQLStatement = "SELECT COUNT(*) from Contacts;";

                    //string qualifiers = ";";
                    selectMaxCmd.CommandText = fullSQLStatement;
                    messageCount = selectMaxCmd.ExecuteScalar<int>();
                });
                return messageCount;

            }
            catch (Exception ex)
            {
                LoggingWP8.WP8Logger.LogMessage("GetTotalContactCountForFolderIdAsync Error: {0}", ex.Message);
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Add contact information
        /// <summary>
        /// Add contact information into database
        /// </summary>
        /// <param name="ObjContact">Contact object</param>
        public void AddContactInfo(SQLiteConnection db, Contacts ObjContact)
        {
            //Create datasource object and get connection string
            try
            {
                ////get connection string and store in variable
                //var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //set all the contact data here
                var contact = new Contacts()
                {
                    id = ObjContact.id,
                    serverId = ObjContact.serverId,
                    folder_id = ObjContact.folder_id,
                    firstName = ObjContact.firstName,
                    lastName = ObjContact.lastName,
                    companyName = ObjContact.companyName,
                    jobTitle = ObjContact.jobTitle,
                    email1Address = ObjContact.email1Address,
                    email2Address = ObjContact.email2Address,
                    email3Address = ObjContact.email3Address,
                    homePhoneNumber = ObjContact.homePhoneNumber,
                    home2PhoneNumber = ObjContact.home2PhoneNumber,
                    homeFaxNumber = ObjContact.homeFaxNumber,
                    businessPhoneNumber = ObjContact.businessPhoneNumber,
                    business2PhoneNumber = ObjContact.business2PhoneNumber,
                    businessFaxNumber = ObjContact.businessFaxNumber,
                    mobilePhoneNumber = ObjContact.mobilePhoneNumber,
                    assistantPhoneNumber = ObjContact.assistantPhoneNumber,
                    homeAddressStreet = ObjContact.homeAddressStreet,
                    homeAddressCity = ObjContact.homeAddressCity,
                    homeAddressState = ObjContact.homeAddressState,
                    homeAddressPostalCode = ObjContact.homeAddressPostalCode,
                    homeAddressCountry = ObjContact.homeAddressCountry,
                    businessAddressStreet = ObjContact.businessAddressStreet,
                    businessAddressCity = ObjContact.businessAddressCity,
                    businessAddressState = ObjContact.businessAddressState,
                    businessAddressPostalCode = ObjContact.businessAddressPostalCode,
                    businessAddressCountry = ObjContact.businessAddressCountry,
                    otherAddressStreet = ObjContact.otherAddressStreet,
                    otherAddressCity = ObjContact.otherAddressCity,
                    otherAddressState = ObjContact.otherAddressState,
                    otherAddressPostalCode = ObjContact.otherAddressPostalCode,
                    otherAddressCountry = ObjContact.otherAddressCountry,
                    fallbackDisplay = ObjContact.fallbackDisplay,
                    dirty = ObjContact.dirty,
                    deleted = ObjContact.deleted,
                    numberOfContactUsed = ObjContact.numberOfContactUsed
                };
                //insert contact information into database
                db.Insert(contact);
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }

        public async void AddContactInfoAsync(Contacts ObjContact)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get connection string and store in variable
                //set all the contact data here
                var contact = new Contacts()
                {
                    id = ObjContact.id,
                    serverId = ObjContact.serverId,
                    clientID = ObjContact.clientID,
                    folder_id = ObjContact.folder_id,
                    firstName = ObjContact.firstName,
                    lastName = ObjContact.lastName,
                    companyName = ObjContact.companyName,
                    jobTitle = ObjContact.jobTitle,
                    email1Address = ObjContact.email1Address,
                    email2Address = ObjContact.email2Address,
                    email3Address = ObjContact.email3Address,
                    homePhoneNumber = ObjContact.homePhoneNumber,
                    home2PhoneNumber = ObjContact.home2PhoneNumber,
                    homeFaxNumber = ObjContact.homeFaxNumber,
                    businessPhoneNumber = ObjContact.businessPhoneNumber,
                    business2PhoneNumber = ObjContact.business2PhoneNumber,
                    businessFaxNumber = ObjContact.businessFaxNumber,
                    mobilePhoneNumber = ObjContact.mobilePhoneNumber,
                    assistantPhoneNumber = ObjContact.assistantPhoneNumber,
                    homeAddressStreet = ObjContact.homeAddressStreet,
                    homeAddressCity = ObjContact.homeAddressCity,
                    homeAddressState = ObjContact.homeAddressState,
                    homeAddressPostalCode = ObjContact.homeAddressPostalCode,
                    homeAddressCountry = ObjContact.homeAddressCountry,
                    businessAddressStreet = ObjContact.businessAddressStreet,
                    businessAddressCity = ObjContact.businessAddressCity,
                    businessAddressState = ObjContact.businessAddressState,
                    businessAddressPostalCode = ObjContact.businessAddressPostalCode,
                    businessAddressCountry = ObjContact.businessAddressCountry,
                    otherAddressStreet = ObjContact.otherAddressStreet,
                    otherAddressCity = ObjContact.otherAddressCity,
                    otherAddressState = ObjContact.otherAddressState,
                    otherAddressPostalCode = ObjContact.otherAddressPostalCode,
                    otherAddressCountry = ObjContact.otherAddressCountry,
                    fallbackDisplay = ObjContact.fallbackDisplay,
                    dirty = ObjContact.dirty,
                    deleted = ObjContact.deleted,
                    numberOfContactUsed = ObjContact.numberOfContactUsed
                };
                //insert contact information into database
                int result = await ds.Db.InsertAsync(contact).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public void AddAllContactInfo(SQLiteConnection db, IList<Contacts> contactsList)
        {
            db.InsertAll(contactsList);
        }

        public async void AddAllContactInfoAsync(SQLiteAsyncConnection db, IList<Contacts> contactsList)
        {
            await db.InsertAllAsync(contactsList);
        }
        #endregion

        #region Update contact information
        /// <summary>
        /// Update partucluar contact information into database
        /// </summary>
        /// <param name="ObjContact">contact object</param>
        public void UpdateContactInfo(SQLiteConnection db, Contacts ObjContact)
        {
            try
            {
                //get contact info based on contact id
                var contact = db.Table<Contacts>().Where(c => c.id == ObjContact.id && c.folder_id == ObjContact.folder_id).Single();
                //if contact exists
                if (contact != null)
                {
                    contact.serverId = ObjContact.serverId;
                    contact.folder_id = ObjContact.folder_id;
                    contact.firstName = ObjContact.firstName;
                    contact.lastName = ObjContact.lastName;
                    contact.companyName = ObjContact.companyName;
                    contact.jobTitle = ObjContact.jobTitle;
                    contact.email1Address = ObjContact.email1Address;
                    contact.email2Address = ObjContact.email2Address;
                    contact.email3Address = ObjContact.email3Address;
                    contact.homePhoneNumber = ObjContact.homePhoneNumber;
                    contact.home2PhoneNumber = ObjContact.home2PhoneNumber;
                    contact.homeFaxNumber = ObjContact.homeFaxNumber;
                    contact.businessPhoneNumber = ObjContact.businessPhoneNumber;
                    contact.business2PhoneNumber = ObjContact.business2PhoneNumber;
                    contact.businessFaxNumber = ObjContact.businessFaxNumber;
                    contact.mobilePhoneNumber = ObjContact.mobilePhoneNumber;
                    contact.assistantPhoneNumber = ObjContact.assistantPhoneNumber;
                    contact.homeAddressStreet = ObjContact.homeAddressStreet;
                    contact.homeAddressCity = ObjContact.homeAddressCity;
                    contact.homeAddressState = ObjContact.homeAddressState;
                    contact.homeAddressPostalCode = ObjContact.homeAddressPostalCode;
                    contact.homeAddressCountry = ObjContact.homeAddressCountry;
                    contact.businessAddressStreet = ObjContact.businessAddressStreet;
                    contact.businessAddressCity = ObjContact.businessAddressCity;
                    contact.businessAddressState = ObjContact.businessAddressState;
                    contact.businessAddressPostalCode = ObjContact.businessAddressPostalCode;
                    contact.businessAddressCountry = ObjContact.businessAddressCountry;
                    contact.otherAddressStreet = ObjContact.otherAddressStreet;
                    contact.otherAddressCity = ObjContact.otherAddressCity;
                    contact.otherAddressState = ObjContact.otherAddressState;
                    contact.otherAddressPostalCode = ObjContact.otherAddressPostalCode;
                    contact.otherAddressCountry = ObjContact.otherAddressCountry;
                    contact.fallbackDisplay = ObjContact.fallbackDisplay;
                    contact.dirty = ObjContact.dirty;
                    contact.deleted = ObjContact.deleted;
                    //update contact info
                    //conn.UpdateAsync(contact);
                    db.Update(contact);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async void UpdateContactInfoAsync(Contacts ObjContact)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get contact info based on contact id
                var result = await ds.Db.Table<Contacts>().Where(c => c.id == ObjContact.id && c.folder_id == ObjContact.folder_id).ToListAsync().ConfigureAwait(false);
                //if contact exists
                if (result != null && result.Count > 0)
                {
                    var contact = result.FirstOrDefault();
                    contact.serverId = ObjContact.serverId;
                    contact.folder_id = ObjContact.folder_id;
                    contact.firstName = ObjContact.firstName;
                    contact.lastName = ObjContact.lastName;
                    contact.companyName = ObjContact.companyName;
                    contact.jobTitle = ObjContact.jobTitle;
                    contact.email1Address = ObjContact.email1Address;
                    contact.email2Address = ObjContact.email2Address;
                    contact.email3Address = ObjContact.email3Address;
                    contact.homePhoneNumber = ObjContact.homePhoneNumber;
                    contact.home2PhoneNumber = ObjContact.home2PhoneNumber;
                    contact.homeFaxNumber = ObjContact.homeFaxNumber;
                    contact.businessPhoneNumber = ObjContact.businessPhoneNumber;
                    contact.business2PhoneNumber = ObjContact.business2PhoneNumber;
                    contact.businessFaxNumber = ObjContact.businessFaxNumber;
                    contact.mobilePhoneNumber = ObjContact.mobilePhoneNumber;
                    contact.assistantPhoneNumber = ObjContact.assistantPhoneNumber;
                    contact.homeAddressStreet = ObjContact.homeAddressStreet;
                    contact.homeAddressCity = ObjContact.homeAddressCity;
                    contact.homeAddressState = ObjContact.homeAddressState;
                    contact.homeAddressPostalCode = ObjContact.homeAddressPostalCode;
                    contact.homeAddressCountry = ObjContact.homeAddressCountry;
                    contact.businessAddressStreet = ObjContact.businessAddressStreet;
                    contact.businessAddressCity = ObjContact.businessAddressCity;
                    contact.businessAddressState = ObjContact.businessAddressState;
                    contact.businessAddressPostalCode = ObjContact.businessAddressPostalCode;
                    contact.businessAddressCountry = ObjContact.businessAddressCountry;
                    contact.otherAddressStreet = ObjContact.otherAddressStreet;
                    contact.otherAddressCity = ObjContact.otherAddressCity;
                    contact.otherAddressState = ObjContact.otherAddressState;
                    contact.otherAddressPostalCode = ObjContact.otherAddressPostalCode;
                    contact.otherAddressCountry = ObjContact.otherAddressCountry;
                    contact.fallbackDisplay = ObjContact.fallbackDisplay;
                    contact.dirty = ObjContact.dirty;
                    contact.deleted = ObjContact.deleted;
                    contact.numberOfContactUsed = ObjContact.numberOfContactUsed;
                    //update contact info
                    await ds.Db.UpdateAsync(contact);

                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Delete particular contact information
        /// <summary>
        /// Delete particular contact information from database
        /// </summary>
        /// <param name="ContactId">contact id</param>
        public void DeleteContactInfo(SQLiteConnection db, int folderId, int ContactId)
        {

            try
            {
                //get contact inforamtion based on contact id
                var contact = db.Table<Contacts>().Where(c => c.id == ContactId && c.folder_id == folderId).Single();
                //if data exists
                if (contact != null)
                {
                    //delete particular record from database
                    db.Delete(contact);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteContactInfoAsync(int folderId, int ContactId)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get contact inforamtion based on contact id 
                var contact = await ds.Db.Table<Contacts>().Where(c => c.id == ContactId && c.folder_id == folderId).ToListAsync().ConfigureAwait(false);
                //if data exists
                if (contact != null && contact.Count > 0)
                {
                    //delete particular record from database
                    await ds.Db.DeleteAsync(contact.FirstOrDefault());
                }

            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        /// <summary>
        /// This function will check for a the contact on basis of emailID1 during send mail
        /// </summary>
        public async Task<Contacts> IsExistingContactFromMail(string mailID)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get contact inforamtion based on contact id 
                //var contact = await connection.Table<Contacts>().Where(c => c.email1Address== mailID || c.email2Address == mailID || c.email3Address ==mailID).FirstAsync().ConfigureAwait(false);
                var var = await ds.Db.Table<Contacts>().Where(c => c.email1Address.ToLower() == mailID.ToLower() || c.email2Address.ToLower() == mailID.ToLower() || c.email3Address.ToLower() == mailID.ToLower()).ToListAsync().ConfigureAwait(false);
                Contacts contact = null;
                if (var.Count > 0)
                    contact = var.ElementAt(0);

                return contact;
            }
            catch (Exception ex)
            {
                //throw exception
                return null;
            }
            finally
            {
                ds.Dispose();
            }

        }

        public async Task<Contacts> IsExistingContactFromName(string displayName, string email)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get contact inforamtion based on contact id 
                var var = await ds.Db.Table<Contacts>().Where(c => c.firstName.ToLower() == displayName.ToLower() &&
                                                                    (c.email1Address.ToLower() == email || c.email2Address.ToLower() == email || c.email3Address.ToLower() == email)).ToListAsync().ConfigureAwait(false);
                Contacts contact = null;
                if (var.Count > 0)
                    contact = var.ElementAt(0);

                return contact;
            }
            catch (Exception ex)
            {
                //throw exception
                return null;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion


        public async Task<List<Contacts>> GetDirtyContactsListAsync()
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get contact inforamtion based on contact id 
                var contactList = await ds.Db.Table<Contacts>().Where(c => c.dirty == 1).ToListAsync().ConfigureAwait(false);
                return contactList;

            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
    }
}
