﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using workspace_datastore.models;
using System.Xml.Linq;
using System.ComponentModel;
using workspace_datastore.Helpers;

/// <summary>
// it incudes feature to add, edit ,get ,and delete message data into\from database.
/// </summary>
namespace workspace_datastore.Managers
{

    //*********************************************************************************************************************
    // <copyright file="MessageManager.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>11-04-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           11-04-2014    1.0            First Version </summary>
    //*********************************************************************************************************************

    public class MessageManager: INotifyPropertyChanged
    {
        public int index { get; set; }
        public int messageId { get; set; }

        public string serverID { get; set; }
        public Guid attachmentId { get; set; }
        public int attach_MessageId { get; set; }
        public string toRecipientText { get; set; }
        public string ccRecipientText { get; set; }
        public string bccRecipientText { get; set; }
        public Boolean isRecipientAsBcc {get;set;}
        public int folderId { get; set; }
        public string attachment_serverId { get; set; }
        public string fromEmail { get; set; }
        public string displayFromEmail { get; set; }
        public double receivedDate { get; set; }
        public string subject { get; set; }
        public int importance { get; set; }
        public int flagStatus { get; set; }
        public string previewText { get; set; }
        public string bodyText { get; set; }
        //public int read { get; set; }
        int _read;
        public int read
        {
            get
            {
                return _read;
            }
            set
            {
                if (value != _read)
                {
                    _read = value;
                    RaisePropertyChanged("read");
                    RaisePropertyChanged("ReadAndImportance");
                }
            }
        }
        //public int flag { get; set; }
        int _flag;
        public int flag
        {
            get
            {
                return _flag;
            }
            set
            {
                if (value != _flag)
                {
                    _flag = value;
                    RaisePropertyChanged("flag");
                }
            }
        }

        int _lastVerbExecuted;

        public int lastVerbExecuted
        {
            get
            {
                return _lastVerbExecuted;
            }
            set
            {
                if (value != _lastVerbExecuted)
                {
                    _lastVerbExecuted = value;
                    RaisePropertyChanged("lastVerbExecuted");
                }
            }
        }

        public double flagStartDate { get; set; }
        public double flagDueDate { get; set; }
        public double flagUTCStartDate { get; set; }
        public double flagUTCDueDate { get; set; }
        public int flagReminderSet { get; set; }
        public bool hasAttachment { get; set; }
        public string messageClass { get; set; }
        
        /// <summary>
        /// Assumes int is signed 32 bits
        /// </summary>
        public int ReadAndImportance
        {
            get
            {
                return read << 16 | importance;
            }
        }

        // Needed for Calendar event messages when doing a "continue search on server"
        public int event_id { get; set; }
        public int allDay { get; set; }
        public string location { get; set; }
        public string timeZone { get; set; }
        public double startDayAndTime { get; set; }
        public double endDayAndTime { get; set; }
        public string uid { get; set; }
        public int availability { get; set; }
        public int originalReminder { get; set; }
        public int responseRequested { get; set; }
        public int meetingStatus { get; set; }
        public string organizerName { get; set; }
        public string organizerEmail { get; set; }


        #region Get all or particular message
        /// <summary>
        /// method to get all message info or get message info based on message id(if id=0 then it will return all messages info)
        /// </summary>
        /// <param name="MessageID">Message id</param>
        /// <returns></returns>
        public List<Message> GetMessageInfo(int folderID, int MessageID)
        {
            //create local varibale to store message info into list 
            List<Message> existMsgInfo = null;
            DataSource ds = new DataSource();
            try
            {
                //check if message id ==0 then retrun all message info
                if (MessageID == 0)
                {
                    //get all message info
                    existMsgInfo = ds.Db.Table<Message>().ToList();
                }
                else
                {
                    //get message info based on messsage id
                    existMsgInfo = ds.Db.Table<Message>().Where(c => c.id == MessageID && c.folder_id == folderID).ToList();
                }

                //return result
                return existMsgInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<List<Message>> GetMessageInfoAsync1(int folderID, int MessageID)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            List<Message> msg = null;
            try
            {
                await ds.Db.RunInTransactionAsync(async (SQLiteConnection connection) =>
                {
                    SQLiteCommand selectMaxCmd = new SQLiteCommand(connection);
                    selectMaxCmd.CommandText = "SELECT * FROM Message WHERE folder_id = " + folderID.ToString() + " AND id = " + MessageID.ToString() + ";";
                    msg = selectMaxCmd.ExecuteQuery<Message>();
                });
                return msg;
            }
            catch (Exception)
            { }
            finally
            {
                ds.Dispose();
            }
            return msg;
        }

        public async Task<List<Message>> GetMessageInfoAsync(int folderID, int MessageID)
        {
            //create local varibale to store message info into list 
            List<Message> existMsgInfo = null;
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //check if message id ==0 then retrun all message info
                if (MessageID == 0)
                {
                    //get all message info
                    existMsgInfo = await ds.Db.Table<Message>().Where(c=> c.folder_id == folderId).ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    //get message info based on messsage id
                    existMsgInfo = await ds.Db.Table<Message>().Where(c => c.id == MessageID && c.folder_id == folderID).ToListAsync().ConfigureAwait(false);
                }
                return existMsgInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<List<Message>> GetDraftMessageInfoAsync(int folderID, int MessageID)
        {
            //create local varibale to store message info into list 
            List<Message> existMsgInfo = null;
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //check if message id ==0 then retrun all message info
                if (MessageID == 0)
                {
                    //get all message info
                    existMsgInfo = await ds.Db.Table<Message>().ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    //get message info based on messsage id
                    existMsgInfo = await ds.Db.Table<Message>().Where(c => c._id == MessageID && c.folder_id == folderID).ToListAsync().ConfigureAwait(false);
                }
                return existMsgInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public List<Message> GetMessageInfo(string serverId)
        {
            //create local varibale to store message info into list 
            List<Message> existMsgInfo = null;
            DataSource ds = new DataSource();
            try
            {
                existMsgInfo = ds.Db.Table<Message>().Where(c => c.serverIdentifier == serverId).ToList();
                return existMsgInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<List<Message>> GetMessageInfoAsync(string serverId)
        {
            //create local varibale to store message info into list 
            List<Message> existMsgInfo = null;
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //check if message id ==0 then retrun all message info
                //if (MessageID == 0)
                //{
                //    existMsgInfo = await con.Table<Message>().ToListAsync().ConfigureAwait(false);
                //}
                //else
                {
                    //get message info based on messsage id
                    existMsgInfo = await ds.Db.Table<Message>().Where(c => c.serverIdentifier == serverId).ToListAsync().ConfigureAwait(false);
                }
                return existMsgInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Get number of unread messages for particular folder
        /// <summary>
        /// method to get all number of unread message based on folder id
        /// </summary>
        /// <param name="folderID">folder id</param>
        /// <returns></returns>
        public int GetNoofUnreadMsgBasedOnFolderID(int folderID)
        {
            //get connection string and store in variable
            DataSource ds = new DataSource();
            try
            {
                //var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //get number of unread message based on folder id
                int noOfUnreadMsg = ds.Db.Table<Message>().Where(c => c.folder_id == folderID && c.read == 0).Count();
                return noOfUnreadMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<List<Message>> GetUnreadMsgBasedOnFolderIDAsync(int folderID)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get number of unread message based on folder id
                var messageCount = await ds.Db.Table<Message>().Where(c => c.folder_id == folderID && c.read == 0).ToListAsync().ConfigureAwait(false);
                return messageCount;
            }
            catch (Exception ex)
            {
                LoggingWP8.WP8Logger.LogMessage("GetTotalMailCountForFolderIdAsync Error: {0}", ex.Message);
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<int> GetNoofUnreadMsgBasedOnFolderIDAsync(int folderID)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                int messageCount = 0;
                await ds.Db.RunInTransactionAsync(async (SQLiteConnection connection) =>
                {
                    SQLiteCommand selectMaxCmd = new SQLiteCommand(connection);
                    string fullSQLStatement = "SELECT COUNT(*) from Message WHERE folder_id = " + folderID.ToString() + " AND read = 0;" ;
                    ////if (sqlStatement.Length > 0)
                    //{
                    //    fullSQLStatement = fullSQLStatement + " AND " + read = "0";
                    //}
                    //string qualifiers = ";";
                    selectMaxCmd.CommandText = fullSQLStatement ;
                    messageCount = selectMaxCmd.ExecuteScalar<int>();
                });
                return messageCount;
            }
            catch (Exception ex)
            {
                LoggingWP8.WP8Logger.LogMessage("GetNoofUnreadMsgBasedOnFolderIDAsync Error: {0}", ex.Message);
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Get number of total emails for a particular folder
        public int GetTotalMailCountForFolderId(int folderID)
        {
            //Create datasource object and get connection string
            DataSource ds = new DataSource();
            try
            {
                int noOfUnreadMsg = ds.Db.Table<Message>().Where(c => c.folder_id == folderID).Count();
                return noOfUnreadMsg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        public async Task<int> GetTotalMailCountForFlaggedFolderAsync(int folderID)
        {
            //get connection string and store in variable
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            FolderManager fldrManager = new FolderManager();
            try
            {
                int messageCount;
                if (folderID == 0)
                {
                    messageCount = await ds.Db.Table<Message>().Where(c => c.flag == 1).CountAsync().ConfigureAwait(false);
                }
                else
                {
                    messageCount = await ds.Db.Table<Message>().Where(c => c.folder_id == folderID && c.flag == 1).CountAsync().ConfigureAwait(false);
                }
                return messageCount;
            }
            catch (Exception ex)
            {
                LoggingWP8.WP8Logger.LogMessage("GetTotalMailCountForFolderIdAsync Error: {0}", ex.Message);
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<int> GetTotalMailCountForFolderIdAsync(int folderID)
        {
            //get connection string and store in variable
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            FolderManager fldrManager = new FolderManager();
            try
            {
                int messageCount;

                    messageCount = await ds.Db.Table<Message>().Where(c => c.folder_id == folderID).CountAsync().ConfigureAwait(false);
                
                return messageCount;
            }
            catch (Exception ex)
            {
                LoggingWP8.WP8Logger.LogMessage("GetTotalMailCountForFolderIdAsync Error: {0}", ex.Message);
                throw ex;
            }
            finally 
            { 
                ds.Dispose();
            }
        }

        public async Task<int> GetTotalMailCountForFolderIdAndFilterAsync(int folderID, string sqlStatement)
        {
            //get connection string and store in variable
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                int messageCount = 0;
                await ds.Db.RunInTransactionAsync(async (SQLiteConnection connection) =>
                {
                    SQLiteCommand selectMaxCmd = new SQLiteCommand(connection);
                    string fullSQLStatement = "SELECT COUNT(*) from Message WHERE folder_id = " + folderID.ToString();
                    if (sqlStatement.Length > 0)
                    {
                        fullSQLStatement += " AND " + sqlStatement;
                    }
                    string qualifiers = ";";
                    selectMaxCmd.CommandText = fullSQLStatement + qualifiers;
                    messageCount = selectMaxCmd.ExecuteScalar<int>();
                });
                return messageCount;
            }
            catch (Exception ex)
            {
                LoggingWP8.WP8Logger.LogMessage("GetTotalMailCountForFolderIdAsync Error: {0}", ex.Message);
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }


        public async Task<int> GetTotalMailCountForEachFolderIdAsync(int folderID, string folderName)
        {
            //get connection string and store in variable
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            FolderManager fldrManager = new FolderManager();
            try
            {
                int messageCount;
				string serverId = System.Convert.ToString(folderID);
              
                //get number of unread message based on folder id
              
                if (folderName.ToLower() != "drafts")
					messageCount = await ds.Db.Table<Message>().Where(c => c.serverIdentifier == serverId && c.read == 0).CountAsync().ConfigureAwait(false);
                else
					messageCount = await ds.Db.Table<Message>().Where(c => c.serverIdentifier == serverId).CountAsync().ConfigureAwait(false);

                return messageCount;
            }
            catch (Exception ex)
            {
                LoggingWP8.WP8Logger.LogMessage("GetTotalMailCountForFolderIdAsync Error: {0}", ex.Message);
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Get all or particular message with attachment
        /// <summary>
        /// method to get all message info with attachment or get message info based on message id(if id=0 then it will return all messages info)
        /// </summary>
        /// <param name="MessageID">Message id</param>
        /// <returns></returns>
        public List<MessageManager> GetMessagewithAttachInfo(int folderId, int MessageID)
        {
            //Create datasource object and get connection string
            DataSource ds = new DataSource();
            try
            {
                //check if message id ==0 then retrun all message info
                if (MessageID == 0)
                {
                    var msgwithattachment = from msg in ds.Db.Table<Message>().Where(msg => msg.folder_id == folderId).ToList()
                                            select new MessageManager
                                            {
                                                index = msg._id,
                                                messageId = msg.id,
                                                displayFromEmail = msg.displayFromEmail,
                                                fromEmail = msg.fromEmail,
                                                toRecipientText = msg.toRecipientText,
                                                ccRecipientText = msg.ccRecipientText,
                                                bccRecipientText = msg.bccRecipientText,
                                                receivedDate = msg.receivedDate,
                                                flagStatus = msg.flagStatus,
                                                event_id = msg.event_id,
                                                subject = msg.subject,
                                                importance = msg.importance,
                                                previewText = msg.previewText,
                                                bodyText = msg.bodyText,
                                                folderId = msg.folder_id,
                                                attachment_serverId = msg.serverIdentifier,
                                                read = msg.read,
                                                flag = msg.flag,
                                                serverID = msg.serverIdentifier,
                                                hasAttachment=msg.hasAttachment,
                                                attach_MessageId = msg.id,
                                                messageClass=msg.messageClass,
                                                lastVerbExecuted = msg.lastVerbExecuted
                                            };
                return msgwithattachment.ToList();

                }
                else
                {
                    // Get a particular message
                    var msgwithattachment = from msg in ds.Db.Table<Message>().Where(msg => msg.folder_id == folderId && msg.id == MessageID).ToList()
                                            select new MessageManager
                                            {
                                                index = msg._id,
                                                messageId = msg.id,
                                                fromEmail = msg.fromEmail,
                                                toRecipientText = msg.toRecipientText,
                                                ccRecipientText = msg.ccRecipientText,
                                                bccRecipientText = msg.bccRecipientText,
                                                receivedDate = msg.receivedDate,
                                                flagStatus = msg.flagStatus,
                                                event_id = msg.event_id,
                                                subject = msg.subject,
                                                importance = msg.importance,
                                                previewText = msg.previewText,
                                                bodyText = msg.bodyText,
                                                folderId = msg.folder_id,
                                                attachment_serverId = msg.serverIdentifier,
                                                read = msg.read,
                                                flag = msg.flag,
                                                serverID = msg.serverIdentifier,
                                                hasAttachment=msg.hasAttachment,
                                                attach_MessageId = msg.id,
                                                messageClass=msg.messageClass,
                                                lastVerbExecuted = msg.lastVerbExecuted
                                            };                        
                        return msgwithattachment.ToList();

                }
                return null;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ////dispose object
                if (null != ds.Db)
                    ds.Dispose();
            }
        }

        public async Task<List<MessageManager>> GetMessagewithAttachInfoAsync(int folderId, int MessageID)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //check if message id ==0 then retrun all message info
                if (MessageID == 0)
                {

                    var msgwithattachment = from msg in await ds.Db.Table<Message>().Where(msg => msg.folder_id == folderId).ToListAsync().ConfigureAwait(false)
                                            select new MessageManager
                                            {
                                                index = msg._id,
                                                messageId = msg.id,
                                                displayFromEmail = msg.displayFromEmail,
                                                fromEmail = msg.fromEmail,
                                                toRecipientText = msg.toRecipientText,
                                                ccRecipientText = msg.ccRecipientText,
                                                bccRecipientText = msg.bccRecipientText,
                                                receivedDate = msg.receivedDate,
                                                flagStatus = msg.flagStatus,
                                                event_id = msg.event_id,
                                                subject = msg.subject,
                                                importance = msg.importance,
                                                previewText = msg.previewText,
                                                bodyText = msg.bodyText,
                                                folderId = msg.folder_id,
                                                attachment_serverId = msg.serverIdentifier,
                                                read = msg.read,
                                                flag = msg.flag,
                                                serverID = msg.serverIdentifier,
                                               hasAttachment=msg.hasAttachment,
                                               attach_MessageId = msg.id,
                                               messageClass=msg.messageClass,
                                                lastVerbExecuted = msg.lastVerbExecuted
                                            };

                    return msgwithattachment.ToList();

                }
                else
                {
                    var msgwithattachment = from msg in await ds.Db.Table<Message>().Where(msg => msg.folder_id == folderId && msg.id == MessageID).ToListAsync().ConfigureAwait(false)
                                            select new MessageManager
                                            {
                                                index = msg._id,
                                                messageId = msg.id,
                                                fromEmail = msg.fromEmail,
                                                toRecipientText = msg.toRecipientText,
                                                ccRecipientText = msg.ccRecipientText,
                                                bccRecipientText = msg.bccRecipientText,
                                                receivedDate = msg.receivedDate,
                                                flagStatus = msg.flagStatus,
                                                event_id = msg.event_id,
                                                subject = msg.subject,
                                                importance = msg.importance,
                                                previewText = msg.previewText,
                                                bodyText = msg.bodyText,
                                                folderId = msg.folder_id,
                                                attachment_serverId = msg.serverIdentifier,
                                                read = msg.read,
                                                flag = msg.flag,
                                                serverID = msg.serverIdentifier,
                                                hasAttachment=msg.hasAttachment,
                                                attach_MessageId = msg.id,
                                                messageClass = msg.messageClass,
                                                lastVerbExecuted = msg.lastVerbExecuted
                                            };                        
                        return msgwithattachment.ToList();

                }
                return null;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<List<MessageManager>> GetMessagewithAttachInfoAsyncForFlagged(int folderId, int MessageID)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //check if message id ==0 then retrun all message info
                if (MessageID == 0)
                {
                    var msgwithattachment = from msg in await ds.Db.Table<Message>().Where(msg =>msg.folder_id==folderId && msg.flag == 1).ToListAsync().ConfigureAwait(false)
                                            select new MessageManager
                                            {
                                                index = msg._id,
                                                messageId = msg.id,
                                                displayFromEmail = msg.displayFromEmail,
                                                fromEmail = msg.fromEmail,
                                                toRecipientText = msg.toRecipientText,
                                                ccRecipientText = msg.ccRecipientText,
                                                bccRecipientText = msg.bccRecipientText,
                                                receivedDate = msg.receivedDate,
                                                flagStatus = msg.flagStatus,
                                                event_id = msg.event_id,
                                                subject = msg.subject,
                                                importance = msg.importance,
                                                previewText = msg.previewText,
                                                //bodyText = msg.bodyText,
                                                folderId = msg.folder_id,
                                                attachment_serverId = msg.serverIdentifier,
                                                read = msg.read,
                                                flag = msg.flag,
                                                serverID = msg.serverIdentifier,
                                                hasAttachment=msg.hasAttachment,
                                                attach_MessageId = msg.id,
                                                messageClass=msg.messageClass,
                                                lastVerbExecuted = msg.lastVerbExecuted
                                            };
                    return msgwithattachment.ToList();
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

      
        class MessageEqualityComparer : IEqualityComparer<Message>
        {
            public bool Equals(Message x, Message y)
            {
                return x.id == y.id;
            }

            public int GetHashCode(Message hc)
            {
                return hc.id;
            }
        }
        
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public async Task<int> GetIDOfNewestMessageForFolder(int folderId)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                int val = 0;
                await ds.Db.RunInTransactionAsync(async (SQLiteConnection connection) =>
                {
                    SQLiteCommand selectMaxCmd = new SQLiteCommand(connection);
                    selectMaxCmd.CommandText = "SELECT Max(id) FROM Message WHERE folder_id = " + folderId.ToString() + ";";
                    val = selectMaxCmd.ExecuteScalar<int>();
                });
                return val;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                ds.Dispose();
            }
        }

        /// <summary>
        /// Returns the next newest message id or 0 if it's the newest
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="currentMessageId"></param>
        /// <returns></returns>
        public async Task<int> GetIDOfNextNewestMessageForFolder(int folderId, int currentMessageId)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                int val = 0;
                await ds.Db.RunInTransactionAsync(async (SQLiteConnection connection) =>
                {
                    SQLiteCommand selectMaxCmd = new SQLiteCommand(connection);
                    selectMaxCmd.CommandText = "SELECT DISTINCT id FROM Message WHERE folder_id = " + folderId.ToString() + " AND id > (SELECT id FROM Message WHERE id = " + currentMessageId.ToString() + " AND folder_id = " + folderId.ToString() + " ORDER BY id DESC) LIMIT 1 ";
                    val = selectMaxCmd.ExecuteScalar<int>();
                });
                return val;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                ds.Dispose();
            }
        }

        /// <summary>
        /// Returns the next newest message id or 0 if it's the newest
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="currentMessageId"></param>
        /// <returns></returns>
        public async Task<int> GetIDOfNextOldestMessageForFolder(int folderId, int currentMessageId)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                int val = 0;
                await ds.Db.RunInTransactionAsync(async (SQLiteConnection connection) =>
                {
                    SQLiteCommand selectMaxCmd = new SQLiteCommand(connection);
                    selectMaxCmd.CommandText = "SELECT DISTINCT id FROM Message WHERE folder_id = " + folderId.ToString() + " AND id < (SELECT id FROM Message WHERE id = " + currentMessageId.ToString() + " AND folder_id = " + folderId.ToString() + ") ORDER BY id DESC LIMIT 1 ";
                    val = selectMaxCmd.ExecuteScalar<int>();
                });
                return val;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                ds.Dispose();
            }
        }


        public async Task<int> GetIDOfOldestMessageForFolder(int folderId)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                int val = 0;
                await ds.Db.RunInTransactionAsync(async (SQLiteConnection connection) =>
                {
                    SQLiteCommand selectMaxCmd = new SQLiteCommand(connection);
                    selectMaxCmd.CommandText = "SELECT Min(id) FROM Message WHERE folder_id = " + folderId.ToString() + ";";
                    val = selectMaxCmd.ExecuteScalar<int>();
                });
                return val;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                ds.Dispose();
            }
        }
       
        //This we are using to draw list screen. We dont need Body, CC, BCC info
        public async Task<List<MessageManager>> GetDescendingMessageBlockWithAttachForListInfoAsync(string folderName, int folderId, string sqlStatement, int blockSize, int startingIndex = 0)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                if (folderName.ToLower() == "drafts")
                {
                    var draftmessages = from draft in await ds.Db.Table<Draft_Parts>().Where(c => c.partType == 1).Skip(startingIndex).Take(blockSize).ToListAsync().ConfigureAwait(false)
                                            join msg in await ds.Db.Table<Message>().ToListAsync().ConfigureAwait(false)
                                            on draft.message_id equals msg._id                                                                                 
                                            select new MessageManager()
                                            {
                                                messageId = msg._id,
                                                displayFromEmail = msg.displayFromEmail,
                                                fromEmail = msg.fromEmail,
                                                receivedDate = msg.receivedDate,
                                                flagStatus = msg.flagStatus,
                                                event_id = msg.event_id,
                                                subject = msg.subject,
                                                importance = msg.importance,
                                                previewText = msg.previewText,
                                                folderId = msg.folder_id,
                                                attachment_serverId = msg.serverIdentifier,
                                                read = msg.read,
                                                flag = msg.flag,
                                                serverID = msg.serverIdentifier,
                                                hasAttachment=msg.hasAttachment,
                                                attach_MessageId = msg.id,
                                                flagStartDate = msg.flagStartDate,
                                                flagDueDate = msg.flagDueDate,
                                                flagUTCStartDate = msg.flagUTCStartDate,
                                                flagUTCDueDate = msg.flagUTCDueDate,
                                                flagReminderSet = msg.flagReminderSet,
                                                lastVerbExecuted = msg.lastVerbExecuted
                                            };
                    return draftmessages.OrderByDescending(msg => msg.receivedDate).ToList();
                }
                else
                {
                    List<Message> val = null;
                    await ds.Db.RunInTransactionAsync(async (SQLiteConnection connection) =>
                    {
                        SQLiteCommand selectMaxCmd = new SQLiteCommand(connection);
                        string fullSQLStatement = "SELECT id, displayFromEmail, fromEmail, receivedDate, flagStatus, event_id, subject, importance, previewText, folder_id, serverIdentifier, read, flag," + 
                        "serverIdentifier, hasAttachment, flagStartDate, flagDueDate, flagUTCStartDate, flagUTCDueDate, flagReminderSet, lastVerbExecuted from Message WHERE folder_id = " + folderId.ToString();
                        if (sqlStatement.Length > 0)
                        {
                            fullSQLStatement += " AND " + sqlStatement;
                        }
                        string qualifiers = " ORDER BY receivedDate DESC LIMIT " + blockSize.ToString() + " OFFSET " + startingIndex.ToString() + ";";
                        selectMaxCmd.CommandText = fullSQLStatement + qualifiers;
                        val = selectMaxCmd.ExecuteQuery<Message>();
                    });
                    var msgwithattachment = from msg in val
                                            select new MessageManager()
                                            {
                                                //index = msg._id,
                                                messageId = msg.id,
                                                displayFromEmail = msg.displayFromEmail,
                                                fromEmail = msg.fromEmail,
                                                //toRecipientText = msg.toRecipientText,
                                                //ccRecipientText = msg.ccRecipientText,
                                                //bccRecipientText = msg.bccRecipientText,
                                                receivedDate = msg.receivedDate,
                                                flagStatus = msg.flagStatus,
                                                event_id = msg.event_id,
                                                subject = msg.subject,
                                                importance = msg.importance,
                                                previewText = msg.previewText,
                                                //bodyText = msg.bodyText,
                                                folderId = msg.folder_id,
                                                attachment_serverId = msg.serverIdentifier,
                                                read = msg.read,
                                                flag = msg.flag,
                                                serverID = msg.serverIdentifier,
                                                hasAttachment = msg.hasAttachment,
                                                //messageClass = msg.messageClass
                                                attach_MessageId = msg.id,
                                                flagStartDate = msg.flagStartDate,
                                                flagDueDate = msg.flagDueDate,
                                                flagUTCStartDate = msg.flagUTCStartDate,
                                                flagUTCDueDate = msg.flagUTCDueDate,
                                                flagReminderSet = msg.flagReminderSet,
                                                lastVerbExecuted = msg.lastVerbExecuted
                                            };
                    return msgwithattachment.ToList();
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<List<MessageManager>> GetDraftPartInfoAsync(int folderId, int MessageID = 0)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //check if message id ==0 then retrun all message info
                if (MessageID == 0)
                {

                    var draftmessages =  from draft in await ds.Db.Table<Draft_Parts>().Where(c => c.partType == 1).ToListAsync().ConfigureAwait(false)
                                         join msg in await ds.Db.Table<Message>().ToListAsync().ConfigureAwait(false)
                                         on draft.message_id equals msg._id                                        
                                         select new MessageManager
                                            {
                                                messageId = msg._id,
                                                displayFromEmail = msg.displayFromEmail,
                                                fromEmail = msg.fromEmail,
                                                receivedDate = msg.receivedDate,
                                                flagStatus = msg.flagStatus,
                                                event_id = msg.event_id,
                                                subject = msg.subject,
                                                importance = msg.importance,
                                                previewText = msg.previewText,
                                                bodyText = msg.bodyText,
                                                folderId = msg.folder_id,
                                                attachment_serverId = msg.serverIdentifier,
                                                read = msg.read,
                                                flag = msg.flag,
                                                serverID = msg.serverIdentifier,
                                                hasAttachment=msg.hasAttachment,
                                                attach_MessageId = msg.id,
                                                messageClass = msg.messageClass,
                                                lastVerbExecuted = msg.lastVerbExecuted
                                            };                   
                    return draftmessages.OrderByDescending(msg => msg.receivedDate).ToList();
                }
                return null;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Add message
        /// <summary>
        /// Add message information into database
        /// </summary>
        /// <param name="ObjMessage">Message object</param>
        public int AddMessageInfo(SQLiteConnection conn, Message ObjMessage)
        {
            try
            {
                var exist = conn.Table<Message>().Where(c => c.serverIdentifier == ObjMessage.serverIdentifier).ToList();
                if (exist != null && exist.Count > 0)
                {
                    return 0;
                }
                //set all the message data here
                var newMessage = new Message()
                {
                    id = ObjMessage.id, // This needs to be copied.
                    serverIdentifier = ObjMessage.serverIdentifier,
                    toRecipientText = ObjMessage.toRecipientText,
                    toRecipientEmail = ObjMessage.toRecipientEmail,
                    ccRecipientText = ObjMessage.ccRecipientText,
                    isRecipientAsBcc = ObjMessage.isRecipientAsBcc,
                    displayFromEmail = ObjMessage.displayFromEmail,
                    fromEmail = ObjMessage.fromEmail,
                    receivedDate = ObjMessage.receivedDate,
                    subject = ObjMessage.subject,
                    folder_id = ObjMessage.folder_id,
                    read = ObjMessage.read,
                    importance = ObjMessage.importance,
                    previewText = ObjMessage.previewText,
                    bodyText = ObjMessage.bodyText,
                    mimeType = ObjMessage.mimeType,
                    conversationId = ObjMessage.conversationId,
                    conversationIndex = ObjMessage.conversationIndex,
                    lastVerbExecuted = ObjMessage.lastVerbExecuted,

                    flagType = ObjMessage.flagType,
                    flag = ObjMessage.flag,
                    flagStatus = ObjMessage.flagStatus,
                    draftActionType = ObjMessage.draftActionType,
                    draftActionReferenceMessage_id = ObjMessage.draftActionReferenceMessage_id,
                    draftActionReferenceMessage_longid = ObjMessage.draftActionReferenceMessage_longid,
                    flagCompletedDate = ObjMessage.flagCompletedDate,
                    flagCompletedTime = ObjMessage.flagCompletedTime,
                    flagStartDate = ObjMessage.flagStartDate,
                    flagDueDate = ObjMessage.flagDueDate,
                    flagUTCStartDate = ObjMessage.flagUTCStartDate,
                    flagUTCDueDate = ObjMessage.flagUTCDueDate,
                    flagReminderSet = ObjMessage.flagReminderSet,
                    flagReminderTime = ObjMessage.flagReminderTime,
                    flagOrdinalDate = ObjMessage.flagOrdinalDate,
                    flagSubOrdinalDate = ObjMessage.flagSubOrdinalDate,
                    //Calendar related item
                    event_id = ObjMessage.event_id,
                    allDay = ObjMessage.allDay,
                    location = ObjMessage.location,
                    timeZone = ObjMessage.timeZone,
                    startDayAndTime = ObjMessage.startDayAndTime,
                    endDayAndTime = ObjMessage.endDayAndTime,
                    meetingStatus = ObjMessage.meetingStatus,
                    organizerEmail = ObjMessage.organizerEmail,
                    organizerName = ObjMessage.organizerName,
                    responseRequested = ObjMessage.responseRequested,
                    uid = ObjMessage.uid,
                    availability = ObjMessage.availability

                };
                //insert message information into database
                int result = conn.Insert(newMessage);
                int msgid = 0;
                if (result == 1)
                {
                    msgid = conn.Table<Message>().ToList().Max(m => (int?)m.id).Value;
                }
                return msgid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task AddMessageInfoAsync(SQLiteAsyncConnection conn, Message ObjMessage)
        {
            try
            {
                var exist = conn.Table<Message>().Where(c => c.serverIdentifier == ObjMessage.serverIdentifier).ToListAsync();
                if (exist != null && exist.Result.Count > 0)
                {
                    return;
                }
                //set all the message data here
                var newMessage = new Message()
                {
                    id = ObjMessage.id,
                    serverIdentifier = ObjMessage.serverIdentifier,
                    toRecipientText = ObjMessage.toRecipientText,
                    toRecipientEmail = ObjMessage.toRecipientEmail,
                    ccRecipientText = ObjMessage.ccRecipientText,
                    isRecipientAsBcc = ObjMessage.isRecipientAsBcc,
                    displayFromEmail = ObjMessage.displayFromEmail,
                    fromEmail = ObjMessage.fromEmail,
                    receivedDate = ObjMessage.receivedDate,
                    subject = ObjMessage.subject,
                    folder_id = ObjMessage.folder_id,
                    read = ObjMessage.read,
                    importance = ObjMessage.importance,
                    previewText = ObjMessage.previewText,
                    bodyText = ObjMessage.bodyText,
                    mimeType = ObjMessage.mimeType,
                    conversationId = ObjMessage.conversationId,
                    conversationIndex = ObjMessage.conversationIndex,
                    lastVerbExecuted = ObjMessage.lastVerbExecuted,
                    flagType = ObjMessage.flagType,
                    flag = ObjMessage.flag,
                    flagStatus = ObjMessage.flagStatus,
                    draftActionType = ObjMessage.draftActionType,
                    draftActionReferenceMessage_id = ObjMessage.draftActionReferenceMessage_id,
                    draftActionReferenceMessage_longid = ObjMessage.draftActionReferenceMessage_longid,
                    flagCompletedDate = ObjMessage.flagCompletedDate,
                    flagCompletedTime = ObjMessage.flagCompletedTime,
                    flagStartDate = ObjMessage.flagStartDate,
                    flagDueDate = ObjMessage.flagDueDate,
                    flagUTCStartDate = ObjMessage.flagUTCStartDate,
                    flagUTCDueDate = ObjMessage.flagUTCDueDate,
                    flagReminderSet = ObjMessage.flagReminderSet,
                    flagReminderTime = ObjMessage.flagReminderTime,
                    flagOrdinalDate = ObjMessage.flagOrdinalDate,
                    flagSubOrdinalDate = ObjMessage.flagSubOrdinalDate,
                    //calendar related stuff
                    event_id = ObjMessage.event_id,
                    allDay = ObjMessage.allDay,
                    location = ObjMessage.location,
                    timeZone = ObjMessage.timeZone,
                    startDayAndTime = ObjMessage.startDayAndTime,
                    endDayAndTime = ObjMessage.endDayAndTime,
                    meetingStatus = ObjMessage.meetingStatus,
                    organizerEmail = ObjMessage.organizerEmail,
                    organizerName = ObjMessage.organizerName,
                    responseRequested = ObjMessage.responseRequested,
                    uid = ObjMessage.uid,
                    availability = ObjMessage.availability,
                    messageClass = ObjMessage.messageClass

                };
                //insert message information into database
                int result = await conn.InsertAsync(newMessage).ConfigureAwait(false);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public void AddAllMessageInfo(SQLiteConnection db, IList<Message> messages)
        {
            try
            {
                //get connection string and store in variable
                db.InsertAll(messages);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task AddAllMessageInfoAsync(IList<Message> messages)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                if (messages.Count > 0)
                {
                    await ds.Db.RunInTransactionAsync(async (SQLiteAsyncConnection connection) =>
                    {
                        await connection.InsertAllAsync(messages).ConfigureAwait(false);
                    });
                }
                //await db.InsertAllAsync(messages).ConfigureAwait(false);
            }
            catch (Exception ex)
            { }
            finally 
            {
                ds.Dispose(); 
            }
        }
        #endregion

        #region Update message
        /// <summary>
        /// Update partucluar message information into database
        /// </summary>
        /// <param name="ObjMessage">Message object</param>
        public void UpdateMessageInfo(Message ObjMessage)
        {
            DataSource ds = new DataSource();
            try
            {
                //get message info base on message id
                var message = ds.Db.Table<Message>().Where(c => c.id == ObjMessage.id).Single();
                //if message exists
                if (message != null)
                {
                    message.serverIdentifier = ObjMessage.serverIdentifier;
                    message.toRecipientText = ObjMessage.toRecipientText;
                    message.toRecipientEmail = ObjMessage.toRecipientEmail;
                    message.ccRecipientText = ObjMessage.ccRecipientText;
                    message.isRecipientAsBcc = ObjMessage.isRecipientAsBcc;
                    message.displayFromEmail = ObjMessage.displayFromEmail;
                    message.fromEmail = ObjMessage.fromEmail;
                    message.receivedDate = ObjMessage.receivedDate;
                    message.subject = ObjMessage.subject;
                    message.folder_id = ObjMessage.folder_id;
                    message.read = ObjMessage.read;
                    message.importance = ObjMessage.importance;
                    message.previewText = ObjMessage.previewText;
                    message.bodyText = ObjMessage.bodyText;
                    message.mimeType = ObjMessage.mimeType;
                    message.conversationId = ObjMessage.conversationId;
                    message.conversationIndex = ObjMessage.conversationIndex;
                    message.lastVerbExecuted = ObjMessage.lastVerbExecuted;
                    message.event_id = ObjMessage.event_id;
                    message.flag = ObjMessage.flag;
                    message.flagType = ObjMessage.flagType;
                    message.flagStatus = ObjMessage.flagStatus;
                    message.draftActionType = ObjMessage.draftActionType;
                    message.draftActionReferenceMessage_id = ObjMessage.draftActionReferenceMessage_id;
                    message.draftActionReferenceMessage_longid = ObjMessage.draftActionReferenceMessage_longid;
                    message.flagCompletedDate = ObjMessage.flagCompletedDate;
                    message.flagCompletedTime = ObjMessage.flagCompletedTime;
                    message.flagStartDate = ObjMessage.flagStartDate;
                    message.flagDueDate = ObjMessage.flagDueDate;
                    message.flagUTCStartDate = ObjMessage.flagUTCStartDate;
                    message.flagUTCDueDate = ObjMessage.flagUTCDueDate;
                    message.flagReminderSet = ObjMessage.flagReminderSet;
                    message.flagReminderTime = ObjMessage.flagReminderTime;
                    message.flagOrdinalDate = ObjMessage.flagOrdinalDate;
                    message.flagSubOrdinalDate = ObjMessage.flagSubOrdinalDate;
                    message.messageClass = ObjMessage.messageClass;
                    //update message info
                    ds.Db.Update(message);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task UpdateMessageInfoAsync(Message ObjMessage)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get message info base on message id
                var message = await ds.Db.Table<Message>().Where(c => c.id == ObjMessage.id && c.folder_id == ObjMessage.folder_id).FirstAsync().ConfigureAwait(false);
                //var message = db.Table<Message>().Where(c => c.id == ObjMessage.id).Single();
                //if message exists
                if (message != null)
                {
                    message.serverIdentifier = ObjMessage.serverIdentifier;
                    message.toRecipientText = ObjMessage.toRecipientText;
                    message.toRecipientEmail = ObjMessage.toRecipientEmail;
                    message.ccRecipientText = ObjMessage.ccRecipientText;
                    message.isRecipientAsBcc = ObjMessage.isRecipientAsBcc;
                    message.displayFromEmail = ObjMessage.displayFromEmail;
                    message.fromEmail = ObjMessage.fromEmail;
                    message.receivedDate = ObjMessage.receivedDate;
                    message.subject = ObjMessage.subject;
                    message.folder_id = ObjMessage.folder_id;
                    message.read = ObjMessage.read;
                    message.importance = ObjMessage.importance;
                    message.previewText = ObjMessage.previewText;
                    message.bodyText = ObjMessage.bodyText;
                    message.mimeType = ObjMessage.mimeType;
                    message.conversationId = ObjMessage.conversationId;
                    message.conversationIndex = ObjMessage.conversationIndex;
                    message.lastVerbExecuted = ObjMessage.lastVerbExecuted;
                    message.event_id = ObjMessage.event_id;
                    message.flag = ObjMessage.flag;
                    message.flagType = ObjMessage.flagType;
                    message.flagStatus = ObjMessage.flagStatus;
                    message.draftActionType = ObjMessage.draftActionType;
                    message.draftActionReferenceMessage_id = ObjMessage.draftActionReferenceMessage_id;
                    message.draftActionReferenceMessage_longid = ObjMessage.draftActionReferenceMessage_longid;
                    message.flagCompletedDate = ObjMessage.flagCompletedDate;
                    message.flagCompletedTime = ObjMessage.flagCompletedTime;
                    message.flagStartDate = ObjMessage.flagStartDate;
                    message.flagDueDate = ObjMessage.flagDueDate;
                    message.flagUTCStartDate = ObjMessage.flagUTCStartDate;
                    message.flagUTCDueDate = ObjMessage.flagUTCDueDate;
                    message.flagReminderSet = ObjMessage.flagReminderSet;
                    message.flagReminderTime = ObjMessage.flagReminderTime;
                    message.flagOrdinalDate = ObjMessage.flagOrdinalDate;
                    message.flagSubOrdinalDate = ObjMessage.flagSubOrdinalDate;
                    message.responseRequested = ObjMessage.responseRequested;
                    message.messageClass = ObjMessage.messageClass;

                    await ds.Db.UpdateAsync(message).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public void UpdateAllMessagesInfo(IList<Message> messages)
        {
            DataSource ds = new DataSource();
            try
            {
                //get connection string and store in variable
                ds.Db.UpdateAll(messages);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async void UpdateAllMessagesInfoAsync(IList<Message> messages)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
#if WP_80_SILVERLIGHT
                await ds.Db.UpdateAllAsync(messages).ConfigureAwait(false);
#endif
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Delete message
        /// <summary>
        /// Delete particular message information from database
        /// </summary>
        /// <param name="MessageID">Message id</param>
        public void DeleteMessageInfo(int messageID)
        {
            DataSource ds = new DataSource();
            try
            {
                //get message based on message id
                var message = ds.Db.Table<Message>().Where(c => c.id == messageID).Single();
                //if data exists
                if (message != null)
                {
                    //delete particular record from database
                    ds.Db.Delete(message);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public void DeleteMessageInfo(int folderID, int messageID)
        {
            DataSource ds = new DataSource();
            try
            {
                //get message based on message id
                var message = ds.Db.Table<Message>().Where(c => c.id == messageID).Single();
                //if data exists
                if (message != null)
                {
                    //delete particular record from database
                    ds.Db.Delete(message);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async void DeleteMessageInfoAsync(int folderID, int messageID)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get message based on message id
                var message = await ds.Db.Table<Message>().Where(c => c.id == messageID && c.folder_id == folderID).ToListAsync().ConfigureAwait(false);
                //if data exists
                if (message != null && message.Count > 0)
                {
                    //delete particular record from database
                    await ds.Db.DeleteAsync(message.FirstOrDefault()).ConfigureAwait(false);
                    //delete attachment
                    AttachmentManager am = new AttachmentManager();
                    am.DeleteAttachmentInfoWithMessageIdAsync(ds.Db,folderID, messageID);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public void DeleteAllMessagesForFolder(int folderID)
        {
            List<MessageManager> messagesWithAttachments = GetMessagewithAttachInfo(folderID, 0);

            if (null != messagesWithAttachments && messagesWithAttachments.Count > 0)
            {
                try
                {
                    foreach (MessageManager mm in messagesWithAttachments)
                    {
                        AttachmentManager am = new AttachmentManager();
                        am.DeleteAttachmentInfoWithMessageId(mm.messageId);
                        mm.DeleteMessageInfo(folderID, mm.messageId);
                    }
                    // Reset the sync key
                    FolderManager fm = new FolderManager();
                    List<Folder> folders = fm.GetFolderInfo(folderID).Result;
                    // Should only be 1
                    folders[0].syncKey = "1";
                    folders[0].syncTime = Convert.ToSingle(DateTime.MinValue.ToOADate());
                }
                catch (Exception)
                {

                }
            }
        }

        public async void DeleteAllMessagesForFolderAsync(int folderID)
        {
            List<MessageManager> messagesWithAttachments = await GetMessagewithAttachInfoAsync(folderID, 0);

            if (null != messagesWithAttachments && messagesWithAttachments.Count > 0)
            {
                try
                {
                    foreach (MessageManager mm in messagesWithAttachments)
                    {                       
                        mm.DeleteMessageInfoAsync(folderID, mm.messageId);
                    }

                    // Reset the sync key
                    FolderManager fm = new FolderManager();
                    List<Folder> folders = await fm.GetFolderInfoAsync(folderID);
                    // Should only be 1
                    folders[0].syncKey = "1";
                    folders[0].syncTime = Convert.ToSingle(DateTime.MinValue.ToOADate());
                }
                catch (Exception)
                {

                }
            }
        }

        public async Task DeleteDraftMessageInfoAsync(int messageID,int folderId)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                if(folderId==0)
                {
                    folderId = await (new FolderManager()).GetFolderIdentifierBasedOnFolderNameAsync("Drafts");                    
                }
                //get message based on message id
                var message = await ds.Db.Table<Message>().Where(c => c._id == messageID && c.folder_id == folderId).ToListAsync().ConfigureAwait(false);
                //if data exists
                if (message != null && message.Count > 0)
                {
                    //delete particular record from database
                    await ds.Db.DeleteAsync(message.FirstOrDefault()).ConfigureAwait(false);
                }
            }
            catch (Exception)
            {
                //throw exception
                //throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task DeletMultipleMsgwithAttachmentAsync(IList<Message> messages, IList<Attachment> attachmnts)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                await ds.Db.RunInTransactionAsync((SQLiteConnection connection) =>
                {
                    //delete all attachments
                    foreach (Attachment attach in attachmnts)
                    {
                        //get attachment based on attachment id
                        var attachments = connection.Table<Attachment>().Where(c => c.message_id == attach.message_id && c.folder_id == attach.folder_id).ToList();
                        if (attachments != null)
                        {
                            foreach (Attachment attachment in attachments)
                            {
                                //if data exists
                                if (attachment != null && attachment.id != null)
                                {
                                    //delete particular record from database
                                    connection.Delete(attachment);
                                }
                            }
                        }
                    }
                    //delete all messages
                    foreach (Message msg in messages)
                    {                       
                        //get message based on message id
                        var message =  connection.Table<Message>().Where(c => c.id == msg.id && c.folder_id == msg.folder_id).ToList();
                        //if data exists
                        if (message != null && message.Count > 0)
                        {
                            //delete particular record from database
                            connection.Delete(message.FirstOrDefault());
                        }
                    }
                });
                
            }
            catch (Exception ex)
            { }
            finally 
            { 
                ds.Dispose();
            }
           
        }
      
        public async Task DeleteMessageInfoAsync(SQLiteAsyncConnection db, int folderID, int messageID)
        {

            try
            {
                //get message based on message id
                var message = await db.Table<Message>().Where(c => c.id == messageID && c.folder_id == folderID).ToListAsync().ConfigureAwait(false);
                //if data exists
                if (message != null && message.Count > 0)
                {
                    //delete particular record from database
                    await db.DeleteAsync(message.FirstOrDefault()).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }
        #endregion

        public int GetMessageIDFromServerId(string serverID)
        {
            //Create datasource object and get connection string
            DataSource ds = new DataSource();
            try
            {
                int messageId = Convert.ToInt32(serverID.Split(':')[1]);
                string serverIdentifier = serverID.Split(':')[0];
                //get message based on message id
                //Pankaj: Todo-If messageId is not present, its crashing...why?
                var message = ds.Db.Table<Message>().Where(c => c.serverIdentifier == serverID).ToList();
                //if data exists
                if (message != null)
                {
                    //delete particular record from database
                    int theId = -1;
                    foreach (Message msg in message)
                    {
                        int j = 10;
                        theId = msg.id;
                        break;
                    }
                    return theId;
                }
                return -1;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<int> GetMessageIDFromServerIdAsync(string serverID)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                int messageId = Convert.ToInt32(serverID.Split(':')[1]);
                string serverIdentifier = serverID.Split(':')[0];
                //get message based on message id
                //Pankaj: Todo-If messageId is not present, its crashing...why?
                var message = await ds.Db.Table<Message>().Where(c => c.serverIdentifier == serverID).ToListAsync().ConfigureAwait(false);
                //if data exists
                if (message != null)
                {
                    //delete particular record from database
                    int theId = -1;
                    foreach (Message msg in message)
                    {
                        theId = msg.id;
                        break;
                    }
                    return theId;
                }
                return -1;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        /// <summary>
        /// Add message information into database
        /// </summary>
        /// <param name="ObjMessage">Message object</param>
        public async Task<Message> AddMessageInDraft(Message ObjMessage, List<Draft_Parts> lstmsgAttachmentForDraft)
        {
            DataSourceAsync conn = new DataSourceAsync();
            try
            {

                //set all the message data here
                var newMessage = new Message()
                {
                    id = ObjMessage.id,
                    serverIdentifier = ObjMessage.serverIdentifier,
                    toRecipientText = ObjMessage.toRecipientText,
                    toRecipientEmail = ObjMessage.toRecipientEmail,
                    ccRecipientText = ObjMessage.ccRecipientText,
                    bccRecipientText = ObjMessage.bccRecipientText,
                    displayFromEmail = ObjMessage.displayFromEmail,
                    fromEmail = ObjMessage.fromEmail,
                    receivedDate = DateTime.Now.ToOADate(),
                    subject = ObjMessage.subject,
                    folder_id = ObjMessage.folder_id,
                    read = ObjMessage.read,
                    importance = ObjMessage.importance,
                    previewText = ObjMessage.previewText,
                    bodyText = ObjMessage.bodyText,
                    mimeType = ObjMessage.mimeType,
                    conversationId = ObjMessage.conversationId,
                    conversationIndex = ObjMessage.conversationIndex,
                    lastVerbExecuted = ObjMessage.lastVerbExecuted,

                    flagType = ObjMessage.flagType,
                    flag = ObjMessage.flag,
                    flagStatus = ObjMessage.flagStatus,
                    draftActionType = ObjMessage.draftActionType,
                    draftActionReferenceMessage_id = ObjMessage.draftActionReferenceMessage_id,
                    draftActionReferenceMessage_longid = ObjMessage.draftActionReferenceMessage_longid,
                    flagCompletedDate = ObjMessage.flagCompletedDate,
                    flagCompletedTime = ObjMessage.flagCompletedTime,
                    flagStartDate = ObjMessage.flagStartDate,
                    flagDueDate = ObjMessage.flagDueDate,
                    flagUTCStartDate = ObjMessage.flagUTCStartDate,
                    flagUTCDueDate = ObjMessage.flagUTCDueDate,
                    flagReminderSet = ObjMessage.flagReminderSet,
                    flagReminderTime = ObjMessage.flagReminderTime,
                    flagOrdinalDate = ObjMessage.flagOrdinalDate,
                    flagSubOrdinalDate = ObjMessage.flagSubOrdinalDate,
                    //Calendar related item
                    event_id = ObjMessage.event_id,
                    allDay = ObjMessage.allDay,
                    location = ObjMessage.location,
                    timeZone = ObjMessage.timeZone,
                    startDayAndTime = ObjMessage.startDayAndTime,
                    endDayAndTime = ObjMessage.endDayAndTime,
                    meetingStatus = ObjMessage.meetingStatus,
                    organizerEmail = ObjMessage.organizerEmail,
                    organizerName = ObjMessage.organizerName,
                    responseRequested = ObjMessage.responseRequested,
                    uid = ObjMessage.uid,
                    availability = ObjMessage.availability,
                    hasAttachment=ObjMessage.hasAttachment,
                    messageClass = ObjMessage.messageClass
                };
                //insert message information into database
                int result = await conn.Db.InsertAsync(newMessage).ConfigureAwait(false);
                int msgid = 0;
                if (result == 1)
                {
                    msgid = newMessage._id;
                }
                if (msgid >= 1)
                {

                    Draft_PartsManager objDraftMngr = new Draft_PartsManager();
                    Draft_Parts objDraftMsg = new Draft_Parts();
                    objDraftMsg.content = newMessage.bodyText;
                    objDraftMsg.contentType = newMessage.mimeType;
                    objDraftMsg.partType = (int)DraftPartsType.PartMessage;
                    objDraftMsg.message_id = msgid;
                    objDraftMngr.AddDraftPartsInfo(conn.Db, objDraftMsg);
                    if (lstmsgAttachmentForDraft != null)
                    {
                        foreach (Draft_Parts draft_attachment in lstmsgAttachmentForDraft)
                        {
                            draft_attachment.message_id = msgid;
                            objDraftMngr.AddDraftPartsInfo(conn.Db, draft_attachment);
                        }
                    }
                }
                return newMessage;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                conn.Dispose();
            }
        }
    }
}
