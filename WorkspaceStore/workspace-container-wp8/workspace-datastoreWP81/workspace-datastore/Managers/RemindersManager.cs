﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using workspace_datastore.models;

/// <summary>
/// It incudes feature to add, edit ,get ,and delete reminder data into\from database.
/// </summary>
namespace workspace_datastore.Managers
{
    //*********************************************************************************************************************
    // <copyright file="RemindersManager.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>11-04-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           11-04-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
        
   public class RemindersManager
   {

        #region Get all or particular reminder
       /// <summary>
        /// method to get all reminders info or get reminder info based on reminder id(if id=0 then it will return all reminders info)
        /// </summary>
        /// <param name="ReminderID">Reminder id</param>
        /// <returns></returns>
        public List<Reminders> GetReminderInfo(int ReminderID)
        {
            //create local varibale to store reminder info into list 
            List<Reminders> existReminderInfo = null;
            try
            {
                //get connection string and store in variable
                var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //check if reminder id ==0 then retrun all remidner info
                if (ReminderID == 0)
                {
                    //get all reminder info
                    existReminderInfo = db.Table<Reminders>().ToList();
                }
                else
                {
                    //get reminder info based on reminder id
                    existReminderInfo = db.Table<Reminders>().Where(c => c.id == ReminderID).ToList();
                }
                //dispose object
                db.Dispose();
                //return result
                return existReminderInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }
       #endregion

        #region Add/Update reminder information
        /// <summary>
        /// Add\Update reminders information into database
        /// </summary>
        /// <param name="ObjReminder">Reminder object</param>
        public void AddUpdateReminderInfo(Reminders ObjReminder)
        {
            //get connection string and store in variable
            var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
            try
            {                
                //get reminder info base on reminder id
                var existingReminder = db.Table<Reminders>().Where(c => c.id == ObjReminder.id).Single();
                //check if data already exists then update else insert
                if (existingReminder != null)
                {
                    existingReminder.event_id = ObjReminder.event_id;
                    existingReminder.minutes = ObjReminder.minutes;
                    existingReminder.method = ObjReminder.method;                   
                    //update reminders info
                    db.Update(existingReminder);
                }
                else 
                {
                    //set all the reminder data here
                    var newReminder = new Reminders()
                    {
                        event_id = ObjReminder.event_id,
                        minutes = ObjReminder.minutes,
                        method = ObjReminder.method                       
                    };
                    //insert reminder information into database
                    db.Insert(newReminder);
                }               
                //dispose object
                db.Dispose();
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                db.Dispose();
            }            
        }
        #endregion

        #region Delete reminder information
        /// <summary>
        /// Delete particular reminder information from database
        /// </summary>
        /// <param name="ReminderID">Reminder id</param>
        public void DeleteReminderInfo(int ReminderID)
        {

            try
            {
                //get connection string and store in variable
                var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //get reminder based on reminder id
                var reminder = db.Table<Reminders>().Where(c => c.id == ReminderID).Single();
                //if data exists
                if (reminder != null)
                {
                    //delete particular record from database
                    db.Delete(reminder);
                }
                //dispose dataabse object
                db.Dispose();
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }
        #endregion
   }
}
