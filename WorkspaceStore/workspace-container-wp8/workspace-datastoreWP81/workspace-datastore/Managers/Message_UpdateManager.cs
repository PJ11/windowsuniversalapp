﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using workspace_datastore.models;

/// <summary>
/// It incudes feature to add, edit ,get ,and delete updated message information into\from database.
/// </summary>
namespace workspace_datastore.Managers
{
    //*********************************************************************************************************************
    // <copyright file="Message_UpdateManager.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>15-04-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           15-04-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
 
   public class Message_UpdateManager
   {
        #region Get all or particular updated message
       /// <summary>
        /// method to get all updated message info or get updated message info based on updated message id(if id=0 then it will return all updated message info)
        /// </summary>
        /// <param name="UpdatedMsgId">UpdatedMsg id</param>
        /// <returns></returns>
        public List<Message_Updates> GetUpdatedMsgInfo(int UpdatedMsgId)
        {
            //create local varibale to store updated message info into list 
            List<Message_Updates> existUpdatedMsgInfo = null;
            try
            {
                //get connection string and store in variable
                var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //check if updated message id ==0 then retrun all updated message info
                if (UpdatedMsgId == 0)
                {
                    //get all updated message info
                    existUpdatedMsgInfo = db.Table<Message_Updates>().ToList();
                }
                else
                {
                    //get updated message info based on updated message id
                    existUpdatedMsgInfo = db.Table<Message_Updates>().Where(c => c.id == UpdatedMsgId).ToList();
                }
                //dispose object
                db.Dispose();
                //return result
                return existUpdatedMsgInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }

        public async Task<List<Message_Updates>> GetUpdatedMsgInfoAsync(int UpdatedMsgId)
        {
            //create local varibale to store updated message info into list 
            List<Message_Updates> existUpdatedMsgInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //check if updated message id ==0 then retrun all updated message info
                if (UpdatedMsgId == 0)
                {
                    //get all updated message info
                    existUpdatedMsgInfo = await ds.Db.Table<Message_Updates>().ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    //get updated message info based on updated message id
                    existUpdatedMsgInfo = await ds.Db.Table<Message_Updates>().Where(c => c.id == UpdatedMsgId).ToListAsync().ConfigureAwait(false);
                }
                //return result
                return existUpdatedMsgInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Add/Update updated message
        /// <summary>
        /// Add\Update updated message information into database
        /// </summary>
        /// <param name="ObjUpdatedMsg">Updated Msg object</param>
        public void AddUpdateUpdatedMsgInfo(Message_Updates ObjUpdatedMsg)
        {
            //get connection string and store in variable
            DataSource ds = new DataSource();
            try
            {
                //get updated message info base on updated message id
                var existingUpdatedMsg = ds.Db.Table<Message_Updates>().Where(c => c.id == ObjUpdatedMsg.id).Single();
                //check if data already exists then update else insert
                if (existingUpdatedMsg != null)
                {
                    existingUpdatedMsg.serverIdentifier = ObjUpdatedMsg.serverIdentifier;
                    existingUpdatedMsg.collectionId = ObjUpdatedMsg.collectionId;
                    existingUpdatedMsg.folder_id = ObjUpdatedMsg.folder_id;
                    existingUpdatedMsg.read = ObjUpdatedMsg.read;
                    existingUpdatedMsg.account_id = ObjUpdatedMsg.account_id;
                    existingUpdatedMsg.flagType = ObjUpdatedMsg.flagType;
                    existingUpdatedMsg.flagStatus = ObjUpdatedMsg.flagStatus;
                    existingUpdatedMsg.moveToFolderServerIdentifier = ObjUpdatedMsg.moveToFolderServerIdentifier;
                    existingUpdatedMsg.flagCompletedDate = ObjUpdatedMsg.flagCompletedDate;
                    existingUpdatedMsg.flagCompletedTime = ObjUpdatedMsg.flagCompletedTime;
                    existingUpdatedMsg.flagStartDate = ObjUpdatedMsg.flagStartDate;
                    existingUpdatedMsg.flagDueDate = ObjUpdatedMsg.flagDueDate;
                    existingUpdatedMsg.flagUTCStartDate = ObjUpdatedMsg.flagUTCStartDate;
                    existingUpdatedMsg.flagUTCDueDate = ObjUpdatedMsg.flagUTCDueDate;
                    existingUpdatedMsg.flagReminderSet = ObjUpdatedMsg.flagReminderSet;
                    existingUpdatedMsg.flagReminderTime = ObjUpdatedMsg.flagReminderTime;
                    existingUpdatedMsg.flagOrdinalDate = ObjUpdatedMsg.flagOrdinalDate;
                    existingUpdatedMsg.flagSubOrdinalDate = ObjUpdatedMsg.flagSubOrdinalDate;
                    existingUpdatedMsg.flag = ObjUpdatedMsg.flag;
                    
                    //update updated message info
                    ds.Db.Update(existingUpdatedMsg);
                }
                else
                {
                    //set all the updated message data here
                    var newUpdatedMsg = new Message_Updates()
                    {
                        serverIdentifier = ObjUpdatedMsg.serverIdentifier,
                        collectionId = ObjUpdatedMsg.collectionId,
                        folder_id = ObjUpdatedMsg.folder_id,
                        read = ObjUpdatedMsg.read,
                        account_id = ObjUpdatedMsg.account_id,
                        flagType = ObjUpdatedMsg.flagType,
                        flagStatus = ObjUpdatedMsg.flagStatus,
                        moveToFolderServerIdentifier = ObjUpdatedMsg.moveToFolderServerIdentifier,
                        flagCompletedDate = ObjUpdatedMsg.flagCompletedDate,
                        flagCompletedTime = ObjUpdatedMsg.flagCompletedTime,
                        flagStartDate = ObjUpdatedMsg.flagStartDate,
                        flagDueDate = ObjUpdatedMsg.flagDueDate,
                        flagUTCStartDate = ObjUpdatedMsg.flagUTCStartDate,
                        flagUTCDueDate = ObjUpdatedMsg.flagUTCDueDate,
                        flagReminderSet = ObjUpdatedMsg.flagReminderSet,
                        flagReminderTime = ObjUpdatedMsg.flagReminderTime,
                        flagOrdinalDate = ObjUpdatedMsg.flagOrdinalDate,
                        flagSubOrdinalDate = ObjUpdatedMsg.flagSubOrdinalDate,
                        flag = ObjUpdatedMsg.flag,
                    };
                    //insert updated message information into database
                    ds.Db.Insert(newUpdatedMsg);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        public async void AddUpdateUpdatedMsgInfoAsync(Message_Updates ObjUpdatedMsg)
        {
            //get connection string and store in variable
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get updated message info base on updated message id
                var existingUpdatedMsg = await ds.Db.Table<Message_Updates>().Where(c => c.id == ObjUpdatedMsg.id).FirstOrDefaultAsync().ConfigureAwait(false);
                //check if data already exists then update else insert
                if (existingUpdatedMsg != null)
                {
                    existingUpdatedMsg.serverIdentifier = ObjUpdatedMsg.serverIdentifier;
                    existingUpdatedMsg.collectionId = ObjUpdatedMsg.collectionId;
                    existingUpdatedMsg.folder_id = ObjUpdatedMsg.folder_id;
                    existingUpdatedMsg.read = ObjUpdatedMsg.read;
                    existingUpdatedMsg.account_id = ObjUpdatedMsg.account_id;
                    existingUpdatedMsg.flagType = ObjUpdatedMsg.flagType;
                    existingUpdatedMsg.flagStatus = ObjUpdatedMsg.flagStatus;
                    existingUpdatedMsg.moveToFolderServerIdentifier = ObjUpdatedMsg.moveToFolderServerIdentifier;
                    existingUpdatedMsg.flagCompletedDate = ObjUpdatedMsg.flagCompletedDate;
                    existingUpdatedMsg.flagCompletedTime = ObjUpdatedMsg.flagCompletedTime;
                    existingUpdatedMsg.flagStartDate = ObjUpdatedMsg.flagStartDate;
                    existingUpdatedMsg.flagDueDate = ObjUpdatedMsg.flagDueDate;
                    existingUpdatedMsg.flagUTCStartDate = ObjUpdatedMsg.flagUTCStartDate;
                    existingUpdatedMsg.flagUTCDueDate = ObjUpdatedMsg.flagUTCDueDate;
                    existingUpdatedMsg.flagReminderSet = ObjUpdatedMsg.flagReminderSet;
                    existingUpdatedMsg.flagReminderTime = ObjUpdatedMsg.flagReminderTime;
                    existingUpdatedMsg.flagOrdinalDate = ObjUpdatedMsg.flagOrdinalDate;
                    existingUpdatedMsg.flagSubOrdinalDate = ObjUpdatedMsg.flagSubOrdinalDate;
                    existingUpdatedMsg.flag = ObjUpdatedMsg.flag;
                    existingUpdatedMsg.lastVerbExecuted = ObjUpdatedMsg.lastVerbExecuted;
                    //update updated message info
                    await ds.Db.UpdateAsync(existingUpdatedMsg).ConfigureAwait(false);
                }
                else
                {
                    //set all the updated message data here
                    var newUpdatedMsg = new Message_Updates()
                    {
                        serverIdentifier = ObjUpdatedMsg.serverIdentifier,
                        collectionId = ObjUpdatedMsg.collectionId,
                        folder_id = ObjUpdatedMsg.folder_id,
                        read = ObjUpdatedMsg.read,
                        account_id = ObjUpdatedMsg.account_id,
                        flagType = ObjUpdatedMsg.flagType,
                        flagStatus = ObjUpdatedMsg.flagStatus,
                        moveToFolderServerIdentifier = ObjUpdatedMsg.moveToFolderServerIdentifier,
                        flagCompletedDate = ObjUpdatedMsg.flagCompletedDate,
                        flagCompletedTime = ObjUpdatedMsg.flagCompletedTime,
                        flagStartDate = ObjUpdatedMsg.flagStartDate,
                        flagDueDate = ObjUpdatedMsg.flagDueDate,
                        flagUTCStartDate = ObjUpdatedMsg.flagUTCStartDate,
                        flagUTCDueDate = ObjUpdatedMsg.flagUTCDueDate,
                        flagReminderSet = ObjUpdatedMsg.flagReminderSet,
                        flagReminderTime = ObjUpdatedMsg.flagReminderTime,
                        flagOrdinalDate = ObjUpdatedMsg.flagOrdinalDate,
                        flagSubOrdinalDate = ObjUpdatedMsg.flagSubOrdinalDate,
                        flag = ObjUpdatedMsg.flag,
                        lastVerbExecuted = ObjUpdatedMsg.lastVerbExecuted,
                    };
                    //insert updated message information into database
                    await ds.Db.InsertAsync(newUpdatedMsg).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Delete updated message
        /// <summary>
        /// Delete particular updated message information from database
        /// </summary>
        /// <param name="UpdatedMsgId">Updated  Msg id</param>
        public void DeleteUpdatedMsgInfo(int UpdatedMsgId)
        {
            DataSource ds = new DataSource();

            try
            {
                //get updated message based on updated message id
                var updatedMsg = ds.Db.Table<Message_Updates>().Where(c => c.id == UpdatedMsgId).Single();
                //if data exists
                if (updatedMsg != null)
                {
                    //delete particular record from database
                    ds.Db.Delete(updatedMsg);
                }
                //dispose dataabse object
                ds.Db.Dispose();
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async void DeleteAllChangedMessagesAsync()
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                List<Message_Updates> changeMessages = await ds.Db.Table<Message_Updates>().ToListAsync().ConfigureAwait(false);
                if (null != changeMessages && changeMessages.Count > 0)
                {
                    foreach (Message_Updates changeMessage in changeMessages)
                    {
                        await ds.Db.DeleteAsync(changeMessage).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

   }
}
