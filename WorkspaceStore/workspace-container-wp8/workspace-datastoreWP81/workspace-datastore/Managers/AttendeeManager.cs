﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using workspace_datastore.models;

/// <summary>
/// Manipulate attendee information into database. it incudes feature to add, edit ,get ,and delete attendee data into\from database.
/// </summary>
namespace workspace_datastore.Managers
{
    //*********************************************************************************************************************
    // <copyright file="AttendeeManager.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>11-04-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           11-04-2014    1.0            First Version </summary>
    //*********************************************************************************************************************

    public class AttendeeManager
    {
        #region Get attendee information
        /// <summary>
        /// method to get all attendee info or get attendee info based on account id(if id=0 then it will return all attachments info)
        /// </summary>
        /// <param name="AccId">attendee id</param>
        /// <returns></returns>
    
        public async Task<List<Attendees>> GetAttendeeInfo(string serverID)
        {
            //create local varibale to store account info into list 
            List<Attendees> existAttendeeInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                existAttendeeInfo = await ds.Db.Table<Attendees>().Where(c => c.event_id == serverID).ToListAsync().ConfigureAwait(false);
                return existAttendeeInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Add attendee information
        /// <summary>
        /// Add attendee information into database
        /// </summary>
        /// <param name="objAttendee">attendee object</param>
        public void AddAttendeeInfo(SQLiteConnection db, Attendees objAttendee)
        {
            try
            {
                //set all the attendee data here
                var newAttendee = new Attendees()
                {
                    event_id = objAttendee.event_id,
                    attendeeName = objAttendee.attendeeName,
                    attendeeEmail = objAttendee.attendeeEmail,
                    attendeeStatus = objAttendee.attendeeStatus,
                    attendeeRelationship = objAttendee.attendeeRelationship,
                    attendeeType = objAttendee.attendeeType
                };
                //insert attendee information into database
                db.Insert(newAttendee);
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }

        public async void  AddAllAttendees(List<Attendees> attendees)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
               await ds.Db.InsertAllAsync(attendees).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async void AddAttendeeInfoAsync(Attendees objAttendee)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                var exist = await ds.Db.Table<Attendees>().Where(c => c.event_id == objAttendee.event_id &&
                    c.attendeeEmail == objAttendee.attendeeEmail).ToListAsync().ConfigureAwait(false);
                if (exist != null && exist.Count > 0)
                {
                    return;
                }
                //get connection string and store in variable
                //var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //set all the attendee data here
                var newAttendee = new Attendees()
                {
                    event_id = objAttendee.event_id,
                    attendeeName = objAttendee.attendeeName,
                    attendeeEmail = objAttendee.attendeeEmail,
                    attendeeStatus = objAttendee.attendeeStatus,
                    attendeeRelationship = objAttendee.attendeeRelationship,
                    attendeeType = objAttendee.attendeeType
                };
                //insert attendee information into database
                await ds.Db.InsertAsync(newAttendee).ConfigureAwait(false);

            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Update particular attendee information
        /// <summary>
        /// Update partucluar attendee information into database
        /// </summary>
        /// <param name="objAttendee">attendee object</param>
        public void UpdateAttendeeInfo(Attendees objaAttendee)
        {
            DataSource ds = new DataSource();
            try
            {
                //get attendee info base on account id
                var attendee = ds.Db.Table<Attendees>().Where(c => c.id == objaAttendee.id).Single();
                //if attendee exists
                if (attendee != null)
                {
                    attendee.event_id = objaAttendee.event_id;
                    attendee.attendeeName = objaAttendee.attendeeName;
                    attendee.attendeeEmail = objaAttendee.attendeeEmail;
                    attendee.attendeeStatus = objaAttendee.attendeeStatus;
                    attendee.attendeeRelationship = objaAttendee.attendeeRelationship;
                    attendee.attendeeType = objaAttendee.attendeeType;
                    //update attendee info
                    ds.Db.Update(attendee);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Delete particular attendee
        /// <summary>
        /// Delete particular attendee information from database
        /// </summary>
        /// <param name="AccId">attendee id</param>
        public void DeleteAttendeeInfo(int AttendeeId)
        {
            DataSource ds = new DataSource();
            try
            {
                //get attendee based on attendee id
                var attendee = ds.Db.Table<Attendees>().Where(c => c.id == AttendeeId).Single();
                //if data exists
                if (attendee != null)
                {
                    //delete particular record from database
                    ds.Db.Delete(attendee);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async void DeleteAttendeeListInfo(string serverIDOfCalendar)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get connection string and store in variable
                //var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //get attendee based on attendee id
                List<Attendees> attendeeList = await ds.Db.Table<Attendees>().Where(c => c.event_id == serverIDOfCalendar).ToListAsync().ConfigureAwait(false);

                //if data exists
                if (attendeeList != null && attendeeList.Count > 0)
                {
                    //delete particular record from database
                    foreach (Attendees attendee in attendeeList)
                    {
                       await ds.Db.DeleteAsync(attendee).ConfigureAwait(false);
                    }
                }
                //dispose dataabse object

            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task DeleteAttendeeListInfoAsync(string serverIDOfCalendar)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get attendee based on attendee id
                List<Attendees> attendeeList = await ds.Db.Table<Attendees>().Where(c => c.event_id == serverIDOfCalendar).ToListAsync().ConfigureAwait(false);

                //if data exists
                if (attendeeList != null && attendeeList.Count > 0)
                {
                    //delete particular record from database
                    foreach (Attendees attendee in attendeeList)
                    {
                        await ds.Db.DeleteAsync(attendee);
                    }
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion
    }
}
