﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using workspace_datastore.models;


/// <summary>
/// Manipulate account information into database. Here we have added feature to add, edit ,get ,and delete data into database.
/// </summary>
namespace workspace_datastore.Managers
{

    //*********************************************************************************************************************
    // <copyright file="AccountManager.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>10-04-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           10-04-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
    public class AccountManager
    {

        #region Get account information
        /// <summary>
        /// method to get all acount info or get account info based on email id(if email id=null or empty then it will return all accounts info)
        /// </summary>
        /// <param name="EmailId">Email id</param>
        /// <returns>accoutn object</returns>
        public List<Account> GetAccountInfo(string EmailId)
        {
            //create local varibale to store account info into list 
            List<Account> existAccInfo = null;
            //Create datasource object and get connection string
            DataSource ds = new DataSource();
            try
            {
                //check if email id is emoty or null retrun all accounts info
                if (string.IsNullOrEmpty(EmailId))
                {
                    //get all account info
                    existAccInfo = ds.Db.Table<Account>().ToList();
                }
                else
                {
                    //get account info based on account id
                    existAccInfo = ds.Db.Table<Account>().Where(c => c.emailAddress == EmailId).ToList();
                }
                return existAccInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<List<Account>> GetAccountInfoAsync(string EmailId)
        {
            //create local varibale to store account info into list 
            List<Account> existAccInfo = null;
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //check if email id is emoty or null retrun all accounts info
                if (string.IsNullOrEmpty(EmailId))
                {
                    //get all account info
                    existAccInfo = await ds.Db.Table<Account>().ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    //get account info based on account id
                    existAccInfo = await ds.Db.Table<Account>().Where(c => c.emailAddress.ToLower() == EmailId.ToLower()).ToListAsync().ConfigureAwait(false);
                }
                return existAccInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Add account information
        /// <summary>
        /// Add\Update account information into database
        /// </summary>
        /// <param name="objAccount">Account object</param>
        public void AddUpdateAccountInfo(Account objAccount)
        {
            //Create datasource object and get connection string
            DataSource ds = new DataSource();
            try
            {
                var result = ds.Db.Table<Account>().Where(c => c.emailAddress.ToLower() == objAccount.emailAddress.ToLower());
                if (result != null && result.Count() > 0)
                {
                    var acc = result.First();
                    //if account exists
                    if (acc != null)
                    {
                        //set all the new value for particular account
                        acc.port = objAccount.port;
                        acc.flags = objAccount.flags;
                        acc.emailAddress = objAccount.emailAddress;
                        acc.authUser = objAccount.authUser;
                        acc.authPass = objAccount.authPass;
                        acc.serverUrl = objAccount.serverUrl;
                        acc.policyKey = objAccount.policyKey;
                        acc.certAlias = objAccount.certAlias;
                        acc.folderSyncKey = objAccount.folderSyncKey;
                        acc.domainName = objAccount.domainName;
                        acc.protocolVersion = objAccount.protocolVersion;
                        //update account info
                        ds.Db.Update(acc);

                    }
                }
                else
                {
                    //set all the account data here
                    var newAccount = new Account()
                    {
                        port = objAccount.port,
                        flags = objAccount.flags,
                        emailAddress = objAccount.emailAddress,
                        authUser = objAccount.authUser,
                        authPass = objAccount.authPass,
                        serverUrl = objAccount.serverUrl,
                        policyKey = objAccount.policyKey,
                        certAlias = objAccount.certAlias,
                        folderSyncKey = objAccount.folderSyncKey,
                        domainName = objAccount.domainName,
                        protocolVersion = objAccount.protocolVersion
                    };

                    //insert account information into database
                    ds.Db.Insert(newAccount);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async void AddUpdateAccountInfoAsync(Account objAccount)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                var result = await ds.Db.Table<Account>().Where(c => c.emailAddress.ToLower() == objAccount.emailAddress.ToLower()).ToListAsync().ConfigureAwait(false);

                if (result != null && result.Count > 0)
                {
                    Account acc = (Account)result.Single();
                    //if account exists
                    if (acc != null)
                    {
                        //set all the new value for particular account
                        acc.port = objAccount.port;
                        acc.flags = objAccount.flags;
                        acc.emailAddress = objAccount.emailAddress;
                        acc.authUser = objAccount.authUser;
                        acc.authPass = objAccount.authPass;
                        acc.serverUrl = objAccount.serverUrl;
                        acc.policyKey = objAccount.policyKey;
                        acc.certAlias = objAccount.certAlias;
                        acc.folderSyncKey = objAccount.folderSyncKey;
                        acc.domainName = objAccount.domainName;
                        acc.protocolVersion = objAccount.protocolVersion;
                        //update account info
                        await ds.Db.UpdateAsync(acc).ConfigureAwait(false);

                    }
                }
                else
                {
                    //set all the account data here
                    var newAccount = new Account()
                    {
                        port = objAccount.port,
                        flags = objAccount.flags,
                        emailAddress = objAccount.emailAddress,
                        authUser = objAccount.authUser,
                        authPass = objAccount.authPass,
                        serverUrl = objAccount.serverUrl,
                        policyKey = objAccount.policyKey,
                        certAlias = objAccount.certAlias,
                        folderSyncKey = objAccount.folderSyncKey,
                        domainName = objAccount.domainName,
                        protocolVersion = objAccount.protocolVersion
                    };

                    //insert account information into database
                    await ds.Db.InsertAsync(newAccount).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion


        #region Delete account information
        /// <summary>
        /// Delete particular account information from database
        /// </summary>
        /// <param name="EmailId">email id</param>
        public void DeleteAccountInfo(string EmailId)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get accoutn based on email id
                var acc = ds.Db.Table<Account>().Where(c => c.emailAddress == EmailId).ToListAsync().Result.Single();
                //if data exists
                if (acc != null)
                {
                    //delete particular record from database
                    ds.Db.DeleteAsync(acc);
                }
                //dispose object
                //db.Dispose();
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async void DeleteAccountInfoAsync(string EmailId)
        {
            DataSourceAsync ds = new DataSourceAsync();
            //SQLiteAsyncConnection db = ds.Db;
            try
            {
                //get connection string and store in variable
                //var db = new SQLiteAsyncConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //get accoutn based on email id
                var acc = await ds.Db.Table<Account>().Where(c => c.emailAddress == EmailId).FirstAsync().ConfigureAwait(false);
                //if data exists
                if (acc != null)
                {
                    //delete particular record from database
                    await ds.Db.DeleteAsync(acc).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion
    }

}
