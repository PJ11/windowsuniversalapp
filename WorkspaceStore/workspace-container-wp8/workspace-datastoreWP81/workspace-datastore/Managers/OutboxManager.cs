﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using workspace_datastore.models;

/// <summary>
/// it incudes feature to add, edit ,get ,and delete outbox data into\from database.
/// </summary>
namespace workspace_datastore.Managers
{
    //*********************************************************************************************************************
    // <copyright file="OutboxManager.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>11-04-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           11-04-2014    1.0            First Version </summary>
    //*********************************************************************************************************************

    public class OutboxManager
    {
        #region Get all or particular outbox data
        /// <summary>
        /// method to get all outbox info or get outbox info based on outbox id(if id=0 then it will return all outbox info)
        /// </summary>
        /// <param name="OutboxId">Outbox id</param>
        /// <returns></returns>
        public List<Outbox> GetOutboxData(string clientId)
        {
            //create local varibale to store outbox data into list 
            List<Outbox> existOutboxData = null;
            DataSource ds = new DataSource();
            try
            {
                //check if outbox id ==0 then retrun all outbox data
                if (clientId.Length == 0)
                {
                    //get all outbox data
                    existOutboxData = ds.Db.Table<Outbox>().ToList();
                }
                else
                {
                    //get outbox data based on outbox id
                    existOutboxData = ds.Db.Table<Outbox>().Where(c => c.clientIdentifier == clientId).ToList();
                }
                return existOutboxData;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task<List<Outbox>> GetOutboxDataAsync(string clientId)
        {
            //create local varibale to store outbox data into list 
            List<Outbox> existOutboxData = null;
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //check if outbox id ==0 then retrun all outbox data
                if (clientId.Length == 0)
                {
                    //get all outbox data
                    existOutboxData = await ds.Db.Table<Outbox>().ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    //get outbox data based on outbox id
                    existOutboxData = await ds.Db.Table<Outbox>().Where(c => c.clientIdentifier == clientId).ToListAsync().ConfigureAwait(false);
                }
                return existOutboxData;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        public List<Attachment> GetOutboxItemAttachments(Outbox outMessage)
        {
            List<Attachment> attachments = new List<Attachment>();
            if (null != outMessage && outMessage.attachmentFilePathsList.Length > 0)
            {
                String[] attachmentsGuid = outMessage.attachmentFilePathsList.Split('\n', '\r');
                foreach (string attachFile in attachmentsGuid)
                {
                    if(attachFile.Length > 0)
                    {
                        Attachment att = new Attachment();
                        att.filePath = attachFile;
                        att.displayName = Path.GetFileName(att.filePath);
                        attachments.Add(att);
                    }
                }
            }
            return attachments;
        }
        #region Add outbox data
        /// <summary>
        /// Add outbox data into database
        /// </summary>
        /// <param name="ObjOutbox">Outbox object</param>
        public void AddOutboxData(Outbox ObjOutbox)
        {
            DataSource ds = new DataSource();
            try
            {
                //set all the outbox data here
                var newOutboxData = new Outbox()
                {
                    clientIdentifier = ObjOutbox.clientIdentifier,
                    account_id = ObjOutbox.account_id,
                    bodyText = ObjOutbox.bodyText,
                    creationTime = ObjOutbox.creationTime,
                    attachmentFilePathsList = ObjOutbox.attachmentFilePathsList,
                    actionType = ObjOutbox.actionType,
                    actionFolderServerId = ObjOutbox.actionFolderServerId,
                    actionMessageServerId = ObjOutbox.actionMessageServerId,
                    actionMessageServerLongId = ObjOutbox.actionMessageServerLongId,
                    message_id = ObjOutbox.message_id
                };
                //insert outbox data into database
                ds.Db.Insert(newOutboxData);
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async void AddOutboxDataAsync(Outbox ObjOutbox)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                var newOutboxData = new Outbox()
                {
                    clientIdentifier = ObjOutbox.clientIdentifier,
                    account_id = ObjOutbox.account_id,
                    bodyText = ObjOutbox.bodyText,
                    creationTime = ObjOutbox.creationTime,
                    attachmentFilePathsList = ObjOutbox.attachmentFilePathsList,
                    actionType = ObjOutbox.actionType,
                    actionFolderServerId = ObjOutbox.actionFolderServerId,
                    actionMessageServerId = ObjOutbox.actionMessageServerId,
                    actionMessageServerLongId = ObjOutbox.actionMessageServerLongId,
                    message_id = ObjOutbox.message_id
                };
                //insert outbox data into database
                await ds.Db.InsertAsync(newOutboxData).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Update outbox data
        /// <summary>
        /// Update partucluar outbox data into database
        /// </summary>
        /// <param name="ObjOutbox">Outbox object</param>
        public void UpdateOutboxData(Outbox ObjOutbox)
        {
            DataSource ds = new DataSource();
            try
            {
                //get outbox data base on outbox id
                var outboxDataToUpdate = ds.Db.Table<Outbox>().Where(c => c.clientIdentifier == ObjOutbox.clientIdentifier).Single();
                //if outbox data exist exists
                if (outboxDataToUpdate != null)
                {
                    outboxDataToUpdate.clientIdentifier = ObjOutbox.clientIdentifier;
                    outboxDataToUpdate.account_id = ObjOutbox.account_id;
                    outboxDataToUpdate.bodyText = ObjOutbox.bodyText;
                    outboxDataToUpdate.creationTime = ObjOutbox.creationTime;
                    outboxDataToUpdate.attachmentFilePathsList = ObjOutbox.attachmentFilePathsList;
                    outboxDataToUpdate.actionType = ObjOutbox.actionType;
                    outboxDataToUpdate.actionFolderServerId = ObjOutbox.actionFolderServerId;
                    outboxDataToUpdate.actionMessageServerId = ObjOutbox.actionMessageServerId;
                    outboxDataToUpdate.actionMessageServerLongId = ObjOutbox.actionMessageServerLongId;
                    outboxDataToUpdate.message_id = ObjOutbox.message_id;
                    //update outbox data
                    ds.Db.Update(outboxDataToUpdate);
                    //dispose database object
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async void UpdateOutboxDataAsync(Outbox ObjOutbox)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get outbox data base on outbox id
                var outboxDataToUpdate = await ds.Db.Table<Outbox>().Where(c => c.clientIdentifier == ObjOutbox.clientIdentifier).FirstAsync().ConfigureAwait(false);
                //if outbox data exist exists
                if (outboxDataToUpdate != null)
                {
                    outboxDataToUpdate.clientIdentifier = ObjOutbox.clientIdentifier;
                    outboxDataToUpdate.account_id = ObjOutbox.account_id;
                    outboxDataToUpdate.bodyText = ObjOutbox.bodyText;
                    outboxDataToUpdate.creationTime = ObjOutbox.creationTime;
                    outboxDataToUpdate.attachmentFilePathsList = ObjOutbox.attachmentFilePathsList;
                    outboxDataToUpdate.actionType = ObjOutbox.actionType;
                    outboxDataToUpdate.actionFolderServerId = ObjOutbox.actionFolderServerId;
                    outboxDataToUpdate.actionMessageServerId = ObjOutbox.actionMessageServerId;
                    outboxDataToUpdate.actionMessageServerLongId = ObjOutbox.actionMessageServerLongId;
                    outboxDataToUpdate.message_id = ObjOutbox.message_id;
                    //update outbox data
                    await ds.Db.UpdateAsync(outboxDataToUpdate).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Delete outbox data
        /// <summary>
        /// Delete particular outbox data from database
        /// </summary>
        /// <param name="OutboxId">Outbox id</param>
        public void DeleteOutboxData(string clientId)
        {
            DataSource ds = new DataSource();
            try
            {
                //get outbox data based on outbox id
                var outboxdata = ds.Db.Table<Outbox>().Where(c => c.clientIdentifier == clientId).Single();
                //if data exists
                if (outboxdata != null)
                {
                    //delete particular record from database
                    ds.Db.Delete(outboxdata);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        /// <summary>
        /// Delete particular outbox data from database
        /// </summary>
        /// <param name="OutboxId">Outbox id</param>
        public async void DeleteOutboxDataAsync(string clientId)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get outbox data based on outbox id
                var outboxdata = await ds.Db.Table<Outbox>().Where(c => c.clientIdentifier == clientId).FirstOrDefaultAsync().ConfigureAwait(false);
                //if data exists
                if (outboxdata != null)
                {
                    //delete particular record from database
                    await ds.Db.DeleteAsync(outboxdata).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion
    }
}
