﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using workspace_datastore.models;

/// <summary>
/// It incudes feature to add, edit ,get ,and delete instance data into\from database.
/// </summary>
namespace workspace_datastore.Managers
{
    //*********************************************************************************************************************
    // <copyright file="InstanceManager.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>11-04-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           11-04-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
  
   public class InstanceManager
   {
        #region Get instance information
       /// <summary>
        /// method to get all instance info or get instance info based on instance id(if id=0 then it will return all instances info)
        /// </summary>
        /// <param name="InstanceID">Instance id</param>
        /// <returns></returns>
       public List<Instances> GetInstanceInfo(int InstanceID)
        {
            //create local varibale to store instance info into list 
            List<Instances> existInstanceInfo = null;
            try
            {
                //get connection string and store in variable
                var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //check if instacne id ==0 then retrun all instance info
                if (InstanceID == 0)
                {
                    //get all instance info
                    existInstanceInfo = db.Table<Instances>().ToList();
                }
                else
                {
                    //get instance info based on instacne id
                    existInstanceInfo = db.Table<Instances>().Where(c => c.id == InstanceID).ToList();
                }
                //dispose object
                db.Dispose();
                //return result
                return existInstanceInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }
       #endregion

        #region Add instance information
        /// <summary>
        /// Add instance information into database
        /// </summary>
        /// <param name="ObjInstance">Instance object</param>
        public void AddInstanceInfo(Instances ObjInstance)
        {
            try
            {
                //get connection string and store in variable
                var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //set all the instance data here
                var newInstance = new Instances()
                {
                    event_id = ObjInstance.event_id,
                    begin = ObjInstance.begin,
                    end = ObjInstance.end,
                    startDay = ObjInstance.startDay,
                    endDay = ObjInstance.endDay,
                    startMinute = ObjInstance.startMinute,
                    endMinute = ObjInstance.endMinute
                };
                //insert instance information into database
                db.Insert(newInstance);
                //dispose object
                db.Dispose();
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }
        #endregion

        #region Update instance information
        /// <summary>
        /// Update partucluar instance information into database
        /// </summary>
        /// <param name="ObjInstance">Instance object</param>
        public void UpdateInstanceInfo(Instances ObjInstance)
        {
            try
            {
                //get connection string and store in variable
                var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //get instance info base on instance id
                var instance = db.Table<Instances>().Where(c => c.id == ObjInstance.id).Single();
                //if instance exists
                if (instance != null)
                {
                    instance.event_id = ObjInstance.event_id;
                    instance.begin = ObjInstance.begin;
                    instance.end = ObjInstance.end;
                    instance.startDay = ObjInstance.startDay;
                    instance.endDay = ObjInstance.endDay;
                    instance.startMinute = ObjInstance.startMinute;
                    instance.endMinute = ObjInstance.endMinute;                    
                    //update instance info
                    db.Update(instance);
                    //dispose database object
                    db.Dispose();
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }
        #endregion

        #region Delete instance information
        /// <summary>
        /// Delete particular instance information from database
        /// </summary>
        /// <param name="InstacnceId">Instacnce id</param>
        public void DeleteInstanceInfo(int InstacnceId)
        {

            try
            {
                //get connection string and store in variable
                var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //get instance based on instance id
                var instance = db.Table<Instances>().Where(c => c.id == InstacnceId).Single();
                //if data exists
                if (instance != null)
                {
                    //delete particular record from database
                    db.Delete(instance);
                }
                //dispose dataabse object
                db.Dispose();
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }
        #endregion
   }
}
