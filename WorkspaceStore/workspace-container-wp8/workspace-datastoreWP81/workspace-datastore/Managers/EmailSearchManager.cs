﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using workspace_datastore.Models;

namespace workspace_datastore.Managers
{
   public class EmailSearchManager
    {
        public async Task AddOrUpdateEmailSearchContactInfoAsync(SQLiteAsyncConnection conn,EmailSearch ObjEmail)
        {
                EmailSearch emailContact = null;
                try
                {
                  var  getExistingContact =await conn.Table<EmailSearch>().Where(c => c.email == ObjEmail.email).ToListAsync().ConfigureAwait(false);
                  if (getExistingContact == null || getExistingContact.Count == 0)
                  {
                      var emailSearch = new EmailSearch()
                      {
                          _id = ObjEmail._id,
                          avatar_link = ObjEmail.avatar_link,
                          datasource_type = ObjEmail.datasource_type,
                          display_name = ObjEmail.display_name,
                          email = ObjEmail.email,
                          first_name = ObjEmail.first_name,
                          label = ObjEmail.label,
                          last_name = ObjEmail.last_name,
                          linker_id = ObjEmail.linker_id,
                          primary_link_id = ObjEmail.primary_link_id,
                          recent_contact_flag = ObjEmail.recent_contact_flag,
                          recent_timestamp = ObjEmail.recent_timestamp,
                      };
                      int result = await conn.InsertAsync(emailSearch).ConfigureAwait(false);
                  }
                  else
                  {
                      emailContact = getExistingContact.FirstOrDefault();
                      emailContact._id = ObjEmail._id;
                      emailContact.avatar_link = ObjEmail.avatar_link;
                      emailContact.datasource_type = ObjEmail.datasource_type;
                      emailContact.display_name = ObjEmail.display_name;
                      emailContact.email = ObjEmail.email;
                      emailContact.first_name = ObjEmail.first_name;
                      emailContact.label = ObjEmail.label;
                      emailContact.last_name = ObjEmail.last_name;
                      emailContact.linker_id = ObjEmail.linker_id;
                      emailContact.primary_link_id = ObjEmail.primary_link_id;
                      emailContact.recent_contact_flag = ObjEmail.recent_contact_flag;
                      emailContact.recent_timestamp = ObjEmail.recent_timestamp;
                      await conn.UpdateAsync(emailContact).ConfigureAwait(false);
                  }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                conn = null;
            }
        }

         public List<EmailSearch> GetRecentEmail()
         {
             DataSourceAsync ds = new DataSourceAsync();
             try
             {
                 var result = ds.Db.Table<EmailSearch>().ToListAsync().Result;
                 return result;
             }
             catch (Exception ex)
             {
                 //throw exception
                 throw ex;
             }
            finally
            {
                ds.Dispose();
            }
         }
    }
}
