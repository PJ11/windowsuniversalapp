﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using workspace_datastore.models;

/// <summary>
/// it incudes feature to add, edit ,get ,and delete folder info into\from database.
/// </summary>
namespace workspace_datastore.Managers
{
    //*********************************************************************************************************************
    // <copyright file="FolderManager.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>11-04-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           11-04-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
   public class FolderManager
   {

        #region Get folder information
       /// <summary>
        /// method to get all folder info or get folder info based on folder id(if id=0 then it will return all folders info)
        /// </summary>
        /// <param name="FolderID">Folder id</param>
        /// <returns></returns>
        public async Task<List<Folder>> GetFolderInfo(int FolderID)
        {
            //create local varibale to store folder info into list 
            List<Folder> existFolderInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            //var db=ds.Db;
            try
            {
                //get connection string and store in variable
                //var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //check if folder id ==0 then retrun all folder info
                if (FolderID == 0)
                {
                    //get all folder info
                    existFolderInfo = await ds.Db.Table<Folder>().ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    //get folder info based on folder id
                    string strFolderId = FolderID.ToString();
                    existFolderInfo = await ds.Db.Table<Folder>().Where(c => c.serverIdentifier == strFolderId).ToListAsync().ConfigureAwait(false);
                }
                return existFolderInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally 
            { 
                ds.Dispose();
            }
        }

        public async Task<List<Folder>> GetFolderInfoAsync(int FolderID)
        {
            //create local varibale to store folder info into list 
            List<Folder> existFolderInfo = null;
             DataSourceAsync ds = new DataSourceAsync();
               // SQLiteAsyncConnection db = ds.Db;
            try
            {
                //get connection string and store in variable
                //var db = new SQLiteAsyncConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
               
                //check if folder id ==0 then retrun all folder info
                if (FolderID == 0)
                {
                    //get all folder info
                    existFolderInfo = await ds.Db.Table<Folder>().ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    //get folder info based on folder id
                    string strFolderId = FolderID.ToString();
                    existFolderInfo = await ds.Db.Table<Folder>().Where(c => c.serverIdentifier == strFolderId).ToListAsync().ConfigureAwait(false);
                }
                return existFolderInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally 
            { 
                ds.Dispose();
            }
        }

       //To get exact folder with help of name and parent folder ID
        public async Task<Folder> GetFolderInfoAsync(string name,int parentFolderID)
        {
            //create local varibale to store folder info into list 
            List<Folder> existFolderInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            
            try
            {
                
                    //get all folder info
                existFolderInfo = await ds.Db.Table<Folder>().ToListAsync().ConfigureAwait(false);
                if (existFolderInfo != null)
                {
                    var folderDB = existFolderInfo.SingleOrDefault(x => x.displayName == name && x.parentFolder_id == parentFolderID);
                    return folderDB;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
 
        }

        public int GetFolderIdentifierBasedOnFolderName(string FolderName)
        {
            //create local varibale to store folder info into list 
            int folderId = -1;
            //get connection string and store in variable
            //var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
            DataSource ds = new DataSource();
            //SQLiteAsyncConnection db = ds.Db;
            try
            {
                //get connection string and store in variable
               // var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //check if folder id ==0 then retrun all folder info
                if (FolderName.Length > 0)
                {
                    //get all folder info
                    List<Folder> fldrs = ds.Db.Table<Folder>().Where(c => c.displayName == FolderName).ToList();
                    if(null != fldrs && fldrs.Count > 0)
                    {
                        folderId = Convert.ToInt32(fldrs[0].serverIdentifier);
                    }
                }
                //dispose object
                //db.Dispose();
                //return result
                return folderId;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally { ds.Dispose();
            }
        }

       public async Task<int> GetFolderIdentifierBasedOnFolderNameAsync(string FolderName, int parentfolderID = 0)
       {
           DataSourceAsync ds = new DataSourceAsync();
          // SQLiteAsyncConnection db = ds.Db;
           //create local varibale to store folder info into list 
           int folderId = -1;
           try
           {
               //get connection string and store in variable
               //var db = new SQLiteAsyncConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
               //check if folder id ==0 then retrun all folder info
               if (FolderName.Length > 0)
               {
                   //get all folder info
                   List<Folder> fldrs = await ds.Db.Table<Folder>().Where(c => c.displayName == FolderName && c.parentFolder_id == parentfolderID).ToListAsync();//.ConfigureAwait(false);
                   if(null != fldrs && fldrs.Count > 0)
                   {
                       folderId = Convert.ToInt32(fldrs[0].serverIdentifier);
                   }
               }
               return folderId;
           }
           catch (Exception ex)
           {
               //throw exception
               throw ex;
           }
           finally 
           { 
               ds.Dispose();
           }
       }

	   //This folder information is not something which we get from active Sync. We will just create it for 
	   //getting folder information in next sync and getting correct sync information. 0 server identifier we never get from server.
	   public async Task<Folder> GetBaseRootFolder()
	   {
		   //create local varibale to store folder info into list 
		   List<Folder> existFolderInfo = null;
		   DataSourceAsync ds = new DataSourceAsync();

		   try
		   {

			   //get all folder info
			   existFolderInfo = await ds.Db.Table<Folder>().ToListAsync().ConfigureAwait(false);
			   if (existFolderInfo != null)
			   {
				   
				   var folderDB = existFolderInfo.SingleOrDefault(x => x.displayName == "RootFolder" && x.serverIdentifier == "0");
				   return folderDB;
			   }
			   else
				   return null;
		   }
		   catch (Exception ex)
		   {
			   //throw exception
			   throw ex;
		   }
		   finally
		   {
			   ds.Dispose();
		   }
 

 
	   }
        #endregion

        #region Add folder inforamtion
        /// <summary>
        /// Add folder information into database
        /// </summary>
        /// <param name="ObjFolder">Folder object</param>
        public void AddFolderInfo(SQLiteConnection conn, Folder ObjFolder)
        {
            try
            {
               
                //set all the folder data here
                var newfolder = new Folder()
                {
                    displayName = ObjFolder.displayName,
                    serverIdentifier = ObjFolder.serverIdentifier,
                    account_id = ObjFolder.account_id,
                    syncKey = ObjFolder.syncKey,
                    syncTime = ObjFolder.syncTime,
                    folderType = ObjFolder.folderType,
                    folderClass = ObjFolder.folderClass,
                    parentFolder_id = ObjFolder.parentFolder_id,
                    syncState = ObjFolder.syncState
                };
                //insert folder information into database
                conn.Insert(newfolder);               
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }

        public async void AddFolderInfoAsync(Folder ObjFolder)
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //set all the folder data here
                var newfolder = new Folder()
                {
                    displayName = ObjFolder.displayName,
                    serverIdentifier = ObjFolder.serverIdentifier,
                    account_id = ObjFolder.account_id,
                    syncKey = ObjFolder.syncKey,
                    syncTime = ObjFolder.syncTime,
                    folderType = ObjFolder.folderType,
                    folderClass = ObjFolder.folderClass,
                    parentFolder_id = ObjFolder.parentFolder_id,
                    syncState = ObjFolder.syncState
                };
                //insert folder information into database
                /*await*/ ds.Db.InsertAsync(newfolder).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Update event information
        /// <summary>
        /// Update partucluar folder information into database
        /// </summary>
        /// <param name="ObjFolder">Folder object</param>
        public void UpdateFolderInfo(Folder ObjFolder)
        {
            //get connection string and store in variable
            //var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
            DataSource ds = new DataSource();
            //SQLiteAsyncConnection db = ds.Db;
            try
            {
                //get connection string and store in variable
               // var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //get folder info base on folder id
                var folder = ds.Db.Table<Folder>().Where(c => c.id == ObjFolder.id).ToList().Single();
                //if folder exists
                if (folder != null)
                {
                    folder.displayName = ObjFolder.displayName;
                    folder.serverIdentifier = ObjFolder.serverIdentifier;
                    folder.account_id = ObjFolder.account_id;
                    folder.syncKey = ObjFolder.syncKey;
                    folder.syncTime = ObjFolder.syncTime;
                    folder.folderType = ObjFolder.folderType;
                    folder.folderClass = ObjFolder.folderClass;
                    folder.parentFolder_id = ObjFolder.parentFolder_id;
                    folder.syncState = ObjFolder.syncState;
                    //update folder info
                    ds.Db.Update(folder);
                    //dispose database object
                   // db.Dispose();
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally 
            { 
                ds.Dispose();
            }
        }

        public async Task UpdateFolderInfoAsync(Folder ObjFolder)
        {
            //get connection string and store in variable
            //var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
            DataSourceAsync ds = new DataSourceAsync();
            //SQLiteAsyncConnection db = ds.Db;
            try
            {
                //get connection string and store in variable
                // var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //get folder info base on folder id
                var folder = await ds.Db.Table<Folder>().Where(c =>c.serverIdentifier == ObjFolder.serverIdentifier ).ElementAtAsync(0).ConfigureAwait(false);
                //if folder exists
                if (folder != null)
                {
                    folder.displayName = ObjFolder.displayName;
                    folder.serverIdentifier = ObjFolder.serverIdentifier;
                    folder.account_id = ObjFolder.account_id;
                    folder.syncKey = ObjFolder.syncKey;
                    folder.syncTime = ObjFolder.syncTime;
                    folder.folderType = ObjFolder.folderType;
                    folder.folderClass = ObjFolder.folderClass;
                    folder.parentFolder_id = ObjFolder.parentFolder_id;
                    folder.syncState = ObjFolder.syncState;
                    //update folder info
                    await ds.Db.UpdateAsync(folder).ConfigureAwait(false);
                    //dispose database object
                    // db.Dispose();
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Delete folder information
        /// <summary>
        /// Delete particular folder information from database
        /// </summary>
        /// <param name="FolderID">Folder id</param>
        public async Task DeleteFolderInfoAsync(string serverID)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();   
            try
            {
                //get connection string and store in variable
               // var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //get folder based on folder id
                //var folder = ds.Db.Table<Folder>().Where(c => c.serverIdentifier == serverID).FirstAsync();

                var folder = await ds.Db.Table<Folder>().Where(c => c.serverIdentifier == serverID).ElementAtAsync(0).ConfigureAwait(false);
                //if data exists
                if (folder != null)
                {
                    //delete particular record from database
                    ds.Db.DeleteAsync(folder);
                }
                //dispose dataabse object
                //db.Dispose();
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally 
            { 
                ds.Dispose();
            }
        }
        #endregion
   }
}
