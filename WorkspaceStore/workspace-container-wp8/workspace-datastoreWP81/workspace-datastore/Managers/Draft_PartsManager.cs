﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using workspace_datastore.models;

/// <summary>
/// it incudes feature to add, edit ,get ,and delete draft parts data into\from database.
/// </summary>
namespace workspace_datastore.Managers
{

    //*********************************************************************************************************************
    // <copyright file="Draft_PartsManager.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>14-04-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           14-04-2014    1.0            First Version </summary>
    //*********************************************************************************************************************

    public class Draft_PartsManager
    {
        #region Get draft information
        /// <summary>
        /// method to get all draft parts info or get draft parts info based on draft part id(if id=0 then it will return all draft parts info)
        /// </summary>
        /// <param name="DraftPartId">Draft Part id</param>
        /// <returns></returns>
        public List<Draft_Parts> GetDraftPartInfo(int DraftPartId)
        {
            //create local varibale to store draft part info into list 
            DataSource ds = new DataSource();
            List<Draft_Parts> existDraftPartsInfo = null;
            try
            {
                //check if draft part id ==0 then retrun all draft part info
                if (DraftPartId == 0)
                {
                    //get all draft parts info
                    existDraftPartsInfo = ds.Db.Table<Draft_Parts>().ToList();
                }
                else
                {
                    //get draft parts info based on draft part id
                    existDraftPartsInfo = ds.Db.Table<Draft_Parts>().Where(c => c.id == DraftPartId).ToList();
                }
                return existDraftPartsInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Get draft information
        /// <summary>
        /// method to get all draft parts info or get draft parts info based on draft part id(if id=0 then it will return all draft parts info)
        /// </summary>
        /// <param name="DraftPartId">Draft Part id</param>
        /// <returns></returns>
        public async Task<List<Draft_Parts>> GetDraftAttachmentInfo(int msgid)
        {
            //create local varibale to store draft part info into list 
            List<Draft_Parts> existDraftPartsInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get draft parts info based on draft part id
                existDraftPartsInfo = await ds.Db.Table<Draft_Parts>().Where(c => c.message_id == msgid && c.partType == 2).ToListAsync();

                return existDraftPartsInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Add draft information
        /// <summary>
        /// Add draft parts information into database
        /// </summary>
        /// <param name="ObjDraftParts">DraftParts object</param>
        public void AddDraftPartsInfo(SQLiteAsyncConnection db, Draft_Parts ObjDraftParts)
        {
            try
            {

                //set all the draft parts data here
                var newDraftParts = new Draft_Parts()
                {
                    message_id = ObjDraftParts.message_id,
                    partType = ObjDraftParts.partType,
                    contentType = ObjDraftParts.contentType,
                    content = ObjDraftParts.content,
                    extData = ObjDraftParts.extData
                };
                //insert draft parts information into database
                db.InsertAsync(newDraftParts);

            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }

        }
        #endregion

        #region Update draft information
        /// <summary>
        /// Update partucluar draft parts information into database
        /// </summary>
        /// <param name="ObjDraftParts">DraftParts object</param>
        public void UpdateDraftPartsInfo(Draft_Parts ObjDraftParts)
        {
            DataSource ds = new DataSource();
            try
            {
                //get draft parts info base on account id
                var draftpart = ds.Db.Table<Draft_Parts>().Where(c => c.id == ObjDraftParts.id).Single();
                //if draft part already exists
                if (draftpart != null)
                {
                    draftpart.message_id = ObjDraftParts.message_id;
                    draftpart.partType = ObjDraftParts.partType;
                    draftpart.contentType = ObjDraftParts.contentType;
                    draftpart.content = ObjDraftParts.content;
                    draftpart.extData = ObjDraftParts.extData;
                    //update draft parts info
                    ds.Db.Update(draftpart);
                    //dispose database object
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Delete draft information
        /// <summary>
        /// Delete particular draft parts information from database
        /// </summary>
        /// <param name="DraftPartId">draft part id</param>
        public async Task DeleteDraftPartsInfo(int DraftMsgId)
        {
            //get connection string and store in variable                
            DataSourceAsync ds = new DataSourceAsync();
            try
            {

                //get data based on Draft Part Id 
                var draftpart = await ds.Db.Table<Draft_Parts>().Where(c => c.message_id == DraftMsgId).ToListAsync().ConfigureAwait(false);
                //if data exists
                if (draftpart != null)
                {
                    foreach (Draft_Parts draft in draftpart)
                    {
                        //delete particular record from database
                        await ds.Db.DeleteAsync(draft).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                //dispose dataabse object
                ds.Dispose();
            }
        }
        #endregion
    }
}
