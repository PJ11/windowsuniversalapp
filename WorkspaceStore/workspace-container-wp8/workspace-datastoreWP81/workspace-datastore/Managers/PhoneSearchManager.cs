﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using workspace_datastore.Models;

namespace workspace_datastore.Managers
{
    class PhoneSearchManager
    {
        public async Task AddOrUpdatePhoneSearchContactInfoAsync(SQLiteAsyncConnection db, PhoneSearch ObjPhone)
        {            
            
                PhoneSearch phoneContact = null;
                try
                {
                    var getPhoneContact =await db.Table<PhoneSearch>().Where(c => c.linker_id == ObjPhone.linker_id).ToListAsync().ConfigureAwait(false);
                    if (getPhoneContact == null || getPhoneContact.Count == 0)
                    {
                        var phoneSearch = new PhoneSearch()
                        {
                            _id = ObjPhone._id,
                            avatar_link = ObjPhone.avatar_link,
                            datasource_type = ObjPhone.datasource_type,
                            display_name = ObjPhone.display_name,
                            first_name = ObjPhone.first_name,
                            label = ObjPhone.label,
                            last_name = ObjPhone.last_name,
                            linker_id = ObjPhone.linker_id,
                            phone_number = ObjPhone.phone_number,
                            phone_number_plain_text = ObjPhone.phone_number_plain_text,
                            primary_link_id = ObjPhone.primary_link_id,
                            recent_contact_flag = ObjPhone.recent_contact_flag,
                            recent_timestamp = ObjPhone.recent_timestamp,
                        };
                        int result = await db.InsertAsync(phoneSearch).ConfigureAwait(false);
                    }

                    else
                    {
                        phoneContact = getPhoneContact.FirstOrDefault();
                        phoneContact._id = ObjPhone._id;
                        phoneContact.avatar_link = ObjPhone.avatar_link;
                        phoneContact.datasource_type = ObjPhone.datasource_type;
                        phoneContact.display_name = ObjPhone.display_name;
                        phoneContact.first_name = ObjPhone.first_name;
                        phoneContact.label = ObjPhone.label;
                        phoneContact.last_name = ObjPhone.last_name;
                        phoneContact.linker_id = ObjPhone.linker_id;
                        phoneContact.phone_number = ObjPhone.phone_number;
                        phoneContact.phone_number_plain_text = ObjPhone.phone_number_plain_text;
                        phoneContact.primary_link_id = ObjPhone.primary_link_id;
                        phoneContact.recent_contact_flag = ObjPhone.recent_contact_flag;
                        phoneContact.recent_timestamp = ObjPhone.recent_timestamp;
                        await db.UpdateAsync(phoneContact).ConfigureAwait(false);
                    }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }

            finally
            {
                db = null;
            }
        }

    }
}
