﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using workspace_datastore.models;

/// <summary>
/// Manipulate attachment information into database. it incudes feature to add, edit ,get ,and delete attachment data into\from database.
/// </summary>
namespace workspace_datastore.Managers
{

    //*********************************************************************************************************************
    // <copyright file="AttachmentManager.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>11-04-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           11-04-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
    public class AttachmentManager
    {
        #region Get all attachment data
        /// <summary>
        /// method to get all attachment info or get attachment info based on attachment id(if id=0 then it will return all attachments info)
        /// </summary>
        /// <param name="AccId">Attachment id</param>
        /// <returns></returns>
        public List<Attachment> GetAttachmentInfo(Guid AttachmentId)
        {
            //create local varibale to store attachment info into list 
            List<Attachment> existAttachmentInfo = null;
            DataSource ds = new DataSource();
            try
            {
                //check if attachment id ==0 then retrun all attachment info
                if (AttachmentId == Guid.Empty)
                {
                    //get all attachment info
                    existAttachmentInfo = ds.Db.Table<Attachment>().ToList();
                }
                else
                {
                    //get attachment info based on attachment id
                    existAttachmentInfo = ds.Db.Table<Attachment>().Where(c => c.id == AttachmentId).ToList();
                }
                return existAttachmentInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion end

        #region Add attachment information
        /// <summary>
        /// Add attachment information into database
        /// </summary>
        /// <param name="objAttachment">attachment object</param>
        public void AddattachmentInfo(Attachment objAttachment)
        {
            //Create datasource object and get connection string
            DataSource ds = new DataSource();
            try
            {
                //set all the attachment data here
                var newAttachement = new Attachment()
                {
                    displayName = objAttachment.displayName,
                    fileReference = objAttachment.fileReference,
                    filePath = objAttachment.filePath,
                    method = objAttachment.method,
                    estimatedSize = objAttachment.estimatedSize,
                    contentIdentifier = objAttachment.contentIdentifier,
                    isInline = objAttachment.isInline,
                    message_id = objAttachment.message_id,
                    folder_id = objAttachment.folder_id,
                    contentType = objAttachment.contentType
                };
                //insert attachment information into database
                ds.Db.Insert(newAttachement);

            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task AddAttachmentInfoAsync(SQLiteAsyncConnection db, Attachment objAttachment)
        {
            try
            {
                await db.InsertAsync(objAttachment).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }
        #endregion

        public void AddAllAttachfo(SQLiteConnection con, IList<Attachment> messages)
        {
            try
            {
                //get connection string and store in variable
                // var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                con.InsertAll(messages);
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }

        public async Task AddAllAttachInfoAsync(IList<Attachment> attachments)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                if (attachments.Count > 0)
                {
                    await ds.Db.RunInTransactionAsync(async (SQLiteAsyncConnection connection) =>
                    {
                        await connection.InsertAllAsync(attachments).ConfigureAwait(false);
                    });
                }
            }
            catch (Exception)
            { }
            finally
            {
                ds.Dispose();
            }
        }

        public List<Attachment> GetAttachmentInfoWithMailID(int MailID, int folderId)
        {
            //create local varibale to store attachment info into list 
            List<Attachment> existAttachmentInfo = null;
            //get connection string and store in variable
            //var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
            DataSource ds = new DataSource();
            try
            {
                //get connection string and store in variable
                //var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //check if attachment id ==0 then retrun all attachment info
                if (MailID == 0)
                {
                    //get all attachment info
                    existAttachmentInfo = ds.Db.Table<Attachment>().ToList();
                }
                else
                {
                    //get attachment info based on attachment id
                    existAttachmentInfo = ds.Db.Table<Attachment>().Where(c => c.message_id == MailID && c.folder_id == folderId).ToList();
                }
                //dispose object
                // db.Dispose();
                //return result
                return existAttachmentInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public List<Attachment> GetAttachmentInfoWithMailIDAsync(int MailID, int folderId)
        {
            //create local varibale to store attachment info into list 
            List<Attachment> existAttachmentInfo = null;
            //get connection string and store in variable
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get connection string and store in variable
                //var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //check if attachment id ==0 then retrun all attachment info
                if (MailID == 0)
                {
                    //get all attachment info
                    existAttachmentInfo = ds.Db.Table<Attachment>().ToListAsync().Result;
                }
                else
                {
                    //get attachment info based on attachment id
                    existAttachmentInfo = ds.Db.Table<Attachment>().Where(c => c.message_id == MailID && c.folder_id == folderId).ToListAsync().Result;
                }
                return existAttachmentInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        #region Update particular attachment information
        /// <summary>
        /// Update partucluar attachment information into database
        /// </summary>
        /// <param name="objAttachment">Attachment object</param>
        public void UpdateAttachmentInfo(Attachment objAttachment)
        {
            DataSource ds = new DataSource();
            try
            {
                //get connection string and store in variable
                //get attachment info base on attachment id
                var attachement = ds.Db.Table<Attachment>().Where(c => c.id == objAttachment.id).Single();
                //if attachment exists
                if (attachement != null)
                {
                    attachement.displayName = objAttachment.displayName;
                    attachement.fileReference = objAttachment.fileReference;
                    attachement.filePath = objAttachment.filePath;
                    attachement.method = objAttachment.method;
                    attachement.estimatedSize = objAttachment.estimatedSize;
                    attachement.contentIdentifier = objAttachment.contentIdentifier;
                    attachement.isInline = objAttachment.isInline;
                    attachement.message_id = objAttachment.message_id;
                    attachement.folder_id = objAttachment.folder_id;
                    attachement.contentType = objAttachment.contentType;
                    //update attachment info
                    ds.Db.Update(attachement);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Delete attachment information
        /// <summary>
        /// Delete particular attachment information from database
        /// </summary>
        /// <param name="AccId">attachment id</param>
        public void DeleteAttachmentInfo(Guid AttachmentId)
        {
            //Create datasource object and get connection string
            DataSource ds = new DataSource();
            try
            {
                //get attachment based on attachment id
                var attachement = ds.Db.Table<Attachment>().Where(c => c.id == AttachmentId).First();
                //if data exists
                if (attachement != null)
                {
                    //delete particular record from database
                    ds.Db.Delete(attachement);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async void DeleteAttachmentInfoAsync(Guid AttachmentId)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get attachment based on attachment id
                var attachement = await ds.Db.Table<Attachment>().Where(c => c.id == AttachmentId).FirstAsync().ConfigureAwait(false);
                //if data exists
                if (attachement != null)
                {
                    //delete particular record from database
                    await ds.Db.DeleteAsync(attachement);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public void DeleteAttachmentInfoWithMessageId(int messageId)
        {
            DataSource ds = new DataSource();
            try
            {
                //get attachment based on attachment id
                var attachments = ds.Db.Table<Attachment>().Where(c => c.message_id == messageId).ToArray();
                if (attachments != null)
                {
                    foreach (Attachment attachment in attachments)
                    {
                        var attachement = ds.Db.Table<Attachment>().Where(c => c.id == attachment.id);
                        //if data exists
                        if (attachement != null)
                        {
                            //delete particular record from database
                            ds.Db.Delete(attachement);
                        }
                    }
                }
                //dispose dataabse object
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
       
        public async Task DeleteAttachmentInfoWithMessageIdAsync(SQLiteAsyncConnection con,int folderID,int messageId)
        {
           // DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get attachment based on attachment id
                var attachments = await con.Table<Attachment>().Where(c => c.message_id == messageId && c.folder_id == folderID).ToListAsync().ConfigureAwait(false);
                if (attachments != null)
                {
                    foreach (Attachment attachment in attachments)
                    {                       
                        //if data exists
                        if (attachment != null && attachment.id != null)
                        {
                            //delete particular record from database
                            await con.DeleteAsync(attachment).ConfigureAwait(false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //throw exception
                //throw ex;
            }
            finally
            {
                //ds.Dispose();
            }
        }
        #endregion
    }
}
