﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SQLite;
using workspace_datastore.models;

namespace workspace_datastore.Managers
{
    /// <summary>
    /// It includes feature to get and delete recent file from database.
    /// </summary>
    public class RecentFileManager
    {
        #region Get Recent files

        public async Task<List<RecentFile>> GetRecentFiles()
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                var result = await ds.Db.Table<RecentFile>().OrderByDescending(m => m.recent_timestamp).Take(10).ToListAsync().ConfigureAwait(false);
                return result;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        #endregion

        #region Delete Recent File

        /// <summary>
        /// Delete particular recent file information from database
        /// </summary>
        public async void DeleteRecentFile(string fileName, string filePath)
        {
            //Create datasource object and get connection string
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                var recentFile =await ds.Db.Table<RecentFile>().Where(c => c.name == fileName && c.path == filePath).FirstOrDefaultAsync();
                //if data exists
                if (recentFile != null)
                {
                    //delete particular record from database
                   await ds.Db.DeleteAsync(recentFile);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        #endregion
    }
}