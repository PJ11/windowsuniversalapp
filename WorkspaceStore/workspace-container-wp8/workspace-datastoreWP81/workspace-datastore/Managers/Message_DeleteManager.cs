﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using workspace_datastore.models;

/// <summary>
/// It incudes feature to add, edit ,get ,and deleted message into\from database.
/// </summary>
namespace workspace_datastore.Managers
{
    //*********************************************************************************************************************
    // <copyright file="Message_DeleteManager.cs" company="Dell Inc">
    // Copyright (c) 2014 All Right Reserved
    // </copyright>
    // <author>Dhruvaraj Jaiswal</author>
    // <email>dhruvaraj_jaiswal@dell.com</email>
    // <date>15-04-2014</date>
    // <summary>AUTHOR ---      DATE ---      VERSION NO --- REVISION HISTORY. </summary>
    // <summary>Dhruv           15-04-2014    1.0            First Version </summary>
    //*********************************************************************************************************************
 
    public class Message_DeleteManager
    {
        #region Get all deleted or particular message
        /// <summary>
        /// method to get all deleted message info or get deleted message info based on deleted message id(if id=0 then it will return all deleted message info)
        /// </summary>
        /// <param name="DeletedMsgId">DeletedMsg id</param>
        /// <returns></returns>
        public List<Message_Deletes> GetDeletedMsgInfo(int DeletedMsgId)
        {
            //create local varibale to store deleted message info into list 
            List<Message_Deletes> existDeletedMsgInfo = null;
            try
            {
                //get connection string and store in variable
                var db = new SQLiteConnection(ConstantData.DBPath/*, ConstantData.DBPwd*/);
                //check if deleted message id ==0 then retrun all deleted message info
                if (DeletedMsgId == 0)
                {
                    //get all deleted message info
                    existDeletedMsgInfo = db.Table<Message_Deletes>().ToList();
                }
                else
                {
                    //get deleted message info based on deleted message id
                    existDeletedMsgInfo = db.Table<Message_Deletes>().Where(c => c.id == DeletedMsgId).ToList();
                }
                //dispose object
                db.Dispose();
                //return result
                return existDeletedMsgInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }

        public async Task<List<Message_Deletes>> GetDeletedMsgInfoAsync(int DeletedMsgId)
        {
            //create local varibale to store deleted message info into list 
            List<Message_Deletes> existDeletedMsgInfo = null;
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //check if deleted message id ==0 then retrun all deleted message info
                if (DeletedMsgId == 0)
                {
                    //get all deleted message info
                    existDeletedMsgInfo = await ds.Db.Table<Message_Deletes>().ToListAsync().ConfigureAwait(false);
                }
                else
                {
                    //get deleted message info based on deleted message id
                    existDeletedMsgInfo = await ds.Db.Table<Message_Deletes>().Where(c => c.id == DeletedMsgId).ToListAsync();
                }
                return existDeletedMsgInfo;
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally 
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Add/Update deleted message
        /// <summary>
        /// Add\Update deleted message information into database
        /// </summary>
        /// <param name="ObjDeletedMsg">DeletedMsg object</param>
        public void AddUpdateDeletedMsgInfo(Message_Deletes ObjDeletedMsg)
        {
            //get connection string and store in variable
            DataSource ds = new DataSource();
            try
            {
                //get deleted message info base on deleted message id
                var existingDeletedMsg = ds.Db.Table<Message_Deletes>().Where(c => c.id == ObjDeletedMsg.id).Single();
                //check if data already exists then update else insert
                if (existingDeletedMsg != null)
                {
                    existingDeletedMsg.serverIdentifier = ObjDeletedMsg.serverIdentifier;
                    existingDeletedMsg.collectionId = ObjDeletedMsg.collectionId;
                    existingDeletedMsg.folder_id = ObjDeletedMsg.folder_id;
                    existingDeletedMsg.account_id = ObjDeletedMsg.account_id;
                    //update deleted message info
                    ds.Db.Update(existingDeletedMsg);
                }
                else
                {
                    //set all the deleted message info here
                    var newDeletedMsg = new Message_Deletes()
                    {
                        serverIdentifier = ObjDeletedMsg.serverIdentifier,
                        collectionId = ObjDeletedMsg.collectionId,
                        folder_id = ObjDeletedMsg.folder_id,
                        account_id=ObjDeletedMsg.account_id
                    };
                    //insert deleted message information into database
                    ds.Db.Insert(newDeletedMsg);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async Task AddUpdateDeletedMsgInfoAsync(Message_Deletes ObjDeletedMsg)
        {
            //get connection string and store in variable
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                //get deleted message info base on deleted message id
                var existingDeletedMsg = await ds.Db.Table<Message_Deletes>().Where(c => c.id == ObjDeletedMsg.id).FirstOrDefaultAsync().ConfigureAwait(false);
                //check if data already exists then update else insert
                if (existingDeletedMsg != null)
                {
                    existingDeletedMsg.serverIdentifier = ObjDeletedMsg.serverIdentifier;
                    existingDeletedMsg.collectionId = ObjDeletedMsg.collectionId;
                    existingDeletedMsg.folder_id = ObjDeletedMsg.folder_id;
                    existingDeletedMsg.account_id = ObjDeletedMsg.account_id;
                    //update deleted message info
                    await ds.Db.UpdateAsync(existingDeletedMsg).ConfigureAwait(false);
                }
                else
                {
                    //set all the deleted message info here
                    var newDeletedMsg = new Message_Deletes()
                    {
                        serverIdentifier = ObjDeletedMsg.serverIdentifier,
                        collectionId = ObjDeletedMsg.collectionId,
                        folder_id = ObjDeletedMsg.folder_id,
                        account_id = ObjDeletedMsg.account_id
                    };
                    //insert deleted message information into database
                    await ds.Db.InsertAsync(newDeletedMsg).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion

        #region Delete deleted message
        /// <summary>
        /// Delete particular deleted message information from database
        /// </summary>
        /// <param name="DeletedMsgId">DeletedMsg id</param>
        public void DeleteDeletedMsgInfo(int DeletedMsgId)
        {
            DataSource ds = new DataSource();
            try
            {
                //get deleted message info based on deleted message id
                var deletedMSG = ds.Db.Table<Message_Deletes>().Where(c => c.id == DeletedMsgId).Single();
                //if data exists
                if (deletedMSG != null)
                {
                    //delete particular record from database
                    ds.Db.Delete(deletedMSG);
                }
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public async void DeleteAllToDeleteMessagesAsync()
        {
            DataSourceAsync ds = new DataSourceAsync();
            try
            {
                List<Message_Deletes> deletes = await ds.Db.Table<Message_Deletes>().ToListAsync().ConfigureAwait(false);
                if(null != deletes && deletes.Count > 0)
                {
                    foreach (Message_Deletes delMsg in deletes)
                    {
                        await ds.Db.DeleteAsync(delMsg).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
            }
        }
        #endregion
    }
}
