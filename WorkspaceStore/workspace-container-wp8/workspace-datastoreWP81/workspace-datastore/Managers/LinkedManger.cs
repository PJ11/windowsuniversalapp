﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using workspace_datastore.Models;

namespace workspace_datastore.Managers
{
    public class LinkedManger
    {
        public async void AddOrUpdateLinkedContactInfoAsync(SQLiteAsyncConnection db, Linked ObjLinked)
        {
            //commenting it, no need of it if you are passing connection object in method. Also please use DataSourceAsync method always
            //DataSource ds = new DataSource();
            try
            {
                var linkedContact = db.Table<Linked>().Where(c => c._id == ObjLinked._id).FirstOrDefaultAsync().Result;

                if (linkedContact == null) // If linked contact is not present then add it in the table
                {
                    var linked = new Linked()
                    {
                        _id = ObjLinked._id,
                        avatar_link = ObjLinked.avatar_link,
                        datasource_id = ObjLinked.datasource_id,
                        datasource_type = ObjLinked.datasource_type,
                        display_name = ObjLinked.display_name,
                        first_name = ObjLinked.first_name,
                        last_name = ObjLinked.last_name,
                        primary_flag = ObjLinked.primary_flag,
                        primary_link_id = ObjLinked.primary_link_id,
                        recent_contact_flag = ObjLinked.recent_contact_flag,
                        recent_timestamp = ObjLinked.recent_timestamp,
                    };
                    int result = await db.InsertAsync(linked).ConfigureAwait(false);
                }

                else // If its already present then update the table 
                {
                    linkedContact._id = ObjLinked._id;
                    linkedContact.avatar_link = ObjLinked.avatar_link;
                    linkedContact.datasource_id = ObjLinked.datasource_id;
                    linkedContact.datasource_type = ObjLinked.datasource_type;
                    linkedContact.display_name = ObjLinked.display_name;
                    linkedContact.first_name = ObjLinked.first_name;
                    linkedContact.last_name = ObjLinked.last_name;
                    linkedContact.primary_flag = ObjLinked.primary_flag;
                    linkedContact.primary_link_id = ObjLinked.primary_link_id;
                    linkedContact.recent_contact_flag = ObjLinked.recent_contact_flag;
                    linkedContact.recent_timestamp = ObjLinked.recent_timestamp;
                }
              //  ds.Db.Update(linkedContact);
                await  db.UpdateAsync(linkedContact).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                //throw exception
                throw ex;
            }
        }


    }
}
