﻿#define USE_ASYNC
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using workspace_datastore.Managers;
using workspace_datastore.models;
using workspace_datastore.Models;

namespace workspace_datastore
{
    public class SqlcipherManager
    {
        #region VARIABLES
        /// <summary>
        /// create global variable here
        /// </summary>
        private readonly static String databasePath = Path.Combine(ApplicationData.Current.LocalFolder.Path, ConstantData.DBName);
        //private static DataSourceAsync ds;
        //private static SqlcipherManager sqlcipherManagerObj;
        #endregion
        // Constructor

        //public static SqlcipherManager getInstance()
        //{
        //    if(sqlcipherManagerObj == null)
        //       sqlcipherManagerObj = new SqlcipherManager();
        //    return sqlcipherManagerObj;
        //}
        //private SqlcipherManager()
        //{
        //    ds = new DataSourceAsync(true);
        //}
        /// <summary>
        /// Create database with all all tables in application folder
        /// </summary>
        /// <returns></returns>
        public static async Task CreateDBTables()
        {
            //Create datasource object and get connection string
#if USE_ASYNC
            DataSourceAsync ds = new DataSourceAsync(true);
#else
            DataSource ds = new DataSource(true);
#endif
            //await ds.getDataSource();
            try
            {
#if USE_ASYNC
                //create tables in database
                await ds.Db.CreateTableAsync<Account>();
                await ds.Db.CreateTableAsync<Attachment>();
                await ds.Db.CreateTableAsync<Attendees>();
                await ds.Db.CreateTableAsync<Contacts>();
                await ds.Db.CreateTableAsync<Draft_Parts>();
                await ds.Db.CreateTableAsync<Events>();
                await ds.Db.CreateTableAsync<Folder>();
                //ds.Db.CreateTableAsync<Instances>().ConfigureAwait(false);
                await ds.Db.CreateTableAsync<Message_Deletes>();
                await ds.Db.CreateTableAsync<Message_Updates>();
                await ds.Db.CreateTableAsync<Outbox>();
                //ds.Db.CreateTableAsync<Reminders>().ConfigureAwait(false);
                await ds.Db.CreateTableAsync<Message>();
                await ds.Db.CreateTableAsync<Linked>();
                await ds.Db.CreateTableAsync<EmailSearch>();
                await ds.Db.CreateTableAsync<PhoneSearch>();
                await ds.Db.CreateTableAsync<RecentFile>();
#if USE_SQL_STATEMENT
                string createMessageTableStmt = "CREATE TABLE Message (";
                createMessageTableStmt += "id INTEGER, folder_id INTEGER," +
                    "serverIdentifier TEXT," +
                    "toRecipientText TEXT," +
                    "ccRecipientText TEXT," +
                    "bccRecipientText TEXT," +
                    "fromEmail TEXT," +
                    "receivedDate DOUBLE," +
                    "subject TEXT," +
                    "read INTEGER," +
                    "importance INTEGER," +
                    "previewText TEXT," +
                    "bodyText TEXT," +
                    "mimeType TEXT," +
                    "conversationId TEXT," +
                    "conversationIndex TEXT," +
                    "lastVerbExecuted INTEGER," +
                    "flagType TEXT," +
                    "flagStatus INTEGER," +
                    "draftActionType INTEGER," +
                    "draftActionReferenceMessage_id INTEGER," +
                    "draftActionReferenceMessage_longid TEXT," +
                    "flagCompletedDate FLOAT," +
                    "flagCompletedTime FLOAT," +
                    "flagStartDate FLOAT," +
                    "flagDueDate FLOAT," +
                    "flagUTCStartDate FLOAT," +
                    "flagUTCDueDate FLOAT," +
                    "flagReminderSet, INTEGER," +
                    "flagReminderTime FLOAT," +
                    "flagOrdinalDate FLOAT," +
                    "flagSubOrdinalDate FLOAT," +
                    "flag INTEGER," +

                    "event_id INTEGER," +
                    "allDay INTEGER," +
                    "location TEXT," +
                    "timeZone TEXT," +
                    "startDayAndTime DOUBLE," +
                    "endDayAndTime DOUBLE," +
                    "uid TEXT," +
                    "availability INTEGER," +
                    "originalReminder INTEGER," +
                    "meetingStatus INTEGER," +
                    "organizerName TEXT," +
                    "organizerEmail TEXT," +
                    "PRIMARY KEY (id, folder_id))";
                ds.Db.ExecuteAsync(createMessageTableStmt).ConfigureAwait(false);
#endif // USE_SQL_STATEMENT
#else
                ds.Db.CreateTable<Account>();
                ds.Db.CreateTable<Attachment>();
                ds.Db.CreateTable<Attendees>();
                ds.Db.CreateTable<Contacts>();
                ds.Db.CreateTable<Draft_Parts>();
                ds.Db.CreateTable<Events>();
                ds.Db.CreateTable<Folder>();
                //ds.Db.CreateTable<Instances>();
                ds.Db.CreateTable<Message_Deletes>();
                ds.Db.CreateTable<Message_Updates>();
                ds.Db.CreateTable<Outbox>();
                //ds.Db.CreateTable<Reminders>();
                ds.Db.CreateTable<Message>();
#endif
            }

            catch (Exception ex)
            {
                LoggingWP8.WP8Logger.LogMessage("Exception Creating DB" + ex.ToString());  
            }
            finally
            {
                ds.Dispose();
            }
        }

        public static async Task<bool> DeleteDBTables()
        {
            DataSourceAsync ds = new DataSourceAsync(true);
            try
            {
               
                //delete tables in database
               await ds.Db.DropTableAsync<Account>();
               await ds.Db.DropTableAsync<Attachment>();
               await ds.Db.DropTableAsync<Attendees>();
               await ds.Db.DropTableAsync<Contacts>();
               await ds.Db.DropTableAsync<Draft_Parts>();
               await ds.Db.DropTableAsync<Events>();
               await ds.Db.DropTableAsync<Folder>();
               await ds.Db.DropTableAsync<Message_Deletes>();
               await ds.Db.DropTableAsync<Message_Updates>();
               await ds.Db.DropTableAsync<Outbox>();
               await ds.Db.DropTableAsync<Message>();
               await ds.Db.DropTableAsync<Linked>();
               await ds.Db.DropTableAsync<EmailSearch>();
               await ds.Db.DropTableAsync<PhoneSearch>();
               await ds.Db.DropTableAsync<RecentFile>();
                return true;

            }
            catch (Exception ex)
            {
                LoggingWP8.WP8Logger.LogMessage("Exception deleting DB" + ex.ToString());
                return false;
            }
            finally
            {
                ds.Dispose();
            }
        }
    }

}
