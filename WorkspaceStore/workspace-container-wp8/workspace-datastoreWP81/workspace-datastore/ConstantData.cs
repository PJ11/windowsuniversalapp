﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace workspace_datastore
{
    public enum MailActionType
    {
        ReferenceNone = 1,
        ReferenceReply = 2,
        ReferenceForward = 3
    };

    public enum DraftPartsType
    {
        PartMessage = 1,
        PartAttachment = 2
    };

    public class ConstantData
    {
        public static string DBName = "workstore_datastore.db";
        public static string DBPwd = string.Empty;
        public static string DBPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, DBName);
        public static string RecentEventsandUnseenEmailDBPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "workstore_recentevents_unseenemail.db");
        public static byte[] DBKey = null;
        public static string DBHexKey = string.Empty;
    }
}
